/**
 *  rends all our buttons in this application
 */

$(document).ajaxComplete(function () {
    prettify();
});

$(document).ready(function () {
    prettify();
});

function prettify() {

    try {
        //style buttons
        $("input:submit, a.button, input:button").button();

        $("div.qq-upload-button").addClass("ui-button ui-widget ui-state-default ui-corner-all")

        //style text fields
        $("input:text, select").addClass("ui-widget ui-state-default ui-corner-all");

        //prettyfy tables
        $('.colored tr:even').addClass('even');
        $('.colored tr:odd').addClass('odd');
    }
    catch (e) {
        //nothing to see here
    }
}