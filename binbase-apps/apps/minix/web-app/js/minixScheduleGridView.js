/**
 * loads a study into a grid and reacts to events to ensure that all data exist, as well allows us to remove elements from the grids
 * @param studyId
 * @param ignoreQCs
 * @param ignoreBlanks
 */
function displayStudyGrid(ignoreQCs, ignoreBlanks, url) {

    //custom button formatter to delete a row
    function buttonFormatter(row, cell, value, columnDef, dataContext) {
        var button = "<a class='remove' id='" + dataContext.id + "' ><span class='hideText'>remove</span></a>";
        return button;
    }

    //formats checks in case of true/false values
    function checkmarkFormatter(row, cell, value, columnDef, dataContext) {
        if (value === '?') {
            return "<img src='../images/16x16/maybe.png'>";
        }
        else if (value === 'true') {
            return "<img src='../images/16x16/yes.png'>";
        }
        else {
            return "<img src='../images/16x16/no.png'>";
        }
    }

    //our defined columns
    var columns = [
        {id: "del", field: "del", name: "Remove", formatter: buttonFormatter, minWidth: 50, width: 100, maxWidth: 120},
        {id: "studyId", name: "Study id", field: "studyId"},
        {id: "classId", name: "Class id", field: "classId"},
        {id: "fileName", name: "File Name", field: "fileName"},
        {id: "rawData", name: "Has Rawdata", field: "rawData", formatter: checkmarkFormatter, minWidth: 50, width: 100, maxWidth: 120}
    ];

    //defined options
    var options = {
        enableCellNavigation: true,
        enableColumnReorder: false,
        forceFitColumns: true
    };

    //load json data from url and build the grid
    $.getJSON(url, function (items) {
        var dataView = new Slick.Data.DataView();

        //our final grid
        var grid;

        //array data
        var slickdata = [];
        var studyId = items.id;
        var counter = 0;

        //build the data array
        for (var i = 0; i < items.classes.length; i++) {

            var clazz = items.classes[i];

            var clazzId = clazz.id;

            for (var x = 0; x < clazz.samples.length; x++) {
                var fileName = clazz.samples[x];

                if ((ignoreQCs == "true") && fileName.match(/.*qc.*/)) {
                    //do nothing
                }
                else if ((ignoreBlanks == "true") && fileName.match(/.*bl.*/)) {
                    //do nothing
                }
                else {
                    //add the data
                    slickdata[counter] = {
                        id: fileName.trim(),
                        studyId: studyId,
                        classId: clazzId,
                        fileName: fileName,
                        rawData: '?'
                    };

                    counter = counter + 1;
                }
            }
        }

        dataView.setItems(slickdata);

        //name of our grid
        var gridSelectorContainer = "#study_grid_" + studyId;
        var gridSelector = "#study_" + studyId;
        var gridSelectorError = "#study_grid_error_" + studyId;

        //set the data for the hidden field
        var inputName = 'study_content_' + studyId;
        var countName = 'study_count_' + studyId;

        //add required fields so the controller can get some data
        $(gridSelector).append("<div id='study_grid_error_" + studyId + "'></div>");
        $(gridSelector).append("<div id='study_grid_" + studyId + "' class='element' style='min-height: 300px' ></div>");
        $(gridSelector).append("<input type='hidden' value='' id= '" + inputName + "'name='" + inputName + "'/>");
        $(gridSelector).append("<input type='hidden' value='' id= '" + countName + "'name='" + countName + "'/>");

        //assign a jsonified version to the input field, to ensure that all the data are loaded
        var updateFields = function () {
            $('#' + inputName).val(JSON.stringify(dataView.getItems()));
            $('#' + countName).val(dataView.getItems().length);
        };


        //grid located at the study_id field
        grid = new Slick.Grid(gridSelectorContainer, dataView, columns, options);

        // Make the grid respond to DataView change events.
        dataView.onRowCountChanged.subscribe(function (e, args) {

            grid.updateRowCount();
            grid.render();
        });

        dataView.onRowsChanged.subscribe(function (e, args) {
            grid.invalidateRows(args.rows);
            grid.render();
        });

        //update our fields
        updateFields();

        //sets all elements to true
        $(gridSelector).parent().on("check_all", function (event, value) {
            //set all to true
            for (var i = 0; i < dataView.getItems().length; i++) {
                var item = dataView.getItemByIdx(i);
                item['rawData'] = 'true';
                dataView.updateItem(item['id'], item);
            }
        });

        //selecting rows based on feedback from the controller
        $(gridSelector).parent().on("update_row", function (event, value) {

            var obj = $.parseJSON(value);
            dataView.beginUpdate();
            var count = 0;
            var update = false;

            //update the missing ones to false
            $.each(obj, function (index, val) {
                var id = val.trim();
                var item = dataView.getItemById(id);

                if (item) {
                    item['rawData'] = 'false';
                    dataView.updateItem(id, item);
                    count = count + 1;
                    update = true;
                }
            });

            //if all are missing remove the whole grid
            if (count == dataView.getItems().length) {
                $(gridSelector).replaceWith("<div class='errors'>we removed this study from the scheduling, since all it's rawdata files where missing</div> ");
            }
            //otherwise tell us that some are missing
            else {
                if (update) {
                    $(gridSelectorError).empty();
                    $(gridSelectorError).html("<div class='errors'>this study is missing some samples, please remove them to proceed</div>");

                    //here we should add a checkbox to remove all the missing ones, so we don't click our self silly
                }
                //telling us all is ok
                else{
                    $(gridSelectorError).empty();
                    $(gridSelectorError).html("<div class='message'>this study has all the required data files</div>");

                }
            }

            //fire the update
            dataView.endUpdate();

        });

        //possible to delete samples from the grid. Simpler to use the grid than jquery based approaches
        grid.onClick.subscribe(function (e, args) {
            // if the delete column (where field was assigned 'del' in the column definition)
            if (args.grid.getColumns()[args.cell].field == 'del') {
                // perform delete
                // assume delete function uses data field id; simply pass args.row if row number is accepted for delete
                dataView.deleteItem(args.grid.getDataItem(args.row).id);
                args.grid.invalidate();

                //updateour data fields
                updateFields();
            }
        });
    });
}

/**
 * the verification failed and some samples are missing. It basically fires an event and let's everything in the system know
 * that the grid needs to be updated
 * @param data
 */
function verificationFailed(request, textStatus, errorThrown) {
    var data = request.responseText;
    $("#selectStudyDiv").trigger('check_all', data).trigger('update_row', data);
}