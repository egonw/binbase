    /**
     * update the species field
     */
    function updateSpecies(values){
        var species = eval("(" + values.responseText + ")");	// evaluate JSON

        if(species){

            var rselect = document.getElementById('species');

			// Clear all previous options
			var l = rselect.length;

			while (l > 0) {
				l--;
				rselect.remove(l);
			}

            //add no seleciton field
            //createOption(rselect,"-- Choose your species --","");

			// Rebuild the select
			for (var i=0; i < species.length; i++) {
				var spec = species[i];
                createOption(rselect,spec.name,spec.id);

			}
        }

        //reset organs
        rselect = document.getElementById('organs');

		// Clear all previous options
		l = rselect.length;

		while (l > 0) {
		    l--;
		    rselect.remove(l);
		}

        fireSpeciesSelect();
    }
    

    /**
     * update the organ field when we select a species
     */
    function updateOrgan(values){
        var organ = eval("(" + values.responseText + ")");	// evaluate JSON

        if(organ){

            var rselect = document.getElementById('organs');

			// Clear all previous options
			var l = rselect.length;

			while (l > 0) {
				l--;
				rselect.remove(l);
			}


            //add no seleciton field
            //createOption(rselect,"-- Choose your organ --","");


			// Rebuild the select
			for (var i=0; i < organ.length; i++) {
				var spec = organ[i];
                createOption(rselect,spec.name,spec.id);
			}
        }
    }

    /**
     * creates a new option field
     */
    function createOption(rselect,text,value){
        var opt = document.createElement('option');
		opt.text = text;
		opt.value = value;
		try {
		    rselect.add(opt, null); // standards compliant; doesn't work in IE
		}
		catch(ex) {
		    rselect.add(opt); // IE only
		}
    }

    /**
     * defines a new species
     */
    function defineNewSpecies(){

        //find out which kingdom was selected
        var zselect = document.getElementById('kingdom');
        var zopt = zselect.options[zselect.selectedIndex];

        //fire the ajax request
        new Ajax.Updater('species_select','/miniX/studieCreationWizard/ajax_defineSpecies',{asynchronous:false,evalScripts:true,parameters:'kingdomId='+zopt.value});

        //fire the selection of the new species
        fireSpeciesSelect();
    }

    /**
     * fires the species select function
     * basically fetches the organs for the current selected species
     */
    function fireSpeciesSelect(){
        var zselectSpecies = document.getElementById('species');
        var zoptSpecies = zselectSpecies.options[zselectSpecies.selectedIndex];
        new Ajax.Request('/miniX/studieCreationWizard/ajax_fetchOrganForSpecies',{asynchronous:false,evalScripts:false,onComplete:function(e){updateOrgan(e)},parameters:'speciesId=' + zoptSpecies.value});

    }

    /**
     * is fired when kingdom is selected to populate species fields
     */
    function fireKingdomSelect(){
        var zselect = document.getElementById('kingdom')
        var zopt = zselect.options[zselect.selectedIndex]
        new Ajax.Request('/miniX/studieCreationWizard/ajax_fetchSpeciesForKingdom',{asynchronous:false,evalScripts:true,onComplete:function(e){updateSpecies(e)},parameters:'kingdomId=' + zopt.value});

    }

    /**
     * sets the value for a select field
     */
    function setValueForSelect(selectId, value) {

        var rselect = document.getElementById(selectId);
		var l = rselect.length;


        for(index = 0;index < rselect.length;index++) {
            if(rselect[index].value == value){
                rselect.selectedIndex = index;
            }
       }
    }

    /**
     * adds a textfield to the destination with the given name
     */
    function addTextField(name, destination) {

        var value = "<input type='text' value='' name ='" + name + "' /><br />";
        alert(document.getElementById(destination));
        document.getElementById(destination).innerHTML += value;
    }