/**
 * schedules a study in the minix system, utilizing the provided url for the actual implementation to be used
 * @param id
 * @param url
 * @param destination
 * @param reference
 * @param classes
 * @param progressId
 * @param progressUrl
 * @param backlink - url to link back to the scheduler view
 */
function scheduleStudy(id, url, destination, reference, classes, progressId, progressUrl, backlink) {
    showProgressBar(progressId, progressUrl, 'scheduleProgress_' + id);

    $.ajax({type: 'POST', url: url, data: {classes: classes, id: id, progressId: progressId, destination: destination, reference: reference}}).done(function (data) {
        $('#scheduleProgress_' + id).empty().html("<div>" + data + "</div>");
        //updates the database which was used in case of a successful scheduling
        $.ajax({type: 'POST', url: backlink, data: {id: id, destination: destination}}).done(function (data) {
            if(data == 'false'){
                $('#scheduleProgress_' + id).empty().html("<div>we scheduled your studie, but there was an error setting the column.</div>");
            }
        });
    }).fail(function (jqXHR, data) {
            $('#scheduleProgress_' + id).empty().html("<div class='errors'>" + jqXHR.responseText + "</div>");
        });
}