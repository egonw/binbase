package minix
import grails.util.WebTest
import org.junit.BeforeClass
import org.junit.Test

class DesignStudieWebTests extends WebTest {

  void setUp() {
  }
  /**
   * first step is to sign in
   */
  @Test(timeout = 90000L)
  void testCreateSimpleStudy() {

    //needed to make ajax work
    config easyajax: 'true', easyajaxdelay: '10000';

    //clear database
    resetDatabase()

    //dumpDatabase(fileName: "testCreateSimpleStudy_before.xml")

    //our url
    invoke "http://127.0.0.1:8080/minix/"

    setInputField(name: "username", value: "admin")
    setInputField(description: "Set password field password: admin", name: "password", value: "admin")

    clickButton "Sign in"


    clickLink "user home"
    clickLink "design new studie"

    //define values
    setInputField(name: "title", value: "a simple test")

    //define one species
    setInputField(name: "species_0", value: "a")

    //next page
    clickButton(name: "submitToOrgan", "define organs")

    //define organ
    setInputField(name: "organ_1000_0", value: "a")

    //next page
    clickButton(name: "submitToTreatments", "define treatments")

    //define one treatment
    setInputField(name: "treatment_name_0", value: "a")
    setInputField(name: "treatment_value_0", value: "1")

    //next page
    clickButton(name: "submitToSamples")

    //define that our class should contain 6 weapons
    setInputField(htmlId: "numberOfSamples", value: "6")

    //next page
    clickButton(name: "ajaxSaveSamples", "display classes")

    //dumpDatabase(fileName: "testCreateSimpleStudy_beforeFailure.xml")

    //we want that our one class will be generated
    setCheckbox(name: "generate_1000_1002_1005")

    //next page
    clickButton(name: "ajaxGenerateClasses", "generate selected classes")

    //dump the generated database
    //dumpDatabase(fileName: "testCreateSimpleStudy_after.xml")

    //next page
    verifyStudyExist(studieName: "a simple test")

    verifyStudyExist(studieCount: 1)

    verifyOrgan(name: "a")

    verifyOrgan(expsectedCount: 2)

    verifySpecies(name: "a")

    verifySpecies(expsectedCount: 2)

    //clear database
    //resetDatabase()

    //dump the generated database
    //dumpDatabase(fileName: "testCreateSimpleStudy_after_reset.xml")

  }

  @Test(timeout = 90000L)
  void testRerunCreateSimpleStudy() {
    testCreateSimpleStudy()
  }

}