package minix


class SubTreatmentsWebTests extends grails.util.WebTest {

    // Unlike unit tests, functional tests are sometimes sequence dependent.
    // Methods starting with 'test' will be run automatically in alphabetical order.
    // If you require a specific sequence, prefix the method name (following 'test') with a sequence
    // e.g. test001XclassNameXListNewDelete

    void testCreationOfSubTreatments() {
        //needed to make ajax work
        config easyajax: 'true', easyajaxdelay: '10000';

        //clear database
        resetDatabase()

        //dumpDatabase(fileName: "testCreateSimpleStudy_before.xml")

        //our url
        invoke "http://127.0.0.1:8080/minix/"

        setInputField(name: "username", value: "admin")
        setInputField(description: "Set password field password: admin", name: "password", value: "admin")
        clickButton "Sign in"


        clickLink "user home"
        clickLink "design a new studie"

        setInputField(name: "title", value: "a very complicated studie")
        setInputField(name: "species_0", value: "a")

        clickButton "define another species"
        setInputField(name: "species_0", value: "b")

        clickButton "define another species"
        setInputField(name: "species_0", value: "c")

        clickButton "define another species"
        setInputField(name: "species_0", value: "d")

        clickButton "define organs"
        setInputField(name: "organ_1000_0", value: "1")

        clickButton "define another organ"
        setInputField(name: "organ_1000_0", value: "2")

        clickButton "define another organ"
        setInputField(name: "organ_1000_0", value: "3")

        setInputField(name: "organ_1001_0", value: "1")

        clickButton "define another organ"
        setInputField(name: "organ_1001_0", value: "2")

        clickButton "define another organ"
        setInputField(name: "organ_1001_0", value: "4")
        setInputField(name: "organ_1002_0", value: "1")

        clickButton "define another organ"
        setInputField(name: "organ_1002_0", value: "2")
        setInputField(name: "organ_1003_0", value: "4")

        clickButton "define another organ"
        setInputField(name: "organ_1003_0", value: "5")

        clickButton "define another organ"
        setInputField(name: "organ_1003_0", value: "6")

        clickButton "define treatments"
        setInputField(name: "treatment_name_0", value: "t")
        setInputField(name: "treatment_value_0", value: "none")

        clickButton(name: "addTreatment")

        clickButton "define a sub treatment"
        setInputField(name: "sub_treatment_name_0", value: "a")
        setInputField(name: "sub_treatment_value_0", value: "1")
        clickButton "save treatment"

        clickButton "define a sub treatment"
        setInputField(name: "sub_treatment_name_0", value: "a")
        setInputField(name: "sub_treatment_value_0", value: "2")
        clickButton "save treatment"

        clickButton "define a sub treatment"
        setInputField(name: "sub_treatment_name_0", value: "b")
        setInputField(name: "sub_treatment_value_0", value: "1")
        clickButton "save treatment"

        clickButton "define a sub treatment"
        setInputField(name: "sub_treatment_name_0", value: "b")
        setInputField(name: "sub_treatment_value_0", value: "2")
        clickButton "save treatment"

        clickButton "define a sub treatment"
        setInputField(name: "sub_treatment_name_0", value: "c")
        setInputField(name: "sub_treatment_value_0", value: "1")
        clickButton "save treatment"

        clickButton "define a sub treatment"
        setInputField(name: "sub_treatment_name_0", value: "c")
        setInputField(name: "sub_treatment_value_0", value: "2")
        clickButton "save treatment"

        clickButton "define a sub treatment"
        setInputField(name: "sub_treatment_name_0", value: "d")
        setInputField(name: "sub_treatment_value_0", value: "1")
        clickButton "save treatment"

        clickButton "define a sub treatment"
        setInputField(name: "sub_treatment_name_0", value: "d")
        setInputField(name: "sub_treatment_value_0", value: "2")
        clickButton "save treatment"

        clickButton(name: "submitToSamples")
        setInputField(name: "numberOfSamples", value: "2")

        clickButton "display classes"
        clickButton "select all classes"
        clickButton "generate selected classes"

        //next page
        verifyStudyExist(studieName: "a very complicated studie")


        verifyOrgan(name: "1")
        verifyOrgan(name: "2")
        verifyOrgan(name: "3")
        verifyOrgan(name: "4")
        verifyOrgan(name: "5")
        verifyOrgan(name: "6")

        verifyOrgan(expsectedCount: 7)

        verifySpecies(name: "a")
        verifySpecies(name: "b")
        verifySpecies(name: "c")
        verifySpecies(name: "d")


        verifySpecies(expsectedCount: 5)

        verifyStudyExist(studieCount: 1)
    }

}