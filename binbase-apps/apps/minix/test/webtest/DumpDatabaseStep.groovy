import javax.sql.DataSource
import org.dbunit.database.DatabaseConnection
import org.dbunit.dataset.IDataSet
import org.dbunit.database.IDatabaseConnection
import org.dbunit.dataset.xml.FlatXmlDataSet
/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Nov 24, 2010
 * Time: 3:46:47 PM
 * To change this template use File | Settings | File Templates.
 */
class DumpDatabaseStep extends com.canoo.webtest.steps.Step {

    String fileName = "result.xml"
    /**
     * resets the database
     */
    void doExecute() {

        def outputFile = fileName
        File file = new File("target/test-reports/data")
        file.mkdirs()

        DataSource dataSource = ctx.dataSource

        IDatabaseConnection connection = new DatabaseConnection(dataSource.connection)

        IDataSet fullDataSet = connection.createDataSet()

        File out = new File(file, outputFile)

        int counter = 1;

        boolean run = out.exists()
        while (run) {
            out = new File(file, "${counter}-${outputFile}")

            run = out.exists()
            if (counter == 100) {
                println "killed after 100 runs.."
                run = false
            }
            counter++
        }

        FlatXmlDataSet.write(fullDataSet, new FileOutputStream(out))

        null


    }
}
