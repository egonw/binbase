import javax.sql.DataSource
import groovy.sql.Sql
import ch.gstream.grails.plugins.dbunitoperator.DbUnitOperator
import org.codehaus.groovy.grails.web.context.ServletContextHolder as SCH
import org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes as GA

/**
 * simple step which resets the complete database
 */
class ResetDatabaseStep extends com.canoo.webtest.steps.Step {

    /**
     * resets the database
     */
    void doExecute() {

        try {

            def ctx = SCH.servletContext.getAttribute(GA.APPLICATION_CONTEXT)

            DbUnitOperator.create()

            try {
                DataSource dataSource = ctx.dataSource
                Sql sql = Sql.newInstance(dataSource)

                sql.execute("DROP SEQUENCE MINIX_ID")
                sql.execute("CREATE SEQUENCE MINIX_ID START WITH  1000 INCREMENT BY 1")
            }
            catch (Exception e) {
                e.printStackTrace()
                throw e
            }


        }
        catch (Exception e) {
            println "error: ${e.getMessage()}"
            e.printStackTrace()
            throw new RuntimeException(e)
        }

    }
}
