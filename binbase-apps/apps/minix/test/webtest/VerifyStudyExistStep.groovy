import minix.Studie
import static org.junit.Assert.assertTrue
import static com.sun.tools.internal.ws.wsdl.parser.Util.fail

/**
 * verfiy that the studie exist
 */
class VerifyStudyExistStep extends com.canoo.webtest.steps.Step {

  /**
   * verify existance
   */

  String studieName

  /**
   * verifyexspeceted count
   */
  Integer studieCount

  void doExecute() {

    if (studieName != null) {
      assertTrue(Studie.findByDescription(studieName) != null)
    }
    else if (studieCount != null) {
      assertTrue(Studie.list().size() == studieCount.intValue())
    }
    else {
      fail("you need to set a studie count of studie name as property!")

    }
  }
}
