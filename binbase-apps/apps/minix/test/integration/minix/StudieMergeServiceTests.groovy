package minix


import grails.test.*
import org.apache.shiro.util.ThreadContext
import org.apache.shiro.subject.Subject
import org.apache.shiro.SecurityUtils

class StudieMergeServiceTests extends GrailsUnitTestCase {

    protected void tearDown() {
        super.tearDown()
    }

    void testSimple2Merge() {

        StudieMergeService service = new StudieMergeService()

        service.userService = new UserService()

        def first = GenerateFileNameServiceTests.generateStudie(10, "first_")
        def second = GenerateFileNameServiceTests.generateStudie(10, "second_")

        first.save(flush: true)
        second.save(flush: true)

        def result = service.merge([first, second], "a merging test")

        result.classes.each {BaseClass it ->
            log.info it.name
        }

        assertTrue(result.classes.size() == 2)
    }


    void testSimple4Merge() {

        StudieMergeService service = new StudieMergeService()
        service.userService = new UserService()

        def result = service.merge([GenerateFileNameServiceTests.generateStudie(10, "first_"), GenerateFileNameServiceTests.generateStudie(10, "second_"), GenerateFileNameServiceTests.generateStudie(10, "third_"), GenerateFileNameServiceTests.generateStudie(10, "forth_")], "a merging test")

        result.classes.each {BaseClass it ->
            log.info it.name
        }

        assertTrue(result.classes.size() == 4)
    }


}
