package minix

import grails.test.GrailsUnitTestCase

class GenerateFileNameServiceTests extends GrailsUnitTestCase {
	protected void setUp() {
		super.setUp()
	}

	protected void tearDown() {
		super.tearDown()
	}

	void testGenerateFileNamesWithRollover() {
		GenerateFileNameService service = new GenerateFileNameService()

		Date date = new Date()
		Instrument instrument = Instrument.findAll()[0]
		GCTof tof = instrument
		tof.rollOver = true

		def s = generateStudie(200)
		Studie study = service.generateFileNames(s, instrument, date, ShiroUser.findAll()[0])

		def dates = new HashSet()
		study.classes.each { ExperimentClass clazz ->
			clazz.samples.each { Sample sample ->

				dates.add(sample.scheduleDate)
				println sample.fileName
				if (sample.validate() == false) {
					println sample.errors
					assert false
				}

				assert sample.getFileName() != null

			}
		}

		assertTrue(dates.size() == 4)
	}

	void testGenerateFileNamesWithoutRollover() {
		GenerateFileNameService service = new GenerateFileNameService()

		Date date = new Date()
		Instrument instrument = Instrument.findAll()[0]
		GCTof tof = instrument
		tof.rollOver = false

		Studie study = service.generateFileNames(generateStudie(200), instrument, date, ShiroUser.findAll()[0])

		def dates = new HashSet()
		study.classes.each { ExperimentClass clazz ->
			clazz.samples.each { Sample sample ->

				dates.add(sample.scheduleDate)
				println sample.fileName
				if (sample.validate() == false) {
					println sample.errors
					assert false
				}

				assert sample.getFileName() != null

			}
		}

		assertTrue(dates.size() == 1)
	}

	/**
	 * generates a study with n samples
	 * @return
	 */
	static Studie generateStudie(int sampleCount, String sampleName = null, int classes = 1, Organ organ = null, Species species = null) {

		Architecture architecture = Architecture.findAll()[0]

		Studie study = new Studie()
		study.description = "test study"
		study.architecture = architecture
		study.title = "none"
		study.name = "none"

		if (study.validate() == false) {
			println study.errors
			assert false
		}
		study.save()

		for (int x = 0; x < classes; x++) {
			TreatmentSpecific spec = new TreatmentSpecific()
			spec.setTreatment Treatment.findAll()[0]
			spec.value = "dada"

			if (spec.save() == false) {
				println spec.errors
				assert false
			}

			ExperimentClass clazz = new ExperimentClass()
			if (organ) {
				clazz.organ = organ
			} else {
				assertTrue(Organ.getAll().size() > 0)
				clazz.organ = Organ.getAll()[0]
				assertTrue(clazz.organ.name == "None");

			}

			if (species) {
				clazz.species = species
			} else {
				assertTrue(Species.getAll().size() > 0)
				clazz.species = Species.getAll()[0]
				assertTrue(clazz.species.name == "None");

			}
			clazz.treatmentSpecific = spec
			clazz.name = "none"

			int count = sampleCount / classes


			log.info "count for class is: ${count}"
			//generate the required amount of samples
			for (int i = 0; i < count; i++) {
				Sample sample = new Sample()

				if (sampleName != null) {
					sample.fileName = "${sampleName}${i}-${x}"
				} else {
					sample.fileName = "${i}-${x}"
				}

				if (sample.validate() == false) {
					println sample.errors
					assert false
				}
				if (sample.save() == false) {

					assert false
				}
				clazz.addToSamples(sample)
			}

			study.addToClasses(clazz)
			clazz.studie = study

			println "study: ${study}"
			if (clazz.validate() == false) {
				println clazz.errors
				assert false
			}

			clazz.save()

			study.save()

			clazz.name = "${clazz.id}"
			clazz.save()

			log.info "class added to study: ${clazz.name}/${clazz.id}"
		}

		study.addToUsers(ShiroUser.findAll()[0])

		if (study.validate() == false) {
			println study.errors
			assert false
		}

		architecture.addToStudies(study)
		architecture.save()

		study.save()

		return study
	}
}
