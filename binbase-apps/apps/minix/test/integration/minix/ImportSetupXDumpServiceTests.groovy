package minix
import grails.test.*
import minix.Studie
import minix.ExperimentClass
import minix.Sample
import minix.ModifyClassService
import minix.ImportSetupXDumpService

class ImportSetupXDumpServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testImportContentSuccess() {

        ImportSetupXDumpService service = new ImportSetupXDumpService()

        File file = new File("web-app/data/test/integration/success.xml")


        Studie studie = service.importContent(file.text)

        assertTrue(studie != null)

        assertTrue(studie.id != null)
        assertTrue(studie.description == "Drosophila melanogaster")
        assertTrue(studie.classes.size() == 2)

        studie.classes.each { ExperimentClass clazz ->
            assertTrue(clazz.samples.size() == 3)
            assertTrue(clazz.organ.name == "whole bodies")
            assertTrue(clazz.species.name == "Drosophila melanogaster")

            clazz.samples.each {Sample sample ->
                assertTrue(sample.fileName != null)
                assertTrue(sample.label != null)
                assertTrue(sample.comment == null)
                assertTrue(sample.fileVersion == 1)
                assertTrue(sample.toString() == "${sample.fileName}_${sample.fileVersion}")

            }

            //we should have 2 + 1 users associated

            assertTrue(studie.users.size() == 3)
        }

    }


    void testImportContentSuccessWithUpdate() {

        ImportSetupXDumpService service = new ImportSetupXDumpService()
        ModifyClassService modifyClassService = new ModifyClassService()
        service.modifyClassService = modifyClassService

        File file = new File("web-app/data/test/integration/success.xml")


        Studie studie = service.importContent(file.text)

        assertTrue(studie != null)

        int id = studie.id

        assertTrue(studie.id != null)
        assertTrue(studie.description == "Drosophila melanogaster")
        assertTrue(studie.classes.size() == 2)

        studie.classes.each { ExperimentClass clazz ->
            assertTrue(clazz.samples.size() == 3)
            assertTrue(clazz.organ.name == "whole bodies")
            assertTrue(clazz.species.name == "Drosophila melanogaster")

            clazz.samples.each {Sample sample ->
                assertTrue(sample.fileName != null)
                assertTrue(sample.label != null)
                assertTrue(sample.comment == null)
                assertTrue(sample.fileVersion == 1)
                assertTrue(sample.toString() == "${sample.fileName}_${sample.fileVersion}")

            }

            //we should have 2 + 1 users associated

            assertTrue(studie.users.size() == 3)
        }
        //attempting first update
        assert (service.importContent(file.text).id == id)

        //attempting second update

        assert (service.importContent(file.text).id == id)
    }
}
