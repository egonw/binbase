package minix

import grails.test.GrailsUnitTestCase

class StudyRemovalServiceTests extends GrailsUnitTestCase {
	StudyRemovalService service

	protected void setUp() {
		super.setUp()

		service = new StudyRemovalService()
		service.attachmentService = new AttachmentService()
		service.modifyClassService = new ModifyClassService()
		service.userService = new UserService()
	}

	protected void tearDown() {
		super.tearDown()
	}


	void testDeleteStudyWithoutAttachment() {

		def first = GenerateFileNameServiceTests.generateStudie(10, "first_")

		first.save(flush: true)

		def id = first.id

		first.refresh()

		service.delete(first)

		assertTrue(Studie.get(id) == null)
	}


	void testDeleteStudyWithAttachment() {

		def first = GenerateFileNameServiceTests.generateStudie(10, "first_")

		first.save(flush: true)

		service.attachmentService.attachFileToStudie("test".bytes, first, "example")

		first.save(flush: true)
		def id = first.id

		first.refresh()
		service.delete(first)

		assertTrue(Studie.get(id) == null)
	}

	/**
	 * can we delete a merged study, without affecting the orignal studies
	 */
	void testDeleteMergedStudy() {

		StudieMergeService mergeService = new StudieMergeService()
		mergeService.userService = new UserService()

		def first = GenerateFileNameServiceTests.generateStudie(10, "first_")
		def second = GenerateFileNameServiceTests.generateStudie(10, "second_")

		first.save(flush: true)
		second.save(flush: true)

		first.refresh()
		second.refresh()
		def third = mergeService.merge([first, second], "combined study")

		third.save(flush: true)
		third.refresh()

		assert (third.classes.size() == 2)

		def thirdId = third.id
		service.delete(third)

		assertTrue(Studie.get(thirdId) == null)

		first = Studie.get(first.id)
		second = Studie.get(second.id)

		assert (first.classes.size() == 1)
		assert (second.classes.size() == 1)

		assert (first.classes.iterator().next().samples.size() == 10)
		assert (second.classes.iterator().next().samples.size() == 10)

	}

	/**
	 * can we delete a merged study, without affecting the orignal studies
	 */
	void testDeleteSubStudy() {

		SubStudieService subStudieService = new SubStudieService()
		subStudieService.userService = new UserService()

		def first = GenerateFileNameServiceTests.generateStudie(10, "first_")
		first.save(flush: true)
		first.refresh()

		def third = subStudieService.createSubStudie(first, [first.classes.iterator().next().samples.first()], "sub", "sub sub")
		third.save(flush: true)
		third.refresh()

		assert (third.classes.size() == 1)

		def thirdId = third.id
		service.delete(third)

		assertTrue(Studie.get(thirdId) == null)

		first = Studie.get(first.id)
		assert (first.classes.size() == 1)

		assert (first.classes.iterator().next().samples.size() == 10)
	}

}
