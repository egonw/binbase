package minix

import minix.roles.RoleUtil

class UserServiceTests extends GroovyTestCase {
	UserService service

	protected void setUp() {
		super.setUp()
		service = new UserService()
	}

	protected void tearDown() {
		super.tearDown()
	}

	void testCreateUser() {
		def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "user", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

		ShiroUser user = createUser(service, params)

		assertTrue(user.validate())

		assertTrue(user.lastName == "dsadsad")
		assertTrue(user.username == "sdsad")
		assertTrue(user.samplePrefix == "sa")
		assertTrue(user.email == "dasdasd@ucd.de")
		assertTrue(user.firstName == "dsad")

		assertTrue(user.roles.contains(RoleUtil.getUserRole()))
	}

	void testCreateManagerUser() {
		def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "manager", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]


		ShiroUser user = createUser(service, params)

		assertTrue(user.validate())

		assertTrue(user.lastName == "dsadsad")
		assertTrue(user.username == "sdsad")
		assertTrue(user.samplePrefix == "sa")
		assertTrue(user.email == "dasdasd@ucd.de")
		assertTrue(user.firstName == "dsad")

		assertTrue(user.roles.contains(RoleUtil.getManagerRole()))
	}

	void testCreateAdminUser() {
		def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]


		ShiroUser user = createUser(service, params)

		assertTrue(user.validate())

		assertTrue(user.lastName == "dsadsad")
		assertTrue(user.username == "sdsad")
		assertTrue(user.samplePrefix == "sa")
		assertTrue(user.email == "dasdasd@ucd.de")
		assertTrue(user.firstName == "dsad")

		assertTrue(user.roles.contains(RoleUtil.getAdminRole()))
	}

	void testCreateAdminUserFailsNoSamplePrefix() {
		def params = [lastName: "dsadsad", username: "sdsad", email: "dasdasd@ucd.de", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

		ShiroUser user = createUser(service, params)
		assertTrue(user.validate() == false)

		println user.errors

	}

	void testCreateAdminUserFailsNoValidEmail() {
		def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

		ShiroUser user = createUser(service, params)
		assertTrue(user.validate() == false)

		println user.errors

	}

	void testCreateAdminUserFailsNoEmail() {
		def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

		ShiroUser user = createUser(service, params)

		assertTrue(user.validate() == false)

		println user.errors
	}

	void testCreateAdminUserFailsNoRole() {
//		UserService service = new UserService()

		def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

		ShiroUser user = createUser(service, params)
		assertTrue(user.validate() == false)

		println user.errors
	}

	void testCreateAdminUserFailedNoUsername() {

		def params = [lastName: "dsadsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

		ShiroUser user = createUser(service, params)

		assertTrue(user.validate() == false)
		println user.errors
	}

	void testDeleteUserWithNoStudies() {
		//define our user
		def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]


		ShiroUser user = createUser(service, params)

		assertTrue(user.validate())

		long id = user.id
		service.deleteUser(user)

		assertTrue(ShiroUser.get(id) == null)
	}


	void testDeleteUserWithStudies() {
		def params = [lastName: "dsadsad", username: "dasd", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

		ShiroUser user = createUser(service, params, 1)

		int id = user.id

		service.releaseDesigns(user)
		service.releaseStudies(user)
		service.deleteUser(user)

		assertTrue(ShiroUser.get(id) == null)
	}

	/**
	 * creates a user with the defined count of studies
	 * @param service
	 * @param params
	 * @param studieCount
	 * @return
	 */
	static ShiroUser createUser(UserService service, def params, int studieCount = 0) {
		ShiroUser user = service.createUser(params)
		if (user.validate()) {

			user.save(flush: true)

			for (int i = 0; i < studieCount; i++) {

				Studie study = GenerateFileNameServiceTests.generateStudie(20)
				//we always should have only one user
				assertTrue(study.addToUsers(user).save(flush: true).users.size() == 1 + 1 /* admin */)

				//we should have i + 1 studies for the user
				assertTrue(user.addToStudies(study).save(flush: true).studies.size() == (i + 1))
			}
			if (studieCount > 0) {
				assertTrue("we are expecting: ${studieCount} studies, but only got ${user.studies.size()}", user.studies.size() == studieCount)
			}
		} else {
			println "user has errors!"
		}
		return user
	}

	void testStudieRelease() {
		ShiroUser user = createUser(service, [lastName: "dsadsad", username: "dasd", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"], 1)

		service.releaseStudies(user)

		assertTrue("user studies should be 0, but are ${user.studies.size()}", user.studies.size() == 0)
	}

}
