package minix

import grails.test.GrailsUnitTestCase
import grails.test.ControllerUnitTestCase
import de.andreasschmitt.export.ExportService
import core.MetaDataService
import de.andreasschmitt.export.exporter.ExporterFactory
import de.andreasschmitt.export.exporter.DefaultExporterFactory

class ExportStudieControllerTests extends ControllerUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testExportStudies() {


        Studie study = AquisitionTableServiceTests.generateNamedStudie(20, false)
        GCTof tof = GCTof.list()[0]

        AquisitionTableService service = new AquisitionTableService()
        GenerateFileNameService generateFileNameService = new GenerateFileNameService()
        generateFileNameService.modifyClassService = new ModifyClassService()

        generateFileNameService.addCalibrationSamples(study, tof, new Date(), ShiroUser.list()[0])
        generateFileNameService.addReactionBlankSamples(study, tof, new Date(), ShiroUser.list()[0])
        generateFileNameService.addQualityControlSamples(study, tof, new Date(), ShiroUser.list()[0])

        ExportStudieController controller = new ExportStudieController()

        //mocking the export service, since the developer of it did a really shitty job to ensure it can be easily tested, FAIL
        controller.exportService = [
            export : { def a, def b, def c, def d, def e,def f, def g ->
                assertTrue(a instanceof String)
                assertTrue(b instanceof OutputStream)
                assertTrue(c instanceof Collection)
                assertTrue(d instanceof Collection)
                assertTrue(e instanceof Map)
                assertTrue(f instanceof Map)
                assertTrue(g instanceof Map)
            }
        ] as ExportService

        controller.metaDataService = new MetaDataService()
        controller.exportStudies([study], controller.response, "csv", "txt", "test")
    }
}
