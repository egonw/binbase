package minix
import grails.test.GrailsUnitTestCase

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/12/11
 * Time: 4:52 PM
 * To change this template use File | Settings | File Templates.
 */
class ShiroUserTests extends GrailsUnitTestCase {

  def testHasAdminRightsTrue() {

    def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

    UserService service = new UserService()

    ShiroUser user = UserServiceTests.createUser(service, params)
    assertTrue("this user should have   admin rights", ShiroUser.hasAdminRights(user))

  }

  def testHasAdminRightsFalse() {
    def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "manager", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

    UserService service = new UserService()

    ShiroUser user = UserServiceTests.createUser(service, params)

    assertFalse("this user should have  no admin rights", ShiroUser.hasAdminRights(user))

  }

  def testHasManagementRightsTrue() {
    def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "manager", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

    UserService service = new UserService()

    ShiroUser user = UserServiceTests.createUser(service, params)

    assertTrue("this user should have  management rights", ShiroUser.hasManagmentRights(user))
  }

  def testAdminHasManagementRightsTrue() {
    def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "admin", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

    UserService service = new UserService()

    ShiroUser user = UserServiceTests.createUser(service, params)

    assertTrue("this user should have  management rights", ShiroUser.hasManagmentRights(user))
  }


  def testHasManagementRightsFalse() {
    def params = [lastName: "dsadsad", username: "sdsad", samplePrefix: "sa", email: "dasdasd@ucd.de", role: "user", firstName: "dsad", passwordHash: "dasdsad", create: "Save User", action: "save", controller: "shiroUser"]

    UserService service = new UserService()

    ShiroUser user = UserServiceTests.createUser(service, params)

    assertFalse("this user should have no management rights", ShiroUser.hasManagmentRights(user))
  }


}
