package minix

import grails.test.*

class ModifyStudyServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testChangeArchitecture() {

        def first = GenerateFileNameServiceTests.generateStudie(10, "first_")

        Architecture architecture = new Architecture()
        architecture.description = "ModifyStudyServiceTests - test architecture"
        architecture.name = "ModifyStudyServiceTests - test"
        architecture.relatedAction = "none"
        architecture.relatedController = "none"
        architecture.save()

        ModifyStudyService service = new ModifyStudyService()

        Architecture old = first.architecture


        int oldStudieCountForArchitecture = old.studies.size()
        println old.studies.size()

        first = service.changeArchitecture(first,architecture)
        println old.studies.size()

        int newStudieCountForArchitecture = old.studies.size()

        assertTrue(oldStudieCountForArchitecture == newStudieCountForArchitecture + 1)
        assertTrue(architecture.studies.size() == 1)

        assertTrue(old != first.architecture)

        println first.architecture.studies.size()
        println old.studies.size()

    }
}
