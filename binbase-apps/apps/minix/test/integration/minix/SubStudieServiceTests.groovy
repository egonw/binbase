package minix

import grails.test.*

class SubStudieServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }


    void testCreateSubStudieWithOneFileAndCombinedClass() {

        SubStudieService service = new SubStudieService()

        service.userService = new UserService()

        def first = GenerateFileNameServiceTests.generateStudie(10, "first_")

        first.save(flush: true)

        def samples = [first.classes.toList()[0].samples.toList()[0]]

        def result = service.createSubStudie(first,samples,"sub studie","first sub studie",true)


        assertTrue(result.classes.size() == 1)
        assertTrue(result.classes.toList()[0].samples.size() == 1)

        assertTrue(first.subStudies.size() == 1)
    }

    void testCreateAndDeleteSubStudieWithOneFileAndCombinedClass() {

        SubStudieService service = new SubStudieService()


        service.userService = new UserService()

        def first = GenerateFileNameServiceTests.generateStudie(10, "first_")

        first.save(flush: true)

        def samples = [first.classes.toList()[0].samples.toList()[0]]

        def result = service.createSubStudie(first,samples,"sub studie","first sub studie",true)

        assertTrue(result.classes.size() == 1)
        assertTrue(result.classes.toList()[0].samples.size() == 1)

        assertTrue(first.subStudies.size() == 1)
    }






    void testCreateSubStudieWithOneFileForClassKeepOriginalClasses() {

        SubStudieService service = new SubStudieService()

        service.userService = new UserService()

        def first = GenerateFileNameServiceTests.generateStudie(10, "first_",2)

        first.save(flush: true)

        def samples = [first.classes.toList()[0].samples.toList()[0],first.classes.toList()[1].samples.toList()[0]]

        def result = service.createSubStudie(first,samples,"sub studie","first sub studie",false)

        assertTrue(result.classes.size() == 2)
        assertTrue(result.classes.toList()[0].samples.size() == 1)

        result = SubStudie.load(result.id)

        assertTrue(result.classes.size() == 2)
        assertTrue(result.classes.toList()[0].samples.size() == 1)

        assertTrue(first.subStudies.size() == 1)

    }


}
