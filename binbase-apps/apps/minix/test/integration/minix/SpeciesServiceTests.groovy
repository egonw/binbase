package minix

import grails.test.*

class SpeciesServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testCombineSpecies() {

        SpeciesService service = new SpeciesService()

        Species monkey = new Species()
        monkey.name = "monkey-1"
        monkey.kingdom = Kingdom.findAll()[0]
        monkey.save()

        Species monkey2 = new Species()
        monkey2.name = "monkey-2"
        monkey2.kingdom = Kingdom.findAll()[0]
        monkey2.save()



        Species monkey3 = new Species()
        monkey3.name = "monkey-3"
        monkey3.kingdom = Kingdom.findAll()[0]
        monkey3.save()


        Organ a = new Organ(name: "kidney")

        a.addToSpeciees(monkey)
        a.addToSpeciees(monkey2)
        a.save()

        Organ b = new Organ(name: "brain")
        b.addToSpeciees(monkey)
        b.addToSpeciees(monkey3)
        b.save()

        Organ c = new Organ(name: "knee")
        c.addToSpeciees(monkey)
        c.addToSpeciees(monkey3)
        c.save()

        def first = GenerateFileNameServiceTests.generateStudie(10, "first_", 1, a, monkey)
        def second = GenerateFileNameServiceTests.generateStudie(10, "second_", 1, b, monkey2)
        def third = GenerateFileNameServiceTests.generateStudie(10, "third_", 1, c, monkey3)

        def combined = service.combineSpecies(Kingdom.findAll()[0], "monkey", [monkey, monkey2, monkey3])

        def checkSpecies = {ExperimentClass e ->
            println "checking if correct species is applied..."
            assertTrue(e.species.equals(combined))
            assertTrue(e.species.organs.contains(a))
            assertTrue(e.species.organs.contains(b))
            assertTrue(e.species.organs.contains(c))


        }

        first.classes.each {checkSpecies(it)}
        second.classes.each {checkSpecies(it)}
        third.classes.each {checkSpecies(it)}

        println "checking name"
        assertTrue(combined.name == "monkey")

        println "checking organs"
        assertTrue(Species.findByName("monkey").organs.contains(Organ.findByName("kidney")))
        assertTrue(Species.findByName("monkey").organs.contains(Organ.findByName("brain")))
        assertTrue(Species.findByName("monkey").organs.contains(Organ.findByName("knee")))


        println "checking if the species were deleted"
        assertNull(Species.findByName("monkey-1"))
        assertNull(Species.findByName("monkey-2"))
        assertNull(Species.findByName("monkey-3"))


    }

    void testDeleteSpecies() {

        SpeciesService service = new SpeciesService()

        Species monkey = new Species()
        monkey.name = "monkey-1"
        monkey.kingdom = Kingdom.findAll()[0]
        monkey.save()

        Species monkey2 = new Species()
        monkey2.name = "monkey-2"
        monkey2.kingdom = Kingdom.findAll()[0]
        monkey2.save()



        Species monkey3 = new Species()
        monkey3.name = "monkey-3"
        monkey3.kingdom = Kingdom.findAll()[0]
        monkey3.save()


        Organ a = new Organ(name: "kidney")

        a.addToSpeciees(monkey)
        a.addToSpeciees(monkey2)
        a.save()

        Organ b = new Organ(name: "brain")
        b.addToSpeciees(monkey)
        b.addToSpeciees(monkey3)
        b.save()

        Organ c = new Organ(name: "knee")
        c.addToSpeciees(monkey)
        c.addToSpeciees(monkey3)
        c.save()

        service.deleteSpecies(monkey)
        service.deleteSpecies(monkey2)
        service.deleteSpecies(monkey3)

        assertNull(Species.findByName("monkey-1"))
        assertNull(Species.findByName("monkey-2"))
        assertNull(Species.findByName("monkey-3"))
    }
}
