package minix

import grails.test.GrailsUnitTestCase
import minix.GenerateFileNameServiceTests
import minix.Studie
import minix.ExperimentClass
import minix.Sample
import minix.CommunicationsService
import binbase.web.core.BBResult

class CommunicationsServiceTests extends GrailsUnitTestCase {

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    /**
     * test the uploading of results
     */
    void testUploadResult() {

        CommunicationsService service = new CommunicationsService()

        Studie studie = GenerateFileNameServiceTests.generateStudie(20)

        int id = studie.id

        println "studie id is: ${id}"


        assert (studie.results == null)
        assert (service.uploadResult(id, "a simple test".toString().bytes))


        studie = Studie.get(id)

        assert (studie.results.size() == 1)

        studie.results.each { BBResult res ->
            assertTrue(res.existingContent())
            assertTrue(new String(res.downloadContent()).equals("a simple test"))
        }
    }

    /**
     * test the retrival of sample ids for names
     */
    void testGetSampleIdForName() {

        CommunicationsService service = new CommunicationsService()

        Studie study = GenerateFileNameServiceTests.generateStudie(20, "test")

        int id = study.id

        study.classes.each {ExperimentClass clazz ->
            clazz.samples.each { Sample sample ->

//                println sample.fileName
	            log.info "filename: $sample.fileName"

                long sid = service.getSampleIdForName(sample.fileName)

  //              println sid
	            log.info "sid: $sid"

                assert (sid == sample.id)
            }
        }
    }
}
