package minix

import grails.test.*
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class ModifyClassServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    public void testAddSamplesClazzCount() {

        Studie studie = GenerateFileNameServiceTests.generateStudie(10, "test studie")

        ExperimentClass clazz = studie.classes.toArray()[0]

        assertTrue(clazz.samples.size() == 10)

        ModifyClassService classService = new ModifyClassService()
        clazz = classService.addSamples(clazz, 10)


        println "${clazz.samples.size()}"
        assertTrue(clazz.samples.size() == 20)

    }

    public void testAddSampleClazz() {

        Studie studie = GenerateFileNameServiceTests.generateStudie(10, "test studie")

        ExperimentClass clazz = studie.classes.toArray()[0]

        assertTrue(clazz.samples.size() == 10)

        ModifyClassService classService = new ModifyClassService()
        clazz = classService.addSample(clazz)


        println "${clazz.samples.size()}"
        assertTrue(clazz.samples.size() == 11)
    }

    public void testRemoveClassClazzStudie() {

        Studie studie = GenerateFileNameServiceTests.generateStudie(10, "test studie")

        ExperimentClass clazz = studie.classes.toArray()[0]

        ModifyClassService classService = new ModifyClassService()
        studie = classService.removeClass(clazz, studie)

        assertTrue(studie.classes.size() == 0)
        studie = Studie.get(studie.id)

        assertTrue(studie.classes.size() == 0)

    }

    public void testMergeClass() {

        log.info "testing merging of class"
        ModifyClassService classService = new ModifyClassService()


        Studie studie = GenerateFileNameServiceTests.generateStudie(10, "test studie", 4)

        log.info "generated studie has: ${studie.classes.size()} classes"
        assertTrue(studie.classes.size() == 4)


        BaseClass clazz = classService.mergeTo(studie.classes.toList()[0], studie.classes.toList()[1])

        log.info "size of merged class: ${clazz.samples.size()}"
    }

    public void testAddCalibrationClass() {


        Studie studie = GenerateFileNameServiceTests.generateStudie(10, "test studie")

        ExperimentClass clazz = studie.classes.toArray()[0]

        ModifyClassService classService = new ModifyClassService()

        classService.addCalibrationClass(studie)

        assertTrue(studie.classes.size() == 2)
        assertTrue(studie.classes.toList()[1].samples.size() == 6)


        def config = ConfigurationHolder.config
        def concentrations = config.minix.standard.concentrations.sort()

        println concentrations

        studie.classes.toList()[1].samples.eachWithIndex { def it, def index ->
            println it
        }
        studie.classes.toList()[1].samples.eachWithIndex { def it, def index ->
            assertTrue(it instanceof CalibrationSample)
            assertTrue(it.concentration != null)


            println it.concentration.concentration
            println concentrations[index]

            assertTrue(it.concentration.concentration == concentrations[index])

        }
    }


    public void testAddQualityControlClass() {


        Studie studie = GenerateFileNameServiceTests.generateStudie(10, "test studie")

        ExperimentClass clazz = studie.classes.toArray()[0]

        ModifyClassService classService = new ModifyClassService()

        classService.addQualityClass(studie,2)

        assertTrue(studie.classes.size() == 2)

        studie.classes.toList()[1].samples.eachWithIndex { def it, def index ->
            assertTrue(it instanceof QualityControlSample)

        }

        classService.addQualityClass(studie,2)
        assertTrue(studie.classes.size() == 2)

        studie.classes.toList()[1].samples.eachWithIndex { def it, def index ->
            assertTrue(it instanceof QualityControlSample)

        }
    }


    public void testDeleteClassesFromStudy() {


        Studie studie = GenerateFileNameServiceTests.generateStudie(10, "test studie with calibration and quality")

        ExperimentClass clazz = studie.classes.toArray()[0]

        ModifyClassService classService = new ModifyClassService()

        classService.addQualityClass(studie,2)
        classService.addCalibrationClass(studie)

        def classes = []
        studie.classes.each {
            classes.add(it)
        }

        classes.each {
        classService.removeClass(it,studie)
    }                 }


}
