package minix

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException
import grails.validation.ValidationException
import minix.progress.ProgressHandler
import org.apache.shiro.SecurityUtils

/**
 * used to merge two studies
 */
class StudieMergeService {

    UserService userService

    boolean transactional = false

    /**
     * does the actual merging and returns a third studie
     *
     @param first
      *
     @param second
      *
     @return
     */
    Studie merge(Collection<Studie> studies, String description, ProgressHandler progress = null, String name = null, Architecture architecture = null) {
        log.info "merging: ${studies} to a single study"

        MergedStudie mergedStudie = new MergedStudie()
        mergedStudie.description = description

        if (name == null) {
            mergedStudie.name = description
            mergedStudie.title = description
        }
        else {
            mergedStudie.name = name
            mergedStudie.title = name
        }

        //assign the studies to the internal members
        studies.eachWithIndex {Studie studie, int counter ->
            mergedStudie.addToMembers(studie)

            if (architecture == null) {
                architecture = studie.architecture
            }

            studie.forceLock()
            studie.save(flush: true)

            if (progress != null) {
                progress.handleProgress(studies.toString(), ((double) counter / ((double) studies.size() - 1) * 100) - 1, [])
            }

        }

        mergedStudie.architecture = architecture

        if (mergedStudie.validate() == false) {
            log.error(mergedStudie.errors)

            throw new ValidationException("sorry there was a validation error for the merging of these studies: ${studies}", mergedStudie.errors);
        }

        mergedStudie.forceLock()
        mergedStudie.save(flush: true)

        //assign the study to the current user
        userService.assignStudy(ShiroUser.findByUsername(SecurityUtils.subject.principal), mergedStudie)

        log.info "new studie is: ${mergedStudie} - ${mergedStudie.classes}"

        if (progress != null) {
            progress.handleProgress(studies.toString(), 100, [])
        }

        return mergedStudie

    }

}
