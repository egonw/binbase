package minix
import minix.step.Step

class DesignQueryService {

    boolean transactional = false

    /**
     * queries all the design for the given user
     * @param user
     * @return
     */
    def queryForUser(ShiroUser user, def params = [:]) {

        def result = StudieDesign.findAllByUserAndStepNotEqual(user, Step.DONE, params)

        return result
    }


    def queryAll(def params = [:]) {

        def result = StudieDesign.findAllByStepNotEqual(Step.DONE, params)

        return result
    }

    def queryCountAll() {
        return queryAll().size()
    }

    /**
     * queries the total design count for the user
     * @param user
     * @return
     */
    def queryCountForUser(ShiroUser user) {
        return queryForUser(user).size()
    }



    def queryRecentDesign(ShiroUser user, int count = 5) {
        if (ShiroUser.hasAdminRights(user)) {
            return StudieDesign.findAllByStepNotEqual(Step.DONE, [sort:'id',max: count, order: "desc"])

        }
        else {
            return StudieDesign.findAllByUserAndStepNotEqual(user, Step.DONE, [sort:'id',max: count, order: "desc"])
        }
    }
}
