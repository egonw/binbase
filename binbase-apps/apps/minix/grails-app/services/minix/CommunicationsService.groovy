package minix

import binbase.web.core.BBResult
import core.EmailService
import core.MetaDataService

/**
 * basic minix communications are available using this service
 */
class CommunicationsService {

	boolean transactional = false

	/**
	 * expose as webservice
	 */
	static expose = ['cxf']

	/**
	 * access to the metadata service
	 */
	MetaDataService metaDataService

	/**
	 * send emails
	 */
	EmailService emailService

	AttachmentService attachmentService

	/**
	 * uploads a result to the system and attaches it to the studie
	 * it assumes it's a zio
	 * @param studyId
	 * @param content
	 * @return
	 */
	boolean uploadResult(long studyId, byte[] content) {
		return uploadResult(studyId, content, "${studyId}.zip")
	}

	/**
	 * uploads and attaches an attachment
	 * @param studieId
	 * @param content
	 * @param name
	 * @return
	 */
	boolean uploadAttachment(long studieId, byte[] content, String name) {
		if (attachmentService.attachFileToStudie(content, Studie.get(studieId), name) != null) {
			return true
		} else {
			return false
		}
	}

	/**
	 * uploads a result with a specified filename
	 * @param studieId
	 * @param content
	 * @param fileName
	 * @return
	 */
	boolean uploadResult(long studieId, byte[] content, def fileName) {

		log.info("uploading a result for ${studieId}...")
		boolean success = true
		try {
			Studie study = Studie.get(studieId)

			if (study == null) {
				throw new RuntimeException("sorry the studie with the id ${studieId} was not found")
			}

			BBResult result = new BBResult()
			result.uploadDate = new Date()

			if (fileName == null) {
				result.fileName = "${studieId}.zip"
			} else {
				result.fileName = fileName
			}
			result.experiment = study

			study.addToResults(result)

			if (result.validate() == false) {
                log.info("there was an error with the upload of ${studieId}")
				log.info(result.errors)
				throw new grails.validation.ValidationException("sorry there was an error saving the result", result.errors)
			} else {
				result.save(flush: true)
				study.save(flush: true)


				log.info("attaching actual content")
				//needs to be done extra since it's not a real object
				result.uploadContent(content)
				result.save(flush: true)

				log.info "sending email notification"
				def emails = []

				study.users.each { ShiroUser user -> emails.add(user.email) }

				try {
					emailService.sendEmail(emails, "result was uploaded to minix: ${study.id}", "dear user\n your latest minix result is done and can be now downloaded. The id for your studie is '${study.id}' and the title is '${study.description}'\n\n If you have any question please feel free to contact your miniX admin under ${ShiroUser.findByUsername("admin").email}")
				}
				catch (Exception e) {
					log.error(e.getMessage(), e)
				}

				log.info("result was attached...")
			}
		}
		catch (Exception e) {
			log.error e.getMessage(), e
		}

		return success
	}

	/**
	 * returns the internal id for the given fileName
	 * @param fileName
	 * @return
	 */
	long getSampleIdForName(String fileName) {
		log.info "query sample id for name: ${fileName}"

		if (fileName == null) {
			throw new RuntimeException("sorry you need to provide a fileName!")
		}

		fileName = generateSampleName(fileName)

		Sample sample = Sample.findByFileName(fileName)

		if (sample == null) {
			log.info "no id found..."

			throw new RuntimeException("sorry was not able to find the file: ${fileName}")

		} else {
			return sample.id
		}
	}

	/**
	 * returns all the metadata as xml file
	 * @param studieId
	 * @return
	 */
	String getMetaDateAsXML(long studieId) {
		Studie study = Studie.get(studieId)

		if (study == null) {
			throw new Exception("sorry we did not find your study id! (${studieId})")
		}

		return metaDataService.generateMetaDataAsXml(study)

	}

	/**
	 * is this studie public?
	 * @param studieId
	 * @return
	 */
	boolean studieIsPublic(long studieId) {
		Studie study = Studie.get(studieId)

		if (study == null) {
			throw new Exception("sorry we did not find your study id! (${studieId})")
		}

		return study.publicExperiment
	}

	/**
	 * returns the comment for a sample
	 * @param sample
	 * @return
	 */
	String getCommentForSample(String sample) {
		try {
			sample = generateSampleName(sample)

			if (Sample.findByFileName(sample)) {
				def value = Sample.findByFileName(sample).getComment()

				if (value) {
					return value
				} else {
					return "no comment"
				}
			} else {
				return "not found"
			}
		}
		catch (Exception e) {
			log.debug(e.getMessage(), e)
			return "sorry not found"
		}
	}

	String getLabelForSample(String sample) {
		try {
			sample = generateSampleName(sample)

			if (Sample.findByFileName(sample)) {
				return Sample.findByFileName(sample).getLabel()
			} else {
				return "not found"
			}
		}
		catch (Exception e) {
			log.debug(e.getMessage(), e)
			return "sorry not found"
		}
	}

	/**
	 * returns the species name for this sample
	 * @param sample
	 * @return
	 */
	String getSpeciesForSample(String sample) {
		try {
			sample = generateSampleName(sample)

			if (Sample.findByFileName(sample)) {
				return Sample.findByFileName(sample).getExperimentClass().getSpecies().getName()
			} else {
				return "not found"
			}
		}
		catch (Exception e) {
			log.debug(e.getMessage(), e)
			return "sorry not found"
		}
	}

	/**
	 * ensures that the _[0-9] is removed from the sample
	 * @param sample
	 * @return
	 */
	String generateSampleName(String sample) {
		if (sample.indexOf("_") > 0) {
			sample = sample.substring(0, sample.lastIndexOf("_"))
		}

		log.info("generated sample name was: ${sample}")
		sample
	}

	/**
	 * returns the organ for this sample
	 * @param sample
	 * @return
	 */
	String getOrganForSample(String sample) {
		try {
			sample = generateSampleName(sample)

			if (Sample.findByFileName(sample)) {
				return Sample.findByFileName(sample).getExperimentClass().getOrgan().getName()
			} else {
				return "not found"
			}
		}
		catch (Exception e) {
			log.debug(e.getMessage(), e)
			return "sorry not found"
		}
	}

	/**
	 * returns the treatment for this sample
	 * @param sample
	 * @return
	 */
	String getTreatmentForSample(String sample) {
		try {
			sample = generateSampleName(sample)

			if (Sample.findByFileName(sample)) {
				return ((ExperimentClass) Sample.findByFileName(sample).getExperimentClass()).getTreatmentSpecific().toString()
			} else {
				return "not found"
			}
		}
		catch (Exception e) {
			log.debug(e.getMessage(), e)
			return "sorry not found"
		}
	}

	/**
	 * returns the database name used to calculated the given study
	 * @param study
	 * @return
	 */
	String getDatabaseForStudy(long studyId) {
		Studie studie = Studie.get(studieId)

		if (studie == null) {
			throw new Exception("sorry we did not find your study id! (${studieId})")
		}

		return studie.databaseColumn
	}

    /**
     * can this sample generate a new bin
     * @param sampleId
     * @return
     */
    boolean canGenerateNewBin(long sampleId){
        Sample sample =  Sample.get(sampleId)

        if(sample){
            log.info("loaded sample: ${sample}")
            BaseClass clazz = sample.getExperimentClass()

            boolean result = true;
            clazz.studie.users.each {ShiroUser user ->
                if(user.guest){
                    log.info("studies with guest users are never allowed to create bins!")
                    result =  false;
                }
            }

            return result;
        }
        else{
            log.warn("sample with id: ${sampleId} was not found")
        }

        return false
    }

}
