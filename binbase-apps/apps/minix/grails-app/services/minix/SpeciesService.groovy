package minix

import binbase.web.core.BBExperimentClass
import binbase.web.core.BBOrgan
import binbase.web.core.BBSpecies
import minix.progress.ProgressHandler

/**
 * used to modify species
 */
class SpeciesService {

    boolean transactional = false

    /**
     * combines the listed species and updates the related studies and designs
     * finaly the existing species will be deleted from the system
     * @param kingdom
     * @param species
     * @return
     */
    Species combineSpecies(Kingdom kingdom, String name, Collection<Species> species, ProgressHandler progressHandler = null, String progressId = null) {
        Species combined = new Species()

        def temp = Species.findByName(name)

        if (temp) {
            if (!species.contains(temp)) {
                species.add(temp)
            }
        }

        def speciesName = "TEMP_SPECIES_${System.currentTimeMillis()}"

        combined.name = speciesName
        combined.kingdom = kingdom

        if (!combined.validate()) {
            log.info(combined.errors)
        }

        combined.save(flush: true)

        int counter = 0

        if (progressHandler != null) {
            double percent = 0
            progressHandler.handleProgress(progressId, percent, [])
            counter = counter + 1
        }
        else{
            println "no progress handler assigned..."
        }

        //go over the species
        species.each {Species spec ->
            if (progressHandler != null) {
                double percent = ((double) counter / ((double) species.size() * 2) * 100)
                progressHandler.handleProgress(progressId, percent, [])
                counter = counter + 1
            }

            if (spec) {
                //remap organs
                def organs = []
                spec.organs.each {BBOrgan organ ->
                    if (organ) {
                        organs.add(organ)
                    }
                }

                organs.each {BBOrgan o ->
                    if (!combined.organs?.contains(o)) {
                        if (!o.speciees?.contains(combined)) {
                            o.addToSpeciees(combined)
                            o.save(flush: true)
                            combined.save(flush: true)
                        }
                    }
                }

                organs.each {BBOrgan organ ->
                    organ.removeFromSpeciees(spec)
                    spec.removeFromOrgans(organ)
                    spec.save()
                    organ.save()
                }

                //remap classes

                def classes = []
                BBExperimentClass.findAllBySpecies(spec).each {
                    classes.add(it)
                }

                classes.each {BBExperimentClass c ->
                    c.species = combined
                    c.save(flush: true)
                }

                def designs = []
                //remap designs
                DesignSpeciesConnection.findAllBySpecies(spec).each {DesignSpeciesConnection c ->
                    designs.add(c)
                }

                designs.each {DesignSpeciesConnection c ->
                    c.species = combined
                    c.save(flush: true)
                }

                //find all study designs
                StudieDesign.executeQuery("select a from StudieDesign a join a.speciees b where b.id = ? ", [spec.id]).each {StudieDesign design ->
                    design.removeFromSpeciees(spec)
                    design.addToSpeciees(combined)
                    design.save(flush: true)
                }
                spec.save(flush: true)

                deleteSpecies(spec)

            }
            if (progressHandler != null) {
                double percent = ((double) counter / ((double) species.size() * 2) * 100)
                progressHandler.handleProgress(progressId, percent, [])
                counter = counter + 1
            }

        }
        combined.save(flush: true)

        combined.name = name

        combined.save(flush: true)
        if (progressHandler != null) {
            double percent = 100
            progressHandler.handleProgress(progressId, percent, [])
            counter = counter + 1
        }
        return combined

    }

    /**
     * deletes the actual species                                                                           5
     * @param species
     */
    void deleteSpecies(Species species) {

        def organs = []

        species.organs.each {
            organs.add(it)
        }

        organs.each {   BBOrgan o ->
            o.removeFromSpeciees(species)
            o.save(flush: true)

        }
        species.save(flush: true)

        species.delete(flush: true)

    }
}
