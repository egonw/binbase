package minix.file

import core.BinBaseSystemService
import minix.FileType

class FileService {

    ///access to binbase file
    BinBaseSystemService binBaseSystemService

    boolean transactional = false

    /**
     * checks if a given file exist
     * @param sampleName
     * @param type
     * @return
     */
    boolean hasFile(String sampleName, FileType type ) {
        if(type == null){
            log.warn("no file type was speficied, assuming BINBASE_TXT file in this case!");
            return binBaseSystemService.txtFileExist(sampleName)
        }
        switch(type){
            case FileType.BINBASE_NETCDF:
                return binBaseSystemService.cdfFileExist(sampleName)
                break
            case FileType.BINBASE_TXT:
                return binBaseSystemService.txtFileExist(sampleName)
                break
        }

        //by default we return false
        return false
    }
}
