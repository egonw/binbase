package minix

import minix.Sample
import minix.Treatment
import minix.TreatmentSpecific
import minix.Species
import minix.Organ
import minix.Studie
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import minix.ShiroUser
import org.apache.shiro.SecurityUtils
import minix.ExperimentClass
import minix.progress.ProgressHandler
import minix.Architecture
import minix.util.ExternalIdUtil

class BinBaseImportService {

    boolean transactional = false

    /**
     * imports the given experiment into the system
     * and generates a studie for it
     * @param experiment
     */
    public Studie importExperiment(Experiment experiment, ProgressHandler progress = null) {

        Studie studie = null
        //the whole creation needs to be in one single transaction
        Studie.withTransaction {
            studie = new Studie()

            studie.description = "based on BinBase id ${experiment.id}"
            studie.architecture = Architecture.findByName("Leco GC-Tof")
            studie.name = "BB_${experiment.id}"
            studie.title = "BB_${experiment.id}"

            studie.save()

            //assign to current user
            ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)
            user.addToStudies(studie)
            studie.addToUsers(user)
            user.save()

            studie.save()

            //build the studie
            experiment.classes.each {def clazz ->

                //define new experiment class
                ExperimentClass experimentClass = new ExperimentClass()
                experimentClass.studie = studie


                String clazzId = clazz.id

                //assign the default values
                experimentClass.organ = Organ.findByName("None")
                experimentClass.species = Species.findByName("None")
                experimentClass.name = clazz.id

                //define threatment specific
                TreatmentSpecific specific = new TreatmentSpecific()
                specific.treatment = Treatment.findByDescription("None")
                specific.value = "None"

                specific.save()

                experimentClass.treatmentSpecific = specific

                clazz.samples.each {def sample ->

                    String sampleId = sample.id
                    String sampleName = sample.name

                    //replace all ":" with "_" to have one standard
                    sampleName.replaceAll(":", "_")

                    //we don't want underscores
                    if (sampleName.contains("_")) {
                        sampleName = sampleName.split("_")[0]
                    }

                    Sample sample1 = new Sample()
                    sample1.fileName = sampleName
                    sample1.experimentClass = experimentClass

                    sample1.save()

                    sample1.addToExternalIds(ExternalIdUtil.generateExternalId(sampleId, sample1))
                    sample1.save()

                    experimentClass.addToSamples(sample1)

                }

                studie.addToClasses(experimentClass)

                experimentClass.save()
                experimentClass.addToExternalIds(ExternalIdUtil.generateExternalId(clazzId, experimentClass))
                experimentClass.save()

            }
        }

        return studie
    }


}
