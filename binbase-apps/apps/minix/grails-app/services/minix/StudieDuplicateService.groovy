package minix

import minix.progress.ProgressHandler
import binbase.web.core.BBExperimentSample

/**
 * used to duplicate a studie for a different architecture
 */
class StudieDuplicateService {

    boolean transactional = false

    def grailsApplication

    /**
     * creates a new studie based on the given studie
     * @param studie
     * @param architecture
     */
    public Studie duplicate(Studie studie, Architecture architecture,ProgressHandler progress = null){

        Studie copy = new Studie()
        copy.architecture = architecture
        copy.name = studie.name
        copy.description = studie.description
        copy.title = studie.title

        copy.save()

        studie.classes.each {BaseClass clazz ->
            BaseClass copyClass = this.class.classLoader.loadClass(clazz.getClass().getName()).newInstance()

            copyClass.organ = clazz.organ
            copyClass.species = clazz.species
            copyClass.studie = copy

            clazz.samples.each {BBExperimentSample sample ->
                BBExperimentSample copySample = this.class.classLoader.loadClass(sample.getClass().getName()).newInstance()

                copySample.experimentClass = copyClass
                copySample.save()

                copyClass.addToSamples(copySample)

            }

            copyClass.save()
            copy.addToClasses(copyClass)
        }

        studie.users.each {ShiroUser user ->
            copy.addToUsers(user)
            user.addToStudies(copy)

            user.save()
        }


        copy.save()

        return copy

    }

}
