package minix
import javax.sql.DataSource
import org.dbunit.database.DatabaseConnection
import org.dbunit.database.IDatabaseConnection
import org.dbunit.dataset.IDataSet
import org.dbunit.dataset.xml.FlatXmlDataSet
import java.util.zip.GZIPOutputStream

/**
 * services to backup the complete system
 */
class BackupService {

  //temporaery since it does not work at this point in time
  //thanks to a dbunit issue
  boolean locked = true
  /**
   * datasource of the project
   */
  DataSource dataSource

  /**
   * generates the backup to the specified directory
   *
   * @param filePath - default current directory
   * @param compress - by default we do not compress data
   * @return
   */
  File backup(File directory = new File("backup"), boolean compress = false) {

    if(locked){
      throw new RuntimeException("this service is disabled at this point in time, do to a DBUNIT issue!")
    }

    if (!directory.exists()) {
      log.info("created directory: ${directory.mkdirs()}")

    }
    IDatabaseConnection connection = new DatabaseConnection(dataSource.connection)

    IDataSet fullDataSet = connection.createDataSet();

    File file = null
    OutputStream writer = null;
    Calendar calendar = Calendar.getInstance()

    String fileName = "minix-${calendar.get(Calendar.YEAR)}${calendar.get(Calendar.MONTH)}${calendar.get(Calendar.DAY_OF_MONTH)}-${calendar.get(Calendar.HOUR)}-${calendar.get(Calendar.MINUTE)}"

    if (compress) {
      file = new File(directory, "${fileName}.xml.gz")
    }
    else {
      file = new File(directory, "${fileName}.xml")
    }

    //make sure that the file does not exist
    int counter = 1
    String name = file.name

    while (file.exists()) {
      String newName = name.replaceFirst(".xml", ".${counter}.xml")

      //if it exists, add a number
      file = new File(directory, newName)
      counter = counter + 1
    }

    //associated output stream
    FileOutputStream out = new FileOutputStream(file)

    BufferedOutputStream buffer = new BufferedOutputStream(out)

    if (compress) {
      writer = new GZIPOutputStream(buffer)
    }
    else {
      writer = buffer
    }

    FlatXmlDataSet.write(fullDataSet, writer);

    writer.flush()
    writer.close()

    //the file where the backup was stored
    return file
  }
}
