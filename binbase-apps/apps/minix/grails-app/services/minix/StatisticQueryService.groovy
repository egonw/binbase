package minix
import minix.Species
import javax.sql.DataSource
import groovy.sql.Sql

/**
 * rusn queries against the database
 */
class StatisticQueryService {

  boolean transactional = true

  DataSource dataSource

  /**
   * todo
   * @return
   */
  def queryStudiesGroupedBySpecies(int limitTop = 10) {
    Sql sql = Sql.newInstance(dataSource)

    def result = []
    sql.eachRow("select count(a.id) as studieCount, c.name as speciesName from studie a, experiment_class b, species c where a.id = b.studie_id and c.id = b.species_id group by c.name order by studieCount DESC LIMIT ${limitTop}", {
      result.add([it.speciesName,it.studieCount])
    })

    println result

    return result
  }
}
