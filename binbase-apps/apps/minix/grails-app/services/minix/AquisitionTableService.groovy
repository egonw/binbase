package minix
import binbase.web.core.BBExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.SampleDate
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
/**
 * used to generate a aquisition table
 */
class AquisitionTableService {

    boolean transactional = true

    def progressService

    /**
     * position of a reaction blank
     */
    int reactionBlankSamplePosition = 3

    /**
     * first position of a calibration standard
     */
    int qcStartPostion = 4
    /**
     * first position of a vial
     */
    int sampleStartPosition = 10
    /**
     * position of qc6 sample
     */
    int qc6Position = qcStartPostion

    /**
     * this method generates a txt file containning all the required information
     * for a gcms experiment
     * @param studie - the studie, which is required
     *
     * @param dpMethod - optional
     * @param msMethod - optional
     * @param gcMethod - optional
     *
     * @return a datafile containg all the informations
     *
     */
    DataFile generateDataAquisitionTable(Studie studie, GCTof gcTof, String dpMethod = "", String msMethod = "", String gcMethod = "", String qcMethod = "", String asMethod = "", boolean shuffle = false, def progressId = false, def progressFactor = 1, int qualityControlInterval = 10) {

        AquisitionDataFile file = new AquisitionDataFile()
        AquisitionDataFile temp = new AquisitionDataFile()

        //define the required columns

        file.addEmptyColumn "Name"
        file.addEmptyColumn "Type"
        file.addEmptyColumn "Folder"
        file.addEmptyColumn "QCMethod"
        file.addEmptyColumn "ASMethod"
        file.addEmptyColumn "GCMethod"
        file.addEmptyColumn "MSMethod"
        file.addEmptyColumn "DPMethod"
        file.addEmptyColumn "Tray"
        file.addEmptyColumn "Vial"
        file.addEmptyColumn "Repetitions"
        file.addEmptyColumn "BarcodeVerify"
        file.addEmptyColumn "User:Dilution Factor"
        file.addEmptyColumn "User:"
        file.addEmptyColumn "User:VIAL"
        file.addEmptyColumn "User:Label"
        file.addEmptyColumn "User:Comment"
        file.addEmptyColumn "User:Class"
		file.addEmptyColumn "User:SampleId"
		

        //only for progress bars needed
        if (progressId) {
            progressService.setProgressBarValue(progressId, 0)
        }


        def qualityControlSamples = []
        def reactionBlankSamples = []
        def calibration = []

        def samples = []
        //just build the datafile
        studie.classes.each {BaseClass clazz ->

            log.info("working on class${clazz}")
            //build the actual measurment samples
            if (clazz instanceof ExperimentClass) {
                clazz.samples.each {Sample sample ->
                    samples.add(sample)
                }
            }
            //build the quality control part
            else if (clazz instanceof QualityControlClass) {
                clazz.samples.each {QualityControlSample sample ->
                    qualityControlSamples.push(sample)
                }
            }
            //build the reaction blank class
            else if (clazz instanceof ReactionBlankClass) {
                clazz.samples.each {ReactionBlankSample sample ->
                    reactionBlankSamples.push(sample)
                }
            }

            //build the calibration part
            else if (clazz instanceof CalibrationClass) {
                clazz.samples.each {CalibrationSample sample ->
                    calibration.add sample
                }

            }

        }

        if (gcTof.rollOver == false) {
            log.info("rollover is disabled!")
            //the lab prefers it to have the samples injected in a sorted manner
            samples.sort {Sample a, Sample b ->

                String nameA = a.fileName
                String nameB = b.fileName

                try {
                    SampleDate ad = SampleDate.createInstance("${nameA}_1");
                    SampleDate bd = SampleDate.createInstance("${nameB}_1");

                    return ad.numberOfDay.compareTo(bd.numberOfDay)

                }
                catch (Exception e) {
                    return nameA.compareTo(nameB)

                }

            }
        }
        else {
            log.info("roll over is enabled")
            samples.sort {it.fileName}
        }

        int counter = sampleStartPosition


        def qualityControlSamplesIterator = qualityControlSamples.iterator()
        def reactionBlankSamplesIterator = reactionBlankSamples.iterator()

        log.info("working on calibratrion samples")
        addCalibrationSample(calibration, gcTof, qcMethod, asMethod, gcMethod, msMethod, dpMethod, file, reactionBlankSamplesIterator)

        log.info("working on normal samples")

        addSamples(samples, shuffle, temp, gcTof, qcMethod, asMethod, gcMethod, msMethod, dpMethod, counter, progressId, progressFactor)

        //used to calculated where to insert the qc samples
        int position = 0

        log.info("building temp data matrix")
        //add the temp file to the data file
        temp.data.eachWithIndex {def row, int index ->

            file.addRow(row)

            //this only for quality controls
            if ((position + 1) == qualityControlInterval) {
                if (reactionBlankSamplesIterator.hasNext()) {
                    file.addRow(addSample(reactionBlankSamplesIterator.next(), gcTof, "", "", "", "", "", reactionBlankSamplePosition,gcTof.useTray))
                }
                if (qualityControlSamplesIterator.hasNext()) {
                    file.addRow(addSample(qualityControlSamplesIterator.next(), gcTof, qcMethod, asMethod, gcMethod, msMethod, dpMethod, qc6Position,gcTof.useTray))
                }
                position = 0
            }
            else {
                position++
            }

        }
        //if there is something left in the stack add it
        if (reactionBlankSamplesIterator.hasNext()) {
            file.addRow(addSample(reactionBlankSamplesIterator.next(), gcTof, "", "", "", "", "", reactionBlankSamplePosition,gcTof.useTray))
        }
        //if there is something left in the stack, add it
        if (qualityControlSamplesIterator.hasNext()) {
            file.addRow(addSample(qualityControlSamplesIterator.next(), gcTof, qcMethod, asMethod, gcMethod, msMethod, dpMethod, qc6Position,gcTof.useTray))
        }
        if (progressId) {
            progressService.setProgressBarValue(progressId, 99)
        }        //return the created file

        log.info("finished generating a1quisition matrix")
        return file
    }

    private def addSamples(ArrayList samples, boolean shuffle, AquisitionDataFile temp, GCTof gcTof, String qcMethod, String asMethod, String gcMethod, String msMethod, String dpMethod, int counter, progressId, progressFactor) {
//assign the file names
        samples.eachWithIndex {
            Sample sample, int index ->

            temp.addRow(
                    addSample(sample, gcTof, qcMethod, asMethod, gcMethod, msMethod, dpMethod, counter,gcTof.useTray)

            )
            if (progressId) {

                progressService.setProgressBarValue(progressId, index * 100 / (samples.size() * progressFactor + 5))
            }

            counter++

        }

        //shuffles our datafile  again, should not really be needed
        if (shuffle) {
            temp.shuffle()
        }
    }

    private def addCalibrationSample(ArrayList calibration, GCTof gcTof, String qcMethod, String asMethod, String gcMethod, String msMethod, String dpMethod, AquisitionDataFile file, Iterator reactionBlankSamplesIterator) {
//sort these samples
        calibration.sort {CalibrationSample a, CalibrationSample b ->

            String nameA = a.fileName
            String nameB = b.fileName

            try {
                SampleDate ad = SampleDate.createInstance("${nameA}_1");
                SampleDate bd = SampleDate.createInstance("${nameB}_1");

                return ad.numberOfDay.compareTo(bd.numberOfDay)

            }
            catch (Exception e) {
                return nameA.compareTo(nameB)

            }
        }

        //reverse the order
        calibration = calibration.reverse()

        def pos = qcStartPostion
        //add now the samples of the quality control class to the top if they exist
        calibration.eachWithIndex {
            CalibrationSample sample, int index ->

            if (index == 0 && reactionBlankSamplesIterator.hasNext()) {
                file.addRow(addSample(reactionBlankSamplesIterator.next(), gcTof, "", "", "", "", "", reactionBlankSamplePosition, gcTof.useTray))
            }
            file.addRow(
                    addSample(sample, gcTof, qcMethod, asMethod, gcMethod, msMethod, dpMethod, pos,gcTof.useTray)
            )
            pos = pos + 1

        }
    }

    /**
     * adds a sample row
     * @param sample
     * @param gcTof
     * @param qcMethod
     * @param asMethod
     * @param gcMethod
     * @param msMethod
     * @param dpMethod
     * @param counter
     * @param tray
     * @return
     */
    private ArrayList<Serializable> addSample(BBExperimentSample sample, GCTof gcTof, String qcMethod, String asMethod, String gcMethod, String msMethod, String dpMethod, int counter, String tray) {
        if(tray == null || tray == "" || tray.toLowerCase() == "none"){
            tray = "${sample.id}"
        }

        return [
                sample.fileName,  //sample file name
                'Sample',        //type of sample
                gcTof.defaultFolder,          //folder
                qcMethod,        //gc method to use
                asMethod,        //as method to use
                gcMethod,        //gc method to use
                msMethod,        //ms method to use
                dpMethod,        //dp method to use
                tray,            //tray information
                counter,              //vial position, would be nice to be able to generate
                1,               //repetitions, just 1
                "",              //unknown
                "",              //unknown
                "",              //unknown
                "",               //unknown
                sample.label,      //associated label
                sample.comment,     //associated comment
                sample.experimentClass.id,  //associated class
				sample.id		//unique samples id

        ]
    }
}
