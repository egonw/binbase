package minix

import binbase.web.core.BBContent
/**
 * used for attachments
 */
class AttachmentService {

    boolean transactional = true

    /**
     * attaches this file to the studie
     *
     @param bytes
      *
     @param studie
      *
     @param name
      *
     @return
     */
    def attachFileToStudie(byte[] bytes, Studie studie, String name) {
        FileAttachment attachment = new FileAttachment()
        attachment.studie = studie
        attachment.fileName = name
        attachment.uploadDate = new Date()
        attachment.description = "No description available"

        if (attachment.validate()) {

            attachment.save(flush: true)
            studie.addToAttachments(attachment).save(flush: true)

            attachment.uploadContent(bytes)
            attachment.save(flush: true)

        }
        else {
            log.error(attachment.errors)
        }

        return attachment
    }

    /**
     * deletes a file from the studie
     * @return
     */
    def deleteFileFromStudie(FileAttachment fileAttachmentInstance) {

        BBContent.findAllByResult(fileAttachmentInstance)?.each {
            it.delete(flush:true)
        }
        fileAttachmentInstance.save(flush:true)
        fileAttachmentInstance.studie.removeFromAttachments(fileAttachmentInstance)
        fileAttachmentInstance.studie?.save(flush: true)
        fileAttachmentInstance?.delete(flush: true)
    }

}
