package minix

import org.apache.shiro.crypto.hash.Sha256Hash
import minix.roles.RoleUtil
import edu.ucdavis.genomics.metabolomics.exception.ValidationException
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

class UserService {

    boolean transactional = true

    //ip service to locate owners
    def geoIpService

    /**
     * creates a user based on the params
     * @param params
     * @return
     */
    ShiroUser createUser(def params) {

        ShiroUser shiroUser = null

        //it all has to happen in a transaction

        if (params.passwordHash != null && params.passwordHash.toString().equals("") == false) {
            params.passwordHashClear = params.passwordHash
            params.passwordHash = new Sha256Hash(params.passwordHash).toHex()
        }

        shiroUser = new ShiroUser(params)

        //if there is an empty string we got no role assigned and so the user will be invalid
        if (params.role.toString().size() > 0 && params.role.toString() != "null") {
            ShiroRole role = RoleUtil.getRole(params.role.toString())
            shiroUser.addToRoles(role)
            role.addToUsers(shiroUser)

            if (shiroUser.validate()) {
                shiroUser.save()
                ShiroUserPermission permisison = new ShiroUserPermission()
                permisison.permission = "*:*"
                permisison.user = shiroUser
                shiroUser.addToPermissionMappings(permisison)

                permisison.save()
            }

            if (shiroUser.validate()) {
                shiroUser.save(flush: true)
                role.save(flush: true)
            }
        }



        return shiroUser
    }

    /**
     * deletes the given user
     * @param user
     * @return
     */
    def deleteUser(ShiroUser user) {

        if (user.studies?.size() > 0) {
            return "user has still studies associated, please release them first!"
        }

        if (user.studieDesigns?.size() > 0) {
            return "user has still designs associated, please release them first!"

        }

        user.roles.each {ShiroRole role ->
            role.removeFromUsers(user)
            role.save()

        }

        user.delete(flush: true)

        return true

    }

    /**
     * releases all the associated studies from this user
     * @param user
     * @return
     */
    def releaseStudies(ShiroUser user) {
        if (user != null) {

            if (user.studies != null) {
                def studies = user.studies
                def temp = []

                studies.each {
                    temp.add it
                }

                temp.each { Studie studie ->
                    user.studies.remove(studie)
                    studie.removeFromUsers user

                    studie.save(flush: true)

                }
                user.save(flush: true)


                assert (user.studies.size() == 0)
            }
        }

        return user
    }

    /**
     * release all the associated designs of this user
     * @param user
     * @return
     */
    def releaseDesigns(ShiroUser user) {
        if (user != null) {

            if (user.studieDesigns != null) {
                def designs = user.studieDesigns
                def temp = []

                designs.each {
                    temp.add it
                }

                temp.each { StudieDesign design ->
                    user.studieDesigns.remove(design)
                    design.user = null
                    design.save(flush: true)

                }

                user.save(flush: true)


                assert (user.studieDesigns.size() == 0)
            }
        }

        return user

    }

    /**
     * assigns a user permission
     * @param shiroUser
     * @return
     */
    def assignPermissions(ShiroUser shiroUser) {

        ShiroUserPermission permisison = new ShiroUserPermission()
        permisison.permission = "*:*"
        permisison.user = shiroUser


        if (permisison.validate() == false) {
            log.error(permisison.errors)
            throw new ValidationException("an error occured", permisison.errors)
        }

        shiroUser.addToPermissionMappings(permisison)

        permisison.save(flush: true)
        shiroUser.save(flush: true)
    }

    /**
     * assigns the role to this user
     * @param shiroUser
     * @param role
     * @return
     */
    def assignRole(ShiroUser shiroUser, ShiroRole role) {

        shiroUser.addToRoles(role)
        role.addToUsers(shiroUser)

        role.save()

        shiroUser.save(flush: true)

    }

    /**
     * logs the login of a user
     * @param user
     * @return
     */
    def logLogin(String username) {

        UserTracking tracking = new UserTracking()
        tracking.username = username
        tracking.ipAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr()
        tracking.event = "login"

        def location = geoIpService.getLocation(tracking.ipAddress)

        //assign location data
        if (location != null) {
            tracking.countryCode = location.countryCode
            tracking.countryName = location.countryName
            tracking.region = location.region
            tracking.regionName = location.regionName
            tracking.city = location.city
            tracking.postalCode = location.postalCode
            tracking.timezone = location.timezone
            tracking.latitude = location.latitude
            tracking.longitude = location.longitude
            tracking.dmaCode = location.dmaCode
            tracking.areaCode = location.areaCode
            tracking.metroCode = location.metroCode
        }

        tracking.save(flush: true)
    }

    /**
     * logs the logout of a user
     * @param user
     * @return
     */
    def logLogout(String username) {

        UserTracking tracking = new UserTracking()
        tracking.username = username
        tracking.ipAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr()
        tracking.event = "logout"

        def location = geoIpService.getLocation(tracking.ipAddress)

        //assign location data
        if (location != null) {
            tracking.countryCode = location.countryCode
            tracking.countryName = location.countryName
            tracking.region = location.region
            tracking.regionName = location.regionName
            tracking.city = location.city
            tracking.postalCode = location.postalCode
            tracking.timezone = location.timezone
            tracking.latitude = location.latitude
            tracking.longitude = location.longitude
            tracking.dmaCode = location.dmaCode
            tracking.areaCode = location.areaCode
            tracking.metroCode = location.metroCode
        }

        tracking.save(flush: true)
    }

    /**
     * logs the failed login
     * @param username
     * @return
     */
    def logLoginFailed(String username) {

        UserTracking tracking = new UserTracking()
        tracking.username = username
        tracking.ipAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr()
        tracking.event = "failed"


        def location = geoIpService.getLocation(tracking.ipAddress)

        //assign location data
        if (location != null) {
            tracking.countryCode = location.countryCode
            tracking.countryName = location.countryName
            tracking.region = location.region
            tracking.regionName = location.regionName
            tracking.city = location.city
            tracking.postalCode = location.postalCode
            tracking.timezone = location.timezone
            tracking.latitude = location.latitude
            tracking.longitude = location.longitude
            tracking.dmaCode = location.dmaCode
            tracking.areaCode = location.areaCode
            tracking.metroCode = location.metroCode
        }

        tracking.save(flush: true)
    }

    /**
     * assigns rights to a given studie for a given user
     * @param user
     * @param studie
     * @return
     */
    def assignStudy(ShiroUser user, Studie studie) {
        user.addToStudies(studie)
        studie.addToUsers(user)

        if (studie.validate() == false) {
            log.error(studie.errors)
        }

        studie.save()
        user.save()

        user.save(flush: true)
        studie.save(flush: true)
    }

    /**
     * revokes access rights for this studie
     * @param user
     * @param studie
     * @return
     */
    List revokeStudyFromUser(ShiroUser user, Studie studie) {

        user.removeFromStudies(studie)
        studie.removeFromUsers(user)
        if (user.validate() && studie.validate()) {
            user.save(flush: true)
            studie.save(flush: true)

        }
        return [user, studie]
    }
}
