package minix

/**
 * helps with the generation of urls
 */
class UrlService {

	def grailsApplication

	boolean transactional = false

	def tinyurlService

	/**
	 * generates a url so it can be accessed at a later point in time
	 * @param params
	 */
	def generateURL(Map params) {

		def g = grailsApplication.mainContext.getBean('org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib')

		return g.createLink(controller: params.controller, action: params.action, params: params, absolute: true)
	}

	/**
	 * generates a quick view url
	 * @param params
	 * @param controller
	 * @param action
	 * @return
	 */
	def generateQuickLinkURL(Map params,String controller = "quickView", String action = "quickView",boolean tiny = true) {

		def g = grailsApplication.mainContext.getBean('org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib')


		def url = g.createLink(controller: controller, action:action, params: [url:generateURL(params)], absolute: true)


		log.debug("generated url: ${url}")
		if (tiny){
			try{
				String tinyUrl = tinyurlService.tiny(url)

				log.debug(" => generated tiny url: ${tinyUrl}")
				url = tinyUrl
			}
			catch (Exception e) {
				log.warn("an exception happened!, no tiny generation possible, returning original url...",e)
			}
		}

		return url
	}
}
