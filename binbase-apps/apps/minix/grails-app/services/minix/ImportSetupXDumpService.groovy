package minix

import binbase.web.core.ExternalId
import grails.validation.ValidationException
import minix.progress.ProgressHandler
import minix.roles.RoleUtil
import minix.util.ExternalIdUtil
import org.apache.shiro.crypto.hash.Sha256Hash

/**
 * imports a setupx dump file
 */
class ImportSetupXDumpService {

    ModifyClassService modifyClassService

    UserService userService

    boolean transactional = false

    /**
     * imports a complete directory
     * and ignores all errors
     * @param file
     */
    void importDirectory(File dir, ProgressHandler progress = null, String id = "") {

        def files = dir.listFiles()

        files.eachWithIndex { File file, int index ->

            //calculate the progress
            double v = (double) (index + 1) / (double) files.size() * 100

            try {

                log.info("importing ${file}")
                importContent(file.text)

                if (progress != null) {
                    progress.handleProgress(id, v, [file: file, success: true, dir: dir])
                }
            }
            catch (Exception e) {
                if (progress != null) {
                    progress.handleProgress(id, v, [file: file, success: false, error: e, dir: dir])
                }
                log.error(e.getMessage(), e)
            }
        }
    }
    /**
     * imports the given file into the system
     * @param file
     * @return
     */
    Studie importContent(String content, boolean strict = false) {

        //log.info "${content}"

        Studie studie = null

        Studie.withTransaction {

            def root = new XmlSlurper().parseText(content)

            //build the studie
            String id = root.@id.text()
            String title = root.@title.text()


            if (id == null | id.size() == 0) {
                throw new ValidationException("sorry the id needs to be defined!", studie.errors)
            }

            if (title == null | title.size() == 0) {
                throw new ValidationException("sorry the title needs to be defined!", studie.errors)
            }

            //use the existing studie so we can recycle id's
            ExternalId external = ExternalId.findByValue(id)

            if (external != null && external.association instanceof Studie) {
                log.info "studie already exist's in the system"
                studie = external.association
                throw new ValidationException("sorry this study already exist in the system and so can't be imported again!")

            } else {
                log.info "adding as new studie"

                studie = new Studie()
                studie.architecture = Architecture.findByName("Leco GC-Tof")

            }

            studie.name = id
            studie.title = title
            studie.description = title

            //validate the studie
            if (studie.validate() == false) {
                log.error(studie.errors)
                throw new ValidationException("an error occurred", studie.errors)
            }

            studie.save()

            if (external == null) {
                log.info "register external id..."
                studie.addToExternalIds(ExternalIdUtil.generateExternalId(id, studie))
                studie.save()

            }

            Set sampleCache = new HashSet()

            //build the classes
            root.classes."class".each { def clazz ->

                ExternalId externalClassId = ExternalId.findByValue(clazz.@id.text())

                ExperimentClass experiementClass = new ExperimentClass()

                if (externalClassId != null && externalClassId.association instanceof ExperimentClass) {
                    experiementClass = (ExperimentClass) externalClassId.association
                    experiementClass.name = "${experiementClass.id}"
                    log.info("found existing class and using it...")
                } else {
                    experiementClass.name = "none"
                    log.info "creating new class..."
                }
                Species species = defineSpecies(clazz.@species.text())
                Organ organ = defineOrgan(clazz.@organ.text(), species)
                TreatmentSpecific treatmentSpecific = defineTreatmentSpecific()

                experiementClass.organ = organ
                experiementClass.species = species
                experiementClass.treatmentSpecific = treatmentSpecific
                experiementClass.studie = studie


                experiementClass.save(flush: true)

                //define all the samples
                clazz.samples.sample.each { def sample ->

                    String fileName = sample.@fileName.text()


                    if (fileName.size() == 0) {
                        if (strict) {
                            throw new ValidationException("sorry there was no filename defined", studie.errors)
                        } else {
                            log.warn("ignored sample without filename!")
                        }
                    } else {

                        //defines the filename properties
                        def names = fileName.replaceAll(":", "_").split(",")
                        fileName = names[names.length - 1]

                        int version = Integer.parseInt(fileName.substring(fileName.lastIndexOf("_") + 1, fileName.length()))
                        fileName = fileName.substring(0, fileName.lastIndexOf("_"));

                        log.info("created file name: $fileName and version $version")
                        //int version = Integer.parseInt(fileName.split("_")[1])
                        //fileName = fileName.split("_")[0]

                        //make sure there are no duplicated sampels in this dataset
                        if (sampleCache.contains(fileName) == false) {
                            sampleCache.add(fileName)

                            Sample experimentSample = Sample.findByFileName(fileName)

                            if (experimentSample == null) {
                                log.info "creating new sample..."
                                experimentSample = new Sample()
                            } else {
                                log.info("using existing sample: ${experimentSample}")
                            }

                            experimentSample.fileName = fileName
                            experimentSample.experimentClass = experiementClass

                            log.info("attempting to import: ${fileName}")

                            if (sample.@label != null && sample.@label.text().size() > 0) {
                                experimentSample.label = sample.@label.text()
                            }

                            if (sample.@comment != null && sample.@comment.text().size() > 0) {
                                experimentSample.comment = sample.@comment.text()
                            }


                            if (experimentSample.validate() == false) {
                                log.error(experimentSample.errors)
                                throw new ValidationException("an error occured", experimentSample.errors)
                            }

                            experimentSample.save(flush: true)

                            ExternalId sampleId = ExternalId.findById(sample.@id.text())

                            if (sampleId == null) {
                                experimentSample.addToExternalIds(ExternalIdUtil.generateExternalId(sample.@id.text(), experimentSample))
                            }

                            if (experiementClass.samples) {
                                if (!experiementClass.samples.contains(experimentSample)) {
                                    experiementClass.addToSamples(experimentSample)
                                }
                            } else {
                                experiementClass.addToSamples(experimentSample)
                            }

                            experimentSample.save(flush: true)
                        } else {
                            log.warn "sample ${fileName} was ignored since it was already found in this experiment"
                        }
                    }
                }

                if (experiementClass.validate() == false) {
                    log.error(experiementClass.errors)

                    if (strict) {
                        throw new ValidationException("an error occured", experiementClass.errors)
                    }
                } else {

                    experiementClass.save()

                    if (externalClassId == null) {
                        experiementClass.addToExternalIds(ExternalIdUtil.generateExternalId(clazz.@id.text(), experiementClass))
                    }

                    if (studie.classes) {
                        if (!studie.classes.contains(experiementClass)) {
                            studie.addToClasses(experiementClass)
                        }
                    } else {
                        studie.addToClasses(experiementClass)
                    }

                    experiementClass.name = "${experiementClass.id}"

                    experiementClass.save()
                }
            }
            studie.save(flush: true)

            log.info "locking studie..."
            studie.forceLock()
            //assign users

            if (root.users != null) {
                root.users.user.each { def user ->
                    String name = user.@name.text()
                    String password = user.@password.text()
                    String email = user.@email.text()
                    String adminString = user.@superAdmin.text().toString()

                    try {
                        boolean admin = adminString.toLowerCase().equals("true")



                        if (name == null | name.size() == 0) {
                            throw new ValidationException("sorry a username is required!", studie.errors)
                        }

                        if (password == null | password.size() == 0) {
                            throw new ValidationException("sorry a password is required!", studie.errors)
                        }

                        if (email == null | email.size() == 0) {
                            throw new ValidationException("sorry an email is required!", studie.errors)
                        }

                        ShiroUser shiroUser = ShiroUser.findByUsername(name)

                        if (shiroUser == null) {
                            shiroUser = new ShiroUser()

                            shiroUser.username = name
                            shiroUser.passwordHash = new Sha256Hash(password).toHex()
                            shiroUser.email = email
                            shiroUser.samplePrefix = name.substring(0, 2)

                            //imported users are always locked since they can't be trusted
                            shiroUser.locked = true



                            ShiroRole role = null;

                            if (admin) {
                                println "assigning manager role..."
                                role = RoleUtil.getManagerRole()
                            } else {
                                println "assigning user role"
                                role = RoleUtil.getUserRole()
                            }

                            println "${admin} - ${adminString} - ${name} - ${role}"


                            shiroUser.addToRoles(role)
                            role.addToUsers(shiroUser)

                            role.save()
                            shiroUser.save(flush: true)


                            if (shiroUser.validate() == false) {
                                log.error(shiroUser.errors)
                                throw new ValidationException("an error occured", shiroUser.errors)
                            }

                            shiroUser.save(flush: true)


                            ShiroUserPermission permisison = new ShiroUserPermission()
                            permisison.permission = "*:*"
                            permisison.user = shiroUser


                            if (permisison.validate() == false) {
                                log.error(permisison.errors)
                                throw new ValidationException("an error occured", permisison.errors)
                            }

                            shiroUser.addToPermissionMappings(permisison)

                            permisison.save(flush: true)
                            shiroUser.save(flush: true)


                        }

                        shiroUser.addToStudies(studie)
                        studie.addToUsers(shiroUser)
                        studie.save(flush: true)
                        shiroUser.save(flush: true)
                    }
                    catch (Exception e) {
                        log.debug("error during user association - ignored")
                    }
                }

                //associate admin account
                ShiroUser shiroUser = ShiroUser.findByUsername("admin")
                if (shiroUser != null) {
                    shiroUser.addToStudies(studie)
                    studie.addToUsers(shiroUser)
                    studie.save(flush: true)
                    shiroUser.save(flush: true)
                }
            } else {
                log.info "no users found..."
            }
        }

        //flushing the database so it knows about it
        studie.save(flush: true)
        return studie
    }

    private def defineOrgan(String organName, Species species) {
        Organ organ = null

        if (organName.size() == 0) {
            organ = createOrgan("None", species, organ)

        } else {
            organ = createOrgan(organName, species, organ)
        }
        return organ

    }

    private Organ createOrgan(String organName, Species species, Organ organ) {
        organ = Organ.findByNameLike(organName)

        if (organ == null) {
            organ = new Organ()
            organ.name = organName
            species.addToOrgans(organ)

            if (organ.validate() == false) {
                log.error(organ.errors)
                throw new ValidationException("an error occured", organ.errors)
            }
            organ.save()

        }
        return organ
    }

    private Species defineSpecies(String speciesName) {
        Species species = null

        if (speciesName.size() == 0) {
            species = createSpecies(Species.findByNameLike("None"), "None")

        } else {
            species = createSpecies(Species.findByNameLike(speciesName), speciesName)
        }
        return species
    }

    private Species createSpecies(Species species, String speciesName) {
        if (species == null) {
            species = new Species()
            species.name = speciesName
            species.kingdom = Kingdom.findByName("None")

            if (species.validate() == false) {
                log.error(species.errors)
                throw new ValidationException("an error occured", species.errors)
            }
            species.save()

        }
        return species
    }

    private TreatmentSpecific defineTreatmentSpecific() {
        Treatment treatment = Treatment.findByDescription("None")
        TreatmentSpecific spec = new TreatmentSpecific()
        spec.treatment = treatment
        spec.value = "None"
        treatment.addToSpecifics(spec)

        spec.save()
        return spec
    }

    /**
     * generates our external id
     *
     @param id
      *
     @param association
      *
     @return
     */

}

