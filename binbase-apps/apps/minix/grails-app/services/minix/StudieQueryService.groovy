package minix
/**
 * a dedicated class for studie queries
 */
class StudieQueryService {

    boolean transactional = false

    /**
     * queries all the studies for the given user
     * @param user
     * @return
     */
    def queryForUser(ShiroUser user, def params = [:]) {

        if(params.order && params.sort){
            return Studie.executeQuery("select a from Studie a, IN (a.users) u where u.username = ? order by a.${params.sort} ${params.order}", [user.username], params)
        }
        else{
        return Studie.executeQuery("select a from Studie a, IN (a.users) u where u.username = ?", [user.username], params)
        }
    }

    /**
     * queries the total studie count for the user
     * @param user
     * @return
     */
    def queryCountForUser(ShiroUser user) {
        return Studie.executeQuery("select a from Studie a, IN (a.users) u where u.username = ?", [user.username]).size()

    }

    /**
     *
     * queries the total studie count for the user and applies the provided filter
     * queries all the studies for the given user
     * @param user
     * @return
     */
    def queryForUserWithFilter(ShiroUser user, def params = [:], def filter) {
        long id = 0

        try {
            id = Long.parseLong(filter)
        }
        catch (Exception e) {
            id = Long.MIN_VALUE
        }

        if (params.sort == null){
            params.sort = "id"
        }

        if (params.order == null){
            params.order = "ASC"
        }

        return Studie.executeQuery("select a from Studie a, IN (a.users) u where u.username = ? and (lower(a.description) LIKE ? OR a.id = ? )  order by a.${params.sort} ${params.order}", [user.username, "%${filter.toString().toLowerCase()}%", id], params)

    }

    /**
     * queries the total studie count for the user and applies the provided filter
     * @param user
     * @return
     */
    def queryCountForUserWithFilter(ShiroUser user, String filter) {
        long id = 0

        try {
            id = Long.parseLong(filter)
        }
        catch (Exception e) {
            id = Long.MIN_VALUE
        }

        return Studie.executeQuery("select a from Studie a, IN (a.users) u where u.username = ? and (lower(a.description) LIKE ? OR a.id = ?)", [user.username, "%${filter.toString().toLowerCase()}%", id]).size()

    }

    /**
     *
     * queries the total studie count for the user and applies the provided filter
     * queries all the studies for the given user
     * @param user
     * @return
     */
    def queryForStudiesWithFilter(def params = [:], def filter) {

        long id = 0

        try {
            id = Long.parseLong(filter)
        }
        catch (Exception e) {
            id = Long.MIN_VALUE
        }


        if (params.sort == null){
            params.sort = "id"
        }

        if (params.order == null){
            params.order = "ASC"
        }

        return Studie.executeQuery("select a from Studie a where (lower(a.description) LIKE ? OR a.id = ?)  order by a.${params.sort} ${params.order}", ["%${filter.toString().toLowerCase()}%", id], params)

    }

    /**
     * queries the total studie count for the user and applies the provided filter
     * @param user
     * @return
     */
    def queryCountForStudiesWithFilter(String filter) {
        long id = 0

        try {
            id = Long.parseLong(filter)
        }
        catch (Exception e) {
            id = Long.MIN_VALUE
        }

        return Studie.executeQuery("select a from Studie a  where (lower(a.description) LIKE ? OR a.id = ?)", ["%${filter.toString().toLowerCase()}%", id]).size()

    }


    def queryRecentStudies(ShiroUser user, int count = 5) {
        if (ShiroUser.hasAdminRights(user)) {
            return Studie.executeQuery("select a from Studie a ORDER by a.id DESC", [max: count])

        }
        else {
            return Studie.executeQuery("select a from Studie a, IN (a.users) u where u.username = ? ORDER by a.id DESC", [user.username], [max: count])
        }
    }


    def queryRecentCalculatedStudies(ShiroUser user, int count = 5) {
        if (ShiroUser.hasAdminRights(user)) {
            return Studie.executeQuery("select b from Studie b, IN (b.results)  r where b.id IN (select distinct a.experiment.id from BBResult a) and r.uploadDate is not null order by r.uploadDate DESC", [max: count])

        }
        else {
            return Studie.executeQuery("select b from Studie b, IN (b.results)  r, IN (b.users) u where u.username = ? and b.id IN (select distinct a.experiment.id from BBResult a) and r.uploadDate is not null order by r.uploadDate DESC",[user.username], [max: count])
        }
    }
}
