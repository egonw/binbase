<table>
    <thead>
    <tr>
        <th>Id</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${queue}" var="job">
        <g:if test="${job instanceof edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL}">
            <tr>
                <td>${((edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL)job).getId()}</td>
            </tr>

        </g:if>
    </g:each>
    </tbody>
</table>