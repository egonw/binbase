<%@ page import="minix.Studie; edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment" %>
<table>
    <thead>
    <tr>
        <th>Database</th>
        <th>Study Id</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${queue}" var="job">
        <tr>
            <td>${((Experiment) job).column}</td>

            <%

                Studie s = null

                try {
                    s = Studie.get(Long.parseLong(job.id))

                }
                catch (Exception e) {}
            %>

            <g:if test="${s != null}">
                <td><g:link controller="studie" action="show"
                            id="${((Experiment) job).id}">${((Experiment) job).id} - ${s.description}</g:link></td>

            </g:if>
            <g:else>
                <td>${((Experiment) job).id}</td>
            </g:else>

        </tr>
    </g:each>
    </tbody>
</table>