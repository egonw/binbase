<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showClass.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showStudie.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>


    <title>BinBase Cluster Queue</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">
        <div class="left">
            <div class="header">

                Cluster Information

            </div>

            <div class="element">
                <div class="scaffoldingProperties">

                    <div id="availableNodes">loading...</div>

                    <div id="brokenNodes">loading...</div>

                    <div id="runningNodes">loading...</div>

                    <div id="configuredNodes">loading...</div>

                    <div id="usableNodesForBinBase">loading...</div>


                    <div id="configuredAutostart">loading...</div>

                </div>
            </div>

            <div class="header">

                Settings

            </div>

            <div class="element">
                <div class="scaffoldingProperties">
                    <span class="scaffoldingPropertyLabel">
                        <label for="refresh">Auto refresh</label>
                    </span>

                    <span class="scaffoldingPropertyValue">

                        <g:checkBox name="refresh" checked="false "/>
                    </span>
                </div>
            </div>


            <g:if test="${minix.ShiroUser.hasAdminRights()}">
                <div class="header">

                    Options

                </div>

                <div class="element">
                    <div class="scaffoldingProperties">
                        <span class="scaffoldingPropertyLabel">
                            <label for="refresh">Start Node</label>
                        </span>

                        <span class="scaffoldingPropertyValue">

                            <g:remoteLink class="schedule" action="ajaxStartNode" update="message">start node</g:remoteLink>
                        </span>

                        <span class="scaffoldingPropertyLabel">
                            <label for="refresh">Clear calculation queue</label>
                        </span>

                        <span class="scaffoldingPropertyValue">

                            <g:remoteLink class="remove" action="ajaxClearQueue" update="message">clear queue</g:remoteLink>
                        </span>
                    </div>
                </div>

            </g:if>
        </div>

        <div class="left-center-column">

            <div id="autostartDisabled"></div>

            <div id="message"></div>

            <div class="header">
                BinBase Cluster Calculation Queue
            </div>

            <div id="logfile"></div>

            <div class="textDescription">
                Currently running jobs of the cluster
            </div>


            <div class="element">
                <div id="cluster-queue">
                    loading...
                </div>
            </div>


            <div class="textDescription">
                Current pending jobs of the cluster
            </div>


            <div class="element">
                <div id="cluster-pending">
                    loading...
                </div>
            </div>


            <div class="textDescription">
                Currently scheduled exports
            </div>

            <div class="element">
                <div id="cluster-export">
                    loading...
                </div>
            </div>

            <div class="textDescription">
                Currently scheduled imports
            </div>

            <div class="element">
                <div id="cluster-import">
                    loading...
                </div>
            </div>

            <div class="textDescription">
                Currently scheduled DSL jobs...
            </div>

            <div class="element">
                <div id="cluster-dsl-schedule">
                    loading...
                </div>
            </div>

            <div class="textDescription">
                Currently jobs waiting to be scheduled...
            </div>

            <div class="element">
                <div id="cluster-schedule">
                    loading...
                </div>
            </div>

        </div>
    </div>

</div>

<g:javascript>

    /**
    * refreshes the page content for us
    */
    function refreshPage(){
        <g:remoteFunction controller="binBaseCluster" action="ajaxRenderAvailableNodes" update="availableNodes"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderBrokenNodes" update="brokenNodes"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderRunningNodes" update="runningNodes"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderConfiguredNodes" update="configuredNodes"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderAutostart" update="configuredAutostart"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderAutostartWarning" update="autostartDisabled"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderBinBaseNodes" update="usableNodesForBinBase"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderExportQueue" update="cluster-export"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderImportQueue" update="cluster-import"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderScheduleQueue" update="cluster-schedule"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderDSLScheduleQueue" update="cluster-dsl-schedule"/>

    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderClusterQueue" update="cluster-queue"/>
    <g:remoteFunction controller="binBaseCluster" action="ajaxRenderPendingClusterQueue" update="cluster-pending"/>

    }

    refreshPage();

    var myInterval;

    $('#refresh').on('click',function(){
        if ($(this).attr("checked") == "checked"){
            refreshPage();
            myInterval = setInterval(refreshPage,20000);
        }
        else {
            clearInterval(myInterval);
        }
    });

    //kill the interval once we leave the page
    $(document).on('pagebeforehide', '#index', function(){
        clearInterval(myInterval);
    });
</g:javascript>

</body>

</html>


