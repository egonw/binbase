<table>
    <thead>
    <tr>

        <g:sortableColumn property="id" title="Id"/>

        <g:sortableColumn property="description" title="Description"/>

        <th>Architecture</th>

        <th>Destination</th>

        <th>Download</th>





    </tr>
    </thead>
    <tbody>
    <g:each in="${studieInstanceList}" status="i" var="studieInstance">
        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

            <td><g:link controller="studie" action="show" id="${studieInstance.id}">${studieInstance.id?.encodeAsHTML()}</g:link></td>

            <td>${studieInstance.description?.encodeAsHTML()}</td>

            <td>${studieInstance.architecture?.encodeAsHTML()}</td>

            <td>${studieInstance.databaseColumn}</td>


            <g:set var="result" value="${studieInstance.mostRecentResult()}"/>
            <td>
                <g:if test="${result !=null}">
                    <g:link controller="BBDownload" class="downloadFile" action="download"
                            id="${result.id}">${result.fileName}</g:link>
                </g:if>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>

<g:if test="${disablePagenate == null}">
    <div class="paginateButtons">
        <g:paginate total="${studieCount}" params="[studieFilter:studieFilter]"/>
    </div>
</g:if>