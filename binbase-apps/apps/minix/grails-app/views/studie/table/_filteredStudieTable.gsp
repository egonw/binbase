<%
    assert studieCount != null
    assert studieInstanceList != null

%>
<div class="void">
    <div id="filteredStudieTable">
        <g:render template="table/filteredTable" model="[studieCount:studieCount,studieInstanceList:studieInstanceList,studieFilter:studieFilter]"/>
    </div>
</div>