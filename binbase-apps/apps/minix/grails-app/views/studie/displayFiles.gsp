<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: 1/20/12
  Time: 10:48 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showClass.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showStudie.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <title>Associated File Information</title>

    <export:resource/>

    <style type="text/css">

    .div-table {
        width: 100%;
        margin: 15px 15px 15px 15px;
        border-spacing: 2px;
    }

    .div-table .div-thead {
        font-style: normal;
    }

    .div-table .div-thead .div-th {
        width: 20%;
        float: left;
        border-bottom-style: solid;
        border-bottom-width: thin;

        background: #ecebeb;
        color: #403f3f;
        font-size: 12px;
        font-weight: lighter;
        line-height: 17px;
        padding-top: 2px;
        padding-bottom: 2px;
        border-bottom: #d5d4d4 thin solid;
        white-space: nowrap;

    }

    .div-table .div-tbody .div-row .div-td {
        width: 20%;
        float: left;
        margin: 0 0 0 0;
        padding: 5px 0px 5px 0px;
    }

    </style>
</head>

<body>

<div class="body">
    <g:render template="/shared/navigation"/>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="design">
        <div class="center-full-page">

            <div class="left-thrirty">
                <div class="header">Operations</div>

                <g:link class="list" controller="studie" action="show"
                        id="${studieInstance.id}">show study</g:link>

            </div>

            <div class="right-seventy left-side-border">
                <div class="header">
                    All datafiles of your study and associated information
                </div>

                <div class="element">
                    <g:each in="${studieInstance.classes}" var="clazz">

                        <div class="content-box">
                            <div class="content-header">
                                ${clazz.id}
                            </div>

                            <div class="classProperties">

                                <div class="div-table">
                                    <div class="div-thead">

                                        <div class="div-th">File Name</div>

                                        <div class="div-th">CDF Files</div>

                                        <div class="div-th">TXT Files</div>

                                        <div class="div-th">Calculated</div>

                                        <div class="div-th">RI Correction</div>

                                    </div>

                                    <div class="div-tbody">
                                        <g:each in="${clazz.samples}" var="sample">
                                            <div class="div-row">

                                                <div class="div-td">
                                                    ${sample}
                                                </div>

                                                <div class="div-td">
                                                    <label for="cdf_sample_${sample.id}"><g:checkBox disabled="true"
                                                                                                     name="cdf_sample_${sample.id}"
                                                                                                     id="cdf_sample_${sample.id}"/><span>cdf file found</span>
                                                    </label>
                                                </div>

                                                <div class="div-td">
                                                    <label for="txt_sample_${sample.id}"><g:checkBox disabled="true"
                                                                                                     name="txt_sample_${sample.id}"
                                                                                                     id="txt_sample_${sample.id}"/><span>txt file found</span>
                                                    </label>
                                                </div>


                                                <div class="div-td">
                                                    <label for="calculated_${sample.id}">
                                                        <g:checkBox
                                                                disabled="true"
                                                                name="calculated_${sample.id}"
                                                                id="calculated_${sample.id}"/><span>calculated</span>
                                                    </label>
                                                </div>

                                                <div class="div-td">
                                                    <label for="correction_failed_${sample.id}">
                                                        <g:checkBox
                                                                disabled="true"
                                                                name="correction_failed_${sample.id}"
                                                                id="correction_failed_${sample.id}"/><span>correction successful</span>
                                                    </label>
                                                </div>



                                                <g:if test="${studieInstance.databaseColumn != null}">
                                                    <g:javascript>
             jQuery.ajax({
                type:'POST',
                async:false,
                dataType : 'text',

                url:"${g.createLink(controller: 'retentionIndex', action: 'ajaxCorrectionFailedForSample', id: sample.id, params: [db: sample.experimentClass.experiment.databaseColumn])}",
            success:function(text,textStatus){

                 var data = jQuery.parseJSON(text);
                 var select = '#correction_failed_${sample.id}';
                 $(select).attr('checked', data.failed == false);
            },

            error:function(XMLHttpRequest,textStatus,errorThrown){

            }});

           jQuery.ajax({
                type:'POST',
                async:false,
                dataType : 'text',

                url:"${g.createLink(controller: 'retentionIndex', action: 'ajaxSampleCalculated', id: sample.id, params: [db: sample.experimentClass.experiment.databaseColumn])}",
            success:function(text,textStatus){

                 var data = jQuery.parseJSON(text);
                 var select = '#calculated_${sample.id}';
                 $(select).attr('checked', data.calculated);
            },

            error:function(XMLHttpRequest,textStatus,errorThrown){

            }});



                                                    </g:javascript>
                                                </g:if>
                                            </div>
                                        </g:each>

                                        <g:javascript>


            $.ajax({
                method: 'get',
                url : "${g.createLink(controller: 'binbaseScheduling', action: 'ajaxHasCDFFileForClass', id: clazz.id)}",
                dataType : 'text',
                success: function (text) {
                    var data = jQuery.parseJSON(text);

                    //build the selectors
                    jQuery.each(data,function(k,v){

                            var cdfSelector = "#cdf_sample_"+k;
                            $(cdfSelector).attr('checked', v);
                    });


                },
                complete: function (){

                }
            });
            $.ajax({
                method: 'get',
                url : "${g.createLink(controller: 'binbaseScheduling', action: 'ajaxHasTextFileForClass', id: clazz.id)}",
                dataType : 'text',
                success: function (text) {
                    var data = jQuery.parseJSON(text);

                    //build the selectors
                    jQuery.each(data,function(k,v){

                            var txtSelector = "#txt_sample_"+k;
                            $(txtSelector).attr('checked', v);
                    });


                }
                                    });

                                        </g:javascript>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </g:each>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
