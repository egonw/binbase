<div id="sub_studie_result_table">


    <%
        String sessionId = System.currentTimeMillis()
    %>
    <g:jprogressDialog progressId="${sessionId}" message="please wait for the deletion to finish" trigger="delete_sub_studie"/>

    <table>
        <thead>
        <tr>
            <th>id</th>
            <th>title</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${studieInstance?.subStudies}" var="study">
            <tr>
                <td><g:link controller="studie" action="show" id="${study.id}" target="_blank">${study.id}</g:link></td>
                <td>${study.title}</td>
                <td>
                    <g:if test="${study.canDelete()}">
                        <g:remoteLink controller="studie" action="ajaxDeleteSubStudie" id="${study.id}"
                                      update="sub_studie_result_table" class="remove" elementId="delete_sub_studie" params="[sessionId:sessionId]">delete</g:remoteLink>
                    </g:if>
                </td>

            </tr>
        </g:each>
        </tbody>
    </table>

</div>