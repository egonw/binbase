<%@ page import="minix.SubStudie; minix.MergedStudie; minix.ShiroUser; minix.TreatmentSpecific; minix.Studie" %>

<html>
<head>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showClass.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showStudie.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>


    <!-- file uploader-->
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'fileuploader.css')}"/>
    <g:javascript src="fileuploader.js"/>

    <!-- ck editor parts -->
    <ckeditor:resources/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'studie.label', default: 'Studie')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>

    <export:resource/>
</head>

<body>
<g:render template="/shared/navigation"/>


<g:set var="locked" value="${!studieInstance.canModify()}"/>

<div class="body">

<div class="design">
<div class="center-full-page">
<g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
</g:if>

<div class="studie-left">

<g:render template="studyInformations" model="[studieInstance: studieInstance, locked: true]"/>

<!-- management informations -->
<g:if test="${ShiroUser.hasManagmentRights()}">

    <div class="header">Further Information</div>


    <div class="element">
        <div class="scaffoldingProperties">

            <span class="scaffoldingPropertyLabel">possible to remove classes</span>
            <span class="scaffoldingPropertyValue"><g:if test="${studieInstance.canModify()}"><span
                    class="yes">yes</span></g:if><g:else><span class="no">no</span></g:else></span>
            <span class="scaffoldingPropertyLabel">possible to add samples</span>
            <span class="scaffoldingPropertyValue"><g:if test="${studieInstance.canModify()}"><span
                    class="yes">yes</span></g:if><g:else><span class="no">no</span></g:else></span>
            <span class="scaffoldingPropertyLabel">possible to remove samples</span>
            <span class="scaffoldingPropertyValue"><g:if test="${studieInstance.canModify()}"><span
                    class="yes">yes</span></g:if><g:else><span class="no">no</span></g:else></span>

            <span class="scaffoldingPropertyLabel">possible to rename files</span>
            <span class="scaffoldingPropertyValue"><g:if test="${studieInstance.canModify()}"><span
                    class="yes">yes</span></g:if><g:else><span class="no">no</span></g:else></span>


            <span class="scaffoldingPropertyLabel">has been exported</span>
            <span class="scaffoldingPropertyValue"><g:if
                    test="${studieInstance.hasBeenExported()}"><span
                        class="yes">yes</span></g:if><g:else><span class="no">no</span></g:else></span>

            <span class="scaffoldingPropertyLabel">is combined study</span>
            <span class="scaffoldingPropertyValue"><g:if
                    test="${studieInstance instanceof minix.MergedStudie}"><span
                        class="yes">yes</span></g:if><g:else><span class="no">no</span></g:else></span>


            <span class="scaffoldingPropertyLabel">is sub-study</span>
            <span class="scaffoldingPropertyValue"><g:if
                    test="${studieInstance instanceof minix.SubStudie}"><span
                        class="yes">yes</span></g:if><g:else><span class="no">no</span></g:else></span>



            <span class="scaffoldingPropertyLabel">last used column</span>
            <span class="scaffoldingPropertyValue"><g:if
                    test="${studieInstance.databaseColumn != null}"><span>${studieInstance.databaseColumn}</span></g:if><g:else><span>unknown</span></g:else>
            </span>

            <g:if test="${ShiroUser.hasAdminRights()}">
                <span class="scaffoldingPropertyLabel">public</span>
                <span class="scaffoldingPropertyValue"><g:checkBox name="studieIsPublic"
                                                                   value="${studieInstance.publicExperiment}"/></span>
            </g:if>
            <g:else>
                <span class="scaffoldingPropertyLabel">public</span>
                <span class="scaffoldingPropertyValue">${studieInstance.publicExperiment}</span>
            </g:else>

        </div>
    </div>

    <g:javascript>
    $('#studieIsPublic').change(function() {

        //update the serverside
        jQuery.ajax({type:'POST',data:{'value': this.checked}, url:"${g.createLink(controller: 'studie', action: 'ajaxStudieSetPublic', id: studieInstance.id)}",success:function(data,textStatus){}});return false;
    });

    </g:javascript>

</g:if>

<div class="header">Operations</div>


<div class="element">

    <ul class="none-vertical">
        <li><g:link class="schedule" controller="scheduleStudy"
                    action="schedule"
                    id="${studieInstance.id}">schedule studie</g:link>
        </li>

        <!--          not longer needed since we got the new and better scheduler now
        <li><g:link class="schedule" controller="binbaseScheduling"
                    action="schedule"
                    id="${studieInstance.id}">schedule studie - old scheduler!</g:link>
        </li>

-->

        <li>
            <g:link class="exportDataXls" controller="exportStudie" action="exportStudieToXLS"
                    id="${studieInstance.id}">export study design</g:link>
        </li>

        <li>

            <g:link controller="studie" action="displayClasses" class="displayClasses"
                    id="${studieInstance.id}">display classes</g:link>
        </li>


        <li>
            <g:link controller="studie" action="displayFiles" class="displayClasses"
                    id="${studieInstance.id}">display file matrix</g:link>

        </li>


        <li>

            <g:link controller="studie" action="displayResults" class="displayResults"
                    id="${studieInstance.id}">display all results</g:link>
        </li>

        <li>
            <g:link class="exportDataXml" controller="exportStudie" action="exportStudieToXML"
                    id="${studieInstance.id}">export study as xml</g:link>
        </li>

        <g:if test="${ShiroUser.hasAdminRights()}">

            <g:if test="${studieInstance.databaseColumn != null}">

                <li>
                    <g:link class="exportDataDsl" controller="exportStudie" action="exportStudieToDSL"
                            id="${studieInstance.id}">export study as dsl</g:link>
                </li>

            </g:if>
        </g:if>

        <li><g:link class="add" controller="subStudie" action="create"
                    id="${studieInstance.id}">create sub study</g:link></li>

        <g:if test="${ShiroUser.hasManagmentRights()}">

            <!-- merged studies can't be deleted at this point in time -->
            <g:if test="${(studieInstance instanceof MergedStudie) == false}">

                <!-- only allowed to delete studies, if they are modifiable -->
                <g:if test="${studieInstance.canDelete()}">

                    <li><g:remoteLink update="deleteDialog" class="remove" controller="studie"
                                      action="showDeleteDialog"
                                      id="${studieInstance.id}">delete study</g:remoteLink></li>

                </g:if>
            </g:if>
        </g:if>
    <!-- merged studies are not allowed to have acquisition tables or be locked -->
        <g:if test="${(studieInstance instanceof MergedStudie || studieInstance instanceof SubStudie) == false}">
            <g:if test="${studieInstance.aquisitionTable == null || studieInstance.canModify()}">

                <li>
                    <g:link class="list" controller="aquisitionTable" action="create"
                            id="${studieInstance.id}">create acquisition table</g:link>
                </li>
            </g:if>

            <g:if test="${ShiroUser.hasManagmentRights()}">

                <!-- lock/unlock options -->

                <g:if test="${studieInstance.results == null || studieInstance.results.size() == 0}">
                    <g:if test="${studieInstance.canModify()}">
                        <li><g:link class="lock" controller="studie" action="lockStudie"
                                    id="${studieInstance.id}">lock study</g:link></li>
                    </g:if>
                    <g:else>
                        <li><g:link class="unlock" controller="studie" action="unlockStudie"
                                    id="${studieInstance.id}">unlock study</g:link></li>
                    </g:else>
                </g:if>
            </g:if>
        </g:if>

    </ul>
</div>

<g:if test="${ShiroUser.hasManagmentRights()}">

    <g:if test="${studieInstance.databaseColumn != null}">

        <div class="header">Quality Control</div>


        <div class="element">

            <ul class="none-vertical">

                <li>
                    <g:link class="copy" controller="retentionIndex"
                            action="showRetentionIndexMarkerPerformanceByStudy"
                            id="${studieInstance.id}">display retention index marker performance - qualifier ratio</g:link>
                </li>
                <li>
                    <g:link class="copy" controller="retentionIndex"
                            action="showRetentionIndexMarkerPerformanceByStudyApexSn"
                            id="${studieInstance.id}">display retention index marker performance - apex sn</g:link>
                </li>

                <li>
                    <g:link class="copy" controller="retentionIndex"
                            action="showRetentionIndexMarkerPerformanceByStudyIntensity"
                            id="${studieInstance.id}">display retention index marker performance - intensity</g:link>
                </li>


                <li>
                    <g:link class="copy" controller="qualityControl"
                            action="binPerformanceByStudy"
                            id="${studieInstance.id}">display bin performance - for this study</g:link>
                </li>

            </ul>
        </div>
    </g:if>
</g:if>

<!-- only show this part if the design actually exist -->
<div class="designExist">
    <div class="header">Design</div>

    <div class="element">

        <ul class="none-vertical">
            <li>
                <g:link class="list" controller="studieDesign" action="showDesignForStudy"
                        id="${studieInstance.id}">display study design</g:link>
            </li>

            <li>
                <g:link class="copy" controller="studieDesign" action="duplicateDesignByStudy"
                        id="${studieInstance.id}">generate design</g:link>
            </li>

        </ul>
    </div>
</div>
<g:javascript>
                     $("designExist").hide();


 $.ajax({
                    url : "${g.createLink(controller: 'studie', action: 'ajaxDesignExist')}",
                    dataType : 'text',
                    data: {id:"${studieInstance.id}"},
                    success: function (text) {
                            if(text == 'true'){
                               $("div[class='designExist']").show();
                            }
                            else{
                               $("div[class='designExist']").hide();
                            }
                         }
                     });

</g:javascript>


<div id="deleteDialog"></div>

</div>

<!-- render study content -->
<div class="studie-arccodion-right">
    <g:render template="showAccordion" model="[studieInstance: studieInstance, locked: true]"/>
</div>
</div>
</div>
</div>
</body>
</html>
