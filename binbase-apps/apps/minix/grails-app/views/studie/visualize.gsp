<%@ page import="minix.TreatmentSpecific; minix.Studie" %>

<html>
<head>

  <link rel="stylesheet" href="${resource(dir: 'css', file: 'showClass.css')}"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'showStudie.css')}"/>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'studie.label', default: 'Studie')}"/>
  <title>Visualize Study</title>

</head>
<body>
<g:render template="/shared/navigation"/>


<g:set var="locked" value="${studieInstance.results.size() > 0}"/>

<div class="body">

  <div class="design">
    <div class="center-full-page">
      <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
      </g:if>


      <div class="element">

        <g:javascript src="dracula/raphael-min.js"/>
        <g:javascript src="dracula/dracula_graffle.js"/>
        <g:javascript src="dracula/dracula_tree.js"/>

        <!-- latest fork -->
        <!--
        <script type="text/javascript" src="https://github.com/grigoryk/dracula-js-fork/raw/master/dracula_graph.js"></script>
        <g:javascript src="dracula/dracula_graph.js"/>
        -->
        <script type="text/javascript">
          $(document).ready(function() {

            var height = 600;
            var width = 700;

            /* only do all this when document has finished loading (needed for RaphaelJS) */
            window.onload = function() {

              var g = new Graph();

              /**
               * the actual rendering of the node, we want squares
               * @param r
               * @param n
               */

              /**
               * define the nodes
               */

              //studie node
              g.addNode("${studieInstance.id}", { label : "${studieInstance.description}",weight:1  });

              <g:each in="${studieInstance.classes}" var="clazz">

              //define class node
              g.addNode("${clazz.id}", { label : "Class:${clazz.id}",weight:8 });
              //define species node
              g.addNode("${clazz.species.id}", { label : "${clazz.species.name}",weight:2 });
              //define organ node
              g.addNode("${clazz.organ.id}", { label : "${clazz.organ.name}",weight:3 });

              //connect species with studie
              g.addEdge("${clazz.species.id}", "${studieInstance.id}", { directed : true });
              //connect organ with species
              g.addEdge("${clazz.organ.id}", "${clazz.species.id}", { directed : true });

              //define the treatment


              <%
                def treamentSpecific = clazz.treatmentSpecific

                if(treamentSpecific.parent == null){
                  %>
              //this connects to the class and organ since there was no parent
              g.addNode("${treamentSpecific.id}", { label : "${treamentSpecific.treatment.description}: ${treamentSpecific.value}",weight:5 });

              //required edges
              g.addEdge("${treamentSpecific.id}", "${clazz.organ.id}", { directed : true });
              g.addEdge("${clazz.id}", "${treamentSpecific.id}", { directed : true });

              <%
                }
                else{

                  //this connects to the class
                  def lowest = treamentSpecific

                  //this connects to the organ
                  def highest = treamentSpecific

                  //the last assigned child
                  def child = null

                //find top parent
                while(highest.parent != null){

                  %>

              //current --> parent
              g.addNode("${highest.id}", { label : '${highest.treatment.description}: ${highest.value}',weight:5 });
              <%


              //required to render the connection after the node was created, otherwise we run into issues

                  if(child != null){
                    %>

              g.addEdge("${child.id}", "${child.parent.id}", { directed : true });

              <%
                  }
                  child = highest
                  highest = highest.parent

                }

                 %>

              <g:if test="${highest.id == lowest.id}">
              //highest and lowest have the same id
              g.addNode("${highest.id}", { label : "${highest.treatment.description}: ${highest.value}",weight:4 });
              g.addEdge("${highest.id}", "${clazz.organ.id}", { directed : true });
              g.addEdge("${clazz.id}", "${highest.id}", { directed : true });
              </g:if>
              <g:else>

              //render organ connection from highest point
              g.addNode("${highest.id}", { label : "${highest.treatment.description}: ${highest.value}",weight:4 });
              g.addEdge("${highest.id}", "${clazz.organ.id}", { directed : true });

              //render class connection from lowest point
              g.addEdge("${clazz.id}", "${lowest.id}", { directed : true });

              //required to render the connection after the node was created, otherwise we run into issues
              g.addEdge("${lowest.id}", "${lowest.parent.id}", { directed : true });

              </g:else>


              <%
                }
              %>

              </g:each>



              /* layout the graph using the Tree layout implementation */
              var layouter = new Graph.Layout.Tree4(g);
              layouter.layout();

              /* draw the graph using the RaphaelJS draw implementation */
              var renderer = new Graph.Renderer.Raphael('canvas', g, width, height);
              renderer.draw();

              redraw = function() {
                layouter.layout();
                renderer.draw();
              };
            };

          });
        </script>
        <div id="canvas">

        </div>

      </div>
    </div>
  </div>
</div>
</body>
</html>
