<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: 1/20/12
  Time: 10:48 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showClass.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showStudie.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'studie.label', default: 'Studie')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>

    <export:resource/>
</head>

<body>

<div class="body">
    <g:render template="/shared/navigation"/>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="design">
        <div class="center-full-page">

            <div class="left-thrirty">
                <div class="header">Operations</div>

                <g:link class="list" controller="studie" action="show"
                        id="${studieInstance.id}">show study</g:link>

            </div>

            <div class="right-seventy left-side-border">
                <div class="header">
                    Results of your study
                </div>

                <div class="element">
                    <binbase:tableResultsForExperiment experimentId="${studieInstance.id}" count="1000"/>
                </div>

            </div>
        </div>
    </div>
</body>
</html>