<g:if test="${studieInstance.classes.size() > 0}">
    <g:if test="${studieInstance.canModify() == false}">
        <div class="">

            <label class="version_select">set file version for all samples _</label>
            <g:select class="version_select" from="${1..9}"
                      name="studie_version_${studieInstance.id}"
                      noSelection="['': '?']"/>


            <g:jprogressDialog interval="500"
                               message="please wait while we update the samples in this studie"
                               progressId="studie_generate_file_${studieInstance.id}"
                               trigger="studie_version_${studieInstance.id}"/>

            <!-- sets the fileversion dynamicaly -->
            <g:javascript>
             $(function() {
             $("#studie_version_${studieInstance.id}").change(function( ) {

                     //open a progress dialog first

                        if(this.value == '?'){

                        }
                        else{
                            //execute the function
                            jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'studie', action: 'ajaxSetFileVersion', id: studieInstance.id)}",success:function(data,textStatus){jQuery('#render_classes_${studieInstance.id}').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                        }
                 });
               });

            </g:javascript>

        </div>

    </g:if>

    <div id="render_classes_${studieInstance.id}">
        <g:render template="renderClasses" model="[studieInstance: studieInstance]"/>
    </div>
</g:if>
<g:else>
    <div class="errors">
        there are no classes defined for this studie
    </div>
</g:else>
