<g:setProvider library="jquery"/>

<g:javascript>
  /**
  * initialize the data view tabs
  */
  $(document).ready(function() {
    $("#dataView").tabs();
  });


  /**
  * initialize the classes view tabs
  */
  $(document).ready(function() {
    $("#classesView").tabs();
  });

</g:javascript>

 <div class="studie-right">

        <div id="dataView">
          <ul>
            <li><a href="#resultsTab"><span>Results</span></a></li>
            <li><a href="#attachmentsTab"><span>Attachments</span></a></li>
            <li><a href="#usersTab"><span>Users</span></a></li>
          </ul>


          <!-- results -->
          <div id="resultsTab">
            <g:if test="${studieInstance.results.size() == 0}">
              sorry at this time there are no results available.
            </g:if>
            <g:else>
              <table>
                <thead>
                <th>upload date</th>
                <th>download</th>
                <th>remove</th>

                </thead>
                <tbody>
                <g:each in="${studieInstance.results}" var="r">

                  <tr>
                    <td>${r?.uploadDate.encodeAsHTML()}</td>
                    <td></td>
                    <td></td>
                  </tr>
                </g:each>
                </tbody>
              </table>
            </g:else>
          </div>


          <!-- attachments -->
          <div id="attachmentsTab">
            <g:render template="renderAttachment" model="[studieInstance:studieInstance]"/>
          </div>

          <div id="usersTab">
            <table>
              <thead>
              <th>user name</th>
              <th>email</th>
              </thead>
              <tbody>
              <g:each in="${studieInstance.users}" var="u">

                <tr>
                  <td><g:link controller="shiroUser" action="show" id="${u.id}">${u?.username.encodeAsHTML()}</g:link></td>
                  <td>${u?.email.encodeAsHTML()}</td>

                </tr>
              </g:each>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="studie-bottom">
        <div class="header">Associated Classes</div>

      <!-- if more than 15 classes it should be renderer as accordion -->

        <g:if test="${studieInstance.classes.size() > 0}">
          <div id="classesView">
            <ul>
              <g:each in="${studieInstance.classes}" var="c" status="i">
                <li><a href="#class_${c.id}"><span>${c.id}</span></a></li>
              </g:each>
            </ul>

            <g:each in="${studieInstance.classes}" var="c">
              <div id="class_${c.id}">
                <g:render template="/experimentClass/displayClass" model="[clazz:c,locked:locked]"/>
              </div>
            </g:each>
          </div>
        </g:if>
        <g:else>
          <div class="errors">
            there are no classes defiend for this studie, how can this be?
          </div>
        </g:else>
      </div>