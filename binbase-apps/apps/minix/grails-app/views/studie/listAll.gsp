<%@ page import="minix.Studie" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>Studie List</title>
</head>

<body>
<g:render template="/shared/navigation"/>
<g:javascript src="jquery/jquery.observe_field.js"/>

<div class="body">

    <div class="design">
        <div class="left">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.studie"/></p>

                <div class="topSpacer"></div>

                <p><g:message code="description.studie.all"/></p>

            </div>

        </div>

        <div class="left-center-column">
            <div class="header">Studie List</div>
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>

            </g:if>
            <div class="element">
                <ul class="none-horizontal">
                    <!-- here we are registering our gctofs systems -->
                    <li><g:link class="new_studie" controller="studieDesign" action="create"><g:message
                            code="new.studie.design"/></g:link></li>
                </ul>
            </div>


            <div class="element">

                <g:form action="listAll">
                    <div class="void">
                        <g:textField id="studieFilterAll" class="filter" name="studieFilter" value="${studieFilter}"/>

                        <g:javascript>
                            //update the result table with the filtered result
                            $(function() {
                                $("#studieFilterAll").observe_field(0.5, function() {
                                    jQuery.ajax({type:'POST',data:{'studieFilter': this.value,'ajaxCall':true}, url:"${g.createLink(controller: 'studie', action: 'listAll')}",beforeSend:function(){
                                                                            jQuery('#filteredStudieTable').html("<div class='message'>please wait while we are filtering the data</div>");
                                    },success:function(data, textStatus) {
                                        jQuery('#filteredStudieTable').html(data);
                                    }

                                    });
                                    return false;

                                });
                            });
                        </g:javascript>

                    </div>
                </g:form>

                <g:render template="table/filteredStudieTable"
                          model="[studieCount: studieCount, studieInstanceList: studieInstanceList]"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>

