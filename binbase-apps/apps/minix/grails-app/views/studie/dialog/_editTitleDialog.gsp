<g:setProvider library="jquery"/>

<%
    def id = System.currentTimeMillis()
%>
<g:javascript>
    $(document).ready(function() {
        // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
        $("#studie-edit-modal-${studie.id}-${id}").dialog("destroy");

        $("#studie-edit-modal-${studie.id}-${id}").dialog({
            height: 300,
            width: 500,
            modal: true,
              closeOnEscape: false,
   open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
        });
    });
</g:javascript>

<g:javascript>

/**
* closes the dialog
*/
function closeDialog(){

  $(document).ready(function() {
    //close the dialog
    $("#studie-edit-modal-${studie.id}-${id}").dialog('close');

    $("#studie-edit-modal-${studie.id}-${id}").dialog('destroy');
  });
}
</g:javascript>

<div id="studie-edit-modal-${studie.id}-${id}" title="update description">

<g:form controller="studie" action="ajaxSaveStudieTitle">

    <div class="description">please enter your new description below</div>

    <div class="button-margin-top">
    <g:textArea class="textarea-full" cols="120" rows="2" name="title" value="${studie.description}"/>
    </div>

   <div class="button-margin-top">
    <g:submitToRemote before="closeDialog();" controller="studie" action="ajaxSaveStudieTitle"
                      name="submit" value="save" class="save" update="studieTitle"
                      after="closeDialog();"/>
    </div>

    <g:hiddenField name="id" value="${studie.id}"/>
</g:form>
</div>