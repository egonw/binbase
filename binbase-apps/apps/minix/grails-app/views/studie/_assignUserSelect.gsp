<g:setProvider library="jquery"/>

<g:form name="assignUserForm">
    <div class="description">
        please select the user you like to assign to this experiment from the bottom list
    </div>

    <div class="description">
        <g:select name="user" from="${users}" optionKey="id"/>

    </div>


    <div class="button-margin-top">
        <g:hiddenField name="id" value="${studie.id}"/>
        <g:hiddenField name="assign" value="true"/>

        <g:submitToRemote class="next" controller="studie" update="[success:'userContent',failure:'userContent']"
                          action="assignUserAjax" value="save"/>

        <div class="button-margin-left">
            <g:submitToRemote class="next" controller="studie" update="[success:'userContent',failure:'userContent']"
                              action="cancelAssignUserAjax" value="cancel"/>
        </div>

    </div>

</g:form>