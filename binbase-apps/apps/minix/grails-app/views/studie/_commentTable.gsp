<%@ page import="minix.Studie" %><g:setProvider library="jquery"/>

<g:setProvider library="jquery"/>

<%
    assert studieInstance != null, "you need to provide a studie instance"
    assert studieInstance instanceof Studie, "the variable needs to be a studie!"
%>

<table class="contentTable button-margin-top">
    <thead>
    <th>date of last update</th>
    <th>comment</th>
    <th></th>

    </thead>
    <tbody>
    <g:each in="${studieInstance.comments}" var="r">
        <g:if test="${r != null}">
            <g:if test="${r.text != null && r.text.trim().length() > 0 && r.text.trim() != 'null'}">

                <tr>
                    <td>${r.lastUpdated}</td>

                    <g:if test="${r.text?.length() > 50}">
                        <td>
                            ${r.text.substring(0, 50)}...
                        </td>
                        <td><a class="showDetails" onclick='$("#dialog-comment-modal-${r.id}").dialog("open")'
                               target="_self">display</a>
                        </td>


                        <%
                            def pageId = System.currentTimeMillis()
                        %>

                        <!-- dialog to show coment content -->
                        <div id="dialog-comment-modal-${r.id}" title="Comment Dialog">

                            ${r.text}
                        </div>

                        <!-- no document ready since its messes up arcordion otherwise -->
                        <g:javascript>

    // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
    $("#dialog-comment-modal-${r.id}").dialog("destroy");

    $("#dialog-comment-modal-${r.id}").dialog({
      height: 480,
      width: 768,
      modal: true,
     autoOpen:false    ,
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
				}
			}
    });

                        </g:javascript>
                    </g:if>
                    <g:else>
                        <td>
                            ${r.text}
                        </td>
                        <td></td>
                    </g:else>

                </tr>
            </g:if>
        </g:if>
    </g:each>

    </tbody>
</table>


