<%@ page import="minix.ShiroUser" %><g:setProvider library="jquery"/>

<g:javascript>

    function removeUserRow(reference) {
        $(document).ready(function() {
            $('#' + reference).remove();
        });
    }
</g:javascript>

<div id="userDeleteMessage">

</div>
<table class="contentTable">
    <thead>
    <tr>
        <th>user name</th>
        <th>email</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${studieInstance.users}" var="u">

        <%
            def rowId = "user_row_${u.id}"
        %>
        <tr id="${rowId}">
            <td><g:link class="showDetails" controller="shiroUser" action="show"
                        id="${u.id}">${u?.username?.encodeAsHTML()}</g:link></td>
            <td>${u?.email?.encodeAsHTML()}</td>
            <td>
                <g:if test="${ShiroUser.hasManagmentRights()}">

                    <g:remoteLink params="[id:u.id,studie:studieInstance.id]" after="removeUserRow('${rowId}');"
                                  update="userDeleteMessage" class="remove" controller="studie"
                                  action="revokeUserAjax">revoke</g:remoteLink>

                </g:if>
            </td>

        </tr>
    </g:each>
    </tbody>
</table>

<div class="void">
    <g:form name="assignUserForm" controller="studie">
        <g:hiddenField name="id" value="${studieInstance.id}"/>

        <div class="button-margin-top">
            <g:submitToRemote class="next" controller="studie" update="[success:'userContent',failure:'userContent']"
                              action="assignUserAjax" value="assign user"/>
        </div>

    </g:form>

</div>