<%@ page import="minix.ShiroUser; minix.TreatmentSpecific; minix.Studie" %>

<html>
<head>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showClass.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showStudie.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'studie.label', default: 'Studie')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>

    <export:resource/>
</head>

<body>
<g:render template="/shared/navigation"/>


<g:set var="locked" value="${studieInstance.results.size() > 0}"/>

<div class="body">

    <div class="design">
        <div class="center-full-page">
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>

            <div class="studie-left">

                <div class="header">Information</div>


                <div class="element">
                    <div class="scaffoldingProperties">

                        <span class="scaffoldingPropertyLabel"><g:message code="studie.id.label" default="Id"/></span>
                        <span class="scaffoldingPropertyValue">${fieldValue(bean: studieInstance, field: "id")}<g:if
                                test="${studieInstance.externalIds != null && studieInstance.externalIds.size() > 0}">- ${studieInstance.externalIds}</g:if></span>

                        <span class="scaffoldingPropertyLabel"><g:message code="studie.description.label"
                                                                          default="Description"/></span>
                        <span class="scaffoldingPropertyValue">${fieldValue(bean: studieInstance, field: "description")}</span>

                        <span class="scaffoldingPropertyLabel"><g:message code="studie.architecture.label"
                                                                          default="Architecture"/></span>
                        <span class="scaffoldingPropertyValue"><g:link controller="architecture" action="show"
                                                                       id="${studieInstance?.architecture?.id}">${studieInstance?.architecture?.encodeAsHTML()}</g:link></span>

                        <span class="scaffoldingPropertyLabel"><g:message code="studie.architecture.countOfClasses"
                                                                          default="Count of classes"/></span>
                        <span class="scaffoldingPropertyValue">${studieInstance.classes.size()}</span>

                    </div>
                </div>

                <g:if test="${ShiroUser.hasManagmentRights()}">

                    <div class="header">Further Information</div>


                    <div class="element">
                        <div class="scaffoldingProperties">

                            <span class="scaffoldingPropertyLabel">has been exported</span>
                            <span class="scaffoldingPropertyValue"><g:if
                                    test="${studieInstance.hasBeenExported()}"><span
                                        class="yes">yes</span></g:if><g:else><span class="no">no</span></g:else></span>

                            <span class="scaffoldingPropertyLabel">last used column</span>
                            <span class="scaffoldingPropertyValue"><g:if
                                    test="${studieInstance.databaseColumn != null}"><span>${studieInstance.databaseColumn}</span></g:if><g:else><span>unknown</span></g:else>
                            </span>

                        </div>
                    </div>

                </g:if>

                <div class="header">Operations</div>

                <div class="element">

                    <ul class="none-vertical">
                        <li><g:link class="schedule" controller="${studieInstance.architecture.relatedController}"
                                    action="${studieInstance.architecture.relatedAction}"
                                    id="${studieInstance.id}">schedule studie</g:link></li>

                        <li>
                            <g:link class="exportData" controller="exportStudie" action="exportStudieToXLS"
                                    id="${studieInstance.id}">export studie design</g:link>
                        </li>

                        <li>

                            <g:link controller="studie" action="displayClasses" class="displayClasses"
                                    id="${studieInstance.id}">display classes</g:link>
                        </li>

                        <g:if test="${ShiroUser.hasManagmentRights()}">

                            <li><g:remoteLink update="deleteDialog" class="remove" controller="studie"
                                              action="showDeleteDialog"
                                              id="${studieInstance.id}">delete study</g:remoteLink></li>

                            <g:if test="${studieInstance.canModify()}">
                                <li><g:link class="lock" controller="studie" action="lockStudie"
                                            id="${studieInstance.id}">lock study</g:link></li>
                            </g:if>
                        </g:if>

                    </ul>

                    <div id="deleteDialog"></div>
                </div>

            </div>

            <div class="studie-arccodion-right">

                <g:render template="showAccordion" model="[studieInstance:studieInstance,locked:true]"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>
