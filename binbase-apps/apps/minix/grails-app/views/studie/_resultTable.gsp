<%@ page import="minix.ShiroUser" %>
<!-- renders results -->




<div class="Header">these are the last 10 result's for your study</div>

<div id="resultContent">

    <binbase:tableResultsForExperiment experimentId="${studieInstance.id}" count="10"/>

    <g:if test="${ShiroUser.hasManagmentRights()}">

        <!-- uploads the attachment data to the server -->
        <g:javascript>

  jQuery(document).ready(function() {

    var resultUploader = new qq.FileUploader({
      // pass the dom node (ex. $(selector)[0] for jQuery users)
      element: document.getElementById('result-uploader'),
      // path to server-side upload script
      action: "${g.createLink(controller: 'studie',action: 'uploadResult')}",
      onComplete: function(id, fileName, responseJSON){
        jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'studie',action: 'ajaxRenderResultTable',id: studieInstance.id)}",success:function(data,textStatus){jQuery('#resultContent').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
      }
    });

            resultUploader.setParams({
           studie: '${studieInstance.id}'
        });
  });

        </g:javascript>


        <div class="button-margin-top">
            <div id="result-uploader">
                <noscript>
                    <p>Please enable JavaScript to use file uploader.</p>
                    <!-- or put a simple form for upload here -->
                </noscript>
            </div>

        </div>

    </g:if>
</div>
