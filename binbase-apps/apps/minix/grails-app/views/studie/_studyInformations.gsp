<!-- general information about this study -->
<div class="header">Information</div>


<div class="element">
    <div class="scaffoldingProperties">

        <span class="scaffoldingPropertyLabel"><g:message code="studie.id.label" default="Id"/></span>
        <span class="scaffoldingPropertyValue"><g:formatNumber
                number="${studieInstance.id}"/>

            <g:if
                    test="${studieInstance.externalIds != null && studieInstance.externalIds.size() > 0}">- ${studieInstance.externalIds}</g:if></span>

        <span class="scaffoldingPropertyLabel"><g:message code="studie.description.label"
                                                          default="Description"/></span>
        <span class="scaffoldingPropertyValue">
            <span id="studieTitle"
                  class="button-float-right">${fieldValue(bean: studieInstance, field: "description")}</span><g:remoteLink
                class="edit" controller="studie" action="ajaxEditStudieTitle"
                update="studieTitle" id="${studieInstance.id}"><span
                    class="hideText">edit</span></g:remoteLink>

        </span>

        <span class="scaffoldingPropertyLabel"><g:message code="studie.architecture.label"
                                                          default="Architecture"/></span>
        <span class="scaffoldingPropertyValue"><g:link controller="architecture" action="show"
                                                       id="${studieInstance?.architecture?.id}">${studieInstance?.architecture?.encodeAsHTML()}</g:link></span>

        <span class="scaffoldingPropertyLabel"><g:message code="studie.architecture.countOfClasses"
                                                          default="Count of classes"/></span>
        <span class="scaffoldingPropertyValue">${studieInstance.classes.size()}</span>

        <g:if test="${studieInstance instanceof  minix.SubStudie}">
            <span class="scaffoldingPropertyLabel"><g:message code="studie.architecture.parent"
                                                              default="Parent study"/></span>
            <span class="scaffoldingPropertyValue"><g:link controller="studie" action="show" id="${studieInstance.origin?.id}" target="_blank">${studieInstance.origin?.id}</g:link></span>
        </g:if>
    </div>
</div>
