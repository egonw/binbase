<%@ page import="minix.Studie" %><g:setProvider library="jquery"/>

<%
    assert studieInstance != null, "you need to provide a studie instance"
    assert studieInstance instanceof Studie, "the variable needs to be a studie!"
%>


<div id="comments">
    <div>

        <div id="commentContent">
            <div class="void">
                <g:form name="openCommentDialog" controller="comment">
                    <g:hiddenField name="id" value="${studieInstance.id}"/>

                    <g:hiddenField name="destination" value="commentContent"/>

                    <g:hiddenField name="type" value="studie"/>

                    <div class="button-margin-bottom">
                        <g:submitToRemote class="next" controller="comment"
                                          update="showCommentDialog"
                                          action="createAjax" value="add comment"/>
                    </div>
                    <div id="showCommentDialog"></div>
                </g:form>

            </div>


            <g:if test="${studieInstance.comments.size() == 0}">
                sorry there are no comments available yet
            </g:if>
            <g:else>
                <g:render template="/studie/commentTable" model="[studieInstance:studieInstance]"/>
            </g:else>

        </div>

    </div>

    <div>

    </div>
</div>