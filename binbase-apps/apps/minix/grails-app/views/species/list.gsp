<%@ page import="minix.Species" %>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <title>Species List</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

  <div class="design">

    <div class="left">
      <div class="header">

        Information

      </div>

      <div class="left_information_box">
        <p><g:message code="description.species"/></p>
        <div class="topSpacer"></div>
                     <p><g:message code="description.species.list"/></p>

      </div>

    </div>
    <div class="left-center-column"><div class="header">Species List</div>
      <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
      </g:if>
      <div class="element">
        <table>
          <thead>
          <tr>

            <g:sortableColumn property="id" title="Id"/>

            <g:sortableColumn property="name" title="Name"/>

            <th>Kingdom</th>

          </tr>
          </thead>
          <tbody>
          <g:each in="${speciesInstanceList}" status="i" var="speciesInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

              <td><g:link action="show" id="${speciesInstance.id}">${speciesInstance.id?.encodeAsHTML()}</g:link></td>

              <td>${speciesInstance.name?.encodeAsHTML()}</td>

              <td>${speciesInstance.kingdom?.encodeAsHTML()}</td>

            </tr>
          </g:each>
          </tbody>
        </table>
        <div class="paginateButtons">
          <g:paginate total="${Species.count()}"/>
        </div>

      </div>
    </div>
  </div>

</div>
</body>
</html>

