<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'retentionindex.css?id=1')}"/>

    <title>Home</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<g:javascript library="flot/jquery.flot"/>
<g:javascript library="flot/jquery.flot.selection"/>
<g:javascript library="flot/jquery.flot.time"/>

<div class="body">
    <div class="design">

        <div class="center-full-page">

            <div class="element">

                <div class="header" id="id_top">
                    Bin Performance Report
                </div>

                <div class="description">This page allows you to monitor the performance of a specific bin for a certain study</div>
            </div>

            <div class="element">

                <div class="textDescription">

                    <g:form action="ajaxShowBinPerformanceByStudy" controller="retentionIndex">

                        <div class="element">
                            <ul class="retention-form">

                                <li>
                                    <label for="study">Study Id:</label>

                                    <input type="text" id="study" name="study" value="${studyId}"/>
                                </li>

                                <li>
                                    <label for="bin">Bin Id:</label>

                                    <input type="text" id="bin" name="bin"/>
                                </li>

                                <li>
                                    <label for="ion">Ion:</label>

                                    <input type="text" id="ion" name="ion"/>
                                </li>

                            </ul>
                        </div>

                        <div class="element">
                            <ul class="retention-form">
                                <li>
                                    <g:submitToRemote class="next" value="generate report" update="next"
                                                      action="ajaxShowBinPerformanceByStudy" controller="retentionIndex"/>
                                </li>
                            </ul>
                        </div>
                    </g:form>

                </div>

            </div>


            <div class="element">
                <div id="next">
                </div>

            </div>

        </div>
    </div>

</div>
</body>
</html>
