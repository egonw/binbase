<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: 1/20/12
  Time: 10:48 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'memberHome.css')}"/>

    <title>BinBase Quality Control Overview</title>

</head>

<body>

<div class="body">
    <g:render template="/shared/navigation"/>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="design">
        <div class="center-full-page">

            <div class="header">
                BinBase Quality Control Reports
            </div>

            <div>
                <ul class="actions">

                    <div class="moduleheader topSpacer">
                        Retention correction performance reports
                    </div>

                    <li class="audit_log">
                        <g:link controller="retentionIndex"
                                action="displayRecentlyFailedCorrections">Recently failed RT corrections</g:link>
                    </li>

                    <li class="audit_log">
                        <g:link controller="retentionIndex"
                                action="showRetentionIndexMarkerPerformance">Retention Index Marker Performance - Qualifier ratios</g:link>
                    </li>

                    <li class="audit_log">
                        <g:link controller="retentionIndex"
                                action="showRetentionIndexMarkerPerformanceApexSn">Retention Index Marker Performance - Signal Noise</g:link>
                    </li>

                    <li class="audit_log">
                        <g:link controller="retentionIndex"
                                action="showRetentionIndexMarkerPerformanceHeight">Retention Index Marker Performance - Intensity</g:link>
                    </li>

                    <div class="moduleheader topSpacer">
                        Bin Performance Reports
                    </div>
                    <li class="audit_log">
                        <g:link controller="retentionIndex"
                                action="showBinPerformance">Bin Performance - Intensity</g:link>
                    </li>

                    <!--
                    <li class="audit_log">
                        <g:link controller="retentionIndex" action="showBinRatio">Bin Ratios</g:link>
                    </li>

-->
                    <li class="audit_log">
                        <g:link controller="retentionIndex"
                                action="showBinPerformanceByDays">Bin Performance - Intensity - for last days</g:link>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>

</html>
