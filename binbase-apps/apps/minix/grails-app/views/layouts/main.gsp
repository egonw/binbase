<!DOCTYPE html>

<html>
<head>
    <title><g:layoutTitle default="MiniX"/></title>


    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="Expires" CONTENT="-1">

    <g:layoutHead/>
    <g:javascript library="application"/>
    <g:javascript library="jquery" plugin="jquery"/>

    <!-- jquery ui compoments -->
    <jqui:resources themeCss="${resource(dir:'jquery-ui/lightGray', file:'jquery-ui-1.8.16.custom.css')}"/>

    <!-- bluff resources, if we don't have these graphs are not rendered -->

    <!-- text field observables -->
    <g:javascript src="jquery/jquery.observe_field.js"/>

    <!-- render all button elements -->
    <g:javascript src="jquery/render.buttons.js"/>

    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon"/>
    <!-- stylesheets for this application -->
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'minix.css?1=1')}"/>

	<!-- snowstorm script -->
	<script src="${resource(dir: 'js/snow', file: 'snowfall.jquery.js')}"></script>

</head>

<body>
<div id="spinner" class="spinner" style="display:none;">
    <img src="${resource(dir: 'images', file: 'spinner.gif')}" alt="Spinner"/>
</div>

<div id="logo">
    <span class="begin collectonme">MiniX</span>
    <span class="end collectonme">Study Creation and Calculation Tool</span>
	<g:if test="${new Date().format('MM') == '12'}">
		<span class="end right collectonme" style="width: auto;">We wish you wonderful Holidays !!!</span>
	</g:if>
</div>
<g:layoutBody/>

</body>
</html>