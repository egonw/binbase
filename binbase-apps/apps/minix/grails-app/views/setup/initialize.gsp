<%@ page import="minix.roles.RoleUtil; minix.ShiroRole; minix.ShiroUser" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'shiroUser.label', default: 'ShiroUser')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <title>Initializing MiniX Default User</title>
</head>
<body>

<div class="body">
    <div class="design">

        <div class="center-column">
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>

            <div class="errors">
                welcome to your miniX installation, we are creating now the admin user, so that you can use and operate this system
            </div>
            <div class="element">
                <g:hasErrors bean="${shiroUserInstance}">
                    <div class="errors">
                        <g:renderErrors bean="${shiroUserInstance}" as="list"/>
                    </div>
                </g:hasErrors>

                <g:form action="save" method="post">
                    <div class="scaffoldingProperties">

                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="username">User name</label>

                            </span>
                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'username', 'errors')}">

                                <g:hiddenField name="username" value="admin"/>
                                admin
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="passwordHash">Password</label>

                            </span>

                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'passwordHash', 'errors')}">

                                <g:textField name="passwordHash" value="${shiroUserInstance?.passwordHash}"/>
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="firstName">First Name</label>

                            </span>
                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'firstName', 'errors')}">

                                <g:textField name="firstName" value="${shiroUserInstance?.firstName}"/>
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="lastName">Last Name</label>

                            </span>
                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'lastName', 'errors')}">

                                <g:textField name="lastName" value="${shiroUserInstance?.lastName}"/>
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="email">Email address</label>

                            </span>
                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'email', 'errors')}">

                                <g:textField name="email" value="${shiroUserInstance?.email}"/>
                            </span>
                        </div>

                    </div>
                    <div class="topSpacer"></div>
                    <div class="myButton">
                        <div class="buttons">
                            <g:submitButton name="create" class="next" value="Save User and login"/>
                        </div>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
