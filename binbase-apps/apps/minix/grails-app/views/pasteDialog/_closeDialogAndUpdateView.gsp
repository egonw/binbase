<g:setProvider library="jquery"/>

<div class="message">
    we updated your clazz with the pasted informations
</div>
<g:javascript>

  $(document).ready(function() {
    //update the class
    $("#dialog-paste-modal-${clazz.id}").dialog('close');
    $("#dialog-paste-modal-${clazz.id}").dialog("destroy");

    jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${createLink(controller: 'experimentClass',action: 'ajaxRenderClassTable',id:clazz.id)}",success:function(data,textStatus){jQuery('#class_${clazz.id}_content').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});

    return false;

  });

</g:javascript>