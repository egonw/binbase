<g:setProvider library="jquery"/>

<%@ page import="minix.step.DialogStep" %>

<%

/**
 * makes sure that we store the values used in the session so that we dont have to select the same fields over and over again
 */
    def columnValues = []

    //populate the values
    0..(maxValue - 1).each { def x ->
        switch(x){
            case 0:
                columnValues[x] = (session["dialog_column_value_${x}"] != null) ? session["dialog_column_value_${x}"] : DialogStep.LABEL
                break
            case 1:
                columnValues[x] = (session["dialog_column_value_${x}"] != null) ? session["dialog_column_value_${x}"] : DialogStep.COMMENT
                break
            case 2:
                columnValues[x] = (session["dialog_column_value_${x}"] != null) ? session["dialog_column_value_${x}"] : DialogStep.FILENAME
                break
            default:
                columnValues[x] = (session["dialog_column_value_${x}"] != null) ? session["dialog_column_value_${x}"] : DialogStep.IGNORE
                break
        }
    }

    %>

<g:form controller="pasteDialog">
    <div class="element">

        <div class="header">
            copy/paste matrix
        </div>

        <div class="description">
            please assign the correct columns to your data
        </div>
    </div>

    <div class="element">

        <g:hiddenField name="clazzId" value="${clazz}"/>

        <g:hiddenField name="maxValue" value="${maxValue}"/>

        <div id="dialogErrors_${pageId}">

        </div>
        <table class="dialogTable">
            <thead>
            <tr>
                <th></th>
                <g:each var="current" status="x" in="${0..(maxValue - 1)}">
                    <th>
                            <g:select id="column_${x}" name="column_${x}" from="${DialogStep.listEnums()}" value="${columnValues[x]}"/>

                        <g:javascript>
                            $("#column_${x}").change(function(){
                                $.ajax({type:'POST',data:{'name': "dialog_column_value_${x}", 'value':this.value}, url:"${createLink(controller: 'pasteDialog',action: 'ajaxStoreColumnSelectionIn')}",error:function(XMLHttpRequest,textStatus,errorThrown){}});
                            });


                        </g:javascript>
                    </th>
                </g:each>
            </tr>
            </thead>

            <tbody>
            <g:each in="${table.split('\n')}" var="line" status="x">

                <tr id="dialog_row_${x}">
                    <td>

                        <g:remoteLink before="jQuery('#dialog_row_' + ${x}).remove();" update="noTarget" type="button"
                                      value="remove" class="remove">remove</g:remoteLink>

                    </td>

                    <%
                        def values = line.split('\t')
                    %>
                    <g:each in="${values}" var="item" status="y">
                        <td>
                            <g:textField name="label_${y}" id="${x}_${y}" value="${item}"/>
                        </td>
                    </g:each>

                    <g:if test="${values.size() < maxValue}">
                        <g:each var="value" in="${values.size()..maxValue - 1}">
                            <td>
                                <g:textField disabled="true" name="label_${value}" id="${x}_${value}" value=""/>
                            </td>
                        </g:each>
                    </g:if>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>

    <div class="element">
        <div class="buttons">
            <g:submitToRemote update='[success:"page_${pageId}",failure:"dialogErrors_${pageId}"]' onFailure='' action="ajaxSaveData" value="save data" class="next"/>
        </div>
    </div>
</g:form>
