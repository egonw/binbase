<g:setProvider library="jquery"/>

<g:javascript>
  $(document).ready(function() {
    // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
    $("#dialog-paste-modal-${clazz.id}").dialog("destroy");

    $("#dialog-paste-modal-${clazz.id}").dialog({
      height: 480,
      width: 768,
      modal: true
    });
  });
</g:javascript>

<%
  def pageId = System.currentTimeMillis()
%>
<div id="dialog-paste-modal-${clazz.id}" title="Paste metadata dialog">

  <div id="page_${pageId}">
    <g:form controller="pasteDialog">

      <div class="element">
        <div class="description">please paste your text here. Columns are defined by tabs and each new line defines another sample</div>
      </div>
      <div class="element">
        <g:textArea class="textarea-full" name="paste" rows="10" cols="200" value="${paste}"/>
      </div>

      <div class="element">
        <div class="buttons">
          <g:submitToRemote action="ajaxProcessData" class="next" update="page_${pageId}" value="upload"/>
        </div>
      </div>

      <g:hiddenField name="pageId" value="${pageId}"/>
      <g:hiddenField name="clazzId" value="${clazz.id}"/>
    </g:form>
  </div>
</div>