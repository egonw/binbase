<g:setProvider library="jquery"/>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<%
    String sessionId = System.currentTimeMillis()
%>


<g:javascript>

    function importStart() {
        $(document).ready(function() {

            $("#messageBox").html("<div class='message'>please wait we are generating the study now...</div>");
        });
    }
</g:javascript>

<div class="design">
    <div class="left">

        <div class="header">sub-study creator</div>
        This tool is going to create now a sub study on your provided properties. It will be based on study: ${studieInstance.id}
    </div>

    <div class="left-center-column">
        <div id="view">
            <g:form name="designStudy">

                <div class="step-dialog">
                    <div class="header">
                        sub study properties
                    </div>

                    <div class="description">
                    </div>

                    <br>

                    <g:if test="${flash.message != null && flash.message.toString().length() > 0}">
                        <div class="fillField">

                            <div class="message">
                                ${flash.message}
                            </div>
                        </div>
                    </g:if>


                    <div class="textDescription">
                        please enter the title of your study into the textfield below
                    </div>


                    <div class="textField">
                        <div class="${hasErrors(bean: design, field: 'title', 'errors')}">
                            <g:textField name="title" value="${studieInstance?.title} sub_${new Date()}"/>
                        </div>
                    </div>

                    <div class="textDescription">
                        please select all the samples you like to include in this study. You can select all samples of a class by clicking on the main class checkbox, next to it's name
                    </div>

                    <div class="element">

                        <g:each in="${studieInstance.classes}" var="clazz">
                            <g:if test="${clazz instanceof minix.ExperimentClass}">
                                <g:render template="/experimentClass/displayClass"
                                          model="[clazz: clazz, locked: true, includeCheck: true]"/>
                            </g:if>
                        </g:each>
                    </div>

                    <g:hiddenField name="originalStudy" value="${studieInstance.id}"/>
                </div>



                <g:hiddenField name="sessionId" value="${sessionId}"/>

                <g:jprogressDialog progressId="${sessionId}" message="please wait for the generation to finish" trigger="create"/>

                <div class="button-margin-top">
                    <g:submitToRemote class="next" before="importStart();" action="runCreate" name="create" id="create"
                                      value="create" update="view"/>
                </div>

            </g:form>
        </div>

    </div>
</div>