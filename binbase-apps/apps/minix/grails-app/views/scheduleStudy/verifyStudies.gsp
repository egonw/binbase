<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <title>Schedule study tool</title>

    <link rel="stylesheet" href="${resource(dir: 'css/slickgrid', file: 'slick.grid.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css/slickgrid', file: 'slick.grid.css')}"/>

</head>

<body>
<g:render template="/shared/navigation"/>
<g:set var="sessionId" value="${new Date().time}"/>


<div class="body">

    <div class="design">
        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">

                <p>
                    this step is the validation of your processing instructions and generating an overview of your studies to be submitted to the processing system.
                </p>

            </div>

        </div>

        <div class="left-center-column">
            <div class="header">
                Welcome to the study schedule tool
            </div>

            <g:form>

                <div id="schedule">

                    <div class="textDescription">
                        please press 'schedule study' to initiate the data processing
                    </div>

                    <g:if test="${studies.size() > 1}">
                        <div>
                            <span class="property-title"></span>

                            <span class="property-value">

                                <div class="buttons">

                                    <g:submitToRemote
                                            update="schedule"
                                            onFailure="verificationFailed(XMLHttpRequest,textStatus,errorThrown);"
                                            class="next"
                                            action="ajaxVerifyAndSchedule"
                                            name="ajaxVerifyAndSchedule"
                                            before="validateStudy()"
                                            id="ajaxVerifyAndScheduleMany" value="schedule calculations"/>
                                </div>
                            </span>

                        </div>
                    </g:if>

                    <div id="selectStudyDiv" class="textDescription">

                        <div class="element" id="sampleValidation">
                        </div>

                        <g:each var="study" in="${studies.sort()}">
                            <div class="header">
                                ${study.title}
                            </div>

                            <div id="study_${study.id}"></div>

                            <g:hiddenField name="study_destination_${study.id}"
                                           value="${destination.get(study.id.toString())}"/>

                            <g:if test="${references.get(study.id.toString()) != null}">
                                <g:hiddenField name="study_reference_${study.id}"
                                               value="${references.get(study.id.toString())}"/>
                            </g:if>
                        </g:each>
                    </div>

                    <g:hiddenField name="ignoreBlanks" value="${ignoreBlanks}"/>
                    <g:hiddenField name="ignoreQC" value="${ignoreQC}"/>
                    <g:hiddenField name="ignoreMissingSamples" value="${ignoreMissingSamples}"/>
                    <g:hiddenField name="sessionId" value="${sessionId}"/>


                    <div class="element">
                        <span class="property-title"></span>

                        <span class="property-value">

                            <div class="buttons">

                                <g:submitToRemote
                                        update="schedule"
                                        onFailure="verificationFailed(XMLHttpRequest,textStatus,errorThrown);"
                                        class="next"
                                        action="ajaxVerifyAndSchedule"
                                        name="ajaxVerifyAndSchedule"
                                        before="validateStudy()"
                                        id="ajaxVerifyAndSchedule" value="schedule calculations"/>
                            </div>
                        </span>

                    </div>

                </div>
            </g:form>
        </div>
    </div>

</div>

<g:javascript src="jquery/jquery.event.drag-2.2.js"/>
<g:javascript src="slickgrid/slick.core.js"/>
<g:javascript src="slickgrid/slick.grid.js"/>
<g:javascript src="slickgrid/slick.dataview.js"/>
<g:javascript src="minixProgressBar.js?${new Date().toTimestamp()}"/>

<g:javascript src="minixScheduleGridView.js?${new Date().toTimestamp()}"/>
<g:javascript src="minixScheduleStudy.js?${new Date().toTimestamp()}"/>



<g:each var="study" in="${studies.sort()}">
    <g:javascript>
        //generating the grids for us, based on the provided information
        displayStudyGrid("${ignoreQC}","${ignoreBlanks}",'${g.createLink(controller: "rest", action: "listDetailedSamplesForStudyId", id: study.id)}');
    </g:javascript>
</g:each>


<g:javascript>
    function validateStudy(){
        showProgressBar(${sessionId},'${g.createLink(controller: "scheduleStudy", action: "ajaxRenderProgress", id: sessionId)}',"sampleValidation","please wait while we are validating that all files exist");
    }

</g:javascript>
</body>

</html>
