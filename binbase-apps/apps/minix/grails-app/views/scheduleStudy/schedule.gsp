<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <title>Schedule study tool</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">
        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">

                <p>
                    please select all the study which you would like to schedule on the system.
                </p>


            </div>

        </div>

        <div class="left-center-column">
            <div class="header">
                Welcome to the study schedule tool
            </div>

            <div class="textDescription">

                This tool allows you to schedule one or more studies in a quick and efficient way.
            </div>


            <g:form name="scheduleStudy" action="defineProcessingInstructions">

                <div id="selectStudyDiv" class="textDescription">
                    <g:render template="selectStudie" model="[studies:studies"/>
                </div>


                <div class="element">
                    <div class="button-margin-top">
                        <g:submitButton class="next" action="defineProcessingInstructions" name="selectProcessingInstructions"
                                        value="next"/>
                    </div>

                </div>


            </g:form>

        </div>
    </div>

</div>
</body>
</html>
