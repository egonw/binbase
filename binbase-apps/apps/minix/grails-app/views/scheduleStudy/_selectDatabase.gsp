<g:setProvider library="jquery"/>

<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<g:if test="${flash.message != null}">
    <div class="message">${flash.message}</div>
</g:if>

<g:if test="${studies}">

    <div class="element">
        <table class="contentTable">
            <thead>
            <th>study id</th>
            <th>description</th>
            <th>architecture</th>
            <th>destination</th>


            </thead>
            <tbody>
            <g:each var="study" in="${studies.sort()}" status="i">
                <tr>

                    <td>${study.id}</td>
                    <td>${study.description}</td>
                    <td>${study.architecture}</td>
                    <td><g:select from="${databases}" id="${study.id}_destination" name="${study.id}_destination" value="${study.databaseColumn}"/></td>


                    <div class="textField">
                        <g:hiddenField name="study_name" value="${study.description}"/>
                        <g:hiddenField name="study_id" value="${study.id}"/>
                    </div>
                </tr>

            </g:each>

            </tbody>
        </table>
    </div>
</g:if>



