<g:setProvider library="jquery"/>

<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<g:if test="${flash.message != null}">
    <div class="message">${flash.message}</div>
</g:if>

<g:if test="${studies}">

    <div id="messageField"></div>

    <div class="element">
        <table class="contentTable">
            <thead>
            <th>study id</th>
            <th>description</th>
            <th>destination</th>
            <th>reference</th>

            </thead>
            <tbody>
            <g:each var="study" in="${studies.sort()}" status="i">
                <tr>
                    <td>${study.id}</td>

                    <td title="${study.description}">${study.description.length() <= 30 ? study.description : study.description[0..30] }...</td>
                    <td>${destination.get(study.id.toString())}</td>
                    <td><g:textField name="${study.id}_refrence"/></td>

                    <g:hiddenField name="study_name" value="${study.description}"/>
                    <g:hiddenField name="study_id" value="${study.id}"/>
                    <g:hiddenField name="${study.id}_destination" value="${destination.get(study.id.toString())}"/>
                </tr>

            </g:each>

            </tbody>
        </table>
    </div>
</g:if>


<g:javascript>
  $(document).ready(function() {

      $("input[name$='_refrence']").on('input', function(e) {
        var selector = $(this);

        if(selector.val() == ""){
            selector.parent().removeClass("errors");
            $('#messageField').html("");

        }
      })
      .autocomplete({
              source: '${g.createLink(action: "findAllStudy", controller: "autoCompletion")}',
              dataType:"json",
              close: function( event, ui){

                //this calls the webpage and checks that we are having all the information we need and that the selected experiment is in the actual selected database
                //since this operation will fail otherwise

                var selector = $(this);

                 $.ajax({
                    async: true,
                    method: 'get',
                    url : "${g.createLink(controller: 'scheduleStudy', action: 'ajaxStudieExistInDatabase')}",
                    dataType : 'text',
                    data: {column:$('#' + selector.attr("name").replace("_refrence","_destination")).val(),referenceStudy:selector.val()},
                    success: function (text) {
                                if(text == 'true'){
                                    selector.parent().removeClass("errors");
                                }
                                else{
                                    $('#messageField').html("<div class=\"element\"><div class=\"errors\">It looks like your selected study has not been calculated yet in the destination and so can't be used as a reference file! Please schedule the studie first or leave the field empty</div></div>");
                                    selector.parent().addClass("errors");

                             }
                         }
                     });
                }

    });
  });
</g:javascript>