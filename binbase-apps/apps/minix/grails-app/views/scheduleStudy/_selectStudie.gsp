<g:setProvider library="jquery"/>

<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<!-- simple tool to define and select species -->

<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("input")
      .filter(function() {
        return this.id.match(/study.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findStudy", controller: "autoCompletion")}',
              dataType:"json"
    });
    })
  });
</g:javascript>


<g:if test="${flash.message != null}">
    <div class="message">${flash.message}</div>
</g:if>

<g:if test="${studies}">

    <div class="element">
        <table class="contentTable">
            <thead>
            <th>study id</th>
            <th>class count</th>
            <th>description</th>
            <th>architecture</th>

            </thead>
            <tbody>
            <g:each var="study" in="${studies.sort()}" status="i">
                <tr>

                    <td>${study.id}</td>
                    <td>${study.classes.size()}</td>
                    <td>${study.description}</td>
                    <td>${study.architecture}</td>


                    <div class="textField">
                        <g:hiddenField name="study_name" value="${study.description}"/>
                        <g:hiddenField name="study_id" value="${study.id}"/>
                    </div>
                </tr>

            </g:each>

            </tbody>
        </table>
    </div>
</g:if>

<div class="textDescription">
    please enter your next study id or name into the textfield below and press 'add study'.
</div>


<div class="element">

    <div class="textField">
        <g:textField name="study"/>

    </div>

    <div class="button-margin-left">
        <g:submitToRemote class="add" action="ajaxAddStudyToSchedule" name="addStudy" id="addStudy"
                          value="add study" update="selectStudyDiv"/>
    </div>

</div>



