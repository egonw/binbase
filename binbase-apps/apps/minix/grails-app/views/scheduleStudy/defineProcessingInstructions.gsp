<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <title>Schedule study tool</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">
        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">

                <p>
                    please select the destination for the scheduling of this study as well as if you like to ignore certain samples or wan't to use a reference file for identification purpose
                </p>

            </div>

        </div>

        <div class="left-center-column">
            <div class="header">
                Welcome to the study schedule tool
            </div>

            <div class="textDescription">
                please select your destination
            </div>


            <g:form name="scheduleStudy" action="defineReferenceInstructions">

                <div id="selectStudyDiv" class="textDescription">
                    <g:render template="selectDatabase" model="[studies: studies, databases: databases]"/>
                </div>

                <div class="element">
                    <div class="prop">
                        <g:checkBox class="name" id="ignoreMissingSamples" name="ignoreMissingSamples" checked="true"/><label
                            class="value" for="ignoreMissingSamples">Ignore missing samples</label>
                        <g:checkBox class="name" id="ignoreBlanks" name="ignoreBlanks" checked="true"/><label class="value"
                                                                                            for="ignoreBlanks">Ignore blanks</label>
                        <g:checkBox class="name" id="ignoreQC" name="ignoreQC" checked="true"/><label class="value"
                                                                                        for="ignoreQC">Ignore quality controls</label>
                    </div>
                </div>

                <div class="element">
                    <div class="button-margin-top">
                        <g:submitButton class="next" name="next"
                                        value="next"/>
                    </div>

                </div>

            </g:form>

        </div>
    </div>

</div>
</body>
</html>
