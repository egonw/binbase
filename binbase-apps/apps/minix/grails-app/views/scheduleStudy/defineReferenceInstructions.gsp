<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <title>Schedule study tool</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">
        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">

                <p>
                    at this step we will define your reference file
                </p>

            </div>

        </div>

        <div class="left-center-column">
            <div class="header">
                Welcome to the study schedule tool
            </div>

            <div class="textDescription">
                please select a reference study now for the studies, which you'd like to have a reference file. If you do not want to use a reference file, just leave the field empty
            </div>


            <g:form name="scheduleStudy" action="verifyStudies">

                <div id="selectStudyDiv" class="textDescription">
                    <g:render template="selectReferences" model="[studies: studies, destination: destination]"/>
                </div>

                <g:hiddenField name="ignoreBlanks" value="${ignoreBlanks}"/>
                <g:hiddenField name="ignoreQC" value="${ignoreQC}"/>
                <g:hiddenField name="ignoreMissingSamples" value="${ignoreMissingSamples}"/>


                <div class="element">
                    <div class="button-margin-top">
                        <g:submitButton class="next" name="verifyStudies"
                                        value="next"/>
                    </div>

                </div>

            </g:form>

        </div>
    </div>

</div>
</body>
</html>
