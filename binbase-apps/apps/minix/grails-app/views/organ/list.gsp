<%@ page import="minix.Organ" %>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <title>Organ List</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

  <div class="design">

    <div class="left">
      <div class="header">

        Information

      </div>

      <div class="left_information_box">
        <p><g:message code="description.organ"/></p>
        <div class="topSpacer"></div>
                     <p><g:message code="description.organ.list"/></p>

      </div>

    </div>
    <div class="left-center-column"><div class="header">Organ List</div>
      <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
      </g:if>
      <div class="element">

        <table>
          <thead>
          <tr>

            <g:sortableColumn property="id" title="Id"/>

            <g:sortableColumn property="name" title="Name"/>

            <th>Species</th>

          </tr>
          </thead>
          <tbody>
          <g:each in="${organInstanceList}" status="i" var="organInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

              <td><g:link action="show" id="${organInstance.id}">${organInstance.id?.encodeAsHTML()}</g:link></td>

              <td>${organInstance.name?.encodeAsHTML()}</td>

              <td>${organInstance.speciees?.encodeAsHTML()}</td>

            </tr>
          </g:each>
          </tbody>
        </table>

        <div class="paginateButtons">
          <g:paginate total="${Organ.count()}"/>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>

