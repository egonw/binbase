
<%@ page import="minix.Architecture" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'architecture.label', default: 'Architecture')}"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

  <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
  <div class="center-full-page">
    <div class="header"><g:message code="default.create.label" args="[entityName]"/></div>
    <g:if test="${flash.message}">
      <div class="message">${flash.message}</div>
    </g:if>

    <g:hasErrors bean="${architectureInstance}">
      <div class="errors">
        <g:renderErrors bean="${architectureInstance}" as="list"/>
      </div>
    </g:hasErrors>

    <g:form action="save" method="post" >
      <div class="scaffoldingProperties">

        

        <div>
          <span class="scaffoldingPropertyLabel">

            <label for="name"><g:message code="architecture.name.label" default="Name"/></label>

          </span>
          <span class="scaffoldingPropertyValue ${hasErrors(bean: architectureInstance, field: 'name', 'errors')}">

            <g:textField name="name" maxlength="25" value="${architectureInstance?.name}" />
          </span>
        </div>
        

        <div>
          <span class="scaffoldingPropertyLabel">

            <label for="description"><g:message code="architecture.description.label" default="Description"/></label>

          </span>
          <span class="scaffoldingPropertyValue ${hasErrors(bean: architectureInstance, field: 'description', 'errors')}">

            <g:textArea name="description" cols="40" rows="5" value="${architectureInstance?.description}" />
          </span>
        </div>
        

        <div>
          <span class="scaffoldingPropertyLabel">

            <label for="relatedController"><g:message code="architecture.relatedController.label" default="Related Controller"/></label>

          </span>
          <span class="scaffoldingPropertyValue ${hasErrors(bean: architectureInstance, field: 'relatedController', 'errors')}">

            <g:textField name="relatedController" value="${architectureInstance?.relatedController}" />
          </span>
        </div>
        

        <div>
          <span class="scaffoldingPropertyLabel">

            <label for="relatedAction"><g:message code="architecture.relatedAction.label" default="Related Action"/></label>

          </span>
          <span class="scaffoldingPropertyValue ${hasErrors(bean: architectureInstance, field: 'relatedAction', 'errors')}">

            <g:textField name="relatedAction" value="${architectureInstance?.relatedAction}" />
          </span>
        </div>
        

      </div>
      <div class="buttons">
        <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}"/></span>
      </div>
    </g:form>
  </div>
</div>
</body>
</html>
