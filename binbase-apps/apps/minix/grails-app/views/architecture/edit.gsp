<%@ page import="minix.Architecture" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="layout" content="main"/>
	<g:set var="entityName" value="${message(code: 'architecture.label', default: 'Architecture')}"/>
	<title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="nav">
	<span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
	<span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></span>
	<span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link></span>
</div>

<div class="body">
	<h1><g:message code="default.edit.label" args="[entityName]"/></h1>
	<g:if test="${flash.message}">
		<div class="message">${flash.message}</div>
	</g:if>
	<g:hasErrors bean="${architectureInstance}">
		<div class="errors">
			<g:renderErrors bean="${architectureInstance}" as="list"/>
		</div>
	</g:hasErrors>
	<g:form method="post">
		<g:hiddenField name="id" value="${architectureInstance?.id}"/>
		<g:hiddenField name="version" value="${architectureInstance?.version}"/>
		<div class="body design center-full-page">
			<table>
				<tbody>

				<div class="scaffoldingProperties">
					<span class="scaffoldingPropertyLabel"><label for="name"><g:message code="architecture.name.label" default="Name"/></label></span>
					<span class="value ${hasErrors(bean: architectureInstance, field: 'name', 'errors')} scaffoldingPropertyValue">
						<g:textField name="name" maxlength="25" value="${architectureInstance?.name}"/>
					</span>
				</div>

				<div class="scaffoldingProperties">
					<span class="scaffoldingPropertyLabel"><label for="description" class="scaffoldingPropertyLabel"><g:message code="architecture.description.label" default="Description"/></label></span>
					<span valign="top" class="value ${hasErrors(bean: architectureInstance, field: 'description', 'errors')}">
						<g:textArea name="description" cols="40" rows="5" value="${architectureInstance?.description}"/>
					</span>
				</div>

				<div class="scaffoldingProperties">
					<span class="scaffoldingPropertyLabel"><label for="relatedController" class="scaffoldingPropertyLabel"><g:message code="architecture.relatedController.label" default="Related Controller"/></label></span>
					</td>
					<td valign="top" class="value ${hasErrors(bean: architectureInstance, field: 'relatedController', 'errors')}">
						<g:textField name="relatedController" value="${architectureInstance?.relatedController}"/>
					</td>
				</div>

				<div class="scaffoldingProperties">
					<span class="scaffoldingPropertyLabel"><label for="relatedAction" class="scaffoldingPropertyLabel"><g:message code="architecture.relatedAction.label" default="Related Action"/></label></span>
					<td valign="top" class="value ${hasErrors(bean: architectureInstance, field: 'relatedAction', 'errors')} sc">
						<g:textField name="relatedAction" value="${architectureInstance?.relatedAction}"/>
					</td>
				</div>

				%{--<tr class="prop">--}%
					%{--<td valign="top" class="name">--}%
						%{--<label for="designs"><g:message code="architecture.designs.label" default="Designs"/></label>--}%
					%{--</td>--}%
					%{--<td valign="top" class="value ${hasErrors(bean: architectureInstance, field: 'designs', 'errors')}">--}%

						%{--<ul>--}%
							%{--<g:each in="${architectureInstance?.designs ?}" var="d">--}%
								%{--<li><g:link controller="studieDesign" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>--}%
							%{--</g:each>--}%
						%{--</ul>--}%
						%{--<g:link controller="studieDesign" action="create"--}%
						        %{--params="['architecture.id': architectureInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'studieDesign.label', default: 'StudieDesign')])}</g:link>--}%

					%{--</td>--}%
				%{--</tr>--}%

				<div class="scaffoldingProperties">
					<span class="scaffoldingPropertyLabel"><label for="instruments" class="scaffoldingPropertyLabel"><g:message code="architecture.instruments.label" default="Instruments"/></label></span>
					<span valign="top" class="value ${hasErrors(bean: architectureInstance, field: 'instruments', 'errors')} scaffoldingPropertyValue">

						<ul>
							<g:each in="${architectureInstance?.instruments ?}" var="i">
								<li><g:link controller="instrument" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></li>
							</g:each>
						</ul>
						<g:link controller="instrument" action="create"
						        params="['architecture.id': architectureInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'instrument.label', default: 'Instrument')])}</g:link>

					</td>
				</tr>

				%{--<tr class="prop">--}%
					%{--<td valign="top" class="name">--}%
						%{--<label for="studies"><g:message code="architecture.studies.label" default="Studies"/></label>--}%
					%{--</td>--}%
					%{--<td valign="top" class="value ${hasErrors(bean: architectureInstance, field: 'studies', 'errors')}">--}%

						%{--<ul>--}%
							%{--<g:each in="${architectureInstance?.studies ?}" var="s">--}%
								%{--<li><g:link controller="studie" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>--}%
							%{--</g:each>--}%
						%{--</ul>--}%
						%{--<g:link controller="studie" action="create"--}%
						        %{--params="['architecture.id': architectureInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'studie.label', default: 'Studie')])}</g:link>--}%

					%{--</td>--}%
				%{--</tr>--}%

				</tbody>
			</table>
		</div>

		<div class="buttons">
			<span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"/></span>
			<span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}"
			                                     onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/></span>
		</div>
	</g:form>
</div>
</body>
</html>
