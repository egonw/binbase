<%@ page import="minix.Architecture" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>Architecture List</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.architecture"/></p>
            </div>

        </div>

        <div class="left-center-column">
            <div class="header">Architecture List</div>
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>
            <div class="element">
                <ul class="none-horizontal">
                    <!-- here we are registering our gctofs systems -->
                    <li><g:link class="add" controller="architecture"
                                action="create">register new architecture</g:link></li>
                </ul>
            </div>

            <div class="element">

                <table>
                    <thead>
                    <tr>

                        <g:sortableColumn property="id" title="Id"/>

                        <g:sortableColumn property="name" title="Name"/>

                        <g:sortableColumn property="description" title="Description"/>

                        <g:sortableColumn property="relatedController" title="Related Controller"/>

                        <g:sortableColumn property="relatedAction" title="Related Action"/>

                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${architectureInstanceList}" status="i" var="architectureInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td><g:link action="show"
                                        id="${architectureInstance.id}">${architectureInstance.id?.encodeAsHTML()}</g:link></td>

                            <td>${architectureInstance.name?.encodeAsHTML()}</td>

                            <td>${architectureInstance.description?.encodeAsHTML()}</td>

                            <td>${architectureInstance.relatedController?.encodeAsHTML()}</td>

                            <td>${architectureInstance.relatedAction?.encodeAsHTML()}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="paginateButtons">
                    <g:paginate total="${Architecture.count()}"/>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>

