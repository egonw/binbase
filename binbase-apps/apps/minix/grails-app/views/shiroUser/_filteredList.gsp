<%@ page import="minix.ShiroUser" %>
<table>
    <thead>
    <tr>

        <g:sortableColumn property="username" title="Username"/>

        <th>Assigned Role</th>

        <th>locked</th>

    </tr>
    </thead>
    <tbody>
    <g:each in="${shiroUserInstanceList}" status="i" var="shiroUserInstance">
        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

            <td><g:link action="show" id="${shiroUserInstance.id}">${shiroUserInstance.username?.encodeAsHTML()}</g:link></td>
            <td>
                <g:render template="renderRole" model="[shiroUserInstance:shiroUserInstance]"></g:render>
            </td>

            <td>
                ${shiroUserInstance.locked}
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
<div class="paginateButtons">
    <g:paginate total="${shiroUserInstanceTotal}"/>
</div>