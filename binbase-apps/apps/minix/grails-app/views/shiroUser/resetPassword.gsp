<%@ page import="minix.roles.RoleUtil; minix.ShiroRole; minix.ShiroUser" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'shiroUser.label', default: 'ShiroUser')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">

                please provide the new password for this user
            </div>

        </div>
        <div class="left-center-column">
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>

        <div class="element">

            <g:form method="post">
                <g:hiddenField name="id" value="${shiroUserInstance?.id}"/>

                <div class="scaffoldingProperties">

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <g:message code="shiroUser.pasword.label" default="Password"/>
                        </span>
                        <span>
                            <g:passwordField name="password" value=""/>
                        </span>
                    </div>

                </div>
                <div class="topSpacer"></div>
                <div class="myButton">

                    <div class="buttons">

                        <span><g:actionSubmit class="save" action="resetPassword" value="${message(code: 'default.button.update.label', default: 'Update')}"/></span>
                    </div>
                </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
</body>
</html>
