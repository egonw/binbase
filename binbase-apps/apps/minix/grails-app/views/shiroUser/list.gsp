<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("input")
      .filter(function() {
        return this.id.match(/userFilter/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findOrgan", controller: "autoCompletion")}', dataType:"json"
              }
      );
    })
  });
</g:javascript>

<%@ page import="minix.ShiroUser" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>Registered Users</title>
    <meta name="layout" content="main"/>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

</head>
<body>

<g:javascript src="jquery/jquery.observe_field.js"/>
<g:render template="/shared/navigation"/>

<div class="body">
    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.user"/></p>
                <div class="topSpacer"></div>
                <p><g:message code="description.user.list"/></p>

            </div>

        </div>
        <div class="left-center-column">

            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>


            <div class="header">Actions</div>

            <div class="element">
                <ul class="none-horizontal">
                    <li><g:link class="add" controller="shiroUser" action="create">new user</g:link></li>
                </ul>
            </div>

            <div class="header">Registered Users</div>

            <div class="element">

                <g:form>
                    <div class="void">
                        <g:textField id="userFilter" class="filter" name="userFilter"/>

                        <g:javascript>
                            //update the result table with the filtered result
                            $(function() {
                                $("#userFilter").observe_field(0.3, function() {
                                    jQuery.ajax({type:'POST',data:{'username': this.value}, url:"${g.createLink(controller: 'shiroUser',action: 'ajaxListFilter')}",success:function(data, textStatus) {
                                        jQuery('#filteredUserTable').html(data);
                                    },error:function(XMLHttpRequest, textStatus, errorThrown) {
                                    }});
                                    return false;

                                });
                            });
                        </g:javascript>
                    </div>
                </g:form>

                <div class="void">
                    <div id="filteredUserTable">
                        <g:render template="filteredList" model="[shiroUserInstanceList:shiroUserInstanceList,shiroUserInstanceList:shiroUserInstanceList]"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

