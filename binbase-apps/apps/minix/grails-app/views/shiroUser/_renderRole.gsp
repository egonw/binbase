<%@ page import="minix.ShiroUser" %><g:if test="${ShiroUser.hasAdminRights(shiroUserInstance)}">

  admin

</g:if>
<g:elseif test="${ShiroUser.hasManagmentRights(shiroUserInstance)}">

  manager

</g:elseif>
<g:else>

  user

</g:else>