<g:setProvider library="jquery"/>

<%
    def randomNumber = System.currentTimeMillis()
%>
<g:javascript>
  $(document).ready(function() {
    // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
    $("#dialog-comment-modal-${id}_${randomNumber}").dialog("destroy");

    $("#dialog-comment-modal-${id}_${randomNumber}").dialog({
      height: 480,
      width: 768,
      modal: true
    });
  });
</g:javascript>

<div id="dialog-comment-modal-${id}_${randomNumber}">
    <g:form name="createNewComment" id="createNewComment" controller="comment">
        <div class="description">
            please enter your comment into the editor below.
        </div>

        <g:hasErrors bean="${comment}">
            <div class="errors">
                <g:renderErrors bean="${comment}" as="list"/>
            </div>
        </g:hasErrors>
        <div class="element">

            <ckeditor:config var="toolbar_commentToolbar">
                [
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll' ] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'tools', items : [ 'Maximize' ] }
		                ]
            </ckeditor:config>

            <ckeditor:editor height="250px" width="100%" name="text" id="${randomNumber}" toolbar="commentToolbar">
                ${comment?.text}
            </ckeditor:editor>

            <g:hiddenField name="id" value="${id}"/>
            <g:hiddenField name="type" value="${type}"/>
            <g:hiddenField name="destination" value="${destination}"/>

        </div>

        <g:javascript>
            function CKupdate() {
                for (instance in CKEDITOR.instances)
                    CKEDITOR.instances[instance].updateElement();

               $("#dialog-comment-modal-${id}_${randomNumber}").dialog("close");
               $("#dialog-comment-modal-${id}_${randomNumber}").dialog("destroy");
                return true;
            }

            function CKreset() {
                if (CKEDITOR.instances['${randomNumber}']) {
                    CKEDITOR.instances['${randomNumber}'].destroy(true);
                }
            }
        </g:javascript>
        <div class="button-margin-top">
            <g:submitToRemote before="CKupdate();" after="CKreset();" class="save" action="saveAjax"
                              update="${destination}"
                              value="save comment"/>
        </div>

    </g:form>

</div>