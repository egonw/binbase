<%@ page import="minix.TreatmentSpecific; minix.Studie" %>

<html>
<head>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'binbaseSchedule.css')}"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'studie.label', default: 'Studie')}"/>
    <title>Schedule BinBase Calculation</title>

</head>

<body>
<g:render template="/shared/navigation"/>

<g:javascript>
    jQuery(document).ready(function () {

        $("#column").bind("change", function () {
            var db = $('#column').val();
            $("#columnSelection").html("<div class='message'>you selected the " + db + " database for your calculations</div>")
        });

    });
</g:javascript>
<div class="body">

    <div class="design">

        <div id="left">
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>


            <div class="center-full-page">
                <div class="element">

                    <div class="header">
                        Submit calculation to BinBase
                    </div>

                </div>

                <div id="next">

                    <div class="schedulingBox">

                        <g:form name="scheduleCalculation">

                            <div id="columnSelection">

                            </div>

                            <div class="element">
                                <span class="property-title">
                                    database for calculation
                                </span>
                                <span class="property-value">
                                    <g:select from="${columns}" name="column" id="column" value="${column}"/>
                                </span>
                            </div>

                            <div class="element">
                                <span class="property-title">
                                    would you like to use a different study as a reference file
                                </span>
                                <span class="property-value">
                                    <g:checkBox name="referenceStudy"/>
                                </span>
                            </div>
                            <g:if test="${studies.size() == 1}">
                                <g:hiddenField name="studie" id="studie" value="${studies[0].id}"/>
                            </g:if>
                            <g:else>
                                <div class="element">
                                    <span class="property-title">
                                        desired study
                                    </span>
                                    <span class="property-value">
                                        <g:select optionKey="id" from="${studies}" name="studie"/>
                                    </span>
                                </div>
                            </g:else>


                            <div class="element">
                                <span class="property-title"></span>

                                <span class="property-value">

                                    <div class="buttons">
                                        <g:submitToRemote class="next" action="ajaxSamplesForCalculation"
                                                          name="ajaxSamplesForCalculation"
                                                          id="ajaxSamplesForCalculation" value="next step"
                                                          update="next"/>
                                    </div>
                                </span>

                            </div>
                            <!-- store the hidden id-->

                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
