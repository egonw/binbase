<g:setProvider library="jquery"/>

<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<!-- simple tool to define and select species -->

<g:javascript>
  $(document).ready(function() {

      $("#nextStep").hide();
      $("input")
      .filter(function() {
        return this.id.match(/referenceStudy.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findAllStudy", controller: "autoCompletion")}',
              dataType:"json",
              close: function( event, ui){

                //this calls the webpage and checks that we are having all the information we need and that the selected experiment is in the actual selected database
                //since this operation will fail otherwise

                 $.ajax({
                    async: true,
                    method: 'get',
                    url : "${g.createLink(controller: 'binbaseScheduling', action: 'ajaxStudieExistInDatabase')}",
                    dataType : 'text',
                    data: {column:"${column}",referenceStudy:$("#referenceStudy").val()},
                    success: function (text) {
                             $("#messageField").html(text);
                         }
                     });
                }
    });
  });
</g:javascript>

<div class="schedulingBox">

    <g:form name="selectReference">

        <g:if test="${flash.message != null}">
            <div class="message">${flash.message}</div>
        </g:if>

        <div>
            <div class="element">
                <span class="property-title">
                    reference study name or id
                </span>

                <span class="property-value">

                    <g:textField id="referenceStudy" name="referenceStudy"/>

                </span>
            </div>

            <div id="messageField"></div>
        </div>

        <g:hiddenField name="studie" value="${studie.id}"/>
        <g:hiddenField name="column" value="${column}"/>


        <div class="element">
            <span class="property-title">
            </span>

            <span class="property-value">

                <div class="buttons" id="nextStep">
                    <g:submitToRemote class="next" action="ajaxSamplesForCalculation" name="selectSamples"
                                      id="selectSamples"
                                      value="next step" update="next"/>
                </div>

            </span>
        </div>
    </g:form>
</div>