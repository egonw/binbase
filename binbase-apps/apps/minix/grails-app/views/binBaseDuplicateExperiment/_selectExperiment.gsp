<g:setProvider library="jquery"/>

<g:if test="${flash.errorMessage}">
  <div class="errors">${flash.errorMessage}</div>
</g:if>

<g:javascript>

  /**
  * responds to the selection
  * @param event
  */
  function respondToSelect(value) {

    //fireoff the ajax request
    jQuery.ajax({data: "database=" + value, type:'POST', url:"${g.createLink(controller: 'binBaseDuplicateExperiment',action: 'updateExperimentsByDatabase_ajax')}",success:function(data, textStatus) {
      jQuery('#binbaseIdSelect').html(data);
    },error:function(XMLHttpRequest, textStatus, errorThrown) {
    }});
    return false;
  }

  $(document).ready(function() {
    $("#databaseSelect").change(function() {

      if (this.value != null && this.value.length > 0) {
        //update a me
        $(".messageBox").html("<div class='message'>please wait we are fetching the BinBase experiment ids for the database '" + this.value + "</div>");

        //fire off the select
        respondToSelect(this.value);

        //clear the message b
        $(".messageBox").html("<div class='message'>all id's are loaded for the database '" + this.value + "' </div>")
      }
      else {
        $(".messageBox").empty();

      }
    });
  });
</g:javascript>

<div class="messageBox">

</div>
<g:form>
  <div class="header">Import existing Studie From BinBase</div>

  <div class="description">this utilitiy helps you to import existing BinBase studies into the database</div>


  <div class="textDescription topSpacer">
    please select your database of interrest
  </div>
  <div class="fillField">
    <g:select from="${columns}" id="databaseSelect" name="column" noSelection="['':'please select your database']" value="${column}"/>
  </div>


  <div class="textDescription topSpacer">
    please select your studie of interrest
  </div>
  <div class="fillField">

    <div id="binbaseIdSelect">
      <g:select from="" name="experiment" noSelection="['':'please select your experiment id']"/>
    </div>
  </div>
  <g:if test="${column != null}">
    <g:javascript>
        $(document).ready(function() {

          respondToSelect('${column}');
        });
    </g:javascript>
  </g:if>

  <div class="myButton">

    <div class="buttons">
      <g:submitToRemote update="next" action="importExperiment_ajax" value="import selected experiment" class="next"/>
    </div>
  </div>
</g:form>
