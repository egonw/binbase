
<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<g:setProvider library="jquery"/>

    <script type="text/javascript">
      window.onload=function() {
      function countdown() {
      if ( typeof countdown.counter == 'undefined' ) {
      countdown.counter = 5; // initial count
      }
      if(countdown.counter > 0) {
      document.getElementById('count').innerHTML = countdown.counter--;
      setTimeout(countdown, 1000);
      }
      else {
      location.href = '../studie/show/${studie.id}';
      }
      }
      countdown();
      };
    </script>

    <div>

      congratulations, your studie is imported and you will
      be forwarded in <span id="count"></span> seconds to your studie for further actions.
    If this page doesn't change, please press
    <g:link controller="studie" action="show" id="${studie.id}">this link</g:link> to continue.

    </div>