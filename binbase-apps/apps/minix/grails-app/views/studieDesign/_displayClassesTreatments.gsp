<g:setProvider library="jquery"/>

<%@ page import="minix.util.TreatmentUtil; minix.TreatmentSpecific" %>
<%
  assert treatmentSpecific != null, "you need to provide a treatment specific variable"
  assert species != null, "you need to provide a species variable"
  assert organ != null, "you need to provide an organ variable"
  assert level != null, "you need to provide an level variable, which defines the deepest child"
  assert design != null, "you need to provide a design variable, which defines the design"

  //required util
  TreatmentUtil util = new TreatmentUtil()
  def result = util.buildListToParent(treatmentSpecific, []).reverse()

  //the distance from the root
  def distance = level - (util.calculateLevel(treatmentSpecific) + 1)
%>
<g:if test="${treatmentSpecific.parent == null}">
  <g:if test="${treatmentSpecific.subTreatmentSpecifics != null}">
    <g:if test="${treatmentSpecific.subTreatmentSpecifics.size() == 0}">
      <!-- tree rendered in form of a table -->
      <tr>
        <td><g:checkBox name="generate_${species.id}_${organ.id}_${treatmentSpecific.id}"/></td>

        <td>${species}</td>
        <td>${organ}</td>

        <g:each var="parent" in="${result}">
          <td>${parent.treatment.description} - ${parent.value}</td>
        </g:each>

        <g:if test="${distance > 0}">
          <g:each in="${ (1..distance)}">
            <td><!-- distance(${distance}) holder --></td>
          </g:each>
        </g:if>

        <td><g:textField name="samples_${species.id}_${organ.id}_${treatmentSpecific.id}" value="${design.numberOfSamples}"/></td>
      </tr>
    </g:if>
  </g:if>
</g:if>


<g:if test="${treatmentSpecific.parent != null}">
  <g:if test="${treatmentSpecific.subTreatmentSpecifics != null}">
    <g:if test="${treatmentSpecific.subTreatmentSpecifics.size() == 0}">

      <!-- tree rendered in form of a table -->
      <tr>
        <td><g:checkBox name="generate_${species.id}_${organ.id}_${treatmentSpecific.id}"/></td>
        <td>${species}</td>
        <td>${organ}</td>

        <g:each var="parent" in="${result}">
          <td>${parent.treatment.description} - ${parent.value}</td>
        </g:each>

        <g:if test="${distance > 0}">
          <g:each in="${ (1..distance)}">
            <td><!-- distance(${distance}) holder --></td>
          </g:each>
        </g:if>

        <td><g:textField name="samples_${species.id}_${organ.id}_${treatmentSpecific.id}" value="${design.numberOfSamples}"/></td>
      </tr>
    </g:if>
  </g:if>
</g:if>

<g:if test="${treatmentSpecific.subTreatmentSpecifics != null}">
  <g:if test="${treatmentSpecific.subTreatmentSpecifics.size() > 0}">
    <g:each var="treatment" in="${treatmentSpecific.subTreatmentSpecifics}">
      <g:render template="displayClassesTreatments" model="[organ:organ,species:species,treatmentSpecific:treatment,level:level,design:design]"/>
    </g:each>
  </g:if>
</g:if>