<%@ page import="minix.StudieDesign" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>StudieDesign List</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.design"/></p>
            </div>

        </div>
        <div class="left-center-column">
            <div class="header">StudieDesign List</div>
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>
            <div class="element">
                <ul class="none-horizontal">
                    <!-- here we are registering our gctofs systems -->
                    <li><g:link class="add" controller="studieDesign" action="create"><g:message code="new.studie.design"/></g:link></li>
                </ul>
            </div>

            <div class="element">

                <table>
                    <thead>
                    <tr>

                        <g:sortableColumn property="id" title="Id"/>

                        <g:sortableColumn property="title" title="Title"/>

                        <th>Generated Studie</th>

                        <g:sortableColumn property="step" title="Step"/>

                        <th>Architecture</th>

                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${studieDesignInstanceList}" status="i" var="studieDesignInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td><g:link action="design" id="${studieDesignInstance.id}" params="[designId:studieDesignInstance.id]">${studieDesignInstance.id?.encodeAsHTML()}</g:link></td>

                            <td>${studieDesignInstance.title?.encodeAsHTML()}</td>

                            <td>${studieDesignInstance.generatedStudie?.encodeAsHTML()}</td>

                            <td>${studieDesignInstance.step?.encodeAsHTML()}</td>

                            <td>${studieDesignInstance.architecture?.encodeAsHTML()}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
                <div class="paginateButtons">
                    <g:paginate total="${studieDesignCount}"/>
                </div>

            </div>
        </div>

    </div>
</div>
</body>
</html>

