<g:setProvider library="jquery"/>

<%@ page import="minix.Treatment" %>


<!-- simple tool to define and select species -->

<%
    assert design != null, "please provide a 'design' variable!"
%>
<g:if test="${flash.message != null &&flash.message.toString().length() > 0}">
    <div class="message">
        <p>${flash.message}</p>
    </div>
</g:if>


<!-- colors our table -->
<g:javascript>

    $(document).ready(function() {
        $('#definedTreatments  tr:even').addClass('even');
        $('#definedTreatments  tr:odd').addClass('odd');
    });

</g:javascript>

<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("input")
      .filter(function() {
        return this.id.match(/treatment_name.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findTreatment", controller: "autoCompletion")}', dataType:"json"
              }
      );
    })
  });
</g:javascript>

<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("input")
      .filter(function() {
        return this.id.match(/treatment_value_.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findTreatmentValue", controller: "autoCompletion")}', dataType:"json"
              }
      );
    })
  });
</g:javascript>

<div class="topSpacer">

</div>
<div id="action_result">

</div>

<div id="renderTreatments">
    <g:render template="treatment/renderTreatmentSpecifics" model="[design:design]"/>
</div>

<!-- we define sub treatments here -->
<div id="subTreatment"></div>


<div class="element">
    <ul class="none-horizontal">
        <li>
            <g:remoteLink class="paste" update="dialog_treatment_${design?.id}" controller="studieDesign" action="ajaxShowPasteThreatmentDialog" id="${design?.id}">paste metadata</g:remoteLink>
        </li>

        <div id="dialog_treatment_${design?.id}"></div>
    </ul>

</div>