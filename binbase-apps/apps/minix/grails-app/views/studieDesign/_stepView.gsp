<%@ page import="minix.step.Step" %>
<!-- used to display the step view -->
<g:setProvider library="jquery"/>

<%
    def step = null

    if (design != null) {
        step = design.step
    }
    else {
        step = Step.DEFINE_SPECIES
    }

    def locked = step > Step.GENERATE_CLASS
%>

<div class="header">

    Design Progress Monitor

</div>

<div class="monitor">
    <ul class="monitor">
        <g:each var="myStep" in="${Step.listEnums()}">

            <g:if test="${myStep.equals(step)}">
                <li class="current">
                    <g:if test="${design == null}">
                        ${myStep.message}
                    </g:if>
                    <g:else>
                        <g:link action="${myStep.method}" params="[designId:design.id]">${myStep.message}</g:link>
                    </g:else>
                </li>
            </g:if>
            <g:else>
                <g:if test="${myStep < step}">
                    <g:if test="${locked }">
                        <li class="previous_locked">
                            ${myStep.message}
                        </li>
                    </g:if>
                    <g:else>
                        <li class="previous">
                            <g:link action="${myStep.method}" params="[designId:design.id]">${myStep.message}</g:link>
                        </li>
                    </g:else>
                </g:if>
                <g:else>
                    <li class="next">${myStep.message}</li>
                </g:else>
            </g:else>
        </g:each>
    </ul>
</div>


<div class="header">

    Information

</div>

<g:if test="${design != null}">
    <g:if test="${design.user != null}">

        <div class="left_information_box">
            Owning user: ${design.user}
        </div>
    </g:if>
</g:if>
<g:else>
    <div class="left_information_box">
        no information's are available at this point in time
    </div>

</g:else>
<g:if test="${locked}">

    <div class="left_information_box">
        at this point your study is generated and has samples assigned, so you cannot go back to a previous step! You are only able to adjust class properties and file properties
    </div>
</g:if>

<div class="topSpacer"></div>
<div class="header">
    Options
</div>

<div class="monitor">
    <ul class="monitor">
        <g:if test="${design != null && design.generatedStudie == null}">
            <li><g:link class="remove" action="delete" controller="studieDesign" id="${design.id}">delete design</g:link></li>
        </g:if>
    </ul>
    <ul class="monitor">
        <g:if test="${design != null}">
            <li><g:link class="copy" action="duplicateDesign" controller="studieDesign" id="${design.id}">duplicate design</g:link></li>
        </g:if>
    </ul>


</div>