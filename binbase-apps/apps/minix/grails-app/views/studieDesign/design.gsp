<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'showClass.css')}"/>
  
  <title>Studie Design Tool</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
  <div id="next">
    <g:render template="designStudie"/>
  </div>
</div>
</body>
</html>
