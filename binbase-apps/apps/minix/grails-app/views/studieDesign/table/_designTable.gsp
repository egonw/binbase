<table>
    <thead>
    <tr>

        <g:sortableColumn property="id" title="Id"/>

        <g:sortableColumn property="title" title="Title"/>

        <th>Generated Studie</th>

        <g:sortableColumn property="step" title="Step"/>

        <th>Architecture</th>

    </tr>
    </thead>
    <tbody>
    <g:each in="${studieDesignInstanceList}" status="i" var="studieDesignInstance">
        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

            <td><g:link controller="studieDesign" action="design" id="${studieDesignInstance.id}" params="[designId:studieDesignInstance.id]">${studieDesignInstance.id?.encodeAsHTML()}</g:link></td>

            <td>${studieDesignInstance.title?.encodeAsHTML()}</td>

            <td>${studieDesignInstance.generatedStudie?.encodeAsHTML()}</td>

            <td>${studieDesignInstance.step?.encodeAsHTML()}</td>

            <td>${studieDesignInstance.architecture?.encodeAsHTML()}</td>

        </tr>
    </g:each>
    </tbody>
</table>
<g:if test="${disablePagenate == null}">

    <div class="paginateButtons">
        <g:paginate total="${studieDesignCount}"/>
    </div>
</g:if>