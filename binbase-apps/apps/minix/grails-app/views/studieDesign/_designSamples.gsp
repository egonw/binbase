<g:setProvider library="jquery"/>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<div class="design">
    <div class="left">
        <g:render template="stepView" model="[design:design]"/>
    </div>

    <div class="left-center-column">
        <g:form name="designSamples">
            <div class="step-dialog">

                <div class="header">
                    step 4 - class generation properties
                </div>

                <div class="description">
                    at this step you define general class properties.
                </div>

                <g:render template="/shared/validation_error" model="[errorBean:design]"/>

                <div class="textDescription">
                    please select how many samples each class should have
                </div>


                <div class="${hasErrors(bean: design, field: 'numberOfSamples', 'errors')}">
                    <div class="textField">
                        <g:textField name="numberOfSamples" value="${design?.numberOfSamples}"/>
                    </div>
                </div>

                <!-- store the hidden id-->
                <g:hiddenField name="designId" id="designId" value="${design?.id}"/>
            </div>

            <div class="button-margin-top">
                <g:submitToRemote class="next" action="ajaxSaveSamples" name="ajaxSaveSamples" id="ajaxSaveSamples"
                                  value="continue to step 5" update="next"/>
            </div>
        </g:form>

    </div>
</div>