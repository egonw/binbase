<!-- render the table with the design specifics -->

<table id="definedTreatments">
    <thead>
    <tr>
        <th>treatment name</th>
        <th>treatment specific value</th>
        <th></th>
        <th></th>
        <th></th>

    </tr>
    </thead>
    <tbody>

    <g:each var="treatmentSpecific" in="${design.treatmentsSpecifics}">

        <g:if test="${treatmentSpecific != null}">

            <%
                def i = treatmentSpecific.id
            %>

            <g:render template="/shared/validation_error" model="[errorBean:treatmentSpecific]"/>

            <tr>
                <td>
                    <div class="${hasErrors(bean: treatmentSpecific.treatment, field: 'description', 'errors')}">
                        <g:textField name="treatment_name_${i}" value="${ treatmentSpecific.treatment.description}"/>

                        <g:javascript>
              $(function() {
              $("#treatment_name_${i}").observe_field(1, function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'treatment',action: 'ajaxSetDescription',id:treatmentSpecific.treatment.id)}",success:function(data,textStatus){jQuery('#action_result').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });
                        </g:javascript>
                    </div>
                </td>
                <td>
                    <div class="${hasErrors(bean: treatmentSpecific, field: 'value', 'errors')}">
                        <g:textField name="treatment_value_${i}" value="${treatmentSpecific.value}"/>
                        <g:hiddenField name="treatment_value_id_${i}" value="${treatmentSpecific.id}"/>

                        <g:javascript>
              $(function() {
              $("#treatment_value_${i}").observe_field(1, function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'treatmentSpecific',action: 'ajaxSetValue',id:treatmentSpecific.id)}",success:function(data,textStatus){jQuery('#action_result').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });
                        </g:javascript>
                    </div>
                </td>

                <!-- add another treatment -->
                <td>
                    <div class="buttons">
                        <g:submitToRemote class="add" action="ajaxDefineSubtreatment" name="addSubTreatment_${i}"
                                          id="addSubTreatment_${i}" value="define a sub treatment"
                                          update="subTreatment"/>
                    </div>

                </td>

                <td>
                    <div class="buttons">
                        <g:submitToRemote class="remove" action="ajaxRemoveTreatment" name="rmTreatment_${i}"
                                          id="rmTreatment_${i}" value="remove" update="next"/>
                    </div>
                </td>

                <td></td>
            </tr>

            <g:if test="${treatmentSpecific.subTreatmentSpecifics != null}">
                <g:if test="${treatmentSpecific.subTreatmentSpecifics.size() > 0}">
                    <!-- render sub treatments -->
                    <g:render template="selectSubTreatment" model="[treatmentSpecific:treatmentSpecific,level:1]"/>
                </g:if>
            </g:if>
        </g:if>

    </g:each>


    <tr>
        <td><div><g:textField name="treatment_name_0"/></div>
        </td>
        <td><div><g:textField name="treatment_value_0"/></div>
        </td>
        <td>
            <div class="buttons">
                <g:submitToRemote class="add" action="ajaxRenderTreatments" name="addTreatment" id="addTreatment"
                                  value="define treatment" update="selectTreatmentDiv"/>
            </div>
        </td>

        <td>

        </td>
        <td><div><g:hiddenField name="treatment_value_id_0"/></div></td>
    </tr>

    </tbody>
</table>