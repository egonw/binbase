<%@ page import="minix.step.ThreatmentPasteDialogStep" %><g:setProvider library="jquery"/>


<g:form controller="pasteDialog">
    <div class="element">

        <div class="header">
            copy/paste matrix
        </div>
        <div class="description">
            please assign the correct columns to your data
        </div>
    </div>
    <div class="element">

        <g:hiddenField name="id" value="${design.id}"/>

        <g:hiddenField name="maxValue" value="${maxValue}"/>

        <table class="dialogTable">
            <thead>
            <tr>
                <th></th>
                <g:each var="current" status="x" in="${0..(maxValue-1)}">
                    <th>
                        <g:if test="${x == 0}">
                            <g:select name="column_${x}" from="${ThreatmentPasteDialogStep.listEnums()}" value="${ThreatmentPasteDialogStep.TREATMENT}"/>
                        </g:if>
                        <g:elseif test="${x == 1}">
                            <g:select name="column_${x}" from="${ThreatmentPasteDialogStep.listEnums()}" value="${ThreatmentPasteDialogStep.TREATMENT_VALUE}"/>
                        </g:elseif>
                        <g:else>
                            <g:select name="column_${x}" from="${ThreatmentPasteDialogStep.listEnums()}" value="${ThreatmentPasteDialogStep.IGNORE}"/>
                        </g:else>
                    </th>
                </g:each>
            </tr>
            </thead>

            <tbody>
            <g:each in="${table.split('\n')}" var="line" status="x">

                <tr id="dialog_row_${x}">
                    <td>

                        <g:remoteLink before="jQuery('#dialog_row_' + ${x}).remove();" update="noTarget" type="button" value="remove" class="remove">remove</g:remoteLink>

                    </td>

                    <%
                        def values = line.split('\t')
                    %>
                    <g:each in="${values}" var="item" status="y">
                        <td>
                            <g:textField name="label_${y}" id="${x}_${y}" value="${item}"/>
                        </td>
                    </g:each>

                    <g:if test="${values.size() < maxValue}">
                        <g:each var="value" in="${values.size() .. maxValue-1}">
                            <td>
                                <g:textField disabled="true" name="label_${value}" id="${x}_${value}" value=""/>
                            </td>
                        </g:each>
                    </g:if>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
    <div class="element">
        <div class="buttons">
            <g:submitToRemote update="page_${pageId}" action="ajaxShowPasteThreatmentDialog" value="save data" class="next"/>
        </div>
    </div>
</g:form>