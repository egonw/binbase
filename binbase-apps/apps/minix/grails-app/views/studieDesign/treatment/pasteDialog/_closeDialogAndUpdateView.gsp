<g:setProvider library="jquery"/>

<div class="message">
    we updated your design with the pasted informations
</div>
<g:javascript>

  $(document).ready(function() {
    //update the class
    $("#dialog-treatment-paste-modal").dialog('close');
    $("#dialog-treatment-paste-modal").dialog("destroy");

    jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'studieDesign',action: 'ajaxRenderTreatmentsForPasteResult',id:design.id)}",success:function(data,textStatus){jQuery('#renderTreatments').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});


    return false;

  });

</g:javascript>