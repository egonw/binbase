<g:setProvider library="jquery"/>

<g:javascript>
  $(document).ready(function() {
    // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
    $("#dialog-treatment-paste-modal").dialog("destroy");

    $("#dialog-treatment-paste-modal").dialog({
      height: 480,
      width: 768,
      modal: true
    });
  });
</g:javascript>

<%
  def pageId = System.currentTimeMillis()
%>
<div id="dialog-treatment-paste-modal" title="Paste treatment data dialog">

  <div id="page_${pageId}">
    <g:form controller="pasteDialog">

      <div class="element">
        <div class="description">please paste your text here. Colums are defined by tabs and each new line defines another treatment</div>
      </div>
      <div class="element">
        <g:textArea class="textarea-full" name="paste" rows="10" cols="200" value="${paste}"/>
      </div>

      <div class="element">
        <div class="buttons">
          <g:submitToRemote action="ajaxShowPasteThreatmentDialog" class="next" update="page_${pageId}" value="upload"/>
        </div>
      </div>

        <g:hiddenField name="pageId" value="${pageId}"/>
        <g:hiddenField name="id" value="${design.id}"/>

    </g:form>
  </div>
</div>