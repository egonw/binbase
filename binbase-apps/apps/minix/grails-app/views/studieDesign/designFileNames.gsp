<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <title>Studie Design Tool</title>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>
  
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'showClass.css')}"/>
  
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

  <div id="error">

  </div>

  <div id="next">
    <g:render template="defineFileNames" model="[design: design,studie:design.generatedStudie]"/>
  </div>
</div>
</body>
</html>
