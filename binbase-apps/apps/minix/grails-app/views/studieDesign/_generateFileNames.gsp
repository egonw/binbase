<g:setProvider library="jquery"/>

<%
  assert design != null, "you need to provide a design object!"
%>
<div class="generateSampleDialog">

  <g:form name="selectAquivitionProperties">

    <div class="header">
      step 6.1 - Fiehnlab file name generation
    </div>

    <div class="description">
      this is an optional step, which generates the standard fiehnlab file identifiers for you.
    </div>

    <span class="textDescription">
      please select your intrument:
    </span>
    <span class="addButton">
      <g:select name="instrument.id"
              from="${design.generatedStudie.architecture.instruments}"
              optionKey="id"/>
    </span>

    <span class="textDescription">
      please select your start date:
    </span>
    <span class="addButton">
      <g:datePicker name="calculationDate" value="${new Date()}" precision="day"
              noSelection="['':'-Choose-']"/>
    </span>

    <div class="textDescription">
    </div>
    <div class="fillField">
      <div class="buttons">
        <g:submitToRemote class="generate" action="ajaxGenerateFileNames" name="ajaxGenerateFileNamesAction" id="ajaxGenerateFileNamesAction" value="start generation" update="next"/>
      </div>
    </div>

    <g:hiddenField name="designId" id="designId" value="${design?.id}"/>

  </g:form>
</div>
