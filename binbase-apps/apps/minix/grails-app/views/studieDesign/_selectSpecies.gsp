<g:setProvider library="jquery"/>

<!-- simple tool to define and select species -->

<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("input")
      .filter(function() {
        return this.id.match(/species.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findSpecies", controller: "autoCompletion")}', dataType:"json"
              }
      );
    })
  });
</g:javascript>
<div class="textField">
    <g:textField name="species_0"/>

</div>

<div class="button-margin-left">
    <g:submitToRemote class="add" action="ajaxRenderSpecies" name="addSpecies" id="addSpecies"
                      value="add additional species" update="selectSpeciesDiv"/>
</div>

<g:each var="specie" in="${species}" status="i">

    <div class="textField">
        <g:textField name="species_${i+1}" value="${specie.name}" />

    </div>
</g:each>