<%
    assert design != null, "you need to provide a 'design' variable for this template!"
%>
<g:setProvider library="jquery"/>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<%@ page import="minix.util.TreatmentUtil; minix.TreatmentSpecific" %>

<div class="design">
    <div class="left">
<g:render template="stepView" model="[design:design]"/>
</div>


<g:javascript>

    function initiateScheduling(session) {

        startValidation(session);
    }

        /**
        * starts the validation process
        * @param session
        */
    function startValidation(session) {
        $(document).ready(function() {

            $("#generateClassProgress").progressbar({value:0});
            $("#generateClassProgressMessage").html("<div class='message'>please wait while we are generating your class</div>");

            $("#error").empty();

            progressTimer = setInterval((function(session) {
                return function() {
                    $.ajax({
                        method: 'get',
                        url : "${g.createLink(controller: 'studieDesign',action: 'ajaxRenderProgress')}/" + session,
                        dataType : 'text',
                        success: function (text) {
                            var value = parseFloat(text);

                            //we are done
                            if (value >= 100) {
                                $("#generateClassProgress").progressbar("value", 100);

                                clearInterval(progressTimer);

                            }

                                //an un exspected error happened
                            else if (value < 0) {
                                clearInterval(progressTimer);
                                $("#generateClassProgress").progressbar("destroy");
                                $("#generateClassProgressMessage").html("<div class='errors'>there was some kind of error...</div>");

                            }
                                //update the progress bar
                            else {
                                $("#generateClassProgress").progressbar("value", value);
                            }
                        }
                    });
                };
            })(session), 250);

        });
    }

</g:javascript>

<div class="left-center-column">
    <g:form name="displayClasses">

        <div class="step-dialog">

        <div class="header">
            step 5 - your generated class matrix
        </div>


        <div class="description">
            This is the estimated class list for the parameters you provided. If this looks correct please select the classes, which you would like to have generated.
        </div>

        <g:render template="/shared/validation_error" model="[errorBean:design]"/>

        <%
            //util to help with calculations
            TreatmentUtil util = new TreatmentUtil()
            //calculate the deepest level of chields
            def level = util.calculateDeepestTreatmentLevel(design) + 1

        %>

        <g:hiddenField name="designId" id="designId" value="${design?.id}"/>

        <!-- colors our table -->
        <g:javascript>

            $(document).ready(function() {
                $('#classesToGenerate  tr:even').addClass('even');
                $('#classesToGenerate  tr:odd').addClass('odd');
            });

        </g:javascript>


                <table id="classesToGenerate" class="button-margin-top">
                    <thead>
                    <tr>
                        <th>generate</th>
                        <th>species</th>
                        <th>organ</th>

                        <g:each var="current" in="${ (0..<level)}">
                            <th>treatment ${current}</th>
                        </g:each>

                        <th>sample count</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="speciesConnection" in="${design.designSpeciesConnections}">
                        <g:each var="organ" in="${speciesConnection.organs}">
                            <g:each var="treatmentSpecific" in="${design.treatmentsSpecifics}">
                                <g:render template="displayClassesTreatments"
                                          model="[design:design,organ:organ,species:speciesConnection.species,treatmentSpecific:treatmentSpecific,level:level]"/>
                            </g:each>
                        </g:each>
                    </g:each>
                    </tbody>
                </table>


        </div>


        <div id="classGeneration">
                <div id="generateClassProgressMessage">

                </div>

                <div id="generateClassProgress">

                </div>

                <div id="error"></div>

            </div>

        <g:set var="sessionId" value="${new Date().time}"/>

        <g:hiddenField name="sessionId" id="sessionId" value="${sessionId}"/>

        <div class="button-margin-top">
            <div class="button-margin-right">
                <input class="check" onclick="$('form input:checkbox').attr('checked', true);" type="button"
                       name="selectAll" value="select all classes"/>
            </div>
            <g:submitToRemote class="next" before="initiateScheduling(${sessionId});" action="ajaxGenerateClasses" name="ajaxGenerateClasses"
                              id="ajaxGenerateClasses" value="generate selected classes" update="next"/>

        </div>

    </g:form>

</div>
</div>