<g:setProvider library="jquery"/>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<div class="design">
    <div class="left">
        <g:render template="stepView" model="[design:design]"/>
    </div>

    <div class="left-center-column">
        <g:form name="designTreatments">

            <div class="step-dialog">

                <div class="header">
                    step 3 - treatment design
                </div>

                <g:render template="/shared/validation_error" model="[errorBean:design]"/>

                <div class="description">
                    we are now defining the treatments we want to use in our studie. Please enter all your treatments and there values!
                </div>

                <div id="selectTreatmentDiv">
                    <g:render template="selectTreatments" model="[design:design]"/>
                </div>

            </div>

            <div class="button-margin-top">
                <g:submitToRemote class="next" action="ajaxSaveTreatments" name="submitToSamples" id="submitToSamples"
                                  value="continue with step 4" update="next"/>
            </div>

            <g:hiddenField name="designId" id="designId" value="${design?.id}"/>

        </g:form>

    </div>
</div>