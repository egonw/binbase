<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<g:setProvider library="jquery"/>

<script type="text/javascript">
    $(document).ready(function () {

        function countdown() {
            if (typeof countdown.counter == 'undefined') {
                countdown.counter = 5; // initial count
            }
            if (countdown.counter > 0) {
                document.getElementById('count').innerHTML = countdown.counter--;
                setTimeout(countdown, 1000);
            }
            else {
                location.href = "${g.createLink(controller: "studie",action: "show",id:design.generatedStudie.id)}";
            }
        }

        countdown();

    });
</script>

<div class="design">
    <div class="left">
        <g:render template="stepView" model="[design: design]"/>
    </div>

    <div class="left-center-column">

        <div>

            congratulations, your study design is done and you will
            be forwarded in <span id="count"></span> seconds to your study for further actions.

            <p>
                If this page doesn't change, please press
                <g:link controller="studie" action="show"
                        id="${design.generatedStudie.id}">this link</g:link> to continue.

            </p>
        </div>
    </div>
</div>