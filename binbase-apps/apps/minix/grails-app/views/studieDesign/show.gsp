
<%@ page import="minix.StudieDesign" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'studieDesign.label', default: 'StudieDesign')}"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>



<div class="body">

  <div class="center-full-page">

    <div class="header"><g:message code="default.show.label" args="[entityName]"/></div>
    <g:if test="${flash.message}">
      <div class="message">${flash.message}</div>
    </g:if>

    <div class="scaffoldingProperties">
      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.id.label" default="Id"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: studieDesignInstance, field: "id")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.title.label" default="Title"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: studieDesignInstance, field: "title")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.generatedStudie.label" default="Generated Studie"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <g:if test="${studieDesignInstance?.generatedStudie != null}">
          <g:link class="showDetails" controller="studie" action="show" id="${studieDesignInstance?.generatedStudie?.id}">${studieDesignInstance?.generatedStudie?.encodeAsHTML()}</g:link>
        </g:if>

        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.step.label" default="Step"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${studieDesignInstance?.step?.encodeAsHTML()}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.architecture.label" default="Architecture"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <g:if test="${studieDesignInstance?.architecture != null}">
          <g:link class="showDetails" controller="architecture" action="show" id="${studieDesignInstance?.architecture?.id}">${studieDesignInstance?.architecture?.encodeAsHTML()}</g:link>
        </g:if>

        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.designSpeciesConnections.label" default="Design Species Connections"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${studieDesignInstance.designSpeciesConnections}" var="d">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="designSpeciesConnection" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.numberOfSamples.label" default="Number Of Samples"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: studieDesignInstance, field: "numberOfSamples")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.speciees.label" default="Speciees"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${studieDesignInstance.speciees}" var="s">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="species" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.treatmentsSpecifics.label" default="Treatments Specifics"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${studieDesignInstance.treatmentsSpecifics}" var="t">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="treatmentSpecific" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="studieDesign.user.label" default="User"/>
      </span>

      <span class="scaffoldingPropertyValue">

        <g:if test="${studieDesignInstance?.user != null}">
          <g:link class="showDetails" controller="shiroUser" action="show" id="${studieDesignInstance?.user?.id}">${studieDesignInstance?.user?.encodeAsHTML()}</g:link>
        </g:if>

      </span>

      
    </div>
  </div>
</div>
</body>
</html>
