<g:setProvider library="jquery"/>

<%
  assert treatmentSpecific != null, "you need to provide a variable with the name 'treatmentSpecific'"
%>


<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("#sub_treatment_0")
      .autocomplete({
              source: '${g.createLink(action: "findTreatment", controller: "autoCompletion")}', dataType:"json"
              }
      );
    })
  });
</g:javascript>

<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("#sub_treatment_value_0")
      .autocomplete({
              source: '${g.createLink(action: "findTreatmentValue", controller: "autoCompletion")}', dataType:"json"
              }
      );
    })
  });
</g:javascript>


<div class="defineSubTreatment">
  <div class="header">
    step 3.1 - please define a sub treatment for ${treatmentSpecific.toParentString()}
  </div>
  <div class="description">
    please define the subtreatment using the fields below and press 'save' to finalize this change.
  </div>

  <div class="topSpacer">

  </div>
  <table>
    <thead>
    <tr>
      <th>name</th>
      <th>specific</th>
      <th></th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td><g:textField name="sub_treatment_name_0" id="sub_treatment_0"/>
      </td>
      <td><g:textField name="sub_treatment_value_0" id="sub_treatment_value_0"/>
      </td>
      <td>

        <g:hiddenField name="sub_treatment_parent_id_0" id="sub_treatment_parent_id_0" value="${treatmentSpecific.id}"/>

        <div class="buttons">
          <g:submitToRemote class="add" action="ajaxSaveSubtreatment" name="saveSubTreatment" id="saveSubTreatment" value="save" update="selectTreatmentDiv"/>
        </div>

      </td>
    </tr>
    </tbody>
  </table>
</div>