<g:setProvider library="jquery"/>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>


<div class="design">
    <div class="left">
        <g:render template="stepView" model="[design:design]"/>
    </div>

    <div class="left-center-column">
        <g:form name="designOrgans">
            <div class="step-dialog">

                <div class="header">step 2 - organ design</div>

                <div class="description">please define all the organs in your studie for each species. You need to define at least one organ for each species</div>


                <g:render template="/shared/validation_error" model="[errorBean:design]"/>

                <div class="description"><p>Defined species for this study</p></div>

                <g:each status="x" var="specie" in="${design.speciees}">

                    <div class="standardWidth">
                        <div class="content-box">
                            <div class="content-header">${specie}</div>
                            <g:hiddenField name="species_${x+1}" id="species_${x+1}" value="${specie.name}"/>

                            <div class="content-inner-content">
                                <div class="speciesOrgans">
                                    <g:render template="selectOrgans" model="[specie:specie,design:design]"/>
                                </div>

                            </div>
                        </div>
                    </div>
                </g:each>

                <g:hiddenField name="designId" id="designId" value="${design?.id}"/>

            </div>

            <div class="button-margin-top">

                <g:submitToRemote class="next" action="ajaxSaveOrgans" name="submitToTreatments"
                                  id="submitToTreatments" value="continue with step 3" update="next"/>

            </div>
        </g:form>
    </div>
</div>