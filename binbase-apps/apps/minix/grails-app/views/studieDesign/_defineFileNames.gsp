<%
    assert design != null, "you need to provide a design object!"
%>
<g:setProvider library="jquery"/>


<div class="design">
    <div class="left">
        <g:render template="stepView" model="[design:design]"/>
    </div>
    <g:form name="defineFileNames" action="designDone">

        <div class="left-center-column">
            <div class="step-dialog">

                <div class="header">
                    step 6 - class modification
                </div>

                <div class="description">
                    modify these classes to fit your exact experimental design. Once done, please press save to finalize your design.
                </div>

                <g:render template="/shared/validation_error" model="[errorBean:design]"/>

                <div class="topSpacer"></div>



                <g:hiddenField name="designId" id="designId" value="${design?.id}"/>
                <g:hiddenField name="studieId" id="studieId" value="${design.generatedStudie.id}"/>

                <g:each var="clazz" in="${design.generatedStudie.classes}">
                    <g:render template="/experimentClass/displayClass" model="[clazz:clazz]"/>
                </g:each>

            </div>

            <div class="button-margin-top">
                <g:submitButton class="next" name="designDone" value="save and display generated studie"/>
            </div>
        </div>
    </g:form>

</div>