<g:setProvider library="jquery"/>

<!-- recrusive function -->
<%
  assert treatmentSpecific != null, "you need to provide a treatment specific"
  assert level != null, "you need to provide a level"
%>


<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("input")
      .filter(function() {
        return this.id.match(/treatment_sub_name_.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findTreatment", controller: "autoCompletion")}', dataType:"json"
              }
      );
    })
  });
</g:javascript>

<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("input")
      .filter(function() {
        return this.id.match(/treatment_sub_value_.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findTreatmentValue", controller: "autoCompletion")}', dataType:"json"
              }
      );
    })
  });
</g:javascript>

<g:if test="${treatmentSpecific.subTreatmentSpecifics != null}">
  <g:each var="subTreatment" in="${treatmentSpecific.subTreatmentSpecifics}">

    <%
      def i = subTreatment.id
    %>
    <tr>
      <td></td>
      <td>
        <div class="${hasErrors(bean: subTreatment.treatment, field: 'description', 'errors')}">
          <g:each in="${ (0..<level)}">
            >
          </g:each>


          <g:textField name="treatment_sub_name_${i}" value="${ subTreatment.treatment.description}" class="sub"/>

          <g:javascript>
            $(function() {
            $("#treatment_sub_name_${i}").observe_field(1, function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'treatment',action: 'ajaxSetDescription',id:subTreatment.treatment.id)}",success:function(data,textStatus){jQuery('#action_result').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });
          </g:javascript>
        </div>
      </td>
      <td>
        <div class="${hasErrors(bean: subTreatment, field: 'value', 'errors')}">
          <g:textField name="treatment_sub_value_${i}" value="${subTreatment.value}" />
          <g:hiddenField name="treatment_sub_value_id_${i}" id="treatment_sub_value_id_${i}" value="${subTreatment.id}"/>

          <g:javascript>
            $(function() {
            $("#treatment_sub_value_${i}").observe_field(1, function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'treatmentSpecific',action: 'ajaxSetValue',id:subTreatment.id)}",success:function(data,textStatus){jQuery('#action_result').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });
          </g:javascript>
        </div>
      </td>
      <td>
        <span class="buttons">
          <g:submitToRemote class="add" action="ajaxDefineSubtreatment" name="addSubTreatment_${i}" id="addSubTreatment_${i}" value="define a sub treatment" update="subTreatment"/>
        </span>
      </td>
      <td>
        <span class="buttons">
          <g:submitToRemote class="remove" action="ajaxRemoveTreatment" name="rmTreatment_${i}" id="rmTreatment_${i}" value="remove" update="next"/>
        </span>

      </td>
    </tr>

    <g:if test="${treatmentSpecific != null}">
      <g:if test="${treatmentSpecific.subTreatmentSpecifics != null}">
        <g:if test="${treatmentSpecific.subTreatmentSpecifics.size() > 0}">

          <g:render template="selectSubTreatment" model="[treatmentSpecific:subTreatment,level:level+1]"/>
        </g:if>
      </g:if>
    </g:if>
  </g:each>

</g:if>