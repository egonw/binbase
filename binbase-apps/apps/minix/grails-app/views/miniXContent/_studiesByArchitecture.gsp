<div class="header">Studies by Architecture</div>

<div class="element">
    <div class="graph-container">
        <div class="placeholder" id="count_by_architecture">
            <div class="message">please wait, we are loading the required data...</div>
        </div>
    </div>
</div>

<g:javascript>
	function labelFormatter(label, series) {
		return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
	}

$(document).ready(function(){
    $.ajax(
            {
                url:"${g.createLink(controller: 'miniXContent', action: 'ajaxRenderStudiesByArchitecture')}",
                type:"GET",
                dataType:"json",
                success: function (myJsonData){


                        var selector = "#count_by_architecture";

                        var d = myJsonData;

                        var plot = $.plot(selector, d,{
		                    series: {
			                  pie: {
				                    show: true,
			   	                    radius: 1,

                                    label: {
                                        show: true,
                                        radius: 2/3,
                                        threshold: 0.02,
                                        formatter: function labelFormatter(label, series) {
		                                    return "<div  style='font-size:8pt; text-align:center; padding:2px; color:white;'>"
                                                        + label + "<br/>" + Math.round(series.data[0][1]) +
                                                    "</div>";
	                                    },

                                        background: {
                                            opacity: 0.8
                                        }
                                    }
			                    }
		                    },
		                    legend: {
		                        show:false
		                    }
	                    });

                }
            }
    );
    });

</g:javascript>