<g:setProvider library="jquery"/>

<%
    def id = System.currentTimeMillis()
%>
<g:javascript>
    $(document).ready(function() {
        // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
        $("#edit-modal-${value.id}-${id}").dialog("destroy");

        $("#edit-modal-${value.id}-${id}").dialog({
            height: 500,
            width: 700,
            modal: true,
              closeOnEscape: false,
   open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
        });
    });
</g:javascript>

<g:javascript>

/**
* closes the dialog
*/
function closeDialog(){

  $(document).ready(function() {
    //close the dialog
    $("#edit-modal-${value.id}-${id}").dialog('close');

    $("#edit-modal-${value.id}-${id}").dialog('destroy');
  });
}
</g:javascript>

<div id="edit-modal-${value.id}-${id}" title="update name">

<g:form action="ajaxSaveName">

    <div class="description">please enter your new name below</div>

    <g:hasErrors bean="${value}">
            <div class="errors">
                <g:renderErrors bean="${value}" as="list" />
            </div>
            </g:hasErrors>

    <div class="button-margin-top">
    <g:textArea class="textarea-full" cols="120" rows="2" name="title" value="${value.name}"/>
    </div>

   <div class="button-margin-top">
    <g:submitToRemote before="closeDialog();" action="ajaxSaveName"
                      name="submit" value="save" class="save" update="updateName"
                      after="closeDialog();"/>
    </div>

    <g:hiddenField name="id" value="${value.id}"/>
</g:form>
</div>