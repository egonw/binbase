<%@ page import="minix.Kingdom" %>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <title>Kingdom List</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

  <div class="design">

    <div class="left">
      <div class="header">

        Information

      </div>

      <div class="left_information_box">
        <p><g:message code="description.kingdom"/></p>
        <div class="topSpacer"></div>
               <p><g:message code="description.kingdom.list"/></p>

      </div>

    </div>
    <div class="left-center-column"><div class="header">Kingdom List</div>
      <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
      </g:if>
      <div class="element">

        <table>
          <thead>
          <tr>

            <g:sortableColumn property="id" title="Id"/>

            <g:sortableColumn property="name" title="Name"/>

          </tr>
          </thead>
          <tbody>
          <g:each in="${kingdomInstanceList}" status="i" var="kingdomInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

              <td><g:link action="show" id="${kingdomInstance.id}">${kingdomInstance.id?.encodeAsHTML()}</g:link></td>

              <td>${kingdomInstance.name?.encodeAsHTML()}</td>

            </tr>
          </g:each>
          </tbody>
        </table>
        <div class="paginateButtons">
          <g:paginate total="${Kingdom.count()}"/>
        </div>
      </div>
    </div>

  </div>
</div>
</body>
</html>

