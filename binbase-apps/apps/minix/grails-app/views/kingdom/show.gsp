<%@ page import="minix.Kingdom" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'kingdom.label', default: 'Kingdom')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<g:render template="/shared/navigation"/>



<div class="body">

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="design">

        <div class="left-thrirty">

            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.kingdom"/></p>
            </div>

            <div class="topSpacer"></div>

            <div class="header">
                Kingdom details
            </div>

            <div class="scaffoldingProperties">

                <span class="scaffoldingPropertyLabel">
                    <g:message code="kingdom.name.label" default="Name"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    <span id="updateName"
                          class="button-float-right">${fieldValue(bean: kingdomInstance, field: "name")}</span><g:remoteLink
                        class="edit" action="ajaxEditName"
                        update="updateName" id="${kingdomInstance.id}"><span
                            class="hideText">edit</span></g:remoteLink>
                </span>

            </div>
        </div>

        <div class="right-seventy left-side-border">
            <div class="header">
                Associated Studies
            </div>

            <div class="element">
                <table>
                    <thead>
                    <tr>
                        <g:sortableColumn property="id" title="Id"/>
                        <g:sortableColumn property="description" title="Description"/>
                        <th>Architecture</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${studieInstanceList}" status="i" var="studieInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td><g:link controller="studie" action="show"
                                        id="${studieInstance.id}">${studieInstance.id?.encodeAsHTML()}</g:link></td>

                            <td>${studieInstance.description?.encodeAsHTML()}</td>

                            <td>${studieInstance.architecture?.encodeAsHTML()}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="paginateButtons">
                    <g:paginate id="${kingdomInstance.id}" action="show" controller="kingdom" total="${studieTotal}"
                                offset="${session.studiePagination?.offset}" params="${[paginate:'Studie']}"/>
                </div>
            </div>

            <div class="header">
                Associated Species
            </div>

            <div class="element">
                <table>
                    <thead>
                    <tr>
                        <g:sortableColumn property="id" title="Id"/>
                        <g:sortableColumn property="name" title="Name"/>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${species}" status="i" var="speciesInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td><g:link controller="species" action="show"
                                        id="${speciesInstance.id}">${speciesInstance.id?.encodeAsHTML()}</g:link></td>

                            <td>${speciesInstance.name?.encodeAsHTML()}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="paginateButtons">
                    <g:paginate id="${kingdomInstance.id}" action="show" controller="kingdom" total="${speciessTotal}"
                                max="15" offset="${session.speciesPagination?.offset}"
                                params="${[paginate:'Species']}"/>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
