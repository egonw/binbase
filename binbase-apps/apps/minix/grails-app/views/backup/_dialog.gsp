<g:if test="${flash.errorMessage}">
  <div class="errors">${flash.errorMessage}</div>
</g:if>


<g:javascript>

  function backupStart() {
    $(document).ready(function() {

      $("#messageBox").html("<div class='message'>please wait we are backing up the system...</div>");
    });
  }
</g:javascript>

<div id="messageBox">

</div>
<g:form>
  <div class="header">Backup MiniX Database</div>

  <div class="description">this utilitiy generates of backup of the minix database.</div>


  <div class="textDescription topSpacer">
    please provide a directory name, otherwise we will use the servers temporaery directory
  </div>

  <div class="fillField">
    <g:textField name="directory" value='${System.getProperty("java.io.tmpdir")}'/>
  </div>

  <div class="textDescription topSpacer">
    would you like to have the data compressed?
  </div>

  <div class="fillField">
    <g:checkBox name="compress" checked="false"/>
  </div>

  <div class="topSpacer"></div>
  <span class="myButton">
    <g:submitToRemote before="backupStart();" update="next" action="executeBackup_ajax" value="backup" class="next"/>
  </span>

</g:form>
