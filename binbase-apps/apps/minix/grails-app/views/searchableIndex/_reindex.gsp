<g:setProvider library="jquery"/>

<g:if test="${flash.errorMessage}">
  <div class="errors">${flash.errorMessage}</div>
</g:if>

<g:javascript>

  function reindexStart() {
    $(document).ready(function() {

      $("#messageBox").html("<div class='message'>please wait we are updating the index now...</div>");
    });
  }
</g:javascript>

<div class="header">ReIndex the database</div>

<div class="description">this utilitiy regenerates the complete index for this system</div>

<div id="messageBox">

</div>
<g:form>

  <div class="textDescription topSpacer">
    are you sure you want to reindex the complete database? This operation can take a very long time!
  </div>

  <div class="topSpacer"></div>
  <span class="myButton">
    <div class="buttons">

      <g:submitToRemote update="messageBox" before="reindexStart();" action="reindexExecute_ajax" value="reindex database" class="next"/>
    </div>
  </span>

</g:form>
