<div class="element">
    <table>
        <thead>
        <tr>
            <th>Name</th>
            <th>BinId</th>
            <th>Retention Index</th>
            <th>Minimum Apex Sn</th>
            <th>Minimum Qualifier Ratio</th>
            <th>Maximum Qualifier Ratio</th>
            <th>Qualifier Ion</th>
            <th>Minimum Distance Ratio</th>
            <th>Maximum Distance Ratio</th>
            <th>Required</th>
            <th>Minimum Similarity</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${result}" var="entry">
            <tr>
                <td>${entry.name}</td>
                <td>${entry.binId}</td>
                <td>${entry.retentionIndex}</td>
                <td>${entry.apexSn}</td>
                <td>${entry.minRatio}</td>
                <td>${entry.maxRatio}</td>
                <td>${entry.qualifier}</td>
                <td>${entry.minDistanceRatio}</td>
                <td>${entry.maxDistanceRatio}</td>
                <td>${entry.required}</td>
                <td>${entry.similarity}</td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>