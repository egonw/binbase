<g:each in="${standards}" var="standard">


    <div id="${study.id}_${standard.id}"></div>

    <g:javascript>
                    var selector = "#${study.id}_${standard.id}";
                    $(selector).load("${g.createLink(action: "ajaxShowRetentionIndexMarkerPerformanceByStudyAndStandard", controller: "retentionIndex", params:[study: study.id, standard: standard.id,standard_name:standard.name,mode:mode])}");
    </g:javascript>
</g:each>