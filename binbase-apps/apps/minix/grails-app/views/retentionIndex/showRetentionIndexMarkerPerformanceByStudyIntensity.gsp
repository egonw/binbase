<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'retentionindex.css')}"/>

    <title>Home</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<g:javascript library="flot/jquery.flot"/>
<g:javascript library="flot/jquery.flot.selection"/>
<g:javascript library="flot/jquery.flot.time"/>


<div class="body">

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="design">
        <div class="center-full-page">

            <div class="left-thrirty">

                <div class="header" id="id_top">
                    Intensity Performance Report
                </div>

                <div class="element">
                    <div class="description">This page displays the overal intensity of the markers over time, used by the retention index correction in this study and provides you with a good estimate about the stability of your system.
                    </div>
                </div>

                <div class="header">Operations</div>

                <div class="element">
                    <g:link class="list" controller="studie" action="show"
                            id="${study.id}">show study</g:link>
                </div>
            </div>

            <div class="right-seventy">

                <!-- if there is only one column just execute the statement -->
                <g:javascript>
                jQuery.ajax({type:'POST', url:'${g.createLink(controller: 'retentionIndex', action: 'ajaxShowRetentionIndexMarkerPerformanceByStudy')}',data:{mode:'intensity','id':${study.id}},success:function(data,textStatus){jQuery('#next').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});

                </g:javascript>


                <div class="element">
                    <div id="next">
                    </div>

                </div>

            </div>
        </div>
    </div>

</div>
</body>
</html>
