<div class="element">

    <div class="textDescription">
        Database: ${db}
    </div>

    <div class="element">
        <table>
            <thead>
            <tr>
                <th>
                    sample name
                </th>
                <th>
                    class name
                </th>
                <th>
                    study
                </th>


            </tr>
            </thead>
            <tbody id="db_${db}">
            </tbody>
        </table>
    </div>
</div>
<g:javascript>

           $(document).ready(function() {
                //render class to speed up the intial load of very very large studies...
                var selector = "#db_${db}";
                $(selector).load(
                    "${g.createLink(controller: 'retentionIndex', action: 'ajaxRenderResultsForDataBase', id: db)}"
                );
           });

</g:javascript>