<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'retentionindex.css?id=1')}"/>

    <title>Home</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<g:javascript library="flot/jquery.flot"/>
<g:javascript library="flot/jquery.flot.selection"/>
<g:javascript library="flot/jquery.flot.time"/>

<div class="body">
    <div class="design">

        <div class="center-full-page">

            <div class="element">

                <div class="header" id="id_top">
                    Bin Performance Report
                </div>

                <div class="description">This page allows you to monitor the performance of a specific bin for a certain date range and instrument</div>
            </div>

            <div class="element">

                <div class="textDescription">

                    <g:form action="ajaxShowBinPerformanceByDB" controller="retentionIndex">

                        <div class="element">
                            <ul class="retention-form">
                                <%
                                    def from = Calendar.getInstance()
                                    from.roll(Calendar.MONTH, false)
                                    from = from.time

                                %>
                                <li>
                                    <label for="from">From:</label>

                                    <input type="text" id="from" value="${from.format("MM/dd/yy")}" name="from"/>
                                </li>
                                <li>
                                    <label for="to">To:</label>

                                    <input type="text" id="to" value="${new Date().format("MM/dd/yy")}" name="to"/>
                                </li>

                                <li>
                                    <label for="bin">Bin Id:</label>

                                    <input type="text" id="bin" name="bin"/>
                                </li>

                                <li>
                                    <label for="ion">Ion:</label>

                                    <input type="text" id="ion" name="ion"/>
                                </li>

                                <li>
                                    <label for="sample">Sample Pattern:</label>

                                    <input type="text" id="sample" name="sample" value=".*"/>
                                </li>

                            </ul>
                        </div>

                        <div class="element">
                            <ul class="retention-form">

                                <g:if test="${columns.size() == 1}">
                                    <g:hiddenField name="db" id="db"
                                                   value="${columns[0]}"/>
                                </g:if>

                                <g:else>
                                    <li>
                                        <label for="db">Database:</label>

                                        <g:select name="db" id="db" from="${columns}"/>

                                    </li>

                                </g:else>
                                <g:if test="${instruments.size() == 1}">
                                    <g:hiddenField name="instrument" id="instrument"
                                                   value="${instruments[0].identifier}"/>
                                </g:if>

                                <g:else>
                                    <li>
                                        <label for="instrument">Instrument:</label>

                                        <g:select name="instrument" id="instrument" from="${instruments}"
                                                  optionKey="identifier"/>

                                    </li>
                                </g:else>


                                <li>
                                    <g:submitToRemote class="next" value="generate report" update="next"
                                                      action="ajaxShowBinPerformanceByDB"/>
                                </li>
                            </ul>
                        </div>
                    </g:form>

                    <g:javascript>
                        $(document).ready(function () {
                            $("#from").datepicker({
                                changeMonth: true,
                                numberOfMonths: 3,
                                onClose: function (selectedDate) {
                                    $("#to").datepicker("option", "minDate", selectedDate);
                                }
                            });
                            $("#to").datepicker({
                                changeMonth: true,
                                numberOfMonths: 3,
                                onClose: function (selectedDate) {
                                    $("#from").datepicker("option", "maxDate", selectedDate);
                                }
                            });
                        });

                    </g:javascript>
                </div>

            </div>


            <div class="element">
                <div id="next">
                </div>

            </div>

        </div>
    </div>

</div>
</body>
</html>
