<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'retentionindex.css')}"/>

    <title>Home</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<g:javascript library="flot/jquery.flot"/>
<g:javascript library="flot/jquery.flot.selection"/>
<g:javascript library="flot/jquery.flot.time"/>


<div class="body">
    <div class="design">

        <div class="center-full-page">

            <div class="element">

                <div class="header" id="id_top">
                    Signal Noise Performance Report
                </div>

                <div class="description">This page displays the signal noise of the retention index markers over time and provides you with an estimate about the stability of your system.
                </div>
            </div>

            <div class="element">


                    <div class="textDescription">

                        <g:form action="ajaxShowRetentionIndexMarkerPerformanceByDB" controller="retentionIndex">

                            <ul class="retention-form">
                                <%
                                    def from = Calendar.getInstance()
                                    from.roll(Calendar.MONTH, false)
                                    from = from.time

                                %>
                                <li>
                                    <label for="from">From:</label>

                                    <input type="text" id="from" value="${from.format("MM/dd/yy")}" name="from"/>
                                </li>
                                <li>
                                    <label for="to">To:</label>

                                    <input type="text" id="to" value="${new Date().format("MM/dd/yy")}" name="to"/>
                                </li>

                                <li>
                                    <label for="id">Database:</label>

                                    <g:select name="id" id="id" from="${columns}"/>

                                </li>

                            </ul>
                            <ul class="retention-form">
                                <li>
                                    <g:submitToRemote class="next" value="generate report" update="next"
                                                      action="ajaxShowRetentionIndexMarkerPerformanceByDB"/>
                                </li>
                            </ul>
                        </g:form>

                        <g:javascript>
                            $(document).ready(function () {
                                $("#from").datepicker({
                                    changeMonth: true,
                                    numberOfMonths: 3,
                                    onClose: function (selectedDate) {
                                        $("#to").datepicker("option", "minDate", selectedDate);
                                    }
                                });
                                $("#to").datepicker({
                                    changeMonth: true,
                                    numberOfMonths: 3,
                                    onClose: function (selectedDate) {
                                        $("#from").datepicker("option", "maxDate", selectedDate);
                                    }
                                });
                            });

                        </g:javascript>
                    </div>

            </div>


            <div class="element">
                <div id="next">
                </div>

            </div>

        </div>
    </div>

</div>
</body>
</html>
