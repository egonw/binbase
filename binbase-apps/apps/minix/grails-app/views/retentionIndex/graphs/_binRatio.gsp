<style type="text/css">
.graph-container {
    box-sizing: border-box;
    width: 95%;
    height: 250px;
    padding: 20px 15px 15px 15px;
    margin: 15px auto 30px auto;
    border: 1px solid #ddd;
    background: #fff;
}

.placeholder {
    width: 100%;
    height: 100%;
    font-size: 14px;
    line-height: 1.2em;
}

</style>

<g:javascript>
    function weekendAreas(axes) {

        var markings = [],
                d = new Date(axes.xaxis.min);

        // go to the first Saturday

        d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
        d.setUTCSeconds(0);
        d.setUTCMinutes(0);
        d.setUTCHours(0);

        var i = d.getTime();

        // when we don't set yaxis, the rectangle automatically
        // extends to infinity upwards and downwards

        do {
            markings.push({ xaxis: { from: i, to: i + 2 * 24 * 60 * 60 * 1000 } });
            i += 7 * 24 * 60 * 60 * 1000;
        } while (i < axes.xaxis.max);

        return markings;
    }

    var options = {
        xaxis: {
            mode: "time",
            tickLength: 5
        },
        selection: {
            mode: "x"
        },
        grid: {
            markings: weekendAreas
        },
        legend: {
            show: false
        },

        series: {
            points: {
                radius: 2,
                show: true,
                fill: true
            }
        }
    };

</g:javascript>

<g:if test='${mode == "intensity"}'>
    <g:javascript>

        function generateUrl(){
            return "${g.createLink(controller: 'miniXContent', action: 'ajaxRenderChartByDateAndDatabaseAndInstrumentForBinRatio', params: [column: column, instrument: instrument, first: bin_first, second:bin_second, from: from, to: to,pattern:pattern])}";
        }
    </g:javascript>
</g:if>

<div>

    <div class="element">
        <div class="header">Quantmass ratio for bin ${bin_first} and ${bin_second}</div>
        <div class="stepTitle">Instrument: ${instrument}</div>
        <div class="stepTitle">Sample Pattern: ${pattern}</div>
        <div class="stepTitle">From: ${new Date(from).format("MM/dd/yy")}</div>
        <div class="stepTitle">To: ${new Date(to).format("MM/dd/yy")}</div>



        <quicklink:message url="${quickLink}"/>


        <div class="graph-container">
            <div class="placeholder" id="chart_${column}_${instrument}_${bin_first}">
                <div class="message">please wait, we are loading the required data...</div>
            </div>
        </div>

        <div class="graph-container" style="height:100px;">
            <div class="placeholder" id="overview_chart_${column}_${instrument}_${bin_first}"></div>
        </div>

    </div>
    <g:javascript>

//this creates the actual graph

$(document).ready(function() {
                var selector = "#chart_${column}_${instrument}_${bin_first}";
                var selector2 = "#overview_chart_${column}_${instrument}_${bin_first}";

                var url =generateUrl();

                $.ajax(
                    {
                        url:url,
                        type:"GET",
                        dataType:"json",
                        success: function (myJsonData){


	$(function() {

		var d = myJsonData;

		var plot = $.plot(selector, d, options);

		var overview = $.plot(selector2, d, {
			series: {
				points: {
                radius: 1,
                show: true,
                fill: true,
                fillColor: "#0051DD"
            },
				shadowSize: 0
			},
			xaxis: {
				ticks: [],
				mode: "time"
			},
			yaxis: {
				ticks: [],
				min: 0,
				autoscaleMargin: 0.1
			},
			selection: {
				mode: "x"
			} ,
			legend:{
			show:false
			}
		});

		// now connect the two

		$(selector).bind("plotselected", function (event, ranges) {

			// do the zooming

			plot = $.plot(selector, d, $.extend(true, {}, options, {
				xaxis: {
					min: ranges.xaxis.from,
					max: ranges.xaxis.to
				}

			}));

			// don't fire event on the overview to prevent eternal loop

			overview.setSelection(ranges, true);
		});

		$(selector2).bind("plotselected", function (event, ranges) {
			plot.setSelection(ranges);
		});

	});


                        }
                    }
                );

            });
    </g:javascript>
</div>