<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'memberHome.css')}"/>

    <title>Home</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">
    <div class="design" >

        <div class="left">
            <div class="element">

                This page will provide you with information about recently failed retention index corrections in your BinBase database system
            </div>
        </div>

        <div class="left-center-column">
            <div id="next">
                <div class="header">
                    This reports shows recently failed samples in your BinBase database
                </div>

                <g:each in="${columns}" var="db">
                    <g:render template="failedCorrection" model="[db: db]"/>
                </g:each>
            </div>

        </div>
    </div>

</div>
</body>
</html>
