<g:setProvider library="jquery"/>

<g:if test="${flash.errorMessage}">
    <div class="errors">${flash.errorMessage}</div>
</g:if>


<g:javascript>

    function importStart() {
        $(document).ready(function() {

            $("#messageBox").html("<div class='message'>please wait we are importing the data now...</div>");
        });
    }
</g:javascript>

<%
    String id = System.currentTimeMillis()
%>
<div id="messageBox">

</div>
<g:form>
    <div class="header">Import existing SetupX Dump</div>

    <div class="description">this utilitiy imports an existing SetupX xml dump</div>


    <div class="textDescription topSpacer">
        please enter your directory path containing the xml files. This directory needs to be on the server!
    </div>

    <div class="fillField">
        <g:textField name="directory"/>
    </div>

    <div class="myButton">
        <div class="buttons">

            <g:hiddenField name="id" value="${id}"/>
            <g:submitToRemote before="importStart();" name="importDumpfile" update="next" action="importDump_ajax" value="import data" class="next"/>
        </div>
    </div>

</g:form>
<g:jprogressDialog progressId="${id}" message="please wait for the import to finish" trigger="importDumpfile"/>
