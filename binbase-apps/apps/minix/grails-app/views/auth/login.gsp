<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'memberHome.css')}"/>

    <title>Login</title>
</head>

<body>
<div class="design">
    <div class="center-full-page">
        <g:if test="${flash.message}">
            <div id="searchView">

                <div class="message">${flash.message}</div>
            </div>
        </g:if>
        <div class="element">
            <g:render template="login"/>
        </div>
    </div>
</div>
</body>
</html>
