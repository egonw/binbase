<%@ page import="minix.ShiroUser" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>Logged in Users</title>
    <meta name="layout" content="main"/>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

</head>
<body>

<g:javascript src="jquery/jquery.observe_field.js"/>
<g:render template="/shared/navigation"/>

<div class="body">
    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.user"/></p>
            </div>

        </div>
        <div class="left-center-column">

            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>

            <div class="header">Currently logged in users</div>
            <div class="element">

                <g:render template="listLoginsTemplate" model="[shiroUserInstanceList:logins]"/>

            </div>
            <div class="element">
                <g:link controller="auth" action="listLogins" class="refresh">refresh</g:link>
            </div>

        </div>
    </div>
</div>
</body>
</html>