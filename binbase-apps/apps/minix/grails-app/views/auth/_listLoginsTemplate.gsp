<%@ page import="minix.ShiroUser" %>
<table>
    <thead>
    <tr>
        <g:sortableColumn property="username" title="Username"/>
    </tr>
    </thead>
    <tbody>
    <g:each in="${shiroUserInstanceList}" status="i" var="shiroUserInstance">
        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

            <td><g:link controller="shiroUser" action="show" id="${shiroUserInstance.id}">${shiroUserInstance.username?.encodeAsHTML()}</g:link></td>

        </tr>
    </g:each>
    </tbody>
</table>
