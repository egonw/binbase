<%@ page import="org.apache.shiro.SecurityUtils" %>
<div class="nav collectonme" id="navigation-bar">
  <span class="menuButton">
    <g:link controller="home" class="home">user home</g:link>
  </span>

    <!--
  <span class="menuButton">
    <g:link controller="search" action="search" class="search">search</g:link>
  </span>

  -->

  <span class="menuButton">
    <g:link controller="studie" action="list" class="list">list studies</g:link>
  </span>
  <span class="menuButton">
      <a class="bug" href="http://code.google.com/p/minix/issues/list" target="_blank">report issue</a>
  </span>
  <span class="menuButton" id="logout-button">
    <g:link class="logout" controller="auth" action="signOut">logout</g:link>
  </span>


    <span class="user">
        logged in: ${SecurityUtils.subject.principal}
    </span>
</div>