
<div class="header">MiniX Exception</div>
<div class="description">sorry, there happened to be some error during your request. Please be so nice and create a<a href="http://code.google.com/p/minix/issues/list" target="_blank"> new issue </a>in our tracking system. So that we can avoid this in the future </div>

  <div class="exceptions">
    <strong>Error ${request.'javax.servlet.error.status_code'}:</strong> ${request.'javax.servlet.error.message'.encodeAsHTML()}<br/>
    <strong>Servlet:</strong> ${request.'javax.servlet.error.servlet_name'}<br/>
    <strong>URI:</strong> ${request.'javax.servlet.error.request_uri'}<br/>
    <g:if test="${exception}">
          <strong>Exception Message:</strong> ${exception.message?.encodeAsHTML()} <br />
          <strong>Caused by:</strong> ${exception.cause?.message?.encodeAsHTML()} <br />
          <strong>Class:</strong> ${exception.className} <br />
          <strong>At Line:</strong> [${exception.lineNumber}] <br />
          <strong>Code Snippet:</strong><br />
          <div class="snippet">
              <g:each var="cs" in="${exception.codeSnippet}">
                  ${cs?.encodeAsHTML()}<br />
              </g:each>
          </div>
    </g:if>
  </div>
<g:if test="${exception}">
    <h2>Stack Trace</h2>
    <div class="stack">
      <pre><g:each in="${exception.stackTraceLines}">${it.encodeAsHTML()}<br/></g:each></pre>
    </div>
</g:if>
