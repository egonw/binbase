<g:hasErrors bean="${errorBean}">
  <div class="errors">
    <g:if test="${message != null}">
      <p>${message}</p>
    </g:if>
    <g:renderErrors bean="${errorBean}" as="list"/>
  </div>
</g:hasErrors>