
<%@ page import="minix.BaseClass" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'baseClass.label', default: 'BaseClass')}"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>



<div class="body">

  <div class="center-full-page">

    <div class="header"><g:message code="default.show.label" args="[entityName]"/></div>
    <g:if test="${flash.message}">
      <div class="message">${flash.message}</div>
    </g:if>

    <div class="scaffoldingProperties">
      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.id.label" default="Id"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: baseClassInstance, field: "id")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.organ.label" default="Organ"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <g:if test="${baseClassInstance?.organ != null}">
          <g:link class="showDetails" controller="BBOrgan" action="show" id="${baseClassInstance?.organ?.id}">${baseClassInstance?.organ?.encodeAsHTML()}</g:link>
        </g:if>

        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.species.label" default="Species"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <g:if test="${baseClassInstance?.species != null}">
          <g:link class="showDetails" controller="BBSpecies" action="show" id="${baseClassInstance?.species?.id}">${baseClassInstance?.species?.encodeAsHTML()}</g:link>
        </g:if>

        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.metadata.label" default="Metadata"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${baseClassInstance.metadata}" var="m">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="BBMetadata" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.experiment.label" default="Experiment"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <g:if test="${baseClassInstance?.experiment != null}">
          <g:link class="showDetails" controller="BBExperiment" action="show" id="${baseClassInstance?.experiment?.id}">${baseClassInstance?.experiment?.encodeAsHTML()}</g:link>
        </g:if>

        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.samples.label" default="Samples"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${baseClassInstance.samples}" var="s">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="BBExperimentSample" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.name.label" default="Name"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: baseClassInstance, field: "name")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.comments.label" default="Comments"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${baseClassInstance.comments}" var="c">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="comment" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.externalIds.label" default="External Ids"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${baseClassInstance.externalIds}" var="e">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="externalId" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="baseClass.studie.label" default="Studie"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <g:if test="${baseClassInstance?.studie != null}">
          <g:link class="showDetails" controller="studie" action="show" id="${baseClassInstance?.studie?.id}">${baseClassInstance?.studie?.encodeAsHTML()}</g:link>
        </g:if>

        

      </span>

      
    </div>
  </div>
</div>
</body>
</html>
