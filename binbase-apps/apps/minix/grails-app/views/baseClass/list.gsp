<%@ page import="minix.BaseClass" %>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <title>BaseClass List</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

  <div class="center-full-page">
    <div class="header">BaseClass List</div>
    <g:if test="${flash.message}">
      <div class="message">${flash.message}</div>
    </g:if>
    <div class="element">

        <table>
          <thead>
          <tr>
            
            <g:sortableColumn property="id" title="Id"/>
            
            <th>Organ</th>
            
            <th>Species</th>
            
            <th>Experiment</th>
            
            <th>Samples</th>
            
            <g:sortableColumn property="name" title="Name"/>
            
          </tr>
          </thead>
          <tbody>
          <g:each in="${baseClassInstanceList}" status="i" var="baseClassInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

              
              <td><g:link action="show" id="${baseClassInstance.id}">${baseClassInstance.id?.encodeAsHTML()}</g:link></td>
              
              <td>${baseClassInstance.organ?.encodeAsHTML()}</td>
              
              <td>${baseClassInstance.species?.encodeAsHTML()}</td>
              
              <td>${baseClassInstance.experiment?.encodeAsHTML()}</td>
              
              <td>${baseClassInstance.samples?.encodeAsHTML()}</td>
              
              <td>${baseClassInstance.name?.encodeAsHTML()}</td>
              
            </tr>
          </g:each>
          </tbody>
        </table>
      <div class="paginateButtons">
        <g:paginate total="${BaseClass.count()}"/>
      </div>
    </div>
  </div>

</div>
</body>
</html>

