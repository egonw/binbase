
<%@ page import="minix.BaseClass" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'baseClass.label', default: 'BaseClass')}"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

  <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
  <div class="design">

    <div class="left">
      <div class="header">

        Information

      </div>


      <div class="left_information_box">
        please provide the required informations to create this object
      </div>

    </div>
    <div class="left-center-column">
      <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
      </g:if>

      <g:hasErrors bean="${baseClassInstance}">
        <div class="errors">
          <g:renderErrors bean="${baseClassInstance}" as="list"/>
        </div>
      </g:hasErrors>

      <g:form action="save" method="post" >
        <div class="scaffoldingProperties">

          

          <div>
            <span class="scaffoldingPropertyLabel">

              <label for="organ"><g:message code="baseClass.organ.label" default="Organ"/></label>

            </span>
            <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'organ', 'errors')}">

              <g:select name="organ.id" from="${binbase.web.core.BBOrgan.list()}" optionKey="id" value="${baseClassInstance?.organ?.id}" noSelection="['null': '']" />
            </span>
          </div>
          

          <div>
            <span class="scaffoldingPropertyLabel">

              <label for="species"><g:message code="baseClass.species.label" default="Species"/></label>

            </span>
            <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'species', 'errors')}">

              <g:select name="species.id" from="${binbase.web.core.BBSpecies.list()}" optionKey="id" value="${baseClassInstance?.species?.id}" noSelection="['null': '']" />
            </span>
          </div>
          

          <div>
            <span class="scaffoldingPropertyLabel">

              <label for="experiment"><g:message code="baseClass.experiment.label" default="Experiment"/></label>

            </span>
            <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'experiment', 'errors')}">

              <g:select name="experiment.id" from="${binbase.web.core.BBExperiment.list()}" optionKey="id" value="${baseClassInstance?.experiment?.id}" noSelection="['null': '']" />
            </span>
          </div>
          

          <div>
            <span class="scaffoldingPropertyLabel">

              <label for="name"><g:message code="baseClass.name.label" default="Name"/></label>

            </span>
            <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'name', 'errors')}">

              <g:textField name="name" value="${baseClassInstance?.name}" />
            </span>
          </div>
          

          <div>
            <span class="scaffoldingPropertyLabel">

              <label for="studie"><g:message code="baseClass.studie.label" default="Studie"/></label>

            </span>
            <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'studie', 'errors')}">

              <g:select name="studie.id" from="${minix.Studie.list()}" optionKey="id" value="${baseClassInstance?.studie?.id}"  />
            </span>
          </div>
          

        </div>
        <div class="myButton">
          <div class="buttons"><span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}"/></span>
          </div>
        </div>
      </g:form>
    </div>
  </div>

</div>
</body>
</html>
