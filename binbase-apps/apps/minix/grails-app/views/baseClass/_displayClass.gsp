<%@ page import="minix.Organ; minix.Species; minix.ShiroUser" %>
<%
    assert clazz != null, "sorry you need to providea  variable with the name 'clazz'"
%>

<g:setProvider library="jquery"/>




<div id="class_${clazz.id}_description">
    <div class="content-box">
        <div class="content-header">
            class: ${clazz.id}
        </div>

        <div class="classProperties">

            <div id="action_result_${clazz.id}" class="classResult">
            </div>


            <div class="classData">
                <div id="class_${clazz.id}_content">
                    <g:render template="/baseClass/displayClassTable"
                              model="[clazz:clazz,locked:locked,aquisitionTable:clazz.studie.canModify()]"/>
                </div>


            </div>

            <ul class="none-horizontal">

                <g:if test="${clazz.studie.canModify()}">

                    <g:if test="${clazz.studie.classes.size() > 1}">
                        <li>
                            <g:remoteLink class="remove" update="deleteDialog_${clazz.id}" controller="experimentClass"
                                          action="showDeleteDialog" id="${clazz.id}">remove class</g:remoteLink>
                        </li>
                    </g:if>

                </g:if>
            </ul>
        </div>


        <div id="deleteDialog_${clazz.id}"></div>

        <div class="topSpacer"></div>
    </div>
</div>
