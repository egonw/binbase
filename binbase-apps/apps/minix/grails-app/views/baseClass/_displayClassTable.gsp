<%@ page import="minix.CalibrationClass; minix.ReactionBlankSample; minix.QualityControlSample; minix.CalibrationSample" %><g:setProvider
        library="jquery"/>


<table id="class_${clazz.id}_table" class="colored contentTable">
    <thead>
    <tr>
        <th>file type</th>

        <g:if test="${clazz instanceof CalibrationClass}">
            <th>
                concentration
            </th>
        </g:if>
        <th>file name</th>
    </tr>
    </thead>
    <tbody id="class_body_${clazz.id}_table">
    <g:each var="sample" in="${clazz.samples}">

        <g:render template="/baseClass/renderRow" model="[sample:sample,clazz:clazz,locked:locked]"/>

    </g:each>

    <tr>
        <td></td>

        <td class="version_row">
            <label class="version_select">set version for all files _</label>
            <g:select class="version_select" from="${1..9}" name="clazz_version_${clazz.id}"
                      noSelection="['':'?']"/>


            <!-- sets the fileversion dynamicaly -->
            <g:javascript>
              $(function() {
              $("#clazz_version_${clazz.id}").change(function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value,'clazz': "${clazz.getClass().name}"}, url:"${createLink(controller: 'baseClass',action: 'ajaxSetFileVersion',id: clazz.id)}",success:function(data,textStatus){jQuery('#class_${clazz.id}_content').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });

            </g:javascript>
        </td>
    </tr>
    </tbody>

</table>


