<%@ page import="minix.CalibrationSample; minix.QualityControlSample; minix.ReactionBlankSample" %><g:setProvider
        library="jquery"/>

<%
    assert sample != null, "you need to provide a sample instance to this template"
    assert clazz != null, "you need to provide a class!"
%>
<!-- renderes a single row of a sample in the table -->
<tr id="row_sample_${sample.id}">
    <td>
        <g:if test="${sample instanceof QualityControlSample}">Quality Control Sample</g:if>
        <g:elseif test="${sample instanceof CalibrationSample}">Calibration Sample</g:elseif>
        <g:elseif test="${sample instanceof ReactionBlankSample}">Reaction Blank Sample</g:elseif>
    </td>
    <g:if test="${sample instanceof CalibrationSample}">
        <td>
            ${sample.concentration.concentration}
        </td>
    </g:if>
    <td class="fileName-row">

        <div class="${hasErrors(bean: sample, field: 'fileName', 'errors')} version_row">
            <label class="version_select">${sample.fileName}_</label>
            <g:select class="version_select" from="${1..200}" name="sample_version_${sample.id}"
                      value="${sample.fileVersion}"/>


            <!-- sets the fileversion dynamicaly -->
            <g:javascript>
              $(function() {
              $("#sample_version_${sample.id}").change(function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'sample',action: 'ajaxSetFileVersion',id: sample.id)}",success:function(data,textStatus){jQuery('#action_result_${clazz.id}').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });

            </g:javascript>

        </div>
    </td>
</tr>