<g:setProvider library="jquery"/>

<%@ page import=" org.apache.shiro.SecurityUtils; minix.ShiroUser" %>


<g:if test="${ShiroUser.hasAdminRights()}">

    <div style="margin-top:30px;">
        <div class="moduleheader">Administration</div>

        <li class="list_import_studie">
            <g:remoteLink update="next" controller="binBaseDuplicateExperiment"
                          action="selectExperiment_ajax">import BinBase studie</g:remoteLink>
        </li>

        <li class="list_import_studie">
            <g:remoteLink update="next" controller="importSetupXDump"
                          action="importDirectory_ajax">import SetupX dump files</g:remoteLink>
        </li>

        <li class="config_user">
            <g:link update="next" controller="shiroUser" action="list">User Management</g:link>
        </li>
    </div>

    <div style="margin-top:30px;">
        <div class="moduleheader">System</div>

        <li class="track_log">
            <g:link controller="userTracking" action="index">Tracking</g:link>
        </li>


        <li class="loggedin_user">
            <g:link update="next" controller="auth" action="listLogins">Currently logged in</g:link>
        </li>


        <li class="audit_log">
            <g:link update="next" controller="audit" action="index">Auditing</g:link>
        </li>

        <li class="reindex">
            <g:remoteLink update="next" controller="searchableIndex"
                          action="reindex_ajax">ReIndex Database</g:remoteLink>
        </li>
        <li class="jboss">
            <g:link url="http://${applicationServerIp}:8080/jmx-console" target="_blank">BinBase Admin Console</g:link>
        </li>

    </div>

</g:if>
