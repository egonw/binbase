<%@ page import=" org.apache.shiro.SecurityUtils; minix.ShiroUser" %>
<!-- very actions for manager -->

<g:if test="${ShiroUser.hasManagmentRights()}">

    <li class="schedule">
        <g:link update="next" controller="scheduleStudy" action="index">schedule studies</g:link>
    </li>


    <div style="margin-top:30px;">
        <div class="moduleheader">Management</div>

        <li class="schedule">
            <g:link controller="instrument" action="list">Instruments</g:link>
        </li>
        <li class="schedule">
            <g:link controller="architecture" action="list">Architectures</g:link>

        </li>

        <li class="merge_studie">
            <g:link controller="mergeStudie" action="mergeStudie"><g:message code="merge.studie"/></g:link>
        </li>
        <li class="merge_studie">
            <g:link controller="mergeOrgan" action="merge"><g:message code="merge.organ"/></g:link>
        </li>
        <li class="merge_studie">
            <g:link controller="mergeSpecies" action="merge"><g:message code="merge.species"/></g:link>
        </li>

    </div>


    <div style="margin-top:30px;">
        <div class="moduleheader">BinBase - Specific</div>

        <li class="schedule">
            <g:link controller="binBaseCluster" action="queue">Calculation Queue</g:link>
        </li>


        <li class="schedule">
            <g:link controller="scheduleBinBaseStudy" action="scheduleDataBase">Schedule Database</g:link>
        </li>



        <li class="schedule">
            <g:link controller="binBaseRetentionIndexMarker" action="showRetentionIndexMarkers">Retention Index Marker Settings</g:link>
        </li>

    </div>


</g:if>
