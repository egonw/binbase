<%@ page import="minix.ShiroUser" %>
<!-- very basic actions for every user -->
<div class="moduleheader">Tasks</div>

<li class="new_studie">
  <g:link controller="studieDesign" action="design"><g:message code="new.studie.design"/> </g:link>
</li>

<li class="list_studies">
  <g:link controller="studie" action="list"><g:message code="list.my.studies"/></g:link>
</li>

<g:if test="${ShiroUser.hasManagmentRights()}">

    <li class="list_studies">
      <g:link controller="studie" action="listAll"><g:message code="list.all.studies"/></g:link>
    </li>

</g:if>

<li class="list_designs">
  <g:link controller="studieDesign" action="list"><g:message code="list.my.designs"/></g:link>
</li>


<li class="list_studies">
  <g:link controller="species" action="list"><g:message code="list.species"/></g:link>
</li>


<li class="list_organs">
  <g:link controller="organ" action="list"><g:message code="list.organ"/></g:link>
</li>

