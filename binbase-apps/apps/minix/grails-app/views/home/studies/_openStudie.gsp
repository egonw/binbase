<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("#errorMessageForField").hide();

      $("input")
      .filter(function() {
        return this.id.match(/id.*/);
      })
      //takes care of the autocomplete
      .autocomplete({
              source: '${g.createLink(action: "findAllStudy", controller: "autoCompletion")}',
              dataType:"json",
              autoFocus: false,
              close: function(event,ui){

                var selector = $("#id");
                var result = selector.val().split(":")[0];

               selector.val(result);
              }

    })

    })
  });

    /**
    * is the enter number a valid studie id. Basically it has to be an integer
* @return {boolean}
*/
    function isValidStudie(){

        //result is not a number so study was not found
        var selector = $("#id");

        if (isNaN(selector.val())){
             $("#errorMessageForField").text("it looks like you entered a name '" +(selector.val()) + "' for a study, which does not exist. Please double check your spelling and try again.").show();

            return false;
        }
        //result was a number so all is good
        else{
            return true;
        }
    }
</g:javascript>


<div class="element">
    <div class="header">Please enter your study id to directly access a study and press 'enter'</div>

    <g:form name="openStudy" controller="studie" action="show" onsubmit="return isValidStudie()">

        <div>
            <div id="errorMessageForField" class="message">

            </div>
            <div class="textField">
                <g:textField name="id"/>
            </div>

        </div>

        <div class="element">
            Please be aware that this is not a search function, it rather allows you to quickly access a study by it's exact id or by a matching name
        </div>
    </g:form>
</div>
