
<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <meta name="layout" content="main"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'memberHome.css')}"/>

  <title>Home</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
  <div class="design">

    <div class="left">
      <div class="element">
        <g:render template="actions/actions" model="[user:user]"/>
      </div>
    </div>

    <div class="left-center-column">

    </div>
  </div>

</div>
</body>
</html>
