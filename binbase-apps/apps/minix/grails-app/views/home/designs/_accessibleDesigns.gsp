<g:setProvider library="jquery"/>

<div class="description">
  these are all your unfinished designs
</div>

<div id="designs">

</div>

<!-- populates studies during load -->
<g:javascript>
  jQuery.ajax({type:'POST', url:"${createLink(controller: 'home', action: 'showAllAccesibleDesigns' )}",success:function(data, textStatus) {
    jQuery('#designs').html(data);
  },error:function(XMLHttpRequest, textStatus, errorThrown) {
  }});
</g:javascript>