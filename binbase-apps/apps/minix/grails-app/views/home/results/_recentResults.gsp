<div class="element">
    <div class="header">Recently calculated studies</div>
    <g:render template="../studie/table/filteredTable" model="[studieCount:studieCount,studieInstanceList:studieCalculatedList,disablePagenate:true]"/>
</div>
