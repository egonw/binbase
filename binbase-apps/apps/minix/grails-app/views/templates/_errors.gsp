<g:hasErrors bean="${domain}">
  <div class="errors">
    <g:renderErrors bean="${domain}" as="list"/>
  </div>
</g:hasErrors>