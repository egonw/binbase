<g:setProvider library="jquery"/>
<g:form name="searchForm" action="searchResult">

    <div id="searchView">
        <div>
            please enter your search terms into the text field below, you can use * as a wildcard charcater.
        </div>

        <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
        </g:if>


        <div><g:textField id="query" name="query"/></div>

        <div><g:submitButton class="query" name="search" value="search"/></div>
    </div>
</g:form>


<g:javascript>
$(document).ready(function(){
     $('#query').focus()
});
</g:javascript>