<%@ page import="org.springframework.util.ClassUtils" %>
<%@ page import="grails.plugin.searchable.internal.lucene.LuceneUtils" %>
<%@ page import="grails.plugin.searchable.internal.util.StringQueryUtils" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="Search Result"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="design">

        <div class="left-thrirty">

            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.searchResult"/></p>
            </div>
        </div>

        <div class="right-seventy left-side-border">
            <g:set var="haveQuery" value="${params.query?.trim()}"/>
            <g:set var="haveResults" value="${searchResult?.results.size() > 0}"/>

            <g:if test="${haveQuery && haveResults == false}">
                <p>Sorry we haven't found anything for your query: <strong>${params.query}</strong></p>
            </g:if>

            <g:if test="${searchResult?.suggestedQuery}">
                <p>Did you mean <g:link controller="searchable" action="index"
                                        params="[q: searchResult.suggestedQuery]">${StringQueryUtils.highlightTermDiffs(params.q.trim(), searchResult.suggestedQuery)}</g:link>?</p>
            </g:if>

            <g:if test="${parseException}">
                <p>Your query - <strong>${params.query}</strong> - is not valid.</p>

                <p>Suggestions:</p>
                <ul>
                    <li>Fix the query: see <a
                            href="http://lucene.apache.org/java/docs/queryparsersyntax.html">Lucene query syntax</a> for examples
                    </li>
                    <g:if test="${LuceneUtils.queryHasSpecialCharacters(params.q)}">
                        <li>Remove special characters like <strong>" - [ ]</strong>, before searching, eg, <em><strong>${LuceneUtils.cleanQuery(params.q)}</strong>
                        </em><br/>
                            <em>Use the Searchable Plugin's <strong>LuceneUtils#cleanQuery</strong> helper method for this: <g:link
                                    controller="searchable" action="index"
                                    params="[q: LuceneUtils.cleanQuery(params.q)]">Search again with special characters removed</g:link>
                            </em>
                        </li>
                    </g:if>
                </ul>
            </g:if>

            <g:if test="${haveResults}">
                <div class="list">
                    <table>
                        <thead>
                        <tr>
                            <th>type</th>
                            <th>description</th>
                        </tr>
                        </thead>
                        <tbody>

                        <g:each var="result" in="${searchResult.results}" status="index">
                            <tr>
                                <g:set var="className" value="${ClassUtils.getShortName(result.getClass())}"/>
                                <g:set var="link"
                                       value="${createLink(controller: className[0].toLowerCase() + className[1..-1], action: 'show', id: result.id)}"/>

                                <g:set var="desc" value="${result.toString()}"/>
                                <g:if test="${desc.size() > 120}"><g:set var="desc"
                                                                         value="${desc[0..120] + '...'}"/></g:if>

                                <td><div class="name"><a href="${link}">${className} #${result.id}</a></div></td>
                                <td><div class="desc">${desc.encodeAsHTML()}</div></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>

                <div>
                    <div class="paginateButtons">
                        <g:if test="${haveResults}">
                            <div class="element">
                                Page:
                                <g:set var="totalPages" value="${Math.ceil(searchResult.total / searchResult.max)}"/>
                                <g:if test="${totalPages == 1}"><span class="currentStep">1</span></g:if>
                                <g:else><g:paginate controller="search" action="searchResult"
                                                    params="[query: params.query]"
                                                    total="${searchResult.total}" prev="&lt; previous"
                                                    next="next &gt;"/></g:else>
                            </div>
                        </g:if>
                    </div>
                </div>
            </g:if>
        </div>
</body>
</html>
