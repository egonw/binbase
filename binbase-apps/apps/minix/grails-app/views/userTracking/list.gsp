<%@ page import="minix.roles.RoleUtil; minix.ShiroRole; minix.ShiroUser" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>Access tracking for minix</title>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

</head>
<body>
<g:render template="/shared/navigation"/>

<g:form>
    <div class="body">
        <div class="design">

            <div class="left">
                <div class="header">

                    Information

                </div>


                <div class="left_information_box">

                    here you can find all the overview for logins, failed logins and logouts in the miniX system

                </div>

                <div class="topSpacer"></div>
                <div class="header">

                    Filter Options

                </div>

                <div class="left_information_box">

                    <div class="scaffoldingProperties">

                        <div>
                            <span class="scaffoldingPropertyLabel">
                                Filter by User
                            </span>
                            <span>
                                <g:select from="${users}" noSelection="['':'show all']" value="${selectedUser}" name="selectedUser"/>
                            </span>
                        </div>

                        <div>
                            <span class="scaffoldingPropertyLabel">
                            </span>

                            <span class="buttons">
                                <span class="button">
                                    <g:submitButton value="execute" name="filter"/>
                                </span>
                            </span>
                        </div>

                    </div>
                </div>
            </div>
            <div class="left-center-column">
                <g:if test="${flash.message}">
                    <div class="message">${flash.message}</div>
                </g:if>

                <div class="element">
                    <div>
                        <div class="paginateButtons">
                            <g:paginate total="${auditLogEventInstanceTotal}" params="[selectedUser:selectedUser]"/>
                        </div>
                        <table>
                            <thead>
                            <tr>

                                <g:sortableColumn property="dateCreated" title="Timestamp" params="[selectedUser:selectedUser]"/>

                                <g:sortableColumn property="username" title="User" params="[selectedUser:selectedUser]"/>

                                <g:sortableColumn property="event" title="Event" params="[selectedUser:selectedUser]"/>

                                <g:sortableColumn property="ipAddress" title="Ip Address" params="[selectedUser:selectedUser]"/>

                                <g:sortableColumn property="countryName" title="Country" params="[selectedUser:selectedUser]"/>

                                <g:sortableColumn property="city" title="City" params="[selectedUser:selectedUser]"/>

                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${auditLogEventInstanceList}" status="i" var="auditLogEventInstance">
                                <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                                    <td>${fieldValue(bean: auditLogEventInstance, field: 'dateCreated')}</td>

                                    <td>${fieldValue(bean: auditLogEventInstance, field: 'username')}</td>

                                    <td>${fieldValue(bean: auditLogEventInstance, field: 'event')}</td>

                                    <td>${fieldValue(bean: auditLogEventInstance, field: 'ipAddress')}</td>

                                    <td>${fieldValue(bean: auditLogEventInstance, field: 'countryName')}</td>

                                    <td>${fieldValue(bean: auditLogEventInstance, field: 'city')}</td>

                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                    <div class="paginateButtons">

                        <g:paginate total="${auditLogEventInstanceTotal}" params="[selectedUser:selectedUser]"/></div>
                </div>
            </div>

        </div>
    </div>
</g:form>
</body>
</html>
