<%@ page import="minix.Instrument" %>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <title>Instrument List</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

  <div class="design">

    <div class="left">
      <div class="header">

        Information

      </div>

      <div class="left_information_box">
        <p><g:message code="description.instrument"/> </p>
      </div>

    </div>
    <div class="left-center-column">
      <div class="header">Instrument List</div>
      <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
      </g:if>

      <div class="element">
        <ul class="none-horizontal">
          <!-- here we are registering our gctofs systems -->
          <li><g:link class="add" controller="GCTof" action="create">register new gc-tof</g:link></li>
        </ul>
      </div>

      <div class="element">

        <table>
          <thead>
          <tr>

            <g:sortableColumn property="id" title="Id"/>

            <g:sortableColumn property="name" title="Name"/>

            <g:sortableColumn property="identifier" title="Identifier"/>

            <g:sortableColumn property="description" title="Description"/>

            <g:sortableColumn property="dailyCapacity" title="Daily Capacity"/>

            <th>Architecture</th>

          </tr>
          </thead>
          <tbody>
          <g:each in="${instrumentInstanceList}" status="i" var="instrumentInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

              <td><g:link controller="${instrumentInstance.class.simpleName}" action="show" id="${instrumentInstance.id}">${instrumentInstance.id?.encodeAsHTML()}</g:link></td>

              <td>${instrumentInstance.name?.encodeAsHTML()}</td>

              <td>${instrumentInstance.identifier?.encodeAsHTML()}</td>

              <td>${instrumentInstance.description?.encodeAsHTML()}</td>

              <td>${instrumentInstance.dailyCapacity?.encodeAsHTML()}</td>

              <td>${instrumentInstance.architecture?.encodeAsHTML()}</td>

            </tr>
          </g:each>
          </tbody>
        </table>
        <div class="paginateButtons">
          <g:paginate total="${Instrument.count()}"/>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>

