<%@ page import="minix.GCTof" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'GCTof.label', default: 'GCTof')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.instrument"/></p>
            </div>



            <div class="left_information_box">
                please chance your fields and press update afterwards.
            </div>

        </div>
        <div class="left-center-column">

            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${GCTofInstance}">
                <div class="errors">
                    <g:renderErrors bean="${GCTofInstance}" as="list"/>
                </div>
            </g:hasErrors>
        <div class="element">

            <g:form method="post">
                <g:hiddenField name="id" value="${GCTofInstance?.id}"/>
                <g:hiddenField name="version" value="${GCTofInstance?.version}"/>
                <div class="scaffoldingProperties">

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="name"><g:message code="GCTof.name.label" default="Name"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'name', 'errors')}">
                            <g:textField name="name" value="${GCTofInstance?.name}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="identifier"><g:message code="GCTof.identifier.label" default="Identifier"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'identifier', 'errors')}">
                            <g:textField name="identifier" maxlength="1" value="${GCTofInstance?.identifier}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="description"><g:message code="GCTof.description.label" default="Description"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'description', 'errors')}">
                            <g:textField name="description" value="${GCTofInstance?.description}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="dailyCapacity"><g:message code="GCTof.dailyCapacity.label" default="Daily Capacity"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'dailyCapacity', 'errors')}">
                            <g:textField name="dailyCapacity" value="${fieldValue(bean: GCTofInstance, field: 'dailyCapacity')}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="architecture"><g:message code="GCTof.platform.label"
                                                                 default="Platform"/></label>

                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'platform', 'errors')}">

                            <g:select name="platform.id" from="${minix.Platform.listOrderByName()}" optionKey="id"
                                      value="${GCTofInstance?.platform?.id}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <g:message code="GCTof.architecture.label" default="Architecture"/>
                        </span>


                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'architecture', 'errors')}">
                            ${GCTofInstance?.architecture?.name}
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="autoSamplerSize"><g:message code="GCTof.autoSamplerSize.label" default="Auto Sampler Size"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'autoSamplerSize', 'errors')}">
                            <g:textField name="autoSamplerSize" value="${fieldValue(bean: GCTofInstance, field: 'autoSamplerSize')}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="autoSamplerStartPosition"><g:message code="GCTof.autoSamplerStartPosition.label" default="Auto Sampler Start Position"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'autoSamplerStartPosition', 'errors')}">
                            <g:textField name="autoSamplerStartPosition" value="${fieldValue(bean: GCTofInstance, field: 'autoSamplerStartPosition')}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="defaultFolder"><g:message code="GCTof.defaultFolder.label" default="Default Folder"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'defaultFolder', 'errors')}">
                            <g:textField name="defaultFolder" value="${GCTofInstance?.defaultFolder}"/>
                        </span>
                    </div>


                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="rollOver"><g:message code="GCTof.rollOver.label" default="Rollover Samplename"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'rollOver', 'errors')}">
                            <g:checkBox name="rollOver" value="${GCTofInstance?.rollOver}"/>
                        </span>
                    </div>


                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="useTray"><g:message code="GCTof.useTray.label" default="Use Tray - if 'none' is specified the system will use the sample id as tray"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'useTray', 'errors')}">
                            <g:textField name="useTray" value="${fieldValue(bean: GCTofInstance, field: 'useTray')}"/>
                        </span>
                    </div>

                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"/></span>
                </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
</body>
</html>
