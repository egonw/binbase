<%@ page import="minix.ShiroUser; minix.GCTof" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'GCTof.label', default: 'GCTof')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'instrument.css')}"/>

    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<g:render template="/shared/navigation"/>


<g:javascript>
    jQuery(document).ready(function () {
        $('#instrumentArrcodion').accordion();
    });

</g:javascript>


<div class="body">

    <div class="design">

        <div class="left-thrirty">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">

                <p><g:message code="description.instrument"/></p>
            </div>

            <div class="topSpacer"></div>

            <div class="header">Instrument Details</div>


            <div class="scaffoldingProperties">

                <span class="scaffoldingPropertyLabel">
                    <g:message code="GCTof.id.label" default="Id"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: GCTofInstance, field: "id")}

                </span>

                <span class="scaffoldingPropertyLabel">
                    <g:message code="GCTof.name.label" default="Name"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: GCTofInstance, field: "name")}

                </span>

                <span class="scaffoldingPropertyLabel">
                    <g:message code="GCTof.identifier.label" default="Identifier"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: GCTofInstance, field: "identifier")}

                </span>

                <span class="scaffoldingPropertyLabel">
                    <g:message code="GCTof.description.label" default="Description"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: GCTofInstance, field: "description")}

                </span>


                <span class="scaffoldingPropertyLabel">
                    <g:message code="GCTof.dailyCapacity.label" default="Daily Capacity"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: GCTofInstance, field: "dailyCapacity")}

                </span>


                <span class="scaffoldingPropertyLabel">
                    <g:message code="GCTof.architecture.label" default="Architecture"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    <g:if test="${GCTofInstance?.architecture != null}">
                        <g:link class="showDetails" controller="architecture" action="show"
                                id="${GCTofInstance?.architecture?.id}">${GCTofInstance?.architecture?.encodeAsHTML()}</g:link>
                    </g:if>

                </span>


                <span class="scaffoldingPropertyLabel">
                    <g:message code="GCTof.platform.label" default="Platform"/>
                </span>

                <span class="scaffoldingPropertyValue">
                    ${GCTofInstance?.platform?.encodeAsHTML()}
                </span>


                <span class="scaffoldingPropertyLabel">
                    <g:message code="GCTof.defaultFolder.label" default="Default Folder"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: GCTofInstance, field: "defaultFolder")}

                </span>


                <div>
                    <span class="scaffoldingPropertyLabel">
                        <label for="rollOver"><g:message code="GCTof.rollOver.label"
                                                         default="Rollover Samplename"/></label>
                    </span>
                    <span class="scaffoldingPropertyValue">
                        <g:checkBox name="rollOver" value="${GCTofInstance?.rollOver}" disabled="true"/>
                    </span>
                </div>

                <div>
                    <span class="scaffoldingPropertyLabel">
                        <label for="rollOver"><g:message code="GCTof.rollOver.label" default="Use Tray"/></label>
                    </span>
                    <span class="scaffoldingPropertyValue">
                        ${fieldValue(bean: GCTofInstance, field: "useTray")}
                    </span>
                </div>

            </div>
        </div>

        <div class="right-seventy left-side-border">
            <g:if test="${ShiroUser.hasAdminRights()}">

                <div class="header">
                    Actions
                </div>

                <div class="element">
                    <ul class="none-horizontal">
                        <li><g:link class="add" controller="GCTof" action="create">new GC-Tof</g:link></li>
                        <li><g:link class="edit" controller="GCTof" action="edit"
                                    id="${GCTofInstance.id}">edit instrument</g:link></li>
                        <li><g:link class="list" controller="GCTof" action="list"
                                    id="${GCTofInstance.id}">browse GC-Tof'ss</g:link></li>
                    </ul>
                </div>
            </g:if>

            <div class="header">
                Methods
            </div>

            <div class="instrumentArrcodion">
                <div id="instrumentArrcodion">
                    <h3><a href="#">Massspec Methods</a></h3>

                    <div class="gctof-method">
                        <g:render template="renderMSMethods" model="[instance: GCTofInstance]"/>
                    </div>

                    <h3><a href="#">GC Methods</a></h3>

                    <div class="gctof-method">
                        <g:render template="renderGCMethods" model="[instance: GCTofInstance]"/>

                    </div>

                    <h3><a href="#">Dataprocessing Methods</a></h3>

                    <div class="gctof-method">
                        <g:render template="renderDpMethods" model="[instance: GCTofInstance]"/>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
