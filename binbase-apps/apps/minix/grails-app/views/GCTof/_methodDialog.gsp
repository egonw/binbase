<g:setProvider library="jquery"/>

<g:form name="createNewMethod" controller="GCTof">
  <div class="description">
    please enter the name of your method, it needs to be identical wtih the same of the method on your physical instrument in the lab!
  </div>

  <g:hasErrors bean="${instance}">
    <div class="errors">
      <g:renderErrors bean="${instane}" as="list"/>
    </div>
  </g:hasErrors>
  <div class="element">
    <g:textField name="methodName" value=""/>

    <g:hiddenField name="id" value="${id}"/>
    <g:hiddenField name="type" value="${type}"/>
    <g:hiddenField name="destination" value="${destination}"/>

  </div>

  <div class="myButton">
    <div class="buttons">
      <g:submitToRemote class="save" action="ajaxSaveMethod" update="${destination}" value="save method"/>
    </div>
  </div>

</g:form>
