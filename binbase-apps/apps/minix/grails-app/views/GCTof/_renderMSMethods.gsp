<g:setProvider library="jquery"/>

<%@ page import="minix.GCTof" %>

<%
  assert instance != null, "you need to provide a instance"
  assert instance instanceof GCTof, "the variable needs to be a gctof!"
%>

<div id="msMethods">

  <div id="msMethodContent">
      <div class="element">
    <g:if test="${instance.msMethods.size() == 0}">
      <div class="message">
        there are no methods defined at this point in time!
      </div>
    </g:if>
    <g:else>
        <table>
            <thead>
            <tr>
                <th></th>
                <th>method name</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${instance.msMethods}" var="method">
                <tr>
                    <td>
                        <g:form name="definemsMethodForm_${method}" controller="GCTof">
                            <g:hiddenField name="id" value="${instance.id}"/>
                            <g:hiddenField name="destination" value="msMethodContent"/>
                            <g:hiddenField name="type" value="ms"/>
                            <g:hiddenField name="methodName" value="${method}"/>
                            <g:submitToRemote class="noBorder remove" controller="GCTof" update="[success:'msMethodContent',failure:'msMethodContent']" action="ajaxRemoveMethod" value="remove"/>
                        </g:form>
                    </td>

                    <td>${method}</td>

                </tr>
            </g:each>

            </tbody>
        </table>

    </g:else>
      </div>
    <div class="void element">
      <g:form name="defineMSMethodForm" controller="GCTof">

        <g:hiddenField name="id" value="${instance.id}"/>
        <g:hiddenField name="destination" value="msMethodContent"/>
        <g:hiddenField name="type" value="ms"/>

        <div class="myButton">
          <div class="buttons">
            <g:submitToRemote class="next" controller="GCTof" update="[success:'msMethodContent',failure:'msMethodContent']" action="ajaxMethodDialog" value="add method"/>
          </div>
        </div>
      </g:form>
    </div>

  </div>

</div>