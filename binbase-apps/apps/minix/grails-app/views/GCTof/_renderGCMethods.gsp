<g:setProvider library="jquery"/>

<%@ page import="minix.GCTof" %>

<%
  assert instance != null, "you need to provide a instance"
  assert instance instanceof GCTof, "the variable needs to be a gctof!"
%>

<div id="gcMethods">

  <div id="gcMethodContent">

      <div class="element">
      <g:if test="${instance.gcMethods.size() == 0}">
      <div class="message">
        there are no methods defined at this point in time!
      </div>
    </g:if>
    <g:else>
        <table>
            <thead>
            <tr>
                <th></th>
                <th>method name</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${instance.gcMethods}" var="method">
                <tr>
                    <td>
                        <g:form name="defineGcMethodForm_${method}" controller="GCTof">
                            <g:hiddenField name="id" value="${instance.id}"/>
                            <g:hiddenField name="destination" value="gcMethodContent"/>
                            <g:hiddenField name="type" value="gc"/>
                            <g:hiddenField name="methodName" value="${method}"/>
                            <g:submitToRemote class="noBorder remove" controller="GCTof" update="[success:'gcMethodContent',failure:'gcMethodContent']" action="ajaxRemoveMethod" value="remove"/>
                        </g:form>
                    </td>
                    <td>${method}</td>
                </tr>
            </g:each>

            </tbody>
        </table>
    </g:else>
      </div>
    <div class="void element">
      <g:form name="defineGCMethodForm" controller="GCTof">

        <g:hiddenField name="id" value="${instance.id}"/>
        <g:hiddenField name="destination" value="gcMethodContent"/>
        <g:hiddenField name="type" value="gc"/>

        <div class="myButton">
          <div class="buttons">
            <g:submitToRemote class="next" controller="GCTof" update="[success:'gcMethodContent',failure:'gcMethodContent']" action="ajaxMethodDialog" value="add method"/>
          </div>
        </div>
      </g:form>
    </div>

  </div>

</div>