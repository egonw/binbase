<g:setProvider library="jquery"/>

<%@ page import="minix.GCTof" %>

<%
  assert instance != null, "you need to provide a instance"
  assert instance instanceof GCTof, "the variable needs to be a gctof!"
%>

<div id="msMethods">

  <div id="dpMethodContent">
            <div class="element">

<g:if test="${instance.dpMethods.size() == 0}">
      <div class="message">
        there are no methods defined at this point in time!
      </div>
    </g:if>
    <g:else>

            <table>
            <thead>
                <tr>
                    <th></th>
                    <th>method name</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${instance.dpMethods}" var="method">
                    <tr>
                        <td>
                            <g:form name="defineDpMethodForm_${method}" controller="GCTof">
                                <g:hiddenField name="id" value="${instance.id}"/>
                                <g:hiddenField name="destination" value="dpMethodContent"/>
                                <g:hiddenField name="type" value="dp"/>
                                <g:hiddenField name="methodName" value="${method}"/>
                                <g:submitToRemote class="noBorder remove" controller="GCTof" update="[success:'dpMethodContent',failure:'dpMethodContent']" action="ajaxRemoveMethod" value="remove"/>
                            </g:form>
                        </td>
                        <td>${method}</td>
                     </tr>
                </g:each>

            </tbody>
        </table>
    </g:else>

</div>
    <div class="void element">
      <g:form name="defineDpMethodForm" controller="GCTof">

        <g:hiddenField name="id" value="${instance.id}"/>
        <g:hiddenField name="destination" value="dpMethodContent"/>
        <g:hiddenField name="type" value="dp"/>

        <div class="myButton">
          <div class="buttons">
            <g:submitToRemote class="next" controller="GCTof" update="[success:'dpMethodContent',failure:'dpMethodContent']" action="ajaxMethodDialog" value="add method"/>
          </div>
        </div>
      </g:form>
    </div>

  </div>

</div>