<%@ page import="minix.GCTof" %>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <title>GCTof List</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

  <div class="center-full-page">
    <div class="header">GCTof List</div>
    <g:if test="${flash.message}">
      <div class="message">${flash.message}</div>
    </g:if>
    <div class="element">

        <table>
          <thead>
          <tr>
            
            <g:sortableColumn property="id" title="Id"/>
            
            <g:sortableColumn property="name" title="Name"/>
            
            <g:sortableColumn property="identifier" title="Identifier"/>
            
            <g:sortableColumn property="description" title="Description"/>
            
            <g:sortableColumn property="dailyCapacity" title="Daily Capacity"/>
            
            <th>Architecture</th>
            
          </tr>
          </thead>
          <tbody>
          <g:each in="${GCTofInstanceList}" status="i" var="GCTofInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

              
              <td><g:link action="show" id="${GCTofInstance.id}">${GCTofInstance.id?.encodeAsHTML()}</g:link></td>
              
              <td>${GCTofInstance.name?.encodeAsHTML()}</td>
              
              <td>${GCTofInstance.identifier?.encodeAsHTML()}</td>
              
              <td>${GCTofInstance.description?.encodeAsHTML()}</td>
              
              <td>${GCTofInstance.dailyCapacity?.encodeAsHTML()}</td>
              
              <td>${GCTofInstance.architecture?.encodeAsHTML()}</td>
              
            </tr>
          </g:each>
          </tbody>
        </table>
      <div class="paginateButtons">
        <g:paginate total="${GCTof.count()}"/>
      </div>
    </div>
  </div>

</div>
</body>
</html>

