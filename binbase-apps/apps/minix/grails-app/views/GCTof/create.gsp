<%@ page import="minix.GCTof" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'GCTof.label', default: 'GCTof')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">
    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p>
                    Please enter all the information to register a new GC-Tof instrument in the system.
                </p>
            </div>

            <div class="topSpacer"></div>

            <div class="header">

                What are instruments

            </div>

            <div class="left_information_box">

                <p><g:message code="description.instrument"/></p>
            </div>
        </div>

        <div class="left-center-column">
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>

            <g:hasErrors bean="${GCTofInstance}">
                <div class="errors">
                    <g:renderErrors bean="${GCTofInstance}" as="list"/>
                </div>
            </g:hasErrors>

            <g:form action="save" method="post">
                <div class="scaffoldingProperties">

                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="name"><g:message code="GCTof.name.label" default="Name"/></label>

                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'name', 'errors')}">

                            <g:textField name="name" value="${GCTofInstance?.name}"/>
                        </span>
                    </div>


                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="identifier"><g:message code="GCTof.identifier.label"
                                                               default="Identifier"/></label>

                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'identifier', 'errors')}">

                            <g:textField name="identifier" maxlength="1" value="${GCTofInstance?.identifier}"/>
                        </span>
                    </div>


                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="description"><g:message code="GCTof.description.label"
                                                                default="Description"/></label>

                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'description', 'errors')}">

                            <g:textField name="description" value="${GCTofInstance?.description}"/>
                        </span>
                    </div>


                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="dailyCapacity"><g:message code="GCTof.dailyCapacity.label"
                                                                  default="Daily Capacity"/></label>

                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'dailyCapacity', 'errors')}">

                            <g:textField name="dailyCapacity"
                                         value="${fieldValue(bean: GCTofInstance, field: 'dailyCapacity')}"/>
                        </span>
                    </div>


                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="architecture"><g:message code="GCTof.platform.label"
                                                                 default="Platform"/></label>

                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'platform', 'errors')}">

                            <g:select name="platform.id" from="${minix.Platform.listOrderByName()}" optionKey="id"
                                      value="${GCTofInstance?.platform?.id}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="architecture"><g:message code="GCTof.architecture.label"
                                                                 default="Architecture"/></label>

                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'architecture', 'errors')}">

                            <g:select name="architecture.id" from="${minix.Architecture.list()}" optionKey="id"
                                      value="${GCTofInstance?.architecture?.id}"/>
                        </span>
                    </div>


                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="defaultFolder"><g:message code="GCTof.defaultFolder.label"
                                                                  default="Default Folder"/></label>

                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'defaultFolder', 'errors')}">

                            <g:textField name="defaultFolder" value="${GCTofInstance?.defaultFolder}"/>
                        </span>
                    </div>


                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="defaultFolder"><g:message code="GCTof.defaultFolder.label"
                                                                  default="Use Tray  - if left empty or 'none' is specfied the system will use the sample id as tray"/></label>


                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: GCTofInstance, field: 'useTray', 'errors')}">

                            <g:textField name="useTray" value="${GCTofInstance?.useTray}"/>
                        </span>
                    </div>


                </div>

                <div class="myButton">
                    <div class="buttons"><span class="button"><g:submitButton name="create" class="save"
                                                                              value="${message(code: 'default.button.create.label', default: 'Create')}"/></span>
                    </div>
                </div>
            </g:form>
        </div>
    </div>

</div>
</body>
</html>
