<%@ page import="org.apache.shiro.SecurityUtils" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="layout" content="main"/>

	<g:setProvider library="jquery"/>

	<title>Welcome</title>

</head>

<body>

<g:if test="${SecurityUtils.subject?.principal != null}">
	<g:render template="/shared/navigation"/>
</g:if>
<g:if test="${flash.message}">
	<div class="message">${flash.message}</div>
</g:if>


<g:javascript library="flot/jquery.flot"/>
<g:javascript library="flot/jquery.flot.selection"/>
<g:javascript library="flot/jquery.flot.time"/>
<g:javascript library="flot/jquery.flot.pie"/>


<div class="body">
	<div class="design">
		<div class="left">
			<g:render template="/miniXContent/studiesByArchitecture"/>
		</div>

		<div class="center">
			<div class="header collectonme">
				Welcome to MiniX
			</div>

			<p>
				MiniX has various functionality allowing the users to run the experiments and samples smoothly and fast in a modern laboratory.
			</p>

			<p>
				If you like to use this system, you need to contact the PI running the lab equipped with a MiniX system. This will allow them to detail questions about the experiment and what they would like to accomplish. Then a user account is created for the you, which allow you to design experimental designs. This process is done using a simple form based wizard and can be done in several steps over a period of days or weeks and allows you define species, organs and treatments.
				Based on this design MiniX is then generating a study for you.
			</p>

			<p>
				This study contains the automatically determined classes to be run on the selected architecture of your choosing. This could be the integrated fully automatic <a
					href="https://code.google.com/p/binbase/"
					target="_blank">binbase system</a> or some other approach. Once your study is complete you can attach the results to it or other attachments, to have all your relevant data in a single place. This allows easier data exchange with other people and ensures that no results are getting lost.
			</p>
		</div>

		<div class="right">
			<g:if test="${SecurityUtils.subject?.principal == null}">
				<g:render template="/auth/login"/>
			</g:if>
		</div>
	</div>

</div>
<script type="text/javascript">
	$(document).ready(function () {
		var date = new Date();
		if(date.getMonth()==11) {
			$('body').snowfall('clear');
			$('.collectonme').show();
			$('body').snowfall({
				collection: '.collectonme',
				flakeCount: 75,
				maxSpeed: 0.5,
				flakeColor: '#acf'
			});
		}
	});
</script>
</body>

</html>
