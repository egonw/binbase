<%@ page import="minix.Sample" %>


<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <title>Sample List</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

  <div class="center-full-page">
    <div class="header">Sample List</div>
    <g:if test="${flash.message}">
      <div class="message">${flash.message}</div>
    </g:if>
    <div class="element">

        <table>
          <thead>
          <tr>
            
            <g:sortableColumn property="id" title="Id"/>
            
            <g:sortableColumn property="fileName" title="File Name"/>
            
            <th>Experiment Class</th>
            
            <g:sortableColumn property="comment" title="Comment"/>
            
            <g:sortableColumn property="label" title="Label"/>
            
            <g:sortableColumn property="fileVersion" title="File Version"/>
            
          </tr>
          </thead>
          <tbody>
          <g:each in="${sampleInstanceList}" status="i" var="sampleInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

              
              <td><g:link action="show" id="${sampleInstance.id}">${sampleInstance.id?.encodeAsHTML()}</g:link></td>
              
              <td>${sampleInstance.fileName?.encodeAsHTML()}</td>
              
              <td>${sampleInstance.experimentClass?.encodeAsHTML()}</td>
              
              <td>${sampleInstance.comment?.encodeAsHTML()}</td>
              
              <td>${sampleInstance.label?.encodeAsHTML()}</td>
              
              <td>${sampleInstance.fileVersion?.encodeAsHTML()}</td>
              
            </tr>
          </g:each>
          </tbody>
        </table>
      <div class="paginateButtons">
        <g:paginate total="${Sample.count()}"/>
      </div>
    </div>
  </div>

</div>
</body>
</html>

