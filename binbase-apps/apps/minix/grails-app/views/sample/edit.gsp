
<%@ page import="minix.Sample" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'sample.label', default: 'Sample')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">
                please provide the required informations to create this object
            </div>

        </div>
        <div class="left-center-column">

            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${sampleInstance}">
                <div class="errors">
                    <g:renderErrors bean="${sampleInstance}" as="list"/>
                </div>
            </g:hasErrors>
        <div class="element">

            <g:form method="post" >
                <g:hiddenField name="id" value="${sampleInstance?.id}"/>
                <g:hiddenField name="version" value="${sampleInstance?.version}"/>
                <div class="scaffoldingProperties">

                    
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="spectra"><g:message code="sample.spectra.label" default="Spectra"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'spectra', 'errors')}">
                            
<ul>
<g:each in="${sampleInstance?.spectra?}" var="s">
    <li><g:link controller="BBSpectra" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="BBSpectra" action="create" params="['sample.id': sampleInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'BBSpectra.label', default: 'BBSpectra')])}</g:link>

                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="fileName"><g:message code="sample.fileName.label" default="File Name"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'fileName', 'errors')}">
                            <g:textField name="fileName" value="${sampleInstance?.fileName}" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="experimentClass"><g:message code="sample.experimentClass.label" default="Experiment Class"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'experimentClass', 'errors')}">
                            <g:select name="experimentClass.id" from="${binbase.web.core.BBExperimentClass.list()}" optionKey="id" value="${sampleInstance?.experimentClass?.id}" noSelection="['null': '']" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="comment"><g:message code="sample.comment.label" default="Comment"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'comment', 'errors')}">
                            <g:textField name="comment" value="${sampleInstance?.comment}" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="label"><g:message code="sample.label.label" default="Label"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'label', 'errors')}">
                            <g:textField name="label" value="${sampleInstance?.label}" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="attributes"><g:message code="sample.attributes.label" default="Attributes"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'attributes', 'errors')}">
                            
<ul>
<g:each in="${sampleInstance?.attributes?}" var="a">
    <li><g:link controller="attribute" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="attribute" action="create" params="['sample.id': sampleInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'attribute.label', default: 'Attribute')])}</g:link>

                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="fileVersion"><g:message code="sample.fileVersion.label" default="File Version"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'fileVersion', 'errors')}">
                            <g:select name="fileVersion" from="${1..9}" value="${fieldValue(bean: sampleInstance, field: 'fileVersion')}"  />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="comments"><g:message code="sample.comments.label" default="Comments"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'comments', 'errors')}">
                            <g:select name="comments" from="${minix.Comment.list()}" multiple="yes" optionKey="id" size="5" value="${sampleInstance?.comments}" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="externalIds"><g:message code="sample.externalIds.label" default="External Ids"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'externalIds', 'errors')}">
                            
<ul>
<g:each in="${sampleInstance?.externalIds?}" var="e">
    <li><g:link controller="externalId" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="externalId" action="create" params="['sample.id': sampleInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'externalId.label', default: 'ExternalId')])}</g:link>

                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="scheduleDate"><g:message code="sample.scheduleDate.label" default="Schedule Date"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: sampleInstance, field: 'scheduleDate', 'errors')}">
                            <g:datePicker name="scheduleDate" precision="day" value="${sampleInstance?.scheduleDate}"  />
                        </span>
                    </div>
                        

                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"/></span>
                </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
</body>
</html>
