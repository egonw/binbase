<g:setProvider library="jquery"/>

<!-- displays the message -->
<div class="message">sample successfully removed!</div>

<g:javascript>

  $(document).ready(function() {
      $('#row_sample_'+${sampleId}).remove();
  });
</g:javascript>

<!-- calls the function-->