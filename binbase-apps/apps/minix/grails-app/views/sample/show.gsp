
<%@ page import="minix.Sample" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'sample.label', default: 'Sample')}"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>



<div class="body">

  <div class="center-full-page">

    <div class="header"><g:message code="default.show.label" args="[entityName]"/></div>
    <g:if test="${flash.message}">
      <div class="message">${flash.message}</div>
    </g:if>

    <div class="scaffoldingProperties">
      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.id.label" default="Id"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: sampleInstance, field: "id")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.spectra.label" default="Spectra"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${sampleInstance.spectra}" var="s">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="BBSpectra" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.fileName.label" default="File Name"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: sampleInstance, field: "fileName")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.experimentClass.label" default="Experiment Class"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <g:if test="${sampleInstance?.experimentClass != null}">
          <g:link class="showDetails" controller="BBExperimentClass" action="show" id="${sampleInstance?.experimentClass?.id}">${sampleInstance?.experimentClass?.encodeAsHTML()}</g:link>
        </g:if>

        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.comment.label" default="Comment"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: sampleInstance, field: "comment")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.label.label" default="Label"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: sampleInstance, field: "label")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.attributes.label" default="Attributes"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${sampleInstance.attributes}" var="a">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="attribute" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.fileVersion.label" default="File Version"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        ${fieldValue(bean: sampleInstance, field: "fileVersion")}
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.comments.label" default="Comments"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${sampleInstance.comments}" var="c">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="comment" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.externalIds.label" default="External Ids"/>
      </span>

      <span class="scaffoldingPropertyValue">

        

        <ul class="scaffoldingList">
          <g:each in="${sampleInstance.externalIds}" var="e">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="externalId" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        

      </span>

      


      <span class="scaffoldingPropertyLabel">
        <g:message code="sample.scheduleDate.label" default="Schedule Date"/>
      </span>

      <span class="scaffoldingPropertyValue">

        
        <g:formatDate date="${sampleInstance?.scheduleDate}"/>
        

      </span>

      
    </div>
  </div>
</div>
</body>
</html>
