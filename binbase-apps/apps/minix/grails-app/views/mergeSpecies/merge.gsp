<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <title>Combine Species Tool</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">
        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">

                <p>
                    please select all the species you would like to combine.
                </p>

                <p>
                    The result will be a new species and the old species will be delted. All related studies will be updated to reflect these changes and link to the new species now.
                </p>
            </div>

        </div>

        <div class="left-center-column">
            <div class="header">
                Welcome to the species merge tool
            </div>

            <div class="textDescription">

                This tool enables you to combine 2 or more species with each other.
            </div>


            <g:form name="merge">

                <div id="selectDiv" class="textDescription">
                    <g:render template="select" model="[values:values"/>
                </div>



            </g:form>

        </div>
    </div>

</div>
</body>
</html>
