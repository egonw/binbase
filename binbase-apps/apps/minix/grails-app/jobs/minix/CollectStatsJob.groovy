package minix

import core.StatisticsService

/**
 * collects content statistics of miniX all X minutes
 */
class CollectStatsJob {

    static triggers = {
        //fire all 60 minutes
        cron name: 'fireEveryHourToCollectMiniXStatistic', cronExpression: "0 0 * * * ?"
    }
    StatisticsService statisticsService

    def execute() {
        statisticsService.createSnapShot()
    }
}
