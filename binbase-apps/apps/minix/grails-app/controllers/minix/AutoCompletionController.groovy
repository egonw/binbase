package minix

import grails.converters.JSON
import org.apache.shiro.SecurityUtils

/**
 * handles all our autocomplete requests
 */
class AutoCompletionController {

    StudieQueryService studieQueryService

    /**
     * searches for organs and returns the found object
     */
    def findOrgan = {

        def organ = Organ.executeQuery("select distinct ev.name from Organ ev where lower(ev.name) like ? ", '%' + params.term.toString().toLowerCase() + '%')

        render organ as JSON
    }

    /**
     * searches for species and returns the found opbjects
     */
    def findSpecies = {

        def species = Species.executeQuery("select distinct ev.name from Species ev where lower(ev.name) like ? ", '%' + params.term.toString().toLowerCase() + '%')

        render species as JSON
    }

    /**
     * finds treatment
     */
    def findTreatment = {

        def species = Treatment.executeQuery("select distinct ev.description from Treatment ev where lower(ev.description) like ? ", '%' + params.term.toString().toLowerCase() + '%')

        render species as JSON
    }

    /**
     * finds treatment values
     */
    def findTreatmentValue = {

        def species = TreatmentSpecific.executeQuery("select distinct ev.value from TreatmentSpecific ev where lower(ev.value) like ? ", '%' + params.term.toString().toLowerCase() + '%')

        render species as JSON
    }

    /**
     * finds a study
     */
    def findStudy = {


        def term = params.term.toString().replaceAll("%", "")

        def result = studieQueryService.queryForUserWithFilter(ShiroUser.findByUsername(SecurityUtils.subject.principal), [max: 10], term)

        def studies = []

        result.each { Studie studie ->
            studies.add("${studie.id}:${studie.description}")
        }
        render studies as JSON
    }


    /**
     * finds a study
     */
    def findAllStudy = {


        def term = params.term.toString().replaceAll("%", "")

        def result = studieQueryService.queryForStudiesWithFilter( [max: 10], term)

        def studies = []

        result.each { Studie studie ->
            studies.add("${studie.id}:${studie.description}")
        }
        render studies as JSON
    }
    /**
     * finds a species and returns it as json string
     */
    def findSpeciesValues = {


        def term = params.term.toString().replaceAll("%", "")

        def result = Species.executeQuery("select ev from Species ev where lower(ev.name) like ? ", '%' + params.term.toString().toLowerCase() + '%')

        def species = []

        result.each { Species val ->
            species.add("${val.id}:${val.name}")
        }
        render species as JSON
    }

    /**
     * finds a species and returns it as json string
     */
    def findOrganValues = {


        def term = params.term.toString().replaceAll("%", "")

        def result = Species.executeQuery("select ev from Organ ev where lower(ev.name) like ? ", '%' + params.term.toString().toLowerCase() + '%')

        def organs = []

        result.each { Organ val ->
            organs.add("${val.id}:${val.name}")
        }
        render organs as JSON
    }


}
