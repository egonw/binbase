package minix

import minix.progress.ProgressHandler
import org.apache.commons.io.IOUtils
import org.apache.shiro.SecurityUtils
import jprogress.ProgressService
import binbase.web.core.BBResult
import binbase.web.core.BBContent
import binbase.web.core.BBExperimentSample
import binbase.web.core.BBDownload

class StudieController implements ProgressHandler{
    def scaffold = true

    AttachmentService attachmentService

    CommunicationsService communicationsService

    StudieQueryService studieQueryService

    ProgressService progressService

    UserService userService

    StudyRemovalService studyRemovalService

    ModifyClassService modifyClassService

    def create = {
        redirect(controller: 'studieDesign', action: 'create')

    }

    void handleProgress(String id, double percent, Object properties) {
        progressService.setProgressBarValue(id, percent)

    }

    def ajaxDeleteSubStudie = {
        SubStudie studie = SubStudie.get(params.id)
        Studie parent = studie.origin
        studyRemovalService.delete(SubStudie.get(params.id),this,params.sessionId)
        render(template:"renderAssociatedSubStudies",model: [studieInstance:parent])
    }
    /**
     * forces the display of the classes...
     */
    def forceClasses = {
        def studieInstance = Studie.get(params.id)
        render(template: "renderClasses", model: [studieInstance: studieInstance])
    }

    def delete = {

        if (!ShiroUser.hasAdminRights()) {
            flash.message = "sorry you do not have the permission to delete this object!"
            redirect(action: "list")
        }
        else {
            def studieInstance = Studie.get(params.id)
            if (studieInstance) {
                try {

                    studyRemovalService.delete(studieInstance,this,params.sessionId)
                    flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'studie.label', default: 'Studie'), params.id])}"
                    redirect(action: "list")
                }
                catch (org.springframework.dao.DataIntegrityViolationException e) {
                    e.printStackTrace()
                    flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'studie.label', default: 'Studie'), params.id])}"
                    redirect(action: "show", id: params.id)
                }
            }
            else {
                flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'studie.label', default: 'Studie'), params.id])}"
                redirect(action: "list")
            }
        }
    }

    /**
     * schedules a studie
     */
    def schedule = {
        def studieInstance = Studie.get(params.id)

        if (studieInstance) {
            redirect(controller: studieInstance.architecture.relatedController, action: studieInstance.architecture.relatedAction, params: ["studie_id": "${studieInstance.id}"])
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'studie.label', default: 'Studie'), params.id])}"
            redirect(action: "list")
        }
    }

    /**
     * uploads data
     */
    def uploadAttachment = {

        Studie studie = Studie.get(params.studie)

        try {

            String name = params.qqfile.toString()
            byte[] bytes = IOUtils.toByteArray(request.inputStream);
            attachmentService.attachFileToStudie(bytes, studie, name)

            render "{success: true}"

        }
        catch (Exception e) {

            log.error(e.getMessage(), e)
            render "{success:false}"
        }
    }

    /**
     * used to change a study from public to not public
     */
    def ajaxStudieSetPublic = {

        def studieInstance = Studie.get(params.id)

        if (studieInstance) {
            studieInstance.publicExperiment = Boolean.parseBoolean(params.value.toString())
            studieInstance.save()
        }
    }

    /**
     * uploads a result
     */
    def uploadResult = {

        Studie studie = Studie.get(params.studie)

        try {

            String name = params.qqfile.toString()

            byte[] bytes = IOUtils.toByteArray(request.inputStream);

            communicationsService.uploadResult(studie.id, bytes, name)
            render "{success: true}"

        }
        catch (Exception e) {

            log.error(e.getMessage(), e)
            render "{success:false}"
        }
    }

    /**
     * renders the result table
     */
    def ajaxRenderResultTable = {
        render(template: "resultTable", model: [studieInstance: Studie.get(params.id)])
    }

    /**
     * deletes a studie
     * @param studieInstance
     */
    /*
    def deleteStudie(Studie studieInstance) {
        Studie.withTransaction {

            studieInstance.aquisitionTable = null

            StudieDesign design = StudieDesign.findByGeneratedStudie(studieInstance)

            if (design != null) {
                //delete the associated design
                design.delete(flush: true)
            }

            def users = []
            //deletes a studie instance after unlinking it from all related users
            studieInstance.users.each {ShiroUser user ->
                users.add(user)
            }

            def results = []

            studieInstance.results.each { BBResult result ->
                results.add(result)
            }
            results.each { BBResult result ->

                result.experiment.removeFromResults(result)
                result.experiment = null
                BBContent.findAllByResult(result).each {it.delete()}

                result.delete()
            }

            studieInstance.save(flush: true)

            def attachments = FileAttachment.findAllByStudie(studieInstance)

            attachments.each { FileAttachment fileAttachmentInstance ->
                attachmentService.deleteFileFromStudie(fileAttachmentInstance)
            }


            studieInstance.save(flush: true)

            def classes = []
            studieInstance.classes.each {

                classes.add(it)
            }

            classes.each {
                modifyClassService.removeClass(it,studieInstance)
            }

            users.each {ShiroUser user ->
                userService.revokeStudyFromUser(user, studieInstance)
            }

            studieInstance.delete(flush: true)
        }
    }

*/
    def ajaxRemoveStudie = {

        render """<div class="message">studie deleted - not yet implemented!</div>"""
    }


    def ajaxRenderAttchmentTable = {

        Studie studie = Studie.get(params.id)

        render(template: "attachmentTable", model: [studieInstance: studie])
    }

    /**
     * list studies of this user
     */
    def list = {

        params.max = Math.min(params.max ? params.int('max') : 10, 100)

        if (params.sort == null) {
            params.sort = "id"
            params.order = "desc"
        }


        if (params.studieFilter == null || params.studieFilter == "") {

            ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

            def result = studieQueryService.queryForUser(user, params)
            def count = studieQueryService.queryCountForUser(user)

            [studieInstanceList: result, studieCount: count]
        }
        else {

            ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

            def result = studieQueryService.queryForUserWithFilter(user, params, params.studieFilter)
            def count = studieQueryService.queryCountForUserWithFilter(user, params.studieFilter)

            if (params.ajaxCall) {
                render template: "table/filteredTable", model: [studieInstanceList: result, studieCount: count, studieFilter: params.studieFilter]
            }

            else {
                [studieInstanceList: result, studieCount: count, studieFilter: params.studieFilter]
            }
        }
    }

    /**
     * list studies of this user
     */
    def listAll = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)


        if (params.sort == null) {
            params.sort = "id"
            params.order = "desc"
        }

        ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

        if (user.hasManagmentRights()) {

            if (params.studieFilter == null || params.studieFilter == "") {

                def result = Studie.list(params)
                return [studieInstanceList: result, studieCount: Studie.count()]

            }
            else {

                def result = studieQueryService.queryForStudiesWithFilter(params, params.studieFilter)
                def count = studieQueryService.queryCountForStudiesWithFilter(params.studieFilter)

                if (params.ajaxCall) {
                    render template: "table/filteredTable", model: [studieInstanceList: result, studieCount: count, studieFilter: params.studieFilter]
                }
                else {
                    return [studieInstanceList: result, studieCount: count, studieFilter: params.studieFilter]

                }
            }
        }
        else {

            flash.message = "sorry you do not have the permission to view all studies, these are your studies!"
            redirect(action: "list")
        }
    }


    def show = {
        Studie studie = Studie.get(params.id)

        if (params.message) {
            flash.message = params.message
        }
        if (studie != null) {
            if (!studie.canAccess()) {
                flash.message = "sorry you do not have the permission to access this object!"
                redirect(action: "list")
            }

            if (studie.publicExperiment == null) {
                studie.publicExperiment = false
                studie.save(flush: true)
            }

            [studieInstance: studie]
        }
        else {
            flash.message = "sorry no studie found!"
            redirect(action: "list")

        }
    }

    def visualize = {
        Studie studie = Studie.get(params.id)

        if (studie != null) {
            if (!studie.canAccess()) {
                flash.message = "sorry you do not have the permission to access this object!"
                redirect(action: "list")
            }

            [studieInstance: studie]
        }
        else {
            flash.message = "sorry no studie found!"
            redirect(action: "list")

        }
    }


    def visualizeTree = {
        Studie studie = Studie.get(params.id)

        if (studie != null) {
            if (!studie.canAccess()) {
                flash.message = "sorry you do not have the permission to access this object!"
                redirect(action: "list")
            }

            def tree = [:]


            [studieInstance: studie]
        }
        else {
            flash.message = "sorry no studie found!"
            redirect(action: "list")

        }
    }


    def assignUserAjax = {
        Studie studie = Studie.get(params.id)

        if (params.assign != null) {

            ShiroUser user = ShiroUser.get(params.user)

            user.addToStudies(studie)
            studie.addToUsers(user)

            if (user.validate() == false) {
                println user.errors
            }

            if (studie.validate() == false) {
                println studie.errors
            }

            user.save(flush: true)
            studie.save(flush: true)

            studie = Studie.get(params.id)
            render template: "renderUserTable", model: [studieInstance: studie]

        }
        else {
            render template: "assignUserSelect", model: [studie: studie, users: ShiroUser.findAllByLocked(false, [sort: "username", order: "asc"])]
        }
    }

    def cancelAssignUserAjax = {
        Studie studie = Studie.get(params.id)
        render template: "renderUserTable", model: [studieInstance: studie]
    }

    def revokeUserAjax = {
        ShiroUser user = ShiroUser.get(params.id)
        Studie studie = Studie.get(params.studie)

        (user, studie) = userService.revokeStudyFromUser(user, studie)

        if (user.validate() == false) {
            println user.errors
            render """<div class="errors">there was an error for ${user.username}</div>"""
        }
        else if (studie.validate() == false) {
            println studie.errors
            render """<div class="errors">there was an error for ${user.username}</div>"""
        }
        else {
            user.save(flush: true)
            studie.save(flush: true)

            render """<div class="message">user rights are revoked for ${user.username}</div>"""

        }
    }

    /**
     * the dialog to delete a studie
     */
    def showDeleteDialog = {

        Studie studie = Studie.get(params.id)

        if (studie != null) {
            render template: "dialog/deleteStudieDialog", model: [studie: studie]
        }
        else {
            flash.message = "sorry no studie found!"
            redirect(action: "list")

        }
    }

    def lockStudie = {
        Studie studie = Studie.get(params.id)

        if (studie.canModify()) {
            studie.forceLock()
        }

        redirect(action: "show", params: [id: studie.id])
    }

    def unlockStudie = {
        Studie studie = Studie.get(params.id)

        if (!studie.canModify()) {
            studie.forceUnLock()

            redirect(action: "show", params: [id: studie.id, message: "your study was unlocked!"])

        }

        else {

            redirect(action: "show", params: [id: studie.id, message: "sorry this study can not be unlocked!"])

        }
    }

    /**
     * sets the file version for all samples in this class
     */
    def ajaxSetFileVersion = {
        Studie studie = Studie.get(params.id)

        boolean success = false

        //no selection
        if (params.value.toString().trim() == "?"){
            render template: "/studie/renderClasses", model: [studieInstance: studie]
            return
        }
        try {

            int version =  params.value.toString().toInteger()

            progressService.setProgressBarValue("studie_generate_file_${studie.id}", 0);

            int counter = 0

            //calculate the total amount of the samples
            def count = 0
            studie.classes.each { BaseClass clazz ->
                count = count + clazz.samples.size()
            }

            studie.classes.each { BaseClass clazz ->
                clazz.samples.each { def sample ->
                    sample.fileVersion = version

                    if (sample.validate()) {
                        sample.save(flush: true)
                        success = true
                    }
                    else {
                        render(template: "/shared/validation_error", model: [errorBean: sample, message: """there was something wrong with your provided value: '${params.value}' for sample: ${sample.id} - ${sample.toString()}"""])
                        progressService.setProgressBarValue("studie_generate_file_${studie.id}", 100);
                        return
                    }

                    progressService.setProgressBarValue("studie_generate_file_${studie.id}", counter / count * 100);
                    counter++
                }

            }

        }
        catch (NumberFormatException e) {
            log.error(e.getMessage(), e)
        }

        progressService.setProgressBarValue("studie_generate_file_${studie.id}", 100);

        if (success) {
            render template: "/studie/renderClasses", model: [studieInstance: studie]
        }
    }

    /**
     * used to edit the studie title
     */
    def ajaxEditStudieTitle = {
        def studieInstance = Studie.get(params.id)

        if (studieInstance != null) {
            render template: "dialog/editTitleDialog", model: [studie: studieInstance]
        }
        else {
            flash.message = "sorry studie not found!"
            redirect(action: "list")

        }
    }

    /**
     * save the new title
     */
    def ajaxSaveStudieTitle = {

        Studie studie = Studie.get(params.id)
        studie.title = params.title
        studie.description = params.title
        studie.save()
        render studie.description
    }

    def displayClasses = {
        Studie studie = Studie.get(params.id)

        [studieInstance: studie]
    }


    def displayFiles = {
        Studie studie = Studie.get(params.id)

        [studieInstance: studie]
    }



    def displayResults = {
        Studie studie = Studie.get(params.id)

        [studieInstance: studie]
    }

    /**
     * simple check if a design exist
     */
    def ajaxDesignExist = {

        def design = StudieDesign.findByGeneratedStudie(Studie.get(params.id))
        //design exist
        if(design){
            render "true"
        }
        //design was somehow deleted
        else{
            render "false"
        }
    }

}

