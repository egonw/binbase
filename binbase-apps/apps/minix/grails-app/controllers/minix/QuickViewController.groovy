package minix

class QuickViewController {

    /**
     * takes the url and renders it in a view
     */
    def quickView = {
        def url = params.url

        [url: url]
    }
}
