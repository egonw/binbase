package minix

class PlatformController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static scaffold = true
}
