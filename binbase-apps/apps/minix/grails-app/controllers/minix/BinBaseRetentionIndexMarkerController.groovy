package minix

import core.BinBaseSystemService
import groovy.sql.Sql

/**d
 * shows the binbase retention index controllers
 */
class BinBaseRetentionIndexMarkerController {

    BinBaseSystemService binBaseSystemService

    QualityControlService qualityControlService

    /**
     * displays all the retention index markers properties in a table
     */
    def showRetentionIndexMarkers = {

        [columns:binBaseSystemService.availableColumns]
    }

    def ajaxLoadMarkers = {
        String db = params.db

        Sql sql =  qualityControlService.createBinBaseConnection(db)

        def result = []
        sql.eachRow("select name,retention_index, b.* from bin a, standard b where a.bin_id = b.bin_id order by retention_index"){
            def row = [name:it.name,retentionIndex:it.retention_index,qualifier:it.qualifier,binId:it.bin_id,minRatio:it.min_ratio,maxRatio:it.max_ratio,apexSn:it.min_apex_sn,minDistanceRatio:it.min_distance_ratio,maxDistanceRatio:it.max_distance_ratio,similarity:it.min_similarity,required:it.required]
            result.add(row)
        }

        sql.close()

        render template: "renderTable", model: [result:result]
    }

}

