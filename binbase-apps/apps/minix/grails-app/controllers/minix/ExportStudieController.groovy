package minix

import de.andreasschmitt.export.ExportService
import core.MetaDataService
import minix.generate.DSL

/**
 * uses to export a studie as excel or other things
 */
class ExportStudieController {

    ExportService exportService

    MetaDataService metaDataService

    def scaffold = false

    /**
     * exports the complete database as csv
     */
    def exportDatabaseAsCSV = {

        if (!ShiroUser.hasManagmentRights()) {
            flash.message = "sorry you do not have the permission to access this operation!"
            redirect(action: "list", controller: "studie")
        }
        else {
            exportStudies(Studie.list(), response, "csv", "csv", "minixdump")
        }
    }

    /**
     * export all studies
     * @param studies
     * @param response
     * @param format
     * @param extension
     * @param filename
     * @return
     */
    def exportStudies(def studies, def response, String format, String extension, String filename) {

        //title of the file
        def parameters = [title: "Study Design"]

        //required labels and mappings
        def label = [
                "classId": "Class Id",
                "comment": "Comment",
                "fileName": "Filename",
                "label": "Label",
                "organName": "Organ",
                "speciesName": "Species",
                "treatment": "Treatment"
        ]

        //required fields
        def fields = [
                "classId",
                "comment",
                "fileName",
                "label",
                "organName",
                "speciesName",
                "treatment"
        ]
        //contains out data for the save
        def data = []

        studies.each {Studie study ->

            //fill our object with data
            study.classes.each {BaseClass clazz ->

                if (clazz instanceof ExperimentClass) {
                    clazz.samples.each {Sample sample ->

                        StudieExportObject exportObject = new StudieExportObject()

                        exportObject.classId = clazz.id
                        exportObject.speciesName = clazz.species.name
                        exportObject.organName = clazz.organ.name
                        exportObject.treatment = clazz.treatmentSpecific.toParentString()
                        exportObject.fileName = "${sample.fileName}_${sample.fileVersion}"
                        exportObject.comment = sample.comment
                        exportObject.label = sample.label

                        data.add(exportObject)

                    }
                }
            }
        }

        //set the extension to xls
        response.setHeader("Content-disposition", "attachment; filename=${filename}.${extension}")

        exportService.export(format, response.outputStream, data, fields, label, [:], parameters)

    }

    /**
     * export the study as xls file
     */
    def exportStudieToXLS = {

        Studie study = Studie.get(params.id)

        if (study == null) {
            flash.message = "sorry this object wasn't found!"
            redirect(action: "list", controller: "studie")
        }
        else if (!study.canAccess()) {
            flash.message = "sorry you do not have the permission to access this object!"
            redirect(action: "list", controller: "studie")
        }
        else {

            //run the export
            exportStudies([study], response, "excel", "xls", study.id.toString())
        }
    }

    /**
     * export the study as dsl file
     */
    def exportStudieToDSL = {

        Studie study = Studie.get(params.id)

        if (study == null) {
            flash.message = "sorry this object wasn't found!"
            redirect(action: "list", controller: "studie")
        }
        else if (!study.canAccess()) {
            flash.message = "sorry you do not have the permission to access this object!"
            redirect(action: "list", controller: "studie")
        }
        else {
            String xml = metaDataService.generateMetaDataAsXml(study)

            log.debug(xml)
            String database = study.databaseColumn

            //it was not exported yet, so there can't be a database
            if (database == null) {
                database = "NONE"
            }

            String content = new DSL().dslGenerateForExperimentWithDefaultSOP(study)

            log.debug(content)
            response.setContentType("text/plain")
            response.setHeader("Content-disposition", "attachment; filename=${study.id}.dsl")

            response.outputStream << content

        }
    }

    /**
     * export the studie as xml file
     */
    def exportStudieToXML = {

        Studie studie = Studie.get(params.id)

        if (studie == null) {
            flash.message = "sorry this object wasn't found!"
            redirect(action: "list", controller: "studie")
        }
        else if (!studie.canAccess()) {
            flash.message = "sorry you do not have the permission to access this object!"
            redirect(action: "list", controller: "studie")
        }
        else {
            response.setContentType("text/xml")
            response.setHeader("Content-disposition", "attachment; filename=${studie.id}.xml")

            String content = metaDataService.generateMetaDataAsXml(studie)

            response.outputStream << content
        }
    }


}

class StudieExportObject {
    String classId

    String speciesName

    String organName

    String fileName

    String treatment

    String comment

    String label

}