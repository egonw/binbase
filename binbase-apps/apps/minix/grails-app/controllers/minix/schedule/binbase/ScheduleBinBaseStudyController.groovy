package minix.schedule.binbase

import edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.*
import core.BinBaseSchedulingService
import core.BinBaseSystemService
import core.SopService
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling
import grails.converters.JSON
import minix.FileType
import minix.Studie
import minix.file.FileService
import minix.generate.DSL
import org.jdom.Element
import org.jdom.output.Format
import org.jdom.output.XMLOutputter

/**
 * schedules binbase specific studies using Experiment Classes and Experiments
 */
class ScheduleBinBaseStudyController {

    BinBaseSchedulingService binBaseSchedulingService

    BinBaseSystemService binBaseSystemService

    ExportJMXFacade binbaseExportConfiguration

    SopService sopService

    FileService fileService

    boolean dslBased = true

    /**
     * little helper view to schedule all studies in a selected database
     */
    def scheduleDataBase = {
        if (params.database == null) {
            return [databases: binBaseSystemService.availableColumns]
        } else {
            def studies = []

            Studie.findAllByDatabaseColumn(params.database).each { Studie studie ->
                studies.add(studie.id)
            }
            redirect(controller: "scheduleStudy", action: "schedule", params: [study_id: studies])
        }
    }

    /**
     * schedules a study including progress bars etc
     */
    def schedule = {

        log.debug("working on scheduling this study to binbase...")
        String progressId = params.progressId

        try {

            session[progressId] = 0

            long id = params.id as long


            log.debug("id is: ${id}")
            String database = params.destination
            String reference = params.reference

            int counter = 0;

            //generate our experiment to be scheduled
            Experiment experiment = assembleExperiment(database, id, params,counter,progressId)
            counter = experiment.classes.length
            int steps = (experiment.classes.length + 1) *2


            //we are sending samples as dsl to binbase, so let's generate our DSL
            if (dslBased) {

                String dslContent = new DSL().dslGenerateForExperimentWithDefaultSOP(experiment,reference,true,binbaseExportConfiguration.isEnableCache())

                edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL dsl = new edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL();
                dsl.content = dslContent
                dsl.id = "${id}"

                log.debug("scheduling dsl: \n\n\n${dslContent}\n\n\n")

                binBaseSchedulingService.scheduleDSL(dsl)
            }
            //we are sending samples in form of classes and experiments to binbase, so lets send them
            else {


                experiment.classes.eachWithIndex { ExperimentClass entry, int i ->
                    experiment.classes[i] = entry
                    log.info("working on: ${entry.id}")
                    binBaseSchedulingService.scheduleImport(entry)
                    increaseProgress(counter, steps, progressId)
                    counter = counter + 1
                }

                //send experiment to binbase


                log.debug("sending experiment: ${experiment.id} to database ${experiment.column}")
                if (experiment.classes.length > 0) {

                    if (!reference.isEmpty()) {
                        experiment = referencedBased(Studie.get(reference.split(":")[0].toLong()), experiment)
                    }

                    binBaseSchedulingService.scheduleExport(experiment)
                } else {
                    throw new Exception("we did not find any raw data for this experiment!")
                }

            }
            //finish the progress bar
            session[progressId] = 100

            log.debug("scheduling is done...")
            //telling the system we are done

            render "scheduled"
        } catch (Exception e) {
            log.error(e.getMessage(), e)
            session[progressId] = -1
            response.status = 500
            render text: "${e.getMessage()}"
        }
    }

    /**
     * assembles our binbase experiment based on the defined classes
     * @param database
     * @param id
     * @param params
     * @return
     */
    private Experiment assembleExperiment(String database, long id, def params,int counter, String progressId) {
        Experiment experiment = new Experiment()

        experiment.setColumn(database)
        experiment.id = "${id}"


        Map classes = JSON.parse(params.classes)
        def finalClasses = []

        int steps = classes.size()+1
        classes.eachWithIndex { clazz, samples, classIndex ->

            //check that we got some samples
            ExperimentClass myClass = new ExperimentClass();
            def finalSamples = []

            samples.eachWithIndex { String fileName, int sampleIndex ->
                ExperimentSample finalSample = new ExperimentSample()
                finalSample.id = fileName
                finalSample.name = fileName

                if (fileService.hasFile(fileName, FileType.BINBASE_TXT)) {
                    finalSamples[sampleIndex] = finalSample
                } else {
                    log.warn("missing file: ${fileName}")
                }
            }
            myClass.id = clazz
            myClass.samples = new ExperimentSample[finalSamples.size()]
            myClass.column = database

            finalSamples.eachWithIndex { ExperimentSample entry, int i ->
                myClass.samples[i] = entry
            }


            if (myClass.samples.length > 0) {
                finalClasses.add(myClass)
                log.debug("build class: ${myClass.id} with ${myClass.samples.length} samples")
            } else {
                log.warn("ignored class: ${myClass.id} since it had 0 samples")
            }

            increaseProgress(counter, steps, progressId)
            counter = counter + 1

        }

        //finish building our experiment and sending it to binbase
        experiment.classes = new ExperimentClass[finalClasses.size()];

        finalClasses.eachWithIndex { ExperimentClass entry, int i ->
            experiment.classes[i] = entry
        }
        experiment
    }

    /**
     * builds a reference based experiment
     * @param referenceStudie
     * @param exp
     * @return
     */
    private Experiment referencedBased(Studie referenceStudie, Experiment exp) {
//we need to modify and generate a new sop

        Element sopXML = XmlHandling.readXml(new ByteArrayInputStream(sopService.getDefaultSopXML().bytes))

        sopXML.getChildren("transform").each { Element child ->
            if (child.getChildren("reference") != null && child.getChildren("reference").size() > 0) {
                child.removeChildren("reference")
            }

            Element reference = new Element("reference")
            reference.setAttribute("experiment", referenceStudie.id.toString())

            child.addContent(reference)
        }

        String newSop = new XMLOutputter(Format.getPrettyFormat()).outputString(sopXML)
        String id = exp.getId() + "_ref_" + referenceStudie.id + "_" + System.currentTimeMillis() + ".stat"

        sopService.uploadSopFile(id, newSop)

        log.info("uploaded new sop file with name: ${id} and content \n\n ${newSop}")
        exp.setSopUrl(id)


        return exp;
    }

    /**
     * increases the progress and should really be a dedicated controller
     * @param counter
     * @param steps
     * @param progressId
     */
    private void increaseProgress(int counter, int steps, String progressId) {

        double progress = counter / steps * 100

        //building our class object
        session[progressId] = progress
    }
}
