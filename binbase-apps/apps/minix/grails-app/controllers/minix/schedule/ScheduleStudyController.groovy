package minix.schedule

import binbase.core.BinBaseConfigReader
import core.BinBaseSystemService
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import grails.converters.JSON
import minix.ShiroUser
import minix.Studie
import minix.file.FileService
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

/**
 * generic controller to schedule a study
 */
class ScheduleStudyController {

    FileService fileService

    BinBaseSystemService binBaseSystemService

    def index = {
        redirect action: 'schedule'
    }
    /**
     * initialize the scheduling
     */
    def schedule = {
        if (params.userId != null) {
            return [studies: ShiroUser.get(params.userId).getStudies()]
        } else {
            if (params.id != null) {
                return [studies: [Studie.get(params.id)]]
            } else if (params.study_id != null) {
                return [studies: readStudiesFromParams(params)]
            }
        }
    }

    /**
     * defines the required processing instructions for this study
     */
    def defineProcessingInstructions = {
        HashSet studies = readStudiesFromParams(params)

        if (studies.isEmpty())
            redirect action: "schedule"

        def columns = new HashSet(binBaseSystemService.availableColumns)


        [studies: studies, databases: columns]
    }

    /**
     * validates our selected stu
     */
    def defineReferenceInstructions = {
        HashSet studies = readStudiesFromParams(params)

        if (studies.isEmpty())
            redirect action: "schedule"

        Map databases = readDatabasesFromParams(params, studies)

        [studies: studies, destination: databases, ignoreBlanks: params.ignoreBlanks, ignoreQC: params.ignoreQC, ignoreMissingSamples: params.ignoreMissingSamples]
    }

    /**
     * verfies all our studies and ensures the files exist
     */
    def verifyStudies = {
        HashSet studies = readStudiesFromParams(params)

        if (studies.isEmpty())
            redirect action: "schedule"

        Map databases = readDatabasesFromParams(params, studies)
        Map references = readReferencesFromParams(params, studies)

        [studies: studies, destination: databases, references: references, ignoreBlanks: params.ignoreBlanks == "on", ignoreQC: params.ignoreQC == "on", ignoreMissingSamples: params.ignoreMissingSamples == "on"]

    }

    /**
     * adds a study to be scheduled
     */
    def ajaxAddStudyToSchedule = {

        HashSet studies = readStudiesFromParams(params)

        render(template: "selectStudie", model: [studies: studies, progressId: params.progressId])
    }

    /**
     * reads all the studies in the params dataset
     *
     * @param params
     * @return
     */
    private HashSet readStudiesFromParams(GrailsParameterMap params) {
        def studies = new HashSet()

        if (params.study.toString() != "" && params.study != null) {
            def studyId = Long.parseLong(params.study.toString().split(":")[0].toString())
            def recentAddedStudie = Studie.get(studyId)
            studies.add(recentAddedStudie)

        }
        if (params.study_id instanceof String) {
            studies.add(Studie.get(Long.parseLong(params.study_id.toString())))

        } else {
            params.study_id.each { String s ->
                studies.add(Studie.get(Long.parseLong(s)))
            }
        }
        return studies
    }

    /**
     * reads t
     * @param params
     * @param studies
     * @return
     */
    private Map readDatabasesFromParams(GrailsParameterMap params, Set studies) {
        def result = [:]

        studies.each { Studie studie ->
            String database = params."${studie.id}_destination"

            result["${studie.id}"] = database
        }

        return result
    }

    private Map readReferencesFromParams(GrailsParameterMap params, Set studies) {
        def result = [:]

        studies.each { Studie studie ->
            String database = params."${studie.id}_refrence"

            result["${studie.id}"] = database
        }

        return result
    }

    /**
     * simple check if the study was already calculated on the binbase side of the system
     */
    def ajaxStudieExistInDatabase = {

        if (params.referenceStudy.toString() != "") {
            params.referenceStudy = params.referenceStudy.toString().split(":")[0].toString()
        }

        def database = params.column
        def study = params.referenceStudy

        Experiment exp = binBaseSystemService.getBinbaseEjbService().getExperiment(database, study, BinBaseConfigReader.key)

        if (exp.classes.size() == 0) {
            render "false"
        } else {
            render "true"
        }
    }

    /**
     * verfies the samples and schedules the calculation
     */
    def ajaxVerifyAndSchedule = {
        //reset the session for this id
        session[params.sessionId] = 0

        boolean ignoreMissing = Boolean.parseBoolean(params.ignoreMissingSamples)

        double countOfAllSamples = 0
        double counter = 0

        //calculate the count of all samples
        params.keySet().each { String key ->
            if (key.startsWith("study_count_")) {

                countOfAllSamples = countOfAllSamples + (params.get(key) as double)
            }
        }

        def missingFiles = [];

        def toSchedule = []

        //work on the actual studies
        params.keySet().each { String key ->

            if (key.startsWith("study_content_")) {
                String json = params.get(key)
                long id = key.replaceFirst("study_content_", "") as double

                Studie study = Studie.get(id)

                //classes for our study
                def classes = [:]

                //verfiy and build our class objects
                JSON.parse(json).each { v ->

                    def clazz = classes.get(v.classId)

                    if (clazz == null) {
                        clazz = []
                    }

                    //current progress
                    double progress = counter / countOfAllSamples * 100
                    boolean hasTxtFile = fileService.hasFile(v.fileName, study.architecture.rawDataFile)

                    log.debug("has txt file: ${v.fileName} - ${hasTxtFile}")
                    //if there is no text file
                    if (!hasTxtFile) {
                        missingFiles.add(v.fileName)
                    }
                    //add it to the sample instructions
                    else {
                        //adding fileName to our result list
                        clazz.add(v.fileName)
                    }

                    counter = counter + 1

                    classes.put(v.classId, clazz)
                    //update the progress bar
                    session[params.sessionId] = progress
                }

                //finalized added studies
                toSchedule.add(
                        [study: study, classes: classes as JSON, url: g.createLink(controller: study.architecture.relatedController, action: study.architecture.relatedAction), destination: params."study_destination_${study.id}", reference: params."study_reference_${study.id}"]
                )
            }
        }
        //set it to 100 to be done with it
        session[params.sessionId] = 100

        //we are missing files and need interaction
        if (missingFiles.size() > 0 && !ignoreMissing) {
            session[params.sessionId] = -1
            response.status = 500
            render text: missingFiles as JSON
            return
        } else {
            render template: "scheduleView", model: [toSchedule: toSchedule]
        }
    }

    /**
     * used to render progress bar values
     */
    def ajaxRenderProgress = {
        try {
            if (session[params.id]) {
                render "${session[params.id]}"
            } else {
                render "0"
            }
        }
        catch (Exception e) {
            log.error(e.getMessage(), e)
            render "-1"
        }
    }

    /**
     * updates the column for the given studi id
     *
     */
    def ajaxUpdateColumn = {
        Studie studie = Studie.get(params.id)
        studie.databaseColumn = params.destination

        if (studie.validate()) {
            studie.save()
            render "true"
        } else {
            render "false"
        }
    }
}
