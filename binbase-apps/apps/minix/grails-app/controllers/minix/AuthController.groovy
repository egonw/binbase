package minix
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.web.util.SavedRequest
import org.apache.shiro.web.util.WebUtils
import java.security.Security

class AuthController {
    def shiroSecurityManager

    def userService

    def index = { redirect(action: "login", params: params) }

    def login = {
        return [username: params.username, rememberMe: (params.rememberMe != null), targetUri: params.targetUri]
    }

    def signIn = {
        try {
            def authToken = new UsernamePasswordToken(params.username, params.password)

            // Support for "remember me"
            if (params.rememberMe) {
                authToken.rememberMe = true
            }

            // If a controller redirected to this page, redirect back
            // to it. Otherwise redirect to the root URI.
            def targetUri = params.targetUri ?: "/"

            // Handle requests saved by Shiro filters.
            def savedRequest = WebUtils.getSavedRequest(request)
            if (savedRequest) {
                targetUri = savedRequest.requestURI - request.contextPath
                if (savedRequest.queryString) targetUri = targetUri + '?' + savedRequest.queryString
            }

            // Perform the actual login. An AuthenticationException
            // will be thrown if the username is unrecognised or the
            // password is incorrect.
            SecurityUtils.subject.login(authToken)

            flash.message = message(code: "login.successful")

            log.info "Redirecting to '${targetUri}'."


            ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

            session.currentUser = user

            redirect(uri: targetUri)
        }
        catch (AuthenticationException ex) {
            // Authentication failed, so display the appropriate message
            // on the login page.
            log.info "Authentication failure for user '${params.username}'."


            flash.message = message(code: "login.failed")

            userService.logLoginFailed(params.username)

            // Keep the username and "remember me" setting so that the
            // user doesn't have to enter them again.
            def m = [username: params.username]
            if (params.rememberMe) {
                m["rememberMe"] = true
            }

            // Remember the target URI too.
            if (params.targetUri) {
                m["targetUri"] = params.targetUri
            }

            // Now redirect back to the login page.
            redirect(action: "login", params: m)
        }
        catch (Exception e) {
            e.printStackTrace()
            render template: "/shared/exception", model: [exception: e]
        }

        return null
    }

    def signOut = {
        // Log the user out of the application.
        SecurityUtils.subject?.logout()

        flash.message = "you have been logout!"
        // For now, redirect back to the home page.
        redirect(uri: "/")
    }

    def unauthorized = {
        render "You do not have permission to access this page."
    }

    def listLogins = {

        def result = []

        getSession().getServletContext().getAttribute("logins").each{ Long user ->
            result.add(ShiroUser.get(user))
        }

        result.sort()
        [logins: result ]
    }
}
