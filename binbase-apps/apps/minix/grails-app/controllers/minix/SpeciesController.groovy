package minix
class SpeciesController {
    def scaffold = true

    /**
     * uses for code completion
     */
    def searchAJAX = {
        def specs = Species.findAllByNameLike("%${params.query}%")

        //Create XML response
        render(contentType: "text/xml") {
            results() {
                specs.each { spec ->
                    result() {
                        name(spec.name)
                    }
                }
            }
        }
    }

    /**
     * displays the species and all associated designs and studie
     */
    def show = {

        def paginate = params.paginate

        if (paginate == "Studie") {
            def pagination = [max: Math.min(params.max ? params.int('max') : 10, 100), offset: Math.min(params.offset ? params.int('offset') : 0, 100)]
            session.studieSpeciesPagination = pagination
        }
        else if (paginate == "Organ") {
            def pagination = [max: Math.min(params.max ? params.int('max') : 10, 100), offset: Math.min(params.offset ? params.int('offset') : 0, 100)]
            session.organPagination = pagination
        }


        Species species = Species.get(params.id)

        //get all associated queries for this studie
        def studies = Studie.executeQuery("select distinct a from Studie as a, ExperimentClass as b, Species as c where a.id = b.studie.id and b.species.id = c.id and c.id = ? ", [species.id], session.studieSpeciesPagination ?: [max: 10, offset: 0])
        def studieTotal = Studie.executeQuery("select count(distinct a) from Studie as a, ExperimentClass as b, Species as c where a.id = b.studie.id and b.species.id = c.id and c.id = ? ", [species.id])[0]

        //get all assoicated organs for this species
        def organs = Organ.executeQuery("select distinct a from Organ a, IN(a.speciees) b where b.id = ?", [species.id], session.organPagination ?: [max: 5, offset: 0])
        def organsCount = Organ.executeQuery("select count(distinct a) from Organ a , IN(a.speciees) b where b.id = ?", [species.id])[0]

        //reset these variables, they are not needed
        params.offset = null
        params.max = null

        //render the result
        return [organs: organs, speciesInstance: species, studieInstanceList: studies, studieTotal: studieTotal, organTotal: organsCount]
    }

    def ajaxEditName = {
        def value = Species.get(params.id)

        if (value != null) {
            render template: "/dialog/editNameDialog", model: [value: value]
        }
        else {
            flash.message = "sorry species not found!"
            redirect(action: "list")

        }
    }

    /**
     * save the new title
     */
    def ajaxSaveName = {

        Species value = Species.get(params.id)
        value.name = params.title

        if(value.validate() == false){
            log.info value.errors
            render template: "/dialog/editNameDialog", model: [value: value]

        }
        value.save(flush:true)
        render value.name
    }
}
