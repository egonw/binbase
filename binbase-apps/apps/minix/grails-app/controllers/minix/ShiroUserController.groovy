package minix

import minix.roles.RoleUtil
import org.apache.shiro.crypto.hash.Sha256Hash

class ShiroUserController {

    StudieQueryService studieQueryService

    DesignQueryService designQueryService

    UserService userService

    def scaffold = true

    def beforeInterceptor = {

        //only admins are allowed to access this controller

        if (ShiroUser.hasAdminRights() == false) {
            flash.message = "sorry you have no permission to access this data!"

            if (ShiroUser.hasManagmentRights()) {
                redirect(controller: "home", action: "showManagerHome")

            }
            else {
                redirect(controller: "home", action: "showMemberHome")
            }

        }
    }

    /**
     * displays user detais for the given users
     * and there designs and studies
     */
    def show = {
        def userInstance = ShiroUser.get(params.id)


        if (userInstance == null) {
            flash.message = "sorry we did not find the user with this id!"
            redirect(action: "list")
        }
        else {

            def paginate = params.paginate

            if (paginate == "Studie") {
                def pagination = [max: params.max, offset: params.offset]
                session.studieUserPagination = pagination
            }
            else if (paginate == "Design") {
                def pagination = [max: params.max, offset: params.offset]
                session.designUserPagination = pagination
            }
            else if (paginate == null) {
                session.studieUserPagination = null
                session.designUserPagination = null
            }

            params.offset = null
            params.max = null

            def studies = studieQueryService.queryForUser(userInstance, session.studieUserPagination ?: [max: 10, offset: 0])
            def designs = designQueryService.queryForUser(userInstance, session.designUserPagination ?: [max: 10, offset: 0])
            def studieTotal = studieQueryService.queryCountForUser(userInstance)
            def designTotal = designQueryService.queryForUser(userInstance)

            return [shiroUserInstance: userInstance, studieInstanceList: studies, studieDesignInstanceList: designs, studieTotal: studieTotal, studieDesignCount: designTotal]
        }
    }

    /**
     * saves the user and encrypts the password
     */
    def save = {

        ShiroUser shiroUser = userService.createUser(params)

        if (shiroUser.validate()) {


            flash.message = "${message(code: 'default.created.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), shiroUser.id])}"
            redirect(action: "show", id: shiroUser.id)
        }
        else {
            shiroUser.passwordHash = null
            render(view: "create", model: [shiroUserInstance: shiroUser])
        }
    }

    def releaseDesigns = {
        ShiroUser user = ShiroUser.get(params.id)

        if (user == null) {
            flash.message = "sorry we did not find the user with this id!"
            redirect(action: "list")
        }
        else if (ShiroUser.hasAdminRights(user)) {
            flash.message = "sorry this option is not possible for admins!"

            redirect(action: "show", id: user.id)
        }
        else {

            def result = userService.releaseDesigns(user)

            flash.message = "the user has now ${result.studieDesigns.size()} designs associated"

            redirect(action: "show", id: user.id)

        }
    }


    def releaseStudies = {
        ShiroUser user = ShiroUser.get(params.id)

        if (user == null) {
            flash.message = "sorry we did not find the user with this id!"
            redirect(action: "list")
        }
        else if (ShiroUser.hasAdminRights(user)) {
            flash.message = "sorry this option is not possible for admins!"

            redirect(action: "show", id: user.id)
        }
        else {

            def result = userService.releaseStudies(user)

            flash.message = "the user has now ${result.studies.size()} studies associated"

            redirect(action: "show", id: user.id)

        }
    }

    def delete = {
        ShiroUser user = ShiroUser.get(params.id)

        if (user == null) {
            flash.message = "sorry we did not find the user with this id!"
            redirect(action: "list")
        }
        else if (ShiroUser.hasAdminRights(user)) {
            flash.message = "sorry this option is not possible for admins!"

            redirect(action: "show", id: user.id)
        }
        else {

            def result = userService.deleteUser(user)

            if (result instanceof String) {
                flash.message = "there was an error: ${result}"

                redirect(action: "show", id: user.id)
            }
            else if (result) {
                flash.message = "the user was deleted"
                redirect(action: "list")

            }


        }
    }


    def ajaxEvaluateRole = {

        if (params.value != null && params.value.toString().size() > 0 && params.value.toString() != "null") {
            ShiroRole role = RoleUtil.getRole(params.value.toString())

            if (role == RoleUtil.getAdminRole()) {
                render template: "renderAdminDescription"
            }

            else if (role == RoleUtil.getUserRole()) {

                render template: "renderUserDescription"
            }

            else if (role == RoleUtil.getManagerRole()) {

                render template: "renderManagerDescription"
            }


        }
    }

    def list = {

        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [shiroUserInstanceList: ShiroUser.list(params), shiroUserInstanceTotal: ShiroUser.count()]
    }

    def ajaxListFilter = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def username = params.username

        if (username == "") {
            render template: "filteredList", model: [shiroUserInstanceList: ShiroUser.list(params), shiroUserInstanceTotal: ShiroUser.count()]
        }

        else {
            def users = ShiroUser.executeQuery("select a from ShiroUser as a where a.username LIKE ? or a.email LIKE ? or a.firstName LIKE ? or a.lastName LIKE ?", ["%${username}%", "%${username}%", "%${username}%", "%${username}%"], params)
            def userCount = ShiroUser.executeQuery("select a from ShiroUser as a where a.username LIKE ? or a.email LIKE ? or a.firstName LIKE ? or a.lastName LIKE ?", ["%${username}%", "%${username}%", "%${username}%", "%${username}%"])

            render template: "filteredList", model: [shiroUserInstanceList: users, shiroUserInstanceTotal: userCount]

        }
    }

    def update = {

        def shiroUserInstance = ShiroUser.get(params.id)
        if (shiroUserInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (shiroUserInstance.version > version) {

                    shiroUserInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: 'shiroUser.label', default: 'ShiroUser')] as Object[], "Another user has updated this ShiroUser while you were editing")
                    render(view: "edit", model: [shiroUserInstance: shiroUserInstance])
                    return
                }
            }
            shiroUserInstance.properties = params

            //update the user role
            if (params.role != null) {
                if (params.role.toString() != "") {
                    ShiroRole role = RoleUtil.getRole(params.role)

                    if (role != null) {
                        def toRemove = []
                        shiroUserInstance.roles.each {
                            toRemove.add(it)
                        }
                        toRemove.each {
                            shiroUserInstance.removeFromRoles(it)
                            shiroUserInstance.save()

                        }

                        shiroUserInstance.addToRoles(role)
                        role.addToUsers(shiroUserInstance)
                    }
                }
            }

            if (!shiroUserInstance.hasErrors() && shiroUserInstance.save(flush: true)) {

                flash.message = "${message(code: 'default.updated.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), shiroUserInstance.id])}"
                redirect(action: "show", id: shiroUserInstance.id)
            }
            else {
                render(view: "edit", model: [shiroUserInstance: shiroUserInstance])
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), params.id])}"
            redirect(action: "list")
        }
    }

    /**
     * resets the password
     */
    def resetPassword = {

        def shiroUserInstance = ShiroUser.get(params.id)


        if (shiroUserInstance) {
            if (params.password != null && params.password.toString() != "") {

                flash.message = "new password has been set!"

                String password = params.password

                shiroUserInstance.passwordHash = new Sha256Hash(password).toHex()

                if (shiroUserInstance.validate() == false) {
                    log.warn shiroUserInstance.errors
                    flash.message = "sorry there was a validation error with this password!"
                    render(view: "resetPassword", model: [shiroUserInstance: shiroUserInstance])
                }
                else {

                    shiroUserInstance.save(flush: true)
                    redirect(action: "show", params: [id: shiroUserInstance.id])
                }
            }
            else {
                flash.message = "please provide a password!"
                render(view: "resetPassword", model: [shiroUserInstance: shiroUserInstance])

            }


        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), params.id])}"
            redirect(action: "list")
        }

        [shiroUserInstance: shiroUserInstance]
    }

    /**
     * sets if this is a guest account or not
     */
    def ajaxSetGuest = {
        ShiroUser user = ShiroUser.get(params.id)
        user.guest = new Boolean(params.guest)
        user.save()

        render ""
    }

}
