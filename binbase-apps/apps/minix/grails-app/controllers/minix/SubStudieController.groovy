package minix

import jprogress.ProgressService
import minix.progress.ProgressHandler

/**
 * controller to generate and display a substudy
 */
class SubStudieController implements ProgressHandler{

    //associated service to actually create one
    SubStudieService subStudieService

    def scaffold = false

    ProgressService progressService

    StudyRemovalService removalService

    /**
     * creates a new sub study based on the given parameters
     */
    def create = {
        //the created study will be based on the given study
        Studie studie = Studie.get(params.id)

        if (params.message) {
            flash.message = params.message
        }
        if (studie != null) {
            if (!studie.canAccess()) {
                flash.message = "sorry you do not have the permission to access this object!"
                redirect(action: "list", controller: "studie")
            }

            if (studie.publicExperiment == null) {
                studie.publicExperiment = false
                studie.save(flush: true)
            }

            [studieInstance: studie]
        }
        else {
            flash.message = "sorry no study found!"
            redirect(action: "list",controller: "studie")

        }
    }

    /**
     * fires an update for the given session
     */
    def ajaxRenderProgress = {
        try {
            render "${session[params.id]}"
        }
        catch (Exception e) {
            log.error(e.getMessage(), e)
            render "-1"
        }
    }

    /**
     * create the actual study
     */
    def runCreate = {

        def title = params.title
        def samples = []
        def studie = Studie.get(params.originalStudy)


        params.keySet().sort().each {
            if(it.toString().matches("include_sample_.[0-9]*_[0-9]*")){
                long id = it.toString().split("_")[3].toLong()
                samples = samples + Sample.get(id)
            }
        }

        def result = subStudieService.createSubStudie(studie,samples,title,title,false,this,params.sessionId)

        result.save(flush: true)

        render(template: "done", model: [result:result])
    }

    /**
     * redirects to the study view
     */
    def show = {
        redirect(controller: "studie",action: "show",id: params.id)
    }

    /**
     * handles the progress
     * @param id
     * @param percent
     * @param properties
     */
    void handleProgress(String id, double percent, Object properties) {
        progressService.setProgressBarValue(id, percent)

    }

}
