package minix

import grails.converters.JSON
import groovy.sql.Sql

import javax.sql.DataSource
/**
 * provides usefull statistics about the minix system
 */
class MiniXContentController {

    DataSource dataSource

    QualityControlService qualityControlService

    /**
     * renders the count of studies by architecture
     */
    def ajaxRenderStudiesByArchitecture = {

        Sql sql = Sql.newInstance(dataSource)

        def result = []

        sql.eachRow("select count (*) as result, b.name as name from studie a, architecture b where a.architecture_id = b.id group by b.name") {
            result.add(label:it.name,data:it.result)
        }

        render result as JSON
    }

    /**
     * renders a chart by date and db and instrument and bin id
     */
    def ajaxRenderChartByDateAndDatabaseAndInstrumentAndBinId = {

        Integer binId = params.binId.toString().toInteger()
        Integer ion = params.ion.toString().toInteger()
        String database = params.column.toString()
        String instrument = params.instrument
        long from = params.from.toString().toLong()
        long to = params.to.toString().toLong()

        def data = qualityControlService.getBinValuesForDates(database,instrument,binId,from,to,ion,params.pattern.toString())

        def result = []

        def ratioSeries = []
        data.each { map ->
            ratioSeries.add([map.date.time, map.intensity])

        }
        result.add([label: "intensity", data: ratioSeries, color: "#52A1DD"])

        render result as JSON
    }

    /**
     * renders the data for a char of a bin ion and study
     */
    def ajaxRenderChartByStudyAndBinId = {
        Integer binId = params.binId.toString().toInteger()
        Integer ion = params.ion.toString().toInteger()
        Long study = params.study.toString().toLong()

        def data = qualityControlService.getBinValuesForStudy(binId,ion,study)

        def result = []

        def ratioSeries = []
        data.each { map ->
            ratioSeries.add([map.date.time, map.intensity])

        }
        result.add([label: "intensity", data: ratioSeries, color: "#52A1DD"])

        render result as JSON

    }


    /**
     * renders a chart by date and db and instrument and bin id
     */
    def ajaxRenderChartByDateAndDatabaseAndInstrumentForBinRatio = {

        Integer first = params.first.toString().toInteger()
        Integer second = params.second.toString().toInteger()

        String database = params.column.toString()
        String instrument = params.instrument
        long from = params.from.toString().toLong()
        long to = params.to.toString().toLong()

        def data = qualityControlService.getBinRatioValuesForDates(database,instrument,first,second,from,to,params.pattern.toString())

        def result = []

        def ratioSeries = []
        data.each { map ->
            ratioSeries.add([map.date.time, map.ratio])

        }
        result.add([label: "ratio", data: ratioSeries, color: "#52A1DD"])

        render result as JSON
    }


}
