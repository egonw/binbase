package minix
class SearchableIndexController {

  def searchableService

  def reindex = {
    render template: "reindex"
  }

  def reindex_ajax = {
    render template: "reindex"
  }

  def reindexExecute_ajax = {
    searchableService.indexAll()

    render("<div class='message'>indexing is done</div>")
  }
}
