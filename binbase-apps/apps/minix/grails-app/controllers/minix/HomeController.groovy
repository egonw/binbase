package minix

import minix.step.Step
import org.apache.shiro.SecurityUtils
import javax.naming.Context
import javax.naming.InitialContext
import binbase.core.BinBaseConfigReader

class HomeController {

    StudieQueryService studieQueryService

    DesignQueryService designQueryService

    /**
     * by default redirect to showPublicHome
     */

    def index = {
        ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

        if (ShiroUser.hasAdminRights(user)) {

            redirect(action: showAdminHome)
        } else if (ShiroUser.hasManagmentRights(user)) {

            redirect(action: showManagerHome)
        } else {

            redirect(action: showMemberHome)
        }
    }

    def showManagerHome = {
        ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)


        [
                studieInstanceList: studieQueryService.queryRecentStudies(user),
                studieCalculatedList: studieQueryService.queryRecentCalculatedStudies(user),

                studieDesignInstanceList: designQueryService.queryRecentDesign(user)
        ]


    }

    def showAdminHome = {
        ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

        String ip = BinBaseConfigReader.getServer()

        [
                applicationServerIp: ip,
                studieInstanceList: studieQueryService.queryRecentStudies(user),
                studieCalculatedList: studieQueryService.queryRecentCalculatedStudies(user),

                studieDesignInstanceList: designQueryService.queryRecentDesign(user)
        ]


    }

    /**
     * shows the home directory of a member
     */
    def showMemberHome = {
        ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

        def result = studieQueryService.queryRecentStudies(user)
        def count = studieQueryService.queryCountForUser(user)

        [studieInstanceList: result, studieCount: count, studieCalculatedList: studieQueryService.queryRecentCalculatedStudies(user),

                studieDesignInstanceList: designQueryService.queryRecentDesign(user)]

    }

}
