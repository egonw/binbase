package minix

import org.apache.commons.io.IOUtils
import org.apache.shiro.SecurityUtils

/**
 * used to generate aquisition tables
 *
 */
class AquisitionTableController {

    AquisitionTableService aquisitionTableService

    AttachmentService attachmentService

    GenerateFileNameService generateFileNameService

    def scaffold = false
    /**
     * used for the progressbar rendering
     */
    def progressService

    /**
     * creates a new aquisition table
     */
    def create = {
        Studie studie = Studie.get(params.id)

        if (studie == null) {
            flash.message = "sorry, we were not able to find to the given studie."
            redirect controller: "studie", action: "list"
        }
        else {
            return [studie: studie]
        }
    }

    def ajax_renderMethodPage = {
        def instrument = Instrument.get(params.id)

        if (instrument instanceof GCTof) {
            render template: "renderGCTof", model: [gctof: instrument]
        }
        else if (instrument == null) {
            render """<div class="message">please select an instrument</div>"""

        }
        else {
            render """<div class="message">sorry your instrument is not supported at this point in time</div>"""
        }
    }

    /**
     * specific for the generation of a gctof aquistion table
     */
    def generate_gctofs_with_progress = {

        log.info(params)

        Studie studie = Studie.get(params.studieId)
        GCTof gctof = GCTof.get(params.gctofId)

        int qualitycontrolInterval = 10


        boolean shuffle = false

        if (params."randomize_table".toString().toLowerCase() == "on"){
            shuffle = true
        }

        def factor = 2
        if (params."generate_samples".toString().toLowerCase() == "on") {


            int day = Integer.parseInt(params.calculationDate_day)
            int month = Integer.parseInt(params.calculationDate_month)
            int year = Integer.parseInt(params.calculationDate_year)

            Date date =  Date.parse( "yyyy-M-d", "${year}-${month}-${day}" )

            ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

            generateFileNameService.generateFileNames(studie, gctof, date, user, params.progressId, factor,shuffle)

            studie.save(flush: true)

            if (params."generate_calibration_class".toString().toLowerCase() == "on") {

                generateFileNameService.addCalibrationSamples(studie, gctof, date, user)

                studie.save(flush: true)
            }

            if (params."generate_quality_class".toString().toLowerCase() == "on") {

                generateFileNameService.addQualityControlSamples(studie, gctof, date, user)

                studie.save(flush: true)
            }
            if (params."generate_reaction_blank_class".toString().toLowerCase() == "on") {

                generateFileNameService.addReactionBlankSamples(studie, gctof, date, user)

                studie.save(flush: true)
            }
        }
        else {
            factor = 1
        }



        String dp = params.dp
        String ms = params.ms
        String gc = params.gc

        //generates the actual studie for us
        def file = aquisitionTableService.generateDataAquisitionTable(studie, gctof, dp, ms, gc, "", "", false, params.progressId, factor, qualitycontrolInterval)

        studie.aquisitionTable = file
        studie.save(flush: true)

        //attaches the result to the studie
        FileAttachment attachment = attachmentService.attachFileToStudie(IOUtils.toByteArray(file.toInputStream()), studie, "${studie.id}-aquisitionTable.seq")

        progressService.setProgressBarValue(params.progressId, 100)
        //displays the result
        render template: "displayTable", model: [file: file, attachment: attachment, studie: studie]

    }
}
