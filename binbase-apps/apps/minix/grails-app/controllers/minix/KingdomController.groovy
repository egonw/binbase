package minix

import grails.converters.JSON

class KingdomController {

    def scaffold = true

    /**
     * shows the current view
     */
    def show = {
        def paginate = params.paginate

        if (paginate == "Studie") {
            def pagination = [max: params.max, offset: params.offset]
            session.studiePagination = pagination
        }
        else if (paginate == "Species") {
            def pagination = [max: params.max, offset: params.offset]
            session.speciesPagination = pagination
        }

        Kingdom kingdom = Kingdom.get(params.id)

        //get all associated queries for this studie
        def studies = Studie.executeQuery("select distinct a from Studie as a, ExperimentClass as b, Species as c where a.id = b.studie.id and b.species.id = c.id and c.kingdom.id = ? ", [kingdom.id], session.studiePagination ?: [max: 10, offset: 0])
        def studieTotal = Studie.executeQuery("select count(distinct a) from Studie as a, ExperimentClass as b, Species as c where a.id = b.studie.id and b.species.id = c.id and c.kingdom.id = ? ", [kingdom.id])[0]

        //get all assoicated organs for this species
        def species = Species.executeQuery("select distinct a from Species a where a.kingdom.id = ?", [kingdom.id], session.speciesPagination ?: [max: 15, offset: 0])
        def speciesCount = Organ.executeQuery("select count(distinct a) from Species a where a.kingdom.id = ?", [kingdom.id])[0]

        //reset these variables, they are not needed
        params.offset = null
        params.max = null

        return [kingdomInstance: kingdom, studieInstanceList: studies, studieTotal: studieTotal, species: species, speciessTotal: speciesCount]
    }


    def ajaxEditName = {
        def value = Kingdom.get(params.id)

        if (value != null) {
            render template: "/dialog/editNameDialog", model: [value: value]
        }
        else {
            flash.message = "sorry species not found!"
            redirect(action: "list")

        }
    }

    /**
     * save the new title
     */
    def ajaxSaveName = {

        def value = Kingdom.get(params.id)
        value.name = params.title
        value.save()
        render value.name
    }
}
