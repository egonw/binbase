package minix
class SearchController {

    /**
     * refernces to the searchable plugin
     */

    def searchableService

    /**
     * searches over the database
     */
    def searchAjax = {
        render """<div class="errors">this doesn't work yet</div> """
    }

    /**
     * supposed to show a search entry field and execute the search
     */
    def search = {

        //a query is already set
        if (params.query != null && params.query != "") {
            redirect(action: searchResult, params: params)
        }
        else{
            flash.message = "please enter a query term"
        }

    }

    def searchResult = {

        String query = params.query.toString().trim()

        //reload the data in the index, required since we don't want marshalling
        params.reload = true
        params.max = 30

        if (params.query == "") {
            redirect(action: search)

        }
        else {
            //search the index with the provided options
            def searchResult = searchableService.search(query, params)

            //setting the query to be able to rerun the query
            searchResult.query = query

            def result = searchResult


            [searchResult: result]
        }
    }
}
