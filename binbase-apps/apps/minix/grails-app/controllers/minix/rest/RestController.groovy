package minix.rest
import grails.converters.JSON
import minix.CommunicationsService
import minix.Sample
import minix.Studie
/**
 * a simple rest based interface to the minix system
 */
class RestController {

    /**
     * used as delegate
     */
    CommunicationsService communicationsService

    /**
     * returns all the samples of this study as JSON string, which is an array with each entry representating a sample name
     */
    def listSamplesForStudyId = {

        if(params.id == null || params.id.toString() == ""){
            render (["please provide a numeric id for a study!"] as JSON)
        }
        Studie studie = Studie.findById(params.id)

        def result = []

        studie.classes.each {
            it.samples.each { sample ->
                result.add(sample.generateName())
            }
        }

        render result as JSON

    }

    /**
     * returns all the samples of this study as JSON string, which is an array with each entry representating a sample name
     */
    def listDetailedSamplesForStudyId = {

        if(params.id == null || params.id.toString() == ""){
            render (["please provide a numeric id for a study!"] as JSON)
        }
        Studie studie = Studie.findById(params.id)

        def result = [id:studie.id,name:studie.description,"classes":new Vector()]

        studie.classes.each {

            def clazz = ["id" : it.id,"samples":new Vector()]

            it.samples.each { sample ->
                clazz.samples.add(sample.generateName())
            }

            result.classes.add(clazz)
        }

        render result as JSON

    }

    /**
     * can this sample generate a new bin
     * @param sampleId
     * @return
     */
    def canGenerateNewBin = {
       render (communicationsService.canGenerateNewBin(Long.parseLong(params.id)).toString()) as JSON
    }

    /**
     * returns the metadata for a given samples like species, organ, treatment
     */
    def getMetadataForSample = {

        if(params.id == null || params.id.toString() == ""){
            response.status = 404
            render ([error:"please provide a sample name!"] as JSON)
        }
        else{

            String name = communicationsService.generateSampleName(params.id)

            Sample sample = Sample.findByFileName(name)

            log.info("found sample: ${sample}")

            if(sample){
                def result = [
                        species:sample.experimentClass.species.name,
                        organ:sample.experimentClass.organ.name,
                        treatmentName:sample.experimentClass.treatmentSpecific.treatment.description,
                        treatmentValue: sample.experimentClass.treatmentSpecific.value,
                        study:sample.experimentClass.studie.description,
                        studyId:sample.experimentClass.studie.id

                ]

                render (result as JSON)
            }
            else{
                response.status = 404
                render ([error:"sorry this sample wans't found: ${name}!"] as JSON)
            }
        }


    }
}