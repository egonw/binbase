package minix

import grails.converters.deep.XML
import org.apache.shiro.SecurityUtils
import minix.step.Step
import minix.step.ThreatmentPasteDialogStep
import org.springframework.dao.DataIntegrityViolationException
import binbase.web.core.Association

/**
 * uses to design a complete experiment based on a couple of questions
 *
 */
class StudieDesignController {

    //most views can be scaffolded
    def scaffold = true

    //injected service for filename generation
    GenerateFileNameService generateFileNameService

    DesignQueryService designQueryService

    ModifyClassService modifyClassService

    def beforeInterceptor = {
        StudieDesign design = StudieDesign.get(params.designId)

        if (params.action == "ajaxRenderProgress") {
            //do nothing

        } else if (design != null && params.action != "designFileNames" && params.action != "designDone") {

            if (design.generatedStudie != null) {

                session.message = "sorry this studie design already generated a studie! This is your designed studie!"
                redirect(action: "designFileNames", params: [designId: params.designId])
            }
        }


        if (session.message) {
            flash.message = session.message
            session.message = null
        }
    }
    /**
     * render species
     */
    def ajaxRenderSpecies = {

        //reads all the species out of the params object
        Collection values = readSpecies(params)

        //render the select species field
        render(template: "selectSpecies", model: [species: values])
    }

    /**
     * deletes a treatment
     */
    def ajaxRemoveTreatment = {

        def value = params.id.toString().split("_")[1].toLong()

        StudieDesign design = StudieDesign.get(params.designId)
        TreatmentSpecific specific = TreatmentSpecific.get(value)

        //remove association from design
        design.removeFromTreatmentsSpecifics specific
        design.save(flush: true)

        //remove from parent
        if (specific.parent != null) {
            TreatmentSpecific parent = specific.parent
            parent.removeFromSubTreatmentSpecifics specific
            parent.save(flush: true)
        }

        //do the actual delete
        try {
            specific.delete(flush: true)
            flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'TreatmentSpecific.label', default: 'treatment with id: '), value])}"
        }
        catch (DataIntegrityViolationException e) {

            flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'TreatmentSpecific.label', default: 'treatment with id: '), value])}"

        }
        //render the select species field
        render(template: "designTreatments", model: [design: design])
    }
    /**
     * render the treatments
     */
    def ajaxRenderTreatments = {

        StudieDesign design = StudieDesign.get(params.designId)

        //reads all the species out of the params object
        Collection values = readTreatmentSpecifics(params)

        //assign the treatments
        assignTreatmentsToDesign(values, design)

        //render the select species field
        render(template: "selectTreatments", model: [design: design])

    }

    /**
     * read all the species from the params file
     * @param params
     * @return
     */
    protected Collection readSpecies(Map params) {
        Set values = new TreeSet()

        flash.message = ""

        Kingdom kingdom = Kingdom.findByName("None");

        if (kingdom == null) {
            kingdom = new Kingdom()
            kingdom.name = "None"
            kingdom.save(flush: true)
            log.info "generated default kingdom..."
        }
        //go over all attributes to find the one we need
        params.eachWithIndex { def value, int index ->

            //filter the attributes
            if (value.toString().startsWith("species_")) {
                String name = value.value

                if (name.length() > 0) {
                    //find the species by name
                    Species species = Species.findByName(name)

                    //if the species is null, we need to create it
                    if (species == null) {
                        species = new Species()
                        species.name = name
                        species.kingdom = kingdom

                        //validate that the species is acceptable
                        if (species.validate()) {
                            species.save(flush: true)
                        }
                    }

                    //make sure that species are unique
                    if (!values.contains(species)) {
                        values.add(species)
                    } else {
                        //duplicate species have been removed
                        flash.message = "duplicated species have been removed"
                    }
                } else {
                    flash.message = "empty fields will be ignored"

                }
            }
        }
        return values
    }

    /**
     * read all the species from the params file
     * @param params
     * @return
     */
    protected Collection readTreatmentSpecifics(Map params) {
        Collection values = new TreeSet()

        //go over all attributes to find the one we need
        params.eachWithIndex { def value, int index ->

            //filter the attributes
            if (value.toString().startsWith("treatment_name_")) {
                String name = value.value
                String fieldId = value.key.toString().split("_")[2]

                //access the id if it exist
                def treatmentSepcificValue = params."treatment_value_${fieldId}"

                //access the value if it exist
                def treatmentSepcificId = params."treatment_value_id_${fieldId}"

                if (name.length() > 0) {

                    //get our related treatment for the given name
                    Treatment treatment = defineTreatment(name)

                    //creates a specific treatment
                    TreatmentSpecific specific = createTreatmentSpecific(treatmentSepcificId, treatmentSepcificValue, treatment)

                    //call the validation
                    if (specific.validate()) {
                        specific.save(flush: true)
                    } else {
                        log.info "specific is invalid... ${specific.id} - ${specific.errors}"
                    }
                    values.add(specific)
                }
            }
        }
        return values
    }

    protected TreatmentSpecific createTreatmentSpecific(treatmentSepcificId, treatmentSepcificValue, Treatment treatment) {
        TreatmentSpecific specific = null
        //ok an id exist
        if (treatmentSepcificId.toString().size() > 0) {

            //load the treatment with the id
            specific = TreatmentSpecific.get(treatmentSepcificId.toString().toLong())
        }
        //ok we need to generate a new specific treatment
        else {
            //time to create a new treatment specific
            specific = new TreatmentSpecific()
        }

        //assign our values
        specific.setValue(treatmentSepcificValue)
        specific.treatment = treatment

        //assign to the specific
        treatment.addToSpecifics(specific)
        treatment.save(flush: true)
        return specific
    }

    protected Treatment defineTreatment(String name) {
        Treatment treatment = Treatment.findByDescription(name)

        //if the species is null, we need to create it
        if (treatment == null) {
            treatment = new Treatment()
            treatment.description = name

            //validate that the species is acceptable
            if (treatment.validate()) {
                treatment.save(flush: true)
            }
        }
        return treatment
    }

    /**
     * reads all the organs from the params for the specied species name
     * @param params
     * @return
     */
    protected Collection readOrgans(Map params, Species species) {
        Collection values = new TreeSet()

        flash.message = ""

        //go over all attributes to find the one we need
        params.eachWithIndex { def value, int index ->

            //filter the attributes
            if (value.toString().startsWith("organ_${species.id}_")) {
                String name = value.value

                if (name.length() > 0) {
                    //find the species by name
                    Organ organ = Organ.findByName(name)

                    //if the species is null, we need to create it
                    if (organ == null) {
                        organ = new Organ()
                        organ.name = name

                        //validate that the species is acceptable
                        if (organ.validate()) {

                            organ.addToSpeciees(species)
                            species.addToOrgans(organ)
                            organ.save(flush: true)
                            species.save(flush: true)
                        }
                    } else {
                        //make sure the species organ relation ship is defined
                        if (organ.speciees.contains(species) == false) {
                            organ.addToSpeciees(species)
                            species.addToOrgans(organ)
                            organ.save(flush: true)
                            species.save(flush: true)
                        }
                    }

                    //make sure that species are unique
                    if (!values.contains(organ)) {
                        values.add(organ)
                    } else {
                        //duplicate species have been removed
                        flash.message = "duplicated organs have been removed"
                    }
                } else {
                    flash.message = "empty fields will be ignored"

                }
            }
        }
        return values
    }
    /**
     * saves the current design
     */
    def ajaxSaveStudie = {

        //assign our species
        def species = readSpecies(params)

        //create studie design
        StudieDesign design = StudieDesign.get(params.designId)

        if (design == null) {
            design = new StudieDesign()
            design.numberOfSamples = 0

            ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

            design.user = user
        }

        design.title = params.title
        design.architecture = Architecture.findByName(params.architecture)

        //assign species data
        species.each {
            design.addToSpeciees(it)
        }

        design.step = Step.DEFINE_SPECIES

        //defautl validation
        boolean valid = design.validate()

        //custom validation
        if (species.size() == 0) {
            design.errors.rejectValue("speciees", "message.code", "you need to define at least one species!")

            valid = false
        }

        //if it's valid we move on
        if (valid) {
            design.step = Step.DEFINE_ORGAN
            design.save(flush: true)
            render(template: "designOrgans", model: [design: design])
        }
        //otherwise we render the mistakes
        else {
            render(template: "designStudie", model: [design: design, architectures: Architecture.list()])
        }
    }

    /**
     * saves a sub treatment
     */
    def ajaxSaveSubtreatment = {
        StudieDesign design = StudieDesign.get(params.designId)

        String name = params.sub_treatment_name_0
        String value = params.sub_treatment_value_0

        Treatment treatment = defineTreatment(name)
        TreatmentSpecific subSpecific = createTreatmentSpecific("", value, treatment)
        TreatmentSpecific parent = TreatmentSpecific.get(params.sub_treatment_parent_id_0)

        parent.addToSubTreatmentSpecifics(subSpecific)

        subSpecific.parent = parent
        subSpecific.save(flush: true)
        parent.addToSubTreatmentSpecifics(subSpecific)
        parent.save(flush: true)
        treatment.save(flush: true)

        render(template: "selectTreatments", model: [design: design])

    }
    /**
     * saves our organs and continues
     */
    def ajaxSaveOrgans = {

        //retrieve the design
        StudieDesign design = StudieDesign.get(params.designId)

        //validate the design
        boolean valid = design.validate()

        //go over each speices
        design.speciees.each { Species specie ->
            //find organs for species
            def organs = readOrgans(params, specie)

            if (organs == null) {
                design.errors.rejectValue("designSpeciesConnections", "studiedesign.save.organ.species", "you need to define at least one organ for the species: ${specie.getName()}!")
                valid = false
            }
            //if no organ found for this species attach another error message
            else if (organs.size() == 0) {
                design.errors.rejectValue("designSpeciesConnections", "studiedesign.save.organ.species", "you need to define at least one organ for the species: ${specie.getName()}!")
                valid = false
            } else {
                assignOrgansToDesign(organs, design, specie)
            }
        }

        //now we renderthe design and highlight the errors
        if (valid == false) {
            design.step = Step.DEFINE_ORGAN
            render(template: "designOrgans", model: [design: design])
        }
        //move on to the treatments
        else {

            design.step = Step.DEFINE_TREATMENTS
            design.save(flush: true)

            render(template: "designTreatments", model: [design: design])

        }

    }

    def ajaxSaveSamples = {
        StudieDesign design = StudieDesign.get(params.designId)
        try {
            design.numberOfSamples = params.numberOfSamples.toString().toInteger()
        }
        catch (NumberFormatException e) {
            design.numberOfSamples = 0
        }
        boolean valid = design.validate()

        if (design.numberOfSamples <= 0) {
            design.errors.rejectValue("numberOfSamples", "studiedesign.save.numberOfSamples", "you need to define a numeric value larger than 0 for the field 'numberOfSamples'!")
            valid = false
        }

        //display classes
        if (valid) {
            design.step = Step.GENERATE_CLASS
            design.save(flush: true)
            render(template: "displayClasses", model: [design: design])

        }
        //try again
        else {
            design.step = Step.DEFINE_CLASS

            render(template: "designSamples", model: [design: design])
        }

    }

    /**
     * save our treatments
     */
    def ajaxSaveTreatments = {

        StudieDesign design = StudieDesign.get(params.designId)

        //validate the design
        boolean valid = design.validate()

        //make sure organs are associated
        if (design.treatmentsSpecifics == null) {
            //provide some organs please
            design.errors.rejectValue("treatmentsSpecifics", "studiedesign.save.treatmentsSpecifics", "you need to define at least one treatment!")
            valid = false
        }

        //reads all the species out of the params object
        Collection values = readTreatmentSpecifics(params)

        assignTreatmentsToDesign(values, design)

        if (values.size() == 0) {
            design.errors.rejectValue("treatmentsSpecifics", "studiedesign.save.treatmentsSpecifics", "you need to define at least one treatment!")
            valid = false
        } else {
            values.each { TreatmentSpecific treat ->
                if (!treat.validate()) {
                    valid = false
                }
            }
        }


        if (valid) {

            design.step = Step.DEFINE_CLASS
            design.save(flush: true)

            render(template: "designSamples", model: [design: design])
        } else {
            design.step = Step.DEFINE_TREATMENTS
            render(template: "designTreatments", model: [design: design])

        }
    }

    /**
     * renders teh reuired form for filename generation and association
     */
    def ajaxGenerateFileNames = {
        StudieDesign design = StudieDesign.get(params.designId)

        //show the dialog
        if (params.id.equals("ajaxGenerateFileNames")) {

            render(template: "generateFileNames", model: [design: design])

        }

        //execute the action
        else if (params.id.equals("ajaxGenerateFileNamesAction")) {

            Date date = new Date(params.calculationDate.toString())
            Instrument instrument = Instrument.get(params."instrument.id")
            Studie studie = design.generatedStudie

            ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

            generateFileNameService.generateFileNames(studie, instrument, date, user)
            design.save(flush: true)

            render(template: "defineFileNames", model: [design: design])

        }
    }

    /**
     * generates the actual classes for a studie
     */
    def ajaxGenerateClasses = {

        StudieDesign design = StudieDesign.get(params.designId)

        if (hasCheckedClasses(params) == false) {
            design.errors.reject("you need to select at least one class to be generated!")

            render(template: "displayClasses", model: [design: design])
        } else {

            def sessionId = params.sessionId
            session[sessionId] = 0

            def steps = 3

            //figure out how many steps we need
            params.eachWithIndex { def val, int index ->
                if (val.toString().startsWith("generate_")) {
                    steps = steps + 1
                }
            }

            //here we are actually going to generate the studie
            log.info "create new studie..."
            Studie studie = new Studie()
            Architecture architecture = design.architecture

            studie.setTitle(design.title)
            studie.setDescription(design.title)
            studie.setName("no name assigned")
            studie.setArchitecture(architecture)
            design.generatedStudie = studie

            architecture.addToStudies(studie)

            log.info("save studie")
            studie.save(flush: true)

            double v = (double) 1 / (double) steps * 100;

            session[sessionId] = v

            if (studie.validate() == false) {
                log.error studie.errors
            } else {

                log.info "adding user rights..."
                //assign the studie to the current user
                ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)
                user.addToStudies(studie)
                studie.addToUsers(user)

                if (studie.validate() == false) {
                    log.error(studie.errors)
                }

                studie.save()
                user.save()

                user.save()
                studie.save()


                if (design.user != user) {
                    design.user.addToStudies(studie)
                    studie.addToUsers(design.user)
                    design.user.save()

                }

                v = (double) 2 / (double) steps * 100;

                session[sessionId] = v

                log.info "generate classes..."
                //generates the actual classes
                int current = 0
                params.eachWithIndex { def val, int index ->

                    //filter the attributes
                    if (val.toString().startsWith("generate_")) {
                        String value = val.value
                        String name = val.key

                        //ok we have an exported class here
                        if (value.length() > 0) {
                            //time to extract the options
                            String[] options = name.split("_")

                            Species species = Species.get(options[1].toLong())
                            Organ organ = Organ.get(options[2].toLong())
                            TreatmentSpecific treatment = TreatmentSpecific.get(options[3].toLong())

                            int count = Integer.parseInt(params."samples_${species.id}_${organ.id}_${treatment.id}".toString())

                            ExperimentClass clazz = new ExperimentClass()
                            clazz.name = "None"
                            clazz.organ = organ
                            clazz.treatmentSpecific = treatment
                            clazz.species = species

                            clazz.studie = studie
                            clazz.experiment = studie

                            studie.addToClasses(clazz)

                            if (clazz.validate() == false) {
                                log.error(clazz.errors)
                            } else {
                                clazz.save(flush: true)
                                clazz.name = "${clazz.id}"
                                modifyClassService.addSamples(clazz, count)
                            }

                            v = (double) (current + 3) / (double) steps * 100;

                            current = current + 1
                            session[sessionId] = v
                        }


                    }
                }
            }

            log.info "save design"
            design.step = Step.ASSIGN_SAMPLES
            design.save(flush: true)

            session[sessionId] = 100

            log.info("render template...")
            render(template: "defineFileNames", model: [design: design, studie: studie])

        }
    }

    /**
     * make sure that at least one class is checked for generation
     * @param params
     * @return
     */
    protected boolean hasCheckedClasses(Map params) {

        boolean success = false
        //go over all attributes to find the one we need
        params.eachWithIndex { def value, int index ->

            //filter the attributes
            if (value.toString().startsWith("generate_")) {
                String name = value.value

                if (name.length() > 0) {
                    success = true
                }

            }
        }

        return success
    }

    /**
     * render select organs
     */
    def ajaxRenderOrgans = {

        //access the design id
        StudieDesign design = StudieDesign.get(params.designId)

        //retrieve the id of the related specie
        long id = params.id.toString().split("_")[1].toLong()

        //retrive the related species
        def species = Species.get(id)

        //all organs for this species
        def organs = readOrgans(params, species)

        //associate organs with design without saving!
        assignOrgansToDesign(organs, design, species)

        //render the related template
        render(template: "selectOrgans", model: [specie: species, design: design])
    }

    /**
     * defines a subtreatment
     */
    def ajaxDefineSubtreatment = {
        long id = params.id.toString().split("_")[1].toLong()

        TreatmentSpecific specific = TreatmentSpecific.get(id)

        render(template: "designSubtreatment", model: [design: design, treatmentSpecific: specific])

    }

    /**
     * assigns the list of organs to the given design without saving
     * @param organs
     * @param design
     * @return
     */
    protected void assignOrgansToDesign(Collection organs, StudieDesign design, Species species) {

        organs.each { Organ organ ->

            def result = DesignSpeciesConnection.findAllByDesignAndSpecies(design, species)

            if (result.size() == 0) {
                DesignSpeciesConnection con = new DesignSpeciesConnection()
                con.species = species
                con.design = design
                con.addToOrgans(organ)
                con.save(flush: true)

                design.addToDesignSpeciesConnections(con)
                design.save(flush: true)
            } else {
                result.each { DesignSpeciesConnection con ->
                    if (con.organs.contains(organ) == false) {
                        con.addToOrgans(organ)
                        con.save(flush: true)
                    }
                }
            }


        }


    }

    /**
     * assign the treatment specifics to the design
     * @param values
     * @param design
     * @return
     */
    protected Collection assignTreatmentsToDesign(Collection values, StudieDesign design) {
        return values.each { TreatmentSpecific treatmentSpecific ->

            //make sure we don't add an organ twice
            if (design.treatmentsSpecifics.contains(treatmentSpecific) == false) {
                design.addToTreatmentsSpecifics(treatmentSpecific)
            }

        }
    }

    /**
     * start with the design and define the studies
     */
    def design = {
        if (params.id) {
            params.designId = params.id
        }

        StudieDesign design = StudieDesign.get(params.designId)

        if (design != null) {
            switch (design.step) {

                case Step.DEFINE_ORGAN:
                    redirect(action: designOrgans, params: [designId: params.designId])
                    return

                case Step.DEFINE_TREATMENTS:
                    redirect(action: designTreatments, params: [designId: params.designId])
                    return

                case Step.DEFINE_CLASS:
                    redirect(action: designSampleProperties, params: [designId: params.designId])
                    return

                case Step.GENERATE_CLASS:
                    redirect(action: designClasses, params: [designId: params.designId])
                    return

                case Step.ASSIGN_SAMPLES:
                    redirect(action: designFileNames, params: [designId: params.designId])
                    return

                case Step.DONE:
                    redirect(action: designDone, params: [designId: params.designId])
                    return

                default:
                    design.step = Step.DEFINE_SPECIES

            }
        }

        [architectures: Architecture.list(), design: design]
    }

    /**
     * design the organs of the design
     */
    def designSpecies = {

        //load the design
        StudieDesign design = StudieDesign.get(params.designId)

        if (design == null) {
            flash.message = "sorry we were not able to find the specified id so you might want to start with a completely new design?"
            redirect(action: "design")
        }

        design.step = Step.DEFINE_SPECIES

        return [architectures: Architecture.list(), design: design]

    }

    /**
     * design the organs of the design
     */
    def designOrgans = {

        //load the design
        StudieDesign design = StudieDesign.get(params.designId)

        if (design == null) {
            flash.message = "sorry we were not able to find the specified id so you might want to start with a completely new design?"
            redirect(action: "design")
        }

        design.step = Step.DEFINE_ORGAN

        return [design: design]
    }

    /**
     * design the possibile treatments and sub treatments
     */
    def designTreatments = {

        //load the design
        StudieDesign design = StudieDesign.get(params.designId)

        if (design == null) {
            flash.message = "sorry we were not able to find the specified id so you might want to start with a completely new design?"
            redirect(action: "design")
        }

        design.step = Step.DEFINE_TREATMENTS

        return [design: design]
    }

    /**
     * design the sample properties for this studie
     */
    def designSampleProperties = {

        //load the design
        StudieDesign design = StudieDesign.get(params.designId)

        if (design == null) {
            flash.message = "sorry we were not able to find the specified id so you might want to start with a completely new design?"
            redirect(action: "design")
        }

        design.step = Step.DEFINE_CLASS

        return [design: design]
    }

    /**
     * select the classes required for the generation of the studies
     */
    def designClasses = {
        //load the design
        StudieDesign design = StudieDesign.get(params.designId)

        if (design == null) {
            flash.message = "sorry we were not able to find the specified id so you might want to start with a completely new design?"
            redirect(action: "design")
        }


        design.step = Step.GENERATE_CLASS

        return [design: design]

    }

    /**
     * define the file names
     */
    def designFileNames = {

        //load the design
        StudieDesign design = StudieDesign.get(params.designId)

        if (design == null) {
            flash.message = "sorry we were not able to find the specified id so you might want to start with a completely new design?"
            redirect(action: "design")
        } else if (design.generatedStudie == null) {
            flash.message = "sorry there was no studie generated from this design yet. Please select your classes to generate it?"
            redirect(action: "displayClasses", params: ["designId": params.designId])
        }

        design.step = Step.ASSIGN_SAMPLES

        return [design: design]
    }

    /**
     * the design is basically done and forwards us to the display studie page
     */
    def designDone = {
        StudieDesign design = StudieDesign.get(params.designId)

        if (design == null) {
            flash.message = "sorry we were not able to find the specified id so you might want to start with a completely new design?"
            redirect(action: "design")
        }

        design.step = Step.DONE

        return [design: design]
    }

    /**
     * redirects to the design stage
     */
    def create = {
        redirect(action: design)
    }

    /**
     *
     */
    def edit = {
        StudieDesign design = StudieDesign.get(params.designId)

        redirect(action: design, params: [designId: params.designId])
    }


    def ajaxRemoveDesign = {

        render """<div class="message">design deleted - not yet implemented!</div>"""
    }

    def list = {


        ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

        params.max = Math.min(params.max ? params.int('max') : 10, 100)

        def result = designQueryService.queryForUser(user, params)
        def count = designQueryService.queryCountForUser(user)

        [studieDesignInstanceList: result, studieDesignCount: count]

    }

    def listAll = {


        ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

        params.max = Math.min(params.max ? params.int('max') : 10, 100)

        def result = designQueryService.queryAll(params)
        def count = designQueryService.queryCountAll()

        [studieDesignInstanceList: result, studieDesignCount: count]

    }

    /**
     * displays the show paste dialog to paste threatment data
     */
    def ajaxShowPasteThreatmentDialog = {

        StudieDesign design = StudieDesign.get(params.id)

        //display the paste dialog
        if (params.paste == null && params.maxValue == null) {
            render template: "treatment/pasteDialog/displayDialog", model: [design: design]
        }
        //display the paste table and take the paste data apart
        else if (params.paste != null) {
            if (params."paste" == "") {
                try {
                    render """<div class="errors">sorry there was no text pasted!</div> """
                }
                catch (Exception e) {
                    render """<div class="errors">${e.getMessage()}</div> """

                    log.error e.getMessage(), e
                }
            } else {
                try {
                    def max = 0
                    params."paste".toString().split('\n').each { String line ->
                        def value = line.split('\t').length

                        if (value >= max) {
                            max = value
                        }
                    }

                    render template: "treatment/pasteDialog/renderPasteTable", model: [table: params."paste".toString(), maxValue: max, pageId: params.pageId, clazz: params.clazzId, design: design]
                }
                catch (Exception e) {
                    render """<div class="errors">${e.getMessage()}</div> """

                    log.error e.getMessage(), e
                }
            }
        }
        //assign the values and refresh the page
        else {
            try {
                def treatmensts = []
                def specifics = []

                def maxValue = params.maxValue.toString().toInteger()

                for (int i = 0; i < maxValue; i++) {
                    String column = params."column_${i}".toString().toLowerCase()

                    //converts the label data to a list
                    def values = params."label_${i}".flatten().toList()

                    //assign it to values
                    if (column == ThreatmentPasteDialogStep.TREATMENT.getLabel()) {

                        values.eachWithIndex { String s, int index ->

                            treatmensts.add(s)
                        }
                    } else if (column == ThreatmentPasteDialogStep.TREATMENT_VALUE.getLabel()) {

                        values.eachWithIndex { String s, int index ->
                            if (s == "") {
                                s = "None"
                            }
                            specifics.add(s)
                        }
                    }
                }

                treatmensts.eachWithIndex { String name, int index ->

                    //get our related treatment for the given name
                    Treatment treatment = defineTreatment(name)

                    //creates a specific treatment
                    TreatmentSpecific specific = createTreatmentSpecific("", specifics[index], treatment)

                    if (specific.validate() == false) {
                        log.info(specific.errors)
                    }

                    specific.save(flush: true)
                    design.addToTreatmentsSpecifics(specific)
                    design.save(flush: true)
                }


                render template: "treatment/pasteDialog/closeDialogAndUpdateView", model: [design: design]
            }
            catch (Exception e) {
                render """<div class="errors">${e.getMessage()}</div> """

                log.error e.getMessage(), e
            }
        }
    }

    /**
     * render the treatments for the paste result
     */
    def ajaxRenderTreatmentsForPasteResult = {
        StudieDesign design = StudieDesign.get(params.id)

        render template: "treatment/renderTreatmentSpecifics", model: [design: design]

    }

    /**
     * fires an update for the given session
     */
    def ajaxRenderProgress = {
        try {
            render "${session[params.id]}"
        }
        catch (Exception e) {
            log.error(e.getMessage(), e)
            render "-1"
        }
    }

    /**
     * displays the design for the specified study
     */
    def showDesignForStudy = {

        StudieDesign design = StudieDesign.findByGeneratedStudie(Studie.get(params.id))
        redirect(action: design, params: [designId: design.id])
    }

    def duplicateDesignByStudy = {
        StudieDesign design = StudieDesign.findByGeneratedStudie(Studie.get(params.id))
        redirect(action: duplicateDesign, params: [id: design.id])
    }
        /**
     * duplicates a given design up to the class generation settings.
     */
    def duplicateDesign = {
        StudieDesign design = StudieDesign.get(params.id)

        StudieDesign design2 = new StudieDesign()
        design2.architecture = design.architecture
        design2.title = design.title + " - copy"
        design2.step = Step.DEFINE_SPECIES
        design2.numberOfSamples = design.numberOfSamples

        ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)
        design2.user = user

        design2.save()

        /**
         * species
         */
        design.speciees.each {
            design2.addToSpeciees(it)
        }

        /**
         * species connections
         */
        design.designSpeciesConnections.each { DesignSpeciesConnection c ->
            assignOrgansToDesign(c.organs, design2, c.species)
        }

        /**
         * treatments
         */
        design.treatmentsSpecifics.each { TreatmentSpecific t ->
            TreatmentSpecific x = copyTreatmentSpecific(t)


            design2.addToTreatmentsSpecifics(x)
        }

        if (design2.validate() == false) {
            println(design2.errors)
        }

        design2.save()

        redirect(action: "design", id: design2.id)
    }

    /**
     * copyes a treatment specific and all its children
     * @param t
     * @return
     */
    private TreatmentSpecific copyTreatmentSpecific(TreatmentSpecific t) {
        Treatment treat = t.treatment
        TreatmentSpecific spec = createTreatmentSpecific("", t.value, treat)

        spec.save()
        if (t.subTreatmentSpecifics != null) {
            t.subTreatmentSpecifics.each { TreatmentSpecific x ->
                TreatmentSpecific y = copyTreatmentSpecific(x)
                spec.addToSubTreatmentSpecifics(y)
                y.parent = spec
                y.save()
                t.save()
            }
        }
        return spec
    }
}
