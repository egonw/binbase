package minix

class ReactionBlankClassController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [reactionBlankClassInstanceList: ReactionBlankClass.list(params), reactionBlankClassInstanceTotal: ReactionBlankClass.count()]
    }

    def show = {
        def reactionBlankClassInstance = ReactionBlankClass.get(params.id)
        if (!reactionBlankClassInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'reactionBlankClass.label', default: 'ReactionBlankClass'), params.id])}"
            redirect(action: "list")
        }
        else {
            [reactionBlankClassInstance: reactionBlankClassInstance]
        }
    }
}
