package minix

import minix.progress.ProgressHandler
import jprogress.ProgressService

/**
 * imports setupx data files
 */
class ImportSetupXDumpController implements ProgressHandler {

    /**
     * access to the import service
     */

    ImportSetupXDumpService importSetupXDumpService

    ProgressService progressService

    /**
     * imports the given directory into the system
     */
    def importDirectory_ajax = {

        render(template: "dialog")
    }

    /**
     * does the actual import
     */
    def importDump_ajax = {
        if (params.directory == null | params.directory.toString().size() == 0) {
            flash.errorMessage = "please provide a directory!"
            render(template: "dialog")

            return
        }

        String id = params.id
        progressService.setProgressBarValue(id, 0)

        File dir = new File(params.directory.toString().trim())

        if (dir.exists() == false) {
            flash.errorMessage = "sorry your directory '${dir.absolutePath}' does not exist!"
            render(template: "dialog")

            return
        }
        else {
            importSetupXDumpService.importDirectory(dir, this,id)
            render(template: "done")
            return
        }
    }

    /**
     * handles the progress
     * @param id
     * @param percent
     * @param properties
     */
    void handleProgress(String id, double percent, Object properties) {
        progressService.setProgressBarValue(id, percent)

    }

}
