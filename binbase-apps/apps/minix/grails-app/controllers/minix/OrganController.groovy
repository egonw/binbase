package minix
class OrganController {
    def scaffold = true

    /**
     * show the data
     */
    def show = {

        def pagination = [max: Math.min(params.max ? params.int('max') : 10, 100), offset: Math.min(params.offset ? params.int('offset') : 0, 100)]

        Organ organ = Organ.get(params.id)

        if (organ) {
            //get all associated queries for this studie


            def studies = Studie.executeQuery("select distinct a from Studie as a, ExperimentClass as b, Organ as c where a.id = b.studie.id and b.organ.id = c.id and c.id = ? ", [organ.id],pagination)

            def studieTotal = Studie.executeQuery("select count(distinct a) from Studie as a, ExperimentClass as b, Organ as c where a.id = b.studie.id and b.organ.id = c.id and c.id = ? ", [organ.id])[0]

            //render the result

            return [organInstance: organ, studieInstanceList: studies, studieTotal: studieTotal]
        }
        else {
            flash.message = "sorry organ not found with id: ${params.id}!"
            redirect(action: "list")
        }
    }


    def ajaxEditName = {
        def value = Organ.get(params.id)

        if (value != null) {
            render template: "/dialog/editNameDialog", model: [value: value]
        }
        else {
            flash.message = "sorry species not found!"
            redirect(action: "list")

        }
    }

    /**
     * save the new title
     */
    def ajaxSaveName = {

        def value = Organ.get(params.id)
        value.name = params.title
        value.save()
        render value.name
    }
}
