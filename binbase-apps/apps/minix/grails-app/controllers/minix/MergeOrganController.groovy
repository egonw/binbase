package minix

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import minix.progress.ProgressHandler
import jprogress.ProgressService

class MergeOrganController {

    OrganService organService

    ProgressService progressService

    def merge = {

        def progressId = "progressBar_${System.currentTimeMillis()}"

        def values = readFromParams([params])
        [values: values, progressId: progressId, name: params.name]
    }

    /**
     * displays all the studies in the current merge setup
     */
    def ajaxViewInMerge = {
        HashSet values = readFromParams(params)

        render(template: "select", model: [values: values, progressId: params.progressId, name: params.name])
    }

    /**
     * reads all the studies in the params dataset
     *
     * @param params
     * @return
     */
    private HashSet readFromParams(GrailsParameterMap params) {

        def result = new HashSet()

        if (params.value.toString() != "" && params.value != null) {
            result.add(loadValue(params.value.toString().split(":")[0].toString()))

        }
        if (params.value_id instanceof String) {
            result.add(loadValue(params.value_id))

        }
        else {

            params.value_id.each {String s ->
                result.add(loadValue(s))

            }

        }
        return result
    }

    def loadValue(String id) {
        return Organ.get(Long.parseLong(id))
    }

    /**
     * executes the actual merge
     */
    def ajaxExecuteMerge = {

        def values = readFromParams(params)

        if (values.size() >= 2) {


            def result = organService.combineOrgan( params.name, values, {String id, double percent, def properties ->
                println "${params.progressId} - ${percent}"
                progressService.setProgressBarValue(params.progressId, percent);
            } as ProgressHandler, params.progressId);

            render(template: "mergeComplete", model: [value: result, progressId: params.progressId])
        }
        else {
            flash.message = "please add more values for this operation to work"
            render(template: "select", model: [values: values, progressId: params.progressId, name: params.name])
        }
    }
}
