package minix
class BackupController {

  BackupService backupService

  def backup = {

  }

  def backup_ajax = {

    render template: "dialog"
  }

  def executeBackup_ajax = {
    println params

    boolean compress = false

    if (params.compress == "on") {
      compress = true
    }

    File file = new File("./")

    if (params.directory.toString().size() > 0) {
      file = new File(params.directory)
    }

    file = backupService.backup(file, compress)
    render template: "done", model: [backupFile: file]

  }
}
