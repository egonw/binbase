package minix

class VendorController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static scaffold = true
}
