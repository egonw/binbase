package minix

import minix.roles.RoleUtil

/**
 * used to generate the initial roles for the application
 * and the intial admin user
 */
class SetupController {

    UserService userService


    def beforeInterceptor = {

        //only admins are allowed to access this controller

        if (ShiroUser.findByUsername("admin") != null) {
            flash.message = "sorry the system was already completely setup!"

            redirect(controller: "home")

        }
    }

    def initialize = {

    }

    /**
     * saves the user and encrypts the password
     */
    def save = {

        ShiroUser shiroUser = userService.createUser(params)

        shiroUser.samplePrefix = "aa"

        //define the roles
        ShiroRole admin = RoleUtil.getAdminRole()

        ShiroRole user = RoleUtil.getUserRole()

        ShiroRole manager = RoleUtil.getManagerRole()

        userService.assignRole(shiroUser, admin)

        //assigns permissions to this user
        if (shiroUser.validate()) {

            shiroUser.save(flush: true)

            userService.assignPermissions(shiroUser)

            flash.message = "${message(code: 'default.created.message', args: [message(code: 'shiroUser.label', default: 'ShiroUser'), shiroUser.id])}"
            redirect(controller: "home", action: "index")
        }
        else {
            shiroUser.passwordHash = null
            render(view: "initialize", model: [shiroUserInstance: shiroUser])
        }
    }
}
