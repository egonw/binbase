package minix

import binbase.web.core.BBExperimentSample
import minix.step.DialogStep

class PasteDialogController {

    /**
     * used to modify classes
     */
    ModifyClassService modifyClassService

    /**
     * used to add additional metadata and additional samples at a later point in time
     */
    def ajaxShow = {
        ExperimentClass clazz = ExperimentClass.get(params.id)
        render template: "displayPasteDialog", model: [clazz: clazz]
    }

    /**
     * processes the uploaded data
     */
    def ajaxProcessData = {

        if (params."paste" == "") {
            render """<div class="errors">sorry there was no text pasted!</div> """
        }
        else {
            try {
                def max = 0
                params."paste".toString().split('\n').each {String line ->
                    def value = line.split('\t').length

                    if (value >= max) {
                        max = value
                    }
                }


                render template: "renderPasteTable", model: [table: params."paste".toString(), maxValue: max, pageId: params.pageId, clazz: params.clazzId]
            }
            catch (Exception e) {
                render """<div class="errors">${e.getMessage()}</div> """

                log.error e.getMessage(), e
            }
        }

    }

    /**
     * sets some session variable
     */
    def ajaxStoreColumnSelectionIn = {
        println params
        log.info("storing in session: ${params.name} - ${params.value}")
        session[params.name] = params.value
        render ""
    }

    def ajaxSaveData = {
        def maxValue = params.maxValue.toString().toInteger()

        ExperimentClass clazz = ExperimentClass.get(params.clazzId)

        if (clazz != null) {
            for (int i = 0; i < maxValue; i++) {
                String column = params."column_${i}".toString().toLowerCase()

                //converts the label data to a list
                def values = []

                try{
                    values = params."label_${i}".flatten().toList()
                }
                catch (Exception ex){
                     values = [params."label_${i}"]
                }

                //assign it to values
                if (column == DialogStep.COMMENT.getLabel()) {
                    if (clazz.studie.canModify()) {
                        modifyClassService.increaseClassSize(clazz, values.size())
                    }

                    values.eachWithIndex {String s, int index ->

                        if (index < clazz.samples.size()) {
                            clazz.samples.toList()[index].comment = s
                        }
                    }
                }
                else if (column == DialogStep.LABEL.getLabel()) {
                    if (clazz.studie.canModify()) {
                        modifyClassService.increaseClassSize(clazz, values.size())
                    }

                    values.eachWithIndex {String s, int index ->

                        if (index < clazz.samples.size()) {
                            clazz.samples.toList()[index].label = s
                        }
                    }
                }
                else if (column == DialogStep.FILENAME.getLabel()) {
                    if (clazz.studie.canModify()) {
                        modifyClassService.increaseClassSize(clazz, values.size())
                    }

                    try{
                    values.eachWithIndex {String s, int index ->

                        if (index < clazz.samples.size()) {
                            BBExperimentSample sample = clazz.samples.toList()[index]
                            sample.fileName = s
                            sample.validate()
                            if (sample.hasErrors()){
                                throw new RuntimeException("sorry the file name has to be unique over the database!")
                            }
                        }
                    }
                    }
                    catch (Exception e){
                        response.status = 500
                        render("<div class='errors'>${e.getMessage()}</div>")
                        return
                    }
                }
                else if (column == DialogStep.IGNORE.getLabel()) {
                    //we ignore this
                }
                else {
                    //we ignore this
                }

                clazz.save(flush: true)
            }
        }

        render template: "closeDialogAndUpdateView", model: [clazz: clazz]

    }


}
