package minix

import org.apache.shiro.SecurityUtils
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import jprogress.ProgressService
import minix.progress.ProgressHandler

class MergeStudieController {

    def scaffold = false

    StudieMergeService studieMergeService

    StudieQueryService studieQueryService

    ProgressService progressService

    /**
     * merges the list of given id's
     */
    def mergeStudie = {

        def progressId = "progressBar_${System.currentTimeMillis()}"

        def studies = readStudiesFromParams([params])
        [studies: studies, progressId: progressId]
    }

    /**
     * displays all the studies in the current merge setup
     */
    def ajaxViewStudiesInMerge = {
        HashSet studies = readStudiesFromParams(params)

        render(template: "selectStudy", model: [studies: studies, progressId: params.progressId])
    }

    /**
     * reads all the studies in the params dataset
     *
     * @param params
     * @return
     */
    private HashSet readStudiesFromParams(GrailsParameterMap params) {

        def studies = new HashSet()

        if (params.study.toString() != "" && params.study != null) {
            def studyId = Long.parseLong(params.study.toString().split(":")[0].toString())
            def recentAddedStudie = Studie.get(studyId)
            studies.add(recentAddedStudie)

        }
        if (params.study_id instanceof String) {
            studies.add(Studie.get(Long.parseLong(params.study_id.toString())))

        }
        else {

            params.study_id.each {String s ->
                studies.add(Studie.get(Long.parseLong(s)))

            }

        }
        return studies
    }

    /**
     * executes the actual merge
     */
    def ajaxExecuteMerge = {

        def studies = readStudiesFromParams(params)

        if (studies.size() >= 2) {
            def result = studieMergeService.merge(studies, "combined studies: ${studies}", {String id, double percent, def properties ->
                progressService.setProgressBarValue(params.progressId, percent);
            } as ProgressHandler)

            render(template: "mergeComplete", model: [study: result, progressId: params.progressId])
        }
        else {
            flash.message = "please add more studies for this operation to work"
            render(template: "selectStudy", model: [studies: studies, progressId: params.progressId])
        }
    }
}
