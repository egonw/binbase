package minix

/**
 * generic controller for architectures
 */
class GenericController {

    def scaffold = true

    /**
     * should return nothing
     */
    def scheduleGeneric = {

    }

}
