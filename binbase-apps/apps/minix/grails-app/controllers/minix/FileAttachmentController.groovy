package minix

import binbase.web.core.BBContent

class FileAttachmentController {
    def scaffold = true

    def delete_ajax = {
        def fileAttachmentInstance = FileAttachment.get(params.id)
        if (fileAttachmentInstance) {
            try {
                BBContent.findByResult(fileAttachmentInstance)?.delete(flush: true)
                fileAttachmentInstance.studie.removeFromAttachments(fileAttachmentInstance)
                fileAttachmentInstance.delete(flush: true)
                render("<div class=\"message\">${message(code: 'default.deleted.message', args: [message(code: 'fileAttachment.label', default: 'FileAttachment'), params.id])}</div>")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                log.error(e.getMessage(), e)

                render "<div class=\"message\">${message(code: 'default.not.deleted.message', args: [message(code: 'fileAttachment.label', default: 'FileAttachment'), params.id])}</div>"
            }
        }
        else {
            render "<div class=\"message\">${message(code: 'default.not.found.message', args: [message(code: 'fileAttachment.label', default: 'FileAttachment'), params.id])}</div>"
        }
    }


    def delete = {
        def fileAttachmentInstance = FileAttachment.get(params.id)
        if (fileAttachmentInstance) {
            try {
                FileAttachment.withTransaction {
                    BBContent.findByResult(fileAttachmentInstance).delete(flush: true)
                    fileAttachmentInstance.studie.removeFromAttachments(fileAttachmentInstance)
                    fileAttachmentInstance.studie.save(flush: true)
                    fileAttachmentInstance.delete(flush: true)
                }
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'fileAttachment.label', default: 'FileAttachment'), params.id])}"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                log.error(e.getMessage(), e)
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'fileAttachment.label', default: 'FileAttachment'), params.id])}"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'fileAttachment.label', default: 'FileAttachment'), params.id])}"
            redirect(action: "list")
        }
    }
}
