package minix

import org.codehaus.groovy.grails.plugins.orm.auditable.AuditLogEvent

class AuditController {

    def index = {


        if (ShiroUser.hasAdminRights() == false) {
            flash.message = "sorry you have no permission to access this data!"

            if (ShiroUser.hasManagmentRights()) {
                redirect(controller: "home", action: "showManagerHome")

            }
            else {
                redirect(controller: "home", action: "showMemberHome")
            }

        }

        redirect(action: list, params: params)
    }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete: 'POST', save: 'POST', update: 'POST']

    /**
     * does the actual listing and ordering
     */
    def list = {
        if (!params.max) params.max = 25
        if (!params.sort) params.sort = "dateCreated"
        if (!params.order) params.order = "desc"

        //the list of users which is in the audit log
        def users = AuditLogEvent.executeQuery("select distinct a.actor from AuditLogEvent a where a.actor is not null")

        def selectedUser = params.selectedUser

        def result = []
        def count = 0

        if (selectedUser != null && selectedUser.toString().size() != 0) {

            count = AuditLogEvent.executeQuery("select count(a.actor) from AuditLogEvent a where a.actor is not null").get(0)

            result = AuditLogEvent.findAllByActor(selectedUser, params)
        }
        else {

            count = AuditLogEvent.count()
            result = AuditLogEvent.list(params)

        }


        [auditLogEventInstanceList: result, auditLogEventInstanceTotal: count, selectedUser: selectedUser, users: users]

    }


}
