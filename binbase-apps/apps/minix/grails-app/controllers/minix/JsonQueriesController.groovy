package minix
import grails.converters.JSON

/**
 * a simple controller to provide json data access to some queries
 */
class JsonQueriesController {

  /**
   * statistics query service
   */
  StatisticQueryService statisticQueryService

  def index = {
    render "nothing to see here"
  }
  /**
   * queries all the studies and groups them by species
   * informations
   */
  def queryStudiesGroupedBySpecies = {
     render statisticQueryService.queryStudiesGroupedBySpecies() as JSON
  }

}
