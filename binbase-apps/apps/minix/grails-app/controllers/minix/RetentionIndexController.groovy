package minix

import core.BinBaseSystemService
import grails.converters.JSON

class RetentionIndexController {

    BinBaseSystemService binBaseSystemService

    QualityControlService qualityControlService

    UrlService urlService

    def beforeInterceptor = {


        if (ShiroUser.hasManagmentRights() == false) {
            flash.message = "sorry you have no permission to access this data!"

            redirect(controller: "home", action: "index")
        }

        params.remove("quickLink")
        def url = urlService.generateQuickLinkURL(params)
        params.quickLink = url

    }

    /**
     * displays recently failed corrections
     */
    def displayRecentlyFailedCorrections = {
        [columns: binBaseSystemService.availableColumns]
    }

    def ajaxRenderResultsForDataBase = {
        List<Sample> samples = qualityControlService.getLastFailedSamplesForDB(params.id, 50)


        if (samples.isEmpty()) {

            render "<tr ><td>nothing found</td></tr>"
        } else {
            samples.each { sample ->
                render """
                      <tr>
                        <td>${sample.toString()}</td>
                        <td>${sample.experimentClass.name}</td>
                        <td>${g.link(controller: "studie", action: "show", id: "${sample.experimentClass.experiment.id}") { sample.experimentClass.experiment.title }}</td>
                       </tr>
                    """
            }
        }
    }

    /**
     * displays if the correction for the given sample failed
     */
    def ajaxCorrectionFailedForSample = {
        Map result = [:]
        assert (params.id != null)
        assert (params.db != null)

        result.failed = qualityControlService.sampleFailed(Sample.get(params.id), params.db)

        render result as JSON
    }

    /**
     * displays if the smaple was calculated
     */
    def ajaxSampleCalculated = {
        Map result = [:]
        assert (params.id != null)
        assert (params.db != null)

        result.calculated = qualityControlService.sampleCalculated(Sample.get(params.id), params.db)

        render result as JSON
    }

    /**
     * visualizes the quality of the retention index markers
     */
    def showRetentionIndexMarkerPerformance = {

        [columns: binBaseSystemService.availableColumns]
    }

    def showRetentionIndexMarkerPerformanceApexSn = {

        [columns: binBaseSystemService.availableColumns]
    }

    def showRetentionIndexMarkerPerformanceHeight = {

        [columns: binBaseSystemService.availableColumns]
    }

    def showBinPerformance = {
        [columns: binBaseSystemService.availableColumns, instruments: GCTof.list()
        ]
    }

    def showBinRatio = {
        [columns: binBaseSystemService.availableColumns, instruments: GCTof.list()
        ]
    }


    def showBinPerformanceByDays = {
        [columns: binBaseSystemService.availableColumns, instruments: GCTof.list()
        ]
    }

    /**
     * displays the performance for a given study
     */
    def showRetentionIndexMarkerPerformanceByStudy = {

        [study: Studie.get(params.id)]
    }

    /**
     * displays the apex sn for the standards
     */
    def showRetentionIndexMarkerPerformanceByStudyApexSn = {
        [study: Studie.get(params.id)]
    }
    def showRetentionIndexMarkerPerformanceByStudyIntensity = {
        [study: Studie.get(params.id)]
    }

    /**
     * renders a template displaying the retention index marker performance by DB
     */
    def ajaxShowBinPerformanceByDB = {
        String database = params.db
        String instrument = params.instrument
        long from = new Date(params.from.toString()).time
        long to = new Date((params.to.toString())).time

        int binId = params.bin.toString().toInteger()
        int ion = params.ion.toString().toInteger()
        String pattern = params.sample.toString()


        render template: "graphs/performanceGraphByBin", model: [instrument: instrument, binId: binId, column: database, from: from, to: to, ion: ion, mode: "intensity", quickLink: params.quickLink, pattern: pattern]
    }

    /**
     * displays the performance of this bin for this study
     */
    def ajaxShowBinPerformanceByStudy = {
        render template: "graphs/performanceGraphForStudyAndBin", model: [study: Studie.get(params.study), binId: params.bin.toString().toInteger(),ion:params.ion.toString().toInteger(),  mode: "intensity", quickLink: params.quickLink]
    }

    def ajaxShowBinRatioPerformanceByDB = {
        String database = params.db
        String instrument = params.instrument
        long from = new Date(params.from.toString()).time
        long to = new Date((params.to.toString())).time

        int binIdA = params.bin_first.toString().toInteger()
        int binIdB = params.bin_second.toString().toInteger()

        String pattern = params.sample.toString()


        render template: "graphs/binRatio", model: [instrument: instrument, bin_first: binIdA,bin_second:binIdB, column: database, from: from, to: to, mode: "intensity", quickLink: params.quickLink, pattern: pattern]
    }

    /**
     * renders a template displaying the retention index marker performance by DB
     */
    def ajaxShowBinPerformanceByDBForNDays = {
        String database = params.db
        String instrument = params.instrument
        long from = new Date().minus(Integer.parseInt(params.days)).time
        long to = new Date().time

        int binId = params.bin.toString().toInteger()
        int ion = params.ion.toString().toInteger()
        String pattern = params.sample.toString()


        render template: "graphs/performanceGraphByBin", model: [instrument: instrument, binId: binId, column: database, from: from, to: to, ion: ion, mode: "intensity", quickLink: params.quickLink, pattern: pattern]
    }

    def ajaxShowRetentionIndexMarkerPerformanceByDBAndInstrumentAndStandard = {


        render template: "graphs/performanceGraph", model: [instrument: Instrument.get(params.instrument.toString().toLong()), standard: [id: params.standard, name: params.standard_name], column: params.column, from: params.from, to: params.to, mode: params.mode, quickLink: params.quickLink]
    }

    def ajaxShowRetentionIndexMarkerPerformanceByStudyAndStandard = {

        render template: "graphs/performanceGraphForStandardAndStudy", model: [study: Studie.get(params.study), standard: [id: params.standard, name: params.standard_name], mode: params.mode, quickLink: params.quickLink]
    }
    /**
     * renders a template displaying the retention index marker performance by DB
     */
    def ajaxShowRetentionIndexMarkerPerformanceByDB = {
        String database = params.id
        List<GCTof> gcs = GCTof.list()
        Collection<Map> standards = qualityControlService.getStandardIdsForDatabase(database)
        Date from = new Date(params.from)
        Date to = new Date(params.to)

        render template: "performanceByDBAndMarker", model: [column: database, instruments: gcs, standards: standards, from: from.time, to: to.time, mode: params.mode, quickLink: params.quickLink]

    }

    def ajaxShowRetentionIndexMarkerPerformanceByStudy = {
        Studie study = Studie.get(params.id)
        Collection<Map> standards = qualityControlService.getStandardIdsForDatabase(study.databaseColumn)

        render template: "performanceForMarkerAndStudy", model: [study: study, standards: standards, mode: params.mode, quickLink: params.quickLink]
    }

    /**
     * generates the data needed for the given study and standard
     */
    def ajaxGenerateGraphForIndexMarkerPerformanceByStudyAndStandard = {

        List data = qualityControlService.getRetentionIndexMarkerPerformanceForStudy(Studie.get(params.id), Integer.parseInt(params.standard.toString()))

        def result = []

        def minRatioSeries = []
        def maxRatioSeries = []
        def ratioSeries = []
        data.each { map ->
            minRatioSeries.add([map.date.time, map.min])
            maxRatioSeries.add([map.date.time, map.max])
            ratioSeries.add([map.date.time, map.ratio])

        }
        result.add([label: "min ratio", data: minRatioSeries, lines: [show: true], color: "#DD0400", points: [show: false]])
        result.add([label: "ratio", data: ratioSeries, color: "#52A1DD"])
        result.add([label: "max ratio", data: maxRatioSeries, lines: [show: true], color: "#DD0400", points: [show: false]])

        render result as JSON
    }

    /**
     * generates the actual graph in form of a json string for the database, instrument and standard
     */
    def ajaxGenerateGraphForIndexMarkerPerformanceByInstrumentAndDBAndStandard = {

        String database = params.column
        GCTof tof = GCTof.get(params.instrument)
        Integer standard = Double.parseDouble(params.standard.toString()).intValue()

        List data = qualityControlService.getRetentionIndexMarkerPerformanceForDates(database, tof.identifier, standard, Long.parseLong(params.from.toString()), Long.parseLong(params.to.toString()))

        def result = []

        def minRatioSeries = []
        def maxRatioSeries = []
        def ratioSeries = []
        data.each { map ->
            minRatioSeries.add([map.date.time, map.min])
            maxRatioSeries.add([map.date.time, map.max])
            ratioSeries.add([map.date.time, map.ratio])

        }
        result.add([label: "min ratio", data: minRatioSeries, lines: [show: true], color: "#DD0400", points: [show: false]])
        result.add([label: "ratio", data: ratioSeries, color: "#52A1DD"])
        result.add([label: "max ratio", data: maxRatioSeries, lines: [show: true], color: "#DD0400", points: [show: false]])

        render result as JSON
    }

    /**
     * generates the data needed for the given study and standard
     */
    def ajaxGenerateGraphForIndexMarkerPerformanceByStudyAndStandardForSN = {

        List data = qualityControlService.getRetentionIndexMarkerPerformanceForStudy(Studie.get(params.id), Integer.parseInt(params.standard.toString()))

        def result = []

        def minRatioSeries = []
        def ratioSeries = []
        data.each { map ->
            minRatioSeries.add([map.date.time, map.min_sn])
            ratioSeries.add([map.date.time, map.sn])

        }
        result.add([label: "min", data: minRatioSeries, lines: [show: true], color: "#DD0400", points: [show: false]])
        result.add([label: "sn", data: ratioSeries, color: "#52A1DD"])

        render result as JSON
    }

    /**
     * generates the actual graph in form of a json string for the database, instrument and standard
     */
    def ajaxGenerateGraphForIndexMarkerPerformanceByInstrumentAndDBAndStandardForSN = {
        def from = (Long.parseLong(params.from.toString()))
        def to = (Long.parseLong(params.to.toString()))

        String database = params.column
        GCTof tof = GCTof.get(params.instrument)
        Integer standard = Double.parseDouble(params.standard.toString()).intValue()

        List data = qualityControlService.getRetentionIndexMarkerPerformanceForDates(database, tof.identifier, standard, from, to)

        def result = []

        def minRatioSeries = []
        def ratioSeries = []
        data.each { map ->
            minRatioSeries.add([map.date.time, map.min_sn])
            ratioSeries.add([map.date.time, map.sn])

        }
        result.add([label: "min", data: minRatioSeries, lines: [show: true], color: "#DD0400", points: [show: false]])
        result.add([label: "sn", data: ratioSeries, color: "#52A1DD"])

        render result as JSON
    }

    /**
     * generates the data needed for the given study and standard
     */
    def ajaxGenerateGraphForIndexMarkerPerformanceByStudyAndStandardForIntensity = {

        List data = qualityControlService.getRetentionIndexMarkerPerformanceForStudy(Studie.get(params.id), Integer.parseInt(params.standard.toString()))

        def result = []

        def ratioSeries = []
        data.each { map ->
            ratioSeries.add([map.date.time, map.intensity])

        }
        result.add([label: "sn", data: ratioSeries, color: "#52A1DD"])

        render result as JSON
    }

    /**
     * generates the actual graph in form of a json string for the database, instrument and standard
     */
    def ajaxGenerateGraphForIndexMarkerPerformanceByInstrumentAndDBAndStandardForSNIntensity = {
        def from = (Long.parseLong(params.from.toString()))
        def to = (Long.parseLong(params.to.toString()))

        String database = params.column
        GCTof tof = GCTof.get(params.instrument)
        Integer standard = Double.parseDouble(params.standard.toString()).intValue()

        List data = qualityControlService.getRetentionIndexMarkerPerformanceForDates(database, tof.identifier, standard, from, to)

        def result = []

        def ratioSeries = []
        data.each { map ->
            ratioSeries.add([map.date.time, map.intensity])

        }
        result.add([label: "intensity", data: ratioSeries, color: "#52A1DD"])

        render result as JSON
    }

}
