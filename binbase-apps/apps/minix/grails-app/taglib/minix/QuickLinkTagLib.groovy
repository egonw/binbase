package minix

/**
 * simple tag lib support our quick links
 */
class QuickLinkTagLib {

    static namespace = "quicklink"

    /**
     * provides a message that we have a quick link available for this webpage
     */
    def message = { attr,body ->

        out << """

        <div class="quicklink">
            <span>
                direct access link:
            </span>
            <a href="${attr.url}" target="_blank">${attr.url}</a>

        </div>


            """
    }
}
