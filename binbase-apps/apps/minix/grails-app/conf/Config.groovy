import grails.util.Environment

// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if(System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

/**
 * externalized configurations are defined here. So that we can easily overwrite them as needed, based on location
 */
def ENV_NAME = "minix-config"

grails.config.locations = binbase.core.IntializeConfigurationLocations.initialize(grails.config.locations,appName)

grails.project.groupId = "minix" // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [html: ['text/html', 'application/xhtml+xml'],
        xml: ['text/xml', 'application/xml'],
        text: 'text/plain',
        js: 'text/javascript',
        rss: 'application/rss+xml',
        atom: 'application/atom+xml',
        css: 'text/css',
        csv: 'text/csv',
        all: '*/*',
        json: ['application/json', 'text/json'],
        form: 'application/x-www-form-urlencoded',
        multipartForm: 'multipart/form-data'
]
// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// whether to install the java.util.logging bridge for sl4j. Disable fo AppEngine!
grails.logging.jul.usebridge = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []

// set per-environment serverURL stem for creating absolute links
environments {
    jboss {
        grails.serverURL = "http://localhost:8080/minix"
    }
    production {
        grails.serverURL = "http://minix.fiehnlab.ucdavis.edu"
    }
    development {
        grails.serverURL = "http://localhost:8080/minix"
    }
    test {
        grails.serverURL = "http://localhost:8080/minix"
    }

}

// log4j configuration
log4j = {

    //tomcat based logging
    def catalinaBase = System.properties.getProperty('catalina.base')
    if (!catalinaBase) catalinaBase = '.'   // just in case
    def logDirectory = "${catalinaBase}/logs"
    // Example of changing the log pattern for the default console
    // appender:
    //
    appenders {
        console name: 'stdout', layout: pattern(conversionPattern: '[%-5p] %l [%m]%n'), threshold: org.apache.log4j.Level.INFO
        rollingFile name: 'debugLog', file: "${logDirectory}/minix_debug.log".toString(), maxFileSize: '1MB', threshold: org.apache.log4j.Level.DEBUG, layout: pattern(conversionPattern: '[%d{ABSOLUTE}] [%-5p] [%-5C] [%m]%n')

        rollingFile name: 'infoLog', file: "${logDirectory}/minix.log".toString(), maxFileSize: '100KB', threshold: org.apache.log4j.Level.INFO, layout: pattern(conversionPattern: '[%d{ABSOLUTE}] [%-5p] [%-5C] [%m]%n')
        rollingFile name: 'stacktrace', file: "${logDirectory}/minix_stack.log".toString(), maxFileSize: '100KB', layout: pattern(conversionPattern: '[%d{ABSOLUTE}] [%-5p] [%-5C] [%m]%n')
    }
    root {
        info 'infoLog', 'stdout'
        debug 'debugLog'
        additivity = true
    }

    error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
            'org.codehaus.groovy.grails.web.pages', //  GSP
            'org.codehaus.groovy.grails.web.sitemesh', //  layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping', // URL mapping
            'org.codehaus.groovy.grails.commons', // core / classloading
            'org.codehaus.groovy.grails.plugins', // plugins
            'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate',
            'org.dbunit.dataset.AbstractTableMetaData'

    warn 'org.mortbay.log', 'org.compass.core','org.springframework.context.support.ReloadableResourceBundleMessageSource'
    info "minix","minix.schedule.binbase", "grails.app.service", 'org.dbunit'

    //debug "org.codehaus.groovy.grails.web.mapping.filter"

    //debugging goes here
    debug debugLog: ["grails.app", "org.codehaus.groovy.grails.plugins.orm.auditable", 'org.codehaus.groovy.grails.web'], additivity: true


}
/**
 * we want to use jquery by default
 */
grails.views.javascript.library = "jquery"

/**
 * shiro options
 */
security.shiro.auth.required = true

//audit logging
auditLog {
    actorClosure = { request, session ->
        org.apache.shiro.SecurityUtils.getSubject()?.getPrincipal()
    }
}

//geoip service
geoip {
    data {
        resource = "/WEB-INF/GeoLiteCity.dat"
        cache = 0
    }
}

//editor configuration
ckeditor {

	defaultFileBrowser = "ofm"
	upload {
		basedir = "/uploads/"
	        overwrite = false
	        link {
	            browser = false
	            upload = false
	            allowed = []
	            denied = ['html', 'htm', 'php', 'php2', 'php3', 'php4', 'php5',
	                      'phtml', 'pwml', 'inc', 'asp', 'aspx', 'ascx', 'jsp',
	                      'cfm', 'cfc', 'pl', 'bat', 'exe', 'com', 'dll', 'vbs', 'js', 'reg',
	                      'cgi', 'htaccess', 'asis', 'sh', 'shtml', 'shtm', 'phtm']
	        }
	        image {
	            browser = false
	            upload = false
	            allowed = ['jpg', 'gif', 'jpeg', 'png']
	            denied = []
	        }
	        flash {
	            browser = false
	            upload = false
	            allowed = ['swf']
	            denied = []
	        }
	}
}

/**
 * minix specific configuration
 */
minix{
    standard{
        concentrations = [0.1, 0.25, 0.5, 1, 2.5, 5]
    }
}