import edu.ucdavis.genomics.metabolomics.util.io.FileUtil
import org.apache.commons.io.FileUtils

dataSource {
    pooled = true
    dbunitXmlType = "flat"
    initialOperation = "REFRESH"

}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
}

/**
 * ALL THE DATABASE CONNECTIONS ARE SPECIFIED IN THE minix-config FILE! Please make sure that you use this file
 */
// environment specific settings
environments {
    development {

        dataSource {
            pooled = true
            dbCreate = "update"
//            initialOperation = "REFRESH"
            loggingSql = false
//            initialData = "data/test/production.xml"

        }
    }
    test {
        dataSource {
            dbCreate = "update"
            loggingSql = false
//            initialOperation = "REFRESH"
//            initialData = "data/test/minix.xml"
        }
    }
    production {
        dataSource {

            pooled = true
            loggingSql = false
            dbCreate = "update"
            initialOperation = "REFRESH"
            initialData = "data/test/production.xml"

        }
    }

    //should be jni datasource
    jboss {
        dataSource {
            pooled = false
            jndiName = "java:MiniXDS"
            dbCreate = "update"
            initialOperation = "REFRESH"
            initialData = "data/test/production.xml"

        }
    }

}