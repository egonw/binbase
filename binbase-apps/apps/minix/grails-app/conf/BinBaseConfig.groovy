import org.codehaus.groovy.grails.commons.ConfigurationHolder

/**
 * this file contains the binbase configuration for this plugin.
 *
 */
binbase {
    key = "${ConfigurationHolder.config.binbase.key}"

    server = "${ConfigurationHolder.config.binbase.server}"
}
