package minix

import binbase.web.core.Association
import binbase.web.core.BBExperimentClass

/**
 * a basic class for experiments
 */
class BaseClass extends BBExperimentClass implements Serializable, Comparable {


    static searchable = {
        mapping {
            supportUnmarshall false
            only:['name']
        }
        comments component: true
    }

    /**
     * associated studie
     */
    Studie studie

    /**
     * associated comments
     */
    SortedSet<Comment> comments

    //we only belong to a studie
    static belongsTo = [studie: Studie]

    static hasMany = [comments: Comment]

    final int compareTo(Object t) {

        if (t instanceof BaseClass) {

            if (this.id != null && t.id != null) {

                BaseClass x = t

                if (x.id == null && this.id == null) {
                    return this.toString().compareTo(x.toString())
                }

                return id.compareTo(x.id)
            }
        }
        return toString().compareTo(t.toString())
    }


    final String toString() {
        if (this.id != null) {
            return this.id
        }
        else {
            if (this.name == null) {
                return super.toString()
            }
            return this.name
        }
    }


    static constraints = {
        name(nullable: true)
    }

    void setStudie(Studie studie) {
        this.experiment = studie
        this.studie = studie
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        studie(nullable: false)
        tablePerHierarchy false
        samples cascade: 'all'


    }

    /**
     * set name before insert to the same as id
     */
    def afterInsert = {

        if (name == null) {
            name = id.toString()
        }

        if (experiment == null && studie != null) {
            experiment = studie
        }
    }

    /**
     * returns the samples for this class
     * @return

     SortedSet < Sample >  getSamples() {
     log.info "listing samples..."
     return  Collections.unmodifiableSortedSet(new TreeSet<Sample>())
     }
     */

}
