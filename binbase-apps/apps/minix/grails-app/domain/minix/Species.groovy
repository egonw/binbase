package minix

import binbase.web.core.BBSpecies
/**
 * species information
 */
class Species extends BBSpecies implements Comparable {

    static auditable = true

    static hasMany = [comments: Comment]

    static searchable = {
        only: ['comments', 'name', 'organs']
        comments component: true

        mapping {
            supportUnmarshall false
        }
    }

    /**
     * associated comments
     */
    SortedSet<Comment> comments

    String toString() {
        return name
    }


    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
    }

    int compareTo(Object t) {
        return toString().compareTo(t.toString())
    }

    /**
     * equals function
     * @param object
     * @return
     */
    boolean equals(Object object) {

        if (object instanceof BBSpecies) {
            return object.id.equals(id)
        }

        return false
    }
}
