package minix
/**
 * standard gctof instrument
 */
class GCTof extends Instrument {

    static auditable = true

    static searchable = {
        mapping {
            supportUnmarshall false
        }

    }
    static hasMany = [gcMethods: String, msMethods: String, dpMethods: String]

    static constraints = {
        rollOver(nullable:true, unique:false)
    }

    /**
     * contains all the gc methods
     */
    Set<String> gcMethods

    /**
     * contains all the msMethods
     */
    Set<String> msMethods

    /**
     * contains all the dpMethods
     */
    Set<String> dpMethods

    /**
     * teh default folder for data analysis
     */
    String defaultFolder = "Acquired Samples"

    /**
     * size of the auto sample
     */
    int autoSamplerSize = 85

    /**
     * what is the first vial in the auto sampler
     */
    int autoSamplerStartPosition = 5

    //needed for sequence generation.
    boolean rollOver = true

    /**
     * which tray should we use for this gc tof
     */
    String useTray = "none"
}


