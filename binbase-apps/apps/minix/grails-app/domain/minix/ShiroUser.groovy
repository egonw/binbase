package minix

import org.apache.shiro.SecurityUtils
import minix.roles.RoleUtil
import javax.servlet.http.HttpSessionBindingListener
import javax.servlet.http.HttpSessionBindingEvent
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.context.request.RequestContextHolder

/**
 * a basic user
 */
class ShiroUser implements HttpSessionBindingListener {

    static auditable = true

    static searchable = {
        only: ['username', 'email', 'firstName', 'lastName']
        mapping {
            supportUnmarshall false
        }
    }

    String username
    String passwordHash

    String firstName
    String lastName

    String email

    String samplePrefix

    boolean locked

    /**
     * is this user a guest
     */
    Boolean guest = false

    Set<ShiroRole> roles

    def userService

    /**
     * related samples
     */
    static hasMany = [
            roles: ShiroRole,
            permissionMappings: ShiroUserPermission,
            studies: Studie,
            studieDesigns: StudieDesign,
            comments: Comment
    ]

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false

        permissionMappings column: 'permission_id'
    }

    /**
     * constraints
     */
    static constraints = {
        username(nullable: false, blank: false)
        passwordHash(nullable: false, blank: false)
        studies(nullable: true)
        firstName(nullable: true, blank: false)
        lastName(nullable: true, blank: false)
        email(nullable: false, blank: false, email: true)
        samplePrefix(size: 2..2, blank: false, nullable: false, unique: false)
        roles(nullable: false)
        guest(nullable: true)
    }

    /**
     * associated comments
     */
    SortedSet<Comment> comments

    /**
     * studies of this user owns
     */
    SortedSet<Studie> studies

    /**
     * designs of this user
     */
    SortedSet<StudieDesign> studieDesigns

    /**
     * returns all our generated permissions
     * @return
     */
    Set<String> getPermissions() {
        Set set = new HashSet()

        this.permissionMappings.each {
            set.add it.permission
        }

        return set
    }


    String toString() {
        return username.toString()
    }

    /**
     * does the current logged in user has managment rights?
     * @return
     */
    static boolean hasManagmentRights(ShiroUser user = null) {

        if (user == null) {
            user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

        }
        if (hasAdminRights(user)) {
            return true
        }

        ShiroRole role = RoleUtil.getManagerRole()

        for (ShiroRole r: user.roles) {
            if (r == role) {
                return true
            }
        }

        return false
    }

    /**
     * does the user own the studie
     * @param studie
     * @param user
     * @return
     */
    static boolean ownsStudy(Studie studie, ShiroUser user = null) {

        if (user == null) {
            user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

        }

        if (studie.users != null) {
            if (studie.users.contains(user)) {
                return true
            }
        }

        return false
    }
    /**
     * does the currently logged in user has admin rights
     * @return
     */
    static boolean hasAdminRights(ShiroUser user = null) {

        if (user == null) {
            user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

        }

        if (user.username == "admin") {
            return true
        }


        ShiroRole role = RoleUtil.getAdminRole()

        for (ShiroRole r: user.roles) {
            if (r == role) {
                return true
            }
        }

        return false
    }


    @Override
    public void valueBound(HttpSessionBindingEvent event) {

        userService.logLogin(this.username)

        Set<Long> logins = (Set<Long>) event.getSession().getServletContext().getAttribute("logins");
        if (logins == null) {
            logins = new HashSet<Long>()

        }
        log.info "user just logged in and is stored in login set: ${this}"
        logins.add(this.id);

        event.getSession().getServletContext().setAttribute("logins", logins);
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {

        userService.logLogout(this.username)

        Set<Long> logins = (Set<Long>) event.getSession().getServletContext().getAttribute("logins");
        log.info "user just logged out and was removed from login set: ${this}"

        logins.remove(this.id);

        event.getSession().getServletContext().setAttribute("logins", logins);
    }
}
