package minix

/**
 * special class for reaction blanks
 */
class ReactionBlankClass extends BaseClass {

    static auditable = true

    static searchable = {
        mapping {
            supportUnmarshall false
            only: ['name']
        }
    }

    static hasMany = [samples: ReactionBlankSample]

    static constraints = {
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }
}
