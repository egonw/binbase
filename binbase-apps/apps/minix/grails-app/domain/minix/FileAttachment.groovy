package minix

import binbase.web.core.BBDownload
/**
 * basic file attachment
 */
class FileAttachment extends BBDownload {

    static auditable = true

    static searchable = false

    /**
     * general constraints
     */
    static constraints = {
        studie(nullable: false)
    }

    static belongsTo = [studie: Studie]

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
    }

    Studie studie

}
