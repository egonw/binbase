package minix

/**
 * specific class for calibration settings
 */
class CalibrationClass extends BaseClass  implements Serializable, Comparable{

    static auditable = true

    static searchable = {
        mapping {
            supportUnmarshall false
            only:['name']
        }
    }

    static hasMany = [samples: CalibrationSample]

    static constraints = {
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }
}
