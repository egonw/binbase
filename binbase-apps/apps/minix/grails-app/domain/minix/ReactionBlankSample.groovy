package minix

import binbase.web.core.BBExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.DateUtil

/**
 * specific reaction blanks
 */
class ReactionBlankSample extends BBExperimentSample implements Serializable, Comparable {

    static auditable = true


    static searchable = {
        only: ["fileName", "comment", "label", 'experimentClass']
        mapping {
            supportUnmarshall false
        }

    }

    static belongsTo = [experimentClass: ReactionBlankClass]

    static hasMany = [comments: Comment]

    static constraints = {

        //range defines the current version
        fileVersion(range: 1..200)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }

    /**
     * the scheduled date for this sample
     */
    Date scheduleDate = DateUtil.stripTime(new Date())

    /**
     * associated comments
     */
    SortedSet<Comment> comments

    int fileVersion = 1

    int compareTo(Object t) {
        if (t instanceof ReactionBlankSample) {
            ReactionBlankSample s = t

            if (s.fileName != null && fileName != null) {
                return fileName.compareTo(s.fileName)
            }
            if (s.id != null && id != null) {
                return id.compareTo(s.id)
            }

        }
        return toString().compareTo(t.toString())
    }


    String toString() {
        return "${fileName}_${fileVersion}"
    }

    /**
     * generates the file name to be used with metadata and so
     * @return
     */
    public String generateName() {
        return "${fileName}_${fileVersion}"
    }
}
