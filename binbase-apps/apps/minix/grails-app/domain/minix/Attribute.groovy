package minix
/**
 * simple attribbute for a sample
 */
class Attribute implements Serializable {

    static auditable = true

    /**
     * owner of this object
     */
    static belongsTo = [sample: Sample, key: Key]


    static constraints = {
        value(unique: false, blank: false)
        key(nullable: false)

    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false

    }

    /**
     * name of the attribute
     */
    Key key

    /**
     * value of the attribute
     */
    String value

    /**
     * related sample
     */
    Sample sample

    /**
     * string representation
     * @return
     */
    String toString() {
        return "${name} - ${value}"
    }
}
