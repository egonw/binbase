package minix

import binbase.web.core.BBOrgan
/**
 * defines an organ
 */
class Organ extends BBOrgan implements Comparable {

    static auditable = true

    static searchable = {
        only: ['comments', 'name']
        comments component: true
        mapping {
            supportUnmarshall false
        }


    }

    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
    }

    static hasMany = [comments: Comment]
    /**
     * associated comments
     */
    SortedSet<Comment> comments

    /**
     * string representation
     * @return
     */
    String toString() {
        return name
    }


    int compareTo(Object t) {
        return toString().compareTo(t.toString())
    }
}
