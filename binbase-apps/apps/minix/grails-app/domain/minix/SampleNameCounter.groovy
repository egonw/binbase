package minix
/**
 * used to generate a sample name
 */
class SampleNameCounter {

    static auditable = true

    static searchable = false

    static constraints = {
        counter(min: 0, max: 100000, nullable: false)
        prefix(unique: true, nullable: false)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false

    }

    /**
     * the count for the specific date
     */
    Integer counter

    /**
     * prefix of the date
     */
    String prefix
}
