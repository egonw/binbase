package minix

import binbase.web.core.BBExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.DateUtil

class QualityControlSample extends CalibrationSample implements Serializable, Comparable {

    static auditable = true


    static searchable = {
        only: ["fileName", "comment", "label", 'experimentClass']
        mapping {
            supportUnmarshall false
        }

    }

    static belongsTo = [experimentClass: QualityControlClass]

    static constraints = {

        //range defines the current version
        fileVersion(range: 1..200)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }

}
