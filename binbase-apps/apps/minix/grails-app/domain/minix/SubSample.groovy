package minix

/**
 * a sub sample
 */
class SubSample {

    /**
     * relates experiment clazz
     */
    SubExperimentClass clazz

    /**
     * related sample id
     */
    Sample relatesToSample

    static belongsTo = [clazz:SubExperimentClass]

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }

    String toString() {
        "sub sample (${id})of ${relatesToSample}"
    }
}
