package minix
/**
 * basic key for an attribute
 */
class Key {

    static auditable = true

    static searchable = false

    static hasMany = [attributes: Attribute]

    static constraints = {
        name(unique: true, blank: false)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false

    }

    /**
     * name of this key
     */
    String name

    /**
     * associated attributes
     */
    Set<Attribute> attributes


    String toString() {
        return name.toString()
    }
}
