package minix
/**
 * a studie based on several other studies
 */
class MergedStudie extends Studie {


	static constraints = {

	}

	static mapping = {
		version false

		tablePerHierarchy false
		discriminator column: "studie_merge"
	}

	static belongsTo = [ShiroUser]

	static hasMany = [members: Studie]

	SortedSet<Studie> members

	/**
	 * samples of this class
	 * @return
	 */
	@Override
	SortedSet<BaseClass> getClasses() {
		SortedSet<BaseClass> internal = new TreeSet<BaseClass>()
		if (members != null) {
			members.each { Studie s ->
				s.classes.each { c ->
					if (c instanceof BaseClass) {
						internal.add(c)
					}
				}
			}
		}

		//set can't be modified!
		return Collections.unmodifiableSortedSet(internal)
	}

	@Override
	void setClasses(SortedSet<BaseClass> classes) {
		//you can't set classes since internally the members provide the classes
	}

	String toString() {
		return "${description} - ${members}"
	}
}
