package minix

import binbase.web.core.BBExperiment
import binbase.web.core.BBResult
import org.apache.shiro.SecurityUtils

/**
 * basic studie
 */
class Studie extends BBExperiment implements Serializable, Comparable {

    static auditable = true

    static searchable = {
        mapping {
            boost 9.0
            spellCheck "include"
            only: ['description', 'comments', 'classes', 'title', 'user', 'name', 'classes', 'id']
            supportUnmarshall false
            comments component: true
        }


    }

    /**
     * each studie needs an architecture
     */
    static belongsTo = [ShiroUser]

    /**
     * associations
     */
    static hasMany = [classes: BaseClass, users: ShiroUser, attachments: FileAttachment, comments: Comment, subStudies: SubStudie]

    /**
     * general constraints
     */
    static constraints = {
        description(blank: false, minSize: 5, maxSize: 1000, unique: false)
        users(nullable: true)
        aquisitionTable(nullable: true)
        databaseColumn(nullable: true)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false
        discriminator column: "studie"
        classes cascade: "all,delete-orphan"
    }

    /**
     * the latest database column this studie was run on
     */
    String databaseColumn

    /**
     * description of this studie
     */
    String description

    /**
     * associated classes for this experiment
     */
    SortedSet<BaseClass> classes

    /**
     * related users
     */
    Set<ShiroUser> users

    /**
     * related studies
     */
    Set<SubStudie> subStudies

    /**
     * associated attachments
     */
    SortedSet<FileAttachment> attachments

    /**
     * associated comments
     */
    SortedSet<Comment> comments

    /**
     * the related architecture
     */
    Architecture architecture

    /**
     * associated aquisition table
     */
    AquisitionDataFile aquisitionTable

    /**
     * do we have generated sample files
     */
    boolean sampleFilesGenerated = false

    /**
     * to string function
     * @return
     */
    String toString() {
        return "${description}"
    }

    int compareTo(Object t) {
        if (t != null) {

            if (t instanceof Studie) {
                if (this.id != null && t.id != null) {
                    return this.id.compareTo(t.id)

                }
            }

            return this.toString().compareTo(t.toString())
        }

        return 0

    }

    /**
     * this this studie been calculated
     * @return
     */
    boolean hasBeenExported() {
        if (this.databaseColumn != null) {
            return true
        } else if (this.results != null && this.results.size() > 0) {
            return true
        }
        return false
    }

    /**
     * can the user access this studie
     * @param user
     * @return
     */
    boolean canAccess(ShiroUser user = null) {
        if (user == null) {
            user = ShiroUser.findByUsername(SecurityUtils.subject.principal)
        }

        if (this.users.contains(user)) {
            return true
        } else if (ShiroUser.hasAdminRights(user)) {
            return true
        } else return ShiroUser.hasManagmentRights(user)
    }

    /**
     * can this study still be modified. This means samples or classes can be added to it
     * @return
     */
    boolean canModify() {
        if (this.aquisitionTable == null) {

            //this generates to many queries...
            if (this.results == null || this.results.size() == 0) {
                if (this.attachments == null || this.attachments.size() == 0) {
                    if (this.subStudies == null || this.subStudies.size() == 0) {
                        return true
                    }
                }
            }
        }

        return false
    }

    /**
     * can we delete this study
     * @return
     */
    boolean canDelete() {

        //this generates to many queries...
        if (this.results == null || this.results.size() == 0) {
            if (this.attachments == null || this.attachments.size() == 0) {
                if (this.subStudies == null || this.subStudies.size() == 0) {
                    return true
                }
            }
        }

        return false
    }

    /**
     * forces this studie to be locked
     * @return
     */
    void forceLock() {
        if (canModify()) {
            this.aquisitionTable = new AquisitionDataFile()
            this.save()
        } else {
            log.debug "study was already locked..."
        }
    }

    /**
     * forces this studie to be locked
     * @return
     */
    void forceUnLock() {
        if (!canModify()) {
            this.aquisitionTable = null
            this.save()
        } else {
            log.debug "study was already unlocked..."
        }
    }

    /**
     * returns the most recent result
     * @return
     */
    BBResult mostRecentResult(){

        SortedSet<BBResult> res = getResults()

        if(res == null){
            return null
        }

        if(res.isEmpty()){
            return null
        }

        return res.sort {it.uploadDate}.first();

    }
}
