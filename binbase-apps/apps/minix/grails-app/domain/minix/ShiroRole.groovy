package minix

class ShiroRole {

    static auditable = true

    String name

    static searchable = false

    static hasMany = [users: ShiroUser, permissions: String]
    static belongsTo = ShiroUser

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false

    }

    static constraints = {
        name(nullable: false, blank: false, unique: true)
    }

    String toString() {
        return name
    }

    boolean equals(o) {
        if (this.is(o)) return true;
        if (getClass() != o.class) return false;

        ShiroRole shiroRole = (ShiroRole) o;

        if (name != shiroRole.name) return false;

        return true;
    }

    int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }
}
