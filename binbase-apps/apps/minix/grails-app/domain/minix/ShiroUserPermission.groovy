package minix
class ShiroUserPermission {

    static auditable = true

    static belongsTo = ShiroUser

    static searchable = false

    static constraints = {
        permission(nullable: false, blank: false)

    }
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false

    }

    ShiroUser user

    String permission

    String toString() {
        return permission
    }

}
