package minix
/**
 * a simple instruments
 */
class Instrument implements Comparable {

    static auditable = true


    static searchable = {

        mapping {
            supportUnmarshall false
        }

        only : ['name','description']
    }

    static belongsTo = [architecture: Architecture]

    /**
     * constraints
     */
    static constraints = {
        name(blank: false, nullable: false, unique: false)
        identifier(blank: false, nullable: false, unique: true, size: 1..1)
        description(blank: false, nullable: false, unique: false)
        dailyCapacity(blank: false, nullable: false, unique: false)

    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy true


    }

    /**
     * name of this instrument
     */
    String name

    /**
     * short description
     */
    String description

    /**
     * identifier
     */
    String identifier

    /**
     * what it belongs too
     */
    Architecture architecture

    /**
     * related platform
     */
    Platform platform

    /**
     * the daily capacity of this instrument
     */
    Integer dailyCapacity = 1

    int compareTo(Object t) {
        return toString().compareTo(t.toString())
    }

    String toString() {
        return "${name} - ${identifier}"
    }
}
