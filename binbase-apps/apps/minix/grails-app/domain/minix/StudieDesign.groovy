package minix

import minix.step.Step

/**
 * a studie design object and basis for any studie
 */
class StudieDesign implements Comparable {

    static auditable = true

    static searchable = {
        mapping {
            boost 1.0
            spellCheck "include"
            only: ['step', 'architecture', 'speciees', 'title', 'user']
            supportUnmarshall false

        }
    }

    //has many
    static hasMany = [
            speciees: Species,
            treatmentsSpecifics: TreatmentSpecific,
            designSpeciesConnections: DesignSpeciesConnection
    ]

    //defined constraints
    static constraints = {
        title(blank: false, unique: false, minSize: 5)
        generatedStudie(nullable: true, blank: true)
        step(nullable: false)
        user(nullable: true)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false

    }

    /**
     * current progress step of the design
     */
    Step step

    /**
     * related architecture
     */
    Architecture architecture

    /**
     * associated species
     */
    SortedSet<Species> speciees

    /**
     * all the species and organs
     */
    SortedSet<DesignSpeciesConnection> designSpeciesConnections

    /**
     * associated treatments with specifications
     */
    SortedSet<TreatmentSpecific> treatmentsSpecifics

    /**
     * one design can only contain one studie!
     */
    Studie generatedStudie

    /**
     * title of this studie
     */
    String title

    /**
     * number of samples
     */
    Integer numberOfSamples

    /**
     * shiro user who owns this design
     */
    ShiroUser user

    int compareTo(Object t) {
        return toString().compareTo(t.toString())
    }


    String toString() {
        return title.toString()
    }
}
