package minix

import binbase.web.core.BBExperimentSample

/**
 * a class which is based on several other base classes
 */
class MergeClass extends BaseClass {

    static constraints = {
    }


    static searchable = {
        mapping {
            supportUnmarshall false
            only:['name']

        }

    }

    static hasMany = [classes: BaseClass]

    /**
     * the classes on which this class is based off
     */
    SortedSet<BaseClass> classes

    /**
     * samples of this class
     * @return
     */
    @Override
    SortedSet<BBExperimentSample> getSamples() {
        SortedSet<BBExperimentSample> samples = new TreeSet<BBExperimentSample>()
        classes.each {BaseClass clazz ->
            clazz.samples.each { Sample sample ->
                samples.add(sample)
            }
        }

        //set can't be modified!
        return Collections.unmodifiableSortedSet(samples)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }
}
