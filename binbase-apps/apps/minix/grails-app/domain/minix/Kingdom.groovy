package minix

import binbase.web.core.BBKingdom
/**
 * kingdom of a species
 */
class Kingdom extends BBKingdom {

    static auditable = true

    static searchable = {
        only: ['name', 'comments']

        mapping {
            supportUnmarshall false
        }


    }

    static hasMany = [comments: Comment]

    /**
     * associated comments
     */
    SortedSet<Comment> comments

    /**
     * name of the kingdom
     */
    String name

    String toString() {
        return name
    }

    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
    }

}
