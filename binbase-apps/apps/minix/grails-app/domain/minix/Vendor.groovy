package minix

/**
 * a vendor for a platform
 */
class Vendor {

    /**
     * related platforms
     */
    static hasMany = [platforms: Platform]

    static constraints = {
        name(unique: true)
    }

    Set<Platform> platforms

    String name

    String toString() {
        return name
    }
}
