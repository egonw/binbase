package minix

/**
 * uses to track the users which login and which logout as login failures and ip's
 */
class UserTracking {

    static searchable = false

    static constraints = {
        ipAddress(nullable: false)
        username(nullable: false)
        event(inList: ["login", "logout", "failed"], nullable: false)
        countryCode(nullable: true)
        countryName(nullable: true)
        region(nullable: true)
        regionName(nullable: true)
        city(nullable: true)
        postalCode(nullable: true)
        timezone(nullable: true)
        latitude(nullable: true)
        longitude(nullable: true)
        dmaCode(nullable: true)
        areaCode(nullable: true)
        metroCode(nullable: true)
    }

    /**
     * mapping
     */
    static mapping = {
        version false
    }

    /**
     * ipAddress of the user
     */
    String ipAddress

    /**
     * the associated user
     */
    String username

    /**
     * the exact event of the action
     */
    String event
    /**
     * when did this happen
     */
    Date dateCreated

    String countryCode

    String countryName

    String region

    String regionName

    String city

    String postalCode

    String timezone

    String latitude

    String longitude

    String dmaCode

    String areaCode

    String metroCode
}
