/*
    required to upgrade from minix 0.1 to 0.2 and ensure all queries and features work
    this should also avoid all possibilities of dataloss
*/

insert into bbkingdom select * from kingdom where id not in (select id from bbkingdom)

GO

insert into bbspecies select * from species where id not in (select id from bbspecies)

GO

insert into bborgan select * from organ where id not in (select id from bborgan)

GO

insert into bbspecies_organs(bborgan_id,bbspecies_id) select organ_id,species_id from species_organs where species_id not in (select bbspecies_id from bbspecies_organs)

go

insert into bbexperiment (id,name,title) select id,description,description from studie

go

/*
  insert into bbexperiment_class (id,experiment_id,organ_id,species_id,name) select id,studie_id,name from base_class
 */

insert into bbexperiment_class (id,experiment_id,name,organ_id,species_id) select a.id,a.studie_id,a.name,b.organ_id,b.species_id from base_class a, experiment_class b where a.id = b.id

go

insert into bbexperiment_sample (id,comment,experiment_class_id,file_name,label) select id,comment,experiment_class_id,file_name,label from sample where id not in (select id from bbexperiment_sample)

go

//change the tables to fit with the new version
ALTER TABLE public.species
    DROP COLUMN kingdom_id
GO

ALTER TABLE public.species
    DROP COLUMN name
GO

ALTER TABLE public.organ
    DROP COLUMN name
GO

ALTER TABLE public.kingdom
    DROP COLUMN name
GO

/**
 update the result data
 */
insert into bbresult (id,content,experiment_id,file_name) select id,content,studie_id,to_char(studie_id,'99999999999') from result where id not in (select id from bbresult)

go

ALTER TABLE public.result
    DROP COLUMN content
GO
ALTER TABLE public.result
    DROP COLUMN studie_id
GO

/*
 set the sequences to the same value to avoid conflicts
 */

select nextval('binbase_core_id')

GO

select nextval('minix_id')

GO

select setval('binbase_core_id',currval('minix_id'))

GO
