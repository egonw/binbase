/**
mostly used to make the merge features worked and fix some outdated tables
 */
drop table species_organs
GO
ALTER TABLE public.experiment_class
    DROP COLUMN species_id
GO
ALTER TABLE public.experiment_class
    DROP COLUMN organ_id
GO
