package minix.util

import minix.Instrument
import minix.ShiroUser
import java.text.NumberFormat
import java.text.DecimalFormat


class FileNameGenerator {

    //format to ensure it always fits
    static NumberFormat format = new DecimalFormat("00");

    static String generateCalibrationSampleName(Calendar calendar, Instrument instrument, ShiroUser user) {
        return generateSampleName(calendar, instrument, user, "qc")

    }

    static String generateReactionBlankSampleName(Calendar calendar, Instrument instrument, ShiroUser user) {
        return generateSampleName(calendar, instrument, user, "bl")

    }

    static String generateSampleName(Calendar calendar, Instrument instrument, ShiroUser user, String type = "sa") {
        //has a year 10k problem, I doubt I ever run into this issue...
        String year = "${calendar.get(Calendar.YEAR).toString().substring(2, 4)}"
        String month = "${format.format(calendar.get(Calendar.MONTH) + 1)}"
        String day = "${format.format(calendar.get(Calendar.DAY_OF_MONTH))}"


        return "${year}${month}${day}${instrument.identifier}${user.getSamplePrefix()}${type}"
    }
}
