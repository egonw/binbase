package minix.util

import minix.StudieDesign
import minix.TreatmentSpecific

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Nov 16, 2010
 * Time: 12:26:54 PM
 * To change this template use File | Settings | File Templates.
 */
class TreatmentUtil {

  /**
   * used to calculate the level of the deepest treatment
   * @param design
   * @return
   */
  int calculateDeepestTreatmentLevel(StudieDesign design) {

    int level = 0

    design.treatmentsSpecifics.each {TreatmentSpecific specific ->

      int calculated = calculateDeepestChildLevel(specific)

      if (calculated >= level) {
        level = calculated
      }
    }

    return level
  }

  /**
   * calculates the level of the given treatment
   * @param specific
   * @return
   */
  int calculateLevel(TreatmentSpecific specific) {

    int level = 0
    while (specific.parent != null) {

      level = level + 1
      specific = specific.parent
    }

    return level;
  }

  /**
   * calculates the level of the given treatment
   * @param specific
   * @return
   */
  int calculateDeepestChildLevel(TreatmentSpecific specific) {

    int level = 0

    specific.subTreatmentSpecifics.each {TreatmentSpecific sub ->
      int cur = 0
      if (sub.subTreatmentSpecifics == null || sub.subTreatmentSpecifics.size() == 0) {
        cur = calculateLevel(sub)
      }
      else {
        cur = calculateDeepestChildLevel(sub)
      }

      if (cur >= level) {
        level = cur
      }
    }

    return level;
  }

  /**
   * builds a list of treatment specifics for easy rendering
   */
  List buildTreatmentSpecificList(TreatmentSpecific specific, List result) {
    result.add(specific)
    specific.subTreatmentSpecifics.each {TreatmentSpecific current ->
      result.add(
              buildTreatmentSpecificList(current, [])
      )
    }

    return result
  }

  /**
   * builds a list of all treatments to the parent treatment
   */
  List buildListToParent(TreatmentSpecific specific, List result) {

    result.add(specific)
    if (specific.parent != null) {
      buildListToParent(specific.parent, result)
    }
    
    return result
  }

}
