package minix.util

import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/20/11
 * Time: 5:04 PM
 * To change this template use File | Settings | File Templates.
 */
class ShuffleDataFile {

  /**
   * shuffles this datafile and keeps the first row as header.
   * @param file
   * @return
   */
  static void shuffle(SimpleDatafile file) {

    def data = file.data

    def header = data[0]

    def content = []

    for (int i = 1; i < data.size(); i++) {
      content.add(data[i])
    }

    data = []

    Collections.shuffle(content)

    data.add header
    content.each {
      data.add it
    }

    file.data = data

  }
}
