package minix.util

import grails.validation.ValidationException
import binbase.web.core.ExternalId
import binbase.web.core.Association

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/2/11
 * Time: 5:50 PM
 * To change this template use File | Settings | File Templates.
 */
class ExternalIdUtil {

  static ExternalId generateExternalId(String id, Association association) {
    ExternalId externalId = new ExternalId()
    externalId.value = id
    externalId.association = association

    if (externalId.validate() == false) {
      throw new ValidationException("an error occured", externalId.errors)
    }

    externalId.save()

    return externalId
  }
}
