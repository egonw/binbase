package minix

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 11/19/13
 * Time: 10:10 AM
 */
enum FileType{
    BINBASE_TXT, BINBASE_NETCDF
}