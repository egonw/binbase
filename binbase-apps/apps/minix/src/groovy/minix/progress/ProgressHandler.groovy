package minix.progress

/**
 * used to handle progress bars and progress in general
 */
public interface ProgressHandler {

  /**
   * provide the value of the current progress
   * @param percent
   */
  public void handleProgress(String id, double percent,def properties)

}