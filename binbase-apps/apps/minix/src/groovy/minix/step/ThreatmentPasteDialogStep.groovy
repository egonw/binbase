package minix.step

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 3/30/11
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
enum ThreatmentPasteDialogStep {

    TREATMENT("treatment name"),
    TREATMENT_VALUE("treatment specific value"),
    IGNORE("ignore")

    private String label

    ThreatmentPasteDialogStep(String label) {
        this.label = label
    }

    static def listEnums() {
        return [TREATMENT, TREATMENT_VALUE, IGNORE]
    }

    String getLabel() {
        return this.label
    }

    String toString() {
        return getLabel()
    }
}
