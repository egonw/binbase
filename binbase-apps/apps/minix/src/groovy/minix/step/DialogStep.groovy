package minix.step

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 2/1/11
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */
public enum DialogStep {

  COMMENT("comment"),
  LABEL("label"),
  IGNORE("ignore"),
FILENAME("filename")


  private String label

  DialogStep(String label) {
    this.label = label
  }

  static def listEnums() {
    return [COMMENT, FILENAME, LABEL, IGNORE]
  }

  String getLabel() {
    return this.label
  }
}