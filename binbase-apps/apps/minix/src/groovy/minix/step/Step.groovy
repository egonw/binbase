package minix.step

/**
 *  defines the current step in our design
 */
public enum Step {

  DEFINE_SPECIES(0, "designSpecies", DEFINE_ORGAN, null, false, "Define Species and Title"),
  DEFINE_ORGAN(1, "designOrgans", DEFINE_SPECIES, DEFINE_TREATMENTS, true, "Define Organs"),
  DEFINE_TREATMENTS(2, "designTreatments", DEFINE_ORGAN, DEFINE_CLASS, true, "Define Treatments"),
  DEFINE_CLASS(3, "designSampleProperties", GENERATE_CLASS, DEFINE_TREATMENTS, true, "Define Class Properties"),
  GENERATE_CLASS(4, "designClasses", ASSIGN_SAMPLES, DEFINE_CLASS, false, "Select Classses for generation"),
  ASSIGN_SAMPLES(5, "designFileNames", DONE, GENERATE_CLASS, false, "Modify Classes"),
  DONE(6, "designDone", null, ASSIGN_SAMPLES, false, "Studie Design is done")

  private final Double progress

  private final String method

  private final String message

  private final Step next

  private final Step previous

  private boolean canGoBack

  Step(double progress, String method, Step next, Step previous, boolean canGoBack, String message) {
    this.progress = progress
    this.next = next
    this.method = method
    this.previous = previous
    this.canGoBack = canGoBack
    this.message = message
  }

  boolean hasNext() {
    return next != null
  }

  boolean hasPrevious() {
    return previous != null
  }

  Step getNextStep() {
    return next
  }

  Step getPreviousStep() {
    return previous
  }

  Double getProgress() {
    return progress
  }

  String getMethod() {
    return method
  }

  String getMessage() {
    return message
  }

  boolean isCanGoBack() {
    return canGoBack
  }

  /**
   * returns all our enums
   * @return
   */
  static def listEnums() {
    return [DEFINE_SPECIES, DEFINE_ORGAN, DEFINE_TREATMENTS, DEFINE_CLASS, GENERATE_CLASS, ASSIGN_SAMPLES, DONE]
  }
}