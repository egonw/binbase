package minix.generate

import minix.ExperimentClass


import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceFactory
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import org.codehaus.groovy.grails.commons.ConfigurationHolder

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 2/18/14
 * Time: 4:53 PM
 */
class DSL {

    /**
     * generates a DSL, which uses the system default DSL
     * @param experiment
     * @param server
     * @param classSize
     * @param logging
     * @return
     */
    public String dslGenerateForExperimentWithDefaultSOP(Experiment experiment, String reference, boolean newBins , boolean caching) {

        String database = experiment.getColumn()
        String setupXName = experiment.getId()

        StringBuffer buffer = new StringBuffer()

        int sampleSize = 0;

        experiment.classes.each { ExperimentClass c ->
            c.samples.each {
                sampleSize = sampleSize + 1
            }
        }

        generalSettings(buffer, setupXName, database, ConfigurationHolder.config.binbase.server, experiment, sampleSize,newBins,caching)

        buffer.append("\n")
        buffer.append("\t//using default sop")
        buffer.append("\n")


        buffer.append("\tdefaultSop true")
        buffer.append("\n")

        //assign the reference
        if (reference != null) {

        }

        //standard config element
        buffer.append("\n\tconfig{\n\t}\n")

        generateLogging(buffer, experiment)


        buffer.append("}\n")
        buffer = generateClasses(experiment, buffer)


        buffer.append("//end of DSL definition\n")

        String dsl = buffer.toString()

        return dsl
    }


    private void generalSettings(StringBuffer buffer, String setupXName, String database, String server, Experiment experiment, int sampleSize,boolean newBins,boolean caching) {
        buffer.append("""

		    /**
		     *
		     * for:
		     *
		     *     experiment: ${setupXName}
		     *     database:   ${database}
		     *     server:     ${server}
		     *
		     *
		     * information:
		     *     classes:    ${experiment.classes.length}
		     *     samples:    ${sampleSize}
		     *
		     * please modify it to you liking or use it as it is
		     */

		    """)


        buffer.append("\n")

        buffer.append("export{")
        buffer.append("\n")

        buffer.append("\t//name of the generated experiment")
        buffer.append("\n")
        buffer.append("\tname \"${setupXName}\"")
        buffer.append("\n")

        buffer.append("\n")
        buffer.append("\t//server of the used binbase instance")
        buffer.append("\n")
        buffer.append("\tserver \"${server}\"")
        buffer.append("\n")


        buffer.append("\n")
        buffer.append("\t//database of the used binbase instance")
        buffer.append("\n")

        buffer.append("\tcolumn \"${database}\"")
        buffer.append("\n")

        buffer.append("\n")
        buffer.append("\t//are new bins permitted")
        buffer.append("\n")

        buffer.append("\tnewBins ${newBins}")
        buffer.append("\n")


        buffer.append("\n")
        buffer.append("\t//is caching permitted")
        buffer.append("\n")

        buffer.append("\tcaching ${caching}")
        buffer.append("\n")


        buffer.append("\n")
        buffer.append("\t//is multithread permitted")
        buffer.append("\n")

        buffer.append("\tmultithread false")
        buffer.append("\n")




        buffer.append("\n")
        buffer.append("\t//do we want to ignore missing samples from calculations, as a warning the complete class will be ignored!")
        buffer.append("\n")

        buffer.append("\tignoreMissingSamples false")
        buffer.append("\n")
    }

    private StringBuffer generateLogging(StringBuffer buffer, Experiment experiment) {
        buffer.append("""

        logging{
                'log4j:configuration'('xmlns:log4j':"http://jakarta.apache.org/log4j/"){
                        appender(name:"console", class:"org.apache.log4j.ConsoleAppender"){
                                param(name:"Target", value:"System.out")
                                //the minimum logging threshold
                                param(name:"Threshold", value:"warn")
                                //a layout example
                                layout(class:"org.apache.log4j.PatternLayout"){
                                        param(name:"ConversionPattern",value:"[%d{ABSOLUTE}] [%-5p] [%t] [%-5c] [%m]%n")
                                }
                        }
                        appender(name:"file", class:"org.apache.log4j.FileAppender"){
                                param(name:"File", value:"log/${experiment.getId()}/${new Date().format("yyyyMMMdd-HHmm")}.log")
                                param(name:"Append", value:"false")
                                //the minimum logging threshold
                                param(name:"Threshold", value:"info")
                                //a layout example
                                layout(class:"org.apache.log4j.PatternLayout"){
                                        param(name:"ConversionPattern",value:"[%d{ABSOLUTE}] [%-5p] [%t] [%-5c] [%m]%n")
                                }
                        }

                        logger(name:"edu.ucdavis.genomics.metabolomics.binbase"){
                                level(value:"debug")
                        }

                       logger(name:"edu.ucdavis.genomics.metabolomics.util.database.WrappedConnection"){
                            level(value:"error")
                        }
                        logger(name:"net"){
                                level(value:"error")
                        }
                        logger(name:"net"){
                                level(value:"error")
                        }
                        logger(name:"org"){
                                level(value:"error")
                        }
                        root{
                                "appender-ref"(ref:"console")
				                "appender-ref"(ref:"file")
                        }
                }
        }



			""")
    }

    /**
     * generates a list of classes
     * @param experiment
     * @param buffer
     */
    def generateClasses(Experiment experiment, StringBuffer buffer) {

        //fetch the classes for the experiment
        experiment.getClasses().each { ExperimentClass clazz ->

            //execute the closure todo something with the clazz before we use it

            if (clazz != null) {


                if (clazz.getSamples().length > 0) {
                    buffer.append("\n")
                    buffer.append("\t//definition for class: ${clazz.getId()} with ${clazz.getSamples().length} samples")
                    buffer.append("\n")


                    buffer.append("\tClass(name: \"${clazz.getId()}\") {")
                    buffer.append("\n")

                    //get over all the samples
                    clazz.getSamples().each { ExperimentSample sample ->


                        buffer.append("\t\tsample \"${sample.getName()}\"")
                        buffer.append("\n")

                    }

                    buffer.append("\t}")
                    buffer.append("\n")
                    buffer.append("\t//end of definition for class: ${clazz.getId()}")
                    buffer.append("\n")
                }
            } else {
                buffer.append("\t//the provided class was null for some reason => skipped\n\n")
            }
        }

        return buffer
    }
}
