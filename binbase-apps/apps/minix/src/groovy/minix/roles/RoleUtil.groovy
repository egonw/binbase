package minix.roles

import minix.ShiroRole

/**
 * utility to easily generate roles on the fly
 */
class RoleUtil {

    /**
     * generates a role for you on the fly
     * @param name
     * @return
     */
    static ShiroRole getRole(String name) {
        ShiroRole role = ShiroRole.findByName(name)
        if (role == null) {
            role = new ShiroRole()
            role.name = name
            role.save(flush: true)
        }

        return role
    }

    static ShiroRole getAdminRole() {
        return getRole("admin")
    }

    static ShiroRole getUserRole() {
        return getRole("user")
    }

    static ShiroRole getManagerRole() {
        return getRole("manager")
    }


}
