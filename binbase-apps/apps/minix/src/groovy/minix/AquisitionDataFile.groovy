package minix

import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/20/11
 * Time: 7:11 PM
 * To change this template use File | Settings | File Templates.
 */
class AquisitionDataFile extends SimpleDatafile implements Serializable{

  static final long serialVersionUID = 2L

  /**
   * shuffles the file
   */
  void shuffle() {

    def data = this.data

    def header = data[0]

    def content = []

    for (int i = 1; i < data.size(); i++) {
      content.add(data[i])
    }

    data = []

    Collections.shuffle(content)

    data.add header
    content.each {
      data.add it
    }

    this.data = data
  }

  @Override
  void write(File file) {
    this.write(new FileWriter(file))
  }

  @Override
  void write(OutputStream out) {
    this.write(new OutputStreamWriter(out))
  }

  @Override
  void write(Writer writer) {
    writer.write("Acquisition Sequence Table Version 2")
    writer.write("\n")
    writer.flush()

    //lets the server finish the rest
    super.write(writer)
  }

  @Override
  InputStream toInputStream() {
    ByteArrayOutputStream out = new ByteArrayOutputStream()
    write(out)
    out.flush()

    byte[] outArray = out.toByteArray()
    out.close()
    return new ByteArrayInputStream(outArray)
  }

  @Override
  void print(PrintStream out) {
    this.write(out)
  }


}
