<% import grails.persistence.Event %>
<%=packageName%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
  <link rel="stylesheet" href="\${resource(dir: 'css', file: 'scaffolding.css')}"/>

  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>

<%
  excludedProps = Event.allEvents.toList() << 'version'
  props = domainClass.properties.findAll { !excludedProps.contains(it.name) }
  Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
%>

<div class="body">

  <div class="center-full-page">

    <div class="header"><g:message code="default.show.label" args="[entityName]"/></div>
    <g:if test="\${flash.message}">
      <div class="message">\${flash.message}</div>
    </g:if>

    <div class="scaffoldingProperties">
      <%
        props.each { p ->
      %>


      <span class="scaffoldingPropertyLabel">
        <g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/>
      </span>

      <span class="scaffoldingPropertyValue">

        <% if (p.isEnum()) { %>
        \${${propertyName}?.${p.name}?.encodeAsHTML()}
        <% } else if (p.oneToMany || p.manyToMany) { %>

        <ul class="scaffoldingList">
          <g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
            <li class="scaffoldingItem">
              <g:link class="showDetails" controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link>
            </li>
          </g:each>
        </ul>
        <% } else if (p.manyToOne || p.oneToOne) { %>

        <g:if test="\${${propertyName}?.${p.name} != null}">
          <g:link class="showDetails" controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link>
        </g:if>

        <% } else if (p.type == Boolean.class || p.type == boolean.class) { %>
        <g:formatBoolean boolean="\${${propertyName}?.${p.name}}"/>
        <% } else if (p.type == Date.class || p.type == java.sql.Date.class || p.type == java.sql.Time.class || p.type == Calendar.class) { %>
        <g:formatDate date="\${${propertyName}?.${p.name}}"/>
        <% } else { %>
        \${fieldValue(bean: ${propertyName}, field: "${p.name}")}
        <% } %>

      </span>

      <%
        }
      %>
    </div>
  </div>
</div>
</body>
</html>
