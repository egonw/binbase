<% import grails.persistence.Event %>
<%=packageName%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
  <link rel="stylesheet" href="\${resource(dir: 'css', file: 'scaffolding.css')}"/>

  <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
  <div class="design">

    <div class="left">
      <div class="header">

        Information

      </div>


      <div class="left_information_box">
        please provide the required informations to create this object
      </div>

    </div>
    <div class="left-center-column">
      <g:if test="\${flash.message}">
        <div class="message">\${flash.message}</div>
      </g:if>

      <g:hasErrors bean="\${${propertyName}}">
        <div class="errors">
          <g:renderErrors bean="\${${propertyName}}" as="list"/>
        </div>
      </g:hasErrors>

      <g:form action="save" method="post" <%= multiPart ? ' enctype="multipart/form-data"' : '' %>>
        <div class="scaffoldingProperties">

          <% excludedProps = Event.allEvents.toList() << 'version' << 'id'
          props = domainClass.properties.findAll { !excludedProps.contains(it.name) }
          Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
          props.each { p ->
            if (!Collection.class.isAssignableFrom(p.type)) {
              cp = domainClass.constrainedProperties[p.name]
              display = (cp ? cp.display : true)
              if (display) { %>

          <div>
            <span class="scaffoldingPropertyLabel">

              <label for="${p.name}"><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/></label>

            </span>
            <span class="scaffoldingPropertyValue \${hasErrors(bean: ${propertyName}, field: '${p.name}', 'errors')}">

              ${renderEditor(p)}
            </span>
          </div>
          <% }
          }
          } %>

        </div>
        <div class="myButton">
          <div class="buttons"><span class="button"><g:submitButton name="create" class="save" value="\${message(code: 'default.button.create.label', default: 'Create')}"/></span>
          </div>
        </div>
      </g:form>
    </div>
  </div>

</div>
</body>
</html>
