package vocbinbase

import grails.plugin.searchable.SearchableService


class RebuildIndexJob {

    /**
     * delays the start by 30 seconds
     * and repeats it once a day
     */
    static triggers = {
        simple name: 'synchronizeService', startDelay: 15000, repeatInterval: 3600000
    }

    def group = "vocbinbase"

    //only one job at a time...
    def concurrent = false

    SearchableService searchableService

    /**
     * execute the actual function
     * @return
     */
    def execute() {
        log.info "starting reindexing..."
        //searchableService.stopMirroring()
        //searchableService.index()
        //searchableService.startMirroring()
        log.info "finished reindexing..."
    }
}
