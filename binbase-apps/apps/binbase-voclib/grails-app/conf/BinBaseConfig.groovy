import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

/**
 * this file contains the binbase configuration for this plugin.
 *
 */
binbase {


    key = "${CH.config.external.binbase.key}"

    server = "${CH.config.external.binbase.server}"

    /**
     * the used binbase voc db for this instance
     */
    database = "volatile2"

    /**
     * the list of experiments we want to sync or the titles of it. We will try to find the id's of them in the metadata files. You can also specify true to sync all experiments or false to skip the synchornisation all together
     */
    syncExperiments = false

    /**
     * we only synchronize the metadata and nothing else
     */
    syncMetaDataOnly = false

    /**
     * where can we find our metadata
     */
    metaDataPath = "/mnt/storage/metadata"

    /**
     * fetches the metadata from the given url
     */
    metaDataUrl = "http://minix.fiehnlab.ucdavis.edu/communications/studieDataAsXML/"

    /**
     * strict means we need to have the metadata file or the experiment won't be syncd
     */
    strict = false

    /**
     * title of this instance
     */
    title = "The Volatile BinBase database"

    /**
     * the welcome title on the index.gsp page
     */
    welcomeTitle = "The Volatile BinBase"
    /**
     * the welcome text on the index.gsp page
     */
    welcomeText = """
                <p>
        <strong>Abstract:</strong>

    <p>
        <strong>Background:</strong> Volatile compounds comprise diverse chemical groups with wide-ranging sources and functions. These compounds originate from major pathways of secondary metabolism in many organisms and play essential roles in chemical ecology in both plant and animal kingdoms. In past decades, sampling methods and instrumentation for the analysis of complex volatile mixtures have improved; however, design and implementation of database tools to process and store the complex datasets have lagged behind.

    </p>

    <p>
        <strong>Description:</strong> The volatile compound BinBase (VOC BinBase) is an automated peak annotation and database system developed for the analysis of GC-TOF-MS data derived from complex volatile mixtures. VOC BinBase is an extension of the previously reported metabolite BinBase software developed to track and identify derivatized metabolites. The BinBase algorithm uses deconvoluted spectra and peak metadata (retention index, unique ion, spectral similarity, peak signal-to-noise ratio, and peak purity) from the Leco ChromaTOF software, and annotates peaks using a multi-tiered filtering system with stringent thresholds. VOC BinBase assigns the identity of compounds existing in the database, or catalogs and tracks those unique molecules fitting strict mass spectral and experimental criteria for which a database match is not found.. Volatile compound, or “Bin”, assignments are supported by the Adams mass spectral-retention index library, which contains over 2000 plant-derived volatile compounds. The VOC BinBase database is currently comprised of 1537 unique mass spectra generated from 3200 samples (18 species). These mass spectra and their retention indices are available as a free download in the MS search program format for use by volatile compound researchers.
    </p>

    <p>
        <strong>Conclusions:</strong> The BinBase database algorithms have been successfully modified to allow for tracking and identification of volatile compounds in complex mixtures. The database is capable of annotating large datasets (hundreds to thousands of samples) and is well-suited to cross-study comparisons (e.g. source, species, season, etc.). This novel volatile compound database tool is applicable to research fields spanning chemical ecology to human health. BinBase is freely available at <a
            href="http://binbase.sourceforge.net/">sourceforge</a>.</p>

    """

    /**
     * the msp file name
     */
    mspFileName = "The Volatile BinBase as MSP file"

    /**
     * is the msp file enabled
     */
    mspEnabled = true

    /**
     * pre cache bin similarities
     */
    similarityPreCaching = false

    /**
     * only synchronize bins which are found in the synchronized experiments
     */
    simplifyBins = true

    /**
     * are all data in this database public, if false, than we will use the minix service to find out if the data are public or not
     */
    allDataArePublic = true
}
