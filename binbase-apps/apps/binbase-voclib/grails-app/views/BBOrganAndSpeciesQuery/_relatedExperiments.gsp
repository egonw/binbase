<!-- renders the experiments for this species and organ selection -->
<g:setProvider library="jquery"/>


<g:if test="${BBSpeciesInstance == null}">
    <g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#experiments_for_organ_and_species').dataTable({
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxExperimentsForOrgansAsJSON", params: [organId: BBOrganInstance?.id], plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
aoColumnDefs: [{
             aTargets: [0]
          }] ,
          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],

		 "bAutoWidth" : false,
	 "aoColumns" : [
	 	{ sWidth : "20%" },
	 	{ sWidth : "20%" },
	 	{ sWidth : "60%" }
 			 ],


		"bFilter": false
		,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#experiments_for_organ_and_species_info").prepend("<input type='button' id='export_data_experiment' value='' class='xls_export'/>");
                }
		            });

		          $('#export_data_experiment').live('click', function() {
                var search = $('#experiments_for_organ_and_species_filter input').val();
                window.location = '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxExperimentsForOrgansAsJSON", params: [organId: BBOrganInstance?.id], plugin: "binbase-web-gui")}'+'&format=Excel&sSearch=' + search
          } );

        });
    });
    </g:javascript>
    <table id="experiments_for_organ_and_species" class="hover_table">
        <thead>
        <tr>

            <th>id</th>
            <th>name</th>
            <th>title</th>
        </tr>
        </thead>

        <!-- will contain the actual content -->
        <tbody>
        </tbody>
    </table>

</g:if>

<g:elseif test="${BBOrganInstance == null}">
    <g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#experiments_for_organ_and_species').dataTable({
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxExperimentsForSpeciesAsJSON", params: [speciesId: BBSpeciesInstance?.id], plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
aoColumnDefs: [{
             aTargets: [0]
          }],
		 "bAutoWidth" : false,
	 "aoColumns" : [
	 	{ sWidth : "20%" },
	 	{ sWidth : "20%" },
	 	{ sWidth : "60%" }
 			 ],


          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": false
		,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#experiments_for_organ_and_species_info").prepend("<input type='button' id='export_data_experiment' value='' class='xls_export'/>");
                }
            });

                $('#export_data_experiment').live('click', function() {
                var search = $('#experiments_for_organ_and_species_filter input').val();
                window.location = '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxExperimentsForSpeciesAsJSON", params: [speciesId: BBSpeciesInstance?.id], plugin: "binbase-web-gui")}'+'&format=Excel&sSearch=' + search
          } );
        });
    });
    </g:javascript>
    <table id="experiments_for_organ_and_species" class="hover_table">
        <thead>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>title</th>
        </tr>
        </thead>

        <!-- will contain the actual content -->
        <tbody>
        </tbody>
    </table>

</g:elseif>
<g:else>
    <g:javascript>
             $(function() {

        $(document).ready(function() {

            $('#experiments_for_organ_and_species').dataTable({
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxExperimentsForSpeciesAndOrganAsJSON", params: [organId: BBOrganInstance?.id, speciesId: BBSpeciesInstance?.id], plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
		 "bAutoWidth" : false,
	 "aoColumns" : [
	 	{ sWidth : "20%" },
	 	{ sWidth : "20%" },
	 	{ sWidth : "60%" }
 			 ],

          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": false
		,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#experiments_for_organ_and_species_info").prepend("<input type='button' id='export_data_experiment' value='' class='xls_export'/>");
                }
            });

          $('#export_data_experiment').live('click', function() {
                var search = $('#experiments_for_organ_and_species_filter input').val();
                window.location = '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxExperimentsForSpeciesAndOrganAsJSON", params: [organId: BBOrganInstance?.id, speciesId: BBSpeciesInstance?.id], plugin: "binbase-web-gui")}'+'&format=Excel&sSearch=' + search
          } );
        });
    });
    </g:javascript>
    <table id="experiments_for_organ_and_species" class="hover_table">
        <thead>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>title</th>
        </tr>
        </thead>

        <!-- will contain the actual content -->
        <tbody>
        </tbody>
    </table>

</g:else>

<!-- used to load the related bin on the click on a row -->
<g:javascript>
    $(function() {

        $(document).ready(function() {

            $('#experiments_for_organ_and_species tbody tr').live('click', function() {
                var nTds = $('td', this);
                var textId = $(nTds[0]).text();

                window.location = '${createLink(controller: "BBExperiment", action: "show")}'+'/'+textId;
            });


        });
    });
</g:javascript>
