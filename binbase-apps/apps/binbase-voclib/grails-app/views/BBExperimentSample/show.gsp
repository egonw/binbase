<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="article">

    <h2 class="star">Sample Details</h2>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>

    <g:if test="${BBExperimentSampleInstance.experimentClass.experiment.publicExperiment}">

        <div class="help-message ui-state-highlight ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="sample.details"/>
            </p>
        </div>

        <div class="dialog">
            <table>
                <tbody>

                <tr class="prop">
                    <td valign="top" class="name">
                        <g:message code="BBExperimentSample.label.label" default="Label"/>
                    </td>

                    <td valign="top" class="value">${fieldValue(bean: BBExperimentSampleInstance, field: "label")}</td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name">
                        <g:message code="BBExperimentSample.comment.label"
                                   default="Comment"/>
                    </td>

                    <td valign="top"
                        class="value">${fieldValue(bean: BBExperimentSampleInstance, field: "comment")}</td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name">
                        <g:message code="BBExperimentSample.experimentClass.label"
                                   default="Experiment Class"/>
                    </td>

                    <td valign="top" class="value">
                        <g:link controller="BBExperimentClass" action="show"
                                id="${BBExperimentSampleInstance?.experimentClass?.id}">
                            ${BBExperimentSampleInstance?.experimentClass?.name}
                        </g:link>
                    </td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Experiment</td>

                    <td valign="top" class="value">
                        <g:link controller="BBExperiment" action="show"
                                id="${BBExperimentSampleInstance?.experimentClass?.experiment?.id}">
                            ${BBExperimentSampleInstance?.experimentClass?.experiment?.name}
                        </g:link>
                    </td>

                </tr>


                <tr class="prop">
                    <td valign="top" class="name">
                        <g:message code="BBExperimentSample.fileName.label"
                                   default="File Name"/>
                    </td>

                    <td valign="top"
                        class="value">${fieldValue(bean: BBExperimentSampleInstance, field: "fileName")}</td>

                </tr>
                </tbody>
            </table>
        </div>

    </g:if>
    <g:else>

        <div class="warning-message ui-state-error ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                <g:message code="public.noaccess"/>
            </p>
        </div>
    </g:else>
</div>
<g:if test="${BBExperimentSampleInstance.experimentClass.experiment.publicExperiment}">

    <div class="article">
        <h2 class="star">Annotated Spectra</h2>

        <div class="help-message ui-state-highlight ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="sample.annotated.spectra"/>
            </p>
        </div>

        <div class="height200px">

            <binbase:tableSpectraForSample sampleId="${BBExperimentSampleInstance.id}"/>

            <div class="dialog">
                <table>
                    <tbody>

                    <tr class="prop">
                        <td valign="top" class="name">count</td>

                        <td valign="top" class="value">${BBExperimentSampleInstance.spectra.size()}</td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</g:if>

</body>
</html>
