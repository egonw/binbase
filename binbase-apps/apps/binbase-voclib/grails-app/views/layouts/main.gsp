<html>
<head>
    <title>${grailsApplication.config.binbase.title}</title>
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon"/>
    <g:layoutHead/>

    <!-- required stylesheets -->
    <link href="${resource(dir: 'css', file: 'normalize.css')}" rel="stylesheet" type="text/css"/>
    <link href="${resource(dir: 'css', file: 'jquery.treeTable.css', plugin: "binbase-web-gui")}" rel="stylesheet"
          type="text/css"/>
    <link href="${resource(dir: 'css', file: 'main.css')}" rel="stylesheet" type="text/css"/>
    <link href="${resource(dir: 'css', file: 'binbase.css')}" rel="stylesheet" type="text/css"/>
    <link href="${resource(dir: 'css/biz', file: 'style.css')}" rel="stylesheet" type="text/css"/>
    <link href="${resource(dir: 'css', file: 'tipTip.css')}" rel="stylesheet" type="text/css"/>


    <g:javascript library="application"/>
    <g:javascript library="jquery" plugin="jquery"/>
    <g:javascript src="jquery.treeTable.js" plugin="binbase-web-gui"/>
    <g:javascript src="jquery.cookie.js" plugin="binbase-web-gui"/>
    <g:javascript src="jquery.foldHelp.js" plugin="binbase-web-gui"/>
    <g:javascript src="jquery.tipTip.js" plugin="binbase-web-gui"/>




    <!-- jquery ui compoments -->
    <jqui:resources themeCss="${resource(dir:'jquery-ui/themes/lightGray', file:'jquery-ui-1.8.16.custom.css')}"/>

    <!-- bluff resources, if we don't have these graphs are not rendered -->
    <bluff:resources/>

    <!-- flot library -->
    <flot:resources plugins="['pie']" includeJQueryLib="false"/>

    <!-- jquery data tables -->
    <jqDT:resources jqueryUi="true"/>

    <g:javascript src="jquery.dataTables.refresh.js" plugin="binbase-web-gui"/>

    <!-- export plugin -->
    <export:resource/>


    <!-- we don't want our pages to be cached... -->
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="-1">
</head>

<body>
<div id="spinner" class="spinner" style="display:none;">
    <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
</div>

<div class="main">

    <div class="header">
        <div class="header_resize">

            <div class="logo">
                <g:if test="${grailsApplication.config.binbase.logoName}">
                    <h1><span>${grailsApplication.config.binbase.logoName}</span> <small>${grailsApplication.config.binbase.logoDescription}</small>
                    </h1>
                </g:if>
            </div>

            <div class="searchform">
                <g:form id="formsearch" name="formsearch" method="post" controller="search" action="search">
                    <span>
                        <g:textField name="query" class="editbox_search" id="query" maxlength="80"/>
                    </span>
                    <g:submitButton name="button_search" class="button_search" value=""/>
                </g:form>
            </div>

            <div class="clr"></div>

        </div>
    </div>


    <div class="content">
        <div class="content_resize">

            <g:render template="/shared/sidebar"/>

            <div class="mainbar">
                <g:layoutBody/>
            </div>


            <div class="clr"></div>
        </div>
    </div>

</div>

<div class="fbg">
    <div class="fbg_resize">

        <div class="clr"></div>
    </div>
</div>

<div class="footer">
    <div class="footer_resize">
        <p class="lf">&copy; Copyright: Fiehnlab</p>

        <div style="clear:both;"></div>
    </div>
</div>
</div>
</body>
</html>