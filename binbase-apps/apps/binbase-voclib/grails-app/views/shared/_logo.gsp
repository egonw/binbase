<style type="text/css">

.binbase_logo {
    float: left;
    width: 800px;
    height: 95px;
    margin: 10px 0 0 0;
    padding: 0 0 0 0;
}

.binbase_logo .logo_image {
    width: 260px;
    background: #ffffff;
    height: 100%;
    margin: 0 0 12px;
    float: left;

}

.binbase_logo .logo_image img {
    width: 200px;
    padding: 0px 15px 0px 15px;
    float: left;
}

.binbase_logo .logo_text {
    color: #f6f5f5;
    font-size: 22px;
    text-indent: 3px;
    padding: 0 0 0 0;
    margin: 35px 0 0 0;
    width: 400px;
    float: left;

}

</style>

<div class="binbase_logo">

    <!--
    <div class="logo_image">
      <!--  <img src="${resource(dir: "images", file: "binbaseLogo.jpg")}" alt="the binbase database"/>
      The volatile BinBase database

    </div>
        -->
    <div class="logo_text">
        The volatile BinBase database
    </div>
</div>