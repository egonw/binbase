<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <link href="${resource(dir: 'css', file: 'people.css')}" rel="stylesheet" type="text/css"/>

</head>

<body>

<div class="article">

    <h2 class="star">Project members</h2>

    <p>
        The Volatile Binbase was developed by the following team members of the UC Davis metabolimics research lab.
    </p>
    <div class="person">
        <div class="picture">
            <img src="${resource(dir: 'images/people', file: 'Fiehn.jpg')}" alt=""/>
        </div>

        <div class="content">
            <a target="_blank" href="http://fiehnlab.ucdavis.edu/staff/fiehn/">Dr. Oliver Fiehn</a>
            <p>position: Project Lead</p>
            <p>contact: <a href="mailto:ofiehn@ucdavis.edu">ofiehn@ucdavis.edu</a></p>

        </div>
    </div>

    <div class="person">
        <div class="picture">
            <img src="${resource(dir: 'images/people', file: 'gert.jpg')}" alt=""/>

        </div>

        <div class="content">
            <a target="_blank" href="http://fiehnlab.ucdavis.edu/staff/wohlgemuth/">Gert Wohlgemuth</a>
            <p>position: Lead Software Developer</p>
            <p>contact: <a href="mailto:wohlgemuth@ucdavis.edu">wohlgemuth@ucdavis.edu</a></p>

        </div>
    </div>


    <div class="person">
        <div class="picture">
            <img src="${resource(dir: 'images/people', file: 'kirsten.jpeg')}" alt=""/>
        </div>

        <div class="content">
            <a target="_blank" href="http://fiehnlab.ucdavis.edu/staff/skogerson">Kirsten Skogerson</a>
            <p>position: PHD Student, data aquisition and analysis</p>
            <p>contact: <a href="mailto:skogerson@ucdavis.edu">skogerson@ucdavis.edu</a></p>
        </div>
    </div>


    <div class="person">
        <div class="picture">
            <img src="${resource(dir: 'images/people', file: 'dinesh.jpg')}" alt=""/>
        </div>

        <div class="content">
            <a target="_blank" href="http://fiehnlab.ucdavis.edu/staff/kumar">Dinesh Kumar Barupal</a>
            <p>position: PHD Student, data analysis</p>
            <p>contact: <a href="mailto:barupal@gmail.com">barupal@gmail.com</a></p>
        </div>
    </div>

</div>

</body>
</html>