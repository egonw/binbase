grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.war.file = "target/${appName}.war"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        grailsPlugins()
        grailsHome()

        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        mavenLocal()
        mavenCentral()
    }
    dependencies {

        runtime 'postgresql:postgresql:8.2-504.jdbc3'

        plugins {
        
		compile('edu.ucdavis.genomics.metabolomics.binbase.minix.plugins:binbase-web-core:latest.integration')
                compile('edu.ucdavis.genomics.metabolomics.binbase.minix.plugins:binbase-web-gui:latest.integration')
                compile('edu.ucdavis.genomics.metabolomics.binbase.minix.plugins:jboss-client:latest.integration')

                runtime(':bluff:0.1.1')
                runtime(':ckeditor:3.6.0.0')
                runtime(':executor:0.3')
                runtime(':hibernate:1.3.7')
                runtime(':jetty:1.2-SNAPSHOT')
                runtime(':jquery:1.7.1')
                runtime(':jquery-datatables:1.7.5')
                runtime(':jquery-ui:1.8.15')
                runtime(':quartz:0.4.2')
                runtime(':remote-pagination:0.3')
                runtime(':searchable:0.6.3')
        }
    }
}
