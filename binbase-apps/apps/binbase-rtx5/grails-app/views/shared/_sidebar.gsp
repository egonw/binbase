<!-- the sidebar for this application -->

<div class="sidebar">
    <div class="gadget">
        <h2 class="star"><span>Options</span></h2>

        <div class="clr"></div>
        <ul class="sb_menu">
            <li><g:link controller="BBBin">Bins</g:link></li>
            <li><g:link controller="BBExperiment">Experiments</g:link></li>
            <li><g:link controller="BBSpecies">Species</g:link></li>
            <li><g:link controller="BBOrgan">Organs</g:link></li>
            <li><g:link controller="contentStatistics" action="showStatistics">Statistics</g:link></li>
            <li><g:link controller="BBDownload" action="list">Downloads</g:link></li>

        </ul>
    </div>

    <div class="gadget">
        <h2 class="star"><span>Related Content</span></h2>

        <div class="clr"></div>
        <ul class="sb_menu">
            <g:render template="/shared/relatedContent"/>
        </ul>
    </div>
</div>