<g:if test="${c.experiment.publicExperiment}">

    <table>
        <thead>
        <tr>

            <th class="ui-state-default">
                comment
            </th>
            <th class="ui-state-default">
                label
            </th>

            <th class="ui-state-default">
                file name
            </th>

        </tr>
        </thead>
        <tbody>

        <g:each in="${c.samples}" var="sample">
            <tr>

                <td>${sample.comment}</td>
                <td>${sample.label}</td>
                <td><g:link controller="BBExperimentSample" action="show"
                            id="${sample.id}">${sample.fileName}</g:link></td>

            </tr>
        </g:each>
        </tbody>
    </table>
</g:if>

<g:else>

    <div class="warning-message ui-state-error ui-corner-all"
         style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
            <g:message code="public.noaccess"/>
        </p>
    </div>
</g:else>
