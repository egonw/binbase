
<div class="dialog">
    <table>
        <tbody>

        <tr class="prop">
            <td valign="top" class="name">Name</td>

            <td valign="top" class="value"><g:link controller="BBExperimentClass" action="show" id="${c.id}">${c.name}</g:link></td>

        </tr>
        <tr class="prop">
            <td valign="top" class="name">Species</td>

            <td valign="top" class="value"><g:link controller="BBSpecies" action="show" id="${c.species.id}">${c.species.name}</g:link></td>

        </tr>

        <tr class="prop">
            <td valign="top" class="name">Organ</td>

            <td valign="top" class="value"><g:link controller="BBOrgan" action="show" id="${c.organ.id}" params="[speciesId:c.species.id]"> ${c.organ.name}</g:link></td>

        </tr>

        </tbody>
    </table>
</div>
<div class="horizontal-spacer"></div>
<div class=""></div>
