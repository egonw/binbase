<%@ page import="grails.converters.JSON" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<g:javascript>

 jQuery(document).ready(function() {

        $('#bin-count').load(
            '${createLink(controller: "contentStatistics", action: "ajaxRenderCountByType", params: [object: 'BBBin'], plugin: 'binbase-web-core')}'
            );

        $('#spectra-count').load(
            '${createLink(controller: "contentStatistics", action: "ajaxRenderCountByType", params: [object: 'BBSpectra'], plugin: 'binbase-web-core')}'
            );
        $('#sample-count').load(
            '${createLink(controller: "contentStatistics", action: "ajaxRenderCountByType", params: [object: 'BBExperimentSample'], plugin: 'binbase-web-core')}'
            );
        $('#class-count').load(
            '${createLink(controller: "contentStatistics", action: "ajaxRenderCountByType", params: [object: 'BBExperimentClass'], plugin: 'binbase-web-core')}'
            );
        $('#experiment-count').load(
            '${createLink(controller: "contentStatistics", action: "ajaxRenderCountByType", params: [object: 'BBExperiment'], plugin: 'binbase-web-core')}'
            );
        $('#species-count').load(
            '${createLink(controller: "contentStatistics", action: "ajaxRenderCountByType", params: [object: 'BBSpecies'], plugin: 'binbase-web-core')}'
            );

        $('#organ-count').load(
            '${createLink(controller: "contentStatistics", action: "ajaxRenderCountByType", params: [object: 'BBOrgan'], plugin: 'binbase-web-core')}'
            );
});

</g:javascript>

<div class="article">

    <div class="left">
        <h2 class="star">Content Statistics</h2>

        <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
        </g:if>

        <div class="dialog">
            <table>
                <tbody>

                <tr class="prop">
                    <td valign="top" class="name">Bins</td>

                    <td valign="top" class="value"><div id="bin-count">fetching data...</div></td>

                    <td valign="top" class="name"><g:link controller="contentStatistics"
                                                          action="showTimeBasedStatistics"
                                                          params="[object:'BBBin']">count over time</g:link></td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Annotated Spectra</td>

                    <td valign="top" class="value"><div id="spectra-count">fetching data...</div></td>

                    <td valign="top" class="name"><g:link controller="contentStatistics"
                                                          action="showTimeBasedStatistics"
                                                          params="[object:'BBSpectra']">count over time</g:link></td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Samples</td>

                    <td valign="top" class="value"><div id="sample-count">fetching data...</div></td>

                    <td valign="top" class="name"><g:link controller="contentStatistics"
                                                          action="showTimeBasedStatistics"
                                                          params="[object:'BBExperimentSample']">count over time</g:link></td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Classes</td>

                    <td valign="top" class="value"><div id="class-count">fetching data...</div></td>

                    <td valign="top" class="name"><g:link controller="contentStatistics"
                                                          action="showTimeBasedStatistics"
                                                          params="[object:'BBExperimentClass']">count over time</g:link></td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Experiments</td>

                    <td valign="top" class="value"><div id="experiment-count">fetching data...</div></td>

                    <td valign="top" class="name"><g:link controller="contentStatistics"
                                                          action="showTimeBasedStatistics"
                                                          params="[object:'BBExperiment']">count over time</g:link></td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Species</td>

                    <td valign="top" class="value"><div id="species-count">fetching data...</div></td>

                    <td valign="top" class="name"><g:link controller="contentStatistics"
                                                          action="showTimeBasedStatistics"
                                                          params="[object:'BBSpecies']">count over time</g:link></td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Organs</td>

                    <td valign="top" class="value"><div id="organ-count">fetching data...</div></td>
                    <td valign="top" class="name"><g:link controller="contentStatistics"
                                                          action="showTimeBasedStatistics"
                                                          params="[object:'BBorgan']">count over time</g:link></td>

                </tr>

                </tbody>
            </table>
        </div>

    </div>

    <div class="right">

        <h2 class="star">Sample Distribution by Species</h2>

        <binbase:pieSampleBySpeciesDistribution/>

    </div>


    <div class="right">
        <h2 class="star">Experiment Distribution by Species</h2>

        <binbase:pieExperimentByOrganDistribution/>
    </div>
</div>

<div class="clr"></div>

</body>
</html>
