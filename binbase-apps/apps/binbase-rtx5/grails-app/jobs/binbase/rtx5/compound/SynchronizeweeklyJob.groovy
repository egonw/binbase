package binbase.rtx5.compound

import binbase.web.gui.SynchronizeJob

/**
 * synchronizes the databes every week
 */
class SynchronizeweeklyJob extends SynchronizeJob {

    static triggers = {
        //fire every friday at 10:15 and synchronize all the databases
        cron name: 'synchronize-weekly', cronExpression: "0 15 10 ? * 6"
    }

    def concurrent = false

}
