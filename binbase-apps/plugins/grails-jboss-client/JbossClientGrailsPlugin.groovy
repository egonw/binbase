class JbossClientGrailsPlugin {
    // the plugin version
def version = "5.1.0"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "1.3.5 > *"
    // the other plugins this plugin depends on
    def dependsOn = [:]
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views/error.gsp"
    ]

    //we won't package this plugin in the jboss enviorment
    def environments = ['dev', 'test','prod']

    def author = "Gert Wohlgemuth"
    def authorEmail = "berlinguyinca@gmail.com"
    def title = "JBoss Client Libraries"
    def description = '''\\
This plugin provides you with all the libraries you need to connect to a JBoss application server
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/jboss-client"

    def doWithWebDescriptor = { xml ->
    }

    def doWithSpring = {
    }

    def doWithDynamicMethods = { ctx ->
    }

    def doWithApplicationContext = { applicationContext ->
    }

    def onChange = { event ->
    }

    def onConfigChange = { event ->
    }
}
