<%@ page import="binbase.web.core.BBExperimentSample" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

<g:setProvider library="jquery"/>

<bluff:resources/>
</head>

<body>
<div class="article">

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>

    <div class="left">
        <h2 class="star">Species Browser</h2>

        <div class="help-message ui-state-highlight ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="species.list"/>
            </p>
        </div>

        <div class="list">

            <div>
                <table class="hover_table" id="species">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

        </div>

    </div>

    <div class="right">

        <h2 class="star">Sample Distribution</h2>


        <div class="help-message ui-state-highlight ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="species.graph.sampleDistribution"/>
            </p>
        </div>

        <binbase:pieSampleBySpeciesDistribution/>

    </div>

    <div class="clr"></div>
</div>



<g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#species').dataTable({
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxListSpeciesAsJSON", plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
"bAutoWidth" : false,
"bPaginate": true,
		"bInfo": true,
		"bLengthChange": false,
	 "aoColumns" : [
	 	{ sWidth : "25%" },
	 	{ sWidth : "75%" }
 			 ],


          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": false ,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#species_info").prepend("<input type='button' id='export_species' value='' class='xls_export'/>");
                }
            });
                    $('#export_species').live('click', function() {
                var search = $('#species_filter input').val();
                window.location = '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxListSpeciesAsJSON", plugin: "binbase-web-gui", params: [format: "Excel"])}'+'&sSearch=' + search
          } );
        });
    });
</g:javascript>

<!-- used to load the related bin on the click on a row -->
<g:javascript>
    $(function() {

        $(document).ready(function() {

            $('#species tbody tr').live('click', function() {
                var nTds = $('td', this);
                var textId = $(nTds[0]).text();

                window.location = '${createLink(controller: "BBSpecies", action: "show")}'+'/'+textId;
            });


        });
    });
</g:javascript>
</body>

</body>
</html>
