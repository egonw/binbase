<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>
<div class="article">

    <h2 class="star">Bin Browser</h2>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>

    <!-- description of this element -->
    <div class="help-message ui-state-highlight ui-corner-all" style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
            <g:message code="generic.list" args="${['bin\'s']}"/>
        </p>
    </div>

    <div class="list">
        <div class="dataTables_wrapper">
            <table class="hover_table" id="bins">
                <thead>
                <tr>
                    <th>Bin Id</th>
                    <th>Name</th>
                    <th>Quant Mass</th>
                    <th>Retention Index</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <div class="clr"></div>
</div>

<g:javascript>
    $(function() {


        $(document).ready(function() {

            $( "#dialog:ui-dialog" ).dialog( "destroy" );

		    $( "#bin-list-wait-dialog" ).dialog({
	    		height: 140,
		    	modal: true,
                autoOpen: false
	    	});

            //create table
            var oTable = $('#bins').dataTable({
                bProcessing: true,
                bServerSide: true,
                sAjaxSource: '${createLink(controller: "BBBinQuery", action: "ajaxListBinAsJSON", plugin: "binbase-web-gui")}' ,
                bJQueryUI: true,
                "bAutoWidth" : false,
	            "aoColumns" : [
	 	            { sWidth : "15%" },
	 	            { sWidth : "35%" },
	 	            { sWidth : "15%" },
	 	            { sWidth : "35%" }
 	            ],


                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		        "bFilter": true ,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#bins_info").prepend("<input type='button' id='export_data_bin' value='' class='xls_export'/>");
                }
          });

          $('#export_data_bin').live('click', function() {
                var search = $('#bins_filter input').val();
                window.location = '${createLink(controller: "BBBinQuery", action: "ajaxListBinAsJSON", plugin: "binbase-web-gui")}'+'?format=Excel&sSearch=' + search
          } );

        });
    });
</g:javascript>

<!-- used to load the related bin on the click on a row -->
<g:javascript>
    $(function() {

        $(document).ready(function() {

            $('#bins tbody tr').live('click', function() {
                var nTds = $('td', this);
                var textId = $(nTds[0]).text();

                window.location = '${createLink(controller: "BBBinQuery", action: "showBinByBinBaseBinId", plugin: "binbase-web-gui")}'+'/'+textId;
            });


        });
    });
</g:javascript>

</body>
</html>




