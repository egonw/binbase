<%@ page import="edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.ValidateSpectra" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="article">

    <h2 class="star">Bin Details</h2>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>

<!-- description of this element -->
    <div class="help-message ui-state-highlight ui-corner-all"
         style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
            <g:message code="bin.details"/>
        </p>
    </div>

    <g:javascript>
        jQuery(document).ready(function() {
            $('#binArcodion').accordion();
        });

    </g:javascript>

    <div id="binArcodion">

        <h2><a href="#">General Properties</a></h2>

        <div class="height320px dialog">
            <table>
                <tbody>

                <tr class="prop">
                    <td valign="top" class="name">
                        <g:message code="BBBin.binbaseBinId.label"
                                   default="Binbase Bin Id"/>
                    </td>

                    <td valign="top" class="value">
                        <g:formatNumber number="${BBBinInstance.binbaseBinId}"
                                        format="########0"/>
                    </td>

                    <td rowspan="5">
                        <div id="${BBBinInstance.id}_massspec_bin">
                            <binbase:renderBin height="220" widh="400" id="${BBBinInstance.id}"/>
                        </div>

                        <div class="help-message ui-state-highlight ui-corner-all"
                             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                                <g:message code="bin.massspec"/>
                            </p>
                        </div>
                    </td>
                </tr>

                <tr class="prop">
                    <td valign="top" class="name"><g:message code="BBBin.name.label" default="Name"/></td>

                    <td valign="top" class="value">${fieldValue(bean: BBBinInstance, field: "name")}</td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name"><g:message code="BBBin.quantMass.label" default="Quant Mass"/></td>

                    <td valign="top" class="value">${fieldValue(bean: BBBinInstance, field: "quantMass")}</td>

                </tr>


                <tr class="prop">
                    <td valign="top" class="name"><g:message code="BBBin.uniqueMass.label" default="Unique Mass"/></td>

                    <td valign="top" class="value">${fieldValue(bean: BBBinInstance, field: "uniqueMass")}</td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name"><g:message code="BBBin.retentionIndex.label"
                                                             default="Retention Index"/></td>

                    <td valign="top" class="value"><g:formatNumber number="${BBBinInstance.retentionIndex}"
                                                                   format="########0"/></td>

                </tr>

                </tbody>
            </table>
        </div>

        <h2><a href="#">Apex Masses</a></h2>

        <div class="height320px">

            <div class="help-message ui-state-highlight ui-corner-all"
                 style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <g:message code="bin.apex.masses"/>
                </p>
            </div>

            <%
                def spectra = ValidateSpectra.convert(BBBinInstance.spectraString)
                def apex = "${BBBinInstance.apexMasses.toString()}"
            %>
            <div class="left">
                <table>
                    <thead>
                    <th class="ui-state-default">Mass</th>
                    <th class="ui-state-default">Relative Intensity</th>
                    </thead>
                    <tbody>

                    <g:each in="${apex.split('\\+').collect {Integer.parseInt(it)}.sort()}">
                        <tr>
                            <td>${it}</td>
                            <td><g:formatNumber
                                    number="${spectra[it-1][ValidateSpectra.FRAGMENT_REL_POSITION]}"
                                    format="########0.00"/></td>

                        </tr>
                    </g:each>
                    </tbody>

                </table>
            </div>

            <div class="right">
                <div id="${BBBinInstance.id}_massspec_bin">
                    <binbase:renderBin height="220" widh="400" id="${BBBinInstance.id}" apexOnly="true"/>
                </div>

                <div class="help-message ui-state-highlight ui-corner-all"
                     style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        <g:message code="bin.apex.masses.graph"/>
                    </p>
                </div>

            </div>
        </div>

        <h2><a href="#">Downloads</a></h2>

        <div class="height320px">
            <!-- description of this element -->
            <div class="help-message ui-state-highlight ui-corner-all"
                 style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <g:message code="bin.downloads"/>
                </p>
            </div>

            <table>
                <tbody>

                <tr class="prop">
                    <td valign="top" class="name">Msp File</td>

                    <td valign="top" class="value"><g:link controller="BBBin" action="downloadMspFile"
                                                           id="${BBBinInstance.id}">download</g:link></td>
                </tr>

                </tbody>
            </table>
        </div>

        <h2><a href="#">Known Species and Organs</a></h2>

        <div class="height320px">

            <div class="help-message ui-state-highlight ui-corner-all"
                 style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <g:message code="bin.species.and.organs"/>
                </p>
            </div>
            <binbaseWebGui:speciesTableForBin id="${BBBinInstance.id}" relative="false"/>
        </div>

        <h2><a href="#">Similar Bins</a></h2>

        <div class="height320px">

            <div class="help-message ui-state-highlight ui-corner-all"
                 style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <g:message code="bin.similar.bins"/>
                </p>
            </div>
            <binbase:similarBinsTable binId="${BBBinInstance.id}"/>
        </div>

    </div>

    <div class="clr"></div>
</div>

</body>
</html>
