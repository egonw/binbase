<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: 10/21/11
  Time: 12:50 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Simple GSP page</title>
    <meta name="layout" content="main"/>
</head>

<body>

<div class="article">

    <table>
        <h2 class="star">Statistics for object ${object.toString().replaceFirst("BB", "")}</h2>

        <div class="help-message ui-state-highlight ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="statistic.details"/>
            </p>
        </div>

        <tr>
            <td>
                <h2>By Day - grouped by hours</h2>
            </td>
        </tr>
        <tr>
            <td>
                <binbase:lineChartObjectsForDayByHour object="${object}"/>
            </td>
        </tr>

        <tr>
            <td>
                <h2>By Week - grouped by days</h2>
            </td>
        </tr>
        <tr>

            <td>
                <binbase:lineChartObjectsForWeekByDay object="${object}"/>
            </td>
        </tr>

        <tr>
            <td>
                <h2>By Month - grouped by week</h2>
            </td>
        </tr>
        <tr>
            <td>
                <binbase:lineChartObjectsForMonthByWeek object="${object}"/>
            </td>
        </tr>

        <tr>
            <td>
                <h2>By Year - grouped by month</h2>
            </td>
        </tr>
        <tr>
            <td>
                <binbase:lineChartObjectsForYearByMonth object="${object}"/>
            </td>
        </tr>

    </table>
</div>

</body>
</html>