<%@ page import="edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.ValidateSpectra" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="article">

<h2 class="star">Spectra Details</h2>
<g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
</g:if>

<div class="help-message ui-state-highlight ui-corner-all"
     style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
        <g:message code="spectra.details"/>
    </p>
</div>


<g:if test="${BBSpectraInstance.sample.experimentClass.experiment.publicExperiment}">
<div class="dialog">
    <div class="left">

        <table>
            <tbody>
            <tr class="prop">
                <td valign="top" class="name">
                    <g:message code="BBSpectra.basePeak.label" default="Base Peak"/>
                </td>

                <td valign="top" class="value">${fieldValue(bean: BBSpectraInstance, field: "basePeak")}</td>

            </tr>


            <tr class="prop">
                <td valign="top" class="name">
                    <g:message code="BBSpectra.intensity.label" default="Intensity"/>
                </td>

                <td valign="top" class="value">
                    <g:formatNumber format="########0"
                                    number="${BBSpectraInstance.intensity}"/>
                </td>

            </tr>

            <tr class="prop">
                <td valign="top" class="name">
                    <g:message code="BBSpectra.retentionIndex.label"
                               default="Retention Index"/>
                </td>

                <td valign="top" class="value">
                    <g:formatNumber format="########0"
                                    number="${BBSpectraInstance.retentionIndex}"/>
                </td>

            </tr>

            <tr class="prop">
                <td valign="top" class="name">
                    <g:message code="BBSpectra.retentionTime.label"
                               default="Retention Time"/>
                </td>

                <td valign="top" class="value">
                    <g:formatNumber format="########0.00"
                                    number="${BBSpectraInstance.retentionTime}"/>
                    s
                </td>

            </tr>


            <tr class="prop">
                <td valign="top" class="name">
                    <g:message code="BBSpectra.similarity.label" default="Similarity"/>
                </td>

                <td valign="top" class="value">
                    <g:formatNumber format="########0"
                                    number="${BBSpectraInstance.similarity}"/>
                </td>

            </tr>

            <tr class="prop">
                <td valign="top" class="name">
                    <g:message code="BBSpectra.sn.label" default="Sn"/>
                </td>

                <td valign="top" class="value">
                    <g:formatNumber format="########0"
                                    number="${BBSpectraInstance.sn}"/>
                </td>

            </tr>

            <tr class="prop">
                <td valign="top" class="name">
                    <g:message code="BBSpectra.uniqueMass.label" default="Unique Mass"/>
                </td>

                <td valign="top" class="value">${fieldValue(bean: BBSpectraInstance, field: "uniqueMass")}</td>
            </tr>

            <tr class="prop">
                <td valign="top" class="name">
                    <g:message code="BBSpectra.bin.label" default="Bin"/>
                </td>

                <td valign="top" class="value" colspan="2">
                    <g:link controller="BBBin" action="show"
                            id="${BBSpectraInstance?.bin?.id}">${BBSpectraInstance?.bin?.name}
                    </g:link>
                </td>

            </tr>


            <tr class="prop">
                <td valign="top" class="name">
                    <g:message code="BBSpectra.sample.label" default="Sample"/>
                </td>

                <td valign="top" class="value" colspan="2">
                    <g:link controller="BBExperimentSample" action="show"
                            id="${BBSpectraInstance?.sample?.id}">${BBSpectraInstance?.sample?.fileName}
                    </g:link>
                </td>

            </tr>
            </tbody>
        </table>

    </div>

    <div class="right">
        <binbase:renderSpectra id="${BBSpectraInstance.id}" height="220" widh="400"/>

        <div class="help-message ui-state-highlight ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="spectra.massspec"/>
            </p>
        </div>
    </div>

</div>

<div class="clr"></div>


<h2 class="star">Apex Masses</h2>

<div class="dialog">

    <div class="help-message ui-state-highlight ui-corner-all"
         style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
            <g:message code="bin.apex.masses"/>
        </p>
    </div>

    <%
        def spectra = ValidateSpectra.convert(BBSpectraInstance.spectraString)
        def apex = "${BBSpectraInstance.apexMasses.toString()}"
    %>
    <div class="left">
        <table>
            <thead>
            <th class="ui-state-default">Mass</th>
            <th class="ui-state-default">Relative Intensity</th>
            </thead>
            <tbody>

            <g:each in="${apex.split('\\+').collect {Integer.parseInt(it)}.sort()}">
                <tr>
                    <td>${it}</td>
                    <td><g:formatNumber
                            number="${spectra[it-1][ValidateSpectra.FRAGMENT_REL_POSITION]}"
                            format="########0.00"/></td>

                </tr>
            </g:each>
            </tbody>

        </table>
    </div>

    <div class="right">
        <p>Apex Masses Spectra</p>

        <div id="${BBSpectraInstance.id}_massspec_spectra">
            <binbase:renderSpectra height="220" widh="400" id="${BBSpectraInstance.id}" apexOnly="true"/>
        </div>

        <div class="help-message ui-state-highlight ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="spectra.apex.masses.graph"/>
            </p>
        </div>

        <p>Apex Masses Bin</p>

        <div id="${BBSpectraInstance.bin.id}_massspec_spectra">
            <binbase:renderBin height="220" widh="400" id="${BBSpectraInstance.bin.id}" apexOnly="true"/>
        </div>

        <div class="help-message ui-state-highlight ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="bin.apex.masses.graph"/>
            </p>
        </div>

    </div>
</div>
</g:if>

<g:else>

    <div class="warning-message ui-state-error ui-corner-all"
         style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
            <g:message code="public.noaccess"/>
        </p>
    </div>
</g:else>

</div>
</body>
</html>
