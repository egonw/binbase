<%@ page import="binbase.web.core.BBExperimentSample" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'BBExperimentSample.label', default: 'BBExperimentSample')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="article">
    <h2 class="star">Sample Browser</h2>


    <div class="help-message ui-state-highlight ui-corner-all"
         style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
            <g:message code="samples.list"/>
        </p>
    </div>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="list">
        <table id="samples" class="hover_table">
            <thead>
            <tr>
                <th>id</th>
                <th>label</th>
                <th>comment</th>
                <th>file name</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>


<g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#samples').dataTable({
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBExperimentSampleQuery", action: "ajaxListSamplesAsJSON", plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
"bAutoWidth" : false,
	 "aoColumns" : [
	 	{ sWidth : "25%" },
	 	{ sWidth : "25%" },
	 	{ sWidth : "25%" },
	 	{ sWidth : "25%" }
 			 ],


          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": false
            });
        });
    });
</g:javascript>

<!-- used to load the related bin on the click on a row -->
<g:javascript>
    $(function() {

        $(document).ready(function() {

            $('#samples tbody tr').live('click', function() {
                var nTds = $('td', this);
                var textId = $(nTds[0]).text();

                window.location = '${createLink(controller: "BBExperimentSample", action: "show")}'+'/'+textId;
            });


        });
    });
</g:javascript>

</body>
</html>
