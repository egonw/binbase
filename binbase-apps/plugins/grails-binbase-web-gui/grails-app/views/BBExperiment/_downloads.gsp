<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>
<div class="article">
    <h2 class="star">Results for ${exp.name}</h2>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>


    <g:if test="${exp.publicExperiment}">
        <binbase:tableResultsForExperiment experimentId="${exp.id}"/>
    </g:if>
    <g:else>
        <div class="warning-message ui-state-error ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                <g:message code="public.noaccess"/>
            </p>
        </div>
    </g:else>
</div>
</body>
</html>
