<!-- renders the bins for this species and organ selection -->
<g:setProvider library="jquery"/>

<g:javascript>

        /**
        *renders a object to 2 digits
        */
    function renderToFixed(obj) {

        var num = obj.aData[obj.iDataColumn]

        return num.toFixed(2) + '%';

    }

</g:javascript>

<g:if test="${BBSpeciesInstance == null}">
    <g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#bins_for_organ_and_species').dataTable({
            "aaSorting": [[ 0, "desc" ]],
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxBinsByOrganAsJSON", params: [organId: BBOrganInstance?.id], plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
aoColumnDefs: [{
             fnRender: renderToFixed,
             aTargets: [0]
          }] ,
"bAutoWidth" : false,
	 "aoColumns" : [
	 	{ sWidth : "10%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "15%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "15%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "20%" }
 			 ],


          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": true,
"fnDrawCallback": function(){
      $('td').bind('mouseenter', function () { $(this).parent().children().each(function(){$(this).addClass('hihglightRow');}); });
      $('td').bind('mouseleave', function () { $(this).parent().children().each(function(){$(this).removeClass('hihglightRow');}); });
}
    ,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#bins_for_organ_and_species_info").prepend("<input type='button' id='export_bins_for_organ_and_species' value='' class='xls_export'/>");
                }
            });

        $('#export_bins_for_organ_and_species').live('click', function() {
                var search = $('#bins_for_organ_and_species_filter input').val();
                window.location = '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxBinsByOrganAsJSON", plugin: "binbase-web-gui", params: [organId: BBOrganInstance?.id,format:"Excel"])}'+'&sSearch=' + search
          } );
        });
    });
    </g:javascript>
    <table id="bins_for_organ_and_species" class="hover_table">
        <thead>
        <tr>
            <th>rel Found</th>
            <th>abs Found</th>
            <th>sample count</th>
            <th>name</th>
            <th>bin id</th>
            <th>retention index</th>
            <th>unique mass</th>
            <th>organ</th>
        </tr>
        </thead>

        <!-- will contain the actual content -->
        <tbody>
        </tbody>
    </table>

</g:if>

<g:elseif test="${BBOrganInstance == null}">
    <g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#bins_for_organ_and_species').dataTable({
            "aaSorting": [[ 0, "desc" ]],
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxBinsBySpeciesAsJSON", params: [speciesId: BBSpeciesInstance?.id], plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,

           "bAutoWidth" : false,
	 "aoColumns" : [
	 	{ sWidth : "10%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "15%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "15%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "20%" }
 			 ],


aoColumnDefs: [{
             fnRender: renderToFixed,
             aTargets: [0]
          }],
          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": true
,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#bins_for_organ_and_species_info").prepend("<input type='button' id='export_bins_for_organ_and_species' value='' class='xls_export'/>");
                }
            });

        $('#export_bins_for_organ_and_species').live('click', function() {
                var search = $('#bins_for_organ_and_species_filter input').val();
                window.location = '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxBinsBySpeciesAsJSON", plugin: "binbase-web-gui", params: [speciesId: BBSpeciesInstance?.id,format:"Excel"])}'+'&sSearch=' + search
          } );
        });
    });
    </g:javascript>
    <table id="bins_for_organ_and_species" class="hover_table">
        <thead>
        <tr>
            <th>rel Found</th>
            <th>abs Found</th>
            <th>sample count</th>
            <th>name</th>
            <th>bin id</th>
            <th>retention index</th>
            <th>unique mass</th>
            <th>species</th>
        </tr>
        </thead>

        <!-- will contain the actual content -->
        <tbody>
        </tbody>
    </table>

</g:elseif>
<g:else>
    <g:javascript>
             $(function() {

        $(document).ready(function() {

            $('#bins_for_organ_and_species').dataTable({
            "aaSorting": [[ 0, "desc" ]],
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxBinsByOrganAndSpeciesAsJSON", params: [organId: BBOrganInstance?.id, speciesId: BBSpeciesInstance?.id], plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
          aoColumnDefs: [{
             fnRender: renderToFixed,
             aTargets: [0]
          }]   ,
"bAutoWidth" : false,
	 "aoColumns" : [
	 	{ sWidth : "10%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "15%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "15%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "10%" },
	 	{ sWidth : "20%" }
 			 ],

          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": true   ,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#bins_for_organ_and_species_info").prepend("<input type='button' id='export_bins_for_organ_and_species' value='' class='xls_export'/>");
                }
            });

        $('#export_bins_for_organ_and_species').live('click', function() {
                var search = $('#bins_for_organ_and_species_filter input').val();
                window.location = '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxBinsByOrganAndSpeciesAsJSON", plugin: "binbase-web-gui", params: [organId: BBOrganInstance?.id, speciesId: BBSpeciesInstance?.id,format:"Excel"])}'+'&sSearch=' + search
          } );
        });

    });
    </g:javascript>
    <table id="bins_for_organ_and_species" class="hover_table">
        <thead>
        <tr>
            <th>rel Found</th>
            <th>abs Found</th>
            <th>sample count</th>
            <th>name</th>
            <th>bin id</th>
            <th>retention index</th>
            <th>unique mass</th>
            <th>organ</th>
            <th>species</th>

        </tr>
        </thead>

        <!-- will contain the actual content -->
        <tbody>
        </tbody>
    </table>

</g:else>

<!-- used to load the related bin on the click on a row -->
<g:javascript>
    $(function() {

        $(document).ready(function() {

            $('#bins_for_organ_and_species tbody tr').live('click', function() {
                var nTds = $('td', this);
                var textId = $(nTds[4]).text();

                window.location = '${createLink(controller: "BBBinQuery", action: "showBinByBinBaseBinId")}'+'/'+textId;
            });


        });
    });
</g:javascript>
