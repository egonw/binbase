<!-- renders the bins for this species and organ selection -->
<g:setProvider library="jquery"/>


<g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#species_for_organ').dataTable({
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxSpeciesByOrganAsJSON", params: [organId: BBOrganInstance?.id], plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
"bAutoWidth" : false,
"bPaginate": false,
		"bInfo": true,
		"bLengthChange": false,
	 "aoColumns" : [
	 	{ sWidth : "20%" },
	 	{ sWidth : "80%" }
 			 ],


          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": false  ,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#species_for_organ_info").prepend("<input type='button' id='export_species_for_organ' value='' class='xls_export'/>");
                }
            });

                $('#export_species_for_organ').live('click', function() {
                var search = $('#species_for_organ_filter input').val();
                window.location = '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxSpeciesByOrganAsJSON", plugin: "binbase-web-gui", params: [organId: BBOrganInstance?.id, format: "Excel"])}'+'&sSearch=' + search
          } );

        });
    });
</g:javascript>
<table id="species_for_organ" class="hover_table">
    <thead>
    <tr>
        <th>id</th>
        <th>species name</th>
    </tr>
    </thead>

    <!-- will contain the actual content -->
    <tbody>
    </tbody>
</table>

<!-- used to load the related bin on the click on a row -->
<g:javascript>
    $(function() {

        $(document).ready(function() {

            $('#species_for_organ tbody tr').live('click', function() {
                var nTds = $('td', this);
                var textId = $(nTds[0]).text();

                window.location = '${createLink(controller: "BBSpecies", action: "show")}'+'/'+textId + "?organId=${BBOrganInstance?.id}";
            });


        });
    });
</g:javascript>
