package binbase.web.gui

import grails.converters.JSON
/**
 * simple controller for webservices to hook into the system
 * to enable synchroniation of an experiment
 */
class BBSynchronizeController {

    def grailsApplication

    /**
     * pushes the synchronization of an experiment
     */
    def synchronizeExperiment = {

        if (params.id == null) {
            render(["sorry you need to provide the id as argument"] as JSON)
        } else {
            long id = Long.parseLong(params.id)
            String database = grailsApplication.config.binbase.database.toString()

            try {
                SynchronizeExperimentJob.triggerNow([experimentId: id, database: database])

                render(["started synchronization of experiment ${id} on database: ${database}"] as JSON)
            } catch (Exception e) {
                render(["an error happened during the triggering of the job: ${e.getMessage()}"] as JSON)
                log.error(e.getMessage(),e)
            }
        }


    }
}
