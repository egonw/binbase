package binbase.web.gui

import core.StatisticsService


class CollectStatisticsJob {

    static triggers = {
        //fire all 60 minutes
        cron name: "fireEveryHourToCollectStatistic", cronExpression: "0 0 * * * ?"
    }
    StatisticsService statisticsService

    def concurrent = false

    def execute() {
        statisticsService.createSnapShot()
    }
}
