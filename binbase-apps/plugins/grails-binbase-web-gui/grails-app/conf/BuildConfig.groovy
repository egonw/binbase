grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.war.file = "target/${appName}.war"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        mavenLocal()
        grailsPlugins()
        grailsHome()
        grailsCentral()

    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

        // runtime 'mysql:mysql-connector-java:5.1.5'
    }

    plugins {
        compile('edu.ucdavis.genomics.metabolomics.binbase.minix.plugins:binbase-web-core:latest.integration')
        runtime(':flot:0.2.3')
        runtime(':jquery:1.7.1')
        runtime(':jquery-ui:1.8.15')
        runtime(':jquery-datatables:1.7.5')
        runtime(':export:1.0')
	runtime(':quartz:0.4.2')
	runtime(':hibernate:1.3.7')
	runtime(':searchable:0.6.3')
	runtime(':bluff:0.1.1')
	runtime(':executor:0.3')
    }
}
