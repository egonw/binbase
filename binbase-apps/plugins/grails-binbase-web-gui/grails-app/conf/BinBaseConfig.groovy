import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

/**
 * this file contains the binbase configuration for this plugin.
 *
 */
binbase {


    key = "${CH.config.external.binbase.key}"

    server = "${CH.config.external.binbase.server}"

    /**
     * the used binbase voc db for this instance
     */
    database = "${CH.config.external.binbase.database}"

    /**
     * the list of experiments we want to sync or the titles of it. We will try to find the id's of them in the metadata files. You can also specify true to sync all experiments or false to skip the synchornisation all together
     */
    syncExperiments = false

    /**
     * we only synchronize the metadata and nothing else
     */
    syncMetaDataOnly = false

    /**
     * where can we find our metadata
     */
    metaDataPath = "/mnt/storage/metadata"

    /**
     * fetches the metadata from the given url
     */
    metaDataUrl = "http://${CH.config.external.binbase.minix}/communications/studieDataAsXML"

    /**
     * strict means we need to have the metadata file or the experiment won't be syncd
     */
    strict = false

    /**
     * title of this instance
     */
    title = "The BinBase database"

    /**
     * the welcome title on the index.gsp page
     */
    welcomeTitle = "The BinBase"
    /**
     * the welcome text on the index.gsp page
     */
    welcomeText = """
                <p>
        <strong>Welcome:</strong>

      <p>
      welcome to the  binbase compound browser
      </p>
    </p>


    """

    /**
     * the msp file name
     */
    mspFileName = "The BinBase as MSP file"

    /**
     * is the msp file enabled
     */
    mspEnabled = true

    /**
     * pre cache bin similarities
     */
    similarityPreCaching = true

    /**
     * only synchronize bins which are found in the synchronized experiments
     */
    simplifyBins = true

    /**
     * are all data in this database public, if false, than we will use the minix service to find out if the data are public or not
     */
    allDataArePublic = "${CH.config.external.binbase.public}"
}
