package binbase.web.gui

/**
 * simplify the use of the ajax based views
 */
class BinBaseGuiTagLib {

    static namespace = 'binbaseWebGui'

    /**
     * displays all the species as a table for the given bin id
     */
    def speciesTableForBin = {  attrs, body ->

           def id = "species_table_for_bin_${System.currentTimeMillis()}"
        out << """


                <div id="${id}">
                <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                </div>
                        <script type="text/javascript">

            jQuery(document).ready(function() {

            \$('#${id}').load('${createLink(controller: "BBBinQuery", action: "ajaxSpeciesForBin", plugin: 'binbase-web-gui',id:attrs.id)}');
                       });

                        </script>

                """
    }


}
