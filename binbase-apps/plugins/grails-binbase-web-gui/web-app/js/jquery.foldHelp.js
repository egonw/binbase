/**
 * simple function which allows us to hide all help dialogs with the click of a button
 * and using jquery-ui based styling
 * this will set a cookie to remember if the help should be shown or not
 *
 * the html element for this should look like this
 *
 *
       <div class="help-message ui-state-highlight ui-corner-all" style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
            your help is here!
        </p>
    </div>
 */
 jQuery(document).ready(function() {

        //add our help button
        $('.help-message').each(
            function(index){
                //append relative styling
                $(this).css('position','relative');
                $(this).css('padding-right','15px');
                $(this).css('padding-top','18px').append('<span class="help-button ui-icon" style="position:absolute;top:2;right:3;width:15px;"></span>');
            }
        );

        //check cookie state
        if($.cookie('binbase-web-gui-show-help') == null){
            $.cookie('binbase-web-gui-show-help',true,{ expires: 365, path: '/' })
        }
        //show help
        else if($.cookie('binbase-web-gui-show-help') == 'true'){

            $('.help-message p').each(
                function(index){
                    $('.help-button',$(this).parent()).removeClass('ui-icon-plusthick').addClass('ui-icon-minusthick');
                    $(this).show();
                }
            );
        }
        //hide help
        else{
            $('.help-message p').each(
                function(index){
                    $('.help-button',$(this).parent()).removeClass('ui-icon-minusthick').addClass('ui-icon-plusthick');
                    $(this).hide();
                }
            );
        }

        //register the event for the help button
        $('.help-message .help-button').each(function(index){

                //add an element to fire the state

                //register click element to fold...
                $(this).click(function () {

                    if($.cookie('binbase-web-gui-show-help') == 'true'){
                        $.cookie('binbase-web-gui-show-help',false,{ expires: 365, path: '/' })
                    }
                    else{
                        $.cookie('binbase-web-gui-show-help',true,{ expires: 365, path: '/' })
                    }

                    //toggle the state of all the help objects
                    $('.help-message p').each(function(index2){
                        if($.cookie('binbase-web-gui-show-help') == 'true'){
                        //switch the cookie and hide/show
                            $('.help-button',$(this).parent()).removeClass('ui-icon-plusthick').addClass('ui-icon-minusthick');
                            $(this).show('slow');
                        }
                        else{
                            $('.help-button',$(this).parent()).removeClass('ui-icon-minusthick').addClass('ui-icon-plusthick');
                            $(this).hide('slow');
                        }
                    })
                })
        });
    });
