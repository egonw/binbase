import org.springframework.ejb.access.SimpleRemoteStatelessSessionProxyFactoryBean

import binbase.core.BinBaseConfigReader
import grails.util.Environment
import org.springframework.context.ApplicationContext
import org.codehaus.groovy.grails.commons.GrailsApplication
import grails.util.GrailsUtil
import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinbaseCategoryDataset

class BinbaseWebCoreGrailsPlugin {

    // the plugin version
def version = "5.1.0"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "1.3.7 > *"
    // the other plugins this plugin depends on

    def dependsOn = [

            jquery: '1.6.1.1 > *',
            jqueryUi: '1.8.11 > *',
            executor: '0.3 > *',
            searchable: '0.6.1 > *',
            bluff: '0.1.1 > *'

    ]

    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views/error.gsp",
            "grails-app/conf/BinBaseConfig.groovy"
    ]

    def author = "Gert Wohlgemuth"
    def authorEmail = "berlinguyinca@gmail.com"
    def title = "Core Plugin for BinBase integration"
    def description = '''\\

        this plugin provides you with the possibility to access the BinBase database system and to synchronize the data to a local database. Last but not least it allows you to schedule imports and exports to binbase over grails services.

'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/binbase-web-core"

    def doWithWebDescriptor = { xml ->
    }

    def doWithSpring = {

/** *
 * we do not need mocking in this plugin...
 */
        if (Environment.current == Environment.TEST) {
            println "warning disabling mocking context!!! to make remote ejb's work"
            if (org.springframework.mock.jndi.SimpleNamingContextBuilder.currentContextBuilder) {
                org.springframework.mock.jndi.SimpleNamingContextBuilder.currentContextBuilder.deactivate()
            }
        }

        /**
         * merge the existing binbase configuration
         */

        if (getConfiguration(parentCtx, application)) {

            BinBaseConfigReader.initialize()

            println "server: ${BinBaseConfigReader.getServer()}"
            println "key: ${BinBaseConfigReader.getKey()}"

            /**
             * configure the jndi environment
             */
            jndiBinBaseTemplate(org.springframework.jndi.JndiTemplate) {

                environment = [
                        "java.naming.factory.initial": "org.jnp.interfaces.NamingContextFactory",
                        "java.naming.factory.url.pkgs": "org.jboss.naming:org.jnp.interfaces",
                        "java.naming.provider.url": "${BinBaseConfigReader.getServer()}:1099".toString()
                ]
            }

            /**
             * connection to the cluster configuration
             */
            clusterConfigService(SimpleRemoteStatelessSessionProxyFactoryBean) { bean ->
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.ClusterConfigService"
                jndiName = "clusterservice/ClusterConfigServiceBean/remote"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * access to the report service
             */
            reportService(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.ClusterReportConfigService"
                jndiName = "clusterservice/ClusterReportConfigServiceBean/remote"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * access to the notification service
             */
            notifierService(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.NotifierService"
                jndiName = "clusterservice/NotifierServiceBean/remote"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * access to the authnetification service
             */
            authentificationService(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.AuthentificatorService"
                jndiName = "bci/AuthentificatorServiceBean/remote"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * access to the key store service
             */
            keyStoreService(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.KeyStoreService"
                jndiName = "bci/KeyStoreServiceBean/remote"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * scheduling service access
             */
            schedulingService(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler"
                jndiName = "bci/SchedulerBean/remote"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * scheduling service access
             */
            binbaseEjbService(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService"
                jndiName = "bci/BinBaseServiceBean/remote"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * access the database configuration
             */
            binbaseDatabaseAccess(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade"
                jndiName = "ejb/DatabaseJMXFacadeBean"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * binbase status access
             */
            binbaseStatus(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacade"
                jndiName = "ejb/StatusJMXFacadeBean"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * access to the general configuration
             */
            binbaseServiceConfiguration(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacade"
                jndiName = "ejb/ServiceJMXFacadeBean"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * access to the export configuration
             */
            binbaseExportConfiguration(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade"
                jndiName = "ejb/ExportJMXFacadeBean"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }

            /**
             * used to send emails
             */
            binbaseCommunicationService(SimpleRemoteStatelessSessionProxyFactoryBean) {
                businessInterface = "edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.CommunicationJMXFacade"
                jndiName = "ejb/CommunicationJMXFacadeBean"
                jndiTemplate = ref(jndiBinBaseTemplate)
            }
        }
        else{
            System.exit(-1)
        }
    }

    def doWithDynamicMethods = { ctx ->
    }

    def doWithApplicationContext = { applicationContext ->
    }

    def onChange = { event ->
    }

    def onConfigChange = { event ->
    }

    private def getConfiguration(ApplicationContext applicationContext, GrailsApplication application) {
        // merge into the main config.
        def config = application.config
        try {
            Class configClass = application.getClassLoader().loadClass("BinBaseConfig")
            ConfigSlurper configSlurper = new ConfigSlurper(GrailsUtil.getEnvironment())
            Map binding = new HashMap()
            binding.userHome = System.properties['user.home']
            binding.grailsEnv = application.metadata["grails.env"]
            binding.appName = application.metadata["app.name"]
            binding.appVersion = application.metadata["app.version"]
            configSlurper.binding = binding
            config.merge(configSlurper.parse(configClass))
            return true
        } catch (Throwable e) {
            println("please make sure you have a BinBaseConfig.groovy file in your configuration directory!")
            e.printStackTrace()
            return false
        }


    }
}
