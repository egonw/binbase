package binbase.core

import org.codehaus.groovy.grails.commons.ConfigurationHolder

/**
 * registers the default configuration value
 */
class IntializeConfigurationLocations {

    /**
     * does the actual intialization
     * @param configuration
     * @param appName
     * @return
     */

    static def initialize(def configuration, def appName)
    {
        def ENV_NAME = "${appName}-config"

        def userHome = System.getProperty("user.home")

        if (!configuration || !(configuration instanceof List)) {
            configuration = []
        }
        if (System.getenv(ENV_NAME)) {
            println "Including configuration file specified in environment: " + System.getenv(ENV_NAME);
            configuration << "file:" + System.getenv(ENV_NAME)
        } else if (System.getProperty(ENV_NAME)) {
            println "Including configuration file specified on command line: " + System.getProperty(ENV_NAME);
            configuration << "file:" + System.getProperty(ENV_NAME)
        } else {
            println "No external configuration file defined, assuming defaults\n"

            configuration = ["classpath:${appName}-config.properties",
                    "classpath:${appName}-config.groovy",
                    "file:${userHome}/.grails/${appName}-config.properties",
                    "file:${userHome}/.grails/${appName}-config.groovy",
                    "file:./${appName}-config.properties",
                    "file:./${appName}-config.groovy"

            ]

            println ""
        }

        return configuration
    }
}
