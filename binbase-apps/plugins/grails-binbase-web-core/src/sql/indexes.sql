/**
 * in this file all the indexes which are required are defiened, since grails is to retarted to do it for you...
 */

CREATE INDEX similarity_from_to_index
    ON public.bbbin_similarity(from_id, to_id)
;
CREATE INDEX bin_database_index
    ON public.bbbin(database_id)
;
create index experiement_sample_class_id on public.bbexperiment_sample(experiment_class_id)
;
create index experiement_class_experiemnt_id on public.bbexperiment_class(experiment_id)
;
create index spectra_sample_id on public.bbspectra(sample_id)

;