package core

import grails.test.*
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade
import binbase.web.core.BBExperiment
import binbase.web.core.BBDatabase
import binbase.web.core.BBExperimentClass
import binbase.web.core.BBExperimentSample
import binbase.web.core.BBMetadata

class MetaDataServiceTests extends GrailsUnitTestCase {

    DatabaseJMXFacade binbaseDatabaseAccess

    def executorService

    def sessionFactory


    String exampleContent = """

<experiment id="1217653" title="Profile of secondary metabolites produced by B. rhodina">
  <publications />
  <classes>
    <class id="1224150" organ="" species="Botryosphaeria rhodina">
      <metadatas />
      <samples>
        <sample id="1224129" fileName="100513bsesa196_1" label="sx14960" comment="">
          <metadatas />
        </sample>
        <sample id="1224133" fileName="100513bsesa197_1" label="sx14961" comment="">
          <metadatas />
        </sample>
        <sample id="1224137" fileName="100513bsesa198_1" label="sx14962" comment="">
          <metadatas />
        </sample>
        <sample id="1224141" fileName="100513bsesa193_1" label="sx14963" comment="">
          <metadatas />
        </sample>
        <sample id="1224145" fileName="100513bsesa194_1" label="sx14964" comment="">
          <metadatas />
        </sample>
        <sample id="1224149" fileName="100513bsesa195_1" label="sx14965" comment="">
          <metadatas />
        </sample>
      </samples>
    </class>
  </classes>
</experiment>




    """

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }


    void testGenerateDSL(){
        MetaDataService service = new MetaDataService()
        def result = service.generateDSLForMetada("test",1217653,new XmlSlurper().parseText(exampleContent))

        assert result != null
        assert result != false

        log.info result

    }
    /*
    void testImportMetaData1393724() {

        SynchronizeDatabaseService service = new SynchronizeDatabaseService()
        service.binbaseDatabaseAccess = binbaseDatabaseAccess
        service.executorService = executorService
        service.sessionFactory = sessionFactory

        MetaDataService metaDataService = new MetaDataService()
        metaDataService.synchronizeDatabaseService = service

        def result = metaDataService.importMetaData("rtx5", "1393724", """

<experiment id="1393724" title="Streptomyces fluoride treatment">
  <classes>
    <class id="1393749" organ="" species="Streptomyces cattleya">
      <metadatas>
        <metadata name="variation" value="plus fluoride " />
        <metadata name="growing place / media" value="glucose-yeast-maltose " />
        <metadata name="seeding date" value="10/8/10 " />
        <metadata name="transplanting date" value="10/8/10 " />
        <metadata name="night humidity" value="100 %" />
        <metadata name="day humidity" value="100 %" />
        <metadata name="night temp." value="20 &amp;#176C" />
        <metadata name="day temp." value="20 &amp;#176C" />
        <metadata name="light: daily period" value="15  h/day" />
        <metadata name="light: fluence" value="120 &amp;#181mol/m&amp;#178&amp;#183s" />
        <metadata name="light source" value="cool white light " />
        <metadata name="Detailed Location" value="Medium glucose-yeast-maltose pH 5&#xD;&#xA;&#xD;&#xA;Medium was inoculated with spores on day -2, back-diluted into fresh media to OD600 0.1 on day -1 and fluoride added on day 0.&#xD;&#xA;&#xD;&#xA;50 mL cultures were grown in 250-mL baffled shake flasks, 3-4 beads per flask, 30 degrees at 200 rpm&#xD;&#xA;2 mM sodium fluoride from 1M stock, or no additive on day 0.&#xD;&#xA;3.0 mL culture samples were removed on day 4 of growth, centrifuged at 10,000 rpm at room temperature for 10-20 minutes and the media decanted. Mycelial pellets were snap-frozen in liquid nitrogen and stored at -80 or on dry ice until extraction." />
      </metadatas>
      <samples>
        <sample id="1393728" fileName="110125abrsa40_1" label="sx7300" comment="d4 1 pos">
          <metadatas />
        </sample>
        <sample id="1393732" fileName="110125abrsa53_1" label="sx7301" comment="d4 2 pos">
          <metadatas />
        </sample>
        <sample id="1393736" fileName="110125abrsa37_1" label="sx7302" comment="d4 3 pos">
          <metadatas />
        </sample>
        <sample id="1393740" fileName="110125abrsa38_1" label="sx7303" comment="d4 4 pos">
          <metadatas />
        </sample>
        <sample id="1393744" fileName="110125abrsa46_1" label="sx7304" comment="d4 5 pos">
          <metadatas />
        </sample>
        <sample id="1393748" fileName="110125abrsa50_1" label="sx7305" comment="d4 6 pos">
          <metadatas />
        </sample>
      </samples>
    </class>
    <class id="1393774" organ="" species="Streptomyces cattleya">
      <metadatas>
        <metadata name="variation" value="control " />
        <metadata name="growing place / media" value="glucose-yeast-maltose " />
        <metadata name="seeding date" value="10/8/10 " />
        <metadata name="transplanting date" value="10/8/10 " />
        <metadata name="night humidity" value="100 %" />
        <metadata name="day humidity" value="100 %" />
        <metadata name="night temp." value="20 &amp;#176C" />
        <metadata name="day temp." value="20 &amp;#176C" />
        <metadata name="light: daily period" value="15  h/day" />
        <metadata name="light: fluence" value="120 &amp;#181mol/m&amp;#178&amp;#183s" />
        <metadata name="light source" value="cool white light " />
        <metadata name="Detailed Location" value="Medium glucose-yeast-maltose pH 5&#xD;&#xA;&#xD;&#xA;Medium was inoculated with spores on day -2, back-diluted into fresh media to OD600 0.1 on day -1 and fluoride added on day 0.&#xD;&#xA;&#xD;&#xA;50 mL cultures were grown in 250-mL baffled shake flasks, 3-4 beads per flask, 30 degrees at 200 rpm&#xD;&#xA;2 mM sodium fluoride from 1M stock, or no additive on day 0.&#xD;&#xA;3.0 mL culture samples were removed on day 4 of growth, centrifuged at 10,000 rpm at room temperature for 10-20 minutes and the media decanted. Mycelial pellets were snap-frozen in liquid nitrogen and stored at -80 or on dry ice until extraction." />
      </metadatas>
      <samples>
        <sample id="1393753" fileName="110125abrsa41_1" label="sx7306" comment="d4 1 neg">
          <metadatas />
        </sample>
        <sample id="1393757" fileName="110125abrsa49_1" label="sx7307" comment="d4 2 neg">
          <metadatas />
        </sample>
        <sample id="1393761" fileName="110125abrsa45_1" label="sx7308" comment="d4 3 neg">
          <metadatas />
        </sample>
        <sample id="1393765" fileName="110125abrsa44_1" label="sx7309" comment="d4 4 neg">
          <metadatas />
        </sample>
        <sample id="1393769" fileName="110125abrsa43_1" label="sx7310" comment="d4 5 neg">
          <metadatas />
        </sample>
        <sample id="1393773" fileName="110125abrsa59_1" label="sx7311" comment="d4 6 neg">
          <metadatas />
        </sample>
      </samples>
    </class>
  </classes>
  <users>
    <user id="6" name="fiehn" password="brueggemann" email="ofiehn@ucdavis.edu" superAdmin="true" />
    <user id="7" name="brooks" password="brooks" email="kdbrooks@ucdavis.edu" superAdmin="false" />
  </users>
</experiment>





        """, true)

        BBExperiment exp = BBExperiment.findByNameAndDatabase("1393724", BBDatabase.findByName("rtx5"))

        println "title: ${exp.title}"

        exp.classes.each { BBExperimentClass clazz ->
            clazz.samples.each { BBExperimentSample s ->
                println "name: ${s.fileName}"
                println "label: ${s.label}"
                println "comment: ${s.comment}"

            }

            println "\n"

            clazz.metadata.each {BBMetadata m->

                println "name: ${m.name}"
                println "value: ${m.value}"
            }
            println "\n"
        }

        assert result
    }

    void testImportMetaData1217653() {

        SynchronizeDatabaseService service = new SynchronizeDatabaseService()
        service.binbaseDatabaseAccess = binbaseDatabaseAccess
        service.executorService = executorService
        service.sessionFactory = sessionFactory

        MetaDataService metaDataService = new MetaDataService()
        metaDataService.synchronizeDatabaseService = service

        def result = metaDataService.importMetaData("rtx5", "1217653", exampleContent, true)

        BBExperiment exp = BBExperiment.findByNameAndDatabase("1217653", BBDatabase.findByName("rtx5"))

        println "title: ${exp.title}"

        exp.classes.each { BBExperimentClass clazz ->
            clazz.samples.each { BBExperimentSample s ->
                println "name: ${s.fileName}"
                println "label: ${s.label}"
                println "comment: ${s.comment}"

                println "\n"
            }

            println "\n"
        }

        assert result
    }
                   */
}
