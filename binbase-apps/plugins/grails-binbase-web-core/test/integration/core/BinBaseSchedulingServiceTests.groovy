package core

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler

/**
 * basically just checks if we can schedule without an exception
 */
class BinBaseSchedulingServiceTests extends GroovyTestCase {

  BinBaseSchedulingService binBaseSchedulingService

  Scheduler schedulingService

  Experiment experiment

  protected void setUp() {
    super.setUp()

    //services
    assertTrue(binBaseSchedulingService != null)

    binBaseSchedulingService.schedulingService = schedulingService

    //sample data
    ExperimentSample sample = new ExperimentSample()
    sample.id = "tada"
    sample.name = "4125ea02_2.cdf"

    ExperimentClass clazz = new ExperimentClass()

    clazz.samples = new ExperimentSample[1]
    clazz.samples[0] = sample
    clazz.id = 123213
    clazz.column = "test  "

    experiment = new Experiment()
    experiment.id = 23123213
    experiment.column = "test"
    experiment.classes = new ExperimentClass[1]

    experiment.classes[0] = clazz
  }

  protected void tearDown() {
    super.tearDown()
  }


  public void testScheduleImport() {
    binBaseSchedulingService.scheduleImport(experiment.classes[0])
  }

  public void testScheduleExport() {
    binBaseSchedulingService.scheduleExport(experiment)
  }


}

