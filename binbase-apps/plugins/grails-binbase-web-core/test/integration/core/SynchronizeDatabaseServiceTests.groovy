package core

import grails.test.*
import binbase.web.core.BBDatabase
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade
import binbase.web.core.BBExperiment
import binbase.web.core.BBExperimentClass
import binbase.web.core.BBExperimentSample
import binbase.web.core.BBSpectra
import binbase.web.core.BBBin

class SynchronizeDatabaseServiceTests extends GrailsUnitTestCase {

    DatabaseJMXFacade binbaseDatabaseAccess

    def executorService

    def sessionFactory

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    /**
     * trys to synchronize twice to ensure the delete and so works.
     */
    void testSycnchronizeReRunTest(){

        SynchronizeDatabaseService service = new SynchronizeDatabaseService()
        service.binbaseDatabaseAccess = binbaseDatabaseAccess
        service.executorService = executorService
        service.sessionFactory = sessionFactory

        service.synchronizeBins("rtx5", false)
        service.synchronizeExperiment("1217653", "rtx5", false)
        service.synchronizeExperiment("1217653", "rtx5", false)

        service.logInfos()

        assert BBDatabase.count() == 1
        assert BBBin.count() > 7000
        assert BBSpectra.count() == 1652
        assert BBExperimentSample.count() == 6
        assert BBExperimentClass.count() == 1
        assert BBExperiment.count() == 1
    }

    void testSynchronizationStatus(){

        SynchronizeDatabaseService service = new SynchronizeDatabaseService()
        service.binbaseDatabaseAccess = binbaseDatabaseAccess
        service.executorService = executorService
        service.sessionFactory = sessionFactory

        service.synchronizeBins("rtx5", false)
        service.synchronizeExperiment("1217653", "rtx5", false)

        def result = service.synchronizationStatus("rtx5")

        assert result."1217653" == true
        println "result: ${result}"
    }

    void testSynchronizeExperiment1NoneThreaded() {

        SynchronizeDatabaseService service = new SynchronizeDatabaseService()
        service.binbaseDatabaseAccess = binbaseDatabaseAccess
        service.executorService = executorService
        service.sessionFactory = sessionFactory

        service.synchronizeBins("rtx5", false)
        service.synchronizeExperiment("1217653", "rtx5", false)

        service.logInfos()

        assert BBDatabase.count() == 1
        assert BBBin.count() > 7000
        assert BBSpectra.count() == 1652
        assert BBExperimentSample.count() == 6
        assert BBExperimentClass.count() == 1
        assert BBExperiment.count() == 1
    }

    void testSynchronzieBinsNoneThreaded() {


        SynchronizeDatabaseService service = new SynchronizeDatabaseService()
        service.binbaseDatabaseAccess = binbaseDatabaseAccess
        service.executorService = executorService
        service.sessionFactory = sessionFactory
        service.synchronizeBins("rtx5", false)
        service.synchronizeBins("rtx5", false)

        service.logInfos()

        assert BBDatabase.count() == 1
        assert BBBin.count() < 9000
    }



    void testSynchronizeExperiment3() {

        SynchronizeDatabaseService service = new SynchronizeDatabaseService()
        service.binbaseDatabaseAccess = binbaseDatabaseAccess
        service.executorService = executorService
        service.sessionFactory = sessionFactory

        service.synchronizeBins("rtx5",false)
        service.synchronizeExperiment("1223283", "rtx5",false)

        service.logInfos()

    }


}
