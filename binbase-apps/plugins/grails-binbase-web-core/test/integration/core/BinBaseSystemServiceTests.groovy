package core

import grails.test.*
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService
/**
 * tests the binbase system service
 */
class BinBaseSystemServiceTests extends GrailsUnitTestCase {

  BinBaseSystemService binBaseSystemService

  BinBaseService binbaseEjbService

  protected void setUp() {
    super.setUp()
    binBaseSystemService.binbaseEjbService = binbaseEjbService
  }

  protected void tearDown() {
    super.tearDown()
  }


  public void testTxtFileExist() {

    assertTrue(binBaseSystemService.txtFileExist("6177fa47_1"))
    assertFalse(binBaseSystemService.txtFileExist("6177fa47_122"))


  }

  public void testCdfFileExist() {

    assertTrue(binBaseSystemService.txtFileExist("dasd"))
    assertFalse(binBaseSystemService.txtFileExist("dasd"))


  }

  public void testGetAvailableColumns() {

    assertTrue(binBaseSystemService.getAvailableColumns().size() > 0)
    assertTrue(binBaseSystemService.getAvailableColumns().contains("test"))

  }

}
