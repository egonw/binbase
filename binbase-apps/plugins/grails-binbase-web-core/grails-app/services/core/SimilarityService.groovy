package core

import binbase.web.core.BBBin
import binbase.web.core.BBDatabase
import edu.ucdavis.genomics.metabolomics.util.math.Similarity
import binbase.web.core.BBBinSimilarity

class SimilarityService {

    def propertyInstanceMap = org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP

    def sessionFactory

    static transactional = true

    /**
     * calculates the similarities for the given bin against all bins in the database
     * @param bin
     * @return
     */
    def calculateSimilaritiesFor(BBBin lib, double minSimilarity = 0, Collection<BBBin> bins = null) {

        if (bins == null) {
            bins = BBBin.findAllByDatabase(lib.database)
        }

        log.info("working on ${lib.name}/${lib.id}")
        Similarity similarity = new Similarity()
        similarity.setLibrarySpectra(lib.spectraString)

        bins.each {BBBin unk ->

            //calculate the similarity over all bins and store it in the database for quick queries
            if (!lib.equals(unk)) {

                similarity.setUnknownSpectra(unk.spectraString)
                double value = similarity.calculateSimimlarity()

                BBBinSimilarity sim = BBBinSimilarity.findByFromAndTo(lib, unk)

                if (sim == null) {

                    if (value > minSimilarity) {
                        sim = new BBBinSimilarity()
                        sim.from = lib
                        sim.to = unk
                        sim.similarity = value

                        sim.save()
                    }
                }
            }
        }

        def session = sessionFactory.currentSession
        session.flush()
        session.clear()
        propertyInstanceMap.get().clear()

        log.info("\t=> done")
    }

}
