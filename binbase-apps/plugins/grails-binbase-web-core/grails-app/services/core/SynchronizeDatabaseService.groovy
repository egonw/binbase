package core

import binbase.web.core.BBDatabase

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade

import binbase.web.core.BBExperiment
import binbase.web.core.BBBin
import groovy.sql.Sql
import binbase.web.core.BBExperimentClass
import binbase.web.core.BBExperimentSample
import binbase.web.core.BBSpectra

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.math.CalculateSpectraProperties

import java.util.concurrent.Callable
import binbase.web.core.BBOrgan
import binbase.web.core.BBKingdom
import binbase.web.core.BBSpecies
import binbase.web.core.BBBinSimilarity
import edu.ucdavis.genomics.metabolomics.util.math.Similarity
import binbase.web.core.BBResult
import org.apache.lucene.store.LockObtainFailedException
import org.omg.PortableInterceptor.NON_EXISTENT
import binbase.web.core.BBDownload
import binbase.web.core.BBContent

/**
 * synchronize all bin data of binbase with the local data
 */
class SynchronizeDatabaseService {

    def dataSource

    def executorService

    static transactional = true

    def sessionFactory

    def propertyInstanceMap = org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP

    static String NONE_TITLE = "none"

    /**
     * access to the binbase database config
     */
    DatabaseJMXFacade binbaseDatabaseAccess

    /**
     * creats a connection to the binbase database
     * @param databaseName
     * @return
     */
    private Sql createBinBaseConnection(String databaseName) {
        def sql = Sql.newInstance("jdbc:postgresql://${binbaseDatabaseAccess.databaseServer}/${binbaseDatabaseAccess.database}", databaseName,
                binbaseDatabaseAccess.databaseServerPassword, "org.postgresql.Driver")
        return sql
    }

    /**
     * just synchronizes the bins from the database into the system
     * @return
     */
    void synchronizeBins(BBDatabase database, boolean multithread = false) {
        log.info "synchronizing all bin data"

        def sql = createBinBaseConnection(database.name)

        storeBins(sql, database, multithread)

    }

    /**
     * synchronizes the bins
     * @param database
     */
    void synchronizeBins(String database, boolean multithread = false) {
        synchronizeBins(checkForDatabase(database), multithread)
    }

    /**
     * removes bins which are not annotated to any actual experiment in this database obviously this method should
     * only used when you only want to have a subset of the complete database exported
     * @param database
     */
    void removeUnusedBins(String database) {
        removeUnusedBins(checkForDatabase(database))
    }

    /**
     * removes bins which are not annotated to any actual experiment in this database
     * @param database
     */
    void removeUnusedBins(BBDatabase database) {
        def sql = new Sql(dataSource)

        sql.eachRow("select distinct id from bbbin where id not in (select distinct bin_id from bbspectra)") {

            BBBin bin = BBBin.get(it.id)


            BBBinSimilarity.findAllByFrom(bin).each {BBBinSimilarity sim ->
                sim.delete(flush: true)
            }
            BBBinSimilarity.findAllByTo(bin).each {BBBinSimilarity sim ->
                sim.delete(flush: true)
            }

            bin.delete(flush: true)
        }
    }

    /**
     * logs the statistics of the current database
     * @return
     */
    def logInfos() {
        log.info "experiment count: ${binbase.web.core.BBExperiment.count()}"
        log.info "classes count: ${binbase.web.core.BBExperimentClass.count()}"
        log.info "samples count: ${binbase.web.core.BBExperimentSample.count()}"
        log.info "spectra count: ${binbase.web.core.BBSpectra.count()}"
        log.info "bin count: ${binbase.web.core.BBBin.count()}"
        log.info "database count: ${binbase.web.core.BBDatabase.count()}"
        log.info "kingdom count: ${binbase.web.core.BBKingdom.count()}"
        log.info "species count: ${binbase.web.core.BBSpecies.count()}"
        log.info "organ count: ${binbase.web.core.BBOrgan.count()}"

    }

    /**
     * synchronizes a singe BBExperiment
     * @param setupxId
     * @param db
     * @return
     */
    void synchronizeExperiment(String setupxId, BBDatabase db, boolean multithread = false,boolean publicExperiment = false) {

        Sql sql = createBinBaseConnection(db.name)

        sql.eachRow("select  max(result_id) as result_id, setupx  from result a where a.setupx = ? group by setupx",[setupxId]) {
            storeExperiment(it, db, sql, multithread,publicExperiment)
        }

    }

    void synchronizeExperiment(String setupxId, String database, boolean multithread = false,boolean publicExperiment = false) {
        synchronizeExperiment(setupxId, checkForDatabase(database), multithread,publicExperiment)
    }

    /**
     * checks if the database exist and if not create.
     * @param database
     * @return
     */
    private BBDatabase checkForDatabase(String database) {
        BBDatabase db = BBDatabase.findByName(database)

        if (db == null) {
            db = new BBDatabase()
            db.name = database
            db.save(flush: true)
        }
        return db
    }
    /**
     * queries the bins and stores them in the internal database
     * @param session
     * @param database
     */
    private void storeBins(Sql session, BBDatabase database, boolean multithread = false) {

        log.info "execute query from the binbase bin sets..."

        def futures = []

        def binMap = [:]

        BBBin.findAllByDatabase(database).each { BBBin bin ->
            binMap.put(new Integer(bin.binbaseBinId), bin)
        }

        session.eachRow("select * from bin where sample_id in (select sample_id from samples where finished = ?)",["TRUE"]) {

            int id = it.bin_id
            BBBin bin = binMap.get(new Integer(id))

            if (bin == null) {

                bin = new BBBin()
                bin.database = database
                bin.binbaseBinId = it.bin_id
                bin.retentionIndex = it.retention_index
                bin.uniqueMass = it.uniquemass
                bin.name = it.name
                bin.spectraString = it.spectra
                bin.quantMass = it.quantmass
                bin.apexMasses = it.apex

                if (!bin.validate()) {
                    throw new Exception("bin validation error: ${bin.errors}")
                }


                if (multithread) {
                    futures.add(executorService.submit({
                        bin.save(flush: true)


                    } as Callable)
                    )
                }
                else {
                    bin.save(flush: true)
                }

            }
        }


        futures.reverse().each {def future ->
            future.get()

        }
        //generate organs
        generateOrgans()

        //calcualte similar bins

        //cleanup gorm
        cleanUpGorm()

        log.info "it's done..."
    }

    private def generateOrgans() {
        BBKingdom kingdom = BBKingdom.findByName("NONE")

        if (kingdom == null) {
            kingdom = new BBKingdom()
            kingdom.name = "NONE"

            assert kingdom.validate(), kingdom.errors

            kingdom.save(flush: true)
        }

        BBSpecies species = BBSpecies.findByName("NONE")

        if (species == null) {
            species = new BBSpecies()

            species.name = "NONE"
            species.kingdom = kingdom

            assert species.validate(), species.errors

            species.save(flush: true)
        }

        BBOrgan organ = BBOrgan.findByName("NONE")

        if (organ == null) {
            organ = new BBOrgan()
            organ.name = "NONE"
            species.addToOrgans(organ)

            assert organ.validate(), organ.errors

            organ.save(flush: true)
        }
    }

    private void storeExperiment(def result, BBDatabase db, Sql session, boolean multithread = false,boolean publicExperiment = false) {

        cleanUpGorm()
        BBExperiment exp = BBExperiment.findByName(result.setupx)

        if (exp != null) {
            BBResult.findAllByExperiment(exp).each { BBResult res ->
                BBContent.findAllByResult(res).each {
                    it.delete()
                }
            }

            exp.delete()
            cleanUpGorm()
        }


        exp = new BBExperiment()

        log.info "working on experiment: ${result.setupx}"
        exp.name = result.setupx
        exp.database = db
        exp.title = SynchronizeDatabaseService.NONE_TITLE

        exp.publicExperiment = publicExperiment


        log.info "\tfetches classes for ${exp.name}"

        def classes = []
        //retrieve the classes for this experiment
        session.eachRow("select distinct c.class as className from result_link b, samples c where b.sample_id = c.sample_id  and c.visible = 'TRUE' and b.result_id = ?",[result.result_id]) {
            classes.add(it.className)
        }

        def futures = []

        if (classes.size() > 0) {

            if (!exp.validate()) {
                throw new Exception("experiment validation error: ${exp.errors}")
            }
            exp.save(flush: true)

            def binMap = [:]

            BBBin.findAllByDatabase(db).each { BBBin bin ->
                binMap.put(new Integer(bin.binbaseBinId), bin)
            }
            classes.each {String name ->

                if (multithread) {
                    futures.add(executorService.submit({
                        addClass(exp, name, createBinBaseConnection(db.name), binMap)
                    } as Callable)
                    )
                }
                else {
                    addClass(exp, name, createBinBaseConnection(db.name), binMap)
                }

            }

            futures.reverse().each {def future ->
                future.get()
            }


            cleanUpGorm()

            log.info "saved experiment:  ${exp.name} "

        }
    }

    /**
     * possible to span as a new thred
     * @param exp
     * @param name
     * @param session
     * @return
     */
    private def addClass(BBExperiment exp, String name, Sql session, def binMap) {
        BBExperimentClass clazz = new BBExperimentClass()
        clazz.experiment = exp
        clazz.name = name

        clazz.organ = BBOrgan.findByName("NONE")
        clazz.species = BBSpecies.findByName("NONE")

        if (!clazz.validate()) {
            throw new Exception("class validation error: ${clazz.errors}")
        }

        clazz.save(flush: true)

        log.info "\t\tfetching samples for: ${clazz.name}"

        session.eachRow("select sample_name,sample_id from samples where class = ? and visible = ?",[name,"TRUE"]) { def s ->

            BBExperimentSample sample = new BBExperimentSample()
            sample.experimentClass = clazz
            sample.fileName = s.sample_name
            sample.comment = SynchronizeDatabaseService.NONE_TITLE
            sample.label = SynchronizeDatabaseService.NONE_TITLE

            if (!sample.validate()) {
                throw new Exception("sample validation error: ${sample.errors}")
            }
            sample.save(flush: true)

            log.info "\t\t\tfetching spectra for: ${sample.fileName}"
            session.eachRow("select * from spectra where sample_id = ? and bin_id is not null",[ s.sample_id]) {def spec ->

                CalculateSpectraProperties prop = new CalculateSpectraProperties(spec.spectra, spec.apex)

                BBSpectra spectra = new BBSpectra()
                BBBin bin = binMap.get(Integer.parseInt(((int) spec.bin_id).toString()))

                if (bin != null) {
                    spectra.bin = bin
                    spectra.uniqueMass = spec.uniquemass
                    spectra.similarity = spec.match
                    spectra.retentionIndex = spec.retention_index
                    spectra.retentionTime = spec.retention_time / 1000
                    spectra.sn = spec.signal_noise
                    spectra.spectraString = spec.spectra
                    spectra.apexMasses = spec.apex
                    spectra.leco = spec.leco

                    try {
                        spectra.intensity = prop.calculateHeight(bin.quantMass)
                    }
                    catch (Exception e) {
                        spectra.intensity = 0
                    }

                    spectra.basePeak = prop.calculateBasePeak()

                    spectra.sample = sample


                    if (!spectra.validate()) {
                        throw new Exception("spectra validation error: ${spectra.errors}")
                    }
                    spectra.save(flush: true)

                }
                else {
                    log.warn "bin with id: ${spec.bin_id} was null"
                }

            }


        }
        session.close()
    }

    synchronized def cleanUpGorm() {
        def session = sessionFactory.currentSession
        session.flush()
        session.clear()
        propertyInstanceMap.get().clear()
    }

    /**
     * calculates the synchronisation status of the given database
     * and returns a map with the experiments as key
     * @param dat
     base
     * @return
     */
    def synchronizationStatus(String database) {
        synchronizationStatus(checkForDatabase(database))
    }

    /**
     * returns a map containning the binbase experimetns id's a key and a true/false if these are already in the local
     * repository
     * @param database
     * @return
     */
    def synchronizationStatus(BBDatabase database) {

        def result = [:]

        Sql sql = createBinBaseConnection(database.name)

        sql.eachRow("select max(a.result_id) as result_id, setupx  from result a, result_link b, samples c where a.result_id = b.result_id and b.sample_id = c.sample_id and c.visible = ? group by setupx") {
            String name = it.setupx

            if (name != null) {
                if (BBExperiment.findByDatabaseAndName(database, name) == null) {
                    result."${name}" = false
                }
                else {
                    result."${name}" = true
                }
            }
        }

        return result
    }

    /**
     * loads the available experiments and the one which need to be updated, because there title is wrong
     * @param database
     * @return
     */
    def getAvailableVisibleExperiments(String database) {

        def result = new HashSet()

        Sql sql = createBinBaseConnection(database)

        //build list of experiments
        sql.eachRow("select setupx, a.result_id from result a, result_link b, samples c where a.result_id = b.result_id and b.sample_id = c.sample_id and c.visible = ? group by setupx,a.result_id order by a.result_id DESC",["TRUE"]) {
            String name = it.setupx

            BBExperiment exp = BBExperiment.findByDatabaseAndName(checkForDatabase(database), name)

            if (exp == null) {
                if (result.contains(name) == false) {
                    result.add(name)
                }
            }
            else {
                log.info "experimnt already synchronised..."
            }
        }

        log.info("result list fetched: ${database}:${result}")
        return result
    }

    /**
     * loads the available experiments and the one which need to be updated, because there title is wrong
     * @param database
     * @return
     */
    def getExperimentsWithoutMetaData(String database) {

        def result = new HashSet()

        def list = BBExperiment.findAllByDatabaseAndTitle(checkForDatabase(database), NONE_TITLE)

        list.each {BBExperiment exp ->
            result.add(exp.name)
        }

        return result
    }

    /**
     * retrieves all experiments, which require synchronisation
     * @param database
     * @return
     */
    def getExperimentsRequireSynchronisation(String database) {

        def noMeta = getExperimentsWithoutMetaData(database)
        def availableData = getAvailableVisibleExperiments(database)

        def result = new HashSet()

        result.addAll(noMeta)
        result.addAll(availableData)

        return result
    }
}
