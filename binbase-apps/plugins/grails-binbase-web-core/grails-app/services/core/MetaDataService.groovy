package core

import binbase.web.core.BBOrgan
import binbase.web.core.BBSpecies
import binbase.web.core.BBExperiment
import binbase.web.core.BBDatabase
import binbase.web.core.BBExperimentClass
import binbase.web.core.BBKingdom
import binbase.web.core.BBExperimentSample
import binbase.web.core.BBMetadata
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
//import edu.ucdavis.genomics.metabolomics.binbase.dsl.DSLGenerator
import binbase.core.BinBaseConfigReader
import edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromExperiment
import groovy.xml.MarkupBuilder
import groovy.util.slurpersupport.NodeChild

class MetaDataService {

    SynchronizeDatabaseService synchronizeDatabaseService

    edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler schedulingService

    static transactional = true

    /**
     * imports the metadata using the minix system, to simplify this system
     * @param database
     * @param setupxId
     * @param miniXUrl
     * @param sync
     * @return
     */
    def importMetaDataFromMiniXUrl(String database, String minixId, String miniXUrl = "http://127.0.0.1:8080/minix/communications/studieDataAsXML", boolean sync = false, boolean dataArePublic = false) {
        try {
            def metaData = "${miniXUrl}/${minixId}"
            log.info "importing data from ${metaData}"

            def text = new URL(metaData).getText()

            log.info "\n\n${text}\n\n"
            def xmlData = new XmlSlurper().parseText(text)

            if (xmlData.errorMessage != null && xmlData.errorMessage.text().toString().length() > 0) {
                log.info "metadata import error: ${xmlData.errorMessage.text()}"
                return false
            }
            else {
                log.info "received file was valid and is used"
                importMetaData(database, minixId, xmlData, sync, dataArePublic)
                return true
            }
        }
        catch (Exception e) {
            log.error(e.getMessage(), e)
            return false
        }

    }

    /**
     * imports the data from the specified database into the local system
     * @param database database to use
     * @param experimentName the experimetn name
     * @param xmlData the actual data
     * @return
     */
    def importMetaData(String database, String setupxId, def xmlData, boolean sync = false, boolean dataArePublic = false) {

        try {

            if (sync) {
                log.info "explicit synchronisation required"
                synchronizeDatabaseService.synchronizeBins(database, false)
                synchronizeDatabaseService.synchronizeExperiment(setupxId, database, false)
            }

            String id = xmlData.@id
            String title = xmlData.@title
            boolean publicData = dataArePublic

            log.info("id: ${id} title: ${title} public: ${publicData}")

            if (dataArePublic) {
                publicData = dataArePublic
                log.info "public data were defined globally: ${publicData}"
            }
            else {
                if (xmlData.@isPublic) {
                    publicData = Boolean.parseBoolean(xmlData.@isPublic.toString())
                    log.info "public data were defined in the experiment: ${publicData}"
                }
            }

            BBExperiment experiment = BBExperiment.findByDatabaseAndName(BBDatabase.findByName(database), setupxId)


            if (experiment == null) {
                log.info "sorry experiment was no found: ${setupxId} - ${database}"
                return false
            }

            experiment.publicExperiment = publicData
            experiment.title = title
            experiment.save(flush: true)

            def classes = [:]

            experiment.classes.each {BBExperimentClass clazz ->
                classes."${clazz.name.trim()}" = clazz
            }

            xmlData.classes."class".each {def clazz ->

                def clazzId = clazz.@id.text().toString().trim()
                def organ = clazz.@organ.text().toString().trim()
                def species = clazz.@species.text().toString().trim()
                def tissue = clazz.@tissue.text()

                log.info("clazz id: ${clazzId}")

                if (species == null || species == "") {
                    species = "NONE"
                }
                if (organ == null || organ == "") {
                    organ = "NONE"
                }

                BBSpecies spec = BBSpecies.findByNameIlike(species)

                if (spec == null) {
                    spec = new BBSpecies()
                    spec.kingdom = BBKingdom.findByNameIlike("NONE")
                    spec.name = species
                    spec.save(flush: true)
                    log.info "index species: ${spec.name}"
                }

                if (classes[clazzId] != null) {
                    classes[clazzId].species = spec

                    BBOrgan org = BBOrgan.findByNameIlike(organ)

                    if (org == null) {
                        org = new BBOrgan()
                        org.name = organ
                        spec.addToOrgans(org)
                        org.save(flush: true)

                        log.info "index organ/spec: ${org.name}/${spec.name}"
                    }

                    if (org.speciees.contains(spec) == false) {
                        spec.addToOrgans(org)
                        spec.save(flush: true)

                        log.info "reindex species/organ: ${spec.name}"

                    }

                    classes[clazzId].organ = org
                    clazz.metadatas.metadata.each {def meta ->

                        def name = meta.@name.text()
                        def value = meta.@value.text()

                        BBMetadata metadata = new BBMetadata()

                        metadata.name = name
                        metadata.value = value

                        metadata.save(flush: true)
                        classes[clazzId].addToMetadata(metadata)

                    }

                    classes[clazzId].save(flush: true)



                    clazz.samples.sample.each {def sample ->
                        def fileName = sample.@fileName.text()
                        def label = sample.@label.text()
                        def comment = sample.@comment.text()

                        classes[clazzId].samples.each { BBExperimentSample s ->

                            if (s.fileName == fileName) {
                                s.label = label
                                s.comment = comment

                                s.save(flush: true)
                            }

                        }

                    }
                }

            }

        }
        catch (Exception e) {
            log.error e.getMessage(), e
            return false
        }

        return true

    }

    /**
     * imports the data from the specified database into the local system
     * @param database database to use
     * @param setupxId setupx id of the experiment
     * @param xmlContent the xml content
     * @param sync do we want to sync the experiment too? by defailt false
     * @return
     */
    def importMetaData(String database, String setupxId, String xmlContent, boolean sync = false, boolean dataArePublic = false) {
        log.info "parsing: \n\n ${xmlContent.trim()}\n\n"
        this.importMetaData(database, setupxId, new XmlSlurper().parseText(xmlContent), sync, dataArePublic)
    }

    /**
     * Generates a DSL to calculate the data for a given SX ids
     * @param database
     * @param setupxId
     * @param xmlContent
     * @return
     */
    def generateDSLForMetada(String database, def setupxId, def xmlData) {
        try {
            def experiment = generateBinBaseExperimentFromMetadata(database, xmlData)

            if (experiment) {
                return new GenerateDSLFromExperiment().dslGenerateForExperimentWithDefaultSOP(experiment, BinBaseConfigReader.getServer())
            }
            else {
                return false
            }
        }
        catch (Exception e) {
            log.error e.getMessage(), e
            return false
        }
    }

    /**
     * generates the metadata xml file from an experiment
     * which can be used for synchronization issues
     * @param experiment
     * @return
     */
    def generateMetaDataAsXml(BBExperiment experiment) {
        def writer = new StringWriter()
        def xml = new MarkupBuilder(writer)
        xml.experiment(id: experiment.id, title: experiment.title, isPublic: experiment.publicExperiment) {
            classes {
                experiment.classes.each {BBExperimentClass clazz ->
                    "class"(id: clazz.id, organ: clazz.organ.name, species: clazz.species.name) {
                        samples {
                            def samp = []

                            clazz.samples.collect {samp << it}

                            Collections.shuffle(samp, new Random())

                            samp.each {BBExperimentSample s ->
                                sample(id: s.id, fileName: s.generateName(), label: s.label, comment: s.comment)
                            }
                        }
                    }
                }
            }
        }

        return writer.toString()

    }

    /**
     * generates a binbase experiment object form the provided metadata
     * @return
     */
    def generateBinBaseExperimentFromMetadata(String database, def xmlData) {
        try {

            if (!(xmlData instanceof NodeChild)) {
                log.info "generate xml slurper object...\n\n${xmlData}\n\n"
                xmlData = new XmlSlurper().parseText(xmlData)
            }

            Experiment exp = new Experiment()
            exp.column = database

            exp.id = xmlData.@id.text()


            assert (exp.id != null)
            assert (exp.id.length() > 0)
            assert (exp.column != null)
            assert (exp.column.length() > 0)


            def classes = []

            xmlData.classes."class".each {def clazz ->
                def clazzId = clazz.@id.text().toString().trim()

                ExperimentClass expClass = new ExperimentClass()

                expClass.id = clazzId
                expClass.column = database

                assert (expClass.id != null)
                assert (expClass.id.length() > 0)
                assert (expClass.column != null)
                assert (expClass.column.length() > 0)

                def samples = []
                clazz.samples.sample.each {def sample ->

                    ExperimentSample s = new ExperimentSample()

                    def fileName = sample.@fileName.text()

                    s.name = fileName
                    s.id = fileName

                    if (s.name != "") {
                        samples.add(s)
                    }

                    if (s.name.contains(',')) {
                        def names = s.name.split(',')
                        s.name = names[names.length - 1]
                    }

                    assert (s.id != null)
                    assert (s.id.length() > 0)
                    assert (s.name != null)
                    assert (s.name.length() > 0)

                }

                ExperimentSample[] sArray = new ExperimentSample[samples.size()]

                for (int i = 0; i < sArray.length; i++) {
                    sArray[i] = samples[i]
                }

                if (samples.size() > 0) {
                    expClass.setSamples(sArray)
                    classes.add(expClass)
                }

            }

            if (classes.size() > 0) {
                ExperimentClass[] cArray = new ExperimentClass[classes.size()]

                for (int i = 0; i < cArray.length; i++) {
                    cArray[i] = classes[i]
                }
                exp.setClasses(cArray)
            }

            assert (exp.classes != null)
            assert (exp.classes.size() > 0)

            return exp
        }
        catch (AssertionError e) {
            throw new Exception(e.getMessage(), e)
        } catch (Exception e) {
            log.error e.getMessage(), e
            return false
        }
    }

    /**
     * schedules the metadata to the binbase service for calculations
     * @param database
     * @param xmldata
     * @return
     */
    def scheduleMetaDataForCalculationToBinBase(String database, def xmldata) {
        def experiment = generateBinBaseExperimentFromMetadata(database, xmldata)

        if (experiment) {
            experiment.classes.each {
                schedulingService.scheduleImport(it)
            }

            schedulingService.scheduleExport(experiment)
        }
    }

    /**
     * checks if an experiment is synchronized and has metadata
     * @param database
     * @param experimentName
     * @return
     */
    def isSynchronized(String database, String experimentName) {
        BBExperiment experiment = BBExperiment.findByDatabaseAndName(BBDatabase.findByName(database), experimentName)

        if (experiment != null) {
            if (exp.title == SynchronizeDatabaseService.NONE_TITLE) {
                return false
            }
            else {
                return true
            }
        }
        return false
    }
}
