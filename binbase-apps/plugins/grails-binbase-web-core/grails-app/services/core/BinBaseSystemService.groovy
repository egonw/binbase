package core

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService
import binbase.core.BinBaseConfigReader
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacade

/**
 * gives you access to the complete binbase system
 */
class BinBaseSystemService {

  static transactional = false

  BinBaseService binbaseEjbService

  ServiceJMXFacade binbaseServiceConfiguration

  /**
   * checks if the txt file exists
   * @param key
   * @param fileName
   * @return
   */
  boolean txtFileExist(String fileName) {
    return binbaseServiceConfiguration.sampleExist(fileName)

  }

  /**
   * test if the cdf file exist
   * @param key
   * @param fileName
   * @return
   */
  boolean cdfFileExist(String fileName) {
    return binbaseEjbService.hasNetCdfFile(fileName, BinBaseConfigReader.key)
  }

  /**
   * retrieves all available columns of the database
   * @param key
   * @return
   */
  List<String> getAvailableColumns() {
    return binbaseEjbService.getRegisteredColumns(BinBaseConfigReader.key).toList()
  }

  /**
   * checks if the system is actually online and ready for communicatons
   * @return
   */
  boolean isOnline() {
    try {

      binbaseEjbService.getRegisteredColumns(BinBaseConfigReader.key).toList()
      return true
    }
    catch (Exception e) {
      return false
    }
  }
}
