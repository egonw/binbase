package core

import com.sun.jmx.snmp.SnmpString
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacade

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade

/**
 * simple service to access sop files and register sop files on the server
 */
class SopService {

    ExportJMXFacade binbaseExportConfiguration

    static transactional = true

    /**
     * returns the default sop
     * @return
     */
    String getDefaultSopName(){
        binbaseExportConfiguration.getDefaultSop()
    }

    /**
     * returns the xml as string for the default sop
     * @return
     */
    String getDefaultSopXML(){
        new String(binbaseExportConfiguration.getSop(getDefaultSopName()))
    }

    /**
     * uploads an sop file to the server with the specified content
     * @param name
     * @param xmlContent
     */
    def uploadSopFile(String name, String xmlContent){
       binbaseExportConfiguration.uploadSop(name,xmlContent.bytes)
    }
}
