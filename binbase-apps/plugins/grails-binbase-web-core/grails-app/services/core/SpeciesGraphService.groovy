package core

import groovy.sql.Sql
import javax.sql.DataSource

class SpeciesGraphService {

    static transactional = false

    DataSource dataSource

    /**
     * returns the sample count for each species
     */
    Collection sampleCountForSpecies() {

        Sql sql = Sql.newInstance(dataSource)

        def result = []

        sql.eachRow("""

        select count(*) as sample,e.name as species

        from
            BBEXPERIMENT_SAMPLE c,
            BBEXPERIMENT_CLASS d,
            BBSPECIES e

         where
            d.id = c.experiment_class_id and
            d.species_id = e.id

         group by

            e.name

         """) {

            result.add(["${it.species}", it.sample])


        }

        return result
    }

    /**
     * returns the count of samples for each organ for this species
     * @param speciesId
     * @return
     */
    Collection sampleCountForOrgansForSpecies(def speciesId) {

        speciesId = Integer.parseInt(speciesId.toString())

        Sql sql = Sql.newInstance(dataSource)

        def result = []

        sql.eachRow("""

            select count(*) as sample ,c.name as organ
                from
                    bbexperiment_sample a,
                    bbexperiment_class b,
                    bborgan c
                where
                    a.experiment_class_id = b.id and
                    c.id = b.organ_id and
                    b.species_id = ${speciesId}
                group by c.name

         """) {

            result.add(["${it.organ}", it.sample])


        }

        return result
    }

    /**
     * displays the species which have samples with this organ and the sample count
     * @param organId
     * @param speciesId
     * @return
     */
    Collection sampleCountForSpeciesForOrgan(def organId) {

        organId = Integer.parseInt(organId.toString())


        Sql sql = Sql.newInstance(dataSource)

        def result = []

        sql.eachRow("""

        select count(*) as sample,e.name as species

        from
            BBEXPERIMENT_SAMPLE c,
            BBEXPERIMENT_CLASS d,
            BBSPECIES e

         where
            d.id = c.experiment_class_id and
            d.species_id = e.id and
            d.organ_id = ${organId}

         group by

            e.name


         """) {

            result.add(["${it.species}", it.sample])


        }

        return result
    }
    /**
     * returns the count of samples for each organ for this species
     * @param speciesId
     * @return
     */
    Collection sampleCountForOrgans(def organId) {

        organId = Integer.parseInt(organId.toString())

        Sql sql = Sql.newInstance(dataSource)

        def result = []

        sql.eachRow("""

            select count(*) as sample ,c.name as organ
                from
                    bbexperiment_sample a,
                    bbexperiment_class b,
                    bborgan c
                where
                    a.experiment_class_id = b.id and
                    c.id = b.organ_id and
                    b.organ_id = ${organId}
                group by c.name

         """) {

            result.add(["${it.organ}", it.sample])


        }

        return result
    }

        /**
     * returns the count of samples for each organ for this species
     * @param speciesId
     * @return
     */
    Collection sampleCountForOrgans() {

        Sql sql = Sql.newInstance(dataSource)

        def result = []

        sql.eachRow("""

            select count(*) as sample ,c.name as organ
                from
                    bbexperiment_sample a,
                    bbexperiment_class b,
                    bborgan c
                where
                    a.experiment_class_id = b.id and
                    c.id = b.organ_id
                group by c.name

         """) {

            result.add(["${it.organ}", it.sample])


        }

        return result
    }
    /**
     * returns the counts of experiments for each species
     * @return
     */
    Collection experimentCountBySpecies() {

        Sql sql = Sql.newInstance(dataSource)

        def result = []

        sql.eachRow("""

        select
            count(distinct d.experiment_id) as experiment,
            e.name as species
        from
            BBEXPERIMENT_CLASS d,
            BBSPECIES e

         where
            d.species_id = e.id

         group by e.name
        """) {
            result.add(["${it.species}", it.experiment])

        }

        return result
    }

    /**
     * calculates the annoations for a given bin over all species
     * perfect for a stacked bar chart
     * @param binId
     * @return
     */
    Map annotationCountForSpeciesByBin(def binId, def relative = false) {

        binId = Long.parseLong(binId.toString())
        relative = Boolean.parseBoolean(relative.toString())

        Sql sql = Sql.newInstance(dataSource)

        //the final dataset
        def finalData = [:]

        //the build dataset
        def result = [:]

        //labels for rows
        def labels = [:]

        //annotation count for bins
        def bins = []

        //annotation count for samples
        def sampels = []

        int counter = 0;

        sql.eachRow("""

select a.annotationCount,a.species,b.sampleCount from
(
select
    count(*) as annotationCount, e.name as species
from
    bbspectra b,
    bbexperiment_sample c,
    bbexperiment_class d,
    bbspecies e

where
    b.sample_id = c.id and
    c.experiment_class_id = d.id and
    d.species_id = e.id and
    b.bin_id = ${binId}

group by
    b.bin_id,
    e.name

) a,
(
select
    count(*) as sampleCount, e.name as species
from
    bbexperiment_sample c,
    bbexperiment_class d,
    bbspecies e

where

    c.experiment_class_id = d.id and
    d.species_id = e.id

group by e.name
) b

where a.species = b.species

        """) {

            if (relative) {
                bins.add(it.annotationCount / it.sampleCount * 100)
                sampels.add(100)
            }
            else {
                bins.add(it.annotationCount)
                sampels.add(it.sampleCount)
            }
            labels.put(counter, it.species)
            counter = counter + 1
        }

        result.put("Annotation Count", bins)
        result.put("Sample Count", sampels)

        finalData.data = result
        finalData.label = labels

        return finalData
    }
}

