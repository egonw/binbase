package core

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler
import binbase.core.BinBaseConfigReader

/**
 * provides you with access to binbase scheduling
 * service
 */
class BinBaseSchedulingService {

    static transactional = false

    /**
     * scheduling service of binbase
     */
    Scheduler schedulingService

    /**
     * imports the given class into the system and calculates it
     * @param clazz
     * @return
     */
    def scheduleImport(ExperimentClass clazz) {
        schedulingService.scheduleImport(clazz, BinBaseConfigReader.getKey())
    }

    /**
     * schedules the given experiment for calculations
     * @param clazz
     * @return
     */
    def scheduleExport(Experiment experiment) {
        schedulingService.scheduleExport(experiment, BinBaseConfigReader.getKey())

    }

    /**
     * schedules a dsl for calculations
     * @param dsl
     */
    def scheduleDSL(DSL dsl) {
        schedulingService.scheduleDSL(dsl, BinBaseConfigReader.getKey())
    }

}
