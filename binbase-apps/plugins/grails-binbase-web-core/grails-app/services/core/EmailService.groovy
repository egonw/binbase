package core

import javax.mail.Message
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import javax.mail.Session
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.CommunicationJMXFacade
import javax.mail.Multipart
import javax.mail.BodyPart
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMultipart
import javax.mail.Transport
import javax.mail.Authenticator
import javax.mail.PasswordAuthentication

/**
 * used to send an email using the binbase system
 */
class EmailService {

    CommunicationJMXFacade binbaseCommunicationService

    static transactional = true

    /**
     * simple way to send an email
     * @param address
     * @param subject
     * @param message
     * @return
     */
    boolean sendEmail(Collection<String> address, String subject, String content) {

        try {
            InternetAddress[] a = new InternetAddress[address.size()]

            address.eachWithIndex {String adi, int i ->
                a[i] = new InternetAddress(adi)
            }

            Properties props = new Properties();

            props.put("mail.smtp.host", binbaseCommunicationService.getSmtpServer());
            props.put("mail.smtp.auth", "true");

            props.put("mail.smtp.port", binbaseCommunicationService.getSmtpPort());
            props.put("mail.smtp.socketFactory.port", binbaseCommunicationService.getSmtpPort());
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.fallback", "false");

            Authenticator auth = {
                try {
                    return new PasswordAuthentication(binbaseCommunicationService.getUsername(), binbaseCommunicationService.getPassword());
                } catch (Exception e) {
                    throw new RuntimeException("authentifiacation faild: " + e.getMessage(), e);
                }
            } as Authenticator

            Session session = Session.getDefaultInstance(props, auth);

            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(binbaseCommunicationService.getFromAdress()));


            msg.setRecipients(Message.RecipientType.TO, a);

            msg.setSubject(subject);
            msg.saveChanges();


            BodyPart body = new MimeBodyPart();

            body.setText(content);
            Multipart multi = new MimeMultipart();
            multi.addBodyPart(body);
            msg.setContent(multi);
            msg.setSentDate(new Date());

            Transport.send(msg);

        }
        catch (Exception e) {
            log.error(e.getMessage(), e)
            return false
        }

        return true
    }

}
