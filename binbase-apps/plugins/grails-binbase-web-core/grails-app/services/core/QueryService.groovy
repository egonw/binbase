package core

import binbase.web.core.BBExperiment
import groovy.sql.Sql
import javax.sql.DataSource
import binbase.web.core.BBResult
import binbase.web.core.BBBin

/**
 * used to fetch data
 */
class QueryService {
    DataSource dataSource

    static transactional = true

    /**
     * returns all experiments for this species
     * @param speciesId
     * @return
     */
    def experimentsForSpecies(def speciesId, def params = [:]) {

        speciesId = Long.parseLong(speciesId.toString())

        return BBExperiment.executeQuery("select distinct a.experiment from BBExperimentClass a where a.species.id = ?", [speciesId], params)

    }

    /**
     * returns all experiments for this species
     * @param speciesId
     * @return
     */
    def experimentsForSpeciesAndOrgan(def speciesId, def organId, def params = [:]) {

        speciesId = Long.parseLong(speciesId.toString())
        organId = Long.parseLong(organId.toString())

        return BBExperiment.executeQuery("select distinct a.experiment from BBExperimentClass a where a.species.id = ? and a.organ.id = ?", [speciesId, organId], params)

    }

    /**
     * returns all experiments for this species
     * @param speciesId
     * @return
     */
    def experimentsForOrgan(def organId, def params = [:]) {

        organId = Long.parseLong(organId.toString())

        return BBExperiment.executeQuery("select distinct a.experiment from BBExperimentClass a where a.organ.id = ?", [organId], params)

    }

    /**
     * fetches all the results for a given experiment
     * @param experimentId
     * @param params
     * @return
     */
    def resultsForExperiment(def experimentId, def params = [:]) {
        experimentId = Long.parseLong(experimentId.toString())

        def result = BBResult.findAllByExperiment(BBExperiment.get(experimentId), params)

        return result
    }

    /**
     * calcualtes the sample count for a certain species
     * @param speciesId
     * @return
     */
    def retrieveSampleCountForSpecies(def speciesId) {


        speciesId = Integer.parseInt(speciesId.toString())

        Sql sql = Sql.newInstance(dataSource)

        def result = []

        sql.eachRow("""

        select

            count(c.id) as sample

        from
            BBEXPERIMENT_SAMPLE c,
            BBEXPERIMENT_CLASS d,
            BBSPECIES e

         where
            d.id = c.experiment_class_id and
            d.species_id = e.id and
            e.id = ${speciesId}


         """) {

            result.add(it.sample)


        }

        return result[0]
    }

    /**
     * calcualtes the sample count for a certain species
     * @param speciesId
     * @return
     */
    def retrieveSampleCountForOrgan(def organId) {


        organId = Integer.parseInt(organId.toString())

        Sql sql = Sql.newInstance(dataSource)

        def result = []

        sql.eachRow("""

                        select count(*) as sample
                from
                    bbexperiment_sample a,
                    bbexperiment_class b,
                    bborgan c
                where
                    a.experiment_class_id = b.id and
                    c.id = b.organ_id and
                    b.organ_id = ${organId}
             """) {

            result.add(it.sample)


        }

        return result[0]
    }

    /**
     * all bins for organ
     * @param organId
     * @param params
     * @return
     */
    def binsForOrgan(def organId, def params = [:]) {
        organId = Long.parseLong(organId.toString())
        return BBBin.findAll("from BBBin a where a.id in (select distinct a.bin.id from BBSpectra a where a.sample.experimentClass.organ.id = ?)", [organId], params)
    }

    /**
     * all bins for species
     * @param speciesId
     * @param params
     * @return
     */
    def binsForSpecies(def speciesId, def params = [:]) {
        speciesId = Long.parseLong(speciesId.toString())
        return BBBin.findAll("from BBBin a where a.id in (select distinct a.bin.id from BBSpectra a where a.sample.experimentClass.species.id = ?)", [speciesId], params)

    }

    /**
     * all bins for species and organ
     * @param organId
     * @param speciesId
     * @param params
     * @return
     */
    def binsForOrganAndSpecies(def organId, def speciesId, def params = [:]) {
        speciesId = Long.parseLong(speciesId.toString())
        organId = Long.parseLong(organId.toString())
        return BBBin.findAll("from BBBin a where a.id in (select distinct a.bin.id from BBSpectra a where a.sample.experimentClass.species.id = ? and a.sample.experimentClass.organ.id = ?)", [speciesId, organId], params)

    }

}
