package core

import javax.sql.DataSource
import groovy.sql.Sql
import binbase.web.core.BBBin
import binbase.web.core.BBStatisticSnapshot
import binbase.web.core.TakeSnapshotJob

/**
 * used to generate basic statistics in the system
 */
class StatisticsService {

    static transactional = true

    DataSource dataSource

    def grailsApplication

    /**
     * creates a snapshot of the count of all domain classes
     * @return
     */
    def createSnapShot() {

        grailsApplication.getDomainClasses().each {
            if (it.name != BBStatisticSnapshot) {
                TakeSnapshotJob.triggerNow([objectName: it.name])
            }

        }

    }

    /**
     * simple query of snapshots
     * @param name
     * @param count
     * @return
     */
    def querySnapShots(String name, def params = [:]) {
        return BBStatisticSnapshot.findAllByObjectName(name, params)
    }

    /**
     * calculates how often bins are found in different percentage windows
     * @param experimentId
     * @return
     */
    def calculateBinAnnotationDistributionForExperiment(def experimentId) {
        experimentId = Long.parseLong(experimentId.toString())

        def result = [:]

        def labels = [:]
        def data = []

        def counter = 0;
        Sql sql = Sql.newInstance(dataSource)

        sql.eachRow("""

        select
        count(bin) as annotations,
        CASE
            when hitrate < 10 then '0-10%'
            when hitrate > 10 and hitrate <= 20 then '10-20%'
            when hitrate > 20 and hitrate <= 30 then '20-30%'
            when hitrate > 30 and hitrate <= 40 then '30-40%'
            when hitrate > 40 and hitrate <= 50 then '40-50%'
            when hitrate > 50 and hitrate <= 60 then '50-60%'
            when hitrate > 60 and hitrate <= 70 then '60-70%'
            when hitrate > 70 and hitrate <= 80 then '70-80%'
            when hitrate > 80 and hitrate <= 90 then '80-90%'
            when hitrate > 90 and hitrate <= 100 then '90-100%'

        end as hit_group
from
(
select (a.annotationCount/(cast(b.sampleCount as float))*100) as hitrate,bin


 from
(
select
    count(*) as annotationCount, a.name as bin
from
    bbbin a,
    bbspectra b,
    bbexperiment_sample c,
    bbexperiment_class d,
    bbexperiment e

where
    a.id = b.bin_id and
    b.sample_id = c.id and
    c.experiment_class_id = d.id and
    d.experiment_id = e.id and
    e.id = ${experimentId}
group by
    a.name
) a,
(
select
    count(*) as sampleCount
from
    bbexperiment_sample c,
    bbexperiment_class d,
    bbexperiment e

where
    c.experiment_class_id = d.id and
    d.experiment_id = e.id and
    e.id = ${experimentId}

) b
) c

group by hit_group

order by hit_group

        """) {

            data.add(it.annotations)
            labels.put(counter, it."hit_group")
            counter = counter + 1
        }


        result.data = ["Annotations Count": data]
        result.labels = labels

        return result
    }

    /**
     * is an acuumulative view how many anotations we have in certain percentage groups
     * for the given experiment
     * @param experimentId
     * @return
     */
    def calculateAccumulativeBinAnnotationDistributionForExperiment(def experimentId) {
        experimentId = Long.parseLong(experimentId.toString())

        def result = [:]

        def labels = [:]
        def data = []

        def counter = 0;
        Sql sql = Sql.newInstance(dataSource)

        sql.eachRow("""

            select
            count(bin) as annotations,
            CASE
                when hitrate < 10 then '0'
                when hitrate > 0 and hitrate <= 20 then 1
                when hitrate > 0 and hitrate <= 30 then 2
                when hitrate > 0 and hitrate <= 40 then 3
                when hitrate > 0 and hitrate <= 50 then 4
                when hitrate > 0 and hitrate <= 60 then 5
                when hitrate > 0 and hitrate <= 70 then 6
                when hitrate > 0 and hitrate <= 80 then 7
                when hitrate > 0 and hitrate <= 90 then 8
                when hitrate > 0 and hitrate <= 100 then 9

            end as hit_group
    from
    (
    select (a.annotationCount/(cast(b.sampleCount as float))*100) as hitrate,bin


     from
    (
    select
        count(*) as annotationCount, a.name as bin
    from
        bbbin a,
        bbspectra b,
        bbexperiment_sample c,
        bbexperiment_class d,
        bbexperiment e

    where
        a.id = b.bin_id and
        b.sample_id = c.id and
        c.experiment_class_id = d.id and
        d.experiment_id = e.id and
        e.id = ${experimentId}
    group by
        a.name
    ) a,
    (
    select
        count(*) as sampleCount
    from
        bbexperiment_sample c,
        bbexperiment_class d,
        bbexperiment e

    where
        c.experiment_class_id = d.id and
        d.experiment_id = e.id and
        e.id = ${experimentId}

    ) b
    ) c

    group by hit_group

    order by hit_group

            """) {

            data.add(it.annotations)

            //calculate our label name
            def label = ""
            switch (it."hit_group") {

                case 0:
                    label = ">0%"
                    break;
                case 1:
                    label = ">10%"

                    break;
                case 2:
                    label = ">20%"

                    break;
                case 3:
                    label = ">30%"

                    break;
                case 4:
                    label = ">40%"

                    break;
                case 5:
                    label = ">50%"

                    break;
                case 6:
                    label = ">60%"

                    break;
                case 7:
                    label = ">70%"

                    break;
                case 8:
                    label = ">80%"

                    break;

                case 9:

                    label = ">90%"
                    break;
            }


            labels.put(counter, label)
            counter = counter + 1
        }

        //accumulate the data
        def temp = []

        data.each {
            temp.add(it)
        }

        temp.reverse()

        for (int i = 0; i < data.size(); i++) {
            data[i] = temp.sum()
            temp.remove(0)
        }
        result.data = ["Accumulative Annotations Count": data]


        result.labels = labels
        return result
    }

    /**
     * returns all bins for a given hit rate
     * @param experimentId
     * @param hitRate
     * @return
     */
    def findBinsForHitRateForExperiment(def experimentId, int hitRate) {
        experimentId = Long.parseLong(experimentId.toString())

        def result = []

        Sql sql = Sql.newInstance(dataSource)
        sql.eachRow("""

        select (
        bin
 from
(
select
    count(*) as annotationCount, a.id as bin
from
    bbbin a,
    bbspectra b,
    bbexperiment_sample c,
    bbexperiment_class d,
    bbexperiment e

where
    a.id = b.bin_id and
    b.sample_id = c.id and
    c.experiment_class_id = d.id and
    d.experiment_id = e.id and
    e.id = ${experimentId}
group by
    a.name
) a,
(
select
    count(*) as sampleCount
from
    bbexperiment_sample c,
    bbexperiment_class d,
    bbexperiment e

where
    c.experiment_class_id = d.id and
    d.experiment_id = e.id and
    e.id = ${experimentId}

)
b
where (a.annotationCount/(cast(b.sampleCount as float))*100) > ${hitRate}

        """) {
            result.add(BBBin.get(it.bin))

        }

        return result
    }
}
