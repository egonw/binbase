// configuration for plugin testing - will not be included in the plugin zip

def ENV_NAME = "${app.name}-config"

if (!grails.config.location || !(grails.config.location instanceof List)) {
    grails.config.location = []
}
if (System.getenv(ENV_NAME)) {
    println "Including configuration file specified in environment: " + System.getenv(ENV_NAME);
    grails.config.location << "file:" + System.getenv(ENV_NAME)
} else if (System.getProperty(ENV_NAME)) {
    println "Including configuration file specified on command line: " + System.getProperty(ENV_NAME);
    grails.config.location << "file:" + System.getProperty(ENV_NAME)
} else {
    println "No external configuration file defined, assuming defaults\n"

    grails.config.locations = ["classpath:bwc-config.properties",
            "classpath:bwc-config.groovy",
            "file:${userHome}/.grails/${app.name}-config.properties",
            "file:${userHome}/.grails/${app.name}-config.groovy",
            "file:./bwc-config.properties",
            "file:./bwc-config.groovy"

    ]

    println ""
}



// log4j configuration
log4j = {

    //tomcat based logging
    def catalinaBase = System.properties.getProperty('catalina.base')
    if (!catalinaBase) catalinaBase = '.'   // just in case
    def logDirectory = "${catalinaBase}/logs"
    // Example of changing the log pattern for the default console
    // appender:
    //
    appenders {
        console name: 'stdout', layout: pattern(conversionPattern: '[%d{ABSOLUTE}] [%-5p] [%t] [%-5C] [%m]%n'), threshold: org.apache.log4j.Level.INFO
        rollingFile name: 'debugLog', file: "${logDirectory}/${app.name}_debug.log".toString(), maxFileSize: '1MB', threshold: org.apache.log4j.Level.DEBUG, layout: pattern(conversionPattern: '[%d{ABSOLUTE}] [%-5p] [%t] [%-5C] [%m]%n')

        rollingFile name: 'infoLog', file: "${logDirectory}/${app.name}.log".toString(), maxFileSize: '100KB', threshold: org.apache.log4j.Level.INFO, layout: pattern(conversionPattern: '[%d{ABSOLUTE}] [%-5p] [%t] [%-5C] [%m]%n')
        rollingFile name: 'stacktrace', file: "${logDirectory}/${app.name}_stack.log".toString(), maxFileSize: '100KB', layout: pattern(conversionPattern: '[%d{ABSOLUTE}] [%-5p] [%t] [%-5C] [%m]%n')
    }
    root {
        info 'infoLog', stdout
        debug 'debugLog'
        additivity = true
    }

    error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
            'org.codehaus.groovy.grails.web.pages', //  GSP
            'org.codehaus.groovy.grails.web.sitemesh', //  layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping', // URL mapping
            'org.codehaus.groovy.grails.commons', // core / classloading
            'org.codehaus.groovy.grails.plugins', // plugins
            'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate',
            'org.dbunit.dataset.AbstractTableMetaData'

    warn 'org.mortbay.log', 'org.compass.core'
    info "grails.app", 'org.dbunit'

    //debug "org.codehaus.groovy.grails.web.mapping.filter"

    //debugging goes here
    debug debugLog: ["grails.app", "org.codehaus.groovy.grails.plugins.orm.auditable", 'org.codehaus.groovy.grails.web', 'org.codehaus.groovy.grails.web.mapping.filter'], additivity: true


}

// The following properties have been added by the Upgrade process...
grails.views.default.codec="none" // none, html, base64
grails.views.gsp.encoding="UTF-8"


/**
 * we want to use jquery by default
 */
grails.views.javascript.library = "jquery"


