/**
 * this file contains the binbase configuration for this plugin.
 *
 */
binbase {

    /**
     * key to access the binbase instance
     */

    key = "129766fa5c3129766fa5c3"

    /**
     * ip of the jboss server which is running binbase
     */
    server = "eros.fiehnlab.ucdavis.edu"

}