package binbase.web.core

class BBExperimentSample extends Association implements Comparable {

    static hasMany = [spectra: BBSpectra]

    static belongsTo = [experimentClass: BBExperimentClass]

    static constraints = {

        spectra(nullable: true)
        fileName(blank: true, unique: true, nullable: true)

        //the experiment class has to be set as nullable
        experimentClass(nullable: true)

        //comments are not unique and can be null but not blank
        comment(blank: false, unique: false, nullable: true)

        //labels are not unique and can be null, but not blank
        label(blank: false, unique: false, nullable: true)
    }


    static searchable = [only: ["fileName", "comment", "label",'experimentClass']]

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        tablePerHierarchy false
        spectra cascade: "all-delete-orphan"
    }


    String fileName

    /**
     * related samples
     */
    Set<BBSpectra> spectra

    /**
     * owning class
     */
    BBExperimentClass experimentClass

    /**
     * comment for a sample
     */
    String comment

    /**
     * label for a sample
     */
    String label


    int compareTo(Object t) {
        if (t instanceof BBExperimentSample) {
            BBExperimentSample s = t

            if (s.fileName != null && fileName != null) {
                return fileName.compareTo(s.fileName)
            }
            if (s.id != null && id != null) {
                return id.compareTo(s.id)
            }

        }
        return toString().compareTo(t.toString())
    }

    /**
     * generates the file name to be used with metadata and so
     * @return
     */
    public String generateName(){
        return fileName
    }
}
