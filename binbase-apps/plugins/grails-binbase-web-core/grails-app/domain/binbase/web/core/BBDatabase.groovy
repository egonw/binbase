package binbase.web.core

/**
 * the database
 */
class BBDatabase {


    static searchable = [only: ['name']]

    static constraints = {
        name unique: true
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
    }

    /**
     * the name of this database
     */
    String name
}
