package binbase.web.core

class BBExperiment extends Association {

    static belongsTo = [database: BBDatabase]

    /**
     * we have many classes
     */
    static hasMany = [classes: BBExperimentClass, results: BBResult]

    static constraints = {
        database(nullable: true)
        publicExperiment(nullable: true)
    }


    static searchable = [only: ['name', 'title','id']]

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        tablePerHierarchy false
        classes cascade: "all-delete-orphan"
    }

    /**
     * is this experiment public
     */
    Boolean publicExperiment = false

    /**
     * the name of the experiment
     */
    String name

    /**
     * title of an experiment
     */
    String title
    /**
     * related classes
     */
    SortedSet<BBExperimentClass> classes

    /**
     * associated results
     */
    SortedSet<BBResult> results

    /**
     * relarted database
     */
    BBDatabase database

}
