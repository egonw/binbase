package binbase.web.core

/**
 * a class dedicated to content data and results. The idea is to reduce the server load.
 */
class BBContent {

    static belongsTo = BBDownload
    static constraints = {
        content(nullable: false)
        result(nullable: false)
    }


    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
    }

    /**
     * related result
     */
    BBDownload result

    /**
     * the actual content of the result
     */
    byte[] content

}
