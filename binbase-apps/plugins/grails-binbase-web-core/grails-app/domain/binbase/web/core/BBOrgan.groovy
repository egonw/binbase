package binbase.web.core

class BBOrgan implements Comparable {

    static searchable = [only: ['name','speciees']]

    static belongsTo = BBSpecies

    /**
     * we have more than one possible species for an organ
     */
    static hasMany = [speciees: BBSpecies]

    static constraints = {
        name(unique: true, blank: false)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        tablePerHierarchy false
    }

    /**
     * related species for this organ
     */
    Set<BBSpecies> speciees

    /**
     * name of this organ
     */
    String name

    /**
     * string representation
     * @return
     */
    String toString() {
        return name
    }


    int compareTo(Object t) {
        return toString().compareTo(t.toString())
    }
}
