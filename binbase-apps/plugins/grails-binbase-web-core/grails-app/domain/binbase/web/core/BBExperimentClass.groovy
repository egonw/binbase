package binbase.web.core

class BBExperimentClass extends Association {

    static belongsTo = [experiment: BBExperiment]

    /**
     * we have many classes
     */
    static hasMany = [samples: BBExperimentSample, metadata: BBMetadata]


    static searchable = [only: ['name','experiment','organ','species']]

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        tablePerHierarchy false
        name column: "name",index: 'experimentclass_name_index'
        samples cascade: "all-delete-orphan"

    }


    static constraints = {
        organ(nullable: true)
        species(nullable: true)
        metadata(nullable: true)
        experiment(nullable: true)
        samples(nullable: true)
    }

    /**
     * associated organ
     */
    BBOrgan organ

    /**
     * associated species
     */
    BBSpecies species

    /**
     * name of this experimetn class
     */
    String name

    /**
     * related samples
     */
    SortedSet<BBExperimentSample> samples

    /**
     * related metadata
     */
    Set<BBMetadata> metadata

    /**
     * the owning experiment
     */
    BBExperiment experiment


}
