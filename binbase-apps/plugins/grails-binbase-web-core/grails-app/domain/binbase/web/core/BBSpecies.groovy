package binbase.web.core

class BBSpecies implements Comparable{

    static searchable = [only: ['name','kingdom','organs']]

    static belongsTo = [kingdom: BBKingdom]

    static hasMany = [organs: BBOrgan]

    static constraints = {
        name(unique: true, blank: false)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        tablePerHierarchy false
    }

    /**
     * set of organs for this species
     */
    Set<BBOrgan> organs

    /**
     * kingdom for this species
     */
    BBKingdom kingdom

    /**
     * name of the species
     */
    String name

    String toString() {
        return name
    }


    int compareTo(Object t) {
        return toString().compareTo(t.toString())
    }

    /**
     * equals function
     * @param object
     * @return
     */
    boolean equals(Object object) {

        if (object instanceof BBSpecies) {
            return object.id.equals(id)
        }

        return false
    }
}
