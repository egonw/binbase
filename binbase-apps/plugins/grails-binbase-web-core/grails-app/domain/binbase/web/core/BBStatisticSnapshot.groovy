package binbase.web.core

/**
 * used to create a snapshop of the systems statistics
 */
class BBStatisticSnapshot {

    static constraints = {
        objectCount(unique: false, nullable: false)
        objectName(unique: false, nullable: false)
        createdAt(unique: false, nullable: false)

    }

    Long objectCount

    String objectName

    Date createdAt

    String toString() {
        return "${objectName}:${objectCount}:${createdAt}"
    }
}
