package binbase.web.core

/**
 * defines an external id like setupx or so
 */
class ExternalId implements Comparable {


  static searchable= [only: ['value']]

  static belongsTo = [association: Association]


  static constraints = {
  }
  /**
   * mapping
   */
  static mapping = {
    id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
    version false

  }

  /**
   * value of the external id
   */
  String value

  /**
   * related object
   */
  Association association


  String toString() {
    return value
  }

  int compareTo(Object t) {
    return this.toString().compareTo(t.toString())
  }
}
