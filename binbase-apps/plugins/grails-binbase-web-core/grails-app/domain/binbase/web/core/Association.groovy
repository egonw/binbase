package binbase.web.core

class Association implements Comparable {

    static auditable = true

    static searchable = false

    static hasMany = [externalIds: ExternalId]

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        tablePerHierarchy false
    }
    /**
     * external ids
     */
    SortedSet<ExternalId> externalIds


    String toString() {
        return id.toString()
    }

    int compareTo(Object t) {
        if (t instanceof Association) {
            return this.id.compareTo(t.id)
        }
        else {
            return this.toString().compareTo(t.toString())
        }
    }
}
