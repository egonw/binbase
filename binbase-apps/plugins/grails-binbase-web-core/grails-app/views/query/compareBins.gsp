<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: 8/8/11
  Time: 3:42 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Compare Selected Bins</title>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="article">
    <g:render template="compareBins" model="[binInstances:binInstances]"/>
</div>
</body>
</html>