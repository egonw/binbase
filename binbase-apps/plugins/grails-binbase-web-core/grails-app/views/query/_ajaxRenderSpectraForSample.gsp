<table>
    <thead>
    <tr>
        <th class="ui-state-default" >Bin</th>
        <th class="ui-state-default" >Retention Index</th>
        <th class="ui-state-default" >Match</th>
        <th class="ui-state-default" >Similarity</th>
        <th class="ui-state-default" ></th>

    </tr>
    </thead>

    <tbody>
    <g:each in="${BBSpectraInstanceList}" var="s" status="i">
        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
            <td><g:link controller="BBBin" action="show" id="${s.bin.id}">
                <g:if test="${s.bin.name.length() > 25}">
                    ${s.bin.name.toString().substring(0, 25)}...
                </g:if>
                <g:else>
                    ${s.bin.name}
                </g:else>

            </g:link></td>
            <td><g:formatNumber number="${s.retentionIndex}"
                                format="########0"/></td>
            <td><g:formatNumber number="${s.similarity}"
                                format="########0"/></td>
            <td><g:formatNumber format="########0" number="${s.intensity}"/></td>

            <td><g:link controller="BBSpectra" action="show" id="${s.id}">
                show
            </g:link></td>
        </tr>
    </g:each>
    </tbody>
</table>