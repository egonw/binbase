<g:javascript>

    /**
    * sets the value of the elements based on if there checked or not and if at least one is checked, we are enabled the compare button
    */
    jQuery(document).ready(function() {
        $("input:submit").button();
        $("input:submit").attr('disabled', 'disabled');
        $('input:checkbox').each(
        function() {
            $(this).click(function() {

                if (this.checked) {
                    var value = $(this).attr('id');
                    $(this).val(value);
                }

                if ($("input:checked").length >= 1) {
                    $("input:submit").removeAttr('disabled');
                }
                else {
                    $("input:submit").attr('disabled', 'disabled');
                }
            });
        }
        );
    });

</g:javascript>
<g:form controller="query" action="compareBins">
    <table>
        <thead>
        <tr>
            <th class="ui-state-default" ></th>
            <th class="ui-state-default" >Bin Name</th>
            <th class="ui-state-default" >BinBase Bin id</th>
            <th class="ui-state-default" >Retention Index Difference</th>
            <th class="ui-state-default" >Unique Mass Identical</th>
            <th class="ui-state-default" >Similarity</th>
        </tr>
        </thead>
        <tbody>

        <g:each in="${similarities}" var="sim">

            <tr>
                <td><g:checkBox name="compare" id="${sim.id}"/></td>

                <td><g:link controller="BBBin" action="show" id="${sim.to.id}">${sim.to.name}</g:link></td>
                <td>${sim.to.binbaseBinId}</td>

                <td>${sim.from.retentionIndex - sim.to.retentionIndex}</td>
                <td>${sim.from.uniqueMass.equals(sim.to.uniqueMass)}</td>

                <td>${sim.similarity}</td>

            </tr>
        </g:each>

        </tbody>
    </table>

    <div>
        <g:submitButton name="compare_bins" value="display selected bins"/>
    </div>
</g:form>

