<div id="result_experiment" class="list">
    <g:if test="${BBResultInstanceList.size() == 0}">

        <div class="message">sorry there are currently no results available for this experiment!</div>

    </g:if>
    <g:else>
        <table>
            <thead>
            <tr>

                <th class="ui-state-default" >Filename</th>
                <th class="ui-state-default" >Description</th>
                <th class="ui-state-default" >Upload Date</th>

            </tr>
            </thead>
            <tbody>
            <g:each in="${BBResultInstanceList}" status="i" var="file">
                <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                    <td><g:link controller="BBDownload" class="downloadFile" action="download"
                                id="${file.id}">${file.fileName}</g:link></td>

                    <td>${file.description}</td>

                    <td>${file.uploadDate}</td>

                </tr>
            </g:each>
            </tbody>
        </table>
    </g:else>

</div>
