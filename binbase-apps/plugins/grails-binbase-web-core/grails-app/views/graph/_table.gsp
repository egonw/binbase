
<%
//sort our data object from largest to lowest
data = data.sort{ def a, def b ->

      return b[1].compareTo(a[1])
}

%>

<table>
    <thead>

    </thead>

    <tbody>
        <g:each in="${data}" var="content" status="i">
            <tr>
                <g:each in="${content}" var="x">
                    <td>${x}</td>
                </g:each>
            </tr>
        </g:each>
    </tbody>
</table>