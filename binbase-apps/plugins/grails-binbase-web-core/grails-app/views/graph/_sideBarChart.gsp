<g:setProvider library="jquery"/>

<bluff:sideStackedBar options="['sort':true]" id="graph_sidebar_${System.currentTimeMillis()}" data="${data}"
                      labels="${label}" theme="theme_pastel" tooltips="true"/>



<%
    def dialogId = "dialog_graph_sidebar_${System.currentTimeMillis()}"
    def buttonId = "dialog_button_graph_sidebar_${System.currentTimeMillis()}"
%>


<script>
    $(function() {

        $(document).ready(function() {
            // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
            $("#dialog:ui-dialog").dialog("destroy");

            $("#${dialogId}").dialog({
                height: 480,
                width: 720,
                modal: true,
                autoOpen: false
            });

            $("#${buttonId}").click(function() {
                $("#${dialogId}").dialog('open');
                return false;
            });

        });
    });
</script>

<%

    if (data.get("Annotation Count").size() > 0) {
%>


<div id="${dialogId}" title="Data">
    <p>
        <g:render template="table" model="[data:data]" plugin="binbase-web-core"/>
    </p>
</div>


<div style="margin-top: 20px">
    <button id="${buttonId}">display data</button>
</div>
<g:javascript>
    jQuery(document).ready(function() {
        $("#${buttonId}").button();

    });
</g:javascript>

<%
    }
%>
