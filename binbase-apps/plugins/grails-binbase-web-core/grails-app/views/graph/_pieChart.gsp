<g:setProvider library="jquery"/>

<%
    def dialogId = "dialog_graph_pie_${System.nanoTime()}"
    def buttonId = "dialog_button_graph_pie_${System.nanoTime()}"
    def largeId = "dialog_graph_pie_${System.nanoTime()}_container"
    def graphId = "dialog_graph_pie_${System.nanoTime()}_small"
    def largeGraphId = "dialog_graph_pie_${System.nanoTime()}_large"

%>

<g:javascript>
            //flot library based graph setup
            var data = [];

            <g:each in="${data}" var="pair" status="i">
                data[${i}] = {label:'${pair[0]}', data:${pair[1]}};
            </g:each>


     var pieOptions = {
              series: {
                  pie: {
                    show: true,
                     radius: 1,
                    label: {
                        show: true,
                        radius: 2/3,
                        formatter: function(label, series){
                            return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
                        },
                        threshold: 0.1
                    }
                  }
                },
              legend: {
                show: false
              }
            };


 var pieOptionsLarge = {
series: {
			pie: {
				show: true,
				radius: 1
			}
		},
		legend: {
			show: true,
			position: 'ne',
			noColumns:2,
			labelFormatter: function(label, series) {
                return label + '(' + Math.round(series.percent)+'%)';
            },
            container: "#${largeGraphId}_legend"
		}

};


</g:javascript>
<flot:plot id="${graphId}" style="width: 400px; height: 300px;"
           data="data" options="pieOptions"/>

<g:javascript>
        $(document).ready(function() {
            // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
            $("#dialog:ui-dialog").dialog("destroy");

            //dialog for the table data
            $("#${dialogId}").dialog({
                height: 480,
                width: 720,
                modal: true,
                autoOpen: false
            });

            //open the dialog
            $("#${buttonId}").click(function() {
                $("#${dialogId}").dialog('open');
                return false;
            });

            //create a jquery button
            $("#${buttonId}").button();

            //dialog for the larger graph
            $("#${largeId}").dialog({
                modal: true, width:720,height:480,autoOpen: false
            });
            //show the graph
            $('#${graphId}').click(
                    function() {
                        $("#${largeId}").dialog('open');
                    }).hover(
                    //show mouse click cursor on hover
                    function() {
                        $(this).addClass("graph_enter");
                    }, function() {
                        $(this).removeClass("graph_enter");
                    }
            );
        });
</g:javascript>

<%

    if (data instanceof Map) {
        if (data.get("Annotation Count").size() > 0) {
%>


<div id="${dialogId}" title="Data">
    <p>
        <g:render template="table" model="[data:data]" plugin="binbase-web-core"/>
    </p>
</div>


<button id="${buttonId}">display data</button>

<%
        }
    }
    else if (data instanceof Collection) {
%>
<div id="${dialogId}" title="Data">
    <p>
        <g:render template="table" model="[data:data]" plugin="binbase-web-core"/>
    </p>
</div>


<div style="margin-top: 20px;float: right;margin-bottom: 20px">
    <button id="${buttonId}">display data</button>
</div>

<%
    }
%>

<!-- zooming in -->
<div id="${largeId}" title="graph">
    <!-- reset the grails default style for there stupid tables -->
    <style type="text/css">
        table{
            border: none;
        }
    </style>

    <div style="width: 700px;margin: 0 0 0 0;padding: 0 0 0 0">
    <div style="float:left;">
    <flot:plot id="${largeGraphId}" style="width: 400px; height: 400px;"
               data="data" options="pieOptionsLarge"/>
     </div>


    <!-- render our graph -->
    <div id="${largeGraphId}_legend" style="width: 200px;float: left;margin:  0 0 0 0;padding: 0 0 0 0">

    </div>
        </div>
</div>