<g:setProvider library="jquery"/>

<%
    def dialogId = "dialog_graph_line_${System.nanoTime()}_${new Random().nextLong()}"
    def buttonId = "dialog_button_graph_line_${System.nanoTime()}_${new Random().nextLong()}"
    def graphId = "graph_line_${System.nanoTime()}_${new Random().nextLong()}"
    def graphData = data.get(data.keySet().iterator().next())
    def largeId = "dialog_graph_line_${System.nanoTime()}_container_${new Random().nextLong()}"
    def largeGraphId = "dialog_graph_line_${System.nanoTime()}_large_${new Random().nextLong()}"
%>

<%

    def merged = []

    def keys = data.keySet()

    def temp = data.get(keys.iterator().next())

    for (int i = 0; i < labels.size(); i++) {
        def values = [];
        values.add(labels[i])
        values.add(temp[i])
        merged.add(values)
    }
%>

<!-- graphs -->
<g:javascript>

    var data = [];
    var dataLabels = [


    <g:each in="${labels.keySet()}" var="val" status="i">
        [${val},'${labels.get(val)}']
        <g:if test="${i < (labels.size() -1)}">
            ,
        </g:if>
    </g:each>


    ];

    data[0] = {label:'${data.keySet().iterator().next()}', data:[


    <g:each in="${graphData}" var="val" status="i">
        [${i},${val}]
        <g:if test="${i < (graphData.size() -1)}">
            ,
        </g:if>
    </g:each>

    ]};

     var options = {
              legend: {
                show: false
              }                                ,
                                xaxis: { ticks: dataLabels}
            };
</g:javascript>

<flot:plot id="${graphId}" style="width: 600px; height: 200px;"
           data="data" options="options"/>

<!-- dialogs -->
<g:javascript>
        $(document).ready(function() {

            $("#${dialogId}").dialog({
                        height: 480,
                        width: 720,
                        modal: true,
                        autoOpen: false
                    });

            $("#${buttonId}").click(function() {
                $("#${dialogId}").dialog('open');
                return false;
            });

              //dialog for the larger graph
            $("#${largeId}").dialog({
                modal: true, width:720,height:480,autoOpen: false
            });
            //show the graph
            $('#${graphId}').click(
                    function() {
                        $("#${largeId}").dialog('open');
                    }).hover(
                    //show mouse click cursor on hover
                    function() {
                        $(this).addClass("graph_enter");
                    }, function() {
                        $(this).removeClass("graph_enter");
                    }
            );

        });
</g:javascript>

<%

    if (data.size() > 0) {
%>


<div id="${dialogId}" title="Data">
    <p>
        <g:render template="/graph/table" model="[data:merged]" plugin="binbase-web-core"/>
    </p>
</div>


<div style="margin-top: 20px;float: right">
    <button id="${buttonId}">display data</button>
</div>


<g:javascript>
    jQuery(document).ready(function() {
        $("#${buttonId}").button();

    });
</g:javascript>

<%
    }
%>


<!-- zooming in -->
<div id="${largeId}" title="graph">

    <div style="width: 650px">
        <div style="width: 80%;margin-left: 10%;margin-right: 10%;padding: 0 0 0 0">
            <flot:plot id="${largeGraphId}" style="width: 610px; height: 400px;"
                       data="data" options="options"/>
        </div>
    </div>
</div>