package binbase.web.core

/**
 * used to generate graphs from binbase data
 */
class GraphController {

    def speciesGraphService

    def statisticsService

    /**
     * renders a pie chart of the sample distribution over all species
     */
    def ajaxRenderSpeciesSampleGraph = {

        def data = speciesGraphService.sampleCountForSpecies()

        log.debug("data: ${data}")
        render(template: "pieChart", model: [data: data], plugin: "binbase-web-core")
    }

    /**
     * renders a pie chart of the sample distribution over all species
     */
    def ajaxRenderOrgansSampleGraph = {

        def data = speciesGraphService.sampleCountForOrgans()

        log.debug("data: ${data}")
        render(template: "pieChart", model: [data: data], plugin: "binbase-web-core")
    }

    /**
     * renders a pie chart of the sample distribution over all species
     */
    def ajaxRenderSpeciesSampleTable = {

        def data = speciesGraphService.sampleCountForSpecies()

        log.debug("data: ${data}")
        render(template: "table", model: [data: data], plugin: "binbase-web-core")
    }

    /**
     * renders the organs as a piechart for the given species
     */
    def ajaxRenderOrganSampleBySpeciesGraph = {

        def data = speciesGraphService.sampleCountForOrgansForSpecies(params.id)

        log.debug("data: ${data}")
        render(template: "pieChart", model: [data: data], plugin: "binbase-web-core")
    }

    /**
     * renders the organs as a piechart for the given species
     */
    def ajaxRenderOrganSampleBySpeciesTable = {

        def data = speciesGraphService.sampleCountForOrgansForSpecies(params.id)

        log.debug("data: ${data}")
        render(template: "table", model: [data: data], plugin: "binbase-web-core")
    }

    /**
     * displays the distribution of samples by species for this organ
     */
    def ajaxRenderOrganSpeciesSampleGraph = {


        def data = speciesGraphService.sampleCountForSpeciesForOrgan(params.id)

        log.debug("data: ${data}")
        render(template: "pieChart", model: [data: data], plugin: "binbase-web-core")
    }

    def ajaxRenderOrganSpeciesSampleTable = {


        def data = speciesGraphService.sampleCountForSpeciesForOrgan(params.id)

        log.debug("data: ${data}")
        render(template: "table", model: [data: data], plugin: "binbase-web-core")
    }

    /**
     * renders the distribtuiob of experiments by species char
     */
    def ajaxRenderExperimentSpeciesGraph = {

        def data = speciesGraphService.experimentCountBySpecies()

        log.debug("data: ${data}")
        render(template: "pieChart", model: [data: data], plugin: "binbase-web-core")
    }

    /**
     * renders the distribtuiob of experiments by species char
     */
    def ajaxRenderExperimentSpeciesTable = {

        def data = speciesGraphService.experimentCountBySpecies()

        log.debug("data: ${data}")
        render(template: "table", model: [data: data], plugin: "binbase-web-core")
    }

    /**
     * renders the species distrubution for a certain bin as chart
     */
    def ajaxRenderSpeciesByBinGraph = {

        def data = speciesGraphService.annotationCountForSpeciesByBin(params.id, params.relative)


        log.debug("data: ${data}")
        render(template: "sideBarChart", model: [data: data.data, label: data.label], plugin: "binbase-web-core")
    }
    /**
     * renders the species distrubution for a certain bin as chart
     */
    def ajaxRenderSpeciesByBinTable = {

        def data = speciesGraphService.annotationCountForSpeciesByBin(params.id, params.relative)


        log.debug("data: ${data}")
        render(template: "table", model: [data: data.data, label: data.label], plugin: "binbase-web-core")
    }

    /**
     * renders a graph which displays the binbase hit rate distribution for a single experiment
     */
    def ajaxRenderBinHitRateDistributionByExperiment = {

        def data = statisticsService.calculateBinAnnotationDistributionForExperiment(params.id)

        log.debug("data: ${data}")
        render(template: "lineChart", model: [data: data.data, labels: data.labels], plugin: "binbase-web-core")

    }

    /**
     * renders a graph which displays the binbase hit rate distribution for a single experiment
     */
    def ajaxRenderBinHitRateDistributionByExperimentTable = {

        def data = statisticsService.calculateBinAnnotationDistributionForExperiment(params.id)

        log.debug("data: ${data}")
        render(template: "table", model: [data: data.data, labels: data.labels], plugin: "binbase-web-core")

    }
    /**
     * renders a graph which displays the binbase hit rate distribution for a single experiment
     */
    def ajaxRenderAccumulativeBinHitRateDistributionByExperiment = {

        def data = statisticsService.calculateAccumulativeBinAnnotationDistributionForExperiment(params.id)

        log.debug("data: ${data}")
        render(template: "lineChart", model: [data: data.data, labels: data.labels], plugin: "binbase-web-core")

    }
    /**
     * renders a graph which displays the binbase hit rate distribution for a single experiment
     */
    def ajaxRenderAccumulativeBinHitRateDistributionByExperimentTable = {

        def data = statisticsService.calculateAccumulativeBinAnnotationDistributionForExperiment(params.id)

        log.debug("data: ${data}")
        render(template: "table", model: [data: data.data, labels: data.labels], plugin: "binbase-web-core")

    }
}
