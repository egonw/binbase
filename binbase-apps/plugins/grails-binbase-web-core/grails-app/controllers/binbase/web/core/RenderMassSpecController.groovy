package binbase.web.core

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion
import org.jfree.chart.axis.ValueAxis
import java.awt.Color
import org.jfree.chart.JFreeChart
import org.jfree.chart.encoders.EncoderUtil
import edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.plot.SpectraChart
import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.ValidateSpectra

class RenderMassSpecController {

    def index = {
        render "sorry no access to this method!"
    }

    //default method, we only need this one
    def renderBin = {

        //get the current id
        def id = params.id
        def width = params.width ?: 400
        def height = params.height ?: 320
        def apexOnly = params.apexOnly ?: false

        log.info "rendering bin: ${id}"
        BBBin bin = BBBin.get(id)

        if (apexOnly) {
            EncoderUtil.writeBufferedImage(createChart(convertApexToSpectra(bin.spectraString,bin.apexMasses)).createBufferedImage(Integer.parseInt(width.toString()), Integer.parseInt(height.toString())), "png", response.getOutputStream())

        }
        else {
            //renders the graph
            EncoderUtil.writeBufferedImage(createChart(bin.spectraString).createBufferedImage(Integer.parseInt(width.toString()), Integer.parseInt(height.toString())), "png", response.getOutputStream())
        }
        return null
    }

    /**
     * converts the apex masses to a spectra
     * @param spectra
     * @param apex
     * @return
     */
    private String convertApexToSpectra(String spectra, String apex){

        def spec = ValidateSpectra.convert(spectra)

        StringBuffer buffer = new StringBuffer()

        apex.split('\\+').collect {Integer.parseInt(it)}.sort().each {def it ->

            def ion = spec[it-1][ValidateSpectra.FRAGMENT_ION_POSITION]
            def intensity = spec[it-1][ValidateSpectra.FRAGMENT_ABS_POSITION]

            buffer.append("${(int)ion}:${(int)intensity} ")
        }

        return buffer.toString().trim()

    }

    //default method, we only need this one
    def renderSpectra = {

        //get the current id
        def id = params.id
        def width = params.width ?: 400
        def height = params.height ?: 320
        def apexOnly = params.apexOnly ?: false



        log.info "rendering spectra: ${id}"
        BBSpectra bin = BBSpectra.get(id)

        //renders the graph
        if (apexOnly) {
            EncoderUtil.writeBufferedImage(createChart(convertApexToSpectra(bin.spectraString,bin.apexMasses)).createBufferedImage(Integer.parseInt(width.toString()), Integer.parseInt(height.toString())), "png", response.getOutputStream())

        }
        else {
            EncoderUtil.writeBufferedImage(createChart(bin.spectraString).createBufferedImage(Integer.parseInt(width.toString()), Integer.parseInt(height.toString())), "png", response.getOutputStream())
        }
        return null
    }

    def ajaxRenderSpectra = {
        render """<img src="${createLink(id: "${params.id}", controller: "renderMassSpec", action: "renderSpectra", plugin: 'binbase-web-core', params: [height: params.height, width: params.width,apexOnly:params.apexOnly])}"/>"""

    }

    def ajaxRenderBin = {
        render """<img src="${createLink(id: "${params.id}", controller: "renderMassSpec", action: "renderBin", plugin: 'binbase-web-core', params: [height: params.height, width: params.width,apexOnly:params.apexOnly])}"/>"""
    }

    /**
     * creates a new chart
     * @param spectra
     * @param height
     * @param width
     * @return
     */
    JFreeChart createChart(String spectra) {

        // load the data into a dataset
        BinBaseXYDataSet dataset = new BinBaseXYDataSet()

        //get the spectra from the parameters
        String spec = spectra.trim()
        Collection<Ion> c = SpectraConverter.stringToCollection(spec)

        //parse the string
        double[] masses = new double[c.size()]
        double[] intensities = new double[c.size()]

        int maxIon = 0;
        int counter = 0;
        for (Ion o: c) {
            masses[counter] = o.getMass()
            intensities[counter] = o.getRelativeIntensity()
            counter++

            //just to set the scale to a smart value
            if (o.getMass() > maxIon) {
                if (o.getRelativeIntensity() > 0.5) {
                    maxIon = o.getMass()
                }
            }
        }

        //add our created dataset
        dataset.addDataSet(masses, intensities, "spec")

        //the chart it self
        JFreeChart chart = SpectraChart.createChart(dataset)
        chart.removeLegend()

        //sets the background color
        chart.setBackgroundPaint(Color.white)

        //disable the stupid shadows...
        chart.getPlot().getRenderer().setShadowVisible(false)

        //set the axis to 110%
        ValueAxis axis = chart.getXYPlot().getRangeAxis();
        axis.setRange(0, 110);

        //set the x axis to 35 - x
        axis = chart.getXYPlot().getDomainAxis();
        axis.setRange(30, maxIon + 5);

        return chart;
    }
}
