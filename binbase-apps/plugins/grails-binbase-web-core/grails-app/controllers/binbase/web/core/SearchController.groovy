package binbase.web.core

class SearchController {

    def searchableService

    def index = { }

    /**
     * simple searchmethod
     */
    def search = {

        def mappedResult = [:]
        def result = []


        if (params.query != null && params.query.toString().length() > 0) {
            result = searchableService.search(params.query,params).results
        }
        else {
            flash.message = "you need to provide a query!"
            result = []
        }

        /**
         * mapping the result to a usable collection
         */
        if (result.size() > 0) {

            flash.message = "query: ${params.query}"

            result.each {def value ->

                if (value instanceof BBBin) {
                    if (mappedResult.bin == null) {
                        mappedResult.bin = new HashSet()
                    }

                    mappedResult.bin.add(value)

                }
                else if (value instanceof BBSpectra) {

                    if (mappedResult.spectra == null) {
                        mappedResult.spectra = new HashSet()
                    }

                    mappedResult.spectra.add(value)
                }
                else if (value instanceof BBExperimentSample) {

                    if (mappedResult.sample == null) {
                        mappedResult.sample = new HashSet()
                    }

                    mappedResult.sample.add(value)
                }
                else if (value instanceof BBExperimentClass) {

                    if (mappedResult.clazz == null) {
                        mappedResult.clazz = new HashSet()
                    }

                    mappedResult.clazz.add(value)
                }
                else if (value instanceof BBExperiment) {

                    if (mappedResult.experiment == null) {
                        mappedResult.experiment = new HashSet()
                    }

                    mappedResult.experiment.add(value)
                }
                else if (value instanceof BBOrgan) {

                    if (mappedResult.organ == null) {
                        mappedResult.organ = new HashSet()
                    }

                    mappedResult.organ.add(value)
                }
                else if (value instanceof BBSpecies) {

                    if (mappedResult.species == null) {
                        mappedResult.species = new HashSet()
                    }

                    mappedResult.species.add(value)
                }


                else {

                    if (mappedResult.other == null) {
                        mappedResult.other = new HashSet()
                    }

                    mappedResult.other.add(value)
                }

            }

        }

        [results: mappedResult]
    }
}
