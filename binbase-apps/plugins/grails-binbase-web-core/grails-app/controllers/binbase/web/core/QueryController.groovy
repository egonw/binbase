package binbase.web.core

import core.QueryService
import core.SimilarityService

/**
 * used for all of our internal queries
 */
class QueryController {

    QueryService queryService

    SimilarityService similarityService

    /**
     * renders all the experiments for this species as a table
     */
    def ajaxRenderExperimentsForSpecies = {

        if(params.max == null)
            params.max = 10

        def data = queryService.experimentsForSpecies(params.id, params)
        def total = queryService.experimentsForSpecies(params.id).size()

        render(template: "ajaxRenderExperimentsForSpecies", model: [speciesId: params.id, BBExperimentInstanceList: data, total: total], plugin: "binbase-web-core")
    }

    /**
     * renders all the experiments for this species as a table
     */
    def ajaxRenderExperimentsForSpeciesAndOrgan = {

        if(params.max == null)
            params.max = 10

        BBOrgan organ = BBOrgan.get(params.organId)

        def data = queryService.experimentsForSpeciesAndOrgan(params.id, organ.id, params)
        def total = queryService.experimentsForSpeciesAndOrgan(params.id).size()

        render(template: "ajaxRenderExperimentsForSpeciesAndOrgan", model: [organId:params.organId,speciesId: params.id, BBExperimentInstanceList: data, total: total], plugin: "binbase-web-core")
    }

    /**
     * renders all the experimetns for this species and organ
     */
    def ajaxRenderExperimentsForOrgan = {


        if(params.max == null)
            params.max = 10

        def data = queryService.experimentsForOrgan(params.id, params)
        def total = queryService.experimentsForOrgan(params.id).size()

        render(template: "ajaxRenderExperimentsForOrgan", model: [organId: params.id, BBExperimentInstanceList: data, total: total], plugin: "binbase-web-core")

    }

    /**
     * returns the count of samples for this species
     */
    def ajaxRetrieveSampleCountForSpecies = {
        def data = queryService.retrieveSampleCountForSpecies(params.id)

        render data
    }

    /**
     * returns the count of samples for this species
     */
    def ajaxRetrieveSampleCountForOrgan = {
        def data = queryService.retrieveSampleCountForOrgan(params.id)

        render data
    }

    /**
     * renders the result table for the experiment
     */
    def ajaxRenderResultsForExperiment = {

        if(params.max == null)
            params.max = 10

        def data = queryService.resultsForExperiment(params.id, params)

        render(template: "ajaxRenderResultsForExperiment", model: [organId: params.id, BBResultInstanceList: data, expId: params.id], plugin: "binbase-web-core")

    }

    /**
     * fetches all spectra for this sample and renders the related template
     */
    def ajaxRenderSpectraForSample = {
        BBExperimentSample sample = BBExperimentSample.get(params.id)

        render(template: "ajaxRenderSpectraForSample", model: [sampleId: params.id, BBSpectraInstanceList: sample.spectra], plugin: "binbase-web-core")
    }

    /**
     * renders the similar bins to this bins
     */
    def ajaxSimilarBins = {
        BBBin bin = BBBin.load(params.id)

        def similarities = BBBinSimilarity.findAllByFrom(bin, [max: 10, sort: "similarity", order: "desc"])

        if (similarities.size() == 0) {
            //calculate them on the fly...
            similarityService.calculateSimilaritiesFor(bin, 800)
            similarities = BBBinSimilarity.findAllByFrom(bin, [max: 10, sort: "similarity", order: "desc"])
        }
        render(template: "ajaxSimilarBins", model: [similarities: similarities], plugin: "binbase-web-core")
    }

    /**
     * compares 2 or more bins
     */
    def compareBins = {
        def bins = []

        params.list("compare").each {
            bins.add(BBBinSimilarity.load(it.toString().toLong()))
        }

        return [binSimilarityInstances: bins]
    }

    /**
     * bins for the given organ
     */
    def ajaxBinsForOrgan = {

        if(params.max == null)
            params.max = 10

        def data = queryService.binsForOrgan(params.organId, params)
        def total = queryService.binsForOrgan(params.organId).size()

        render(template: "ajaxBinsByOrgan", model: [bins: data, organId: params.organId, total: total], plugin: "binbase-web-core")
    }

    /**
     * bins for the given species
     */
    def ajaxBinsForSpecies = {

        if(params.max == null)
            params.max = 10

        def data = queryService.binsForSpecies(params.speciesId, params)
        def total = queryService.binsForSpecies(params.speciesId).size()

        render(template: "ajaxBinsBySpecies", model: [bins: data, speciesId: params.speciesId, total: total], plugin: "binbase-web-core")
    }

    /**
     * bins for the given species
     */
    def ajaxBinsForSpeciesAndOrgan = {

        if(params.max == null)
            params.max = 10

        def data = queryService.binsForOrganAndSpecies(params.organId, params.speciesId, params)
        def total = queryService.binsForOrganAndSpecies(params.organId, params.speciesId).size()

        render(template: "ajaxBinsByOrganAndSpecies", model: [bins: data, organId: params.organId, speciesId: params.speciesId, total: total], plugin: "binbase-web-core")
    }

}
