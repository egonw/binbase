package binbase.web.core

class BBOrganController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }
    def scaffold = true


    /**
     * displays the organ and if provided limit it by organs
     */
    def show = {
        BBSpecies species = BBSpecies.get(params.speciesId)

        BBOrgan organ = BBOrgan.get(params.id)

        if(organ == null){
            flash.message = "sorry we did not found this object, it might had been in the cache, which could had been outdated so we are updating the cache now."

            //updating the cache to remove the oudated information
            BBOrgan.reindex()

            redirect(action: "list",controller: "BBOrgan")
        }
        [BBSpeciesInstance: species, BBOrganInstance: organ]
    }
}
