package binbase.web.core

class BBExperimentSampleController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }
    def scaffold = true
}
