package binbase.web.core

/**
 * renders some standard binbase tables with remote pagination
 */
class TableTagLib {

    static namespace = 'binbase'

    /**
     * display the table of experiments for the given species and can also be limited by the organ selection
     */
    def tableExperimentsForSpecies = { attrs, body ->

        def speciesId = attrs.speciesId
        def id = "table_experiments_for_species_${speciesId}_${System.nanoTime()}"

        out << """


                                <div id="${id}">
                                <img src="${resource(dir: 'images', file: 'spinner.gif')}"
             alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                </div>
                                <script type="text/javascript">

                    jQuery(document).ready(function() {

                            \$('#${id}').load('${createLink(id: "${speciesId}", controller: "query", action: "ajaxRenderExperimentsForSpecies", plugin: 'binbase-web-core')}');

                               });
                                </script>
                                """
    }

    /**
     * display the table of experiments for the given species and can also be limited by the organ selection
     */
    def tableExperimentsForSpeciesAndOrgan = { attrs, body ->

        def speciesId = attrs.speciesId
        def id = "table_experiments_for_species_${speciesId}_${System.nanoTime()}"

        out << """


                                <div id="${id}">
                                <img src="${resource(dir: 'images', file: 'spinner.gif')}"
             alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                </div>
                                <script type="text/javascript">

                    jQuery(document).ready(function() {

                            \$('#${id}').load('${createLink(id: "${speciesId}", controller: "query", action: "ajaxRenderExperimentsForSpecies", plugin: 'binbase-web-core', params: [organId: attrs.organId])}');

                               });
                                </script>
                                """
    }

    /**
     * display the table of experiments for the given species
     */
    def tableExperimentsForOrgan = { attrs, body ->

        def organId = attrs.organId
        def id = "table_experiments_for_organ_${organId}_${System.nanoTime()}"

        out << """


                            <div id="${id}">
                             <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                            <script type="text/javascript">

                jQuery(document).ready(function() {

                        \$('#${id}').load('${createLink(id: "${organId}", controller: "query", action: "ajaxRenderExperimentsForOrgan", plugin: 'binbase-web-core')}');

                           });
                            </script>
                            """
    }

    /**
     * display all the results for an experiment
     */
    def tableResultsForExperiment = { attrs, body ->

        def experimentId = attrs.experimentId

        def id = "table_results_for_${experimentId}_${System.nanoTime()}"

        out << """


                            <div id="${id}">
                             <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                            <script type="text/javascript">

                jQuery(document).ready(function() {

                        \$('#${id}').load('${createLink(params:[order:'desc', sort:'uploadDate',max:attrs.count ? attrs.count : 10],id: "${experimentId}", controller: "query", action: "ajaxRenderResultsForExperiment", plugin: 'binbase-web-core')}');

                           });
                            </script>
                            """
    }

    /**
     * renders a table with all the spectra for the sample
     */
    def tableSpectraForSample = { attrs, body ->

        def sampleId = attrs.sampleId

        def id = "table_spectra_for_${sampleId}_${System.nanoTime()}"

        out << """


                            <div id="${id}">
                             <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                            <script type="text/javascript">

                jQuery(document).ready(function() {

                        \$('#${id}').load('${createLink(id: "${sampleId}", controller: "query", action: "ajaxRenderSpectraForSample", plugin: 'binbase-web-core')}');

                           });
                            </script>
                            """
    }

    /**
     * rendes a pie chart of the sample by species distribution
     */
    def tableSampleBySpeciesDistribution = {

        def id = "table_sample_by_species_distribution_${System.nanoTime()}"
        out << """


                    <div id="${id}">
                    <img src="${resource(dir: 'images', file: 'spinner.gif')}"
             alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                    </div>
                            <script type="text/javascript">

                jQuery(document).ready(function() {

                \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderSpeciesSampleTable", plugin: 'binbase-web-core')}');
                           });

                            </script>

                    """

    }

    /**
     * renders a pie chart of the sample by organ distribution for a given species
     */
    def tableSampleByOrganForSpeciesDistribtution = { attrs, body ->

        def id = "table_sample_by_organ_for_species_distribution_${System.nanoTime()}"
        out << """


                            <div id="${id}">
                            <img src="${resource(dir: 'images', file: 'spinner.gif')}"
             alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                            <script type="text/javascript">

                jQuery(document).ready(function() {

                        \$('#${id}').load('${createLink(id: "${attrs.speciesId}", controller: "graph", action: "ajaxRenderOrganSampleBySpeciesTable", plugin: 'binbase-web-core')}');

                           });
                            </script>
                            """
    }

    /**
     * renders a pie chart for sample count by species for a given organ
     */
    def tableSampleBySpeciesForOrganDistribution = { attrs, body ->

        def id = "table_sample_by_species_for_organ_distribution_${System.nanoTime()}"
        out << """


                            <div id="${id}">
                             <img src="${resource(dir: 'images', file: 'spinner.gif')}"
             alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                            <script type="text/javascript">

                jQuery(document).ready(function() {

                        \$('#${id}').load('${createLink(id: "${attrs.organId}", controller: "graph", action: "ajaxRenderOrganSpeciesSampleTable", plugin: 'binbase-web-core')}');

                           });
                            </script>
                            """

    }

    /**
     * renders a pie chart for the experiment by species distribution
     */
    def tableExperimentByOrganDistribution = { attrs, body ->

        def id = "table_experiment_by_species_distribution_${System.nanoTime()}"
        out << """

                            <div id="${id}">
                                                         <img src="${resource(dir: 'images', file: 'spinner.gif')}"
             alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                            </div>

                            <script type="text/javascript">

                jQuery(document).ready(function() {

                        \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderExperimentSpeciesTable", plugin: 'binbase-web-core')}');

                           });
                            </script>
                            """

    }

    /**
     * renders the bin annotation distribution rate by experiment
     * in form of a line chart
     */
    def binHitRateDistributionByExperimentTable = { attrs, body ->

        def id = "table_annotation_rate_by_experiment_${attrs.experimentId}_${System.nanoTime()}"
        out << """

                                    <div id="${id}">
                                                                 <img src="${resource(dir: 'images', file: 'spinner.gif')}"
             alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                                    </div>

                                    <script type="text/javascript">

                        jQuery(document).ready(function() {

                                \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderBinHitRateDistributionByExperimentTable", id: attrs.experimentId, plugin: 'binbase-web-core')}');

                                   });
                                    </script>
                                    """


    }

    /**
     * the accumulative annoation of bins for this experiment
     */
    def accumulativeBinAnnotationCountByExperimentTable = { attrs, body ->

        def id = "table_annotation_rate_by_experiment_${attrs.experimentId}_${System.nanoTime()}"
        out << """

                                    <div id="${id}">
                                                                 <img src="${resource(dir: 'images', file: 'spinner.gif')}"
             alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                                    </div>

                                    <script type="text/javascript">

                        jQuery(document).ready(function() {

                                \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderAccumulativeBinHitRateDistributionByExperimentTable", id: attrs.experimentId, plugin: 'binbase-web-core')}');

                                   });
                                    </script>
                                    """


    }

    def similarBinsTable = { attrs, body ->


        def id = "table_similar_bins${attrs.binId}_${System.nanoTime()}"
        out << """

                                            <div id="${id}">
                                                                         <img src="${resource(dir: 'images', file: 'spinner.gif')}"
             alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                                            </div>

                                            <script type="text/javascript">

                                jQuery(document).ready(function() {

                                        \$('#${id}').load('${createLink(controller: "query", action: "ajaxSimilarBins", id: attrs.binId, plugin: 'binbase-web-core')}');

                                           });
                                            </script>
                                            """


    }

    /**
     * all bins for this species as table
     */
    def tableBinsForSpecies = { attrs, body ->
        def id = "table_bins_for_species${attrs.binId}_${System.nanoTime()}"
        out << """

                                                  <div id="${id}">
                                                                               <img src="${resource(dir: 'images', file: 'spinner.gif')}"
                   alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                  </div>
                                                  </div>

                                                  <script type="text/javascript">

                                      jQuery(document).ready(function() {

                                              \$('#${id}').load('${createLink(controller: "query", action: "ajaxBinsForSpecies", params: [speciesId: attrs.speciesId], plugin: 'binbase-web-core')}');

                                                 });
                                                  </script>
                                                  """

    }

    /**
     * all bins for this organ as table
     */
    def tableBinsForOrgan = { attrs, body ->
        def id = "table_bins_for_organ${attrs.binId}_${System.nanoTime()}"
        out << """

                                                  <div id="${id}">
                                                                               <img src="${resource(dir: 'images', file: 'spinner.gif')}"
                   alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                  </div>
                                                  </div>

                                                  <script type="text/javascript">

                                      jQuery(document).ready(function() {

                                              \$('#${id}').load('${createLink(controller: "query", action: "ajaxBinsForOrgan", params: [organId: attrs.organId], plugin: 'binbase-web-core')}');

                                                 });
                                                  </script>
                                                  """

    }

    /**
     * all bins for this organ as table
     */
    def tableBinsForOrganAndSpecies = { attrs, body ->
        def id = "table_bins_for_organ${attrs.binId}_${System.nanoTime()}"
        out << """

                                                      <div id="${id}">
                                                                                   <img src="${resource(dir: 'images', file: 'spinner.gif')}"
                       alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                      </div>
                                                      </div>

                                                      <script type="text/javascript">

                                          jQuery(document).ready(function() {

                                                  \$('#${id}').load('${createLink(controller: "query", action: "ajaxBinsForSpeciesAndOrgan", params: [organId: attrs.organId, speciesId: attrs.speciesId], plugin: 'binbase-web-core')}');

                                                     });
                                                      </script>
                                                      """

    }

}
