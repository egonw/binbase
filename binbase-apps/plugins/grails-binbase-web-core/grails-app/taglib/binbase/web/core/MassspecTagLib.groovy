package binbase.web.core

/**
 * renders a massspec on the fly
 */
class MassspecTagLib {

    static namespace = 'binbase'

    def grailsApplication

    def renderBin = { attrs, body ->

        if (attrs.largeWidth == null) {
            attrs.largeWidth = 600
        }
        if (attrs.largeHeight == null) {
            attrs.largeHeight = 500
        }


        def id = "spectra_graph_${attrs.id}_${System.currentTimeMillis()}"
        out << """


                        <div id="${id}">
                        <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                        </div>
                        <script type="text/javascript">

            jQuery(document).ready(function() {

                    \$('#${id}').load('${createLink(id: "${attrs.id}", controller: "renderMassSpec", action: "ajaxRenderBin", plugin: 'binbase-web-core', params: [width: attrs.width, height: attrs.height,apexOnly:attrs.apexOnly])}');

                       });
                        </script>

                        <div id="${id}_large_dialog" title="massspec">
                            <div id="${id}_large">
                                <img src="${resource(dir: 'images', file: 'spinner.gif')}"
                                    alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                        </div>

                        <script type="text/javascript">

                        jQuery(document).ready(function() {

                                \$('#${id}_large').load('${createLink(id: "${attrs.id}", controller: "renderMassSpec", action: "ajaxRenderBin", plugin: 'binbase-web-core', params: [width: attrs.largeWidth, height: attrs.largeHeight,apexOnly:attrs.apexOnly])}');

                                \$( "#dialog:ui-dialog" ).dialog( "destroy" );

                                \$( "#${id}_large_dialog" ).dialog({
			                        modal: true, width:${attrs.largeWidth + 30},height:${attrs.largeHeight + 50},autoOpen: false
    		                    });

                                //show the massspec in large
                                \$('#${id}').click(function() {
                                    \$( "#${id}_large_dialog" ).dialog('open');
                                }).hover(
                                    //show mouse click cursor
                                    function() {
                                        \$(this).addClass("graph_enter");
                                    },function(){
                                        \$(this).removeClass("graph_enter");
                                    }
                                );
                       });
                        </script>

                        """
    }

    def renderSpectra = { attrs, body ->

        if (attrs.largeWidth == null) {
            attrs.largeWidth = 600
        }
        if (attrs.largeHeight == null) {
            attrs.largeHeight = 500
        }

        def id = "spectra_graph_${attrs.id}_${System.currentTimeMillis()}"
        out << """


                        <div id="${id}">
                        <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                        </div>
                        <script type="text/javascript">

            jQuery(document).ready(function() {

                    \$('#${id}').load('${createLink(id: "${attrs.id}", controller: "renderMassSpec", action: "ajaxRenderSpectra", plugin: 'binbase-web-core', params: [width: attrs.width, height: attrs.height,apexOnly:attrs.apexOnly])}');

                       });
                        </script>


                        <div id="${id}_large_dialog" title="massspec">
                            <div id="${id}_large">
                                <img src="${resource(dir: 'images', file: 'spinner.gif')}"
                                    alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                            </div>
                        </div>

                        <script type="text/javascript">

                        jQuery(document).ready(function() {

                                \$('#${id}_large').load('${createLink(id: "${attrs.id}", controller: "renderMassSpec", action: "ajaxRenderSpectra", plugin: 'binbase-web-core', params: [width: attrs.largeWidth, height: attrs.largeHeight,apexOnly:attrs.apexOnly])}');

                                \$( "#dialog:ui-dialog" ).dialog( "destroy" );

                                \$( "#${id}_large_dialog" ).dialog({
			                        modal: true, width:${attrs.largeWidth + 30},height:${attrs.largeHeight + 50},autoOpen: false
    		                    });

                                //show the massspec in large
                                \$('#${id}').click(function() {
                                    \$( "#${id}_large_dialog" ).dialog('open');
                                }).hover(
                                    //show mouse click cursor
                                    function() {
                                        \$(this).addClass("graph_enter");
                                    },function(){
                                        \$(this).removeClass("graph_enter");
                                    }
                                );
                       });
                        </script>

                        """
    }


}
