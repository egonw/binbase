package binbase.web.core

/**
 * a simple graph tag lib to provide pre generated graphs for you to use
 * internally it generated javascript request to load the data in the background
 */
class GraphTagLib {

    static namespace = 'binbase'

    /**
     * rendes a pie chart of the sample by species distribution
     */
    def pieSampleBySpeciesDistribution = {

        def id = "pie_sample_by_species_distribution_${System.currentTimeMillis()}"
        out << """


                <div id="${id}">
                <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                </div>
                        <script type="text/javascript">

            jQuery(document).ready(function() {

            \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderSpeciesSampleGraph", plugin: 'binbase-web-core')}');
                       });

                        </script>

                """

    }

    /**
     * rendes a pie chart of the sample by species distribution
     */
    def pieSampleByOrganDistribution = {

        def id = "pie_sample_by_organ_distribution_${System.currentTimeMillis()}"
        out << """


                <div id="${id}">
                <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                </div>
                        <script type="text/javascript">

            jQuery(document).ready(function() {

            \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderOrgansSampleGraph", plugin: 'binbase-web-core')}');
                       });

                        </script>

                """

    }
    /**
     * renders a pie chart of the sample by organ distribution for a given species
     */
    def pieSampleByOrganForSpeciesDistribtution = { attrs, body ->

        def id = "pie_sample_by_organ_for_species_distribution_${System.currentTimeMillis()}"
        out << """


                        <div id="${id}">
                        <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                        </div>
                        <script type="text/javascript">

            jQuery(document).ready(function() {

                    \$('#${id}').load('${createLink(id: "${attrs.speciesId}", controller: "graph", action: "ajaxRenderOrganSampleBySpeciesGraph", plugin: 'binbase-web-core')}');

                       });
                        </script>
                        """
    }

    /**
     * renders a pie chart for sample count by species for a given organ
     */
    def pieSampleBySpeciesForOrganDistribution = { attrs, body ->

        def id = "pie_sample_by_species_for_organ_distribution_${System.currentTimeMillis()}"
        out << """


                        <div id="${id}">
                         <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                        </div>
                        <script type="text/javascript">

            jQuery(document).ready(function() {

                    \$('#${id}').load('${createLink(id: "${attrs.organId}", controller: "graph", action: "ajaxRenderOrganSpeciesSampleGraph", plugin: 'binbase-web-core')}');

                       });
                        </script>
                        """

    }

    /**
     * renders a pie chart for the experiment by species distribution
     */
    def pieExperimentByOrganDistribution = { attrs, body ->

        def id = "pie_experiment_by_species_distribution_${System.currentTimeMillis()}"
        out << """

                        <div id="${id}">
                                                  <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                        </div>

                        <script type="text/javascript">

            jQuery(document).ready(function() {

                    \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderExperimentSpeciesGraph", plugin: 'binbase-web-core')}');

                       });
                        </script>
                        """

    }

    /**
     * renders the associated species to a bin
     */
    def sideBarSpeciesByBin = { attrs, body ->

        def id = "side_bar_species_by_bib_distribution_${attrs.id}_${System.currentTimeMillis()}"
        out << """

                                <div id="${id}">
                                                          <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                </div>

                                <script type="text/javascript">

                    jQuery(document).ready(function() {

                            \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderSpeciesByBinGraph", id: attrs.id, params: [relative: attrs.relative], plugin: 'binbase-web-core')}');

                               });
                                </script>
                                """


    }

    /**
     * renders the bin annotation distribution rate by experiment
     * in form of a line chart
     */
    def binHitRateDistributionByExperiment = { attrs, body ->

        def id = "line_bin_annotation_rate_by_experiment_${attrs.experimentId}_${System.currentTimeMillis()}"
        out << """

                                <div id="${id}">
                                                         <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                </div>

                                <script type="text/javascript">

                    jQuery(document).ready(function() {

                            \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderBinHitRateDistributionByExperiment", id: attrs.experimentId, plugin: 'binbase-web-core')}');

                               });
                                </script>
                                """


    }

    /**
     * the accumulative annoation of bins for this experiment
     */
    def accumulativeBinAnnotationCountByExperiment = { attrs, body ->

        def id = "line_bin_annotation_rate_by_experiment_${attrs.experimentId}_${System.currentTimeMillis()}"
        out << """

                                <div id="${id}">
                                                         <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                </div>

                                <script type="text/javascript">

                    jQuery(document).ready(function() {

                            \$('#${id}').load('${createLink(controller: "graph", action: "ajaxRenderAccumulativeBinHitRateDistributionByExperiment", id: attrs.experimentId, plugin: 'binbase-web-core')}');

                               });
                                </script>
                                """


    }

    def lineChartObjectsForDayByHour = { attrs, body ->

        def date = new Date()

        if (attrs.date) {
            date - attrs.date
        }
        def id = "lineChartObjectsForDayByHour_${attrs.object}_${System.currentTimeMillis()}"
        out << """

                                <div id="${id}">
                                                          <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                </div>

                                <script type="text/javascript">

                    jQuery(document).ready(function() {

                            \$('#${id}').load('${createLink(controller: "BBStatisticSnapshot", action: "renderGraphForDayByHour", params: [object: attrs.object], plugin: 'binbase-web-core')}');

                               });
                                </script>
                                """


    }
    def lineChartObjectsForWeekByDay = { attrs, body ->

        def id = "lineChartObjectsForWeekByDay_${attrs.object}_${System.currentTimeMillis()}"
        out << """

                                <div id="${id}">
                                                         <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                </div>

                                <script type="text/javascript">

                    jQuery(document).ready(function() {

                            \$('#${id}').load('${createLink(controller: "BBStatisticSnapshot", action: "renderGraphForWeekByDay", params: [object: attrs.object], plugin: 'binbase-web-core')}');

                               });
                                </script>
                                """
    }
    def lineChartObjectsForMonthByWeek = {  attrs, body ->

        def id = "renderGraphForMonthByWeek_${attrs.object}_${System.currentTimeMillis()}"
        out << """

                                <div id="${id}">
                                                         <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                </div>

                                <script type="text/javascript">

                    jQuery(document).ready(function() {

                            \$('#${id}').load('${createLink(controller: "BBStatisticSnapshot", action: "renderGraphForMonthByWeek", params: [object: attrs.object], plugin: 'binbase-web-core')}');

                               });
                                </script>
                                """

    }
    def lineChartObjectsForYearByMonth = {   attrs, body ->

        def id = "renderGraphForYearByMonth_${attrs.object}_${System.currentTimeMillis()}"
        out << """

                                <div id="${id}">
                                                          <img src="${resource(dir: 'images', file: 'spinner.gif')}"
         alt="${message(code: 'spinner.alt', default: 'Loading...')}"/>
                                </div>

                                <script type="text/javascript">

                    jQuery(document).ready(function() {

                            \$('#${id}').load('${createLink(controller: "BBStatisticSnapshot", action: "renderGraphForYearByMonth", params: [object: attrs.object], plugin: 'binbase-web-core')}');

                               });
                                </script>
                                """
    }


}
