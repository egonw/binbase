/*
 * Created on Sep 26, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.date;

import junit.framework.TestCase;

import java.sql.Date;
import java.sql.Timestamp;


/**
 * DOCUMENT ME!
 *
 * @author $author$
 * @version $Revision: 1.4 $
 */
public class SampleDateTest extends TestCase {
    /**
     * DOCUMENT ME!
     *
     * @param args DOCUMENT ME!
     */
    public static void main(String[] args) {
        junit.textui.TestRunner.run(SampleDateTest.class);
    }

    /*
     * Test method for 'edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.SampleDate.createInstance(String)'
     */
    public void testCreateInstance() {
        SampleDate mpi = SampleDate.createInstance("2229ea44_1");
        SampleDate uc = SampleDate.createInstance("050812akbq4_1");
        SampleDate uc1 = SampleDate.createInstance("050812akbq14_1");
        SampleDate uc2 = SampleDate.createInstance("050812akbq04_1");
        SampleDate uc3 = SampleDate.createInstance("050812AKbq04_1");
        SampleDate uc4 = SampleDate.createInstance("050812aKbq04_1");
        SampleDate uc5 = SampleDate.createInstance("050812aKbsa04_1");
        SampleDate uc6 = SampleDate.createInstance("050812aKbs04_1");
        SampleDate uc7 = SampleDate.createInstance("050812sa14_1");
        SampleDate uc8 = SampleDate.createInstance("050812aKbsa0499_1");
        SampleDate uc9 = SampleDate.createInstance("050812sa1499_1");
        SampleDate uc10 = SampleDate.createInstance("050812aKbs0499_1");

        assertNotNull(mpi);
        assertNotNull(uc);
        assertNotNull(uc1);
        assertNotNull(uc2);
        assertNotNull(uc3);
        assertNotNull(uc4);
        assertNotNull(uc5);
        assertNotNull(uc6);
        assertNotNull(uc7);

        assertTrue(uc instanceof UCSampleDate);
        assertTrue(uc1 instanceof UCSampleDate);
        assertTrue(uc2 instanceof UCSampleDate);
        assertTrue(uc3 instanceof UCSampleDate);
        assertTrue(uc4 instanceof UCSampleDate);
        assertTrue(uc5 instanceof UCSampleDate);
        assertTrue(uc6 instanceof UCSampleDate);
        assertTrue(uc7 instanceof UCSampleDate);
        assertTrue(uc8 instanceof UCSampleDate);
        assertTrue(uc9 instanceof UCSampleDate);
        assertTrue(uc10 instanceof UCSampleDate);

        assertTrue(mpi instanceof MPISampleDate);
        
        
        assertTrue(mpi.getMachine().equals("e"));
        assertTrue(mpi.getOperator().equals("a"));
        assertTrue(mpi.getRunNumber()==1);
        
    }

    /**
     * DOCUMENT ME!
     */
    public void testGetDateAsSQL() {
        SampleDate mpi = SampleDate.createInstance("2229ea44_1");
        SampleDate uc = SampleDate.createInstance("050812akbq4_1");

        Timestamp sqlMPI = mpi.getDateAsSQL();
        Timestamp sqlUC = uc.getDateAsSQL();

        assertNotNull(sqlMPI);
        assertNotNull(sqlUC);

        assertTrue(sqlUC.after(sqlMPI));
        assertTrue(sqlMPI.before(sqlUC));

        assertTrue(sqlMPI.toString().equals("2002-08-17"));
        assertTrue(sqlUC.toString().equals("2005-08-12"));

        SampleDate date = SampleDate.createInstance("050812akbq1_1");
        assertTrue(date.getDateAsSQL().toString().equals("2005-08-12"));

        date = SampleDate.createInstance("050812akbq12_1");
        assertTrue(date.getDateAsSQL().toString().equals("2005-08-12"));

        date = SampleDate.createInstance("050812akbq14_1");
        assertTrue(date.getDateAsSQL().toString().equals("2005-08-12"));

        date = SampleDate.createInstance("050812akbsa14_1");
        assertTrue(date.getDateAsSQL().toString().equals("2005-08-12"));

        date = SampleDate.createInstance("050812akbs14_1");
        assertTrue(date.getDateAsSQL().toString().equals("2005-08-12"));

        date = SampleDate.createInstance("050812sa14_1");
        assertTrue(date.getDateAsSQL().toString().equals("2005-08-12"));
        
        date = SampleDate.createInstance("060908bscsa140_1");
        assertTrue(date.getDateAsSQL().toString().equals("2006-09-08"));        
        
        date = SampleDate.createInstance("050812sa1499_1");
        assertTrue(date.getDateAsSQL().toString().equals("2005-08-12"));
        
        date = SampleDate.createInstance("060908bscsa1409_1");
        assertTrue(date.getDateAsSQL().toString().equals("2006-09-08"));        
        
        date = SampleDate.createInstance("060908bscq1409_1");
        assertTrue(date.getDateAsSQL().toString().equals("2006-09-08"));
    }

    /*
     * Test method for 'edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.SampleDate.getNumberOfDay()'
     */
    public void testGetNumberOfDay() {
        SampleDate date = SampleDate.createInstance("2001ea44_1");
        assertTrue(date.getNumberOfDay() == 44);

        date = SampleDate.createInstance("2002ea12_1");
        assertTrue(date.getNumberOfDay() == 12);

        date = SampleDate.createInstance("050812akbq1_1");
        assertTrue(date.getNumberOfDay() == 1);

        date = SampleDate.createInstance("050812akbq12_1");
        assertTrue(date.getNumberOfDay() == 12);

        date = SampleDate.createInstance("050812akbq14_1");
        assertTrue(date.getNumberOfDay() == 14);

        date = SampleDate.createInstance("050812akbsa14_1");
        assertTrue(date.getNumberOfDay() == 14);

        date = SampleDate.createInstance("050812akbs14_1");
        assertTrue(date.getNumberOfDay() == 14);

        date = SampleDate.createInstance("050812sa14_1");
        assertTrue(date.getNumberOfDay() == 14);
        
        date = SampleDate.createInstance("060908bscsa140_1");
        assertTrue(date.getNumberOfDay() == 140);        
        
        date = SampleDate.createInstance("050812sa1444_1");
        assertTrue(date.getNumberOfDay() == 1444);
        
        date = SampleDate.createInstance("060908bscsa1404_1");
        assertTrue(date.getNumberOfDay() == 1404);        
    }
}
