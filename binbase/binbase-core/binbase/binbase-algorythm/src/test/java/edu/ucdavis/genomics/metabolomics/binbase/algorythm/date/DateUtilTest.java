package edu.ucdavis.genomics.metabolomics.binbase.algorythm.date;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.Test;

public class DateUtilTest {

	@Test
	public void testIsSameDay() {
		assertTrue(DateUtil.isSameDay(new Date(), new Date()));
		assertFalse(DateUtil.isSameDay(new Date(), new Date(0)));
		assertTrue(DateUtil.isSameDay(new Date(0), new Date(0)));
		assertTrue(DateUtil.isSameDay(new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()-1000)));

	}

}
