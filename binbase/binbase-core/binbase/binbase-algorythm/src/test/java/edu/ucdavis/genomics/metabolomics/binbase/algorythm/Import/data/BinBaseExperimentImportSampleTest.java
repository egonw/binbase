package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BinBaseExperimentImportSampleTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCalculateProgress() {
		BinBaseExperimentImportSample sample = new BinBaseExperimentImportSample();
		sample.setCorrected(true);
		sample.setImported(true);
		sample.setMatched(true);
		sample.setMetaDataStored(true);
		sample.setValidated(true);
		System.out.println(sample.calculateProgress());
		assertTrue(sample.calculateProgress() == 100);
	}

}
