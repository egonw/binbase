package edu.ucdavis.genomics.metabolomics.binbase.algorythm.filter;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;

/**
 * simple test if the basepeak filter works
 * @author wohlgemuth
 *
 */
public class BasepeakFilterTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBasepeakFilter() {
		XMLConfigurator.getInstance().destroy();
		XMLConfigurator.getInstance();
		@SuppressWarnings("unused")
		BasepeakFilter filter = new BasepeakFilter();
		
	}

	@Test
	public void testAccept() {
		XMLConfigurator.getInstance().destroy();
		XMLConfigurator.getInstance();
		BasepeakFilter filter = new BasepeakFilter();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("spectra","87:100 88:50 147:12");
		assertTrue(filter.accept(map));
	}

}
