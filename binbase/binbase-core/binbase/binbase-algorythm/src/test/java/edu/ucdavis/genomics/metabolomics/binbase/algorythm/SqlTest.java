package edu.ucdavis.genomics.metabolomics.binbase.algorythm;

import java.sql.Connection;
import java.util.Set;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.database.statements.StatementRepository;
import edu.ucdavis.genomics.metabolomics.util.database.statements.StatementTester;
import edu.ucdavis.genomics.metabolomics.util.database.test.BinBaseDatabaseTest;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

/**
 * just a simple test to check if all the sql statements compile
 * 
 * @author wohlgemuth
 * 
 */
public class SqlTest extends BinBaseDatabaseTest {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new XmlDataSet(new ResourceSource("/minimum-rtx5.xml")
				.getStream());
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test(timeout = 60000)
	public void testCompile() throws Exception {
		StatementRepository repository = StatementRepository.getInstance();
		repository.addSource(new ResourceSource("/config/sql.xml"));

		Set<String> statements = repository.getStatementNames();

		Connection connection = this.getFactory().getConnection();
		try {
			for (String statement : statements) {
				logger.info("preparing statement with name: " + statement);
				try{
				StatementTester.testStatements(repository.getStatement(statement), connection);
				logger.info("successfull next statement");
				logger.info("");
				}
				catch (Exception e) {
					logger.error(e.getMessage() + " - statement: " + statement + " - sql: " + repository.getStatement(statement));
					throw e;
				}
			}
		}

		finally{
			this.getFactory().close(connection);
		}
	}
}
