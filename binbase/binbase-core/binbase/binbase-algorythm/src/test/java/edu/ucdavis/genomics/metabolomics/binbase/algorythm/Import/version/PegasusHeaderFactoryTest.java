/*
 * Created on Feb 28, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.version;

import java.io.InputStream;
import java.util.Scanner;

import junit.framework.TestCase;

public class PegasusHeaderFactoryTest extends TestCase {
	
	public PegasusHeaderFactoryTest(String arg0) {
		super(arg0);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public final void testGetHeader() throws Exception {
	
		PegasusHeaderFactory factory = PegasusHeaderFactory.create();
		
		InputStream source222 = getClass().getResourceAsStream("/222.txt");
		InputStream source325 = getClass().getResourceAsStream("/325.txt");
		
		Scanner scanner222 = new Scanner(source222);
		Scanner scanner325 = new Scanner(source325);
		
		scanner222.nextLine();
		scanner325.nextLine();
		
		PegasusHeader header = factory.getHeader(scanner222.nextLine().split("\t"));
		assertTrue(header.getVersion() == 222);
		assertFalse(header.getVersion() == 325);
		
		header = factory.getHeader(scanner325.nextLine().split("\t"));
		assertTrue(header.getVersion() == 325);
		assertFalse(header.getVersion() == 222);
		
	}

}
