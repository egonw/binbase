package edu.ucdavis.genomics.metabolomics.binbase.algorythm.util;

import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.util.database.test.BinBaseDatabaseTest;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.status.Log4JReport;
import edu.ucdavis.genomics.metabolomics.util.status.Report;

/**
 * Tests the creation of results with diffrent implmentation
 * 
 * @author wohlgemuth
 * 
 */
public abstract class ResultCreatorTest extends BinBaseDatabaseTest {

	private Logger logger = Logger.getLogger(getClass());
	private Report report = new Log4JReport();

	private ResultCreator creator;

	private Experiment exp;
	private Connection c;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.c = this.getFactory().getConnection();
		creator = this.createCreator(report);
		// setup the experiment - based on our xml file!
		createExperiment();
	}

	private void createExperiment() {
		ExperimentClass clazz = new ExperimentClass();
		clazz.setId("1");
		clazz.setColumn(System.getProperty("Binbase.user"));

		ExperimentSample samples[] = new ExperimentSample[6];
		samples[0] = new ExperimentSample("4125ea02_2", "4125ea02_2");
		samples[1] = new ExperimentSample("4125ea03_2", "4125ea03_2");
		samples[2] = new ExperimentSample("4125ea04_2", "4125ea04_2");
		samples[3] = new ExperimentSample("4125ea05_2", "4125ea05_2");
		samples[4] = new ExperimentSample("4125ea06_2", "4125ea06_2");
		samples[5] = new ExperimentSample("4125ea07_2", "4125ea07_2");

		clazz.setSamples(samples);

		exp = new Experiment();
		exp.setClasses(new ExperimentClass[] { clazz });
		exp.setColumn(clazz.getColumn());
		exp.setId("2");
	}

	@After
	public void tearDown() throws Exception {
		this.getFactory().close(c);
		creator = null;
		exp = null;
		super.tearDown();
	}

	/**
	 * gives us the implementation to test
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract ResultCreator createCreator(Report report) throws Exception;

	/**
	 * tests that the creator works as exspected
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCreator() throws Exception {

		// make sure we got 7 samples
		assertTrue(this.getSampleCount() == 7);

		// make sure we got no results
		assertTrue(this.getResultCount() == 0);
		assertTrue(this.getResultLinkCount() == 0);

		// make sure we the experiment is ready
		assertTrue(creator.readyForExport(exp));

		// create an experiment and store it
		int id = creator.createResultDefinition(exp);

		// make sure the id actuall exist
		assertTrue(idExist(id));

		// make sure the amount equals the exspactation
		assertTrue(this.getResultCount() == 1);
		assertTrue(this.getResultLinkCount() == 6);

		// delete an experiment
		creator.dropResult(id);

		// make sure the amount equals the exspactation
		assertTrue(this.getResultCount() == 0);
		assertTrue(this.getResultLinkCount() == 0);

		// make sure we got 7 samples not that accidently any samples disappered
		assertTrue(this.getSampleCount() == 7);

	}

	/**
	 * returns the count of results
	 * 
	 * @return
	 * @throws Exception
	 */
	protected int getResultCount() throws Exception {
			ResultSet res = c.createStatement().executeQuery(
					"select count(*) from result");

			res.next();
			return res.getInt(1);
	}

	/**
	 * returns the count of result links
	 * 
	 * @return
	 * @throws Exception
	 */
	protected int getResultLinkCount() throws Exception {
			ResultSet res = c.createStatement().executeQuery(
					"select count(*) from result_link");

			res.next();
			return res.getInt(1);
	}

	/**
	 * valdiates the the id exist
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	protected boolean idExist(int id) throws Exception {
			ResultSet res = c.createStatement().executeQuery(
					"select count(*) from result where \"result_id\" = " + id);

			return res.next();
	}

	/**
	 * returns the amount of samples
	 * 
	 * @return
	 * @throws Exception
	 */
	protected int getSampleCount() throws Exception {
			ResultSet res = c.createStatement().executeQuery(
					"select count(*) from samples");

			res.next();
			return res.getInt(1);
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		try {
			logger.info("looking for datafile...");
			ResourceSource source = new ResourceSource("/experiment-rtx5.xml");
			logger.debug("using source: " + source.getSourceName() + " exist: "
					+ source.exist());
			assertTrue(source.exist());
			return new XmlDataSet(source.getStream());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
