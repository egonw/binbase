package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver;

import java.util.List;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.RawdataResolver;

/**
 * used to create a builder to create all our required resolvers
 * @author wohlgemuth
 *
 */
public interface ResolverBuilder {

	/**
	 * builds the actual resolvers
	 * @return
	 */
	public List<RawdataResolver> build();
}
