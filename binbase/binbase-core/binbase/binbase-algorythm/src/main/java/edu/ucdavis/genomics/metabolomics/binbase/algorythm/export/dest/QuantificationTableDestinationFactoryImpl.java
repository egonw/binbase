/*
 * Created on Nov 18, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.export.dest;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination;
import edu.ucdavis.genomics.metabolomics.util.io.dest.DestinationFactory;

/**
 * 
 * @author wohlgemuth
 * @version Nov 18, 2005
 *
 */
public class QuantificationTableDestinationFactoryImpl extends DestinationFactory{

	public QuantificationTableDestinationFactoryImpl() {
		super();
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Nov 18, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.dest.DestinationFactory#createDestination(java.lang.Object, java.util.Map)
	 */
	public Destination createDestination(Object identifier, Map propertys) throws ConfigurationException {
		Destination destination = new QuantificationTableDestination();
        destination.configure(propertys);
        destination.setIdentifier(identifier);

		return destination;
	}

}
