package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver;

import java.io.File;
import java.io.FileOutputStream;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.RawdataResolver;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * retrieves the files from the server
 * 
 * @author wohlgemuth
 * 
 */
public class RemoteResolver extends AbstractFileResolver {

	private static Semaphore netcdfDowload = new Semaphore(Runtime.getRuntime()
			.availableProcessors()+10);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public File resolveNetcdfFile(String nameOfSample) {
		try {
			netcdfDowload.acquire();
			
			File file = new File(System.getProperty("java.io.tmpdir"),nameOfSample+".cdf");

			// only download if the file does not exist already
			if (file.exists() == false) {
				getLogger().debug("looking for file on server");

				file.deleteOnExit();

				BinBaseService service = (BinBaseService) EjbClient
						.getRemoteEjb(Configurator.BCI_GLOBAL_NAME,
								BinBaseServiceBean.class);
				getLogger().debug("getting file from server");

				byte[] content = service.getNetCdfFile(nameOfSample, KeyFactory
						.newInstance().getKey());
				getLogger().debug("received data");

				FileOutputStream out = new FileOutputStream(file);
				out.write(content);
				out.flush();
				out.close();
				out = null;

				content = null;
				System.gc();
				getLogger().info("done");
			}
			return file;
		} catch (BinBaseException e) {
			if (e.getMessage().indexOf("sorry couldn't find the object") > -1) {
				getLogger().info(e.getMessage());
			} else {
				getLogger().error(e.getMessage(), e);
			}
			return null;
		} catch (Exception e) {
			getLogger().error(e.getMessage(), e);
			return null;
		} finally {
			netcdfDowload.release();
		}
	}

	protected Logger getLogger() {
		return Logger.getLogger(getClass());
	}

	@Override
	public Integer getPriority() {
		return 1;
	}

	@Override
	public int compareTo(RawdataResolver o) {
		return o.getPriority().compareTo(this.getPriority());
	}
}
