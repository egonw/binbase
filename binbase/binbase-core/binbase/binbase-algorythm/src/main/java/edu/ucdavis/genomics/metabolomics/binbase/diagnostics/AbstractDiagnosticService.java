package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

/**
 * a simplified version of the diagnostic service to ease the work with it
 * 
 * @author wohlgemuth
 * 
 */
public abstract class AbstractDiagnosticService implements DiagnosticsService {

	/**
	 * a simplified wrapper from string to the concrete objects
	 * 
	 * @param className
	 * @param sampleId
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	@Override
	public void diagnosticAction(String className, int sampleId, int spectraId,
			int binId, Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Result resultOfAction,Object[] parameters) {

		this.diagnosticAction(className, sampleId, spectraId, binId, caller,
				new Step(stepOfAlgorithm), new Reason(
						reasonForTheArchivedResult), resultOfAction,parameters);
	}

	/**
	 * a simplified wrapper from string to the concrete objects
	 * 
	 * @param sampleId
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	@Override
	public void diagnosticAction(int sampleId, int spectraId, int binId,
			Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Result resultOfAction,Object[] parameters) {

		this.diagnosticAction(sampleId, spectraId, binId, caller, new Step(
				stepOfAlgorithm), new Reason(reasonForTheArchivedResult),
				resultOfAction,parameters);

	}

	/**
	 * a simplified wrapper from string to the concrete objects
	 * 
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	@Override
	public void diagnosticAction(int spectraId, int binId, Class<?> caller,
			String stepOfAlgorithm, String reasonForTheArchivedResult,
			Result resultOfAction,Object[] parameters) {

		this.diagnosticAction(spectraId, binId, caller, new Step(
				stepOfAlgorithm), new Reason(reasonForTheArchivedResult),
				resultOfAction,parameters);
	}

	/**
	 * a successful action
	 * 
	 * @param className
	 *            The class
	 * @param sampleId
	 *            The sampleId
	 * @param spectraId
	 *            The spectraId
	 * @param binId
	 *            The binId
	 * @param caller
	 *            Who called this method
	 * @param stepOfAlgorithm
	 *            What is the step of the Algorithm
	 * @param reasonForTheArchivedResult
	 *            what is the reason for the ArchivedResult
	 * @param resultOfAction
	 *            What was the result
	 */
	@Override
	public void diagnosticActionSuccess(String className, int sampleId,
			int spectraId, int binId, Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult,Object[] parameters) {
		this.diagnosticAction(className, sampleId, spectraId, binId, caller,
				stepOfAlgorithm, reasonForTheArchivedResult, Result.SUCCESS,parameters);
	}

	/**
	 * a successful action
	 * 
	 * @param sampleId
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 */
	@Override
	public void diagnosticActionSuccess(int sampleId, int spectraId, int binId,
			Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult,Object[] parameters) {
		this.diagnosticAction(sampleId, spectraId, binId, caller,
				stepOfAlgorithm, reasonForTheArchivedResult, Result.SUCCESS,parameters);
	}

	/**
	 * a successful action
	 * 
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 */
	@Override
	public void diagnosticActionSuccess(int spectraId, int binId,
			Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult,Object[] parameters) {
		this.diagnosticAction(spectraId, binId, caller, stepOfAlgorithm,
				reasonForTheArchivedResult, Result.SUCCESS,parameters);
	}

	/**
	 * a failed action
	 * 
	 * @param className
	 *            The class
	 * @param sampleId
	 *            The sampleId
	 * @param spectraId
	 *            The spectraId
	 * @param binId	
	 *            The binId
	 * @param caller
	 *            Who called this method
	 * @param stepOfAlgorithm
	 *            What is the step of the Algorithm
	 * @param reasonForTheArchivedResult
	 *            what is the reason for the ArchivedResult
	 * @param resultOfAction
	 *            What was the result
	 */
	@Override
	public void diagnosticActionFailed(String className, int sampleId,
			int spectraId, int binId, Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult,Object[] parameters) {
		this.diagnosticAction(className, sampleId, spectraId, binId, caller,
				stepOfAlgorithm, reasonForTheArchivedResult, Result.FAILED,parameters);
	}

	/**
	 * a failed action
	 * 
	 * @param sampleId
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 */
	@Override
	public void diagnosticActionFailed(int sampleId, int spectraId, int binId,
			Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult,Object[] parameters) {
		this.diagnosticAction(sampleId, spectraId, binId, caller,
				stepOfAlgorithm, reasonForTheArchivedResult, Result.FAILED,parameters);
	}

	/**
	 * a failed action
	 * 
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 */
	@Override
	public void diagnosticActionFailed(int spectraId, int binId,
			Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult,Object[] parameters) {
		this.diagnosticAction(spectraId, binId, caller, stepOfAlgorithm,
				reasonForTheArchivedResult, Result.FAILED,parameters);
	}

	/**
	 * a simplified wrapper from string to the concrete objects
	 * 
	 * @param className
	 * @param sampleId
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	@Override
	public void diagnosticActionSuccess(String className, int sampleId,
			int spectraId, int binId, Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult,Object[] parameters) {

		this.diagnosticActionSuccess(className, sampleId, spectraId, binId,
				caller, new Step(stepOfAlgorithm), new Reason(
						reasonForTheArchivedResult),parameters);
	}

	/**
	 * a simplified wrapper from string to the concrete objects
	 * 
	 * @param sampleId
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	@Override
	public void diagnosticActionSuccess(int sampleId, int spectraId, int binId,
			Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Result resultOfAction,Object[] parameters) {

		this.diagnosticActionSuccess(sampleId, spectraId, binId, caller,
				new Step(stepOfAlgorithm), new Reason(
						reasonForTheArchivedResult),parameters);

	}

	/**
	 * a simplified wrapper from string to the concrete objects
	 * 
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	@Override
	public void diagnosticActionSuccess(int spectraId, int binId,
			Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult,Object[] parameters) {

		this.diagnosticActionSuccess(spectraId, binId, caller, new Step(
				stepOfAlgorithm), new Reason(reasonForTheArchivedResult),parameters);
	}

	/**
	 * a simplified wrapper from string to the concrete objects
	 * 
	 * @param className
	 * @param sampleId
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	@Override
	public void diagnosticActionFailed(String className, int sampleId,
			int spectraId, int binId, Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult,Object[] parameters) {

		this.diagnosticActionFailed(className, sampleId, spectraId, binId,
				caller, new Step(stepOfAlgorithm), new Reason(
						reasonForTheArchivedResult),parameters);
	}

	/**
	 * a simplified wrapper from string to the concrete objects
	 * 
	 * @param sampleId
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	@Override
	public void diagnosticActionFailed(int sampleId, int spectraId, int binId,
			Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Result resultOfAction,Object[] parameters) {

		this.diagnosticActionFailed(sampleId, spectraId, binId, caller,
				new Step(stepOfAlgorithm), new Reason(
						reasonForTheArchivedResult),parameters);

	}

	/**
	 * a simplified wrapper from string to the concrete objects
	 * 
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	@Override
	public void diagnosticActionFailed(int spectraId, int binId,
			Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult,Object[] parameters) {

		this.diagnosticActionFailed(spectraId, binId, caller, new Step(
				stepOfAlgorithm), new Reason(reasonForTheArchivedResult),parameters);
	}


	@Override
	public void diagnosticAction(int spectraId, Class<?> caller,
			String stepOfAlgorithm, String reasonForTheArchivedResult,
			String resultOfAction, Object[] parameters) {
		this.diagnosticAction(spectraId, caller, new Step(stepOfAlgorithm), new Reason(reasonForTheArchivedResult), new Result(resultOfAction), parameters);
		
	}

	@Override
	public void diagnosticActionSuccess(int spectraId, Class<?> caller,
			String stepOfAlgorithm, String reasonForTheArchivedResult,
			Object[] parameters) {
		this.diagnosticAction(spectraId, caller, new Step(stepOfAlgorithm), new Reason(reasonForTheArchivedResult), Result.SUCCESS, parameters);
		
	}

	@Override
	public void diagnosticActionFailed(int spectraId, Class<?> caller,
			String stepOfAlgorithm, String reasonForTheArchivedResult,
			Object[] parameters) {
		this.diagnosticAction(spectraId, caller, new Step(stepOfAlgorithm), new Reason(reasonForTheArchivedResult), Result.FAILED, parameters);
		
	}

	@Override
	public void diagnosticActionSuccess(int spectraId, Class<?> caller,
			Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
			Object[] parameters) {
		this.diagnosticAction(spectraId, caller, (stepOfAlgorithm), (reasonForTheArchivedResult), Result.SUCCESS, parameters);
		
	}

	@Override
	public void diagnosticActionFailed(int spectraId, Class<?> caller,
			Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
			Object[] parameters) {
		this.diagnosticAction(spectraId, caller, (stepOfAlgorithm), (reasonForTheArchivedResult), Result.FAILED, parameters);
		
	}

}
