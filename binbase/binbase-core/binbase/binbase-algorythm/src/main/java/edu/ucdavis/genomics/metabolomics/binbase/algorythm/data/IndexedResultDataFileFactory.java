package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data;

import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory;

/**
 * creates an indexed version of the datafile
 * @author wohlgemuth
 *
 */
public class IndexedResultDataFileFactory  extends DataFileFactory{
	
	
	public DataFile createDataFile() {
		ResultDataFile file = new ResultDataFile();
		file.setIndexed(true);
		
		return file;
	}
}
