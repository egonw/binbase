package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output;

import java.awt.image.RenderedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

public class JPEG implements Writer {

	@Override
	public boolean isDatafileSupported() {
		return false;
	}

	@Override
	public boolean isSourceSupported() {
		return false;
	}

	@Override
	public void write(OutputStream out, DataFile file) throws IOException {
	}

	@Override
	public void write(OutputStream out, Source content) throws IOException {
	}

	@Override
	public void write(OutputStream out, Object content) throws IOException {
		ImageIO.write((RenderedImage) content, "jpg", out);
		out.flush();
	}

	@Override
	public String toString() {
		return "jpg";
	}
}
