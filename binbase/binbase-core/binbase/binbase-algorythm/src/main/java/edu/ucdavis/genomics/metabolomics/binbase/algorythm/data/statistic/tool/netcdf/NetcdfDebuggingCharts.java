package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.netcdf;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.OldReplaceWithQuantIntensity;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter;
import edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.plot.SpectraChart;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet;
import edu.ucdavis.genomics.metabolomics.util.math.Similarity;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.SampleObject;

/**
 * generates the netcdf debugging charts, which visualize which peak was picked for the replacement
 * 
 * @author wohlgemuth
 * 
 */
public class NetcdfDebuggingCharts extends NetcdfDebugging {

	int chartHeight = 600;
	int chartWidth = 800;
	int spectraHeight = chartHeight / 2;
	int spectraWidth = chartWidth / 2;

	// offset in pixels
	int offset = 10;

	/**
	 * generates a chart which peak was picked
	 * 
	 * @param object
	 * @param bin
	 * @param sample
	 * @throws BinBaseException
	 */
	@SuppressWarnings("unchecked")
	protected void workOnObject(ContentObject<?> object,
			HeaderFormat<String> bin, SampleObject<String> sample)
			throws BinBaseException {

		try {
			Map<Double, Double> map = (Map<Double, Double>) object
					.getAttachments().get("graph");

			XYSeries data = new XYSeries(bin.getAttributes().get("quantmass"));

			double[] x = new double[map.size()];
			double[] y = new double[map.size()];

			// used for highlighting our searchwindows
			int searchBegin = 0;
			int searchEnd = 0;

			// where are our noise values
			int noiseBegin = 0;
			int noiseEnd = 0;

			// nearest position of the selected peak
			int hitPosition = 0;

			// position where we started with our search
			int startPosition = 0;

			// our detected peak time
			double assumed = Double.parseDouble(object.getAttributes().get(
					"assumed_time"));

			// the time where we started searching
			double searchTime = Double.parseDouble(object.getAttributes().get(
					"start_time"));

			int counter = 0;

			// data need to be sorted...
			List<Double> list = new Vector<Double>();
			for (Iterator<Double> it = map.keySet().iterator(); it.hasNext();) {
				list.add(it.next());
			}

			Collections.sort(list);

			// build our data object for the graph
			for (Iterator<Double> it = list.iterator(); it.hasNext();) {
				double time = it.next();
				x[counter] = time;
				y[counter] = map.get(time);
				data.add(time, map.get(time));

				if (time < searchTime
						- OldReplaceWithQuantIntensity.RI_DETECTION_RANGE) {
					searchBegin = counter;
				}
				if (time < searchTime
						+ OldReplaceWithQuantIntensity.RI_DETECTION_RANGE) {
					searchEnd = counter;
				}
				if (time < assumed - OldReplaceWithQuantIntensity.NOISE_RANGE) {
					noiseBegin = counter;
				}
				if (time < assumed + OldReplaceWithQuantIntensity.NOISE_RANGE) {
					noiseEnd = counter;
				}
				if (time <= assumed) {
					hitPosition = counter;
				}
				if (time <= searchTime) {
					startPosition = counter;
				}

				counter++;
			}

			XYSeriesCollection dataset = new XYSeriesCollection();
			dataset.addSeries(data);

			// actual chart
			JFreeChart chart = ChartFactory.createXYLineChart(bin
					.getAttributes().get("name")
					+ " - "
					+ sample.getAttributes().get("name"), "time",
					"intensity for mass "
							+ bin.getAttributes().get("quantmass"), dataset,
					PlotOrientation.VERTICAL, true, true, false);
			chart.setBackgroundPaint(Color.white);

			// renderers
			XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
			renderer.setSeriesLinesVisible(0, true);
			renderer.setSeriesShapesVisible(0, false);
			renderer.setSeriesItemLabelsVisible(0, true);

			// begin markers
			IntervalMarker begin = new IntervalMarker(x[searchBegin],
					x[searchEnd]);
			begin.setLabel("search range");
			begin.setPaint(Color.yellow);
			begin.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
			begin.setLabelTextAnchor(TextAnchor.TOP_RIGHT);

			// end markers
			IntervalMarker end = new IntervalMarker(x[noiseBegin], x[noiseEnd]);
			end.setLabel("noise range");
			end.setPaint(new Color(245, 255, 250));
			end.setLabelAnchor(RectangleAnchor.TOP_LEFT);
			end.setLabelTextAnchor(TextAnchor.TOP_LEFT);

			final Marker hit = new ValueMarker(x[hitPosition]);
			hit.setPaint(Color.red);
			hit.setLabel("hit: "
					+ NumberFormat.getIntegerInstance().format(assumed));
			hit.setLabelAnchor(RectangleAnchor.CENTER);
			hit.setLabelTextAnchor(TextAnchor.TOP_RIGHT);

			final Marker search = new ValueMarker(x[startPosition]);
			search.setPaint(Color.blue);
			search.setLabel("origin: "
					+ NumberFormat.getIntegerInstance().format(searchTime));
			search.setLabelAnchor(RectangleAnchor.CENTER);
			search.setLabelTextAnchor(TextAnchor.BOTTOM_RIGHT);

			// sets the marker
			((XYPlot) chart.getPlot()).addDomainMarker(end, Layer.BACKGROUND);
			((XYPlot) chart.getPlot()).addDomainMarker(begin, Layer.BACKGROUND);

			// sets the hits and orignins
			((XYPlot) chart.getPlot()).addDomainMarker(hit, Layer.FOREGROUND);
			((XYPlot) chart.getPlot())
					.addDomainMarker(search, Layer.FOREGROUND);

			// set the renderer
			((XYPlot) chart.getPlot()).setRenderer(renderer);

			combineGraphs(object, bin, sample, chart);
		} catch (Exception e) {
			logger.warn("hit exception, ignore for now: " + e.getMessage(), e);
		}

	}

	/**
	 * combines all the graphs into one large graphs
	 * 
	 * @param object
	 * @param bin
	 * @param sample
	 * @param chart
	 * @throws BinBaseException
	 */
	private void combineGraphs(ContentObject<?> object,
			HeaderFormat<String> bin, SampleObject<String> sample,
			JFreeChart chart) throws BinBaseException {
		try {
			Similarity sim = new Similarity();
			sim.setLibrarySpectra(bin.getAttributes().get("spectra"));
			sim.setUnknownSpectra(object.getAttributes().get("scan"));

			// build the image objects
			BufferedImage chartImage = chart.createBufferedImage(chartWidth,
					chartHeight);
			BufferedImage unknownImage = createMassSpec(
					object.getAttributes().get("scan"),
					spectraWidth,
					spectraHeight,
					"hit ("
							+ NumberFormat.getIntegerInstance().format(
									sim.calculateSimimlarity())
							+ " similarity)");
			BufferedImage binImage = createMassSpec(
					bin.getAttributes().get("spectra"), spectraWidth,
					spectraHeight, bin.getAttributes().get("name"));

			BufferedImage out = new BufferedImage(chartWidth + 2 * offset,
					chartHeight + 2 * offset + spectraHeight,
					chartImage.getType());

			Graphics g = out.getGraphics();

			// set it to a white background
			g.setColor(Color.white);
			g.fillRect(0, 0, out.getWidth(), out.getHeight());

			// draw the main chart
			g.drawImage(chartImage, offset, offset, chartWidth, chartHeight,
					Color.white, null);

			// draw the bin massspec
			g.drawImage(binImage, offset, chartHeight + offset, spectraWidth,
					spectraHeight, Color.white, null);

			// draw the spectra massspec
			g.drawImage(unknownImage, spectraWidth + offset, chartHeight
					+ offset, spectraWidth, spectraHeight, Color.white, null);

			// write to file
			writeJPEGImage(out, bin.getAttributes().get("name") + "_"
					+ bin.getAttributes().get("id"), sample.getAttributes()
					.get("name"));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e);
		}
	}

	/**
	 * creates the actual spectra chart
	 * 
	 * @param spectra
	 * @param width
	 * @param height
	 * @return
	 */
	public BufferedImage createMassSpec(String spectra, int width, int height,
			String name) {

		// load the data into a dataset
		BinBaseXYDataSet dataset = new BinBaseXYDataSet();

		// get the spectra from the parameters
		Collection<Ion> c = SpectraConverter.stringToCollection(spectra);

		// parse the string
		double[] masses = new double[c.size()];
		double[] intensities = new double[c.size()];

		int maxIon = 0;
		int counter = 0;
		for (Ion o : c) {
			masses[counter] = o.getMass();
			intensities[counter] = o.getRelativeIntensity();
			counter++;

			// just to set the scale to a smart value
			if (o.getMass() > maxIon) {
				if (o.getRelativeIntensity() > 0.5) {
					maxIon = o.getMass();
				}
			}
		}

		// add our created dataset
		dataset.addDataSet(masses, intensities, name);

		// the chart it self
		JFreeChart chart = SpectraChart.createChart(dataset);
		chart.removeLegend();
		chart.setTitle(name);

		// sets the background color
		chart.setBackgroundPaint(Color.white);

		// set the axis to 110%
		ValueAxis axis = chart.getXYPlot().getRangeAxis();
		axis.setRange(0, 110);

		// set the x axis to 35 - x
		axis = chart.getXYPlot().getDomainAxis();
		axis.setRange(30, maxIon + 5);

		return chart.createBufferedImage(width, height);
	}

	public String getDescription() {
		return "generates details graph for every peak, which was replaced during the replacement phase. Needs the System property 'BINBASE_NETCDF_ENABLE_GRAPH' set to true or will fail";
	}
}
