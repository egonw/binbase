package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.util.Collection;
import java.util.Properties;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.StaticStatisticActions;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.JPEG;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.PNG;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.Writer;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

/**
 * basic processable class
 * 
 * @author wohlgemuth
 */
public abstract class BasicProccessable implements Processable {

	private Logger logger = Logger.getLogger(getClass());

	protected StaticStatisticActions action = new StaticStatisticActions();
	
	@Override
	public String getFileIdentifier() {
		return "result";
	}

	@Override
	public DataFile simpleProcess(DataFile datafile, Element configuration)
			throws BinBaseException {
		logger.info("nothing done in the default implementation");
		return null;
	}

	private String id;

	protected String getId() {
		return id;
	}

	private String currentFolder;
	private Collection<String> destinationIds;

	@Override
	public void setCurrentFolder(String folder) {
		this.currentFolder = folder;
	}

	@Override
	public void setCurrentId(String id) {
		this.id = id;
	}

	public void setDestinationIds(Collection<String> destinationIds) {
		this.destinationIds = destinationIds;
	}

	/**
	 * writes an object to the result file
	 * 
	 * @throws IOException
	 */
	public void writeObject(Object content, Element configuration,
			String identifier) throws IOException {
		if (currentFolder == null) {
			currentFolder = getFolder();
		}

		action.writeEntry(id, currentFolder, content,
				configuration, destinationIds, identifier);
	}

	/**
	 * writes an object to the result file
	 * 
	 * @throws IOException
	 */
	public void writeObject(Object content, String identifier, Writer writer)
			throws IOException {
		if (currentFolder == null) {
			currentFolder = getFolder();
		}

		action.writeOutput(id, currentFolder, content,
				destinationIds, identifier, writer);
	}

	/**
	 * writes an object to the result file
	 * 
	 * @throws IOException
	 */
	public void writeJPEGImage(BufferedImage content, String identifier)
			throws IOException {
		if (currentFolder == null) {
			currentFolder = getFolder();
		}

		action.writeOutput(id, currentFolder, content,
				destinationIds, identifier, new JPEG());
	}

	public void writePNGImage(BufferedImage content, String identifier)
			throws IOException {
		if (currentFolder == null) {
			currentFolder = getFolder();
		}
		action.writeOutput(id, currentFolder, content,
				destinationIds, identifier, new PNG());
	}

	/**
	 * writes an object to the result file
	 * 
	 * @throws IOException
	 */
	public void writeObject(Object content, Element configuration,
			String identifier, String subFolder) throws IOException {
		if (currentFolder == null) {
			currentFolder = getFolder();
		}

		action.writeEntry(id, currentFolder + "/" + subFolder,
				content, configuration, destinationIds, identifier);
	}

	/**
	 * writes an object to the result file
	 * 
	 * @throws IOException
	 */
	public void writeObject(Object content, String identifier, Writer writer,
			String subFolder) throws IOException {
		if (currentFolder == null) {
			currentFolder = getFolder();
		}

		action.writeOutput(id, currentFolder + "/" + subFolder,
				content, destinationIds, identifier, writer);
	}

	/**
	 * writes an object to the result file
	 * 
	 * @throws IOException
	 */
	public void writeJPEGImage(BufferedImage content, String identifier,
			String subFolder) throws IOException {
		if (currentFolder == null) {
			currentFolder = getFolder();
		}

		action.writeOutput(id, currentFolder + "/" + subFolder,
				content, destinationIds, identifier, new JPEG());
	}

	public void writePNGImage(BufferedImage content, String identifier,
			String subFolder) throws IOException {
		if (currentFolder == null) {
			currentFolder = getFolder();
		}

		action.writeOutput(id, currentFolder + "/" + subFolder,
				content, destinationIds, identifier, new PNG());
	}

	protected Connection createDatabaseConnection(String column,
			ConnectionFactory factory) throws BinBaseException,
			RemoteException, CreateException, NamingException {
		Properties p = Configurator.getDatabaseService().createProperties();
		p.setProperty(SimpleConnectionFactory.KEY_USERNAME_PROPERTIE, column);
		p.put("hibernate.cache.use_minimal_puts", "false");
		p.put("hibernate.cache.use_query_cache", "false");
		p.put("hibernate.cache.use_second_level_cache", "false");

		factory.setProperties(p);
		HibernateFactory.newInstance(p);
		Connection connection = factory.getConnection();
		return connection;
	}

	/**
	 * should the result be written to a file
	 * 
	 * @return
	 */
	public boolean writeResultToFile() {
		return true;
	}
}
