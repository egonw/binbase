/*
 * Created on Nov 18, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.export;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;

public class SimpleExportServiceFactoryImpl extends ExportServiceFactory{

	public SimpleExportServiceFactoryImpl() {
		super();
	}

	public ExportService createService() throws ConfigurationException {
		ExportService service = new SimpleExportService();
		return service;
	}

}
