package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

/**
 * just usful to provide an implmentation with a diagnostics server
 * 
 * @author wohlgemuth
 * 
 */
public interface Diagnostics {

	/**
	 * returns the actual service for diagnostics reasons
	 * 
	 * @return
	 */
	DiagnosticsService getDiagnosticsService();
}
