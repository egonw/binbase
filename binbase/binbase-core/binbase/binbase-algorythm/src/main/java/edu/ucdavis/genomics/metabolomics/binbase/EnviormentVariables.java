package edu.ucdavis.genomics.metabolomics.binbase;

/**
 * possible binbase enviorment variables
 * @author wohlgemuth
 *1�
 */
public interface EnviormentVariables {

	/**
	 * is used in netcdf file based replacements to load the files locally and not from 
	 * the application server
	 */
	public String BINBASE_LOCAL_NETCDF_DIR = "BINBASE_LOCAL_CDF_DIR";
	
}
