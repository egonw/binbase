package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

import org.apache.log4j.Logger;

/**
 * a diagnostics service which logs straight to a log4j category
 * 
 * @author wohlgemuth
 * 
 */
public class Log4JDiagnosticsService extends AbstractDiagnosticService {

	Logger logger = Logger.getLogger("diagnosticis");

	@Override
	public void diagnosticAction(String className, int sampleId, int spectraId,
			int binId, Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult, Result resultOfAction,
			Object[] parameters) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("experiment class: ");
		buffer.append(className);
		buffer.append(" sample id: ");
		buffer.append(sampleId);
		buffer.append(" spectra id: ");
		buffer.append(spectraId);
		buffer.append(" bin id: ");
		buffer.append(binId);
		buffer.append(" calling class: ");
		buffer.append(caller.getSimpleName());
		buffer.append(" current step: ");
		buffer.append(stepOfAlgorithm);
		buffer.append(" reason for result: ");
		buffer.append(reasonForTheArchivedResult);
		buffer.append(" result: ");
		buffer.append(resultOfAction);
		buffer.append(" used parameters: ");

		for (Object o : parameters) {
			buffer.append("[");
			buffer.append(o);
			buffer.append("] ");
		}

		buffer.append("\n");

		logger.info(buffer);
	}

	@Override
	public void diagnosticAction(int sampleId, int spectraId, int binId,
			Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult, Result resultOfAction,
			Object[] parameters) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("experiment class: NONE");
		buffer.append(" sample id: ");
		buffer.append(sampleId);
		buffer.append(" spectra id: ");
		buffer.append(spectraId);
		buffer.append(" bin id: ");
		buffer.append(binId);
		buffer.append(" calling class: ");
		buffer.append(caller.getSimpleName());
		buffer.append(" current step: ");
		buffer.append(stepOfAlgorithm);
		buffer.append(" reason for result: ");
		buffer.append(reasonForTheArchivedResult);
		buffer.append(" result: ");
		buffer.append(resultOfAction);
		buffer.append(" used parameters: ");

		for (Object o : parameters) {
			buffer.append("[");
			buffer.append(o);
			buffer.append("] ");
		}
		buffer.append("\n");

		logger.info(buffer);
	}

	@Override
	public void diagnosticAction(int spectraId, int binId, Class<?> caller,
			Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
			Result resultOfAction, Object[] parameters) {

		StringBuffer buffer = new StringBuffer();

		buffer.append("experiment class: NONE");
		buffer.append(" sample id: NONE");
		buffer.append(" spectra id: ");
		buffer.append(spectraId);
		buffer.append(" bin id: ");
		buffer.append(binId);
		buffer.append(" calling class: ");
		buffer.append(caller.getSimpleName());
		buffer.append(" current step: ");
		buffer.append(stepOfAlgorithm);
		buffer.append(" reason for result: ");
		buffer.append(reasonForTheArchivedResult);
		buffer.append(" result: ");
		buffer.append(resultOfAction);
		buffer.append(" used parameters: ");

		for (Object o : parameters) {
			buffer.append("[");
			buffer.append(o);
			buffer.append("] ");
		}

		buffer.append("\n");

		logger.info(buffer);

	}

	@Override
	public void diagnosticAction(int spectraId, Class<?> caller,
			Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
			Result resultOfAction, Object[] parameters) {
		StringBuffer buffer = new StringBuffer();

		buffer.append("experiment class: NONE");
		buffer.append(" sample id: NONE");
		buffer.append(" spectra id: ");
		buffer.append(spectraId);
		buffer.append(" bin id: NONE");
		buffer.append(" calling class: ");
		buffer.append(caller.getSimpleName());
		buffer.append(" current step: ");
		buffer.append(stepOfAlgorithm);
		buffer.append(" reason for result: ");
		buffer.append(reasonForTheArchivedResult);
		buffer.append(" result: ");
		buffer.append(resultOfAction);
		buffer.append(" used parameters: ");

		for (Object o : parameters) {
			buffer.append("[");
			buffer.append(o);
			buffer.append("] ");
		}

		buffer.append("\n");

		logger.info(buffer);		
	}
}
