package edu.ucdavis.genomics.metabolomics.binbase.algorythm;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.matching.SimpleSampleMatcher;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * simple factory to create a sample matcher object
 * 
 * @author wohlgemuth
 * 
 */
public abstract class SampleMatcherFactory extends AbstractFactory {

	public static final String DEFAULT_PROPERTY_NAME = SampleMatcherFactory.class
			.getName();

	/**
	 * generates the actual matcher implementation
	 * 
	 * @return
	 */
	public abstract SampleMatcher createMatcher(String databaseIdentifier);

	/**
	 * creates a new factory instance
	 * 
	 * @param report
	 * @return
	 */
	public static SampleMatcherFactory newInstance() {
		return newInstance(System.getProperty(DEFAULT_PROPERTY_NAME));
	}

	public static SampleMatcherFactory newInstance(String factoryClass) {
		if (factoryClass == null) {
			return new SampleMatcherFactory() {

				@Override
				public SampleMatcher createMatcher(String databaseIdentifier) {
					SimpleSampleMatcher matcher = new SimpleSampleMatcher();
					matcher.setConnectionIdentifier(databaseIdentifier);
					return matcher;
				}
			};
		}

		try {
			Class<SampleMatcherFactory> classObject = (Class<SampleMatcherFactory>) Class
					.forName(factoryClass);
			SampleMatcherFactory factory;

			factory = classObject.newInstance();
			return factory;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}
