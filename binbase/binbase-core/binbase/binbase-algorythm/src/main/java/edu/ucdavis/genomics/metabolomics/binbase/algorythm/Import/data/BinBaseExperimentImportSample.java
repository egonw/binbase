package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.type.ForcedSample;

/**
 * needed for monitoring
 * 
 * @author wohlgemuth
 */
public class BinBaseExperimentImportSample extends ExperimentSample {

	public BinBaseExperimentImportSample(ExperimentSample sample, BinBaseExperimentClass clazz) {
		this.setId(sample.getId());
		this.setName(sample.getName());
		this.setClazz(clazz);
		
		if(sample instanceof ForcedSample){
			this.machine = ((ForcedSample)sample).getMachine();
		}
	}

	public BinBaseExperimentImportSample() {
	}

	private BinBaseExperimentClass clazz;

	private static final long serialVersionUID = 2L;

	private boolean matched;

	private boolean postMatched;
	
	public boolean isPostMatched() {
		return postMatched;
	}

	private boolean imported;

	private boolean corrected;

	private boolean validated;

	private boolean metaDataStored;

	private int sampleId;

	private double max = 6;

	private int current = 0;

	private String machine;
	
	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public boolean isMatched() {
		return matched;
	}

	public void setMatched(boolean matched) {
		if (this.isMatched() == false) {
			progress(matched);
		}
		else if (matched == false)
			progress(matched);

		this.matched = matched;
	}


	public void setPostMatched(boolean matched) {
		if (this.isPostMatched() == false) {
			progress(matched);
		}
		else if (matched == false)
			progress(matched);

		this.postMatched = matched;
	}

	public boolean isImported() {
		return imported;
	}

	public void setImported(boolean imported) {
		if (this.isImported() == false) {
			progress(imported);
		}
		else if (imported == false)
			progress(imported);

		this.imported = imported;
	}

	public boolean isCorrected() {
		return corrected;
	}

	@Override
	public String toString() {
		return super.toString() + " (" + isCorrected() + " - " + isImported() + " - " + isMatched() + " - " + isPostMatched() + " - " + isMetaDataStored() + " - " + isValidated()
				+ ") - (progress: " + calculateProgress() + "%)";
	}

	public void setCorrected(boolean corrected) {
		if (this.isCorrected() == false) {
			progress(corrected);
		}
		else if (corrected == false)
			progress(corrected);

		this.corrected = corrected;
	}

	public boolean isValidated() {
		return validated;
	}

	public void setValidated(boolean validated) {
		if (this.isValidated() == false) {
			progress(validated);
		}
		else if (validated == false)
			progress(validated);

		this.validated = validated;
	}

	public int getSampleId() {
		return sampleId;
	}

	public void setSampleId(int sampleId) {
		this.sampleId = sampleId;
	}

	public boolean isMetaDataStored() {
		return metaDataStored;
	}

	public void setMetaDataStored(boolean metaDataStored) {
		if (this.isMetaDataStored() == false) {
			progress(metaDataStored);
		}
		else if (metaDataStored == false)
			progress(metaDataStored);

		this.metaDataStored = metaDataStored;
	}

	public double calculateProgress() {
		return (double) current / (double) max * 100;
	}

	private void progress(boolean matched) {
		if (matched) {
			current++;
		}
		else {
			current--;
		}
	}

	public BinBaseExperimentClass getClazz() {
		return clazz;
	}

	public void setClazz(BinBaseExperimentClass clazz) {
		this.clazz = clazz;
	}
}
