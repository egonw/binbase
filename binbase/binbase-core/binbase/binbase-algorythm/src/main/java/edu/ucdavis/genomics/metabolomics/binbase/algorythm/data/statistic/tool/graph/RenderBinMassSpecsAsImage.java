package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.graph;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter;
import edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.plot.SpectraChart;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;

/**
 * render all bins in this file as jpeg image
 * @author wohlgemuth
 *
 */
public class RenderBinMassSpecsAsImage extends BasicProccessable {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public String getFolder() {
		return "graph/bins";
	}

	@Override
	public DataFile process(ResultDataFile datafile, Element configuration) throws BinBaseException {
		List<HeaderFormat<String>> bins = datafile.getBins(true);

		logger.info("creating massspecs for " + bins.size() + " bins");
		for (HeaderFormat<String> bin : bins) {
			String spectra = bin.getAttributes().get("spectra");
			String name = bin.getAttributes().get("name");

			try {
				writeJPEGImage(createMassSpec(spectra, 800, 600,name), name + ":" + bin.getAttributes().get("id"));
			}
			catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		logger.info("done");
		
		return null;
	}

	/**
	 * creates the actual spectra chart
	 * 
	 * @param spectra
	 * @param width
	 * @param height
	 * @return
	 */
	public BufferedImage createMassSpec(String spectra, int width, int height, String name) {

		// load the data into a dataset
		BinBaseXYDataSet dataset = new BinBaseXYDataSet();

		// get the spectra from the parameters
		Collection<Ion> c = SpectraConverter.stringToCollection(spectra);

		// parse the string
		double[] masses = new double[c.size()];
		double[] intensities = new double[c.size()];

		int maxIon = 0;
		int counter = 0;
		for (Ion o : c) {
			masses[counter] = o.getMass();
			intensities[counter] = o.getRelativeIntensity();
			counter++;

			// just to set the scale to a smart value
			if (o.getMass() > maxIon) {
				if (o.getRelativeIntensity() > 0.5) {
					maxIon = o.getMass();
				}
			}
		}

		// add our created dataset
		dataset.addDataSet(masses, intensities, "spec");

		// the chart it self
		JFreeChart chart = SpectraChart.createChart(dataset);
		chart.removeLegend();
		chart.setTitle(name);

		// sets the background color
		chart.setBackgroundPaint(Color.white);

		// set the axis to 110%
		ValueAxis axis = chart.getXYPlot().getRangeAxis();
		axis.setRange(0, 110);

		// set the x axis to 35 - x
		axis = chart.getXYPlot().getDomainAxis();
		axis.setRange(30, maxIon + 5);

		BufferedImage image = chart.createBufferedImage(width, height);
  
		chart = null;
		
		return image;
	}
	
	public String getDescription(){
		return "renders the bin massspec for each bin in this dataset as graph";
	}
}
