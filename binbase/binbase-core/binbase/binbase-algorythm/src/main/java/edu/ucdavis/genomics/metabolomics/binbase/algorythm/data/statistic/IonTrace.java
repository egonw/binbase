package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Vector;

import net.sf.mzmine.datastructures.RawDataAtNode;
import net.sf.mzmine.datastructures.Scan;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ZeroObject;

/**
 * used to create an ion trace for a chrommatogramm
 * @author wohlgemuth
 *
 */
public class IonTrace {

	private Logger logger = Logger.getLogger(getClass());
	
	/**
	 * generates the ion trace and returns it as datafile
	 * @param sample
	 * @return
	 */
	public DataFile generate(String sample){
		File file = getFile(sample);
		SimpleDatafile result = new ResultDataFile();
		
		RawDataAtNode node = new RawDataAtNode(0, file);
		node.setWorkingCopy(file);
		node.preLoad();
		node.initializeScanBrowser(0, node.getNumberOfScans());


		//create the header row
		result.addRow(createHeader(node));
		
		for (int i = 0; i < node.getNumberOfScans(); i++) {
			result.addRow(createContentRow(node));
		}
		
		return result;
	}
	
	/**
	 * create a row with ion scans
	 * @param node
	 */
	private List <FormatObject<Double>> createContentRow(RawDataAtNode node) {
		//create and attach a datarow
		Scan s = node.getNextScan();
		List <FormatObject<Double>>row = new Vector<FormatObject<Double>>();
		
		double mz[] = s.getMZValues();
		double intensity[] = s.getIntensityValues();

		row.add(new HeaderFormat<Double>(new Double(s.getScanNumber())));
		row.add(new HeaderFormat<Double>(new Double(node.getScanTime(s.getScanNumber()))));
		
		//fill the row with 0 objects
		for(double i = node.getMinMZValue(); i <= node.getMaxMZValue(); i++){
			row.add(new ZeroObject<Double>(0.0));
		}
		
		//now set the value for the given scans
		for(double i = node.getMinMZValue(); i <= node.getMaxMZValue(); i++){
			
			for(int x = 0; x < mz.length; x++){
				if(mz[x] == i){
					int index = (int)(i - node.getMinMZValue()) + 2;	
					row.set(index,new ContentObject<Double>(intensity[x]));
					break;
				}
			}
		}
		return row;
	}

	/**
	 * generate the header for this file
	 * @param result
	 * @param node
	 */
	private List<FormatObject<String>> createHeader(RawDataAtNode node) {
		List<FormatObject<String>> header = new Vector<FormatObject<String>>();
		
		header.add(new HeaderFormat<String>("ScanNumber"));
		header.add(new HeaderFormat<String>("RetentionTime"));
		
		for(double i = node.getMinMZValue(); i <= node.getMaxMZValue(); i++){
			header.add(new HeaderFormat<String>(String.valueOf(i)));	
		}
		
		return header;
	}
	
	private File getFile(String nameOfSample) {
		logger.info("aquire file: " + nameOfSample);
		try {

			File file = File.createTempFile(nameOfSample, "cdf");
			file.deleteOnExit();
			BinBaseService service = (BinBaseService) EjbClient.getRemoteEjb(Configurator.BCI_GLOBAL_NAME, BinBaseServiceBean.class);
			byte[] content = service.getNetCdfFile(nameOfSample,KeyFactory.newInstance().getKey());

			FileOutputStream out = new FileOutputStream(file);
			out.write(content);
			out.flush();
			out.close();
			logger.info("done");
			return file;
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		logger.info("not found!");
		return new File(nameOfSample);
	}

}
