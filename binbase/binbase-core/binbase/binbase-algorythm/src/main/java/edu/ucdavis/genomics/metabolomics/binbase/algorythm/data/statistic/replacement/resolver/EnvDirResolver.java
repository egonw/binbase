package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver;

import java.io.File;
import java.net.URI;

import org.jboss.logging.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.EnviormentVariables;

/**
 * access an enviorement variable and trys to load
 * 
 * @author wohlgemuth
 */
public class EnvDirResolver extends AbstractFileResolver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public File resolveNetcdfFile(String sampleName) {

		return createProtectedFile(new File(getFile(System.getProperty(EnviormentVariables.BINBASE_LOCAL_NETCDF_DIR), sampleName).getAbsolutePath()));
	}

	public String toString() {
		return getClass().getName() + " - variable:  " + EnviormentVariables.BINBASE_LOCAL_NETCDF_DIR + " dir: "
				+ System.getProperty(EnviormentVariables.BINBASE_LOCAL_NETCDF_DIR);
	}
}

