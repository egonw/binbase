/*
 * Created on Jan 20, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.export;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.binbase.bci.export.ExportService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.export.ExportServiceFactory;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Report;

/**
 * creates an seql service factory
 * 
 * @author wohlgemuth
 * @version Jan 20, 2006
 * 
 */
public class SQLExportServiceFactoryImpl extends ExportServiceFactory {

	public SQLExportServiceFactoryImpl() {
		super();
	}

	public ExportService createService(Map p,Report report) throws ConfigurationException {
		SQLExportService service = new SQLExportService(ConnectionFactory.getFactory().getConnection(),report);
		return service;
	}

}
