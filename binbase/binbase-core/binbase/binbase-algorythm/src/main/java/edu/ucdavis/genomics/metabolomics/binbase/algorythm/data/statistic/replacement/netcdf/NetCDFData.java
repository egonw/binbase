package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.netcdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.util.io.Copy;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.math.Regression;
import edu.ucdavis.genomics.metabolomics.util.math.SpectraArrayKey;

/**
 * defines basic access to netcdf files related to the binbase
 * 
 * @author wohlgemuth
 * 
 */
public abstract class NetCDFData implements SpectraArrayKey {

	/**
	 * method to create a file from a source in case our implementation can only
	 * work on files on not streams
	 * 
	 * @param source
	 * @return
	 * @throws IOException
	 */
	protected static File getFile(Source source) throws IOException {
		File file = null;
		/*
		 * if (source instanceof FileSource) { return ((FileSource)
		 * source).getFile(); } else
		 */{
			try {
				file = File.createTempFile(source.getSourceName(), ".binbase",
						new File(System.getenv("TMPDIR")));
			} catch (Exception e) {
				file = File.createTempFile(source.getSourceName(), ".binbase");
			}

			file.deleteOnExit();
			Copy.copy(source.getStream(), new FileOutputStream(file));
			return file;
		}
	}

	private double precision = 0.5;

	/**
	 * default to load and initialize a file
	 * 
	 * @param source
	 * @throws IOException
	 */
	public NetCDFData(Source source) throws IOException {
		prepare(source);
		load(source);
	}

	/**
	 * loads a file
	 * 
	 * @param source
	 * @throws IOException
	 */
	protected abstract void load(Source source) throws IOException;

	/**
	 * prepare stuff
	 * 
	 * @author wohlgemuth
	 * @version Nov 6, 2006
	 * @param source
	 * @throws IOException
	 */
	protected void prepare(Source source) throws IOException {

	}

	/**
	 * returns the given spectra at this point or null
	 * 
	 * @param retentionTime
	 * @return
	 */
	public final double[][] getSpectra(double retentionTime) {
		return getSpectra(retentionTime, precision);
	}

	/**
	 * returns the spectra at this time +/- the given precision
	 * 
	 * @param retentionTime
	 * @param precision
	 * @return
	 */
	public final double[][] getSpectra(double retentionTime, double precision) {
		return getSpectra(retentionTime, precision, false);
	}

	/**
	 * returns all spectra at the given time combined to one entry
	 * 
	 * @param retentionTime
	 * @param precision
	 * @param combine
	 * @return
	 */
	public abstract double[][] getSpectra(double retentionTime,
			double precision, boolean combine);

	/**
	 * close the file and realease ressources
	 * 
	 * @author wohlgemuth
	 * @version Nov 8, 2006
	 */
	public abstract void close();

	/**
	 * sets the curve which should be used for correcting the times in this file
	 * so we can access them using retentionIndex instead of retentiontimes
	 * 
	 * we than calculate the right retention time from the given retention index
	 * 
	 * @author wohlgemuth
	 * @version Nov 8, 2006
	 */
	public abstract void setCorrectionCurve(Regression regression);
	
	/**
	 * contains for each ion the calculated noise
	 * @author wohlgemuth
	 * @version Nov 9, 2006
	 * @return
	 */
	public abstract Map<Integer, Double> getNoise();
	
	/**
	 * returns the noise for a given range
	 * @author wohlgemuth
	 * @version Nov 14, 2006
	 * @param time time of masspec of interesst
	 * @param range range for noise finding
	 * @return
	 */
	public abstract Map<Integer, Double> getNoise(double time, int range);

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}
	
	
}
