package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import;

import java.sql.Connection;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;

public class MultiThreadClassImporterFactoryImpl extends ClassImporterFactory{

	@Override
	public ClassImporter createImporter(Connection connection) throws ConfigurationException {
		ClassImporter i = new MultithreadClassImporter();
		i.setConnection(connection);
		i.setReport(this.getReport());
		return i;
	}

}
