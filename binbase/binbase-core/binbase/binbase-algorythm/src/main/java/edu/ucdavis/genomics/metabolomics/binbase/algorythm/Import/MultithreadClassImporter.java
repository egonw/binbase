package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.BinBaseExperimentImportSample;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.methods.CorrectionMethod;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.methods.GitterMethode;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.methods.Methodable;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.thread.ExecutorsServiceFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.FileStoreUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.exception.ValidationException;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;

/**
 * executes imports and exports in paralell
 * 
 * @author wohlgemuth
 */
public class MultithreadClassImporter extends SingleThreadClassImporter {

	@Override
	protected void prepareStatements() throws Exception {
		CLASS = SingleThreadClassImporter.class.getName();

		super.prepareStatements();
	}

	@Override
	protected void doCorrection(final BinBaseExperimentImportSample[] samples, List<BinBaseExperimentImportSample> failed) throws Exception {

		final ExecutorService service = ExecutorsServiceFactory.createService();

		List<Future<BinBaseExperimentImportSample>> futures = new ArrayList<Future<BinBaseExperimentImportSample>>();

		for (final BinBaseExperimentImportSample sample : samples) {

			Callable<BinBaseExperimentImportSample> call = new Callable<BinBaseExperimentImportSample>() {

				public BinBaseExperimentImportSample call() throws Exception {

					final ConnectionFactory factory = createFactory();
					final Connection connection = factory.getConnection();
					final Methodable method = new CorrectionMethod();
					method.setNewBinAllowed(false);
					method.setConnection(connection);

					try {
						if (correctSingleSample(sample, method)) {
							return sample;
						}
					}
					finally {
						factory.close(connection);
					}

					return null;
				}

			};

			futures.add(service.submit(call));
		}

		ExecutorsServiceFactory.shutdownService(service);

		for (Future<BinBaseExperimentImportSample> fut : futures) {
			if (fut.get() != null) {
				failed.add(fut.get());
			}
		}
	}

	@Override
	protected int[] importSample(final BinBaseExperimentImportSample[] samples, final String classname) throws Exception {

		final int sampleIds[] = new int[samples.length];

		final ExecutorService service = ExecutorsServiceFactory.createService();

		for (int i = 0; i < samples.length; i++) {

			final int counter = i;

			service.submit(new Callable<Object>() {

				@Override
				public Object call() throws Exception {

					final ConnectionFactory factory = createFactory();
					final Connection connection = factory.getConnection();
					Importer importer = new Importer();
					importer.setConnection(connection);
					try {
						try {
							importSingleSample(samples[counter], classname, importer, sampleIds, counter);
						}
						catch (Exception e) {
							logger.error(e.getMessage(), e);
						}
					}
					finally {
						factory.close(connection);
					}

					return null;

				}

			});

		}

		ExecutorsServiceFactory.shutdownService(service);
		return sampleIds;
	}

	@Override
	protected void matchSamples(final BinBaseExperimentImportSample[] samples) throws Exception {
		final ExecutorService service = ExecutorsServiceFactory.createService();

		for (final BinBaseExperimentImportSample sample : samples) {

			service.submit(new Callable<Object>() {

				@Override
				public Object call() throws Exception {

					final ConnectionFactory factory = createFactory();
					final Connection connection = factory.getConnection();
					Methodable method = new GitterMethode();
					method.setNewBinAllowed(false);
					method.setConnection(connection);
					try {
						try {
							doMatch(sample, method);
						}
						catch (Exception e) {
							logger.error(e.getMessage(), e);
						}
					}
					finally {
						factory.close(connection);
					}
					return null;
				}
			});
		}

		ExecutorsServiceFactory.shutdownService(service);
	}

	@Override
	protected void validateSources(BinBaseExperimentImportSample[] samples) throws ConfigurationException, ValidationException {

		ExecutorService service = ExecutorsServiceFactory.createService();

		for (final BinBaseExperimentImportSample sample : samples) {
			service.submit(new Callable<Object>() {

				@Override
				public Object call() throws Exception {
					vaildateSingleSource(sample);
					return null;
				}
			});
		}
		ExecutorsServiceFactory.shutdownService(service);
	}

	@Override
	protected void storeImportData(final ExperimentClass experiment) throws Exception {

		final ExecutorService service = ExecutorsServiceFactory.createService();

		for (final ExperimentSample sample : experiment.getSamples()) {

			service.submit(new Callable<Object>() {

				@Override
				public Object call() throws Exception {
					Thread.currentThread().setName("storing: " + sample.getName());

					final FileStoreUtil util = new FileStoreUtil();
					final ConnectionFactory factory = createFactory();
					final Connection connection = factory.getConnection();

					try {
						try {
							util.storeFile(getReport(), sample.getName(), connection);
						}
						catch (Exception e) {
							logger.error(e.getMessage(), e);
						}
					}
					finally {
						factory.close(connection);
					}
					return null;
				}
			});

		}
		ExecutorsServiceFactory.shutdownService(service);

	}

	protected ConnectionFactory createFactory() throws RemoteException, BinBaseException, CreateException, NamingException {
		ConnectionFactory factory = ConnectionFactory.createFactory();
		Properties p = Configurator.getDatabaseService().createProperties();
		p.setProperty(SimpleConnectionFactory.KEY_USERNAME_PROPERTIE, getCurrentClass().getColumn());

		factory.setProperties(p);

		return factory;
	}
}
