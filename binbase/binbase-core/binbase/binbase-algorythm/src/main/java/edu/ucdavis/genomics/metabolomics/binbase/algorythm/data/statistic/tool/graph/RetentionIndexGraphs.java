package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.graph;

import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.jboss.logging.Logger;
import org.jdom.Element;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.methods.correction.CorrectionCache;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.math.Regression;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.SampleObject;

/**
 * generates the ri curves for all samples in this experiment
 * 
 * @author wohlgemuth
 */
public class RetentionIndexGraphs extends BasicProccessable {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public String getFolder() {
		return "graph/curves";
	}

	@Override
	public DataFile process(final ResultDataFile datafile, Element configuration) throws BinBaseException {

		logger.info("generation RI-Curves...");

		CorrectionCache cache = new CorrectionCache();

		ConnectionFactory factory = ConnectionFactory.getFactory();
		try {
			Connection connection = this.createDatabaseConnection(datafile.getDatabase(), factory);
			cache.setConnection(connection);
			PreparedStatement statement = connection.prepareStatement("select retention_time from spectra where sample_id = ?");

			try {
				// generate for each sample a value
				for (SampleObject<String> sample : datafile.getSamples()) {
					int id = Integer.parseInt(sample.getAttributes().get("id"));

					int correctionId = cache.getCorrectionId(id);
					Regression regression = cache.getRiVsRTRegression(correctionId);

					logger.info("found id: " + id + " and curve");

					final List<Integer> bins = cache.getBinIdsInCorrectionForSample(correctionId);

					XYSeries calibration = new XYSeries("calibration");
					XYSeries assumed = new XYSeries("corrected values");

					// calculated all the graph data
					statement.setInt(1, id);
					ResultSet set = statement.executeQuery();

					// needed to position elements
					double min = Double.MAX_VALUE;
					double max = Double.MIN_VALUE;
					double minY = Double.MAX_VALUE;

					while (set.next()) {

						double value = set.getInt(1);
						double yV = regression.getY(set.getInt(1));
						assumed.add(set.getInt(1), yV);

						if (value < min) {
							min = value;
						}
						if (value > max) {
							max = value;
						}
						if (yV < minY) {
							minY = yV;
						}
					}

					set.close();

					double[] x = regression.getXData();
					double[] y = regression.getYData();

					for (int i = 0; i < x.length; i++) {
						double value = x[i];

						calibration.add(x[i], y[i]);

						if (value < min) {
							min = value;
						}
						if (value > max) {
							max = value;
						}

					}
					logger.info("min value: " + min);
					XYSeriesCollection dataset = new XYSeriesCollection();
					dataset.addSeries(calibration);
					dataset.addSeries(assumed);

					// actual chart
					JFreeChart chart = ChartFactory.createXYLineChart(sample.getAttributes().get("name"), "bin RI data", "calculated RI", dataset,
							PlotOrientation.VERTICAL, true, true, false);
					chart.setBackgroundPaint(Color.white);

					// generate the formulas to display them in the chart
					int offset = 1;
					for (String s : regression.getFormulas()) {
						XYTextAnnotation marker = new XYTextAnnotation(s, min, minY - (50000 * offset));
						marker.setPaint(Color.BLACK);
						marker.setTextAnchor(TextAnchor.BOTTOM_LEFT);
						offset = offset + 1;

						((XYPlot) chart.getPlot()).addAnnotation(marker);

					}

					// renderers
					XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
					renderer.setSeriesLinesVisible(0, false);
					renderer.setSeriesShapesVisible(0, true);
					renderer.setSeriesItemLabelsVisible(0, true);
					renderer.setSeriesLinesVisible(1, true);
					renderer.setSeriesShapesVisible(1, false);

					// we want tool tips for the first series
					renderer.setSeriesItemLabelGenerator(0, new XYItemLabelGenerator() {

						@Override
						public String generateLabel(XYDataset dataset, int series, int item) {
							return datafile.getBin(bins.get(item)).getAttributes().get("name");
						}
					});

					// begin markers
					IntervalMarker begin = new IntervalMarker(x[0], x[1]);
					begin.setLabel("begin: linear correction");
					begin.setPaint(new Color(245, 255, 250));
					begin.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
					begin.setLabelTextAnchor(TextAnchor.TOP_CENTER);

					// end markers
					IntervalMarker end = new IntervalMarker(x[x.length - 2], x[x.length - 1]);
					end.setLabel("end: linear correction");
					end.setPaint(new Color(245, 255, 250));
					end.setLabelAnchor(RectangleAnchor.TOP_LEFT);
					end.setLabelTextAnchor(TextAnchor.TOP_CENTER);

					// middle markers
					IntervalMarker poly = new IntervalMarker(x[1], x[x.length - 2]);
					poly.setLabel("middle: polynomial correction");
					poly.setPaint(new Color(230, 230, 250));
					poly.setLabelAnchor(RectangleAnchor.TOP);
					poly.setLabelTextAnchor(TextAnchor.TOP_CENTER);

					// sets the marker
					((XYPlot) chart.getPlot()).addDomainMarker(poly, Layer.BACKGROUND);
					((XYPlot) chart.getPlot()).addDomainMarker(begin, Layer.BACKGROUND);
					((XYPlot) chart.getPlot()).addDomainMarker(end, Layer.BACKGROUND);

					// set the renderer
					((XYPlot) chart.getPlot()).setRenderer(renderer);

					// write to file
					writeJPEGImage(chart.createBufferedImage(800, 600), sample.getAttributes().get("name"));

				}
			}
			finally {
				factory.close(connection);
			}
		}
		catch (Exception e) {
			throw new BinBaseException(e.getMessage(), e);
		}

		return null;
	}
	
	public String getDescription(){
		return "generates a graph of every correction curve in this dataset";
	}
}
