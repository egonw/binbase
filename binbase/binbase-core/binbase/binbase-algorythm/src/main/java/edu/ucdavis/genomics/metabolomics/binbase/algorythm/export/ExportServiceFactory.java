/*
 * Created on Nov 18, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.export;

import java.util.Properties;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.config.Configable;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;

/**
 * factory to create export services
 * @author wohlgemuth
 * @version Nov 18, 2005
 *
 */
public abstract class ExportServiceFactory {

	/**
	 * name of the property
	 */
	public static final String DEFAULT_PROPERTY_NAME = "edu.ucdavis.genomics.metabolomics.binbase.algorythm.export.ExportServiceFactory";

	private static String foundFactory = null;

	public ExportServiceFactory() {
		super();
	}

	/**
	 * returns an new instance of the factory
	 * 
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static ExportServiceFactory newInstance() {
		// Locate Factory
		foundFactory = findFactory(DEFAULT_PROPERTY_NAME, SimpleExportServiceFactoryImpl.class.getName());

		return newInstance(foundFactory);
	}

	/**
	 * returns an new instance of the specified factory
	 * 
	 * @param factoryClass
	 *            the specified factory to use
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static ExportServiceFactory newInstance(String factoryClass) {
		Class classObject;
		ExportServiceFactory factory;

		try {
			classObject = Class.forName(factoryClass);
			factory = (ExportServiceFactory) classObject.newInstance();
			return factory;

		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}

	/**
	 * creates the classimporter it self
	 * 
	 * @author wohlgemuth
	 * @version Nov 15, 2005
	 * @return
	 * @throws ConfigurationException
	 */
	public abstract ExportService createService() throws ConfigurationException;

	/**
	 * finds our factory
	 * 
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @param property
	 * @param defaultValue
	 * @return
	 */
	private static String findFactory(String property, String defaultValue) {
		String factory;

		// Check System Property
		factory = System.getProperty(property);
		if (factory == null) {
			// Check Configurator
			try {
				XMLConfigable config = Configable.CONFIG;
				Properties p = config.getConfigProvider().getProperties();
				factory = p.getProperty(property);
			} catch (Exception e) {
			}
		}

		if (factory == null) {
			factory = defaultValue;
		}

		return factory;
	}

}
