package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.thread.ExecutorsServiceFactory;

/**
 * runs all diagnostics tasks in the background to increase performance
 * 
 * @author wohlgemuth
 * 
 */
public class AsyncDiagnosticsService extends AbstractDiagnosticService {

	private static final String ASYNC_DIAGNOSTICS_SERVICE_THREAD_NAME = "async-diagnostics-service";

	private DiagnosticsService service;

	/**
	 * we only need one thread for this service, since all service share this
	 * one
	 */
	private static ExecutorService executorService;
	
	private static SynchronousQueue<Runnable> queue;

	static {
		queue = new SynchronousQueue<Runnable>();
		executorService = new ThreadPoolExecutor(
			    1, 1, 
			    1000, TimeUnit.SECONDS, 
			    queue,
			    new ThreadPoolExecutor.DiscardPolicy());
	}

	@Override
	protected void finalize() throws Throwable {
		if(executorService != null){
			if(queue.isEmpty()){
				ExecutorsServiceFactory.shutdownService(executorService);
				executorService = null;
			}
		}
		super.finalize();
	}

	public AsyncDiagnosticsService(DiagnosticsService service) {
		this.service = service;
	}

	@Override
	public void diagnosticAction(final String className, final int sampleId,
			final int spectraId, final int binId, final Class<?> caller,
			final Step stepOfAlgorithm,
			final Reason reasonForTheArchivedResult,
			final Result resultOfAction, final Object[] parameters) {

		executorService.submit(new Runnable() {

			@Override
			public void run() {
				Thread.currentThread().setName(
						ASYNC_DIAGNOSTICS_SERVICE_THREAD_NAME);
				service.diagnosticAction(className, sampleId, spectraId, binId,
						caller, stepOfAlgorithm, reasonForTheArchivedResult,
						resultOfAction, parameters);
			}
		});
	}

	@Override
	public void diagnosticAction(final int sampleId, final int spectraId,
			final int binId, final Class<?> caller, final Step stepOfAlgorithm,
			final Reason reasonForTheArchivedResult,
			final Result resultOfAction, final Object[] parameters) {

		executorService.submit(new Runnable() {

			@Override
			public void run() {
				Thread.currentThread().setName(
						ASYNC_DIAGNOSTICS_SERVICE_THREAD_NAME);
				service.diagnosticAction(sampleId, spectraId, binId, caller,
						stepOfAlgorithm, reasonForTheArchivedResult,
						resultOfAction, parameters);
			}
		});
	}

	@Override
	public void diagnosticAction(final int spectraId, final Class<?> caller,
			final Step stepOfAlgorithm,
			final Reason reasonForTheArchivedResult,
			final Result resultOfAction, final Object[] parameters) {

		executorService.submit(new Runnable() {

			@Override
			public void run() {
				Thread.currentThread().setName(
						ASYNC_DIAGNOSTICS_SERVICE_THREAD_NAME);
				service.diagnosticAction(spectraId, caller, stepOfAlgorithm,
						reasonForTheArchivedResult, resultOfAction, parameters);
			}
		});
	}

	@Override
	public void diagnosticAction(final int spectraId, final int binId,
			final Class<?> caller, final Step stepOfAlgorithm,
			final Reason reasonForTheArchivedResult,
			final Result resultOfAction, final Object[] parameters) {

		executorService.submit(new Runnable() {

			@Override
			public void run() {
				Thread.currentThread().setName(
						ASYNC_DIAGNOSTICS_SERVICE_THREAD_NAME);
				service.diagnosticAction(spectraId, binId, caller,
						stepOfAlgorithm, reasonForTheArchivedResult,
						resultOfAction, parameters);
			}
		});
	}

}
