package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

public class AsyncLog4JDiagnosticsServiceFactory extends DiagnosticsServiceFactory {

	private DiagnosticsService service = new AsyncDiagnosticsService(new Log4JDiagnosticsService());

	@Override
	public DiagnosticsService createService() {
		return service ;
	}

}
