/*
 * Created on Nov 15, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import;

import java.sql.Connection;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportBasedFactory;

/**
 * a factory which provides us with the correct implementation of the needed
 * factory
 * 
 * @author wohlgemuth
 * @version Nov 15, 2005
 * 
 */
public abstract class ClassImporterFactory extends ReportBasedFactory {

	
	/**
	 * name of the property
	 */
	public static final String DEFAULT_PROPERTY_NAME = ClassImporterFactory.class.getName();

	public ClassImporterFactory() {
		super();
	}

	/**
	 * returns an new instance of the factory
	 * 
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static ClassImporterFactory newInstance(Report report) {
		return newInstance(findFactory(DEFAULT_PROPERTY_NAME, SingleThreadClassImporterFactoryImpl.class.getName()),report);
	}

	/**
	 * returns an new instance of the specified factory
	 * 
	 * @param factoryClass
	 *            the specified factory to use
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static ClassImporterFactory newInstance(String factoryClass,Report report) {
		Class classObject;
		ClassImporterFactory factory;

		try {
			classObject = Class.forName(factoryClass);
			factory = (ClassImporterFactory) classObject.newInstance();
			factory.setReport(report);
			return factory;
		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}

	/**
	 * creates the classimporter it self
	 * 
	 * @author wohlgemuth
	 * @version Nov 15, 2005
	 * @return
	 * @throws ConfigurationException
	 */
	public abstract ClassImporter createImporter(Connection connection) throws BinBaseException;
}
