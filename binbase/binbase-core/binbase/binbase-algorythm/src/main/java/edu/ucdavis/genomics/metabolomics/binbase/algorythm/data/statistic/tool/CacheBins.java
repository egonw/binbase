package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool;

import java.io.BufferedInputStream;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;

/**
 * is used to cache bins
 * 
 * @author wohlgemuth
 */
public class CacheBins extends BasicProccessable implements Processable {

	private Logger logger = Logger.getLogger(getClass());

	public String getFolder() {
		return "none";
	}

	@Override
	public boolean writeResultToFile() {
		return false;
	}

	public DataFile process(ResultDataFile datafile, Element configuration)
			throws BinBaseException {
		List<HeaderFormat<String>> list = datafile.getBins();

		for (HeaderFormat<String> header : list) {
			String url = header.getAttributes().get("url");
			logger.info(url);

			try {
				URL u = new URL(url);
				Scanner scanner = new Scanner(new BufferedInputStream(u
						.openConnection().getInputStream()));
				StringBuffer result = new StringBuffer();
				while (scanner.hasNextLine()) {
					result.append(scanner.nextLine());
				}
				;
				logger.trace(result);

			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}
		return null;
	}

	public String getDescription() {
		return "caches all the bins in the database system";
	}
}
