package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.sql;

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.DateUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;

/**
 * adds some optimizations for database based calculations instead of file based
 * calculations
 * 
 * @author wohlgemuth
 */
public class SQLResultDataFile extends ResultDataFile {

	/**
	 * calculates all dates in this result set
	 * 
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	protected Set<Long> caculateDates(Connection connection) throws SQLException {
		PreparedStatement statement = connection
				.prepareStatement("select distinct date from result_link a, samples b where result_id = ? and a.sample_id = b.sample_id");
		statement.setInt(1, this.getResultId());

		ResultSet result = statement.executeQuery();
		Set<Long> set = new HashSet<Long>();

		while (result.next()) {

			long time = DateUtil.stripTime(result.getDate(1)).getTime();
			getLogger().info("retrieved time: " + time);
			set.add(time);
		}
		result.close();

		return set;
	}

	@Override
	protected void fillRICache() throws ConfigurationException {

		getLogger().debug("database: " + this.getDatabase());
		getLogger().debug("result: " + this.getResultId());
		
		try {
			long begin = System.currentTimeMillis();

			ConnectionFactory factory = ConnectionFactory.getFactory();

			Properties p = Configurator.getDatabaseService().createProperties();
			p.setProperty(SimpleConnectionFactory.KEY_USERNAME_PROPERTIE, this.getDatabase());
			Connection connection = factory.getConnection();

			getLogger().debug("calculate times for bins...");
			calculateTimesForBins(connection);

			getLogger().debug("calculate dates...");
			Set<Long> dates = caculateDates(connection);

			getLogger().debug("calculate times for date and current result...");

			for (Long date : dates) {
				calculateTimesForDateAndCurrentResult(connection, date);
			}
			
			long end = System.currentTimeMillis();
			
			getLogger().debug("needed time: " + ((end - begin)/1000/60) + " minutes");
		}
		catch (SQLException e) {
			throw new ConfigurationException(e.getMessage(), e);
		}
		catch (RemoteException e) {
			throw new ConfigurationException(e.getMessage(), e);
		}
		catch (BinBaseException e) {
			throw new ConfigurationException(e.getMessage(), e);
		}
		catch (CreateException e) {
			throw new ConfigurationException(e.getMessage(), e);
		}
		catch (NamingException e) {
			throw new ConfigurationException(e.getMessage(), e);
		}
	}

	/**
	 * calculates the times for the given date
	 * 
	 * @param connection
	 * @param date
	 * @throws SQLException
	 */
	private void calculateTimesForDate(Connection connection, long date) throws SQLException {
		PreparedStatement statement = connection
				.prepareStatement("select avg(retention_time) as \"rt\",avg(retention_index) as \"ri\",bin_id,count(\"bin_id\") as co from spectra a, samples b where a.sample_id = b.sample_id and bin_id is not null and b.date = ? and b.visible = 'TRUE' and correction_failed = 'FALSE' group by bin_id");
		getLogger().debug(date + " - " + this.getResultId());
		
		statement.setDate(1, new java.sql.Date(date));
		ResultSet res = statement.executeQuery();

		Map<Integer, Double> riMap = new HashMap<Integer, Double>();
		Map<Integer, Double> rtMap = new HashMap<Integer, Double>();

		while (res.next()) {
			riMap.put(res.getInt(3), res.getDouble(2));
			rtMap.put(res.getInt(3), res.getDouble(1));
			getLogger().debug("average ri/rt for bin " + res.getInt(3) + " was " + res.getDouble(2) + "/" + res.getDouble(1) + " and was calculated over " + res.getInt(4) + " samples");
		}

		res.close();

		this.cacheRIDate(riMap, date);
		this.cacheRTDate(rtMap, date);

	}


	/**
	 * calculates the times for the given date
	 * 
	 * @param connection
	 * @param date
	 * @throws SQLException
	 */
	private void calculateTimesForDateAndCurrentResult(Connection connection, long date) throws SQLException {
		getLogger().info("calculating retention time...");
		PreparedStatement statement = connection
				.prepareStatement("select avg(retention_time) as \"rt\",avg(retention_index) as \"ri\",bin_id,count(\"bin_id\") as co from spectra a, samples b, result_link c where a.sample_id = b.sample_id and b.sample_id = c.sample_id and bin_id is not null and date_trunc('day',b.date::date) = date_trunc('day',?::date) and b.visible = 'TRUE' and correction_failed = 'FALSE' and result_id = ? group by bin_id");
		getLogger().debug(date + " - " + this.getResultId());
		
		statement.setTimestamp(1, new java.sql.Timestamp(date));
		statement.setInt(2, this.getResultId());

		ResultSet res = statement.executeQuery();

		Map<Integer, Double> riMap = new HashMap<Integer, Double>();
		Map<Integer, Double> rtMap = new HashMap<Integer, Double>();

		getLogger().debug("fetching results...");
		while (res.next()) {
			riMap.put(res.getInt(3), res.getDouble(2));
			rtMap.put(res.getInt(3), res.getDouble(1));
			getLogger().debug("average ri/rt for bin " + res.getInt(3) + " was " + res.getDouble(2) + "/" + res.getDouble(1) + " and was calculated over " + res.getInt(4) + " samples");
		}

		res.close();

		this.cacheRIDate(riMap, date);
		this.cacheRTDate(rtMap, date);

	}
	/**
	 * calculates the times for the bins
	 * 
	 * @param connection
	 * @throws SQLException
	 */
	private void calculateTimesForBins(Connection connection) throws SQLException {
		PreparedStatement statement = connection
				.prepareStatement("select avg(retention_time) as \"rt\",avg(retention_index) as \"ri\",bin_id from spectra a, result_link b where a.sample_id = b.sample_id and bin_id is not null and result_id = ? group by bin_id");

		getLogger().debug("result it: " + this.getResultId());
		statement.setInt(1, this.getResultId());

		ResultSet result = statement.executeQuery();

		while (result.next()) {
			this.addToAverageRetentionTimeCache(result.getInt(3), result.getDouble(1));
			this.addToAverageRetentionIndexCache(result.getInt(3), result.getDouble(2));
		}
		result.close();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
