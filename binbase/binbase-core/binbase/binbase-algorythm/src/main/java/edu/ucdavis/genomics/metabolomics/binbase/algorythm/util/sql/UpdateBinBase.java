package edu.ucdavis.genomics.metabolomics.binbase.algorythm.util.sql;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.PatternException;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.SampleDate;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.config.source.DatabaseConfigSource;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXProvider;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.SQLObject;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;

/**
 * recalculates several properties of the binbase and can take hours or days, so
 * run only if neccesseary
 * 
 * @author wohlgemuth
 * 
 */
public class UpdateBinBase extends SQLObject {

	private boolean regenerateSampleProperties = true;

	private boolean regenerateSampleMetaInfos = true;

	private boolean visibleSamplesOnly = false;

	private PreparedStatement update = null;

	private PreparedStatement select = null;

	PreparedStatement dropClassInfo;
	PreparedStatement insertClassInfo;

	private Logger logger = Logger.getLogger(getClass());

	private PreparedStatement selectClassKey;

	private PreparedStatement selectSampleIdsBySampleName;

	private PreparedStatement selectNextClassKeyId;

	private PreparedStatement insertClassKey;

	private PreparedStatement selectSampleNameBySampleId;

	/**
	 * connects to auth, fetches all databases and after wards updates all the
	 * sample informations
	 * 
	 * @throws ConfigurationException
	 * @throws Exception
	 */
	public void run() throws ConfigurationException, Exception {

		ServiceJMXFacade facade = Configurator.getImportService();
		SetupXProvider provider = SetupXFactory.newInstance().createProvider(
				System.getProperties());

		for (String database : facade.getDatabases()) {
			XMLConfigurator.getInstance().addConfiguration(
					new DatabaseConfigSource(XMLConfigurator.getInstance()
							.getProperties()));
			Properties p = XMLConfigurator.getInstance().getProperties();
			p.setProperty(SimpleConnectionFactory.KEY_USERNAME_PROPERTIE,
					database);

			ConnectionFactory factory = ConnectionFactory.getFactory();
			factory.setProperties(p);

			Connection c = factory.getConnection();
			this.setConnection(c);

			ResultSet set = select.executeQuery();

			try {
				while (set.next()) {
					try {

						int sampleID = set.getInt(2);

						String setupxID = null;
						try {
							setupxID = provider.getSetupXId(set.getString(1));
						} catch (Exception e) {
							logger
									.warn("was not able to connect to provides, skip updating of meta informations!, "
											+ e.getMessage());
						}

						if (setupxID != null) {
							// generates all sample properties new based on
							// the sample date
							generateSampleProperties(set, setupxID);

							// updates all sample metainformations based on
							// setupx
							generateMetaInformations(provider, sampleID,
									setupxID);
						}
					} catch (PatternException e) {
						logger.warn(e.getMessage());
					}
				}
			} catch (SQLException e) {
				logger.warn(e.getMessage(), e);
				c.rollback();
			}

			factory.close(c);

		}

	}

	/**
	 * prepares all needed sql statements
	 * 
	 * @param c
	 * @throws SQLException
	 */
	protected void prepareStatements() throws SQLException {
		Connection c = this.getConnection();

		if (isVisibleSamplesOnly()) {
			logger.info("only fetch visible samples");
			select = c.prepareStatement(SQL_CONFIG.getValue(CLASS
					+ ".visibleSample"));
		} else {
			logger.info("fetch all samples");
			select = c.prepareStatement(SQL_CONFIG.getValue(CLASS
					+ ".allSamples"));
		}

		update = c.prepareStatement(SQL_CONFIG.getValue(CLASS + ".update"));

		dropClassInfo = c.prepareStatement(SQL_CONFIG.getValue(CLASS
				+ ".dropClassInfo"));

		insertClassInfo = c.prepareStatement(SQL_CONFIG.getValue(CLASS
				+ ".insertClassInfo"));

		insertClassKey = c.prepareStatement(SQL_CONFIG.getValue(CLASS
				+ ".insertClassKey"));

		selectClassKey = c.prepareStatement(SQL_CONFIG.getValue(CLASS
				+ ".selectClassKey"));

		selectNextClassKeyId = c.prepareStatement(SQL_CONFIG
				.getValue("static.nextLinkId"));

		selectSampleIdsBySampleName = c.prepareStatement(SQL_CONFIG
				.getValue(CLASS + ".selectSampleIdsBySampleName"));
		
		selectSampleNameBySampleId = c.prepareStatement("select sample_name from samples where sample_id = ?");
	}

	public void generateMetaInformations(SetupXProvider provider,
			String sampleName, String setupxID) throws Exception, SQLException {
		this.selectSampleIdsBySampleName.setString(1, sampleName);

		ResultSet result = this.selectSampleIdsBySampleName.executeQuery();

		while (result.next()) {
			generateMetaInformations(provider, result.getInt(1), setupxID);
		}
	}

	public void generateMetaInformations(String xmlContent, String sampleName,
			String setupxID) throws Exception, SQLException {
		this.selectSampleIdsBySampleName.setString(1, sampleName);

		ResultSet result = this.selectSampleIdsBySampleName.executeQuery();

		while (result.next()) {
			generateMetaInformations(xmlContent, result.getInt(1), setupxID);
		}
	}

	public void generateMetaInformations(String xmlContent, int sampleID,
			String setupxID) throws Exception, SQLException {
		Connection c = this.getConnection();

		if (xmlContent != null) {

			Element xml = XmlHandling.readXml(new ByteArrayInputStream(
					xmlContent.getBytes()));

			logger.info("update class infos");
			if (xml.getChild("classInfo") != null) {
				Element classInfo = xml.getChild("classInfo");
				@SuppressWarnings("unused")
				List<Element> classInfos = classInfo.getChildren("atom");

				// delete outdated class infos
				dropClassInfo.setInt(1, sampleID);
				dropClassInfo.execute();

				for (int i = 0; i < classInfos.size(); i++) {
					// find key id or insert new id

					this.selectClassKey.setString(1, classInfos.get(i)
							.getAttribute("key").getValue());
					ResultSet next = this.selectClassKey.executeQuery();
					int keyId = 0;
					if (next.next()) {
						// key exist so use it
						keyId = next.getInt(1);
					} else {
						// create new key and use it
						ResultSet newKey = selectNextClassKeyId.executeQuery();
						newKey.next();
						keyId = newKey.getInt(1);

						insertClassKey.setInt(1, keyId);
						insertClassKey.setString(2, classInfos.get(i)
								.getAttribute("key").getValue());
						insertClassKey.execute();
					}

					next = this.selectClassKey.executeQuery();

					if (next.next()) {
						insertClassInfo.setInt(1, sampleID);
						insertClassInfo.setInt(2, keyId);
						insertClassInfo.setString(3, classInfos.get(i)
								.getText());
						insertClassInfo.setInt(4, next.getInt(1));
						insertClassInfo.execute();
					} else {
						logger.warn("couldn't generate key!");
					}
				}

			}

			logger.info("update run infos");
			if (xml.getChild("runInfo") != null) {
				Element runInfo = xml.getChild("runInfo");
				@SuppressWarnings("unused")
				List<Element> runInfos = runInfo.getChildren("atom");

				if (runInfos.isEmpty() == false) {
					StringBuffer buffer = new StringBuffer(
							"UPDATE samples SET ");
					java.util.Iterator<Element> it = runInfos.iterator();

					if (it.hasNext()) {

						boolean found = false;
						while (it.hasNext()) {
							Element next = it.next();

							String key = next.getAttribute("key").getValue();

							logger.debug(key);

							if (key.equals("Name") == false) {
								if (key.equals("Vial") == false) {
									if (key.equals("Type") == false) {
										if (key.equals("Folder") == false) {
											if (found == true) {
												buffer.append(",");
											}
											if (found == false) {
												found = true;
											}
											key = key.replaceAll(" ", "")
													.trim().toLowerCase();
											buffer
													.append("\"" + key
															+ "\" = ?");
										}
									}
								}
							}
						}
					}

					buffer.append(" where \"sample_id\" = ?");

					logger.info("generated sql: " + buffer.toString());
					it = runInfos.iterator();

					PreparedStatement updateSample = c.prepareStatement(buffer
							.toString());

					int counter = 1;
					if (it.hasNext()) {
						while (it.hasNext()) {
							Element next = it.next();

							String key = next.getAttribute("key").getValue();

							if (key.equals("Name") == false) {
								if (key.equals("Vial") == false) {
									if (key.equals("Type") == false) {
										if (key.equals("Folder") == false) {
											try {
												updateSample.setInt(counter,
														Integer.parseInt(next
																.getText()));
											} catch (NumberFormatException e) {
												try {
													updateSample
															.setDouble(
																	counter,
																	Double
																			.parseDouble(next
																					.getText()));
												} catch (NumberFormatException ex) {
													updateSample.setString(
															counter, next
																	.getText());
												}
											}
											counter++;
										}
									}
								}
							}
						}
					}

					updateSample.setInt(counter, sampleID);
					updateSample.execute();
				}
			}
		}
	

	}

	/**
	 * generates and fills the tables with metainformations
	 * 
	 * @param provider
	 * @param c
	 * @param sampleID
	 * @param setupxID
	 * @throws Exception
	 * @throws SQLException
	 */
	public void generateMetaInformations(SetupXProvider provider, int sampleID,
			String setupxID) throws Exception, SQLException {

		if (this.isRegenerateSampleMetaInfos()) {

			String meta = null;
			try {
				meta = provider.getMetaInformationBySample(setupxID);
				generateMetaInformations(meta, sampleID, setupxID);
			} catch (Exception e) {
				logger.warn("setupX exception ");
			}

		}
	}

	/**
	 * generate content based on the sample name
	 * 
	 * @param set
	 * @param setupxID
	 * @throws SQLException
	 */
	public void generateSampleProperties(ResultSet set, String setupxID)
			throws SQLException {
		if (this.isRegenerateSampleProperties()) {
			SampleDate date = SampleDate.createInstance(set.getString(1));

			logger.info(set.getString(1) + " - " + date.getMachine() + " - "
					+ date.getOperator() + " - " + date.getRunNumber());
			update.setString(1, date.getMachine());
			update.setString(2, date.getOperator());
			update.setInt(3, date.getRunNumber());
			update.setString(4, setupxID);
			update.setInt(5, set.getInt(2));
			update.execute();

		}
	}

	public static void main(String[] args) throws Exception {
		new UpdateBinBase().run();
	}

	/**
	 * do we want to regenerate the sample properties
	 * 
	 * @return
	 */
	public boolean isRegenerateSampleProperties() {
		return regenerateSampleProperties;
	}

	/**
	 * do we want to regenerate the sample properties
	 * 
	 * @return
	 */
	public void setRegenerateSampleProperties(boolean regenerateSampleProperties) {
		this.regenerateSampleProperties = regenerateSampleProperties;
	}

	/**
	 * do we want to regenerate the sample meta informations
	 * 
	 * @return
	 */
	public boolean isRegenerateSampleMetaInfos() {
		return regenerateSampleMetaInfos;
	}

	/**
	 * do we want to regenerate the sample meta informations
	 * 
	 * @return
	 */
	public void setRegenerateSampleMetaInfos(boolean regenerateSampleMetaInfos) {
		this.regenerateSampleMetaInfos = regenerateSampleMetaInfos;
	}

	/**
	 * we update only visible samples
	 * 
	 * @return
	 */
	public boolean isVisibleSamplesOnly() {
		return visibleSamplesOnly;
	}

	/**
	 * we update only visible samples
	 * 
	 * @return
	 */
	public void setVisibleSamplesOnly(boolean visibleSamplesOnly) {
		this.visibleSamplesOnly = visibleSamplesOnly;
	}

	/**
	 * finds the setupx id over the sample id
	 * @param provider
	 * @param sampleId
	 */
	public void generateMetaInformations(SetupXProvider provider,
			Integer sampleId) throws Exception, SQLException {
	
		this.selectSampleNameBySampleId.setInt(1, sampleId);
		ResultSet next = this.selectSampleNameBySampleId.executeQuery();
		
		if(next.next()){
			generateMetaInformations(provider, sampleId,provider.getSetupXId(next.getString(1)));
		}
		next.close();
	}
}
