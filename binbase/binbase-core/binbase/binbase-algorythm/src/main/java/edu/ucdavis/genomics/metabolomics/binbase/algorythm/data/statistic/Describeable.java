package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic;

/**
 * provides a simple description
 * @author wohlgemuth
 *
 */
public interface Describeable {

	/**
	 * returns the description of this element
	 * @return
	 */
	public String getDescription();
}
