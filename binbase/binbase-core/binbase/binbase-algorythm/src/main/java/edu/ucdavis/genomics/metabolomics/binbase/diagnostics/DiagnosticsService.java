package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

/**
 * the binbase diagnostics service, used to detect what happens to a spectra
 * during the runtime of the software. Different implementations provide you
 * with different possibilities
 * 
 * @author wohlgemuth
 * 
 */
public interface DiagnosticsService {

	/**
	 * a complete diagnostic action
	 * 
	 * @param className
	 *            The class
	 * @param sampleId
	 *            The sampleId
	 * @param spectraId
	 *            The spectraId
	 * @param binId
	 *            The binId
	 * @param caller
	 *            Who called this method
	 * @param stepOfAlgorithm
	 *            What is the step of the Algorithm
	 * @param reasonForTheArchivedResult
	 *            what is the reason for the ArchivedResult
	 * @param resultOfAction
	 *            What was the result
	 */
	public void diagnosticAction(String className, int sampleId, int spectraId,
			int binId, Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult, Result resultOfAction,
			Object[] parameters);

	/**
	 * a diagnostic action where we have no class name
	 * 
	 * @param sampleId
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	public void diagnosticAction(int sampleId, int spectraId, int binId,
			Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult, Result resultOfAction,
			Object[] parameters);

	/**
	 * diagnostic action for a single spectra
	 * 
	 * @param spectraId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 * @param parameters
	 */
	public void diagnosticAction(int spectraId, Class<?> caller,
			Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
			Result resultOfAction, Object[] parameters);

	/**
	 * diagnostic action in simplified version
	 * 
	 * @param spectraId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 * @param parameters
	 */
	public void diagnosticAction(int spectraId, Class<?> caller,
			String stepOfAlgorithm, String reasonForTheArchivedResult,
			String resultOfAction, Object[] parameters);

	public void diagnosticActionSuccess(int spectraId, Class<?> caller,
			String stepOfAlgorithm, String reasonForTheArchivedResult,
			Object[] parameters);

	public void diagnosticActionFailed(int spectraId, Class<?> caller,
			String stepOfAlgorithm, String reasonForTheArchivedResult,
			Object[] parameters);

	/**
	 * diagnostic action for a single spectra
	 * 
	 * @param spectraId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 * @param parameters
	 */
	public void diagnosticActionSuccess(int spectraId, Class<?> caller,
			Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
			Object[] parameters);

	/**
	 * diagnostic action for a single spectra
	 * 
	 * @param spectraId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 * @param parameters
	 */
	public void diagnosticActionFailed(int spectraId, Class<?> caller,
			Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
			Object[] parameters);

	/**
	 * a diagnostic action where we have no class of sample information
	 * 
	 * @param spectraId
	 * @param binId
	 * @param caller
	 * @param stepOfAlgorithm
	 * @param reasonForTheArchivedResult
	 * @param resultOfAction
	 */
	public void diagnosticAction(int spectraId, int binId, Class<?> caller,
			Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
			Result resultOfAction, Object[] parameters);

	public abstract void diagnosticActionFailed(int spectraId, int binId,
			Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Object[] parameters);

	public abstract void diagnosticActionFailed(int sampleId, int spectraId,
			int binId, Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Result resultOfAction,
			Object[] parameters);

	public abstract void diagnosticActionFailed(String className, int sampleId,
			int spectraId, int binId, Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Object[] parameters);

	public abstract void diagnosticActionSuccess(int spectraId, int binId,
			Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Object[] parameters);

	public abstract void diagnosticActionSuccess(int sampleId, int spectraId,
			int binId, Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Result resultOfAction,
			Object[] parameters);

	public abstract void diagnosticActionSuccess(String className,
			int sampleId, int spectraId, int binId, Class<?> caller,
			String stepOfAlgorithm, String reasonForTheArchivedResult,
			Object[] parameters);

	public abstract void diagnosticActionFailed(int spectraId, int binId,
			Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult, Object[] parameters);

	public abstract void diagnosticActionFailed(int sampleId, int spectraId,
			int binId, Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult, Object[] parameters);

	public abstract void diagnosticActionFailed(String className, int sampleId,
			int spectraId, int binId, Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult, Object[] parameters);

	public abstract void diagnosticActionSuccess(int spectraId, int binId,
			Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult, Object[] parameters);

	public abstract void diagnosticActionSuccess(int sampleId, int spectraId,
			int binId, Class<?> caller, Step stepOfAlgorithm,
			Reason reasonForTheArchivedResult, Object[] parameters);

	public abstract void diagnosticActionSuccess(String className,
			int sampleId, int spectraId, int binId, Class<?> caller,
			Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
			Object[] parameters);

	public abstract void diagnosticAction(int spectraId, int binId,
			Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Result resultOfAction,
			Object[] parameters);

	public abstract void diagnosticAction(int sampleId, int spectraId,
			int binId, Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Result resultOfAction,
			Object[] parameters);

	public abstract void diagnosticAction(String className, int sampleId,
			int spectraId, int binId, Class<?> caller, String stepOfAlgorithm,
			String reasonForTheArchivedResult, Result resultOfAction,
			Object[] parameters);

}
