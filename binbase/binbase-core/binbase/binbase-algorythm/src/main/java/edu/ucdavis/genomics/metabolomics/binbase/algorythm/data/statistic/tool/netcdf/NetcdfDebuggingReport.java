package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.netcdf;

import java.io.IOException;

import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.SampleObject;

/**
 * generates the netcdf debugging report for a sample
 * 
 * @author wohlgemuth
 * 
 */
public class NetcdfDebuggingReport extends NetcdfDebugging {

	@Override
	protected void workOnSample(SimpleDatafile file, Element configuration,
			SampleObject<String> sample) throws BinBaseException {
		try {
			writeObject(file, configuration, sample.getValue() + "-debug");
		} catch (IOException e) {
			throw new BinBaseException(e);
		}
	}
	
	public String getDescription(){
		return "a detailed report of the netcdf replacement";
	}

}
