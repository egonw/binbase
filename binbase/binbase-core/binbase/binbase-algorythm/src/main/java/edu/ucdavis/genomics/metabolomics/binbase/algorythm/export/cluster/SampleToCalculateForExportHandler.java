package edu.ucdavis.genomics.metabolomics.binbase.algorythm.export.cluster;

import java.io.File;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.export.SampleExport;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.export.dest.QuantificationTableDestinationFactoryImpl;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.methods.GitterMethode;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.config.source.DatabaseConfigSource;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.AbstractClusterHandler;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.io.dest.DestinationFactory;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;


/**
 * handler for this class to optimize the postmatching
 * 
 * @author wohlgemuth
 * 
 */
public final class SampleToCalculateForExportHandler extends AbstractClusterHandler
		 {

	private Logger logger = Logger.getLogger(getClass());
	private String group = this.getClass().getSimpleName();

	@Override
	protected boolean startProcessing() throws Exception {
		// standard binbase configuration
		XMLConfigurator.getInstance().destroy();
		XMLConfigurator.getInstance().addConfiguration(
				new FileSource(new File("config/applicationServer.xml")));
		XMLConfigurator.getInstance().addConfiguration(
				new FileSource(new File("config/targets.xml")));
		XMLConfigurator.getInstance().addConfiguration(
				new FileSource(new File("config/hibernate.xml")));
		XMLConfigurator.getInstance().addConfiguration(
				new DatabaseConfigSource(XMLConfigurator.getInstance()
						.getProperties()));

		this.setProperties(XMLConfigurator.getInstance().getProperties());

		this.getProperties().setProperty(
				SimpleConnectionFactory.KEY_USERNAME_PROPERTIE,
				((SampleToCalculateForExport) this.getObject()).getDatabase());

		ConnectionFactory fact = ConnectionFactory.getFactory();
		fact.setProperties(this.getProperties());
		Connection connection = fact.getConnection();
		connection.setAutoCommit(true);

		try {
			int id = ((SampleToCalculateForExport) this.getObject()).getId();
			// ok postmatch the result now... we need to update the
			// table
			logger.info("postmatching: " + id);
			GitterMethode method = new GitterMethode();
			method.setConnection(connection);
			method.setNewBinAllowed(false);
			method.setSampleId(id);
			method.run();

			logger.info("update table: " + id);

			logger.info("generate xml");
			SampleExport export = new SampleExport();
			export.setConnection(connection);

			String xmlContent = export.getQuantifiedSampleAsXml(id);
			Map<String, Connection> map = new HashMap<String, Connection>();
			map.put("CONNECTION", connection);

			byte[] result = xmlContent.getBytes();
			OutputStream out = DestinationFactory.newInstance(
					QuantificationTableDestinationFactoryImpl.class.getName())
					.createDestination(id, map).getOutputStream();
			out.write(result);
			out.flush();
			out.close();
			method = null;
			System.gc();

		} finally {
			fact.close(connection);
		}
		return true;
	}

	public String getStatisticalGroup() {
		return group;
	}

	public void setStatisticalGroup(String group) {
		this.group = group;

	}
}