package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

import java.io.Serializable;

/**
 * a step in the binbase system
 * @author wohlgemuth
 *
 */
public class Step implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String description;


	public Step(String description) {
		super();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	protected void setDescription(String description) {
		this.description = description;
	}
	

	public String toString(){
		return description;
	}
}
