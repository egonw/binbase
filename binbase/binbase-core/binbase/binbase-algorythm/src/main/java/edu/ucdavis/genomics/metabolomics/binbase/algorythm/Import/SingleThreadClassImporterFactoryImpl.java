/*
 * Created on Nov 15, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import;

import java.sql.Connection;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;

/**
 * provides us with single threaded implementations of the factory
 * @author wohlgemuth
 * @version Nov 15, 2005
 *
 */
public class SingleThreadClassImporterFactoryImpl extends ClassImporterFactory{

	public SingleThreadClassImporterFactoryImpl() {
		super();
	}

	/**
	 * creates a new importer with no threading support
	 * @author wohlgemuth
	 * @version Nov 15, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.ClassImporterFactory#createImporter()
	 */
	public ClassImporter createImporter(Connection connection) throws ConfigurationException {
		ClassImporter i = new SingleThreadClassImporter();
		i.setConnection(connection);
		i.setReport(this.getReport());
		return i;
	}

}
