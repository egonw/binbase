/*
 * Created on Oct 2, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import edu.ucdavis.genomics.metabolomics.util.io.Copy;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;

import net.sf.mzmine.datastructures.RawDataAtNode;
import net.sf.mzmine.datastructures.Scan;

/**
 * defines a netcdf file for operations on it
 * 
 * @author wohlgemuth
 * 
 */
public class NetCDFFile extends RawDataAtNode {

	/**
	 * transforms a source into a temporary files
	 * 
	 * @param source
	 * @return
	 * @throws IOException
	 */
	static File getFile(Source source) throws IOException {
		if (source instanceof FileSource) {
			return ((FileSource) source).getFile();
		} else {
			File file = File.createTempFile(source.getSourceName(), ".binbase");
			file.deleteOnExit();
			Copy.copy(source.getStream(), new FileOutputStream(file));
			return file;
		}
	}

	public NetCDFFile(Source source) throws IOException {
		this(getFile(source));
	}

	public NetCDFFile(File file) {
		super(0, file);
		this.setWorkingCopy(file);
	}

	/**
	 * finds the intensity for this ion on the given time is a very slow linear search
	 * 
	 * @param retentionTime
	 * @param uniqueMass
	 * @return
	 */
	public double findIntensity(double retentionTime, int uniqueMass) {
		return 0;
	}

	/**
	 * returns all ions at the given time
	 * 
	 * @param retentionTime
	 * @return
	 */
	public Scan getScan(double retentionTime, double precision) {
		initializeScanBrowser(0, getNumberOfScans());

		for (int i = 0; i < getNumberOfScans(); i++) {
			Scan s = getNextScan();
			double time = getScanTime(s.getScanNumber());
			if ((retentionTime >= (time - precision)) && (retentionTime <= (time + precision))) {
				return s;
			}
		}

		return null;
	}
}
