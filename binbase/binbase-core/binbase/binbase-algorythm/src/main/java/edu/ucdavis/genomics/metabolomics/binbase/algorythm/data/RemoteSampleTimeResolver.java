package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceFactory;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * uses the ejb server to calculate the access tiem for a sample
 * 
 * @author wohlgemuth
 */
public class RemoteSampleTimeResolver implements SampleTimeResolver {

	private Map<String,Long> cache = new HashMap<String, Long>();
	
	private Logger logger = Logger.getLogger(getClass());
	
	@Override
	public long resolveTime(String sample) throws BinBaseException {
		if(cache.get(sample) != null){
			return cache.get(sample);
		}
		try {
			final BinBaseService service = BinBaseServiceFactory.createFactory().createService();
			final String key = Configurator.getKeyManager().getInternalKey();

			Long time = service.getTimeStampForSample(sample, key);
			cache.put(sample, time);
			
			return time;
		}
		catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw new BinBaseException(e.getMessage(), e);
		}
	}

}
