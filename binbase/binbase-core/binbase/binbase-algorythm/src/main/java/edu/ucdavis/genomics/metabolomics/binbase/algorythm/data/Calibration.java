package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jdom.DataConversionException;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.util.statistics.deskriptiv.Meanable;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;

/**
 * provides the system with calibrations
 * 
 * @author wohlgemuth
 * 
 */
public class Calibration {

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * this calculates the calibration matrix and provides us with a result
	 * object containing everything we need
	 * 
	 * @return
	 */
	public CalibrationFile calculateCalibrationMatrix(ResultDataFile file,
			List<Element> standards, Meanable combine, List<Element> regressions)
			throws NumberFormatException {

		// contains the actual calibration matrix
		Map<String, Map<Double, Double>> calibrationValues = new HashMap<String, Map<Double, Double>>();

		// go over all standards
		for (Element e : standards) {

			String bin = e.getAttributeValue("name");

			// add our bin to the result
			calibrationValues.put(bin, new HashMap<Double, Double>());

			List<Element> concentrations = e.getChildren("sample");

			// contains a mapping from concentration to sample name
			Map<Double, Set<String>> concentrationMapping = new HashMap<Double, Set<String>>();

			// load the map for all required mappings
			for (Element s : concentrations) {
				String sampleName = s.getAttributeValue("name");
				Double concentration = null;
				try {
					concentration = s.getAttribute("concentration")
							.getDoubleValue();
				} catch (DataConversionException e1) {
					throw new NumberFormatException(e1.getMessage());
				}

				if (concentrationMapping.containsKey(concentration) == false) {
					concentrationMapping.put(concentration,
							new HashSet<String>());
				}

				concentrationMapping.get(concentration).add(sampleName);
			}

			// generate the calibration file
			for (Double conc : concentrationMapping.keySet()) {

				Set<String> samples = concentrationMapping.get(conc);

				Collection<Double> intensities = new Vector<Double>(
						samples.size());

				for (String sample : samples) {
					try {
						FormatObject<?> value = file.getSpectra(sample, bin);
						Double intensity = (Double) value.getValue();

						file.getSample(sample).addAttribute("concentration", conc.toString());
						intensities.add(intensity);
					} catch (BinNotFoundException ex) {
						logger.warn("bin not found...");
						intensities.add(0.0);
					}
				}

				// this is the combined height for all the samples of this
				// intensity
				Double combinedHeight = combine.calculate(intensities);

				calibrationValues.get(bin).put(conc, combinedHeight);
			}
		}

		Collection<List<Double>> regressionCurves = new HashSet<List<Double>>();
		for (Element e : regressions) {

			List<Double> conc = new Vector<Double>();

			for (Element v : (List<Element>) e.getChildren("concentration")) {
				Double concentration;
				try {
					concentration = v.getAttribute("value").getDoubleValue();
					conc.add(concentration);

				} catch (DataConversionException e1) {
					logger.warn(e1.getMessage(), e1);
				}

			}
			
			regressionCurves.add(conc);
		}
		
		// put the result together
		CalibrationFile result = new CalibrationFile(calibrationValues, file,regressionCurves);

		return result;
	}

}
