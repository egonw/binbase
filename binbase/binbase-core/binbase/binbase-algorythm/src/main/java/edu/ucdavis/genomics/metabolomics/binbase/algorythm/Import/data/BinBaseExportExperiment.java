package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;

/**
 * needed for tracking of experiments
 * 
 * @author wohlgemuth
 */
public class BinBaseExportExperiment extends Experiment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private boolean definitionStored;

	private boolean emailSend;

	private boolean statisticsGenerated;

	private boolean sendToSetupX;

	int current;

	int max = 4;

	public BinBaseExportExperiment(Experiment experiment) {
		this.setClasses(experiment.getClasses());
		this.setColumn(experiment.getColumn());
		this.setEmail(experiment.getEmail());
		this.setPriority(experiment.getPriority());
		this.setSopUrl(experiment.getSopUrl());
		this.setId(experiment.getId());
	}

	public double calculateProgress() {
		return (double) current / (double) max * 100;
	}

	private void progress(boolean matched) {
		if (matched) {
			current++;
		}
		else {
			current--;
		}
	}

	public boolean isDefinitionStored() {
		return definitionStored;
	}

	public void setDefinitionStored(boolean definitionStored) {
		if (this.definitionStored == false) {
			progress(definitionStored);
		}
		this.definitionStored = definitionStored;
	}

	public boolean isEmailSend() {
		return emailSend;
	}

	public void setEmailSend(boolean emailSend) {
		if (this.emailSend == false) {
			progress(emailSend);
		}
		this.emailSend = emailSend;
	}

	public boolean isStatisticsGenerated() {
		return statisticsGenerated;
	}

	public void setStatisticsGenerated(boolean statisticsGenerated) {
		if (this.isStatisticsGenerated() == false) {
			progress(statisticsGenerated);
		}
		this.statisticsGenerated = statisticsGenerated;
	}

	public boolean isSendToSetupX() {
		return sendToSetupX;
	}

	public void setSendToSetupX(boolean sendToSetupX) {
		if(this.sendToSetupX == false){
		progress(sendToSetupX);
		}
		this.sendToSetupX = sendToSetupX;
	}

}
