package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import java.io.Serializable;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseReports;
import edu.ucdavis.genomics.metabolomics.binbase.BinBaseServices;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.SampleMatcher;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.SampleMatcherFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.PostmatchingJob;
import edu.ucdavis.genomics.metabolomics.util.config.Configable;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;

/**
 * post matches samples
 * 
 * @author wohlgemuth
 */
public class PostMatchSampleHandler extends BinBaseClusterHandler {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	protected boolean process(Serializable job) throws Exception {
		PostmatchingJob sample = (PostmatchingJob) job;

		getReport().report(sample, BinBaseReports.UPDATE, BinBaseReports.SAMPLE_POSTMATCH_JOB);

		ConnectionFactory factory = ConnectionFactory.getFactory();

		if (sample.getDatabase() == null) {
			sample.setDatabase(Configable.CONFIG.getValue("server.defaultDatabase"));
			logger.info("set default database: " + sample.getDatabase());
		}
		else {
			logger.info("set to database: " + sample.getDatabase());
		}

		//accessing the service
		BinBaseService service = BinBaseServices.getService();
		
		//getting import key
		String key = Configurator.getKeyManager().getInternalKey();
			
		SampleMatcher matcher = SampleMatcherFactory.newInstance().createMatcher(sample.getDatabase());
		
		for (ExperimentSample current : sample.getSampleIds()) {
			//fetch the latest sample id because that's the one we need to update. We don't care about older samples anymore
			logger.info("fetching id for " + current);
			
			int sampleId = service.getSampleIdForLatestVersion(current, sample.getDatabase(), key);

			matcher.addSampleToCalculate(sampleId);
		}
		
		//updates all the samples in the system
		matcher.matchSamples();
		
		logger.info("calling gc");

		System.gc();

		sample.setXmlGenerated(true);

		logger.info("done");

		return true;
	}

}
