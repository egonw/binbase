package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import java.io.Serializable;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.util.sql.UpdateBinBase;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.UpdateMetaDataJob;

/**
 * updates the complete database and regenerates the metadata for setupx
 * @author wohlgemuth
 *
 */
public class UpdateMetaDataHandler extends BinBaseClusterHandler {

	@Override
	protected boolean process(Serializable job) throws Exception {

		if (job instanceof UpdateMetaDataJob) {
			UpdateBinBase binbase = new UpdateBinBase();
			binbase.setRegenerateSampleMetaInfos(true);
			binbase.setRegenerateSampleProperties(false);
			binbase.run();
		}

		return true;
	}

}
