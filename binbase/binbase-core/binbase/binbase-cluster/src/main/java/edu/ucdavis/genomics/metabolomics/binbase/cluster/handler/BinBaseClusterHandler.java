package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import java.io.File;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.mail.MailService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.mail.MailServiceFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacadeUtil;
import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateSetupXFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.io.ConfigSource;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.config.source.DatabaseConfigSource;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

/**
 * a binbase specific clusterhandler with some fancy configuration mojo
 * 
 * @author wohlgemuth
 */
public abstract class BinBaseClusterHandler extends AbstractClusterHandler {
	private Logger logger = Logger.getLogger(getClass());

	@Override
	protected final boolean startProcessing() throws Exception {
		configure();

		return process(this.getObject());
	}

	/**
	 * does the actuall processing
	 * 
	 * @param job
	 * @return
	 * @throws Exception
	 */
	protected abstract boolean process(Serializable job) throws Exception;

	/**
	 * does the configuration
	 * 
	 * @throws Exception
	 */
	protected void configure() throws Exception {
		try {
			logger.info("try to initialize from file config/applicationServer.xml");
			XMLConfigurator.getInstance().addConfiguration(new FileSource(new File("config/applicationServer.xml")));
			logger.info("successfull!");

		}
		catch (Exception e) {
			logger.warn(e.getMessage());
			try {
				logger.info("try to initialize from resource config/applicationServer.xml");
				XMLConfigurator.getInstance().addConfiguration(new ResourceSource("/config/applicationServer.xml"));
				logger.info("successfull!");

			}
			catch (Exception ex) {
				logger.info("trying the home directory of the user! - " + e.getMessage());
				try {
					logger.info("try to initialize from file ~/.config/applicationServer.xml");
					XMLConfigurator.getInstance()
							.addConfiguration(new FileSource(new File(System.getProperty("user.home") + "./config/applicationServer.xml")));
					logger.info("successfull!");

				}
				catch (Exception exi) {
					logger.fatal("giving up! - " + e.getMessage());
					throw e;
				}
			}
		}
		try {
			logger.info("try to initialize targets from central config - targets.xml");
			XMLConfigurator.getInstance().addConfiguration(new ConfigSource("targets.xml"));
			logger.info("successfull!");
		}
		catch (Exception e) {
			logger.warn(e.getMessage());

			try {
				logger.info("try to initialize targets from file config/targets.xml");
				XMLConfigurator.getInstance().addConfiguration(new FileSource(new File("config/targets.xml")));
				logger.info("successfull!");

			}
			catch (Exception exe) {
				logger.warn(exe.getMessage());
				try {
					logger.info("try to initialize from resource /config/targets.xml");
					XMLConfigurator.getInstance().addConfiguration(new ResourceSource("/config/targets.xml"));
					logger.info("successfull!");

				}
				catch (Exception ex) {
					try {
						logger.info("try to initialize from file ~/.config/targets.xml");
						XMLConfigurator.getInstance().addConfiguration(new FileSource(new File(System.getProperty("user.home") + "./config/targets.xml")));
						logger.info("successfull!");

					}
					catch (Exception exi) {

						logger.warn(e.getMessage());
					}
				}
			}
		}
		logger.info("configure database properites");
		XMLConfigurator.getInstance().addConfiguration(new DatabaseConfigSource(XMLConfigurator.getInstance().getProperties()));

		// setting setupx delegate factory

		System.setProperty(SetupXFactory.DEFAULT_PROPERTY_NAME, DelegateSetupXFactory.class.getName());
		logger.info("merge system properites with set properties!");
		this.setProperties(XMLConfigurator.getInstance().getProperties());
		this.getProperties().putAll(System.getProperties());
		logger.info("configuration is done");

		if (this.getProperties().get(SetupXFactory.DEFAULT_PROPERTY_NAME) == null) {
			System.setProperty(SetupXFactory.DEFAULT_PROPERTY_NAME, DelegateSetupXFactory.class.getName());
			this.getProperties().put(SetupXFactory.DEFAULT_PROPERTY_NAME, DelegateSetupXFactory.class.getName());
		}
	}

	public String getStatisticalGroup() {
		return group;
	}

	public void setStatisticalGroup(String group) {
		this.group = group;
	}


	private String group = this.getClass().getSimpleName();


    /**
     * sends the email that the result is done
     *
     * @param ex
     * @param content
     * @return
     */
    @SuppressWarnings("unchecked")
    protected void sendEmail(String id, String optionalReceiver, String content) {

        logger.info("sending email");
        try {

            if (Configurator.getNotifierService().isEnableEmail()) {
                ExportJMXFacadeUtil.getHome(XMLConfigurator.getInstance().getProperties()).create();
                MailService service = MailServiceFactory.newInstance().create();

                if (optionalReceiver != null) {
                    service.sendMessage(optionalReceiver, "result with id: " + id + " is done!", "you experiment is done!");
                } else {
                    logger.info("no email defined for this experiment!");
                }

                try {
                    logger.info("send notification emails about a finished experiment");

                    StatusJMXFacade notification = StatusJMXFacadeUtil.getHome(XMLConfigurator.getInstance().getProperties()).create();
                    Collection<String> collection = notification.getNotificationsAdresses();
                    Iterator<String> it = collection.iterator();

                    while (it.hasNext()) {
                        String email = it.next().toString();
                        logger.info("sending to email: " + email);
                        service.sendMessage(email, "result is done notification - " + id, "your experiment is done");
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn("can't send email: " + e.getMessage());
        }
    }


}
