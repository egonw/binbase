/*
 * Created on Jul 14, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseReports;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.ClassImporterFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.util.config.Configable;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;

/**
 * stores calculates experiments in the database
 * 
 * @author wohlgemuth
 * @version Jul 14, 2006
 * 
 */
public class ExperimentClassHandler extends BinBaseClusterHandler {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public boolean process(Serializable job) throws Exception {

		ExperimentClass clazz = (ExperimentClass) job;

		getReport().report(clazz.getId(), BinBaseReports.IMPORT, BinBaseReports.CLASS);

		ConnectionFactory factory = ConnectionFactory.getFactory();

		if (clazz.getColumn() == null) {
			clazz.setColumn(Configable.CONFIG.getValue("server.defaultDatabase"));
			logger.info("set default database: " + clazz.getColumn());
		} else {
			logger.info("set to database: " + clazz.getColumn());
		}

		this.getProperties().setProperty(SimpleConnectionFactory.KEY_USERNAME_PROPERTIE, clazz.getColumn());
		factory.setProperties(this.getProperties());

		Connection connection = factory.getConnection();

		// create the service

		try {
			// import the data
			logger.debug("import class");

			connection.setAutoCommit(true);

			ClassImporterFactory.newInstance(this.getReport()).createImporter(connection).importData(clazz);
			// connection.commit();
			getReport().report(clazz.getId(), BinBaseReports.DONE, BinBaseReports.CLASS);


			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			connection.rollback();
			getReport().report(clazz.getId(), BinBaseReports.FAILED, BinBaseReports.CLASS);
			throw e;
		} finally {
			try {
				// unlock tables
				logger.info("close connection");
				factory.close(connection);
			} catch (SQLException e) {
				logger.error(e);
			}
		}
	}

}
