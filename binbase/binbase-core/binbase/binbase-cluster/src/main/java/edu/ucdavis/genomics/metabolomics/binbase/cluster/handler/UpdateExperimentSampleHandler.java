package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import java.io.File;
import java.sql.Connection;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseReports;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.util.sql.UpdateBinBase;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.config.source.DatabaseConfigSource;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.action.UpdateExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXProvider;
import edu.ucdavis.genomics.metabolomics.util.config.Configable;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;

/**
 * update meta information for an object
 * @author wohlgemuth
 *
 */
public class UpdateExperimentSampleHandler extends AbstractClusterHandler{
	private Logger logger = Logger.getLogger(getClass());


	private String group = this.getClass().getSimpleName();


	public String getStatisticalGroup() {
		return group;
	}

	public void setStatisticalGroup(String group) {
		this.group = group;
	}

	@Override
	public boolean startProcessing() throws Exception {
		// delete configuration and overwrite it with our configuration
		XMLConfigurator.getInstance().destroy();
		XMLConfigurator.getInstance().addConfiguration(
				new FileSource(new File("config/targets.xml")));
		XMLConfigurator.getInstance().addConfiguration(
				new DatabaseConfigSource(XMLConfigurator.getInstance()
						.getProperties()));

		this.setProperties(XMLConfigurator.getInstance().getProperties());

		UpdateExperimentSample sample = (UpdateExperimentSample) this.getObject();
		

		getReport().report(sample.getId(), BinBaseReports.UPDATE,
				BinBaseReports.SAMPLE);

		ConnectionFactory factory = ConnectionFactory.getFactory();
		

		if (sample.getDatabase() == null) {
			sample.setDatabase(Configable.CONFIG
					.getValue("server.defaultDatabase"));
			logger.info("set default database: " + sample.getDatabase());
		} else {
			logger.info("set to database: " + sample.getDatabase());
		}

		this.getProperties().setProperty(
				SimpleConnectionFactory.KEY_USERNAME_PROPERTIE,
				sample.getDatabase());
		factory.setProperties(this.getProperties());

		
		//update sample informations
		SetupXProvider provider = SetupXFactory.newInstance()
		.createProvider();
		UpdateBinBase binbase = new UpdateBinBase();
		Connection connection = factory.getConnection();
		binbase.setConnection(connection);
		
		if(sample.getXmlContent() == null){
			binbase.generateMetaInformations(provider, sample.getName(), sample.getId());
		}
		else{
			binbase.generateMetaInformations(sample.getXmlContent(), sample.getName(), sample.getId());	
		}

		factory.close(connection);
		return true;
	}
}
