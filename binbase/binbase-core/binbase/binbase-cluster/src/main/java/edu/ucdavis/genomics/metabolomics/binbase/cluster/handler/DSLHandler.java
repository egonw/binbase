package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseReports;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.calc.LocalExporter;
import edu.ucdavis.genomics.metabolomics.binbase.server.ejb.Configurator;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;
import edu.ucdavis.genomics.metabolomics.util.io.dest.ByteArrayDestination;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import org.jboss.logging.Logger;

import java.io.File;
import java.io.Serializable;

/**
 * handles the calculation of dsl's
 *
 * @author wohlgemuth
 */
public class DSLHandler extends BinBaseClusterHandler {

    private Logger logger = Logger.getLogger(getClass());

    @Override
    protected boolean process(Serializable job) throws Exception {

        if (job instanceof DSL) {
            DSL dsl = (DSL) job;
            logger.info(dsl.getContent());

            LocalExporter exporter = new LocalExporter(dsl.getContent());
            String result = exporter.run();

            if (result != null) {

            	logger.info("uploading result to application server: " + dsl.getId());


                ByteArrayDestination destination = new ByteArrayDestination();
                FileSource source = new FileSource(new File(result));

                Copy.copy(source.getStream(),destination.getOutputStream());

                Configurator.getExportService().uploadResult(dsl.getId()+".zip",destination.getBytes());
            	logger.info("trying to attach result to minix");
                try {

                    SetupXFactory.newInstance().createProvider(this.getProperties()).upload(dsl.getId(), result);

                    getReport().report(dsl, new ReportEvent("uploaded to lims", "dsl is uploaded to lims"), BinBaseReports.DSL);
                } catch (Exception e) {
                    logger.warn("can't upload to lims: " + e.getMessage());
                }


            } else {
                logger.warn("job did not produce an attachable result!");
            }

            try {
                sendEmail(dsl.getId(), null, result);
            } catch (Exception e) {

            }
            return true;
        } else {
            logger.info("wrong kind of job, job is instanceof: " + job.getClass());
        }

        return false;
    }

}
