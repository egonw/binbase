package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseReports;
import edu.ucdavis.genomics.metabolomics.binbase.BinBaseServices;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Priority;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.PostmatchingJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.UpdateBinBaseJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.SchedulingException;
import edu.ucdavis.genomics.metabolomics.binbase.server.ejb.compounds.Configurator;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.config.Configable;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.Lockable;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.LockableFactory;

/**
 * updates the complete binbase, which basically means it recalculates all
 * samples, if they are outdated to ave time we always calculate a couple of
 * samples at a time since the overhead kills the calculations otherwise
 * 
 * @author wohlgemuth
 */
public class UpdateBinBaseHandler extends BinBaseClusterHandler {

	private Logger logger = Logger.getLogger(getClass());

//	private static String LOCK = UpdateBinBaseHandler.class.getName() + "_LOCK";

	@SuppressWarnings("unchecked")
	@Override
	protected boolean process(Serializable jobs) throws Exception {

		Lockable lock = LockableFactory.newInstance().create(getClass().getName());

		UpdateBinBaseJob job = (UpdateBinBaseJob) jobs;

		getReport().report(job, BinBaseReports.UPDATE, BinBaseReports.JOB);

		if (job.getColumn() == null) {
			job.setColumn(Configable.CONFIG.getValue("server.defaultDatabase"));
			logger.info("set default database: " + job.getColumn());
		}
		else {
			logger.info("set to database: " + job.getColumn());
		}
		// accessing the service
		BinBaseService service = BinBaseServices.getService();

		// getting import key
		String key = Configurator.getKeyManager().getInternalKey();

		// get all ids of interrest
		String[] experiments = service.getAllExperimentIds(job.getColumn(), key);

		Vector<ExperimentSample> samplesToUpdate = new Vector<ExperimentSample>();
		Vector<Experiment> experimentsToUpdate = new Vector<Experiment>();

		// fill the vectors with the needed data
		for (String ex : experiments) {

//			lock.aquireRessource(LOCK, TIMEOUT);
			// fetch the experiment descriptions
			Experiment exp = service.getExperiment(job.getColumn(), ex, key);
//			lock.releaseRessource(LOCK);

			// check that we don't schedule an experiment twice
			Collection<Experiment> data = Configurator.getStatusService().listExportJobs();

			if (data.contains(exp) == false) {

				// check that we don't kill the server with to many updates,
				// since it can have a lot of update jobs already
				if (Configurator.getStatusService().getCountOfExportJobs() >= (Configurator.getConfigService().getMaxNodes())) {
					logger.debug("the queue for experiment calculations is full, do not continue!");
					break;
				}
				// make sure that we don't have to many local jobs running
				else if (experimentsToUpdate.size() >= (Configurator.getConfigService().getMaxNodes())) {
					logger.debug("the local queue for experiment calculations is full, do not continue!");
					break;
				}
				// ok we can now do some scheduling work
				else {
//					lock.aquireRessource(LOCK, TIMEOUT);

					if (service.isExperimentInNeedOfRecalculation(exp, job.getColumn(), key)) {
						logger.debug("experiment is in need for an update: " + exp.getId());

						if (service.isExperimentAbleToBeRecalculated(exp, job.getColumn(), key)) {
							for (ExperimentClass c : exp.getClasses()) {
								for (ExperimentSample s : c.getSamples()) {

									// several experiments can share the
									// same
									// sample,
									// but we only want to calculate it
									// once!
									if (samplesToUpdate.contains(s) == false) {
										samplesToUpdate.add(s);
									}
								}
							}
							experimentsToUpdate.add(exp);

						}
						else {
							logger.info("sorry for some reasons this samples can't be recalculated");
						}
					}
					else {
						logger.info("experiment is uptodate: " + exp.getId());
					}
					
//					lock.releaseRessource(LOCK);
				}
			}
			else {
				logger.info("experiment was already scheduled: " + exp);
			}
		}

		// trigger the sample part
		triggerSamples(job, samplesToUpdate);

		// trigger the experiments
		triggerExperiment(job, experimentsToUpdate);

		// report that we are done
		getReport().report(job, BinBaseReports.DONE, BinBaseReports.JOB);

		// return success
		return true;
	}

	/**
	 * triggers the experiments
	 * 
	 * @param job
	 * @param samplesToUpdate
	 * @throws SchedulingException
	 */
	private void triggerExperiment(UpdateBinBaseJob job, Vector<Experiment> experimentsToUpdate) throws SchedulingException {
		for (Experiment exp : experimentsToUpdate) {
			Scheduler.schedule(exp, Scheduler.PRIORITY_WHENEVER);
		}
	}

	/**
	 * buidls the jobs for the list of samples ids to update them
	 * 
	 * @param job
	 * @param samplesToUpdate
	 * @throws BinBaseException
	 * @throws RemoteException
	 * @throws CreateException
	 * @throws NamingException
	 * @throws SchedulingException
	 */
	private void triggerSamples(UpdateBinBaseJob job, Vector<ExperimentSample> samplesToUpdate) throws BinBaseException, RemoteException, CreateException,
			NamingException, SchedulingException {
		int counter = 0;

		// should be set over a jmx, the idea is to avoid to many small jobs
		int max = 50;

		// first we update the related samples
		PostmatchingJob pm = new PostmatchingJob();
		pm.setDatabase(job.getColumn());

		for (ExperimentSample id : samplesToUpdate) {
			pm.addSampleID(id);

			counter++;
			if (counter >= max) {
				pm = fireUpdateSample(job, pm);

				// restting counter
				counter = 0;
			}

		}

		pm = fireUpdateSample(job, pm);
	}

	/**
	 * fires the actual database upgrade job
	 * 
	 * @param job
	 * @param pm
	 * @return
	 * @throws BinBaseException
	 * @throws RemoteException
	 * @throws CreateException
	 * @throws NamingException
	 * @throws SchedulingException
	 */
	private PostmatchingJob fireUpdateSample(UpdateBinBaseJob job, PostmatchingJob pm) throws BinBaseException, RemoteException, CreateException,
			NamingException, SchedulingException {
		if (Configurator.getStatusService().listDatabaseUpdateJobs().contains(pm) == false) {
			logger.info("created job: " + pm);
			Scheduler.schedule(pm, Priority.PRIORITY_VERY_LOW, PostMatchSampleHandler.class.getName());

			pm = new PostmatchingJob();
			pm.setDatabase(job.getColumn());

		}
		else {
			logger.debug("job is already in the queue! - " + pm);
		}
		return pm;
	}
}
