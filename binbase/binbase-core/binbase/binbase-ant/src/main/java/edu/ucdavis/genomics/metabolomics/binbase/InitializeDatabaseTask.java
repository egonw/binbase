package edu.ucdavis.genomics.metabolomics.binbase;

import java.io.File;
import java.io.FileInputStream;

import org.apache.tools.ant.BuildException;
import org.dbunit.dataset.xml.XmlDataSet;

import edu.ucdavis.genomics.metabolomics.util.database.test.InitializeDBUtil;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;

/**
 * initializes the database from a dataset
 * @author wohlgemuth
 *
 */
public class InitializeDatabaseTask extends AbstractDatabaseTask {

	String datafile;

	@Override
	protected void validateProperties() throws BuildException {
		super.validateProperties();
		check(datafile, "please specify the datafile!");
	}

	public String getDatafile() {
		return datafile;
	}

	public void setDatafile(String datafile) {
		this.datafile = datafile;
	}

	@Override
	protected void executeDatabaseTask() throws Exception {

		logger.info("datafile was set: " + datafile);
		File file = new File(datafile);
		File createFile = new File("binbase-schema.sql");
		
		logger.info("using file: " + file.getAbsolutePath() + " to initialize the database");
		logger.info("using file: " + createFile.getAbsolutePath() + " to create the database");
		
		//initializes the db
		InitializeDBUtil.initialize(this.getUsername(),this.getPassword(),this.getServer(),this.getDatabase(),new FileSource(createFile), new XmlDataSet(new FileInputStream(file)));
		
	}

}
