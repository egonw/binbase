package edu.ucdavis.genomics.metabolomics.binbase;

import org.apache.tools.ant.BuildException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;

public class RegisterExportDirectoryTask extends AbstractApplicationServerTask {

	String netcdfDirectory;

	@Override
	protected void execueTask() throws Exception {
		logger.info("adding directory: " + netcdfDirectory
				+ " as netcd directory");
		Configurator.getExportService().addNetCDFDirectory(netcdfDirectory);

		logger.info("done...");
	}

	@Override
	protected void validateProperties() throws BuildException {
		check(netcdfDirectory,
				"please set a directory where I can find netcdf files");
	}

	public String getNetcdfDirectory() {
		return netcdfDirectory;
	}

	public void setNetcdfDirectory(String netcdfDirectory) {
		this.netcdfDirectory = netcdfDirectory;
	}
}
