package edu.ucdavis.genomics.metabolomics.binbase;

import org.apache.tools.ant.BuildException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.job.SchedulerJob;
import edu.ucdavis.genomics.metabolomics.binbase.bci.job.SchedulerJobHandler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.ExperimentClassHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.ExperimentHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.UpdateBinBaseHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.CacheUrlJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.UpdateBinBaseJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.LocalCalculationFactory;

public class ConfigureImportTask extends AbstractApplicationServerTask {

	String clusterAddress;
	String clusterPassword;
	String clusterUsername;
	String importDirectory;
	String clusterFactory;

	int maxNodes = 1;

	boolean validateSources = true;

	boolean autostart = true;

	@Override
	protected void execueTask() throws Exception {
		Configurator.getImportService().setValidateSources(validateSources);
		Configurator.getConfigService().setMaxNodes(maxNodes);
		Configurator.getConfigService().setAutoStart(autostart);

		if (checkBoolean(importDirectory)) {
			Configurator.getImportService().clearDirectorys();
			Configurator.getImportService().addDirectory(importDirectory);
		}

		Configurator.getConfigService().setCluster(clusterAddress);
		Configurator.getConfigService().setUsername(clusterUsername);
		Configurator.getConfigService().setPassword(clusterPassword);

		if(clusterFactory == null){
			//log("no factory was set, use local factory by default, so clustering is disabled!");
			Configurator.getConfigService().setClusterFactory(LocalCalculationFactory.class.getName());
		}
		else{
			//log("setting factory: " + clusterFactory);
			Configurator.getConfigService().setClusterFactory(clusterFactory);
		}
		//register experiment class for import needed
		Configurator.getConfigService().addHandler(ExperimentClass.class.getName(), ExperimentClassHandler.class.getName());
		
		//register experiment for export needed
		Configurator.getConfigService().addHandler(Experiment.class.getName(), ExperimentHandler.class.getName());
		
		//register handler which is needed for caching
		Configurator.getConfigService().addHandler(CacheUrlJob.class.getName(), CacheUrlJob.class.getName());

		//register handler which is needed for updates
		Configurator.getConfigService().addHandler(UpdateBinBaseJob.class.getName(), UpdateBinBaseHandler.class.getName());

		//reigister handleer for scheduling
		Configurator.getConfigService().addHandler(SchedulerJob.class.getName(), SchedulerJobHandler.class.getName());
	}

	@Override
	protected void validateProperties() throws BuildException {
		check(clusterAddress, "please provide a value for the cluster address");
		check(clusterPassword, "please provide a value for the cluster password");
		check(clusterUsername, "please provide a value for the cluster username");

	}

	public String getClusterAddress() {
		return clusterAddress;
	}

	public void setClusterAddress(String clusterAddress) {
		this.clusterAddress = clusterAddress;
	}

	public String getClusterPassword() {
		return clusterPassword;
	}

	public void setClusterPassword(String clusterPassword) {
		this.clusterPassword = clusterPassword;
	}

	public String getClusterUsername() {
		return clusterUsername;
	}

	public void setClusterUsername(String clusterUsername) {
		this.clusterUsername = clusterUsername;
	}

	public String getImportDirectory() {
		return importDirectory;
	}

	public void setImportDirectory(String importDirectory) {
		this.importDirectory = importDirectory;
	}

	public int getMaxNodes() {
		return maxNodes;
	}

	public void setMaxNodes(int maxNodes) {
		this.maxNodes = maxNodes;
	}

	public boolean isValidateSources() {
		return validateSources;
	}

	public void setValidateSources(boolean validateSources) {
		this.validateSources = validateSources;
	}

	public boolean isAutostart() {
		return autostart;
	}

	public void setAutostart(boolean autostart) {
		this.autostart = autostart;
	}

	public String getClusterFactory() {
		return clusterFactory;
	}

	public void setClusterFactory(String clusterFactory) {
		this.clusterFactory = clusterFactory;
	}

}
