package edu.ucdavis.genomics.metabolomics.binbase;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.tools.ant.BuildException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;

public class ConfigureExportTask  extends AbstractApplicationServerTask{

	String netcdfDirectory;
	
	String sopDirectory;

	private String resultDirectory;
	
	
	@Override
	protected void execueTask() throws Exception {
		Configurator.getExportService().clearSopDirs();
		Configurator.getExportService().clearNetCDFDirectorys();
		Configurator.getExportService().addNetCDFDirectory(netcdfDirectory);
		Configurator.getExportService().addSopDir(sopDirectory);
		
		Configurator.getExportService().uploadSop("default", readSop("default"));
		Configurator.getExportService().setDefaultSop("default");
		Configurator.getExportService().setResultDirectory(resultDirectory);
	}

	private byte[] readSop(String string) throws FileNotFoundException, IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Copy.copy(new FileInputStream("default.stat"), out);
		return out.toByteArray();
	}

	@Override
	protected void validateProperties() throws BuildException {
		check(netcdfDirectory, "please set a directory where I can find netcdf files");
		check(sopDirectory, "please set a directory where I can find sop files");
		check(resultDirectory, "please provide a directory where result should be stored");
	}

	public String getNetcdfDirectory() {
		return netcdfDirectory;
	}

	public void setNetcdfDirectory(String netcdfDirectory) {
		this.netcdfDirectory = netcdfDirectory;
	}

	public String getSopDirectory() {
		return sopDirectory;
	}

	public void setSopDirectory(String sopDirectory) {
		this.sopDirectory = sopDirectory;
	}

	public String getResultDirectory() {
		return resultDirectory;
	}

	public void setResultDirectory(String resultDirectory) {
		this.resultDirectory = resultDirectory;
	}

}
