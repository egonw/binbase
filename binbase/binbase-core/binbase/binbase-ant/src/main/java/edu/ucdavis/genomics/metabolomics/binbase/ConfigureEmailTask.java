package edu.ucdavis.genomics.metabolomics.binbase;

import org.apache.tools.ant.BuildException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.CommunicationJMXFacade;

/**
 * configure the communication email
 * 
 * @author nase
 * 
 */
public class ConfigureEmailTask extends AbstractApplicationServerTask {

	String emailPassword;

	String emailSmptServer;

	String emailSmtpPort;

	String emailFromAddress;

	String emailUserName;

	@Override
	protected void execueTask() throws Exception {
		CommunicationJMXFacade f = Configurator.getCommunicationService();
		f.setFromAdress(emailFromAddress);
		f.setPassword(emailPassword);
		f.setSmtpPort(emailSmtpPort);
		f.setSmtpServer(emailSmptServer);
		f.setUsername(emailUserName);

		System.out.println("task...");
	}

	@Override
	protected void validateProperties() throws BuildException {
		check(emailPassword, "please provide a password for your email server");
		check(emailSmptServer, "please provide your smtp server address");
		check(emailSmtpPort, "please provide your smtp port name");
		check(emailFromAddress, "please provide your email address");
		check(emailUserName, "please provide your username");
	}

	public String getEmailPassword() {
		return emailPassword;
	}

	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}

	public String getEmailSmptServer() {
		return emailSmptServer;
	}

	public void setEmailSmptServer(String emailSmptServer) {
		this.emailSmptServer = emailSmptServer;
	}

	public String getEmailSmtpPort() {
		return emailSmtpPort;
	}

	public void setEmailSmtpPort(String emailSmtpPort) {
		this.emailSmtpPort = emailSmtpPort;
	}

	public String getEmailFromAddress() {
		return emailFromAddress;
	}

	public void setEmailFromAddress(String emailFromAddress) {
		this.emailFromAddress = emailFromAddress;
	}

	public String getEmailUserName() {
		return emailUserName;
	}

	public void setEmailUserName(String emailUserName) {
		this.emailUserName = emailUserName;
	}
}
