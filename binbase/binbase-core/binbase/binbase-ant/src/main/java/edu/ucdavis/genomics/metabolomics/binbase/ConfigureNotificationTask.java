package edu.ucdavis.genomics.metabolomics.binbase;

import org.apache.tools.ant.BuildException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;

public class ConfigureNotificationTask extends AbstractApplicationServerTask{

	String emailAddress;
	
	@Override
	protected void execueTask() throws Exception {
		Configurator.getStatusService().addNotificationAdress(emailAddress);
	}

	@Override
	protected void validateProperties() throws BuildException {
		check(emailAddress,"please provide an email address");
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
