package edu.ucdavis.genomics.metabolomics.binbase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

import javax.naming.Context;

import org.apache.log4j.Logger;
import org.apache.tools.ant.AntClassLoader;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Java;
import org.apache.tools.ant.types.Path;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;

/**
 * takes care that the enviroment variables are set
 * 
 * @author nase
 */
public abstract class AbstractApplicationServerTask extends Task {

	private Project project;

	public Project getProject() {
		return project;
	}

	public void setProject(Project proj) {
		project = proj;

	}

	private String applicationServerIp;

	private Properties properties = new Properties();

	protected Logger logger = Logger.getLogger(getClass());

	private Exception thrown;

	@Override
	public final void execute() throws BuildException {
		log("checking variables");
		check(applicationServerIp,
				"sorry please provide an application server address!");

		validateProperties();

		saveParameter();

		try {
			log("creating java enviorement");
			Java java = new Java();
			java.setProject(getProject());
			java.setFork(true);
			java.setClassname(AbstractApplicationServerTask.class.getName());

			java.setClasspath(getClasspath());

			File f = File.createTempFile("config", "tmp");
			FileOutputStream out = new FileOutputStream(f);
			this.getProperties().store(out, "installer generated config file");
			out.flush();
			out.close();
			logger.info("configuration file save to: " + f.getAbsolutePath());

			java.createArg().setValue(this.getClass().getName());
			java.createArg().setValue(f.getAbsolutePath());
			int result = java.executeJava();

			logger.info("result code was: " + result);

			if (result != 0) {
				throw new BuildException(
						"result of the execution waas != 0, there was an error!");
			}
			if (thrown != null) {
				throw thrown;
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof BuildException) {
				throw (BuildException) e;
			} else {
				throw new BuildException(e.getMessage(), e);
			}

		}
	}

	private Path getClasspath() {
		String taskClasspath;
		if (this.getClass().getClassLoader() instanceof AntClassLoader) {
			AntClassLoader antClassLoader = (AntClassLoader) this.getClass()
					.getClassLoader();
			taskClasspath = System.getProperty("java.class.path") + ";"
					+ antClassLoader.getClasspath();
		} else {
			taskClasspath = System.getProperty("java.class.path");
		}
		Path classpath = new Path(getProject());
		classpath.setPath(taskClasspath);
		return classpath;
	}

	private void saveParameter() {
		logger.info("saving properties...");

		Method[] fields = this.getClass().getMethods();

		for (Method f : fields) {
			if (f.getName().startsWith("get")) {
				String pro = f.getName().replaceFirst("get", "");
				if (f.getParameterTypes().length == 0) {
					try {
						if (f.invoke(this, new Object[] {}) != null) {
							logger.info(("saving: " + pro + " - " + f.invoke(
									this, new Object[] {}).toString()));
							properties.setProperty(pro, f.invoke(this,
									new Object[] {}).toString());
						}
					} catch (Exception e) {
						throw new BuildException(e);
					}
				}
			} else if (f.getName().startsWith("is")) {
				String pro = f.getName().replaceFirst("is", "");
				if (f.getParameterTypes().length == 0) {
					try {
						if (f.invoke(this, new Object[] {}) != null) {
							logger.info(("saving: " + pro + " - " + f.invoke(
									this, new Object[] {}).toString()));
							properties.setProperty(pro, f.invoke(this,
									new Object[] {}).toString());
						}
					} catch (Exception e) {
						throw new BuildException(e);
					}
				}
			}
		}
	}

	protected Configurator getConfigurator() {
		return new Configurator();
	}

	protected abstract void execueTask() throws Exception;

	/**
	 * validates the properties
	 */
	protected abstract void validateProperties() throws BuildException;

	/**
	 * the application server address to use
	 * 
	 * @return
	 */
	public String getApplicationServerIp() {
		return applicationServerIp;
	}

	/**
	 * the application server address to use
	 * 
	 * @param applicationServerIp
	 */
	public void setApplicationServerIp(String applicationServerIp) {
		this.applicationServerIp = applicationServerIp;
	}

	protected final void check(Object o, String errormessage) {
		if (o == null)
			throw new BuildException(errormessage);
	}

	protected final boolean checkBoolean(Object o) {
		return (o != null);
	}

	/**
	 * starts the task and delegates it
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		AbstractApplicationServerTask task = (AbstractApplicationServerTask) Class
				.forName(args[0]).newInstance();
		try {
			Properties properties = new Properties();
			File file = new File(args[1]);
			file.deleteOnExit();
			properties.load((new FileInputStream(file)));
			task.setProperties(properties);
			task.validateProperties();
			task.execueTask();
		} catch (Exception e) {
			task.setThrown(e);
			e.printStackTrace();
			throw e;
		}
	}

	public Properties getProperties() {
		return properties;
	}

	private void setProperties(Properties properties)
			throws IllegalAccessException, InvocationTargetException {
		this.properties = properties;
		loadProperties(properties);
	}

	private void loadProperties(Properties properties)
			throws IllegalAccessException, InvocationTargetException {
		logger.info("loading properties...");
		Method[] fields = this.getClass().getMethods();

		for (Method f : fields) {
			if (f.getName().startsWith("set")) {
				String pro = f.getName().replaceFirst("set", "");
				if (properties.getProperty(pro) != null) {
					logger.info("loading: " + pro + ", " + properties.get(pro));
					invoke(properties.get(pro), f);
				}
			}
		}

		System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
				"org.jnp.interfaces.NamingContextFactory");
		System.setProperty(Context.URL_PKG_PREFIXES,
				"org.jboss.naming:org.jnp.interfaces");
		System.setProperty(Context.PROVIDER_URL, applicationServerIp + ":1099");

	}

	private void invoke(Object o, Method f) throws IllegalAccessException,
			InvocationTargetException {
		logger.info("value: " + o);
		try {

			if (invokeIt(Integer.parseInt(o.toString()), f))
				return;
		} catch (NumberFormatException e) {

		}
		try {

			if (invokeIt(Boolean.parseBoolean(o.toString()), f))
				return;
		} catch (NumberFormatException e) {
		}
		try {

			if (invokeIt(Long.parseLong(o.toString()), f))
				return;
		} catch (NumberFormatException e) {

		}
		try {
			if (invokeIt(o, f))
				return;
		} catch (NumberFormatException e) {

		}

	}

	private boolean invokeIt(Object o, Method f) throws IllegalAccessException,
			InvocationTargetException {
		try {
			f.invoke(this, o);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}

	}

	public Exception getThrown() {
		return thrown;
	}

	public void setThrown(Exception thrown) {
		this.thrown = thrown;
	}
}
