package edu.ucdavis.genomics.metabolomics.binbase;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import org.apache.tools.ant.BuildException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;

public class RegisterConfigFileTask extends AbstractApplicationServerTask {

	String configFile;

	public String getConfigFile() {
		return configFile;
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	@Override
	protected void execueTask() throws Exception {

		FileInputStream in = new FileInputStream(configFile);
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		File f = new File(configFile);
		String name = f.getName();

		logger.info("uploading config file: " + name + " from path: "
				+ f.getAbsolutePath());
		
		Copy.copy(in, out, true);

		byte[] content = out.toByteArray();

		Configurator.getImportService().uploadConfigFile(f.getName(),
				content);

		logger.info("done...");
	}

	@Override
	protected void validateProperties() throws BuildException {
		logger.info("validating that configfile is set: " + configFile);
		check(configFile != null,
				"please provide a configuration file to be registered");
		File f = new File(configFile);

		logger.info("validating that configfile exits: " + f.exists(	));
		check(f.exists(),
				"please provide an existing configuration file to be registered");

	}

}
