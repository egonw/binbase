package edu.ucdavis.genomics.metabolomics.binbase.spring;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.KeyStoreService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreException;

public class BinBaseKeyStoreHelper implements KeyStoreService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private KeyStoreService service;

	public boolean containsApplication(String application)
			throws KeyStoreException {
		return service.containsApplication(application);
	}

	public boolean containsKey(String key) throws KeyStoreException {
		return service.containsKey(key);
	}

	public KeyStoreService getService() {
		return service;
	}

	public void setService(KeyStoreService service) {
		this.service = service;
	}

	public void deleteAllKeys() throws KeyStoreException {
		service.deleteAllKeys();
	}

	public boolean existApplication(String applicationName) {
		return service.existApplication(applicationName);
	}

	public void removeApplication(String applicationName)
			throws KeyStoreException {
		service.removeApplication(applicationName);
	}

	public void storeKey(String applicationName, String key)
			throws KeyStoreException {
		service.storeKey(applicationName, key);
	}
}
