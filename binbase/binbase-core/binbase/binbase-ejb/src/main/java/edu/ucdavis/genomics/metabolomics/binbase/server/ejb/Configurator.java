package edu.ucdavis.genomics.metabolomics.binbase.server.ejb;

import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;

/**
 * access to servuce provided by this class
 * 
 * @author wohlgemuth
 */
public class Configurator extends edu.ucdavis.genomics.metabolomics.binbase.server.ejb.compounds.Configurator {

}
