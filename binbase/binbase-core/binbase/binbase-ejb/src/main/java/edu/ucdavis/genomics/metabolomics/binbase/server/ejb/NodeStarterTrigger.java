package edu.ucdavis.genomics.metabolomics.binbase.server.ejb;

import java.util.List;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;

import org.apache.log4j.Logger;
import org.jboss.annotation.ejb.ResourceAdapter;
import org.jboss.mq.server.jmx.QueueMBean;
import org.jboss.mx.util.MBeanServerLocator;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

//fire all 5 minutes
@MessageDriven(activationConfig = { @ActivationConfigProperty(propertyName = "cronTrigger", propertyValue = "0 0/5 * * * ?") })
@ResourceAdapter("quartz-ra.rar")
public class NodeStarterTrigger implements Job {

	private Logger logger = Logger.getLogger(getClass());

	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		try {
			logger.debug("ceck if we need to start a job");

			ObjectName name = new ObjectName("jboss.mq.destination:service=Queue,name=queue");
			MBeanServer server = MBeanServerLocator.locate();
			QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler.newProxyInstance(server, name, QueueMBean.class, false);
			List list = queue.listMessages();

			logger.debug("number of jobs scheduled: " + list.size());

			if (list.isEmpty() == false) {
				logger.debug("firing report event that we should start a job,since the queue is not empty");
				Report report = ReportFactory.newInstance().create("application server");
				report.report("the queue is not empty, so there should be a calculation running", Reports.SCHEDULE, Reports.TRIGGER);
			}
			else {
				logger.debug("no calculations needed");
			}
		}
		catch (Exception e) {
			logger.debug(e.getMessage(), e);
		}
	}

}
