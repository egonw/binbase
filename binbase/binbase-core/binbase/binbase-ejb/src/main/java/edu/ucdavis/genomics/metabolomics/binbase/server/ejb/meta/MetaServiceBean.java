package edu.ucdavis.genomics.metabolomics.binbase.server.ejb.meta;

import java.io.StringReader;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.CreateException;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.ejb.AbstractService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXProvider;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.ObjectNotFoundException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;

@Stateless
public class MetaServiceBean extends AbstractService  implements MetaService {

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public ExperimentClass[] getAllExperimentClassesInDatabase(String database, String key) throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		PreparedStatement classes = null;
		PreparedStatement samples = null;

		try {
			classes = connection.prepareStatement("SELECT class, count(class) FROM samples WHERE visible = 'TRUE' group by class");
			samples = connection.prepareStatement("SELECT  sample_name,setupx_id FROM samples  WHERE visible = 'TRUE' and class = ?");

			ResultSet res = classes.executeQuery();

			List<ExperimentClass> result = new ArrayList<ExperimentClass>();

			while (res.next()) {

				String clazzId = res.getString(1);
				int sampleSize = res.getInt(2);

				ExperimentClass clazz = new ExperimentClass();
				clazz.setColumn(database);
				clazz.setId(clazzId);

				ExperimentSample[] experimentSamples = new ExperimentSample[sampleSize];

				samples.setString(1, res.getString(1));
				ResultSet res2 = samples.executeQuery();

				int i = 0;
				while (res2.next()) {
					experimentSamples[i] = new ExperimentSample(res2.getString(2), res2.getString(1));
					i++;
				}
				res2.close();

				clazz.setSamples(experimentSamples);
				result.add(clazz);
			}
			res.close();
			classes.close();
			samples.close();

			return result.toArray(new ExperimentClass[result.size()]);
		}
		catch (SQLException e) {
			try {
				classes.close();
				samples.close();
			}
			catch (SQLException e1) {
				logger.debug(e.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e);
		}
		finally {
			closeConnection(connection);
		}
	}

	@Override
	public boolean hasExperimentClass(String metaId, String database, String key) throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		PreparedStatement classes = null;

		try {
			classes = connection.prepareStatement("SELECT class FROM samples WHERE visible = 'TRUE' and class = ? group by class");

			classes.setString(1, metaId);

			ResultSet res = classes.executeQuery();

			try {
				return res.next();
			}
			finally {
				res.close();
				classes.close();
			}
		}
		catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e);
		}
		finally {
			closeConnection(connection);
		}
	}

	@Override
	public ExperimentClass getExperimentClassForMetaId(String metaId, String database, String key) throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		PreparedStatement classes = null;
		PreparedStatement samples = null;

		try {
			classes = connection.prepareStatement("SELECT class, count(class) FROM samples WHERE visible = 'TRUE' and class = ? group by class");
			samples = connection.prepareStatement("SELECT  sample_name,setupx_id FROM samples  WHERE visible = 'TRUE' and class = ?");

			classes.setString(1, metaId);

			ResultSet res = classes.executeQuery();
			ExperimentClass clazz = new ExperimentClass();

			try {
				if (res.next()) {

					String clazzId = res.getString(1);
					int sampleSize = res.getInt(2);

					clazz.setColumn(database);
					clazz.setId(clazzId);

					ExperimentSample[] experimentSamples = new ExperimentSample[sampleSize];

					samples.setString(1, res.getString(1));
					ResultSet res2 = samples.executeQuery();

					int i = 0;
					while (res2.next()) {
						experimentSamples[i] = new ExperimentSample(res2.getString(2), res2.getString(1));
						i++;
					}
					res2.close();

					clazz.setSamples(experimentSamples);

					return clazz;
				}
				else {
					throw new BinBaseException("class not found for id: " + metaId);
				}
			}
			finally {
				res.close();
				classes.close();
				samples.close();
			}
		}
		catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e);
		}
		finally {
			closeConnection(connection);
		}
	}

	@Override
	public String getExperimentIdForClass(ExperimentClass clazz, String database, String key) throws BinBaseException {
		return getExperimentIdForClassByClassId(clazz.getId(), database, key);
	}

	@Override
	public String getExperimentIdForClassByClassId(String classId, String database, String key) throws BinBaseException {
		validateKey(key);

		if (hasExperimentClass(classId, database, key)) {
			SetupXProvider provider = SetupXFactory.newInstance().createProvider();
			String xml = provider.getMetaInformationByClass(classId);

			if (xml == null) {
				throw new ObjectNotFoundException("sorry this class was not found in setupX: " + classId);
			}
			else {
				Element element;
				try {
					element = XmlHandling.readXml(new StringReader(xml));

					xml = new XMLOutputter(Format.getPrettyFormat()).outputString(element);
					logger.trace(xml);
					String id = ((Element)((Element)element.getChild("Samples").getChildren("part").get(0)).getChildren("atom").get(2)).getText();
					logger.debug("received: " + id);
					return id;
				}
				catch (Exception e) {
					throw new BinBaseException(e.getMessage(),e);
				}
			}
		}
		else {
			throw new ObjectNotFoundException("sorry this class was not found in the database: " + classId);
		}
	}


	public static void main(String args[]) throws BinBaseException, RemoteException, CreateException, NamingException {
		XMLConfigurator.getInstance();
		System.setProperty(SetupXFactory.DEFAULT_PROPERTY_NAME, "edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateSetupXFactory");

		System.out.println(new MetaServiceBean().getExperimentIdForClassByClassId("609587", "rtx5", Configurator.getKeyManager().getInternalKey()));
	}
}
