package edu.ucdavis.genomics.metabolomics.binbase;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.DSLHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.ExperimentClassHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.ExperimentHandler;
import edu.ucdavis.genomics.metabolomics.util.status.Log4JReport;

/**
 * calculates data without the use of a scheduler and cluster
 * 
 * @author wohlgemuth
 * 
 */
public class StandaloneCalculator extends Calculator {

	/**
	 * calculates the import of the data
	 * 
	 * @param clazz
	 * @throws Exception
	 */
	public void calculateImport(ExperimentClass clazz) throws Exception {
		ExperimentClassHandler handler = new ExperimentClassHandler();
		handler.setReport(new Log4JReport());
		handler.setObject(clazz);
		handler.start();
	}

	/**
	 * calculates the export of the data
	 * 
	 * @param exp
	 * @throws Exception
	 */
	public void calculateExport(Experiment exp) throws Exception {
		ExperimentHandler handler = new ExperimentHandler();
		handler.setReport(new Log4JReport());
		handler.setObject(exp);
		handler.start();
	}

	@Override
	public void calculateDSL(DSL dsl) throws Exception {
		DSLHandler handler = new DSLHandler();
		handler.setReport(new Log4JReport());
		handler.setObject(dsl);
		handler.start();
		
	}
	
}
