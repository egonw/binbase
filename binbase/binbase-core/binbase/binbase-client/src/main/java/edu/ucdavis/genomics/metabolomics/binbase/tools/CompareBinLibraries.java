package edu.ucdavis.genomics.metabolomics.binbase.tools;

import java.io.FileOutputStream;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.XLS;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.math.Similarity;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;

public class CompareBinLibraries {

	/**
	 * generates a comparison of 2 databases and the similarity of there bins
	 * 
	 * @param databaseFirst
	 * @param databaseSecond
	 * @param minimumSimilarity
	 * @return
	 */
	public DataFile getBinComparision(String databaseFirst,
			String databaseSecond, double minimumSimilarity) throws Exception {
		
		if(databaseFirst.equals(databaseSecond)){
			throw new RuntimeException("well it does not really make much sense now if the first and second database are identical...");
		}

		Logger logger = Logger.getLogger(getClass());
		
		Session first = createSession(databaseFirst);
		Session second = createSession(databaseSecond);

		System.out.println(first.connection().getMetaData().getUserName());
		System.out.println(second.connection().getMetaData().getUserName());
		
		List<Bin> firstBins = first.createCriteria(Bin.class).list();
		List<Bin> secondBins = second.createCriteria(Bin.class).list();
		
		
		SimpleDatafile file = new SimpleDatafile();

		file.addEmptyColumn("bin_name - " + databaseFirst);
		file.addEmptyColumn("bin_name - " + databaseSecond);
		file.addEmptyColumn("bin_id - " + databaseFirst);
		file.addEmptyColumn("bin_id - " + databaseSecond);
		file.addEmptyColumn("bin ri - " + databaseFirst);
		file.addEmptyColumn("bin ri - " + databaseSecond);
		file.addEmptyColumn("ri distance");
		
		file.addEmptyColumn("similarity");

		for(Bin firstBin : firstBins){
			Similarity sim = new Similarity();
			sim.setLibrarySpectra(firstBin.getMassSpec());
			for(Bin secondBin : secondBins){

				sim.setUnknownSpectra(secondBin.getMassSpec());

				double similarity = sim.calculateSimimlarity();
				
				if(similarity >= minimumSimilarity){
					List row = new Vector();
					row.add(firstBin.getName());
					row.add(secondBin.getName());

					row.add(firstBin.getId());
					row.add(secondBin.getId());
					row.add(firstBin.getRetentionIndex());
					row.add(secondBin.getRetentionIndex());
					row.add(firstBin.getRetentionIndex()-secondBin.getRetentionIndex());
					row.add(similarity);
					file.addRow(row);
				
					logger.info(row);
				}
			}
		}
		return file;
	}

	private Session createSession(String database) throws RemoteException, BinBaseException, CreateException, NamingException {
		Properties main = Configurator.getDatabaseService().createProperties();

		Properties properties = (XMLConfigurator.getInstance().getProperties());
		properties.putAll(main);

		properties.put("Binbase.user", database);


		Session first = HibernateFactory.newInstance(properties,true)
				.getSession();
		return first;
	}

	public static void main(String[] args) throws NumberFormatException, Exception {
		if (args.length != 4) {

			System.out.println("Usage:");
			System.out.println("arg[0] - first database");
			System.out.println("arg[1] - second database");
			System.out.println("arg[2] - minimum similarity");
			System.out.println("arg[3] - application server");

			return;
		}

		XMLConfigurator.getInstance().reset();
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.factory.initial",
				"org.jnp.interfaces.NamingContextFactory", true);
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.factory.url.pkgs",
				"org.jboss.naming:org.jnp.interfaces", true);
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.provider.url",
				args[3] + ":1099", true);
		
		XMLConfigurator.getInstance().addConfiguration(new ResourceSource("/config/hibernate.xml"));

		
		CompareBinLibraries comp = new CompareBinLibraries();

		DataFile file = comp.getBinComparision(args[0], args[1], Double
				.parseDouble(args[2]));
		
	
		XLS xls = new XLS();
		FileOutputStream out = new FileOutputStream("result-" +args[0] + " vs "+ args[1]+ ".xls");
		xls.write(out, file);
		out.flush();
		out.close();
	}
}
