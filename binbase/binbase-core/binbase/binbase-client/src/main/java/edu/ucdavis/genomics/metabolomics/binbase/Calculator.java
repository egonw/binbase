package edu.ucdavis.genomics.metabolomics.binbase;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;

/**
 * provides a simple access to the binbases and allows the import and export of data
 * 
 */
public abstract class Calculator {

	/**
	 * imports and exports the given experiment
	 * @param exp
	 * @throws Exception
	 */
	public void calculateImportAndExport(Experiment exp) throws Exception {
		for (ExperimentClass clazz : exp.getClasses()) {
			calculateImport(clazz);
		}
		calculateExport(exp);
	}

	/**
	 * exports the given experiment
	 * @param exp
	 */
	public abstract void calculateExport(Experiment exp) throws Exception;
	
	/**
	 * imports the given class
	 * @param clazz
	 * @throws Exception 
	 */
	public abstract void calculateImport(ExperimentClass clazz) throws Exception;
	
	public abstract void calculateDSL(DSL dsl) throws Exception;
}
