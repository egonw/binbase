package edu.ucdavis.genomics.metabolomics.binbase;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;

/**
 * calculates the data using a cluster and scheduler
 * @author wohlgemuth
 *
 */
public class ClusterCalculator extends Calculator{

	@Override
	public void calculateExport(Experiment exp) throws Exception {
		BinBaseServices.getScheduler().scheduleExport(exp, KeyFactory.newInstance().getKey());
	}

	@Override
	public void calculateImport(ExperimentClass clazz) throws Exception {
		BinBaseServices.getScheduler().scheduleImport(clazz, KeyFactory.newInstance().getKey());
	}

	@Override
	public void calculateDSL(DSL dsl) throws Exception {
		BinBaseServices.getScheduler().scheduleDSL(dsl, KeyFactory.newInstance().getKey());
		
	}

}
