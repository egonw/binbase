import edu.ucdavis.genomics.metabolomics.binbase.dsl.dsl.ExporterDSL
import edu.ucdavis.genomics.metabolomics.binbase.dsl.calc.*

/**
 * User: wohlgemuth
 * Date: Sep 18, 2009
 * Time: 12:56:41 PM
 *
 */

public class Exporter {


  /**
   * main method which executes this process
   */
  static void main(String[] args) {

    try {
      //if no argument is given we assume it's the quant.dsl file
      if (args.size() == 0) {
        if (new File("exporter.dsl").exists() == false) {
          println "sorry file does not exist in launch directory! Please specify 'exporter.dsl' location"
        }
        else {
          new LocalExporter(new File("exporter.dsl").run())

        }
      }
      //otherwise execute it for all given files
      else {
        for (String s in args) {
          println "runnning: ${s}"

          File file = new File(s)
		
		new LocalExporter(file).run()
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace()
    }
	finally{
		System.exit 1
	}
  }
}