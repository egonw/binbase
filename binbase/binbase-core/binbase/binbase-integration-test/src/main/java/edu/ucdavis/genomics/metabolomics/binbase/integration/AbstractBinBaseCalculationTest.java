package edu.ucdavis.genomics.metabolomics.binbase.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.BinBaseConfiguredIntegrationTest;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

/**
 * tests the import and export of samples is only an abstract class and the
 * actual implementation has to be done by the sub classes. The reason is that
 * we can test it standalone, with scheduler, withouth scheduler and so on
 * 
 * @author wohlgemuth
 * 
 */
public abstract class AbstractBinBaseCalculationTest extends
		GeneralAbstractCalculationTest {

	private Logger logger = Logger.getLogger(getClass());

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * tests if we generate bins
	 * 
	 * @throws Exception
	 */
	@Test(timeout = 3800000)
	public void testCreateBinDataSet() throws Exception {

		logger.info("createing class");
		ExperimentClass clazz = new ExperimentClass();
		clazz.setId("1");
		clazz.setColumn(System.getProperty("Binbase.user"));

		logger.info("adding samples to class");

		ExperimentSample samples[] = new ExperimentSample[6];
		samples[0] = new ExperimentSample("4125ea02_2", "4125ea02_2");
		samples[1] = new ExperimentSample("4125ea03_2", "4125ea03_2");
		samples[2] = new ExperimentSample("4125ea04_2", "4125ea04_2");
		samples[3] = new ExperimentSample("4125ea05_2", "4125ea05_2");
		samples[4] = new ExperimentSample("4125ea06_2", "4125ea06_2");
		samples[5] = new ExperimentSample("4125ea07_2", "4125ea07_2");

		clazz.setSamples(samples);

		scheduleImport(clazz);
		calculateScheduledExperimentClass();

		logger.info("creating experiment...");

		Experiment exp = new Experiment();
		exp.setClasses(new ExperimentClass[] { clazz });
		exp.setColumn(clazz.getColumn());
		exp.setId("2");

		// TODO problem is here
		logger.info("scheduling export");
		scheduleExport(exp);

		logger.info("starting with the calculation..."); // calculate the
		// export
		calculateScheduledExperiment();
		// amalyse if the exspected result is correct - database

		logger.info("check if bins where created...");
		logger.info("bin count: " + this.getBinCount());
		assertTrue(this.getBinCount() == 134);

		logger.info("check FOUND count of spectra...");
		assertTrue(this.getSpectraAnotatedCount() == 713);

		logger.info("check NOT FOUND count of spectra...");
		assertTrue(this.getSpectraNotAnotatedCount() == 1130);

		logger.info("check if the sample table contains the data we want...");
		assertTrue(this.getSampleCount() == 7);

		logger.info("check on experiment");
		assertTrue(this.getResultCount() == 1);

		logger.info("check on sample links");
		assertTrue(this.getResultLinkCount() == 6);
	}

	/**
	 * tests if we create no bins
	 * 
	 * @throws Exception
	 */
	@Test(timeout = 3800000)
	public void testCreateNoBinsDataSet() throws Exception {

		logger.info("createing class");
		ExperimentClass clazz = new ExperimentClass();
		clazz.setId("1");
		clazz.setColumn(System.getProperty("Binbase.user"));

		logger.info("adding samples to class");
		ExperimentSample samples[] = new ExperimentSample[3];
		samples[0] = new ExperimentSample("4125ea02_2", "4125ea02_2");
		samples[1] = new ExperimentSample("4125ea03_2", "4125ea03_2");
		samples[2] = new ExperimentSample("4125ea04_2", "4125ea04_2");

		clazz.setSamples(samples);
		// schedule this data scheduleImport(clazz);
		// import
		scheduleImport(clazz);
		calculateScheduledExperimentClass();

		logger.info("creating experiment...");

		Experiment exp = new Experiment();
		exp.setClasses(new ExperimentClass[] { clazz });
		exp.setColumn(clazz.getColumn());
		exp.setId("2");

		logger.info("scheduling export");
		scheduleExport(exp);

		logger.info("starting with the calculation..."); // calculate the
		// export
		calculateScheduledExperiment();
		// amalyse if the exspected result is correct - database

		logger.info("check if bins were NOT created...");
		assertTrue(getBinCount() == 13);

		logger.info("check FOUND count of spectra...");
		assertTrue(getSpectraAnotatedCount() == 51);

		logger.info("check NOT FOUND count of spectra...");
		assertTrue(getSpectraNotAnotatedCount() == 983);

		logger.info("check if the sample table contains the data we want...");
		assertTrue(getSampleCount() == 4);

		logger.info("check on experiment");
		assertTrue(getResultCount() == 1);

		logger.info("check on sample links");
		assertTrue(getResultLinkCount() == 3);

	}

	/**
	 * tests that our data are reproducable. For this we use 6 identical samples
	 * and want to see that all of them contain the same result
	 * 
	 * @throws Exception
	 */
	@Test(timeout = 3800000)
	public void testReproducabilityDataSet() throws Exception {

		logger.info("createing class");
		ExperimentClass clazz = new ExperimentClass();
		clazz.setId("1");
		clazz.setColumn(System.getProperty("Binbase.user"));

		logger.info("adding samples to class");
		ExperimentSample samples[] = new ExperimentSample[6];
		samples[0] = new ExperimentSample("4125ea02_1", "4125ea02_1");
		samples[1] = new ExperimentSample("4125ea02_2", "4125ea02_2");
		samples[2] = new ExperimentSample("4125ea02_3", "4125ea02_3");
		samples[3] = new ExperimentSample("4125ea02_4", "4125ea02_4");
		samples[4] = new ExperimentSample("4125ea02_5", "4125ea02_5");
		samples[5] = new ExperimentSample("4125ea02_6", "4125ea02_6");

		clazz.setSamples(samples);

		// schedule that data
		scheduleImport(clazz);

		// calculate the import
		calculateScheduledExperimentClass();

		Experiment exp = new Experiment();
		exp.setClasses(new ExperimentClass[] { clazz });
		exp.setColumn(clazz.getColumn());
		exp.setId("2");

		scheduleExport(exp);
		// calculate the export
		calculateScheduledExperiment();

		// amalyse if the exspected result is correct

		logger.info("check if bins were NOT created...");
		assertTrue(getBinCount() == 110);

		logger.info("check FOUND count of spectra...");
		assertTrue(getSpectraAnotatedCount() == 667);

		logger.info("check NOT FOUND count of spectra...");
		assertTrue(getSpectraNotAnotatedCount() == 1374);

		logger.info("check if the sample table contains the data we want...");
		assertTrue(getSampleCount() == 7);

		logger.info("check on experiment");
		assertTrue(getResultCount() == 1);

		logger.info("check on sample links");
		assertTrue(getResultLinkCount() == 6);

	}

	/**
	 * a more complex experiment to test how well the paralell processing works
	 * which can take quiete a while till its finished
	 * 
	 * @throws Exception
	 */
	@Test(timeout =16800000)
	public void testParrallelCalculation() throws Exception {

		logger.info("createing classes...");

		// no bins
		ExperimentClass clazzes[] = new ExperimentClass[3];
		{
			ExperimentClass clazz = new ExperimentClass();
			clazz.setId("1");
			clazz.setColumn(System.getProperty("Binbase.user"));

			logger.info("adding samples to class");

			ExperimentSample samples[] = new ExperimentSample[3];
			samples[0] = new ExperimentSample("4149ea30_1", "4149ea30_1");
			samples[1] = new ExperimentSample("4149ea30_2", "4149ea30_2");
			samples[2] = new ExperimentSample("4149ea30_3", "4149ea30_3");

			clazz.setSamples(samples);

			clazzes[0] = clazz;
		}

		// no bins
		{
			ExperimentClass clazz = new ExperimentClass();
			clazz.setId("2");
			clazz.setColumn(System.getProperty("Binbase.user"));

			logger.info("adding samples to class");

			ExperimentSample samples[] = new ExperimentSample[3];
			samples[0] = new ExperimentSample("061108byusa35_1",
					"061108byusa35_1");
			samples[1] = new ExperimentSample("061108byusa35_2",
					"061108byusa35_2");
			samples[2] = new ExperimentSample("061108byusa35_3",
					"061108byusa35_3");
			clazz.setSamples(samples);

			clazzes[1] = clazz;
		}
		// create bins
		{
			ExperimentClass clazz = new ExperimentClass();
			clazz.setId("3");
			clazz.setColumn(System.getProperty("Binbase.user"));

			logger.info("adding samples to class");

			ExperimentSample samples[] = new ExperimentSample[6];
			samples[0] = new ExperimentSample("061114bfisa238_1",
					"061114bfisa238_1");
			samples[1] = new ExperimentSample("061114bfisa238_2",
					"061114bfisa238_2");
			samples[2] = new ExperimentSample("061114bfisa238_3",
					"061114bfisa238_3");
			samples[3] = new ExperimentSample("061114bfisa238_4",
					"061114bfisa238_4");
			samples[4] = new ExperimentSample("061114bfisa238_5",
					"061114bfisa238_5");
			samples[5] = new ExperimentSample("061114bfisa238_6",
					"061114bfisa238_6");

			clazz.setSamples(samples);

			clazzes[2] = clazz;
		}

		logger.info("define experiment");
		Experiment exp = new Experiment();
		exp.setClasses(clazzes);
		exp.setColumn(System.getProperty("Binbase.user"));
		exp.setId("4");

		logger.info("schedule classes");
		for (ExperimentClass clazz : clazzes) {
			logger.info("schedule clazz: " + clazz);
			scheduleImport(clazz);
		}

		logger.info("calculate scheduled classes");
		calculateScheduledExperimentClass();

		logger.info("scheduling export");
		scheduleExport(exp);

		logger.info("calculate scheduled experiment"); // calculate the
		// export
		calculateScheduledExperiment();
		// amalyse if the exspected result is correct - database

	}

	/**
	 * calculates the scheduled class
	 * 
	 * @throws Exception
	 */
	protected abstract void calculateScheduledExperimentClass()
			throws Exception;

	/**
	 * calculates the scheduled experiment
	 * 
	 * @throws Exception
	 */
	protected abstract void calculateScheduledExperiment() throws Exception;

	/**
	 * schedules our samples
	 * 
	 * @param clazz
	 */
	protected abstract void scheduleImport(ExperimentClass clazz)
			throws Exception;

	/**
	 * schedules the export
	 * 
	 * @param exp
	 */
	protected abstract void scheduleExport(Experiment exp) throws Exception;

}
