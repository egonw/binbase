package edu.ucdavis.genomics.metabolomics.binbase.integration.cluster;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseServices;
import edu.ucdavis.genomics.metabolomics.binbase.bci.BinBaseConfiguredIntegrationTest;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.PostMatchSampleHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.UpdateBinBaseHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.PostmatchingJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.UpdateBinBaseJob;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

public abstract class BinBaseUpdateDatabaseCalculationTest extends BinBaseConfiguredIntegrationTest {
	private Logger logger = Logger.getLogger(getClass());

	@Override
	protected IDataSet getDataSet() throws Exception {
		getLogger().info("looking for datafile...");
		ResourceSource source = new ResourceSource("/database/query-helper-rtx5.xml");
		getLogger().debug("using source: " + source.getSourceName() + " exist: " + source.exist());
		assertTrue(source.exist());
		return new XmlDataSet(source.getStream());
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
		getLogger().info("adding directories with content to the applciation server...");
		super.addTestImportDirectory(new File("src/test/resources/sampleExperiment/createBins"));
		super.addTestImportDirectory(new File("src/test/resources/sampleExperiment/noBins"));
		super.addTestImportDirectory(new File("src/test/resources/sampleExperiment/reproduce"));
		super.addTestImportDirectory(new File("src/test/resources/sampleExperiment/complex"));

		super.addTestSopDirectory(new File("src/test/resources/sop"));

		getLogger().info("define default sop");
		Configurator.getExportService().setDefaultSop("simple.stat");

		assertTrue(Configurator.getExportService().listSops().contains("simple.stat"));

		getLogger().info("uploading config files");
		super.addConfigFile(new File("src/test/resources/config/binbase.xml"));
		super.addConfigFile(new File("src/test/resources/config/sql.xml"));
		super.addConfigFile(new File("src/test/resources/config/targets.xml"));

		Collection config = Configurator.getImportService().listConfigFiles();

		assertTrue(config.contains("binbase.xml"));
		assertTrue(config.contains("sql.xml"));
		assertTrue(config.contains("targets.xml"));

		getLogger().info("start with calculation");
		
		Configurator.getConfigService().addHandler(UpdateBinBaseJob.class.getName(), UpdateBinBaseHandler.class.getName());
		Configurator.getConfigService().addHandler(PostmatchingJob.class.getName(), PostMatchSampleHandler.class.getName());

	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	/**
	 * updates the database and recalculates samples
	 * @throws Exception 
	 */
	@Test(timeout=180000)
	public void updateDatabase() throws Exception{
	
		UpdateBinBaseJob job = new UpdateBinBaseJob();
		job.setColumn(System.getProperty("Binbase.user"));

		scheduleUpdate(job);
		assertTrue(this.getMessageQueue().size() == 1);
		calculate();

		assertTrue(this.getMessageQueue().size() == 0);
		assertTrue(outdatedSampleCount() == 0);
		
	}

	private int outdatedSampleCount() throws Exception{

		int res = BinBaseServices.getService().getOutDatedSampleIds("test", KeyFactory.newInstance().getKey()).length;
		logger.info("count: " + res);
		return res;
	}
	/**
	 * does the actual scheduling
	 * @param job
	 * @throws Exception 
	 */
	protected abstract void scheduleUpdate(UpdateBinBaseJob job) throws Exception;
	
	/**
	 * does the actual calculation
	 * @throws Exception 
	 */
	protected abstract void calculate() throws Exception;

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Logger getLogger() {
		return logger;
	}
}
