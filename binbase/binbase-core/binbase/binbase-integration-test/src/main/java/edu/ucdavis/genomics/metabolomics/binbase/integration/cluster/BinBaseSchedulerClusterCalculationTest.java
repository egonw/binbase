package edu.ucdavis.genomics.metabolomics.binbase.integration.cluster;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseServices;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;

/**
 * uses a scheduler to start the import and export of data on the cluster
 * 
 * @author wohlgemuth
 * 
 */
public abstract class BinBaseSchedulerClusterCalculationTest extends
		BinBaseClusterCalculationTest {

	@Override
	protected void scheduleExport(Experiment exp) throws Exception {
		Scheduler scheduler = BinBaseServices.getScheduler();
		scheduler.scheduleExport(exp, KeyFactory.newInstance().getKey());

		// starting the node
		UTIL.startNode();
		// we wont wait, since we want to simulate real cluster use
	}

	@Override
	protected void scheduleImport(ExperimentClass clazz) throws Exception {
		Scheduler scheduler = BinBaseServices.getScheduler();
		scheduler.scheduleImport(clazz, KeyFactory.newInstance().getKey());

		// starting the node
		UTIL.startNode();
		// we wont wait, since we want to simulate real cluster use
	}

	// needs to be called...
	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}
}
