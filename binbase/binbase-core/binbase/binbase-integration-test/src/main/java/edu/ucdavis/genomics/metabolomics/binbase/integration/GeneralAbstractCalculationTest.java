package edu.ucdavis.genomics.metabolomics.binbase.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;

import edu.ucdavis.genomics.metabolomics.binbase.bci.BinBaseConfiguredIntegrationTest;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.io.ConfigSource;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

public abstract class GeneralAbstractCalculationTest extends BinBaseConfiguredIntegrationTest {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public void setUp() throws Exception {
		super.setUp();
		logger.info("adding directories with content to the applciation server...");
		super.addTestImportDirectory(new File("src/test/resources/sampleExperiment/createBins"));
		super.addTestImportDirectory(new File("src/test/resources/sampleExperiment/noBins"));
		super.addTestImportDirectory(new File("src/test/resources/sampleExperiment/reproduce"));
		super.addTestImportDirectory(new File("src/test/resources/sampleExperiment/complex"));

		super.addTestSopDirectory(new File("src/test/resources/sop"));

		logger.info("define default sop");
		Configurator.getExportService().setDefaultSop("simple.stat");

		assertTrue(Configurator.getExportService().listSops().contains("simple.stat"));

		logger.info("uploading config files");
		super.addConfig(getClass().getResourceAsStream("/config/binbase.xml"), "binbase.xml");

		super.addConfig(getClass().getResourceAsStream("/config/sql.xml"), "sql.xml");
		super.addConfigFile(new File("src/test/resources/config/targets.xml"));

		logger.info("release all locks");
		Collection config = Configurator.getImportService().listConfigFiles();

		assertTrue(config.contains("binbase.xml"));
		assertTrue(config.contains("sql.xml"));
		assertTrue(config.contains("targets.xml"));

		Configurator.getLockingService().releaseAllRessources();
		
		logger.info("start with calculation");
		XMLConfigurator.getInstance().addConfiguration(new ConfigSource("binbase.xml"));
		XMLConfigurator.getInstance().addConfiguration(new ConfigSource("sql.xml"));
		
		XMLConfigurator.getInstance().getXMLConfigable("binbase.config").printTree(System.out);
	}
	

	@Override
	protected IDataSet getDataSet() throws Exception {
		logger.info("looking for datafile...");
		ResourceSource source = new ResourceSource(
				"/database/query-helper-rtx5.xml");
		logger.debug("using source: " + source.getSourceName() + " exist: "
				+ source.exist());
		assertTrue(source.exist());
		return new XmlDataSet(source.getStream());
	}

	/**
	 * returns the given count query
	 * 
	 * @param query
	 * @return
	 * @throws SQLException
	 */
	protected int executeCountStatement(String query) throws SQLException {
		logger.info("executing query: " + query);
		ResultSet result = this.getConnection().createStatement().executeQuery(
				query);
		result.next();
		int res = result.getInt(1);
		logger.info("result is: " + res);
		result.close();
		return res;
	}

	protected int getBinCount() throws SQLException {
		return executeCountStatement("select count(*) from bin");
	}

	protected int getSpectraAnotatedCount() throws SQLException {
		return executeCountStatement("select count(*) from spectra where \"bin_id\" is not null");
	}


	protected int getSpectraNotAnotatedCount(String sample) throws SQLException {
		return executeCountStatement("select count(*) from spectra a, samples b where a.sample_id = b.sample_id and b.sample_name = '" + sample + "' and \"bin_id\" is null");
	}


	protected int getSpectraAnotatedCount(String sample) throws SQLException {
		return executeCountStatement("select count(*) from spectra a, samples b where a.sample_id = b.sample_id and b.sample_name = '" + sample + "' and \"bin_id\" is not null");
	}

	
	
	protected int getSpectraNotAnotatedCount() throws SQLException {
		return executeCountStatement("select count(*) from spectra where \"bin_id\" is null");
	}

	protected int getSampleCount() throws SQLException {
		return executeCountStatement("select count(*) from samples");
	}

	protected int getResultCount() throws SQLException {
		return executeCountStatement("select count(*) from result");
	}

	protected int getResultLinkCount() throws SQLException {
		return executeCountStatement("select count(*) from result_link");
	}

}
