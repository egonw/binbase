package edu.ucdavis.genomics.metabolomics.binbase.integration.standalone;

import java.util.Vector;

import org.junit.After;
import org.junit.Before;

import junit.framework.Assert;

import edu.ucdavis.genomics.metabolomics.binbase.StandaloneCalculator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.integration.AbstractBinBaseCalculationTest;

/**
 * caluclates the data using no cluster and no schedules so the simplest
 * possible way
 * 
 * @author wohlgemuth
 * 
 */
public class BinBaseStandaloneCalculationTest extends AbstractBinBaseCalculationTest {

	private java.util.Vector<ExperimentClass> clazz = new Vector<ExperimentClass>();

	private Vector<Experiment> exp = new Vector<Experiment>();

	@Override
	protected void calculateScheduledExperiment() throws Exception {
		Assert.assertTrue(this.exp.isEmpty() == false);
		for (Experiment exp : this.exp) {
			new StandaloneCalculator().calculateExport(exp);
		}
	}

	@Override
	protected void calculateScheduledExperimentClass() throws Exception {
		Assert.assertTrue(this.clazz.isEmpty() == false);
		for (ExperimentClass clazz : this.clazz) {
			new StandaloneCalculator().calculateImport(clazz);
		}
	}

	@Override
	protected void scheduleExport(Experiment exp) throws Exception {
		this.exp.add(exp);
	}

	@Override
	protected void scheduleImport(ExperimentClass clazz) throws Exception {
		this.clazz.add(clazz);
	}
	

	// needs to be called...
	@Before
	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

}
