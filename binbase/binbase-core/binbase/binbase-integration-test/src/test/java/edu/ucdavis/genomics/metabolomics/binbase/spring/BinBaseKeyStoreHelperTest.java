package edu.ucdavis.genomics.metabolomics.binbase.spring;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreException;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;

public class BinBaseKeyStoreHelperTest extends AbstractApplicationServerTest {

	BinBaseKeyStoreHelper store;

	@Before
	public void setUp() throws Exception {
		super.setUp();

		System.setProperty("application.server", System.getProperty("test.binbase.cluster.server"));
		ApplicationContext context = new ClassPathXmlApplicationContext("binbase-ejb-services.xml");

		store = (BinBaseKeyStoreHelper) context.getBean("KeyStoreHelper");
		assertTrue(store != null);
		store.deleteAllKeys();

	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();

	}


	@Test
	public void testRemoveKey() throws KeyStoreException {
		for (int i = 0; i < 10; i++) {
			store.storeKey("app:" + i, "key:" + i);
			assertTrue(store.containsKey("key:"+i));
		}
		for (int i = 9; i > 0; i--) {
			store.removeApplication("app:" + i);
			assertTrue(!store.containsKey("key:"+i));
		}

	}

	@Test
	public void testStoreKey() throws KeyStoreException {
		for (int i = 0; i < 10; i++) {
			store.storeKey("app:" + i, "key:" + i);
			assertTrue(store.containsKey("key:"+i));
		}
	}

	@Test
	public void testExistApplication() throws KeyStoreException {
		for (int i = 0; i < 10; i++) {
			store.storeKey("app:" + i, "key:" + i);
			assertTrue(store.existApplication("app:" + i));
			assertTrue(store.existApplication("apdadasp:" + i) == false);

		}
	}

}
