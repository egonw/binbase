package edu.ucdavis.genomics.metabolomics.binbase.integration.cluster;

import java.io.File;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.RocksClusterImplementation;
import edu.ucdavis.genomics.metabolomics.util.PropertySetter;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;

/**
 * calculates the data on a rocks cluster test using the standard
 * 
 * @author wohlgemuth
 * 
 */
public class RocksBinBaseSchedulerClusterCalculationTest extends
		BinBaseNoSchedulerClusterCalculationTest {

	/**
	 * needed to tell the cluster util the correct implementation
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void initializeCluster() throws Exception {
		Logger logger = Logger
				.getLogger(RocksBinBaseSchedulerClusterCalculationTest.class);
		PropertySetter
				.setPropertiesToSystem("src/test/resources/test.properties");
		UTIL = new RocksClusterImplementation(System
				.getProperty("test.binbase.cluster.server"), System
				.getProperty("test.binbase.cluster.username"), System
				.getProperty("test.binbase.cluster.password"));

		// initialize the util
		initializeUtil();

		// upload libraries needed for test
		File dir = new File("target/binbase-integration-test.dir/");

		for (File lib : dir.listFiles()) {
			logger.info("uploading libary: " + lib.getName());
			UTIL.uploadLibrary(lib.getName(), new FileSource(lib));
		}
		// ready to play
	}

	// needs to be called...
	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}
}
