package edu.ucdavis.genomics.metabolomics.binbase;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import org.apache.tools.ant.Project;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class ConfigureDatabaseTaskTest extends AbstractApplicationServerTest {

	ConfigureDatabaseTask task;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		task = new ConfigureDatabaseTask();
		task.setProject(new Project());
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testExecute() throws RemoteException, BinBaseException, CreateException, NamingException{
		task.setApplicationServerIp(System.getProperty("test.binbase.cluster.server"));
		task.setDatabase(System.getProperty("Binbase.database"));
		task.setPassword(System.getProperty("Binbase.password"));
		task.setServer(System.getProperty("Binbase.host"));
		task.setUsername(System.getProperty("Binbase.user"));
		
		task.execute();

		assertTrue(Configurator.getDatabaseService().getDatabase().equals(task.getDatabase()));
		assertTrue(Configurator.getDatabaseService().getDatabaseServer().equals(task.getServer()));
		assertTrue(Configurator.getDatabaseService().getDatabaseServerPassword().equals(task.getPassword()));
	}
	
}
