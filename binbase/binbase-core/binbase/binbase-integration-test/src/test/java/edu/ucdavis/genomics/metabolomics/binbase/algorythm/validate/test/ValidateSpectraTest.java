/*
 * Created on Sep 5, 2003
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.test;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.ValidateSpectra;
import edu.ucdavis.genomics.metabolomics.util.math.SpectraArrayKey;

import junit.framework.TestCase;


/**
 * @author wohlgemuth
 * @version Sep 5, 2003
 * <br>
 * BinBaseDatabase
 * @description
 */
public class ValidateSpectraTest extends TestCase {
    String spectra = null;

    /**
     * Constructor for ValidateSpectraTest.
     * @param arg0
     */
    public ValidateSpectraTest(String arg0) {
        super(arg0);
    }

    /**
     * DOCUMENT ME!
     */
    public static final void testAdd() {
        String spec_a = "1:1 2:2 3:3 4:4 5:5";
        String spec_b = "6:6 7:7 8:8 9:9 10:100 11:11 12:12 13:13 14:14 15:15";

        double[][] spec_A = ValidateSpectra.convert(spec_a);
        double[][] spec_B = ValidateSpectra.convert(spec_b);
        double[][] spec_C = ValidateSpectra.add(spec_A, spec_B);

        assertTrue(ValidateSpectra.sizeDown(spec_C).length == (ValidateSpectra.sizeDown(
                spec_A).length + ValidateSpectra.sizeDown(spec_B).length));

        spec_C = ValidateSpectra.sizeDown(spec_C);

        for (int i = 0; i < 15; i++) {
            assertTrue(Math.abs(spec_C[i][0] - (i + 1)) < 0.0001);
        }
    }

    /**
     * DOCUMENT ME!
     */
    public static final void testRenoising() {
        String apex = "1+2+3+4";
        String spec = "1:1 2:2 3:3 4:4 5:5 6:5 7:5 8:5 9:56 10:100";

        double[][] spec_ = ValidateSpectra.renoising(ValidateSpectra.convert(
                    spec), 50, apex);

        {
            boolean assert_ = false;

            for (int i = 0; i < spec_.length; i++) {
                if (Math.abs(spec_[i][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                            1) < 0.0001) {
                    assert_ = true;
                }
            }

            assertTrue(assert_);
        }

        {
            boolean assert_ = false;

            for (int i = 0; i < spec_.length; i++) {
                if (Math.abs(spec_[i][0] - 2) < 0.0001) {
                    assert_ = true;
                }
            }

            assertTrue(assert_);
        }

        {
            boolean assert_ = false;

            for (int i = 0; i < spec_.length; i++) {
                if (Math.abs(spec_[i][0] - 3) < 0.0001) {
                    assert_ = true;
                }
            }

            assertTrue(assert_);
        }

        {
            boolean assert_ = false;

            for (int i = 0; i < spec_.length; i++) {
                if (Math.abs(spec_[i][0] - 4) < 0.0001) {
                    assert_ = true;
                }
            }

            assertTrue(assert_);
        }
    }

    /**
     * DOCUMENT ME!
     */
    public final void testSimilarity() {
        assertTrue(Math.abs(ValidateSpectra.similarity(ValidateSpectra.convert(
                        spectra), ValidateSpectra.convert(spectra)) - 1000) < 0.0001);
    }

    /**
     * DOCUMENT ME!
     */
    public static final void testSizeDown() {
        String spec = "1:1 2:2 3:3 4:4 5:5 6:0 7:0 8:0 9:0 10:0";

        double[][] spec_b = ValidateSpectra.sizeDown(ValidateSpectra.convert(
                    spec));

        assertTrue(spec_b.length == 5);
    }

    /**
     * DOCUMENT ME!
     */
    public static final void testSizeUp() {
        String spec = "1:1 2:2 3:3 4:4 5:5 6:0 7:0 8:0 9:0 10:0";

        double[][] spec_b = ValidateSpectra.sizeUp(ValidateSpectra.convert(spec));

        assertTrue(spec_b.length == 1000);
    }

    /**
     * DOCUMENT ME!
     */
    public static final void testSortSpectra() {
        String spec = "1:1 2:2 3:3 4:4 5:5 6:6 7:7 8:8 9:56 10:100";
        double[][] spec_ = ValidateSpectra.sortSpectra(ValidateSpectra.convert(
                    spec), SpectraArrayKey.FRAGMENT_ION_POSITION);

        assertTrue(Math.abs(spec_[0][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                1) < 0.0001);
        assertTrue(Math.abs(spec_[1][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                2) < 0.0001);
        assertTrue(Math.abs(spec_[2][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                3) < 0.0001);
        assertTrue(Math.abs(spec_[3][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                4) < 0.0001);
        assertTrue(Math.abs(spec_[4][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                5) < 0.0001);
        assertTrue(Math.abs(spec_[5][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                6) < 0.0001);
        assertTrue(Math.abs(spec_[6][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                7) < 0.0001);
        assertTrue(Math.abs(spec_[7][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                8) < 0.0001);
        assertTrue(Math.abs(spec_[8][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                9) < 0.0001);
        assertTrue(Math.abs(spec_[9][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                10) < 0.0001);

        spec_ = ValidateSpectra.sortSpectra(ValidateSpectra.convert(spec),
                SpectraArrayKey.FRAGMENT_ABS_POSITION);

        assertTrue(Math.abs(spec_[0][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                1) < 0.0001);
        assertTrue(Math.abs(spec_[1][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                2) < 0.0001);
        assertTrue(Math.abs(spec_[2][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                3) < 0.0001);
        assertTrue(Math.abs(spec_[3][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                4) < 0.0001);
        assertTrue(Math.abs(spec_[4][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                5) < 0.0001);
        assertTrue(Math.abs(spec_[5][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                6) < 0.0001);
        assertTrue(Math.abs(spec_[6][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                7) < 0.0001);
        assertTrue(Math.abs(spec_[7][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                8) < 0.0001);
        assertTrue(Math.abs(spec_[8][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                9) < 0.0001);
        assertTrue(Math.abs(spec_[9][SpectraArrayKey.FRAGMENT_ION_POSITION] -
                10) < 0.0001);
    }

    /**
     * DOCUMENT ME!
     *
     * @throws Exception DOCUMENT ME!
     */
    public void testgenerateSubSpectra() throws Exception {
        double[][] x = ValidateSpectra.convert(spectra);
        assertTrue(ValidateSpectra.sizeDown(ValidateSpectra.generateSubSpectra(
                    x, 50)).length == 1);
        assertTrue(ValidateSpectra.sizeDown(ValidateSpectra.generateSubSpectra(
                    x, 10)).length == 6);
    }

    /**
     * @version Sep 5, 2003
     * @author wohlgemuth
     * <br>
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        spectra = new String(
                "1:1 2:2 3:3 4:4 5:5 6:6 7:7 8:8 9:9 10:100 11:11 12:12 13:13 14:14 15:15");
    }

    /**
     * @version Sep 5, 2003
     * @author wohlgemuth
     * <br>
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
        spectra = null;
    }
}
