package edu.ucdavis.genomics.metabolomics.binbase.integration.standalone;

import static junit.framework.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseServices;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.node.CalculationNode;
import edu.ucdavis.genomics.metabolomics.binbase.integration.AbstractBinBaseCalculationTest;

/**
 * basically simulates the cluster including scheduler and so on
 * 
 * @author wohlgemuth
 * 
 */
public class BinBaseLocalNodeCalculationTest extends
		AbstractBinBaseCalculationTest {

	private Logger logger = Logger.getLogger(getClass());
	
	@Override
	protected void calculateScheduledExperiment() throws Exception {
		runNode();
	}

	/**
	 * starts a node in a none threaded mode
	 * @return
	 * @throws Exception
	 */
	private CalculationNode runNode() throws Exception {
		//we don't run threaded since this is not needed for this
		CalculationNode node = new CalculationNode();

		node.setName(this.getClass().getName());
		node.setSingleRun(false);
		node.run();
		return node;
	}

	@Override
	protected void calculateScheduledExperimentClass() throws Exception {
		runNode();
	}

	@Override
	protected void scheduleExport(Experiment exp) throws Exception {
		Scheduler scheduler = BinBaseServices.getScheduler();
		scheduler.scheduleExport(exp, KeyFactory.newInstance().getKey());

	}

	@Override
	protected void scheduleImport(ExperimentClass clazz) throws Exception {
		logger.info("calling scheduler to schedule clazz...");
		Scheduler scheduler = BinBaseServices.getScheduler();
		scheduler.scheduleImport(clazz, KeyFactory.newInstance().getKey());
		logger.info("class should be scheduled now...");

		assertTrue(this.getMessageQueue().isEmpty() == false);
	}

	// needs to be called...
	@Before
	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}
}