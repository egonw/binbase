package edu.ucdavis.genomics.metabolomics.binbase.algorythm.statistics;

import static org.junit.Assert.assertTrue;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.IndexedResultDataFileFactory;

/**
 * setups an index result datafile
 * 
 * @author wohlgemuth
 */
public class IndexedResultDatafileTest extends ResultDataFileTest {

	public void setUp() throws Exception {
		super.setUp();
		assertTrue(file.isIndexed());
		
	}

	protected void factory() {
		System.setProperty(IndexedResultDataFileFactory.DEFAULT_PROPERTY_NAME, IndexedResultDataFileFactory.class.getName());

	}
}
