package edu.ucdavis.genomics.metabolomics.binbase.algorythm.util.sql;

import static org.junit.Assert.*;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.BinBaseConfiguredIntegrationTest;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

public class UpdateBinBaseTest extends BinBaseConfiguredIntegrationTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testRun() throws ConfigurationException, Exception {
		UpdateBinBase binbase = new UpdateBinBase();
		binbase.run();
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		ResourceSource source = new ResourceSource("/database/query-helper-rtx5.xml");
		assertTrue(source.exist());
		return new XmlDataSet(source.getStream());
	}
}
