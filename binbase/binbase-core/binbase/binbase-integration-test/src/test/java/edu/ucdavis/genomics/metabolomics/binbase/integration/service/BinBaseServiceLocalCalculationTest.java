package edu.ucdavis.genomics.metabolomics.binbase.integration.service;

import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseServices;
import edu.ucdavis.genomics.metabolomics.binbase.StandaloneCalculator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;
import edu.ucdavis.genomics.metabolomics.binbase.integration.AbstractBinBaseCalculationTest;

/**
 * calculates locally but uses the binbase service for import export of
 * experiments
 * 
 * @author wohlgemuth
 * 
 */
public class BinBaseServiceLocalCalculationTest extends AbstractBinBaseCalculationTest {

	private Logger logger = Logger.getLogger(getClass());

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected void calculateScheduledExperiment() throws Exception {
		for (Message message : this.getMessageQueue()) {
			if (((ObjectMessage) message).getObject() instanceof Experiment) {
				Experiment exp = (Experiment) ((ObjectMessage) message).getObject();
				new StandaloneCalculator().calculateExport(exp);
			}
		}
	}

	@Override
	protected void calculateScheduledExperimentClass() throws Exception {
		for (Message message : this.getMessageQueue()) {
			if (((ObjectMessage) message).getObject() instanceof ExperimentClass) {

				ExperimentClass exp = (ExperimentClass) ((ObjectMessage) message).getObject();
				new StandaloneCalculator().calculateImport(exp);
			}
		}
	}

	@Override
	protected void scheduleExport(Experiment exp) throws Exception {
		BinBaseServices.getService().triggerExportSecure(exp, KeyFactory.newInstance().getKey());
		while (this.getMessageQueue().isEmpty()) {
			logger.info("waiting for the scheduler to schedule the message");
			Thread.sleep(1000);
		}
	}

	@Override
	protected void scheduleImport(ExperimentClass clazz) throws Exception {
		BinBaseServices.getService().triggerImportClassSecure(clazz, KeyFactory.newInstance().getKey());
		while (this.getMessageQueue().isEmpty()) {
			logger.info("waiting for the scheduler to schedule the message");
			Thread.sleep(1000);
		}

	}
}
