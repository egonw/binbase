package edu.ucdavis.genomics.metabolomics.binbase.integration.cluster;

import java.io.File;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.UpdateBinBaseJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.RocksClusterImplementation;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;
import edu.ucdavis.genomics.metabolomics.util.PropertySetter;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;

public class RocksBinBaseUpdateDatabaseCalculationTest extends BinBaseUpdateDatabaseCalculationTest {

	protected static ClusterUtil UTIL = null;

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		UTIL.destroy();
	}

	/**
	 * standard setup of the cluster util
	 * 
	 * @throws Exception
	 */
	public static void initializeUtil() throws Exception {

		// development machine
		if (System.getProperty("test.binbase.cluster.application-server") != null) {
			System.out.println("using defined application server: " + System.getProperty("test.binbase.cluster.application-server"));
			UTIL.initializeCluster(System.getProperty("test.binbase.cluster.application-server"));
		}
		// test machine modus
		else {
			System.out.println("using cluster frontend as application server: " + System.getProperty("test.binbase.cluster.server"));
			UTIL.initializeCluster(System.getProperty("test.binbase.cluster.server"));
		}
	}

	/**
	 * needed to tell the cluster util the correct implementation
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void initializeCluster() throws Exception {
		Logger logger = Logger.getLogger(RocksBinBaseUpdateDatabaseCalculationTest.class);
		PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");
		UTIL = new RocksClusterImplementation(System.getProperty("test.binbase.cluster.server"), System.getProperty("test.binbase.cluster.username"), System
				.getProperty("test.binbase.cluster.password"));

		// initialize the util
		initializeUtil();

		// upload libraries needed for test
		File dir = new File("target/binbase-integration-test.dir/");

		for (File lib : dir.listFiles()) {
			logger.info("uploading libary: " + lib.getName());
			UTIL.uploadLibrary(lib.getName(), new FileSource(lib));
		}
		// ready to play
	}

	@Override
	protected void calculate() throws Exception {
		getLogger().info(UTIL.startNode());

		// wait till the scheduling is done
		while (this.getMessageQueue().isEmpty()) {
			getLogger().info("waiting for the scheduler to schedule the message");
			Thread.sleep(1000);
		}
		getLogger().info("node is started and received a message");
		while(UTIL.getPendingQueue().size() > 0){
			getLogger().info("waiting for job to start");
			Thread.sleep(2000);
		}
		while(UTIL.getUsedSlotCount() > 0){
			getLogger().info("waiting for job to finish");
			Thread.sleep(2000);
		}
		getLogger().info("job is finished");
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected void scheduleUpdate(UpdateBinBaseJob job) throws Exception {
		Scheduler.schedule(job);
	}

}
