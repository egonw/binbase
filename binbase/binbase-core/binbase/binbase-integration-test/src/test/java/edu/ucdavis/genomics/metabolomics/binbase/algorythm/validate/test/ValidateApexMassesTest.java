/*
 * Created on Sep 5, 2003
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.test;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.ValidateApexMasses;

import junit.framework.TestCase;

import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;


/**
 * @author wohlgemuth
 * @version Sep 5, 2003
 * <br>
 * BinBaseDatabase
 * @description
 */
public class ValidateApexMassesTest extends TestCase {
    /**
     * Constructor for ValidateApexMassesTest.
     * @param arg0
     */
    public ValidateApexMassesTest(String arg0) {
        super(arg0);
    }

    /**
     * DOCUMENT ME!
     */
    public static final void testContact() {
        String apex_a = "1+2+3+4+5";
        String apex_b = "6+7+8+9+10";

        String contact = ValidateApexMasses.contact(apex_a, apex_b);

        StringTokenizer tokenizer = new StringTokenizer(contact, "+");

        assertTrue(tokenizer.countTokens() == 10);

        assertTrue("6".equals(tokenizer.nextToken()));
        assertTrue("7".equals(tokenizer.nextToken()));
        assertTrue("8".equals(tokenizer.nextToken()));
        assertTrue("9".equals(tokenizer.nextToken()));
        assertTrue("10".equals(tokenizer.nextToken()));

        assertTrue("1".equals(tokenizer.nextToken()));
        assertTrue("2".equals(tokenizer.nextToken()));
        assertTrue("3".equals(tokenizer.nextToken()));
        assertTrue("4".equals(tokenizer.nextToken()));
        assertTrue("5".equals(tokenizer.nextToken()));
    }

    /**
     * DOCUMENT ME!
     */
    public static final void testContains() {
        String apex_a = "1+2+3+4+5";

        assertTrue(ValidateApexMasses.contains(apex_a, 5) == true);
        assertTrue(ValidateApexMasses.contains(apex_a, 6) == false);
    }

    /*
     * Test for String convert(Collection)
     */
    public static final void testConvertCollection() {
        List list = new Vector();
        list.add("1");
        list.add("2");
        list.add("3");

        assertTrue("1+2+3".equals(ValidateApexMasses.convert(list)));
    }

    /**
     * DOCUMENT ME!
     */
    public static final void testGenerate() {
        String apex = "1+2+3+4+5+6+7+8+9+10";
        String unique = "11";

        Collection generate = ValidateApexMasses.generate(apex, unique);

        assertTrue(generate.contains(unique));
    }

    /**
     * DOCUMENT ME!
     */
    public static final void testRemove() {
        String apex_a = "1+2+3+4+5";
        int remove = 5;

        apex_a = ValidateApexMasses.remove(apex_a, remove);

        assertTrue(apex_a.indexOf('5') == -1);
    }

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
