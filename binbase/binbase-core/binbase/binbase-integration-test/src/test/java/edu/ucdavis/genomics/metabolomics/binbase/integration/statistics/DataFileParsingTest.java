package edu.ucdavis.genomics.metabolomics.binbase.integration.statistics;

import static org.junit.Assert.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.xml.sax.SAXException;

import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.AbstractXMLTransformHandler;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.Transformator;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.NullObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.RefrenceObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.SampleObject;

/**
 * a test to see if the parsing creates the exspected result with the binbase
 * datafiles
 * 
 * @author wohlgemuth
 */
public class DataFileParsingTest {

	boolean failed = false;

	@Test
	public void testParsing() throws IOException, ParserConfigurationException, SAXException {
		final Logger logger = Logger.getLogger(getClass());

		AbstractXMLTransformHandler handler = new AbstractXMLTransformHandler() {

			@Override
			public void writeLine(List<FormatObject<?>> line) {
				try {
					logger.info("got data");
					for (FormatObject<?> o : line) {
						if (o != null) {
							logger.info("class: " + o.getClass().getName());
							logger.info("value: " + o.getValue());
							logger.info("attributes: " + o.getAttributes());

							if (o instanceof ContentObject) {
								assertTrue(o.getAttributes().isEmpty() == false);
							}
							if (o instanceof NullObject) {
								assertTrue(o.getAttributes().isEmpty() == false);
							}
							if (o instanceof SampleObject) {
								assertTrue(o.getAttributes().isEmpty() == false);
							}

							logger.info("");
						}
					}
				}
				catch (Exception e) {
					logger.error(e.getMessage(), e);
					failed = true;
				}
			}

			public String getDescription() {
				return "just a test class to validate the parsing functionality";
			}

		};

		// properties for our file
		handler.setKey(Transformator.HEIGHT);
		handler.addHeader("retention_index");
		handler.addHeader("quantmass");
		handler.addHeader("id");

		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(false);

		Source rawdata = new ResourceSource("/datafile/simple.xml");

		InputStream stream = rawdata.getStream();
		SAXParser builder = factory.newSAXParser();

		logger.debug("parse document into handler");
		builder.parse(stream, handler);

		assertTrue(failed == false);
	}
}
