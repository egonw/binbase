package edu.ucdavis.genomics.metabolomics.binbase.algorythm.statistics;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFileFactory;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.SampleTimeResolver;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.StaticStatisticActions;
import edu.ucdavis.genomics.metabolomics.binbase.bci.BinBaseConfiguredApplicationServerIntegrationTest;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.SampleObject;

public class ResultDataFileTest extends BinBaseConfiguredApplicationServerIntegrationTest {

	protected ResultDataFile file;

	private Logger logger = Logger.getLogger(getClass());

	@Before
	public void setUp() throws Exception {
		super.setUp();
		super.addTestImportDirectory(new File("src/test/resources/sampleExperiment/createBins"));
		Source sop = new ResourceSource("/sop/simple.stat");
		Source rawdata = new ResourceSource("/datafile/simple.xml");

		Document sopDefinition = new SAXBuilder().build(sop.getStream());
		Element root = sopDefinition.getRootElement();

		factory();
		file = new StaticStatisticActions().readFile("no id", rawdata, root.getChild("transform"));

		logger.info("setting the resolver");
		file.setResolver(new SampleTimeResolver() {

			@Override
			public long resolveTime(String sample) throws BinBaseException {
				return 5000;
			}
		});
	}

	protected void factory(){
		System.setProperty(ResultDataFileFactory.DEFAULT_PROPERTY_NAME,ResultDataFileFactory.class.getName());

	}
	
	@After
	public void tearDown() throws Exception {
		super.tearDown();
		file = null;
		System.gc();
	}

	@Test
	public void testGetBin() {
		HeaderFormat<String> bin = file.getBin(14328);
		assertTrue(bin.getValue().equals("C16 IST"));
	}

	@Test
	public void testGetBinsForGroup() {
		List<Integer> list = file.getBinsForGroup(82924);
		assertTrue(list.size() == 1);
		HeaderFormat<String> bin = file.getBin(list.get(0));
		assertTrue(bin.getValue().equals("C16 IST"));

	}

	@Test
	public void testRemoveBin() {
		HeaderFormat<String> bin = file.getBin(14328);
		assertTrue(bin.getValue().equals("C16 IST"));
		assertTrue(file.getBins().size() == 13);
		file.removeBin(bin);
		bin = file.getBin(14328);
		assertTrue(bin == null);
		assertTrue(file.getBins().size() == 12);
	}

	@Test
	public void testRemoveSample() {
		SampleObject<String> sample = file.getSample("4125ea02_2");
		assertTrue(sample.getValue().equals("4125ea02_2"));
		assertTrue(file.getSamples().isEmpty() == false);

		int sizeBefore = file.getSamples().size();
		file.removeSample(sample);
		assertTrue(file.getSamples().size() == sizeBefore - 1);
		sample = file.getSample("4125ea02_2");
		assertTrue(sample == null);
	}

	@Test
	public void testGetBinPosition() {
		assertTrue(file.getBinPosition(14328) == 10);
	}

	@Test
	public void testGetSampleInt() {
		SampleObject<String> sample = file.getSample(227849);
		assertTrue(sample.getValue().equals("4125ea02_2"));
	}

	@Test
	public void testGetMassspecsForBin() {
		List<ContentObject<Double>> list = file.getMassspecsForBin(14328);

		assertTrue(list.size() == 3);

		list = file.getMassspecsForBin(15538);

		assertTrue(list.size() == 0);
	}

	@Test
	public void testGetMassspecsForBinGroup() {
		List<ContentObject<Double>> list = file.getMassspecsForBinGroup(82924);
		assertTrue(list.size() == 3);
	}

	@Test
	public void testGetMassspecsForSample() {
		List<ContentObject<Double>> list = file.getMassspecsForSample("4125ea02_2");
		assertTrue(list.size() == 12);
	}

	@Test
	public void testGetStandardForSample() {
		List<ContentObject<Double>> list = file.getStandardForSample("4125ea02_2");
		assertTrue(list.size() == 12);
	}

	@Test
	public void testGetDefinedStandards() {
		List<HeaderFormat<String>> list = file.getDefinedStandards();
		assertTrue(list.size() == file.getBins().size());
	}

	@Test
	public void testGetBins() {
		assertTrue(file.getBins().size() == 13);
		assertFalse(file.getBins().size() == 12);

	}

	@Test
	public void testGetSamples() {
		assertTrue(file.getSamples().size() == 3);
		assertFalse(file.getSamples().size() == 0);
	}

	@Test
	public void testFilterBins() {
		List<String> toKeep = new Vector<String>();
		toKeep.add("C.*");

		assertTrue(file.getBins().size() == 13);

		// standards are never filtered, but instead added to the ignore column!
		file.filterBins(toKeep);
		logger.info(file.getBins(true).size());
		assertTrue(file.getBins(true).size() == 13);
		assertTrue(file.getBins(false).size() == 0);

	}

	@Test
	public void testFilterSamples() {
		List<String> toKeep = new Vector<String>();
		toKeep.add(".*ea.*");
		file.filterSamples(toKeep);
		assertTrue(file.getSamples().size() == 3);
		toKeep = new Vector<String>();
		file.filterSamples(toKeep);
		assertTrue(file.getSamples().size() == 0);
	}

	@Test
	public void testGetAverageRetentionTimeForBinInt() {
		assertFalse(Double.isInfinite(file.getAverageRetentionTimeForBin(14328)));
		assertTrue(Double.isInfinite(file.getAverageRetentionTimeForBin(15538)));
	}

	@Test
	public void testGetAverageRetentionTimeForBinIntString() throws BinBaseException {
		logger.info(file.getAverageRetentionTimeForBin(14328, "4125ea02_2"));
		assertFalse(Double.isInfinite(file.getAverageRetentionTimeForBin(14328, "4125ea02_2")));
		assertTrue(Double.isInfinite(file.getAverageRetentionTimeForBin(15538, "4125ea02_2")));

	}

	@Test
	public void testGetClazz() {
		List rows = file.getClazz("1");
		assertTrue(rows.size() == 2);

		rows = file.getClazz("2");
		assertTrue(rows.size() == 1);

		rows = file.getClazz("0");
		assertTrue(rows.size() == 0);

	}

	@Test
	public void testGetClazzCount() {
		int count = file.getClazzCount();
		logger.info("clazz count: " + count);
		assertTrue(count == 2);

	}

	@Test
	public void testGetClazzNames() {
		List classes = file.getClazzNames();
		assertTrue(classes.contains("1"));
		assertTrue(classes.contains("2"));
		assertTrue(classes.size() == 2);

	}

}
