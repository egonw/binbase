package edu.ucdavis.genomics.metabolomics.binbase.integration.standalone;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.SimpleStatisticProcessor;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.io.ResultDestination;
import edu.ucdavis.genomics.metabolomics.binbase.bci.io.SopSource;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;

/**
 * tests if our statistic processor works as planned
 * @author wohlgemuth
 *
 */
public class SimpleStatisticProcessorTest extends AbstractApplicationServerTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
		logger.info("adding directories with content to the applciation server...");

		logger.info("define default sop");
		Configurator.getExportService().setDefaultSop("simple.stat");
		Configurator.getExportService().addSopDir(new File("src/test/resources/sop").getAbsolutePath());
		assertTrue(Configurator.getExportService().listSops().contains("simple.stat"));

		logger.info("set result directory");
		
		File dir = new File("target/content/result");
		dir.mkdirs();
		Configurator.getExportService().setResultDirectory(dir.getAbsolutePath());
		logger.info("result dir is: " + dir.getAbsolutePath());
		logger.info("start with calculation");
	}
	
	/**
	 * test how the processor works
	 * @throws ConfigurationException
	 */
	@Test
	public void testStatisticProccessor() throws ConfigurationException{
		Source sop = new SopSource("simple.stat");
		SimpleStatisticProcessor pro = new SimpleStatisticProcessor();
		pro.process(getRawdata(), sop, getDestination("simple.zip"));
	}
	
	/**
	 * provides us with data
	 * @return
	 */
	protected Source getRawdata(){
		return new ResourceSource("/datafile/simple.xml");
	}
	
	/**
	 * gives us the final destination
	 * @param name
	 * @return
	 */
	protected Destination getDestination(String name){
		return new ResultDestination(name);
	}
}
