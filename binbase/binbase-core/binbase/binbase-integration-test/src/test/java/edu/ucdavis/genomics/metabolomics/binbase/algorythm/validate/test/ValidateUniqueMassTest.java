/*
 * Created on Sep 5, 2003
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.test;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.ValidateUniqueMass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.BinBaseConfiguredApplicationServerIntegrationTest;
import edu.ucdavis.genomics.metabolomics.binbase.bci.io.ConfigSource;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;


/**
 * @author wohlgemuth
 * @version Sep 5, 2003
 * <br>
 * BinBaseDatabase
 * @description
 */
public class ValidateUniqueMassTest extends BinBaseConfiguredApplicationServerIntegrationTest {

    /**
     * DOCUMENT ME!
     */
	@Test
    public  final void testIsInValidUnique() {
        String spectra = "1:1 2:2 3:3 4:4 5:5 6:6 7:7 8:8 9:9 10:100 11:11 12:12 13:13 14:14 15:15";
        assertTrue(ValidateUniqueMass.isValidUnique(spectra, 1) == false);
    }

    /**
     * DOCUMENT ME!
     */
    public  final void testIsValidUnique() {
        String spectra = "1:1 2:2 3:3 4:4 5:5 6:6 7:7 8:8 9:9 10:100 11:11 12:12 13:13 14:14 15:15";

        assertTrue(ValidateUniqueMass.isValidUnique(spectra, 10) == true);
    }

    /**
     * @version Sep 5, 2003
     * @author wohlgemuth
     * <br>
     * @see junit.framework.TestCase#setUp()
     */
    public void setUp() throws Exception {
        super.setUp();
		super.addConfig(getClass().getResourceAsStream("/config/binbase.xml"), "binbase.xml");

		super.addConfig(getClass().getResourceAsStream("/config/sql.xml"), "sql.xml");
		super.addConfigFile(new File("src/test/resources/config/targets.xml"));
    }

    /**
     * @version Sep 5, 2003
     * @author wohlgemuth
     * <br>
     * @see junit.framework.TestCase#tearDown()
     */
    public void tearDown() throws Exception {
        super.tearDown();
    }

}
