/*
 * Created on 05.06.2003
 *
 * To change the template for this generated file go to Window>Preferences>Java>Code Generation>Code and Comments
 */
package edu.ucdavis.genomics.metabolomics.util;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.Statement;


/**
 * @author wohlgemuth
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public abstract class SQLObject extends BasicObject implements SQLable {
	private Connection connection;
	
    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.transform.abstracthandler.algorythm.binlib.SQLable#setConnection(java.sql.Connection)
     */
    public final void setConnection(Connection connection) {
        try {
        	this.connection = connection;
            this.prepareStatements();
            this.prepareVariables();
        } catch (Exception ex) {
            sendException(ex);
        }
    }

	

	/**
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.transform.abstracthandler.algorythm.binlib.SQLable#getConnection()
     */
    public final Connection getConnection() {
        return this.connection;
    }

    /**
     *  close all open statements
     */
    protected void closeStatements() {
        try {
            Class<?> current = this.getClass();

            Field[] f = current.getDeclaredFields();

            for (int i = 0; i < f.length; i++) {
                f[i].setAccessible(true);

                Class<?> type = f[i].getType();

                if (type.getName().matches("java.sql.Statement") ||
                        "java.sql.PreparedStatement".equals(type.getName())) {
                    Object o = f[i].get(this);

                    if (o != null) {
                        Statement stm = (Statement) o;
                        stm.close();
                        stm = null;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("erroro during closing statements...", e);
        }
    }

    /**
     * finds all statements of this object and close these is slow but better
     * than an exception...
     *
     * @see java.lang.Object#finalize()
     */
    protected void finalize() throws Throwable {
        closeStatements();

        super.finalize();
    }

    /**
     * bereitet die statements vor
     *
     * @throws Exception
     */
    protected void prepareStatements() throws Exception {
    }

    /**
     * setzt werte f?r variablen
     */
    protected void prepareVariables() throws Exception {
    }

}
