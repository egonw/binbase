package edu.ucdavis.genomics.metabolomics.util.collection;

import java.io.PrintStream;
import java.io.PrintWriter;

public class CompressionException extends RuntimeException {
	private Exception e;
	/**
	 * 
	 */
	private static final long serialVersionUID = 544405122017848900L;

	public CompressionException(Exception e){
		this.e = e;
	}

	public boolean equals(Object obj) {
		return e.equals(obj);
	}

	public Throwable fillInStackTrace() {
		return e.fillInStackTrace();
	}

	public Throwable getCause() {
		return e.getCause();
	}

	public String getLocalizedMessage() {
		return e.getLocalizedMessage();
	}

	public String getMessage() {
		return e.getMessage();
	}

	public StackTraceElement[] getStackTrace() {
		return e.getStackTrace();
	}

	public int hashCode() {
		return e.hashCode();
	}

	public Throwable initCause(Throwable cause) {
		return e.initCause(cause);
	}

	public void printStackTrace() {
		e.printStackTrace();
	}

	public void printStackTrace(PrintStream s) {
		e.printStackTrace(s);
	}

	public void printStackTrace(PrintWriter s) {
		e.printStackTrace(s);
	}

	public void setStackTrace(StackTraceElement[] stackTrace) {
		e.setStackTrace(stackTrace);
	}

	public String toString() {
		return e.toString();
	}
}
