/*
 * Created on 01.07.2005
 */
package edu.ucdavis.genomics.metabolomics.util.xml.xslt;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


/**
 * @author wohlgemuth
 * is used to transform xml files into other files...
 */
public class XSLTTransform {
    /**
     *
     * @param in
     *            what we want to transform
     * @param out
     *            where the result should go
     * @param xslt
     *            how we want to transform
     * @throws TransformerException
     */
    public static void transform(InputStream in, OutputStream out,
        InputStream xslt) throws TransformerException {
        TransformerFactory transFact = TransformerFactory.newInstance();
        Source xsltSource = new StreamSource(xslt);
        Transformer transformer = transFact.newTransformer(xsltSource);
        Source input = new StreamSource(in);
        Result output = new StreamResult(out);
        transformer.transform(input, output);
    }
}
