/*
 * Created on 28.07.2004
 */
package edu.ucdavis.genomics.metabolomics.util.statistics.replacement;

import java.util.List;


/**
 * @author wohlgemuth
 *
 */
public class NoReplacement implements ZeroReplaceable {
    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.utils.statistics.replacement.ZeroReplaceable#replaceZeros(java.util.List)
     */
    public List replaceZeros(List list) {
        return list;
    }
}
