package edu.ucdavis.genomics.metabolomics.util.database.statements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;

/**
 * provides a central access to sql statements loaded from sources
 * the sources need to be xml files with no double entries
 * 
 * you also can just load properties files with the related methods
 * @author nase
 * 
 */
public final class StatementRepository {

	/**
	 * the singleton
	 */
	private static StatementRepository instance = null;

	/**
	 * our internal statement cache
	 */
	private Map<String, String> cache = new HashMap<String, String>();

	/**
	 * contains all registered hashcodes of sources used
	 */
	private Collection<Integer> registeredSource = new ArrayList<Integer>();

	private Logger logger = Logger.getLogger(getClass());

	protected StatementRepository(){
		
	}
	/**
	 * provides access to the instance
	 * 
	 * @return
	 */
	public static StatementRepository getInstance() {
		if (instance == null) {
			instance = new StatementRepository();
		}
		return instance;
	}

	/**
	 * returns a cached statement or null
	 * 
	 * @param key
	 * @return
	 */
	public String getStatement(String key) {
		return cache.get(key);
	}

	/**
	 * adds a source based on xml content
	 * 
	 * @param source
	 * @throws LoadingException
	 * @throws IOException
	 */
	public void addSource(Source source) throws LoadingException, IOException {
		logger.debug("trying to load source: " + source.getSourceName());
		if (registeredSource.contains(source.hashCode())) {
			logger.warn("source already loaded, refreshing content: "
					+ source.getSourceName());
		}
		
		registeredSource.add(source.hashCode());
		
		try {
			generateQueries(XmlHandling.readXml(source.getStream()));
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			throw new LoadingException(e);
		}
	}


	/**
	 * adds a source based on a properties file content
	 * 
	 * @param source
	 * @throws IOException 
	 * @throws LoadingException
	 * @throws IOException
	 */
	public void addPropertySource(Source source) throws IOException{
		logger.debug("trying to load source: " + source.getSourceName());
		if (registeredSource.contains(source.hashCode())) {
			logger.warn("source already loaded, refreshing content: "
					+ source.getSourceName());
		}
		
		registeredSource.add(source.hashCode());
		
		Properties p = new Properties();
		p.load(source.getStream());
		
		Enumeration<Object> e = p.keys();
		
		while(e.hasMoreElements()){
			String key = e.nextElement().toString();
			logger.debug("adding path for statement: " + key + " - " + p.getProperty(key));
			this.cache.put(key, p.getProperty(key));
		}
		
	}
	/**
	 * generate the internal queries
	 * 
	 * @param e
	 */
	@SuppressWarnings("unchecked")
	private void generateQueries(Element e) {
		logger.debug("working on element: " + e.getName());
		// generate path to element
		if (e.getText().length() > 0) {
			logger.debug("current element has content");
			String path = calculatePathToRoot(e);

			logger.debug("add to cache: " + path + " - " + e.getText());
			// store this element
			this.cache.put(path, e.getText().trim());
		}
		// work on sub elements
		List<Element> list = e.getChildren();

		for (Element next : list) {
			generateQueries(next);
		}
	}

	/**
	 * calculates the path to the root element
	 * 
	 * @param e
	 * @return
	 */
	protected String calculatePathToRoot(Element e) {
		return calculatePathToRoot(e, "");
	}

	
	/**
	 * returns the names of all statements
	 * @return
	 */
	public Set<String> getStatementNames(){
		return this.cache.keySet();
	}
	/**
	 * walks throw the path
	 * 
	 * @param e
	 * @param path
	 * @return
	 */
	private String calculatePathToRoot(Element e, String path) {
		logger.debug("currently: " + e.getName() + " - " + path);
		if (e.getParentElement() == null) {
			if (path.length() == 0) {
				return e.getName();
			} else {
				return e.getName() + "." + path;
			}
		} else {
			if (path.length() == 0) {
				path = e.getName();
			} else {
				path = e.getName() + "." + path;
			}
			return calculatePathToRoot(e.getParentElement(), path);
		}
	}
}
