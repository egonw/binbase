package edu.ucdavis.genomics.metabolomics.util.statistics.data;

import edu.ucdavis.genomics.metabolomics.util.statistics.replacement.ZeroReplaceable;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * extends the simple datafile by the possibility of doing replacement actaions
 * in threads
 *
 * @author wohlgemuth
 */
public class ThreadedDataFile extends SimpleDatafile {

    private int loadFactor = 1;
    private final int cpuCount = Runtime.getRuntime().availableProcessors();

    public ThreadedDataFile() {
        this.setData(Collections.synchronizedList(this.getData()));
    }

    protected int getMaxThreads() {
        return loadFactor * cpuCount;
    }

    public int getLoadFactor() {
        return loadFactor;
    }

    public void setLoadFactor(final int loadFactor) {
        this.loadFactor = loadFactor;
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected void replaceColumnBased(final ZeroReplaceable replace) {
        super.replaceColumnBased(replace);
    }

    @Override
    protected void replaceRowBased(final ZeroReplaceable replace) {

        if (cpuCount > 1) {
            getLogger().info(
                    "running in threaded mode with " + cpuCount + " cpus");
            replaceRowBasedMultithreaded(replace);
        } else {
            getLogger().info(
                    "fallback to original method, only one CPU accessible!");
            super.replaceRowBased(replace);
        }

    }

    /**
     * provides us wit an executor service
     *
     * @param maxThreads
     * @return
     */
    protected ExecutorService createService(int maxThreads) {
        return Executors.newFixedThreadPool(maxThreads);
    }

    protected void replaceRowBasedMultithreaded(final ZeroReplaceable replace) {

        int maxThreads = getMaxThreads();
        getLogger().debug("limit too threads: " + maxThreads);

        final ExecutorService service = createService(maxThreads);

        getLogger().info(
                "loading all rows into the queue, count of rows: "
                        + getTotalRowCount());
        for (int i = 0; i < getTotalRowCount(); i++) {

            final int row = i;
            final boolean ignore = ignoreRow(row);

            // ok row was accepted
            if (ignore == false) {

                // does the actual calculation
                final Runnable run = new Runnable() {

                    @SuppressWarnings("unchecked")
                    public void run() {

                        Thread.currentThread().setName("row=" + row);
                        getLogger().info(
                                "starting replacement of row: " + row);

                        getLogger().debug("replacing line: " + row);
                        // get row
                        final List toReplace = generateListOfValuesForReplacement(
                                row, getRow(row));

                        // replace the zeros
                        final List result = replace.replaceZeros(toReplace);

                        writeBackToRow(row, result);
                        getLogger().info(
                                "finished replacement of row: " + row);

                    }
                };

                // add it to the service
                service.execute(run);
            } else {
                getLogger().debug(
                        "row was set as ignored so we do it: " + row);
            }
        }

        service.shutdown();

        try {
            service.awaitTermination(31, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            getLogger().error(e.getMessage(), e);
        }

    }

    private Semaphore semaphore = new Semaphore(1);

    @Override
    protected void writeBackToColumn(int i, List result) {
        try {
            semaphore.acquire();
            super.writeBackToColumn(i, result);
            semaphore.release();
        } catch (InterruptedException e) {
            getLogger().warn(e.getMessage(), e);
        }
    }

    @Override
    protected void writeBackToRow(int i, List result) {
        try {
            semaphore.acquire();
            super.writeBackToRow(i, result);
            semaphore.release();
        } catch (InterruptedException e) {
            getLogger().warn(e.getMessage(), e);
        }
    }
}
