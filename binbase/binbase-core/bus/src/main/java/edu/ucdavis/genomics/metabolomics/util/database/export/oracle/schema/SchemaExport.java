/*
 * Created on May 6, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.database.export.oracle.schema;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * @author wohlgemuth generates from an oracle database the schema file and
 *         write it to a file
 */
public class SchemaExport {
    /**
     * is the database casesensitive
     */
    boolean caseSensitive = true;

    /**
     * DOCUMENT ME!
     */
    private Connection con;

    /**
     * DOCUMENT ME!
     */
    private File outputFile;

    /**
     * DOCUMENT ME!
     */
    private String database;

    /**
     * DOCUMENT ME!
     */
    private String host;

    /**
     * DOCUMENT ME!
     */
    private String password;

    /**
     * DOCUMENT ME!
     */
    private String port;

    /**
     * DOCUMENT ME!
     */
    private String user;

    /**
     * @param user
     * @param password
     * @param database
     * @param host
     * @param port
     * @param outputFile
     */
    public SchemaExport(String user, String password, String database,
        String host, String port, File outputFile) {
        super();
        this.user = user;
        this.password = password;
        this.database = database;
        this.host = host;
        this.port = port;
        this.outputFile = outputFile;
    }

    /**
     * arg[0] = username arg[1] = password arg[2] = database arg[3] = host
     * arg[4] = port arg[5] = outputFile
     *
     *
     * @param args
     */
    public static void main(String[] args) {
        new SchemaExport(args[0], args[1], args[2], args[3], args[4],
            new File(args[5])).generate();
    }

    /**
     * DOCUMENT ME!
     *
     * @throws SQLException DOCUMENT ME!
     * @throws ClassNotFoundException DOCUMENT ME!
     */
    public void connect() throws SQLException, ClassNotFoundException {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        con = DriverManager.getConnection("jdbc:oracle:thin:@" + host + ":" +
                port + ":" + database, user, password);
    }

    /**
     * generate the schema itself
     *
     */
    public void generate() {
        try {
            connect();

            BufferedWriter writer = new BufferedWriter(new FileWriter(
                        outputFile));

            generateSequences(writer, con);
            generateTables(writer, con);
            generateViews(writer, con);
            generateIndexes(writer, con);
            generateTriggers(writer, con);
            generateProcedures(writer, con);

            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param writer
     * @param connection @
     * @throws SQLException
     * @throws IOException
     */
    protected void generateIndexes(Writer writer, Connection connection)
        throws SQLException, IOException {
        writer.write("-- create indexes\n");

        Statement tablenames = connection.createStatement();

        // get table name
        ResultSet names = tablenames.executeQuery(
                "select TABLE_NAME from USER_TABLES");

        while (names.next()) {
            String name = names.getString(1);

            if (name.indexOf("$$") == -1) {
                boolean next = true;

                // get index name
                Statement index = con.createStatement();
                ResultSet indexName = index.executeQuery(
                        "select CONSTRAINT_NAME from USER_CONSTRAINTS where TABLE_NAME= '" +
                        name + "'");

                while (indexName.next()) {
                    if (next) {
                        writer.write("\n-- indexes for table " + name + "\n\n");
                        next = false;
                    }

                    Statement indexData = con.createStatement();
                    String indexN = indexName.getString(1);

                    // get index properties
                    ResultSet indexColumn = indexData.executeQuery(
                            "select UCC.COLUMN_NAME, UC.CONSTRAINT_TYPE, UC.SEARCH_CONDITION, UC2.TABLE_NAME as REFERENCES_TABLE from USER_CONS_COLUMNS UCC, USER_CONSTRAINTS UC, USER_CONSTRAINTS UC2 where UCC.CONSTRAINT_NAME = UC.CONSTRAINT_NAME and UC.R_CONSTRAINT_NAME = UC2.CONSTRAINT_NAME(+)  and UCC.CONSTRAINT_NAME = '" +
                            indexN + "' order by UCC.CONSTRAINT_NAME");

                    if (caseSensitive) {
                        writer.write("ALTER TABLE \"" + name + "\" ADD ( ");
                    } else {
                        writer.write("ALTER TABLE " + name + " ADD ( ");
                    }

                    boolean first = true;

                    String indexTyp = "";
                    String searchCondition = "";
                    String referenceTable = "";

                    while (indexColumn.next()) {
                        String columnName = indexColumn.getString(1);
                        indexTyp = indexColumn.getString(2);
                        searchCondition = indexColumn.getString(3);
                        referenceTable = indexColumn.getString(4);

                        if ("C".equals(indexTyp.toUpperCase())) {
                            writer.write(" CHECK ( " + searchCondition.trim() +
                                ")");
                        } else {
                            if (first) {
                                if ("P".equals(indexTyp.toUpperCase())) {
                                    writer.write(" PRIMARY KEY (");
                                } else if ("U".equals(indexTyp.toUpperCase())) {
                                    writer.write(" UNIQUE (");
                                } else if ("R".equals(indexTyp.toUpperCase())) {
                                    writer.write(" FOREIGN KEY (");
                                }

                                first = false;

                                if (caseSensitive) {
                                    writer.write("\"" + columnName + "\"");
                                } else {
                                    writer.write(columnName);
                                }
                            } else {
                                if (caseSensitive) {
                                    writer.write(",\"" + columnName + "\"");
                                } else {
                                    writer.write("," + columnName);
                                }
                            }
                        }
                    }

                    if ("C".equals(indexTyp.toUpperCase()) == false) {
                        writer.write(")");
                    }

                    if ("R".equals(indexTyp.toUpperCase())) {
                        if (caseSensitive) {
                            writer.write(" REFERENCES \"" + referenceTable +
                                "\"");
                        } else {
                            writer.write(" REFERENCES " + referenceTable);
                        }
                    }

                    writer.write(" VALIDATE ");
                    writer.write(");\n");

                    indexData.close();
                }

                index.close();
            }
        }

        tablenames.close();
    }

    /**
     * generate all sequences from the database
     *
     * @param writer
     * @param connection
     * @throws IOException
     * @throws SQLException
     */
    protected static void generateSequences(Writer writer, Connection connection)
        throws IOException, SQLException {
        writer.write("-- sequences\n\n");

        Statement getSEQUENCES = connection.createStatement();
        ResultSet result = getSEQUENCES.executeQuery(
                "select sequence_name, min_value, max_value,increment_by,cycle_flag,order_flag,cache_size from user_sequences");

        while (result.next()) {
            writer.write("CREATE SEQUENCE " + result.getString(1) + " ");
            writer.write("INCREMENT BY " + result.getString(4) + " ");
            writer.write("start with " + result.getString(2) + " ");
            writer.write("maxvalue " + result.getString(3) + " ");

            if ("Y".equals(result.getString(5).toUpperCase())) {
                writer.write("CYCLE ");
            } else {
                writer.write("NOCYCLE ");
            }

            if ("Y".equals(result.getString(6).toUpperCase())) {
                writer.write("ORDER ");
            } else {
                writer.write("NORDER ");
            }

            writer.write("CACHE_SIZE " + result.getString(7) + " ");
            writer.write(";\n");
        }

        result.close();
        getSEQUENCES.close();
        writer.write("\n");
    }

    /**
     * DOCUMENT ME!
     *
     * @param writer DOCUMENT ME!
     * @param connection DOCUMENT ME!
     */
    protected void generateProcedures(Writer writer, Connection connection) {
        // TODO
    }

    /**
     * generates all tables
     *
     * @param writer
     * @param connection
     * @throws SQLException
     * @throws IOException
     */
    protected void generateTables(Writer writer, Connection connection)
        throws SQLException, IOException {
        writer.write("-- tables\n\n");

        Statement tablenames = connection.createStatement();
        ResultSet names = tablenames.executeQuery(
                "SELECT TABLE_NAME FROM user_tables");

        while (names.next()) {
            String name = names.getString(1);

            if (name.indexOf("$$") == -1) {
                if (caseSensitive) {
                    writer.write("create table \"" + name + "\"(\n");
                } else {
                    writer.write("create table " + name + "(\n");
                }

                Statement column = con.createStatement();
                ResultSet columnData = column.executeQuery(
                        "select column_name,data_type,data_length,data_precision,data_scale,nullable,data_default from user_tab_columns where table_name = '" +
                        name + "'");

                boolean first = true;

                while (columnData.next()) {
                    if (first) {
                        first = false;
                    } else {
                        writer.write(",\n");
                    }

                    String columnName = columnData.getString(1);
                    String type = columnData.getString(2);
                    String length = columnData.getString(3);
                    String preciscion = columnData.getString(4);
                    String scale = columnData.getString(5);
                    String nullable = columnData.getString(6);
                    String defaut = columnData.getString(7);

                    if (caseSensitive) {
                        writer.write("\t\"" + columnName + "\" ");
                    } else {
                        writer.write("\t" + columnName + " ");
                    }

                    // generate the number datatyp
                    if ("NUMBER".equals(type.toUpperCase())) {
                        if ((scale == null) && (preciscion == null)) {
                            writer.write("number ");
                        } else if ((preciscion == null) && ("0".equals(scale))) {
                            writer.write("integer ");
                        } else {
                            int s = Integer.parseInt(scale);
                            int p = Integer.parseInt(preciscion);

                            if (s == 0) {
                                writer.write("number(" + p + ") ");
                            } else {
                                writer.write("number(" + p + "," + s + ") ");
                            }
                        }
                    } else if ("CLOB".equals(type.toUpperCase())) {
                        writer.write(type + " ");
                    } else if ("FLOAT".equals(type.toUpperCase())) {
                        if ((scale == null) && (preciscion == null)) {
                            writer.write("FLOAT ");
                        } else {
                            int p = Integer.parseInt(preciscion);
                            writer.write("FLOAT(" + p + ") ");
                        }
                    } else if ("CLOB".equals(type.toUpperCase())) {
                        writer.write(type + " ");
                    } else if ("DATE".equals(type.toUpperCase())) {
                        writer.write(type + " ");
                    } else if ("TIMESTAMP".equals(type.toUpperCase())) {
                        writer.write(type + " ");
                    } else {
                        writer.write(type + "(" + length + ") ");
                    }

                    if (defaut != null) {
                        writer.write("default " + defaut + " ");
                    }

                    if ("N".equals(nullable.toUpperCase())) {
                        writer.write("not null ");
                    }
                }

                writer.write("\n");
                writer.write(");\n");
                writer.write("\n");

                columnData.close();
                column.close();
            }
        }

        names.close();
        tablenames.close();
    }

    /**
     * DOCUMENT ME!
     *
     * @param writer DOCUMENT ME!
     * @param connection DOCUMENT ME!
     */
    protected void generateTriggers(Writer writer, Connection connection) {
        // TODO
    }

    /**
     * DOCUMENT ME!
     *
     * @param writer DOCUMENT ME!
     * @param connection DOCUMENT ME!
     *
     * @throws IOException DOCUMENT ME!
     * @throws SQLException DOCUMENT ME!
     */
    protected void generateViews(Writer writer, Connection connection)
        throws IOException, SQLException {
        writer.write("-- views\n\n");

        Statement statment = connection.createStatement();
        ResultSet result = statment.executeQuery(
                "select view_name,text from user_views");

        while (result.next()) {
            String name = result.getString(1);
            String text = result.getString(2);

            if (caseSensitive) {
                writer.write("CREATE OR REPLACE VIEW \"" + name + "\" AS " +
                    text.trim() + ";\n");
            } else {
                writer.write("CREATE OR REPLACE VIEW " + name + " AS " +
                    text.trim().replaceAll("\"", "") + ";\n");
            }

            writer.write("\n");
        }
    }
}
