/*
 * Created on 18.10.2004
 */
package edu.ucdavis.genomics.metabolomics.util.thread;

import edu.ucdavis.genomics.metabolomics.util.SQLObject;


/**
 * definiert einen thread f?r sql objekte
 * allerdings muss noch ein connection management eingefuehrt werden
 * @author wohlgemuth
 */
public abstract class SQLThread extends SQLObject implements Threadable {
    /**
     *
     * @uml.property name="listener"
     * @uml.associationEnd multiplicity="(0 1)"
     */
    ThreadListener listener;

    /**
     * DOCUMENT ME!
     */
    private String name = getClass().getName();

    /**
     * DOCUMENT ME!
     */
    private Thread thread = null;

    /**
     * Creates a new SQLThread object.
     */
    public SQLThread() {
        listener = new ThreadListener() {
                    public void setResult(Object o) {
                        // TODO Auto-generated method stub
                    }

                    public void startThread() {
                        // TODO Auto-generated method stub
                    }

                    public void threadFinshed() {
                        // TODO Auto-generated method stub
                    }
                };
    }

    /**
     *
     * @uml.property name="listener"
     */

    /*
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.thread.Threadable#setListener(edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.thread.ThreadListener)
     */
    public void setListener(ThreadListener listener) {
        this.listener = listener;
    }

    /**
     * DOCUMENT ME!
     *
     * @param thread DOCUMENT ME!
     *
     * @uml.property name="thread"
     */
    public void setThread(Thread thread) {
        this.thread = thread;
    }

    /*
     * @see java.lang.Runnable#run()
     */
    public final void run() {
        if (listener == null) {
            throw new RuntimeException("please set a listener!");
        }

        try {
            this.setConnection(getConnection());
            listener.startThread();
            listener.setResult(this.runThread());
        } catch (Exception e) {
            sendException(e);
        }

        listener.threadFinshed();
    }

    /**
     * f?hrt die berechnung als solches aus
     *
     * @return gibt irgendein ergebniss zur?ck welches in der collection
     *         gespeichert wird
     */
    public abstract Object runThread() throws Exception;

    /**
     * DOCUMENT ME!
     *
     * @param name DOCUMENT ME!
     *
     * @uml.property name="name"
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @uml.property name="name"
     */
    public final String getName() {
        return name;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @uml.property name="thread"
     */
    public final Thread getThread() {
        return thread;
    }
}
