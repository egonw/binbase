package edu.ucdavis.genomics.metabolomics.util.collection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException;

/**
 * provides a compressed implementation of a map
 * 
 * @author wohlgemuth
 * 
 */
public class CompressedMap<K, V> implements Map<K, V>,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<K, byte[]> map = null;

	/**
	 * constructor for using an internal hashmap
	 * @param attributes 
	 * 
	 * @param map
	 */
	public CompressedMap(Map<K,V> attributes) {
		this.map = new HashMap<K, byte[]>();
		
		for(K key : attributes.keySet()){
			this.put(key, attributes.get(key));
		}
	}

	public CompressedMap(){
		this.map = new HashMap<K, byte[]>();
	}
	/**
	 * compresses the object using a gzip stream
	 * 
	 * @param object
	 * @return
	 */
	private byte[] compress(V object) throws CompressionException {
		if (object == null) {
			return null;
		}

		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			GZIPOutputStream out1 = new GZIPOutputStream(out);
			ObjectOutputStream out2 = new ObjectOutputStream(out1);
			out2.writeObject(object);
			out2.flush();
			out2.close();

			byte[] result = out.toByteArray();
			
			out = null;
			out1  = null;
			out2 = null;
			
			return result;
		} catch (Exception e) {
			throw new CompressionException(e);
		}
	}

	/**
	 * uncompresses the byte using a gzip strem
	 * 
	 * @param content
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private V uncompress(byte[] content) throws CompressionException {
		if (content == null) {
			return null;
		}

		try {
			ByteArrayInputStream out = new ByteArrayInputStream(content);
			GZIPInputStream out1 = new GZIPInputStream(out);
			ObjectInputStream out2 = new ObjectInputStream(out1);
			V result = (V) out2.readObject();
			out2.close();

			out = null;
			out1  = null;
			out2 = null;
			
			return result;
		} catch (Exception e) {
			throw new CompressionException(e);
		}
	}

	public void clear() {
		map.clear();
	}

	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@SuppressWarnings("unchecked")
	public boolean containsValue(Object value) {
				Iterator<byte[]> it = this.map.values().iterator();
		
		while(it.hasNext()){
			byte[] b = it.next();
			if(value == null){
				if(b==null){
					return true;
				}
			}
			else if(b == null){
				if(value==null){
					return true;
				}
			}
			else if(uncompress(b).equals(value)){
				return true;
			}
		}
		
		return false;
	}

	public Set<Entry<K, V>> entrySet() {
		throw new NotSupportedException("sorry operation is not available for this map!");
	}

	public boolean equals(Object o) {
		return map.equals(o);
	}

	public V get(Object key) {
		return uncompress(map.get(key));
	}

	public int hashCode() {
		return map.hashCode();
	}

	public boolean isEmpty() {
		return map.isEmpty();
	}

	public Set<K> keySet() {
		return map.keySet();
	}

	public V put(K key, V value) {
		map.put(key, compress(value));
		return value;
	}

	public void putAll(Map<? extends K, ? extends V> t) {
		for(K key : t.keySet()){
			this.put(key, t.get(key));
		}
	}

	public V remove(Object key) {
		return uncompress(map.remove(key));
	}

	public int size() {
		return map.size();
	}

	public Collection<V> values() {
		Collection<V> values = new Vector<V>();
		for(K key : map.keySet()){
			values.add(uncompress(map.get(key)));
		}
		return values;
	}

}
