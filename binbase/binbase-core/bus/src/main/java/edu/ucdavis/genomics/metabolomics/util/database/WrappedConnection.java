/*
 * Created on Sep 26, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.database;

import java.sql.Array;
import org.apache.commons.dbcp.DelegatingConnection;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;
import org.postgresql.jdbc3.AbstractJdbc3Connection;

/**
 * a wrapper arround a collection to avoid that a connection is closed other
 * then by a connection factory
 * 
 * @author wohlgemuth
 * 
 */
public class WrappedConnection extends DelegatingConnection {
	/**
	 * DOCUMENT ME!
	 */
	private Connection connection;

	/**
	 * DOCUMENT ME!
	 */
	private final Logger logger = Logger.getLogger(WrappedConnection.class);

	private final ConnectionFactory factory;

	/**
	 * @param connection
	 */
	public WrappedConnection(final Connection connection, final ConnectionFactory factory) {
		super(connection);

		
		this.connection = connection;
		this.factory = factory;
	}


	/**
	 * DOCUMENT ME!
	 * 
	 * @throws SQLException
	 */
	public void close() throws SQLException {
		logger.warn(
				"try to avoid this method and use the factory method instead!",
				new Exception(""));
		factory.close(connection);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param factory
	 *            DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public void close(final boolean factory) throws SQLException {
		this.factory.close(connection);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param arg0
	 *            DOCUMENT ME!
	 * @param arg1
	 *            DOCUMENT ME!
	 * @param arg2
	 *            DOCUMENT ME!
	 * @param arg3
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public CallableStatement prepareCall(final String arg0, final int arg1, final int arg2,
			final int arg3) throws SQLException {
		logStatement(arg0);
		return connection.prepareCall(arg0, arg1, arg2, arg3);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param arg0
	 *            DOCUMENT ME!
	 * @param arg1
	 *            DOCUMENT ME!
	 * @param arg2
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public CallableStatement prepareCall(final String arg0, final int arg1, final int arg2)
			throws SQLException {
		logStatement(arg0);
		return connection.prepareCall(arg0, arg1, arg2);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param arg0
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public CallableStatement prepareCall(final String arg0) throws SQLException {
		logStatement(arg0);
		return connection.prepareCall(arg0);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param arg0
	 *            DOCUMENT ME!
	 * @param arg1
	 *            DOCUMENT ME!
	 * @param arg2
	 *            DOCUMENT ME!
	 * @param arg3
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public PreparedStatement prepareStatement(final String arg0, final int arg1, final int arg2,
			final int arg3) throws SQLException {
		logStatement(arg0);
		return connection.prepareStatement(arg0, arg1, arg2, arg3);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param arg0
	 *            DOCUMENT ME!
	 * @param arg1
	 *            DOCUMENT ME!
	 * @param arg2
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public PreparedStatement prepareStatement(final String arg0, final int arg1, final int arg2)
			throws SQLException {
		logStatement(arg0);
		return connection.prepareStatement(arg0, arg1, arg2);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param arg0
	 *            DOCUMENT ME!
	 * @param arg1
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public PreparedStatement prepareStatement(final String arg0, final int arg1)
			throws SQLException {
		logStatement(arg0);
		return connection.prepareStatement(arg0, arg1);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param arg0
	 *            DOCUMENT ME!
	 * @param arg1
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public PreparedStatement prepareStatement(final String arg0, final int[] arg1)
			throws SQLException {
		logStatement(arg0);
		return connection.prepareStatement(arg0, arg1);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param arg0
	 *            DOCUMENT ME!
	 * @param arg1
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public PreparedStatement prepareStatement(final String arg0, final String[] arg1)
			throws SQLException {
		logStatement(arg0);
		return connection.prepareStatement(arg0, arg1);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param arg0
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public PreparedStatement prepareStatement(final String arg0) throws SQLException {
		logStatement(arg0);
		return connection.prepareStatement(arg0);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof WrappedConnection) {
			try {
				final WrappedConnection external = ((WrappedConnection) obj);

				return external.getConnection().equals(getConnection());

			} catch (final Exception e) {
				logger.error(e.getMessage(), e);
				return false;
			}
		}

		return false;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(final Connection connection) {
		this.connection = connection;
	}

	public ConnectionFactory getFactory() {
		return factory;
	}

	/**
	 * just formats the given string as a single line
	 * 
	 * @param content
	 * @return
	 */
	private void oneLineFormat(String content) {
		content = content.replaceAll("\n", " ");
		content = content.replaceAll("\t", " ");

		content = content.trim();

		final char[] a = content.toCharArray();
		char last = ' ';
		StringBuffer result = new StringBuffer();

		for (final char current : a) {
			if (current == ' ') {
				if (last == ' ') {
					// skip
				} else {
					result.append(current);
				}
			} else {

				if (current == '\n') {
					// skip
				} else {
					result.append(current);
					if (current == ';') {
						logger.debug(result);
						result = new StringBuffer();
					}
				}
			}

			last = current;
		}
		if (result.length() > 0) {
			logger.debug(result);
		}
	}

	/**
	 * logging function
	 * 
	 * @param arg0
	 */
	protected void logStatement(final String arg0) {
		if (logger.isDebugEnabled()) {
			oneLineFormat(arg0);
		}
	}
}
