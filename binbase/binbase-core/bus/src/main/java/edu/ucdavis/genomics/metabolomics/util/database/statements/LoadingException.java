package edu.ucdavis.genomics.metabolomics.util.database.statements;

public class LoadingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public LoadingException() {
	}

	public LoadingException(String arg0) {
		super(arg0);
	}

	public LoadingException(Throwable arg0) {
		super(arg0);
	}

	public LoadingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
