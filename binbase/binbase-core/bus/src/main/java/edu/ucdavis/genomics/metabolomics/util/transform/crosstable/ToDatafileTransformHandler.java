/*
 * Created on Nov 19, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.transform.crosstable;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;

/**
 * creates datafile from the given stream
 * 
 * @author wohlgemuth
 * @version Nov 19, 2005
 * 
 */
public class ToDatafileTransformHandler extends AbstractXMLTransformHandler {
	/**
	 * our datafile
	 */
	private DataFile dataFile = null;

	private DataFileFactory factory = null;
	
	private Logger logger = Logger.getLogger(getClass());
	
	public ToDatafileTransformHandler() {
		super();	
		logger.info("generate default instance");
		this.factory = DataFileFactory.newInstance();
		
		logger.info("datafile factory is of class: " + factory.getClass().getName());
	}

	public ToDatafileTransformHandler(DataFileFactory factory) {
		super();
		logger.info("generate instance using:  " + factory.getClass().getName());

		this.factory = factory;
	}

	/**
	 * writes a line to the datafile
	 * 
	 * @author wohlgemuth
	 * @version Nov 19, 2005
	 * @see edu.ucdavis.genomics.metabolomics.util.transform.crosstable.AbstractXMLTransformHandler#writeLine(java.util.List)
	 */
	public void writeLine(List<FormatObject<?>> line) {
		// convert line to numbers
		List<FormatObject<?>> result = new ArrayList<FormatObject<?>>();

		for (int i = 0; i < line.size(); i++) {
			result.add(line.get(i));
		}
		dataFile.addRow(line);
	}

	/**
	 * returns the generated datafile
	 * 
	 * @author wohlgemuth
	 * @version Nov 19, 2005
	 * @return
	 */
	public DataFile getFile() {
		return dataFile;
	}

	/**
	 * generates a datafile from this
	 * 
	 * @author wohlgemuth
	 * @version Nov 19, 2005
	 * @see edu.ucdavis.genomics.metabolomics.util.Describealbe#getDescription()
	 */
	public String getDescription() {
		return "create a datafile from the given xml stream";
	}

	public void startDocument() throws SAXException {
		super.startDocument();
		dataFile = this.factory.createDataFile();
	}
}
