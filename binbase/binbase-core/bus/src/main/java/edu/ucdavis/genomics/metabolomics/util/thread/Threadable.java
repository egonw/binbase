/*
 * Created on 25.04.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.util.thread;


/**
 * @author wohlgemuth
 */
public interface Threadable extends Runnable {
    /**
     * setzt den threadlistener
     * @param listener
     */
    void setListener(ThreadListener listener);

    /**
     * set the threads name, needed for logging
     * @param name
     *
     * @uml.property name="name"
     */
    void setName(String name);

    /**
     * get the threads name
     * @return
     *
     * @uml.property name="name"
     */
    String getName();

    /**
     * setzt den thread
     * @param thread
     *
     * @uml.property name="thread"
     */
    void setThread(Thread thread);

    /**
     * gibt den thread dieses objektes zurueck
     * @return
     *
     * @uml.property name="thread"
     */
    Thread getThread();
    
    /**
     * returns an unqiue thread identifier needed for cleanup
     * @author wohlgemuth
     * @version Nov 14, 2005
     * @return
     */
    Object getIdentifier();
    
    /**
     * sets the unique identifier
     * @author wohlgemuth
     * @version Nov 14, 2005
     * @param o
     */
    public void setIdentifier(Object o);
}
