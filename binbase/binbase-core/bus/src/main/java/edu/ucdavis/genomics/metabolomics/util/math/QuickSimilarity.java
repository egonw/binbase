package edu.ucdavis.genomics.metabolomics.util.math;

import edu.ucdavis.genomics.metabolomics.util.sort.Quicksort;

/**
 * a similarity which compares only the N highes peaks which each other
 * @author wohlgemuth
 *
 */
public class QuickSimilarity extends Similarity{

	private int maxIons = 5;
	
	
	public QuickSimilarity(int maxIons) {
		super();
		this.maxIons = maxIons;
	}

	@Override
	public void setLibrarySpectra(double[][] librarySpectra) {		
		super.setLibrarySpectra(reduce(librarySpectra, getMaxIons()));
	}

	@Override
	public void setLibrarySpectra(String librarySpectra) {
		setLibrarySpectra(reduce(convert(librarySpectra),getMaxIons()));
	}

	@Override
	public void setUnknownSpectra(double[][] unknownSpectra) {
		super.setUnknownSpectra(reduce(unknownSpectra,getMaxIons()));
	}

	@Override
	public void setUnknownSpectra(String unknownSpectra) {
		setUnknownSpectra(reduce(convert(unknownSpectra),getMaxIons()));
	}

	@Override
	public int getMaxIons() {
		return maxIons;
	}
	
	/**
	 * reduces the given spectra to the speciefied amount of highest ions
	 * @param spectra
	 * @return
	 */
	protected double[][] reduce(double[][] spectra,int ionCount){
		Quicksort sort = new Quicksort();
		sort.sort(spectra, SpectraArrayKey.FRAGMENT_ABS_POSITION);
		
        double[][] array = new double[getMaxIons()][ARRAY_WIDTH];

        double maxAbundance = -1;
        int counter = 0;
        for(int i = spectra.length-1; i >= (spectra.length-getMaxIons()); i--){
        	array[counter][FRAGMENT_ION_POSITION] = spectra[i][FRAGMENT_ION_POSITION];
        	array[counter][FRAGMENT_ABS_POSITION] = spectra[i][FRAGMENT_ABS_POSITION];
        	array[counter][FRAGMENT_REL_POSITION] = spectra[i][FRAGMENT_REL_POSITION];

        	if(array[counter][FRAGMENT_ABS_POSITION] > maxAbundance){
        		maxAbundance = array[counter][FRAGMENT_ABS_POSITION];
        	}
        	counter++;
        }
        
        for(int i = 0; i < getMaxIons(); i++){
        	array[i][FRAGMENT_REL_POSITION] = array[i][FRAGMENT_ABS_POSITION]/maxAbundance * 100;
        }
        
		return array;
	}

}
