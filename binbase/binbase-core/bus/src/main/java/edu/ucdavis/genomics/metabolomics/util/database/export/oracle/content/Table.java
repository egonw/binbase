/*
 * Created on Jun 9, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.database.export.oracle.content;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;


/**
 * @author wohlgemuth exports the content of an table
 */
public class Table {
    /**
     * DOCUMENT ME!
     *
     * @param c DOCUMENT ME!
     * @param stream DOCUMENT ME!
     * @param table DOCUMENT ME!
     *
     * @throws SQLException DOCUMENT ME!
     * @throws IOException DOCUMENT ME!
     */
    public static void export(Connection c, OutputStream stream, String table)
        throws SQLException, IOException {
        export(c, new OutputStreamWriter(stream), table);
    }

    /**
     * DOCUMENT ME!
     *
     * @param c DOCUMENT ME!
     * @param writer DOCUMENT ME!
     * @param table DOCUMENT ME!
     *
     * @throws SQLException DOCUMENT ME!
     * @throws IOException DOCUMENT ME!
     */
    public static void export(Connection c, Writer writer, String table)
        throws SQLException, IOException {
        PreparedStatement p = c.prepareStatement("SELECT * FROM \"" + table +
                "\"");
        ResultSet result = p.executeQuery();
        ResultSetMetaData meta = result.getMetaData();

        while (result.next()) {
            int count = meta.getColumnCount();

            StringBuffer buffer = new StringBuffer();

            //create insert part
            buffer.append("INSERT INTO \"");
            buffer.append(table);
            buffer.append("\" (");

            for (int i = 0; i < count; i++) {
                buffer.append("\"");
                buffer.append(meta.getColumnName(i + 1));
                buffer.append("\"");

                if ((i + 1) < count) {
                    buffer.append(",");
                }
            }

            buffer.append(") VALUES (");

            //create values
            for (int i = 0; i < count; i++) {
                switch (meta.getColumnType(i + 1)) {
                    case Types.CHAR:
                        buffer.append("'");
                        buffer.append(result.getString(i + 1));
                        buffer.append("'");

                        break;

                    case Types.VARCHAR:
                        buffer.append("'");
                        buffer.append(result.getString(i + 1));
                        buffer.append("'");

                        break;

                    case Types.BLOB:
                        buffer.append("'");
                        buffer.append(result.getString(i + 1));
                        buffer.append("'");

                        break;

                    case Types.CLOB:
                        buffer.append("'");
                        buffer.append(result.getString(i + 1));
                        buffer.append("'");

                        break;

                    case Types.NULL:
                        buffer.append(result.getString(i + 1));

                        break;

                    default:
                        buffer.append(result.getString(i + 1));

                        break;
                }

                if ((i + 1) < count) {
                    buffer.append(",");
                }
            }

            buffer.append(");\n");

            writer.write(buffer.toString());
            buffer = null;
        }

        result.close();
        p.close();
        writer.flush();
        writer.close();
    }

    /**
     * DOCUMENT ME!
     *
     * @param args DOCUMENT ME!
     *
     * @throws SQLException DOCUMENT ME!
     * @throws IOException DOCUMENT ME!
     */
    public static void main(String[] args) throws SQLException, IOException {
        Connection c = new SimpleConnectionFactory().getConnection();
        export(c, new FileWriter(args[0]), args[1]);
    }
}
