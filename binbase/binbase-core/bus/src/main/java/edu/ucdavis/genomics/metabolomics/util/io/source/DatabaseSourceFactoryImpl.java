/*
 * Created on Nov 10, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.io.source;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.io.source.SourceFactory;

public class DatabaseSourceFactoryImpl extends SourceFactory{

    public DatabaseSourceFactoryImpl() {
        super();
    }

    /**
     * create a database based source
     * @author wohlgemuth
     * @version Nov 10, 2005
     * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.source.SourceFactory#createSource(java.lang.Object, java.util.Map)
     */
    public DatabaseSource createSource(Object identifier, Map propertys) throws ConfigurationException {
    	DatabaseSource source =new DatabaseSource();
        source.configure(propertys);
        source.setIdentifier(identifier);
        return source;
    }

}
