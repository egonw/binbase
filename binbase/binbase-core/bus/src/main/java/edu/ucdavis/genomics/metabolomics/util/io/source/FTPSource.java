/*
 * Created on Mar 17, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.io.source;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FTPDestination;

/**
 * soruce for accessing ftp server
 * 
 * @author wohlgemuth
 * @version Mar 17, 2006
 * 
 */
public class FTPSource implements Source {
	FTPClient client = new FTPClient();

	private String server;

	private String port;

	private String username;

	private String password;

	private String name;

	private TempFileOutputStream stream;

	private boolean binaery = false;
	
	private Logger logger = Logger.getLogger(FTPSource.class);
	
	public FTPSource() {
		super();
	}

	public InputStream getStream() throws IOException {
		if (stream == null) {
			stream = new TempFileOutputStream();
		}
		return stream.getStream();
	}

	public String getSourceName() {
		return this.name;
	}

	public void setIdentifier(Object o) throws ConfigurationException {
		logger.debug("looking for: " + o);
		this.name = o.toString();
	}

	public void configure(Map p) throws ConfigurationException {
		this.server = (String) p.get(FTPDestination.FTP_SERVER);
		this.port = (String) p.get(FTPDestination.FTP_PORT);
		this.username = (String) p.get(FTPDestination.FTP_USER);
		this.password = (String) p.get(FTPDestination.FTP_PASSWORD);
		this.binaery  = ((Boolean) p.get(FTPDestination.FTP_BINAERY)).booleanValue();
	}

	public boolean exist() {
		try {
			if (stream != null) {
				return true;
			} else {
				stream = new TempFileOutputStream();
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * not supported
	 * 
	 * @author wohlgemuth
	 * @version Mar 17, 2006
	 * @see edu.ucdavis.genomics.metabolomics.util.io.source.Source#getVersion()
	 */
	public long getVersion() {
		throw new NotSupportedException("not supported by the source");
	}

	private class TempFileOutputStream{
		private File tempFile;

		public TempFileOutputStream() throws IOException {
			try {
				if (server == null) {
					throw new IOException("FTP_SERVER property is missing");
				} else {
					if (port != null) {
						client.connect(server, Integer.parseInt(port));
					} else {
						client.connect(server);
					}
				}

				if (username != null) {
					client.login(username, password);
				} else {
					client.login("anonymous", this.getClass().getName());
				}
				
				client.enterLocalPassiveMode();
				
				if (binaery) {
					logger.info("use binaery mode");
					client.setFileType(FTP.BINARY_FILE_TYPE);
				} else {
					logger.info("use ascii mode");				
					client.setFileType(FTP.ASCII_FILE_TYPE);
				}


				tempFile = File.createTempFile("binbase", ".ftp");
				OutputStream output = new FileOutputStream(tempFile);
				InputStream stream = client.retrieveFileStream(name);
				
				Copy.copy(stream,output);
	
				tempFile.deleteOnExit();
			} finally {
				if (client.isConnected()) {
					client.disconnect();
				}
			}
		}

		public InputStream getStream() throws IOException {
			InputStream stream =
				new FileInputStream(tempFile);
			return stream;
		}
	}
}
