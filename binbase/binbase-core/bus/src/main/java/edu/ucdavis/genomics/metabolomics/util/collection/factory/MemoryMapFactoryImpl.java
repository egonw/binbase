package edu.ucdavis.genomics.metabolomics.util.collection.factory;

import java.util.HashMap;
import java.util.Map;

public class MemoryMapFactoryImpl<K,V> extends MapFactory<K, V> {

	@Override
	public Map<K, V> createMap() {
		return new HashMap<K,V>();
	}

}
