/*
 * Created on 18.04.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.util.graph.dataset;

import edu.ucdavis.genomics.metabolomics.util.math.Regression;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.List;
import java.util.Vector;


/**
 * @author wohlgemuth
 *
 */
public class ChrommatogrammDataset extends BinBaseXYDataSet {
    /**
     * Comment for <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 2L;

    /**
     * f?gt ein neues chrommatogramm auf file basis hinzu
     * @param file
     * @param key
     * @throws IOException
     */
    public void addChrommatogramm(File file, String key)
        throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String[] header = reader.readLine().split("\t");
        String line = null;

        int retentionIndexPosition = 0;
        int keyPosition = 0;

        for (int i = 0; i < header.length; i++) {
            if (header[i].equals(key)) {
                keyPosition = i;
            }

            if ("Retention Index".equals(header[i])) {
                retentionIndexPosition = i;
            }
        }

        List x = new Vector();
        List y = new Vector();

        while ((line = reader.readLine()) != null) {
            String[] content = line.split("\t");
            x.add(new Double(content[retentionIndexPosition]));
            y.add(new Double(content[keyPosition]));
            x.add(new Double(content[retentionIndexPosition]));
            y.add(new Double(0));
        }

        double[] mx = new double[x.size()];
        double[] my = new double[y.size()];

        for (int i = 0; i < mx.length; i++) {
            mx[i] = ((Double) x.get(i)).doubleValue();
            my[i] = ((Double) y.get(i)).doubleValue();
        }

        this.addDataSet(mx, my, file.getName());
    }

    /**
     * verschiebt das datenset um einen vorgegebenen wert
     * @param id
     * @param shift
     */
    public void shiftDataset(int id, double shift) {
        double[] x = this.getX(id);
        double[] y = this.getY(id);

        for (int i = 0; i < x.length; i++) {
            x[i] = x[i] + shift;
        }

        this.replaceDataSet(x, y, this.getName(id));
    }

    /**
     * setzt die x verschiebung anhand einer zuvor vorgenommenen regression welche aus einer ri korrektur reultiert!
     * @param id
     * @param shift
     */
    public void shiftDataset(int id, Regression shift) {
        double[] x = this.getX(id);
        double[] y = this.getY(id);

        for (int i = 0; i < x.length; i++) {
            x[i] = shift.getY(x[i]);
        }

        this.replaceDataSet(x, y, this.getName(id));
    }

	@Override
	public Comparable getSeriesKey(int arg0) {
		return arg0;
	}
}
