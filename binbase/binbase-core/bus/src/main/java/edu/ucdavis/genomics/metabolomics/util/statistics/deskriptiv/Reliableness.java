/*
 * Created on 03.08.2004
 */
package edu.ucdavis.genomics.metabolomics.util.statistics.deskriptiv;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.WrongTypeOfValueException;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;


/**
 * @author wohlgemuth
 * gibt die zuverl?ssigkeit zur?ck. Der wert liegt zwischen 0 und 1 und ist das verh?ltnis zwischen anzahl von nullstellen und gesammtanzahl der klasse
 * 1 ist f?r 100% nullstellen frei
 */
public class Reliableness extends DeskriptiveMethod {
    /**
     * DOCUMENT ME!
     *
     * @uml.property name="logger"
     * @uml.associationEnd multiplicity="(1 1)"
     */
    private Logger logger = Logger.getLogger(this.getClass());

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.utils.statistics.deskriptiv.DeskriptiveMethod#getName()
     */
    public String getName() {
        return "Reliableness";
    }

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.utils.statistics.deskriptiv.DeskriptiveMethod#acceptZeros()
     */
    public boolean acceptZeros() {
        return true;
    }

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.utils.statistics.deskriptiv.DeskriptiveMethod#calculate(java.util.List)
     */
    public double calculate(Collection list) {
        Iterator it = list.iterator();
        int count = 0;

        while (it.hasNext() == true) {
            Object v = it.next();
            
            Object o = v;
            if(o instanceof FormatObject){
            	o = ((FormatObject)o).getValue();
            }

            if (o instanceof String) {
                try {
                    double value = Double.parseDouble((String) o);

                    if (Math.abs(value - 0) < 0.0001) {
                        count++;
                    }
                } catch (NumberFormatException e) {
                    logger.error(e.getMessage(), e);
                }
            } else if (o instanceof Number) {
                double value = ((Number) o).doubleValue();

                if (Math.abs(value - 0) < 0.0001) {
                    count++;
                }
            } else {
                throw new WrongTypeOfValueException(
                    "value has not the right class, is a " +
                    o.getClass().getName());
            }
        }

        double result =((double) count / (double) list.size()); 
        
        if(result > 0){
        	result = 1- result;
        }
        else{
        	result = result + 1;
        }
        return result;
    }
}
