/*
 * Created on 18.10.2004
 */
package edu.ucdavis.genomics.metabolomics.util.thread;

import java.util.Collection;


/**
 * @deprecated not anymore supported should be replaced with the new locking system
 * @author wohlgemuth definiert ein interface tf?r die haupteinheit der eigenen
 *         thread enginge
 */
public interface ThreadRunnable extends Threadable {
    /**
     * gint die anzahl der threads f?r dieses instanz zur?ck
     *
     * @return
     */
    int getCountOfThreads();

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.thread.Threadable#setListener(edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.thread.ThreadListener)
     */
    void setListener(ThreadListener listener);

    /**
     * gibt die collection mit den gesammelten ergebnissen zur?ck, welche die einzelnen threads lieferten.
     * @return
     */
    Collection getResults();

    /**
     * f?gt einen neuen thread hinzu
     *
     * @param thread
     */
    void addThreadable(Threadable thread);

    /**
     * l?scht die liste
     *
     */
    void clear();

    /**
     * entfernt einen thread
     *
     * @param thread
     */
    void removeThreadable(Threadable thread);

    /**
     * startet alle threads
     */
    void run();
}
