package edu.ucdavis.genomics.metabolomics.util.database;

import java.util.Properties;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException;
import edu.ucdavis.genomics.metabolomics.util.BasicObject;


/**
 * <p>
 * Title: DriverUtilities
 * </p>
 * <p>
 * Description: generates database urls
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: mpimp-golm
 * </p>
 *
 * @author unascribed
 * @version 1.0
 */
public class DriverUtilities {
    // init the log enviorement
    static Logger logger = null;

    static {
        new BasicObject();
        logger = Logger.getLogger("DriverUtilities");
        logger.debug("logger init");
    }

    /**
     * DOCUMENT ME!
     */
    public static final int UNKNOWN = -1;

    /**
     * DOCUMENT ME!
     */
    public static final int ORACLE = 2;

    public static final int POSTGRES = 3;

    public static final int MYSQL = 4;

    
    /**
     * DOCUMENT ME!
     */
    public static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";

    public static final String POSTGRES_DRIVER = "org.postgresql.Driver";

    public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";

    
    /**
     * creates the properties from the system properties
     *
     * @return
     */
    public static Properties createConnectionProperties(Properties p) {
        logger.debug("create connection properties with: " + p);

        Properties pr = (Properties) p.clone();

        String user = p.getProperty("Binbase.user");
        String password = p.getProperty("Binbase.password");
        String host = p.getProperty("Binbase.host");
        String database = p.getProperty("Binbase.database");

        logger.debug("user property found: " + !(user == null));
        logger.debug("password property found: " + !(password == null));
        logger.debug("host property found: " + !(host == null));
        logger.debug("database property found: " + !(database == null));

        if(user == null){
        	throw new ConnectionConfigurationException("please provide property: Binbase.user");
        }
        if(password == null){
        	throw new ConnectionConfigurationException("please provide property: Binbase.password");
        }
        if(host == null){
        	throw new ConnectionConfigurationException("please provide property: Binbase.host");
        }
        if(database == null){
        	throw new ConnectionConfigurationException("please provide property: Binbase.database");
        }
        
        if(pr.get("Binbase.type") == null){
        	throw new ConnectionConfigurationException("please provide property: Binbase.type");
        }
        
        pr.setProperty("hibernate.connection.username", user);
        pr.setProperty("hibernate.connection.password", password);

        int typ = Integer.parseInt(p.getProperty("Binbase.type"));

        if (typ == ORACLE) {
            // oracle specific, dont change this!
            pr.setProperty("SetBigStringTryClob", "true");
            pr.setProperty("hibernate.dialect",
                "org.hibernate.dialect.Oracle9Dialect");
            pr.setProperty("hibernate.connection.driver_class", ORACLE_DRIVER);
            pr.setProperty("hibernate.connection.url",
                generateOracleUrl(host, database));
        }
        else if(typ == POSTGRES){
        	//postgres specific
            pr.setProperty("SetBigStringTryClob", "true");
            pr.setProperty("hibernate.dialect",
                "org.hibernate.dialect.PostgreSQLDialect");
            pr.setProperty("hibernate.connection.driver_class", POSTGRES_DRIVER);
            pr.setProperty("hibernate.connection.url",
                generatePostgresUrl(host, database));
        	
        }
        else if(typ == MYSQL){
            pr.setProperty("hibernate.dialect",
            "org.hibernate.dialect.MySQLDialec");
        pr.setProperty("hibernate.connection.driver_class", MYSQL_DRIVER);
        pr.setProperty("hibernate.connection.url",
            generateMySQLUrl(host, database));
        	
        }
        else {
            throw new NotSupportedException("sorry used driver isnt supported");
        }

        return pr;
    }

    /**
     * DOCUMENT ME!
     *
     * @param host
     *            DOCUMENT ME!
     * @param database
     *            DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static String generateOracleUrl(String host, String database) {
        return "jdbc:oracle:thin:@" + host + ":1521:" + database;
    }

    public static String generatePostgresUrl(String host, String database) {
        return "jdbc:postgresql://" + host + "/" + database;
    }

    /**
     * generates a database connection string
     *
     * @param host
     *            hostname or ip
     * @param database
     *            database name
     * @param vendor
     *            database vendor
     * @return connection string
     */
    public static String generateUrl(String host, String database, int vendor) {
        switch(vendor){
        case ORACLE:
        	return generateOracleUrl(host, database);
        case POSTGRES:
        	return generatePostgresUrl(host, database);
        case MYSQL:
        	return generateMySQLUrl(host, database);
        	
        default:
        	return null;
        }

    }
    
    private static String generateMySQLUrl(String host, String database) {
		return "jdbc:mysql://"+host+"/"+database;
	}

	public static String getDriver(int vendor){
        switch(vendor){
        case ORACLE:
        	logger.info("oracle was requested...");
        	return ORACLE_DRIVER;
        case POSTGRES:
        	logger.info("postgres was requested...");
        	return POSTGRES_DRIVER;
        default:
            throw new NotSupportedException("sorry used driver isnt supported");
        }

    }
}
