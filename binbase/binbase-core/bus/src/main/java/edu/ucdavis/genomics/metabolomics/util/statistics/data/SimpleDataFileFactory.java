/*
 * Created on Jun 20, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.statistics.data;

public class SimpleDataFileFactory extends DataFileFactory{

	@Override
	public DataFile createDataFile() {
		return new SimpleDatafile();
	}

}
