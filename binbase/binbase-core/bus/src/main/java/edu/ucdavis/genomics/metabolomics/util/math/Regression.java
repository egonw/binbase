/*
 * Created on 18.04.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.util.math;

import java.io.Serializable;

import org.jdom.Element;


/**
 * basis klasse f?r regressionen
 *
 * @author wohlgemuth
 *
 */
public interface Regression extends Serializable,Cloneable{
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    double[] getCoeffizent();

    /**
     * DOCUMENT ME!
     *
     * @param x DOCUMENT ME!
     * @param y DOCUMENT ME!
     */
    void setData(double[] x, double[] y);

    double[] getXData();
    
    double[] getYData();
    
    /**
     * DOCUMENT ME!
     *
     * @param x DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    double getY(double x);
    
    /**
     * configuration of the regression
     * @author wohlgemuth
     * @version Feb 20, 2007
     * @param element
     */
    public void config(Element element);
    
    public String[] getFormulas();
}
