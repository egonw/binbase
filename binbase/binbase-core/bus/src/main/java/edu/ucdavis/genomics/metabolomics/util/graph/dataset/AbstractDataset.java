/*
 * Created on 26.04.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.util.graph.dataset;

import org.jfree.data.general.Dataset;


/**
 * @author wohlgemuth
 *
 */
public interface AbstractDataset extends Dataset{

    /**
     * DOCUMENT ME!
     */
    public abstract void clear();

    /**
     * DOCUMENT ME!
     */
    public abstract void refresh();

    /**
     * DOCUMENT ME!
     */
    public abstract void update();
}
