/*
 * Created on Aug 18, 2003
 *
 */
package edu.ucdavis.genomics.metabolomics.util.xls;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.BinObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ClassObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.CombinedObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ErrorObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.MetaObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.NullObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ProblematicObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.RefrenceObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.SampleObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ZeroObject;

/**
 * @author wohlgemuth
 * @version Aug 18, 2003 <br>
 *          BinBaseDatabase
 * @description erstellt eine formatierte excel tabelle, mit farben und
 *              nullwertmarkierung
 */
public class ColoredSplitToSheets implements Splitter {

	private transient Logger logger = Logger.getLogger(getClass());

	/**
	 * count of columns per sheet
	 */
	private static final int MAX_COLUMN = 256;

	/**
	 * style for the class
	 */
	private HSSFCellStyle classStyle;

	/**
	 * DOCUMENT ME!
	 */
	private HSSFCellStyle errorStyle;

	/**
	 * style for header entrys
	 */
	private HSSFCellStyle headerStyle;

	/**
	 * DOCUMENT ME!
	 */
	private HSSFCellStyle metaStyle;

	/**
	 * style for the samples
	 */
	private HSSFCellStyle sampleStyle;

	/**
	 * style for zero numbers
	 */
	private HSSFCellStyle zeroStyle;

	/**
	 * the workbook
	 */
	private HSSFWorkbook workbook = new HSSFWorkbook();

	/**
	 * sheets
	 */
	private HSSFSheet[] sheets = null;

	/**
	 * DOCUMENT ME!
	 */
	private boolean firstrun = true;

	/**
	 * DOCUMENT ME!
	 */
	private boolean header;

	/**
	 * DOCUMENT ME!
	 */
	private boolean last;

	/**
	 * DOCUMENT ME!
	 */
	private int size = 0;

	/**
	 * the row counter
	 */
	private short rowCounter;

	private HSSFCellStyle probStyle;

	private HSSFCellStyle nullStyle;

	private HSSFCellStyle binStyle;

	private HSSFCellStyle refrenceStyle;

	private HSSFCellStyle contentStyle;

	private HSSFCellStyle combinedStyle;

	/**
	 * Creates a new ColoredSplitToSheets object.
	 */
	public ColoredSplitToSheets() {
		// sample style
		{
			sampleStyle = workbook.createCellStyle();

			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setItalic(true);
			sampleStyle.setFont(font);

			sampleStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			sampleStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			sampleStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		}
		// header style
		{
			headerStyle = workbook.createCellStyle();

			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			headerStyle.setFont(font);
			headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			headerStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		}

		// bin style
		{
			binStyle = workbook.createCellStyle();
			binStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			binStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			binStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		}

		// refrence style
		{
			refrenceStyle = workbook.createCellStyle();
			refrenceStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			refrenceStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			refrenceStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		}

		// content style
		{
			contentStyle = workbook.createCellStyle();
			contentStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			contentStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
			contentStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		}

		// combined style
		{
			combinedStyle = workbook.createCellStyle();
			combinedStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			combinedStyle.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
			combinedStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		}
		// zero style
		{
			zeroStyle = workbook.createCellStyle();
			zeroStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			zeroStyle.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
			zeroStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		}
		// null style
		{
			nullStyle = workbook.createCellStyle();
			nullStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			nullStyle.setFillForegroundColor(HSSFColor.ORANGE.index);
			nullStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		}

		// problematic style
		{
			probStyle = workbook.createCellStyle();
			probStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			probStyle.setFillForegroundColor(HSSFColor.LIGHT_ORANGE.index);
			probStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		}

		// error style
		{
			errorStyle = workbook.createCellStyle();
			errorStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			errorStyle.setFillForegroundColor(HSSFColor.DARK_RED.index);
			errorStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		}
		// class style
		{
			classStyle = workbook.createCellStyle();
			classStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			classStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			classStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		}
		// meta style
		{
			metaStyle = workbook.createCellStyle();
			metaStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			metaStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
			metaStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		}
	}

	/**
	 * @version Aug 21, 2003
	 * @author wohlgemuth <br>
	 * @see edu.ucdavis.genomics.metabolomics.binbase.utils.xls.Splitter#getBook()
	 */
	public HSSFWorkbook getBook() {
		return this.workbook;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.utils.xls.Splitter#setHeader(boolean)
	 * @uml.property name="header"
	 */
	public void setHeader(boolean value) {
		header = value;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.utils.xls.Splitter#isHeader()
	 * @uml.property name="header"
	 */
	public boolean isHeader() {
		return header;
	}

	/**
	 * f?gt eine neue linie in das workbook ein
	 * 
	 * @version Aug 18, 2003
	 * @author wohlgemuth <br>
	 * @param line
	 */
	public void addLine(List<FormatObject<?>> line) {
		if (line == null) {
			return;
		}

		boolean lastLine = line.isEmpty();

		/*
		 * berechnen der ben?tigten ausmasse f?r das excel sheet
		 */
		if (firstrun == true) {
			size = line.size();

			double count = (double) size / (double) (MAX_COLUMN - 1);
			count = Math.ceil(count) + 1;
			sheets = new HSSFSheet[(int) count];

			for (int i = 0; i < sheets.length; i++) {
				sheets[i] = workbook.createSheet();
			}
			logger.info("count of sheets needed: " + (sheets.length + 1));
		}

		/*
		 * f?llen und formatieren des sheets nach dem gew?nschten vorgaben
		 */
		Iterator<FormatObject<?>> it = line.iterator();

		for (short i = 0; i < sheets.length; i++) {
			HSSFRow row = sheets[i].createRow(rowCounter);
			short x = 0;

			boolean next = it.hasNext();

			while (next == true) {
				if (x < MAX_COLUMN) {
					try {
						Object nextObject = it.next();
						FormatObject<?> o = null;

						if (nextObject instanceof FormatObject) {
							o = (FormatObject<?>) nextObject;
						} else {
							if (nextObject == null) {
								nextObject = "";
							}
							o = new MetaObject<Object>(nextObject);
						}

						// it.remove();

						// wenn diese zeile nichtbesonders formatiert werden
						// soll
						// header
						if (last == true) {
							firstrun = last;
						}

						formatObject(row, x, o);

						x++;
					} catch (NoSuchElementException e) {
						next = false;
					}
				} else {
					next = false;
				}
			}
		}

		rowCounter++;
		firstrun = false;
		this.last = lastLine;
	}

	/**
	 * formats the actual object
	 * 
	 * @param row
	 * @param x
	 * @param o
	 */
	private void formatObject(HSSFRow row, short x, FormatObject o) {
		if (o instanceof SampleObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(sampleStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(o.getValue().toString());
		} else if (o instanceof BinObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(binStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);

			cell.setCellValue(o.getValue().toString());

		} else if (o instanceof ClassObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(classStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(o.getValue().toString());
		} else if (o instanceof NullObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(nullStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			try {
				cell.setCellValue(Double.parseDouble(o.getValue().toString()));
			} catch (NumberFormatException e) {
				cell.setCellValue((o.getValue().toString()));
			}

		} else if (o instanceof ProblematicObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(probStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			try {
				cell.setCellValue(Double.parseDouble(o.getValue().toString()));
			} catch (NumberFormatException e) {
				cell.setCellValue((o.getValue().toString()));
			}

		} else if (o instanceof CombinedObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(combinedStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			try {
				cell.setCellValue(Double.parseDouble(o.getValue().toString()));
			} catch (NumberFormatException e) {
				cell.setCellValue((o.getValue().toString()));
			}

		} else if (o instanceof ZeroObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(zeroStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			try {
				cell.setCellValue(Double.parseDouble(o.getValue().toString()));
			} catch (NumberFormatException e) {
				cell.setCellValue((o.getValue().toString()));
			}

		} else if (o instanceof HeaderFormat) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(headerStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(o.getValue().toString());
		} else if (o instanceof RefrenceObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(refrenceStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);

			RefrenceObject<?> ref = (RefrenceObject<?>) o;

			if (ref.getHyperlink() != null) {
				if (ref.getHyperlink().length() > 0) {
					cell.setCellFormula("HYPERLINK(\"" + ref.getHyperlink()
							+ "\";\"" + ref.getValue() + "\")");
				} else {
					cell.setCellValue(o.getValue().toString());
				}
			} else {
				cell.setCellValue(o.getValue().toString());
			}
		} else if (o instanceof MetaObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(metaStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(o.getValue().toString());
		} else if (o instanceof ContentObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(contentStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			try {
				cell.setCellValue(Double.parseDouble(o.getValue().toString()));
			} catch (NumberFormatException e) {
				cell.setCellValue((o.getValue().toString()));
			}
		} else if (o instanceof ErrorObject) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(errorStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			cell.setCellValue(o.getValue().toString());
		} else if (o == null) {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(nullStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			cell.setCellValue(0);
		} else {
			HSSFCell cell = row.createCell(x);
			cell.setCellStyle(sampleStyle);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(o.getValue().toString());
		}
	}

	@Override
	protected void finalize() throws Throwable {
		sheets = null;
		super.finalize();
	}

	/**
	 * Speichert die Datei als excel workbook und beendet die m?glichkeit des
	 * anf?gen von Linie
	 * 
	 * @version Aug 18, 2003
	 * @author wohlgemuth <br>
	 * @param file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void saveBook(File file) throws FileNotFoundException, IOException {
		this.saveBook(new FileOutputStream(file));
	}

	/**
	 * @version Aug 30, 2003
	 * @author wohlgemuth <br>
	 * @see edu.ucdavis.genomics.metabolomics.binbase.utils.xls.Splitter#saveBook(java.io.OutputStream)
	 */
	public void saveBook(OutputStream stream) throws IOException {
		workbook.write(stream);
	}
}
