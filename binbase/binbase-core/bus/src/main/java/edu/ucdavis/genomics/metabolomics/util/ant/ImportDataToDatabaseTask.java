package edu.ucdavis.genomics.metabolomics.util.ant;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;

import javax.mail.Quota.Resource;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.stream.IDataSetProducer;
import org.dbunit.dataset.stream.StreamingDataSet;
import org.dbunit.dataset.xml.XmlProducer;
import org.xml.sax.InputSource;

import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities;
import edu.ucdavis.genomics.metabolomics.util.database.test.InitializeDBUtil;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

public class ImportDataToDatabaseTask extends Task {

	public static void main(String args[]) {

		ImportDataToDatabaseTask task = new ImportDataToDatabaseTask();
		task.setProject(new Project());
		task.setDatabase(args[0]);
		task.setServer(args[1]);
		task.setUsername(args[2]);
		task.setPassword(args[3]);
		task.setInputFile(new File(args[4]));

		task.execute();
	}

	@Override
	public void execute() throws BuildException {
		if (username == null) {
			throw new BuildException("please set an username");
		}
		if (password == null) {
			throw new BuildException("please set a password");
		}

		if (server == null) {
			throw new BuildException("please set a server");
		}

		if (database == null) {
			throw new BuildException("please set a database");
		}
		if (inputFile == null) {
			throw new BuildException("please set an output file");
		}

		try {

			System.setProperty(ConnectionFactory.KEY_DATABASE_PROPERTIE, this.getDatabase());
			System.setProperty(ConnectionFactory.KEY_TYPE_PROPERTIE, String.valueOf(DriverUtilities.POSTGRES));
			System.setProperty(ConnectionFactory.KEY_HOST_PROPERTIE, this.getServer());
			System.setProperty(ConnectionFactory.KEY_PASSWORD_PROPERTIE, this.getPassword());
			System.setProperty(ConnectionFactory.KEY_USERNAME_PROPERTIE, this.getUsername());

			ConnectionFactory factory = ConnectionFactory.getFactory();
			factory.setProperties(System.getProperties());
			Connection connection = factory.getConnection();

			IDataSetProducer producer = new XmlProducer(new InputSource(new FileInputStream(inputFile)));
			IDataSet dataSet = new StreamingDataSet(producer);

			File file = new File("sql/binbase-create.sql");
			if(file.exists()){
				InitializeDBUtil.initialize(this.getUsername(), this.getPassword(), this.getServer(), this.getDatabase(), new FileSource(file), dataSet);
			}
			else{
				ResourceSource source = new ResourceSource("/sql/binbase-create.sql");
				InitializeDBUtil.initialize(this.getUsername(), this.getPassword(), this.getServer(), this.getDatabase(), source, dataSet);

			}
			factory.close(connection);

		}
		catch (Exception e) {
			throw new BuildException(e);
		}
	}

	private String username;

	private String password;

	private String server;

	private String database;

	private File inputFile;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public File getInputFile() {
		return inputFile;
	}

	public void setInputFile(File inputFile) {
		this.inputFile = inputFile;
	}
}
