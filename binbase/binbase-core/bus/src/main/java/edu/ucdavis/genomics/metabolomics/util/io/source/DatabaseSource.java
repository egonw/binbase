/*
 * Created on Nov 10, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.io.source;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;

/**
 * defines a source which uses an underlaying database to load data
 * 
 * @author wohlgemuth
 * @version Nov 10, 2005
 * 
 */
public class DatabaseSource implements Source {
	private Logger logger = Logger.getLogger(DatabaseSource.class);
	/**
	 * the used identifier
	 */
	private String identifier;

	/**
	 * the statement which provides us with the latest version
	 */
	private PreparedStatement select;

	private Connection connection;
	private PreparedStatement version;

	public DatabaseSource() {
		super();
	}

	protected ResultSet executeStatement() throws SQLException {
		logger.info("using source name: " + this.getSourceName().trim());
		select.setString(1, this.getSourceName().trim());
		ResultSet result = this.select.executeQuery();

		return result;
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Nov 10, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.source.Source#getStream()
	 */
	public InputStream getStream() throws IOException {
		try {
			ResultSet result = this.executeStatement();

			InputStream in = null;

			if (result.next()) {
				in = result.getBinaryStream(1);
			}

			result.close();

			if (in != null) {
				GZIPInputStream stream = new GZIPInputStream(in);
				return stream;
			} else {
				throw new IOException("identifier not found: " + this.getSourceName());
			}
		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}
	}

	/**
	 * the used identifier
	 * 
	 * @author wohlgemuth
	 * @version Nov 10, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.source.Source#setIdentifier(java.lang.Object)
	 */
	public void setIdentifier(Object o) throws ConfigurationException {
		if (o instanceof String) {
			this.identifier = (String) o;
		} else {
			throw new ConfigurationException("identifier must be a string");
		}
	}

	/**
	 * configure source
	 * 
	 * @author wohlgemuth
	 * @version Nov 10, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.source.Source#configure(java.util.Map)
	 */
	public void configure(Map p) throws ConfigurationException {
		try {
			if (p.get("CONNECTION") == null) {
				logger.warn("create new connection!!!");
				ConnectionFactory factory = ConnectionFactory.getFactory();
				Properties prop = new Properties();
				prop.putAll(p);
				factory.setProperties(prop);
				this.setConnection(factory.getConnection());
			} else {
				this.setConnection((Connection) p.get("CONNECTION"));
			}
		} catch (Exception e) {
			throw new ConfigurationException(e);
		}
	}


	/**
	 * 
	 * @author wohlgemuth
	 * @version Nov 10, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.source.Source#getSourceName()
	 */
	public String getSourceName() {
		return this.identifier;
	}

	protected void prepareStatements() throws SQLException {
		this.select = this.getConnection().prepareStatement("SELECT data FROM rawdata where name = ? and visible = 'TRUE'");
		this.version = this.getConnection().prepareStatement("SELECT hash FROM rawdata where name = ? and visible = 'TRUE'");
	}

	/**
	 * checks against the database if something with this identifier exist
	 * 
	 * @author wohlgemuth
	 * @version Nov 17, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.source.Source#exist()
	 */
	public boolean exist() {
		try {
			ResultSet result = this.executeStatement();

			boolean exist;

			if (result.next()) {
				exist = true;
			} else {
				exist = false;
			}
			result.close();
			return exist;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}
	
	
	/**
	 * returns the connection
	 * @author wohlgemuth
	 * @version Jan 18, 2006
	 * @return
	 */
	protected Connection getConnection(){
		return connection;
	}

	public void setConnection(Connection connection) throws SQLException {
		this.connection = connection;
		this.prepareStatements();
	}

	public long getVersion() {
		try {
			version.setString(1, this.getSourceName().trim());
			ResultSet result = this.version.executeQuery();

			long version = -1;

			if (result.next()) {
				version = result.getInt(1);
			}
			result.close();

			if(version == -1){
				throw new Exception("no version found, source does not exist");
			}
			return version;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
