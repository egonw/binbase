/*
 * Created on Nov 10, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.io.dest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.io.source.DatabaseSourceFactoryImpl;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.io.source.SourceFactory;

/**
 * we save the data into a database and compress them before
 * 
 * @author wohlgemuth
 * @version Nov 10, 2005
 * 
 */
public class DatabaseDestination implements Destination {
	private Logger logger = Logger.getLogger(DatabaseDestination.class);
	/**
	 * an internal link to the source itsels
	 */
	Source source;
	/**
	 * insert some data into the database
	 */
	PreparedStatement insert;

	/**
	 * updates the database
	 */
	PreparedStatement update;

	/**
	 * our stream to save data
	 */
	OutputStream stream;
	
	private Map properties;

	public DatabaseDestination() {
		super();
	}

	/**
	 * here we configure the connection itself
	 * 
	 * @author wohlgemuth
	 * @version Nov 10, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.dest.Destination#configure(java.util.Map)
	 */
	public void configure(Map p) throws ConfigurationException {
		try {
			this.setProperties(p);
			if (p.get("CONNECTION") == null) {
				this.setConnection(ConnectionFactory.getFactory().getConnection());
			} else {
				this.setConnection((Connection) p.get("CONNECTION"));
			}
		} catch (SQLException e) {
			throw new ConfigurationException(e.getMessage(),e);
		}
	}

	/**
	 * returns a outputstream based on a blob
	 * 
	 * @author wohlgemuth
	 * @version Nov 10, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.dest.Destination#getOutputStream()
	 */
	public OutputStream getOutputStream() throws IOException {

		// we like to save data in a blob
		stream = new BlobOutputStream();

		// we want to compress our data
		GZIPOutputStream out = new GZIPOutputStream(stream);

		// return the compressed stream
		return out;
	}

	String property;

	private Connection connection;

	/**
	 * identifier must be a file or is used as filename
	 * 
	 * @author wohlgemuth
	 * @version Nov 10, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.dest.Destination#setIdentifier(java.lang.Object)
	 */
	public void setIdentifier(Object o) throws ConfigurationException {
		if (o == null) {
			throw new ConfigurationException("object cannot be null");
		}

		property = o.toString();
		source = SourceFactory.newInstance(DatabaseSourceFactoryImpl.class.getName()).createSource(property,this.getProperties());
	}

	/**
	 * used to store bolobs and make a commit at the end of the transaction
	 * 
	 * @author wohlgemuth
	 * @version Nov 10, 2005
	 * 
	 */
	private class BlobOutputStream extends ByteArrayOutputStream {

		/**
		 * when we close the stream we want to save the data and not before
		 * 
		 * @author wohlgemuth
		 * @version Nov 10, 2005
		 * @see java.io.Closeable#close()
		 */
		public synchronized void close() throws IOException {
			super.close();

			// update all content with this name to visible = false
			try {
				executeInsert(this);
				// getConnection().commit();
			} catch (Exception e) {
				try {
					getConnection().rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				throw new IOException(e.getMessage());
			}
		}

	}

	/**
	 * insert the data into the database
	 * 
	 * @author wohlgemuth
	 * @version Nov 18, 2005
	 * @throws SQLException
	 */
	protected void executeInsert(ByteArrayOutputStream stream) throws SQLException {
		// the current date
		Timestamp date = new Timestamp(new java.util.Date().getTime());

		// the name of the file without extension
		String name = property;

		
		if(source.exist() == true){
			
			if(source.getVersion() == stream.size()){
				logger.info("source is identical and will not imported " + source.getSourceName() + "/" + name);

				return;
			}
			else{
				logger.info("source is different from from current, set old to not available!");

				//update different version so they are not exported
				update.setString(1, name);
				update.setInt(2, stream.size());
				update.execute();
			}
		}
		
		
		// insert data
		insert.setTimestamp(1, date);
		insert.setString(2, name);
		insert.setBinaryStream(3, new ByteArrayInputStream(stream.toByteArray()), stream.toByteArray().length);
		insert.setInt(4, stream.size());
		insert.execute();
	}

	protected void prepareStatements() throws SQLException {
		this.insert = this.getConnection().prepareStatement("INSERT INTO RAWDATA(version,name,data,visible,hash) values(?,?,?,'TRUE',?)");
		this.update = this.getConnection().prepareStatement("UPDATE RAWDATA set visible = 'FALSE' where name=? AND hash !=?");
	}

	/**
	 * returns the connection
	 * 
	 * @author wohlgemuth
	 * @version Jan 18, 2006
	 * @return
	 */
	protected Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) throws SQLException {
		this.connection = connection;
		this.prepareStatements();
	}

	private Map getProperties() {
		return properties;
	}

	private void setProperties(Map properties) {
		this.properties = properties;
	}
}
