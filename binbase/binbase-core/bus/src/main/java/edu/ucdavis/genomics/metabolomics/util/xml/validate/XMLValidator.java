/*
 * Created on Dec 10, 2004
 */
package edu.ucdavis.genomics.metabolomics.util.xml.validate;

import org.jdom.input.SAXBuilder;

import java.io.StringReader;


/**
 * @author wohlgemuth einfache klasse zur validierung von xm files
 */
public class XMLValidator {
    /**
     * validiert das xml file und ueberpprueft ob es vom syntax her korrekt ist
     *
     * @param xml
     * @return
     */
    public static boolean isValid(String xml) {
        return isValid(xml, false,null);
    }
    
    /**
     * @deprecated
     * @param xml
     * @param valid
     * @return
     */
    public static boolean isValid(String xml,boolean valid) {
        return isValid(xml, valid,null);
    }

    /**
     * validiert das xml file und ueberpprueft ob es vom syntax her korrekt ist
     *
     * @param xml
     * @return
     */
    public static boolean isValid(String xml, boolean strict,String schemaURL) {
        try {
            // Create a SAXBuilder parser.
            SAXBuilder saxBuilder = new SAXBuilder();

            if (strict) {
            	if(schemaURL == null){
            		throw new RuntimeException("sorry you need to specify a schema url!");
            	}
                saxBuilder.setFeature("http://xml.org/sax/features/validation",
                    true);
                saxBuilder.setFeature("http://apache.org/xml/features/validation/schema",
                    true);
                saxBuilder.setFeature("http://apache.org/xml/features/validation/schema-full-checking",
                    true);

                saxBuilder.setProperty("http://apache.org/xml/properties/schema/external-noNamespaceSchemaLocation",schemaURL);
            }

            saxBuilder.build(new StringReader(xml));

            return true;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }
}
