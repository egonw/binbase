package edu.ucdavis.genomics.metabolomics.util.ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.util.Properties;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.DatabaseUtilities;
import edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities;
import edu.ucdavis.genomics.metabolomics.util.database.ExtractDataFromDatabase;

/**
 * simple ant task to dump the binbase
 * 
 * @author wohlgemuth
 */
public class ExtractDataFromDatabaseTask extends Task {
	@Override
	public void execute() throws BuildException {
		if (username == null) {
			throw new BuildException("please set an username");
		}
		if (password == null) {
			throw new BuildException("please set a password");
		}

		if (server == null) {
			throw new BuildException("please set a server");
		}

		if (database == null) {
			throw new BuildException("please set a database");
		}
		if (outputFile == null) {
			throw new BuildException("please set an output file");
		}

		try {
			System.setProperty(ConnectionFactory.KEY_USERNAME_PROPERTIE, username);
			String url = DriverUtilities.generateOracleUrl(server, database);
			String driver = DriverUtilities.ORACLE_DRIVER;
			if(isUseSpy()){
				driver = ps6spy;
				Properties p = new Properties();
				p.load(new FileInputStream("spy.properties"));
				p.setProperty("realdriver", DriverUtilities.ORACLE_DRIVER);
				p.store(new FileOutputStream("spy.properties"), "updated with real driver");
			}
			
			Connection c = DatabaseUtilities.createConnection(driver, url, username, password);

			ExtractDataFromDatabase extract = new ExtractDataFromDatabase();
			FileOutputStream out = new FileOutputStream(outputFile);
			extract.extractData(out, c);
			out.flush();
			out.close();
		}
		catch (Exception e) {
			throw new BuildException(e);
		}
	}
	private boolean useSpy = false;

	private String username;

	private String password;

	private String server;

	private String database;

	private File outputFile;

	private String ps6spy = "com.p6spy.engine.spy.P6SpyDriver";
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public File getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(File outputFile) {
		this.outputFile = outputFile;
	}

	public boolean isUseSpy() {
		return useSpy;
	}

	public void setUseSpy(boolean useSpy) {
		this.useSpy = useSpy;
	}

}
