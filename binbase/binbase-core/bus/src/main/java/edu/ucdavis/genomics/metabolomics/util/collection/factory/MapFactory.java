package edu.ucdavis.genomics.metabolomics.util.collection.factory;

import java.util.Collection;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

public abstract class MapFactory<K,V> extends AbstractFactory{
	

    public static final String DEFAULT_PROPERTY_NAME = MapFactory
    .class.getName();

    private static String foundFactory = null;

    public static <K,V> MapFactory<K, V> newInstance(){
        // Locate Factory
        foundFactory = findFactory(
                DEFAULT_PROPERTY_NAME,
                MemoryMapFactoryImpl.class.getName());

        return newInstance(foundFactory);    	
    }
    
    @SuppressWarnings("unchecked")
	public static <K,V> MapFactory<K, V> newInstance(String factoryClass) {
        Class<MapFactory<K, V>> classObject;
        MapFactory<K,V> factory;

        try {
            classObject = (Class<MapFactory<K, V>>) Class.forName(factoryClass);
            factory = classObject.newInstance();
            return factory;

        } catch (Exception e) {
            throw new FactoryException(e);
        }
    }

	private Collection<K> configuration;

    public Collection<K> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Collection<K> configuration) {
		this.configuration = configuration;
	}

	/**
     * creates a factory of the following configuration
     * @return
     */ 
    public abstract Map<K,V> createMap();

}
