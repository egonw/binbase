/*
 * Created on 26.04.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.plot;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;

import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet;
import edu.ucdavis.genomics.metabolomics.util.graph.label.PeakLabelGenerator;

/**
 * @author wohlgemuth erstellt einen spektren chart und visualisert die daten
 */
public class SpectraChart {
	/**
	 * erstellt den chart und gibt ihn zur?ck
	 * 
	 * @return
	 */
	public static JFreeChart createChart(BinBaseXYDataSet data) {
		JFreeChart chart = new JFreeChart("",
				createPlot(data));
		chart.setAntiAlias(true);
		//chart.setLegend(null);

		return chart;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param data
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	public static XYPlot createPlot(BinBaseXYDataSet data) {
		NumberAxis x = new NumberAxis("fragment");
		ValueAxis y = new NumberAxis("intensity");
		x.setAutoRangeIncludesZero(false);

		x.setRange(80, 500);
		x.setAutoRangeMinimumSize(80);
		x.setAutoRange(false);

		y.setRange(0, 1.2);
		y.setAutoRange(false);

		XYBarRenderer rendererShift = new XYBarRenderer();
		PeakLabelGenerator tooltip = new PeakLabelGenerator();
		
		rendererShift.setToolTipGenerator(tooltip);
		rendererShift.setBaseItemLabelsVisible(Boolean.TRUE);
		rendererShift.setBaseItemLabelGenerator(tooltip);
		
		XYPlot plot = new XYPlot(data, x, y, rendererShift);
		return plot;
	}
}
