/*
 * Created on Jun 16, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object;

import java.util.Collection;
import java.util.Map;
import java.util.Vector;

import org.xml.sax.Attributes;

import edu.ucdavis.genomics.metabolomics.util.collection.factory.MapFactory;

public class ContentObject<Type>  extends FormatObject<Type>{

	private Map<String,Object> attachments;
	
	public ContentObject(Type value, Map<String,String> attributes) {
		super(value, attributes);
		Collection<String> storedKeys = new Vector<String>();
		storedKeys.add("graph");
		
		MapFactory<String,Object > factory = MapFactory.<String, Object>newInstance();
		factory.setConfiguration(storedKeys);
		
		attachments = factory.createMap();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	public ContentObject(Type value) {
		super(value);
	}
	public ContentObject(Type value, Attributes a) {
		super(value,a);
	}
	
			
	public Map<String,Object> getAttachments() {
		return attachments;
	}

	public void setAttachments(Map<String,Object> attachments) {
		this.attachments = attachments;
	}
	
	
	public void addAttachment(String name, Object value){
		this.attachments.put(name, value);
	}
}

