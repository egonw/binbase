/*
 * Created on Jun 20, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.statistics.data;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * used to create datafiles
 * @author wohlgemuth
 * @version Jun 20, 2006
 *
 */
public abstract class DataFileFactory extends AbstractFactory{

    /**
     * name of the property
     */
    public static final String DEFAULT_PROPERTY_NAME = DataFileFactory
    .class.getName();

    private static String foundFactory = null;

    /**
     * returns an new instance of the factory
     * 
     * @author wohlgemuth
     * @version Nov 9, 2005
     * @return
     */
    public static DataFileFactory newInstance() {

        // Locate Factory
        foundFactory = findFactory(
                DEFAULT_PROPERTY_NAME,
                SimpleDataFileFactory.class.getName());

        return newInstance(foundFactory);
    }


    /**
     * returns an new instance of the factory
     * 
     * @author wohlgemuth
     * @version Nov 9, 2005
     * @return
     */
    public static DataFileFactory newInstance(String factoryClass) {
        Class<?> classObject;
        DataFileFactory factory;

        try {
            classObject = Class.forName(factoryClass);
            factory = (DataFileFactory) classObject.newInstance();
            return factory;

        } catch (Exception e) {
            throw new FactoryException(e);
        }
    }
    
    public abstract DataFile createDataFile();

}
