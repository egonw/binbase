/*
 * Created on 15.04.2004
 */
package edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.plot;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;

import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet;

/**
 * @author wohlgemuth erstellt einen chart welcher die werte der analyse zur?ck
 *         gibt
 */
public final class RiCorrectionAnalysisChart {
	/**
	 * Creates a new RiCorrectionAnalysisChart object.
	 */
	private RiCorrectionAnalysisChart() {
	}

	/**
	 * erstellt den chart und gibt ihn zur?ck
	 * 
	 * @return
	 */
	public static JFreeChart createChart(BinBaseXYDataSet dataShift,
			BinBaseXYDataSet border, String sampleName) {
		XYBarRenderer rendererShift = new XYBarRenderer(0.5);
		XYItemRenderer rendererOutline = new StandardXYItemRenderer();

		NumberAxis x = new NumberAxis("retention index");
		ValueAxis y = new NumberAxis("shift");
		XYPlot plot = new XYPlot(dataShift, x, y, rendererShift);
		plot.setDataset(1, border);
		plot.setRenderer(1, rendererOutline);

		y.setRange(0, border.getYValue(0, 0) * 2);
		x.setAutoRangeIncludesZero(false);

		JFreeChart chart = new JFreeChart(
				"retention index correction for sample: " + sampleName,
				plot);

		return chart;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param args
	 *            DOCUMENT ME!
	 * 
	 * @throws Exception
	 *             DOCUMENT ME!
	 */
	public static void main(String[] args) throws Exception {
		BinBaseXYDataSet dataset = new BinBaseXYDataSet();
		BinBaseXYDataSet dataset2 = new BinBaseXYDataSet();

		dataset.addDataSet(new double[] { 10, 20, 30, 40, 50 }, new double[] {
				1, 2, 8, 2, 1 }, "ABS(shift[i] - mean)/s");
		dataset2.addDataSet(new double[] { 10, 20, 30, 40, 50 }, new double[] {
				2, 2, 2, 2, 2 }, "top border");
		dataset2.addDataSet(new double[] { 10, 20, 30, 40, 50 }, new double[] {
				-2, -2, -2, -2, -2 }, "bottom border");

		JFreeChart chart = createChart(dataset, dataset2, "test");
		ChartPanel panel = new ChartPanel(chart);
		JFrame frame = new JFrame();
		frame.setSize(800, 600);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setVisible(true);
	}
}
