/*
 * Created on Sep 10, 2003
 *
 */
package edu.ucdavis.genomics.metabolomics.util.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author wohlgemuth
 * @version Sep 10, 2003 <br>
 *          BinBaseDatabase
 * @description Beschreibt eine Connectionfactory welche dazu dient connections
 *              bereitzustellen
 */
public abstract class ConnectionFactory {

	/**
	 * DOCUMENT ME!
	 */
	public static final String KEY_USERNAME_PROPERTIE = "Binbase.user";

	/**
	 * DOCUMENT ME!
	 */
	public static final String KEY_PASSWORD_PROPERTIE = "Binbase.password";

	/**
	 * DOCUMENT ME!
	 */
	public static final String KEY_DATABASE_PROPERTIE = "Binbase.database";

	/**
	 * DOCUMENT ME!
	 */
	public static final String KEY_DRIVER_PROPERTIE = "Binbase.driver";

	/**
	 * DOCUMENT ME!
	 */
	public static final String KEY_HOST_PROPERTIE = "Binbase.host";

	/**
	 * DOCUMENT ME!
	 */
	public static final String KEY_TYPE_PROPERTIE = "Binbase.type";
	/**
	 * DOCUMENT ME!
	 * 
	 * @uml.property name="instance"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private static ConnectionFactory instance = null;

	/**
	 * DOCUMENT ME!
	 */
	protected Logger logger = Logger.getLogger(getClass());

	/**
	 * DOCUMENT ME!
	 */
	private Properties properties;

	/**
	 * DOCUMENT ME!
	 */
	private boolean force = false;

	/**
	 * @author wohlgemuth
	 * @version Sep 10, 2003 <br>
	 */
	protected ConnectionFactory() {
		super();
	}

	/**
	 * destroyes the factory, needed to create new factorys
	 * 
	 * @author wohlgemuth
	 * @version Jun 4, 2006
	 * @throws SQLException
	 */
	public static synchronized void destroyFactory() throws SQLException {
		instance = null;
	}

	/**
	 * gibt eine Factory zur?ck
	 * 
	 * @version Sep 10, 2003
	 * @author wohlgemuth <br>
	 * @return
	 * @uml.property name="factory"
	 */
	public static synchronized ConnectionFactory getFactory() {
		try {
			return getFactory(edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory.class.getName());
		}
		catch (InstantiationException e) {
			throw new RuntimeException(e);
		}
		catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static synchronized ConnectionFactory createFactory() {
		try {
			return createFactory(edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory.class.getName());
		}
		catch (InstantiationException e) {
			throw new RuntimeException(e);
		}
		catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static ConnectionFactory createFactory(String classname) throws InstantiationException, IllegalAccessException, ClassNotFoundException {

		return (ConnectionFactory) Class.forName(classname).newInstance();
	}

	/**
	 * gibt eine spezifische factory zur?ck
	 * 
	 * @version Sep 10, 2003
	 * @author wohlgemuth <br>
	 * @param classname
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static ConnectionFactory getFactory(String classname) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (instance == null) {
			instance = (ConnectionFactory) Class.forName(classname).newInstance();
		}

		return instance;
	}

	/**
	 * @version Sep 10, 2003
	 * @author wohlgemuth <br>
	 * @return
	 */
	public final Connection getConnection() {
		return getConnection(true);
	}

	/**
	 * gibt eine neue connection zur?ck und erzwingt das sie aus einen eventuell
	 * vorhandenen pool ermittelt wird diese method muss jedoch nicht zwingend
	 * einen pool verwenden
	 * 
	 * @version Oct, 13 2004
	 * @author wohlgemuth <br>
	 * @return
	 */
	public abstract Connection getConnection(boolean pooled);

	/**
	 * @param properties
	 *            The properties to set.
	 * @uml.property name="properties"
	 */
	public final void setProperties(Properties properties) {
		this.properties = DriverUtilities.createConnectionProperties(properties);
	}

	/**
	 * @return Returns the properties.
	 * @uml.property name="properties"
	 */
	public final Properties getProperties() {
		return properties;
	}

	/**
	 * schliesst eine connection
	 * 
	 * @throws Exception
	 */
	public abstract void close() throws Exception;

	public abstract boolean hasConnection();

	/**
	 * schliesst eine verbindung oder schiebt sie in den pool zur?ck
	 * 
	 * @param connection
	 * @throws SQLException
	 */
	public abstract void close(Connection connection) throws SQLException;

	/**
	 * @param force
	 *            The force to set.
	 * @uml.property name="force"
	 */
	public void setForce(boolean force) {
		this.force = force;
	}

	/**
	 * @return Returns the force.
	 * @uml.property name="force"
	 */
	public boolean isForce() {
		return force;
	}

	/**
	 * DOCUMENT ME!
	 */
	public void initLogging() {
		// TODO Auto-generated method stub
	}

	/**
	 * DOCUMENT ME!
	 */
	public void updateConfig() {
	}
}
