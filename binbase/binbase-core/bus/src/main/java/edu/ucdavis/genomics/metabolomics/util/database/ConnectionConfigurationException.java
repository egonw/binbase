package edu.ucdavis.genomics.metabolomics.util.database;

public class ConnectionConfigurationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public ConnectionConfigurationException() {
		super();
		
	}

	public ConnectionConfigurationException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public ConnectionConfigurationException(String message) {
		super(message);
		
	}

	public ConnectionConfigurationException(Throwable cause) {
		super(cause);
		
	}

}
