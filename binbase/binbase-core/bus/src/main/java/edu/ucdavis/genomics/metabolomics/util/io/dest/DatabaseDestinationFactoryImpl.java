/*
 * Created on Nov 10, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.io.dest;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination;
import edu.ucdavis.genomics.metabolomics.util.io.dest.DestinationFactory;

/**
 * implementation to acutally create a database destination
 * @author wohlgemuth
 * @version Nov 10, 2005
 *
 */
public class DatabaseDestinationFactoryImpl extends DestinationFactory{

    /**
     * @author wohlgemuth
     * @version Nov 10, 2005
     */
    public DatabaseDestinationFactoryImpl() {
        super();
    }

    /**
     * creates a database based implementations
     * @author wohlgemuth
     * @version Nov 10, 2005
     * @see edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.dest.DestinationFactory#createDestination(java.lang.Object, java.util.Map)
     */
    public DatabaseDestination createDestination(Object identifier, Map propertys) throws ConfigurationException {
    	DatabaseDestination destination = new DatabaseDestination();
        destination.configure(propertys);
        destination.setIdentifier(identifier);
        
        return destination;
    }

}
