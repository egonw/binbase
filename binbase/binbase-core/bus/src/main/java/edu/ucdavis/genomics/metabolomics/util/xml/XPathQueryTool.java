/*
 * Created on Sep 2, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.xml;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;

import org.jdom.Document;
import org.jdom.Element;

import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import org.jdom.xpath.XPath;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import java.util.List;


/**
 * simple tool to query xml files using xpath
 * @author wohlgemuth
 *
 */
public class XPathQueryTool {
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Document d = XmlHandling.readDocument(new File(args[0]));
        XMLOutputter out = new XMLOutputter(Format.getPrettyFormat());

        while (true) {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                            System.in));
                System.out.println("query:");

                XPath samples = XPath.newInstance(in.readLine());
                List data = samples.selectNodes(d);

                for (int i = 0; i < data.size(); i++) {
                    out.output((Element) data.get(i), System.out);
                    System.out.println();
                }

                System.out.println("size:" + data.size());
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
