package edu.ucdavis.genomics.metabolomics.util.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.ForwardOnlyResultSetTableFactory;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.xml.XmlDataSet;

import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination;

/**
 * exports the complete database to an xml file
 * 
 * @author wohlgemuth
 */
public class ExtractDataFromDatabase {

	private Map<String, String> limited = new HashMap<String, String>();

	private Logger logger = Logger.getLogger(getClass());

	public void extractData(File out) throws DataSetException, FileNotFoundException, SQLException, IOException, ClassNotFoundException {
		extractData(new FileOutputStream(out));
	}

	public void extractData(String out) throws DataSetException, FileNotFoundException, SQLException, IOException, ClassNotFoundException {
		extractData(new FileOutputStream(out));
	}

	public void extractData(Destination out) throws DataSetException, FileNotFoundException, SQLException, IOException, ClassNotFoundException {
		extractData(out.getOutputStream());
	}

	public void extractData(OutputStream out) throws DataSetException, SQLException, IOException, ClassNotFoundException {
		ConnectionFactory factory = ConnectionFactory.getFactory();
		factory.setProperties(System.getProperties());

		Connection connection = factory.getConnection();
		try {
			extractData(out, connection);
		}
		finally {
			factory.close(connection);
		}
	}

	/**
	 * extract the data using the given connection
	 * 
	 * @param out
	 * @param connection
	 * @param tables
	 * @throws DataSetException
	 * @throws SQLException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void extractData(OutputStream out, Connection connection, String... tables) throws DataSetException, SQLException, IOException,
			ClassNotFoundException {
		IDatabaseConnection dbunit = null;
		if (System.getProperty("Binbase.user") != null) {
			logger.info("using schema");
			dbunit = new DatabaseConnection(connection, System.getProperty("Binbase.user").toUpperCase());
		}
		else {
			logger.info("using no schema");
			dbunit = new DatabaseConnection(connection);

		}
		dbunit.getConfig().setFeature(DatabaseConfig.FEATURE_BATCHED_STATEMENTS, true);
		dbunit.getConfig().setProperty(DatabaseConfig.PROPERTY_RESULTSET_TABLE_FACTORY, new ForwardOnlyResultSetTableFactory());
		dbunit.getConfig().setProperty(DatabaseConfig.PROPERTY_ESCAPE_PATTERN, "\"?\"");

		dbunit.getConfig().setFeature("http://www.dbunit.org/features/skipOracleRecycleBinTables", true);
		
		if (System.getProperty("Binbase.user") != null) {

			QueryDataSet dataset = new QueryDataSet(dbunit);

			if (tables != null) {
				if (tables.length > 0) {
					for (String table : tables) {
						logger.info("adding table: " + table);
						dataset.addTable(table, "SELECT * FROM " + System.getProperty("Binbase.user") + "." + table);
					}

					XmlDataSet.write(dataset, out);
				}
				else {
					logger.info("filling dataset from all tables");
					dataset = fillDataSet(dataset, connection);
					XmlDataSet.write(dataset, out);
				}
			}
			else {
				logger.info("filling dataset from all tables");

				dataset = fillDataSet(dataset, connection);
				XmlDataSet.write(dataset, out);
			}
		}
		else {
			XmlDataSet.write(dbunit.createDataSet(), out);
		}

	}

	public void extractData(OutputStream out, Connection connection) throws DataSetException, SQLException, IOException, ClassNotFoundException {
		extractData(out, connection, new String[]{});
	}

	/**
	 * gets the data for our schema
	 * 
	 * @param dataset
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	protected QueryDataSet fillDataSet(QueryDataSet dataset, Connection connection) throws SQLException {
		logger.info("try get metadata from connection: " + connection);
		DatabaseMetaData data = connection.getMetaData();
		logger.info("working on: " + data.getDriverName());

		ResultSet result = null;
		logger.info("browsing tables...");
		result = data.getTables(null, System.getProperty("Binbase.user").toUpperCase(), "%", new String[] { "TABLE" });
		while (result.next()) {
			String table = result.getString("TABLE_NAME").toLowerCase();
			String query = this.getCustomQuery(table);

			logger.info("adding table: " + table);
			logger.info("current table: " + table);
			if (query == null) {
				dataset.addTable(table, "SELECT * FROM " + table);
			}
			else {
				logger.info("adding custom query: " + query);
				dataset.addTable(table, query);
			}
		}

		return dataset;
	}

	public void addCustomQueries(String table, String query) {
		this.limited.put(table, query);
	}

	public String getCustomQuery(String table) {
		return this.limited.get(table);
	}

	public void removeCustomQueries() {
		this.limited.clear();
	}
}
