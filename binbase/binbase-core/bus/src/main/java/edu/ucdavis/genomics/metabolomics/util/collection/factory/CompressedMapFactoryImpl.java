package edu.ucdavis.genomics.metabolomics.util.collection.factory;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.util.collection.CompressedMap;

public class CompressedMapFactoryImpl<K,V> extends MapFactory<K, V>{

	@Override
	public Map<K, V> createMap() {
		return new CompressedMap<K,V>();
	}

}
