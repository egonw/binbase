/*
 * Created on 06.04.2004
 */
package edu.ucdavis.genomics.metabolomics.util.graph.dataset;

import com.keypoint.PngEncoder;

import org.jfree.chart.JFreeChart;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;


/**
 * @author wohlgemuth
 */
public class GraphUtil {
    /**
     * schreibt einen chart als png
     * @param file
     * @param chart
     * @param width
     * @param height
     * @throws Exception
     */
    public static void writeChartAsPNG(File file, JFreeChart chart, int width,
        int height) throws Exception {
        BufferedImage image = chart.createBufferedImage(width, height);
        OutputStream out = new FileOutputStream(file);
        PngEncoder encoder = new PngEncoder(image);
        out.write(encoder.pngEncode());
        out.flush();
        out.close();
    }

    /**
     * schreibt einen chart als png
     * @param file
     * @param chart
     * @param width
     * @param height
     * @throws Exception
     */
    public static void writeChartAsPNG(File file, JFreeChart chart)
        throws Exception {
        writeChartAsPNG(file, chart, 800, 600);
    }
}
