package edu.ucdavis.genomics.metabolomics.util.statistics.data;

import java.io.FileInputStream;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Semaphore;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.statistics.replacement.ReplaceWithMin;
import edu.ucdavis.genomics.metabolomics.util.statistics.replacement.ZeroReplaceable;

public class ThreadedDataFileTest extends SimpleDatafileTest {

	public ThreadedDataFileTest(final String arg0) {
		super(arg0);
	}

	@Override
	protected SimpleDatafile createTestFile() {
		return new ThreadedDataFile();
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void testReplacementBigTest() throws Exception {
		final Semaphore sem = new Semaphore(2);

		final ThreadedDataFile file = new ThreadedDataFile();
		file.read(new FileInputStream(("src/test/resources/test/221.txt")));
		file.setIgnoreRows(new int[] { 0, 1, 2, 3 });
		file.setIgnoreColumns(new int[] { 0, 1 });

		file.replaceZeros(new ZeroReplaceable() {

			private static final long serialVersionUID = 1L;

			@Override
			public List replaceZeros(List list) {
				try {
					sem.acquire();
					Thread.currentThread().sleep(500);
					sem.release();
				}
				catch (Exception e) {

				}
				return list;

			}

		}, true);

	}

	/**
	 * test to test the zeroreplacement over the hole datafile with no grouping
	 * 
	 * @author wohlgemuth
	 * @version Nov 21, 2005
	 */
	public void testReplaceSmallTest() throws Exception {

		// a simple datfile for the test
		final ThreadedDataFile file = new ThreadedDataFile();
		{
			final List data = new Vector();
			data.add("A"); // 0
			data.add("B"); // 1
			data.add("C"); // 2
			data.add("D"); // 3
			data.add("E"); // 4
			data.add("F"); // 5
			file.addRow(data);
		}
		{
			final List data = new Vector();
			data.add("B"); // 0
			data.add("1"); // 1
			data.add("1"); // 2
			data.add("X"); // 3
			data.add("2"); // 4
			data.add("2"); // 5
			file.addRow(data);
		}
		{
			final List data = new Vector();
			data.add("C"); // 0
			data.add("1"); // 1
			data.add("3"); // 2
			data.add("X"); // 3
			data.add("3"); // 4
			data.add("2"); // 5
			file.addRow(data);
		}
		{
			final List data = new Vector();
			data.add("D"); // 0
			data.add("X"); // 1
			data.add("X"); // 2
			data.add("X"); // 3
			data.add("X"); // 4
			data.add("X"); // 5
			file.addRow(data);
		}
		{
			final List data = new Vector();
			data.add("E"); // 0
			data.add("2"); // 1
			data.add("4"); // 2
			data.add("X"); // 3
			data.add("4"); // 4
			data.add("0"); // 5
			file.addRow(data);
		}

		{
			final List data = new Vector();
			data.add("F"); // 0
			data.add("2"); // 1
			data.add("0"); // 2
			data.add("X"); // 3
			data.add("1"); // 4
			data.add("2"); // 5
			file.addRow(data);
		}

		System.out.println("before...");
		file.print(System.out);

		file.setIgnoreColumns(new int[] { 0, 3 });
		file.setIgnoreRows(new int[] { 0, 3 });

		file.replaceZeros(new ReplaceWithMin());
		System.out.println("after...");
		file.print(System.out);

		List column = file.getColumn(0);

		column = file.getColumn(2);
		assertTrue(Double.parseDouble(column.get(5).toString()) == 1);

		column = file.getColumn(5);
		assertTrue(Double.parseDouble(column.get(4).toString()) == 2);

	}

	@Override
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

}
