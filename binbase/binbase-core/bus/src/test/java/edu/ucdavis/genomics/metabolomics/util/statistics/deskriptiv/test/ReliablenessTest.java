/*
 * Created on 03.08.2004
 */
package edu.ucdavis.genomics.metabolomics.util.statistics.deskriptiv.test;

import edu.ucdavis.genomics.metabolomics.util.statistics.deskriptiv.Reliableness;

import junit.framework.TestCase;

import java.util.List;
import java.util.Vector;


/**
 * @author wohlgemuth
 *
 */
public class ReliablenessTest extends TestCase {
    /**
     * @param arg0
     */
    public ReliablenessTest(String arg0) {
        super(arg0);
    }

    /**
     * DOCUMENT ME!
     */
    public void testCalculate() {
        List data = new Vector();
        data.add("1");
        data.add("2");
        data.add("3");
        data.add("4");
        data.add("0");

        Reliableness v = new Reliableness();
        System.err.println(v.calculate(data));
        assertTrue(Math.abs(v.calculate(data) - 0.8) < 0.0001);

        data.clear();
        data.add("0");
        data.add("0");
        data.add("0");
        data.add("0");
        data.add("0");
        System.err.println(v.calculate(data));
        assertTrue(Math.abs(v.calculate(data) - 0) < 0.0001);

        data.clear();
        data.add("1");
        data.add("1");
        data.add("1");
        data.add("1");
        data.add("1");
        System.err.println(v.calculate(data));
        assertTrue(Math.abs(v.calculate(data) - 1) < 0.0001);
    }
}
