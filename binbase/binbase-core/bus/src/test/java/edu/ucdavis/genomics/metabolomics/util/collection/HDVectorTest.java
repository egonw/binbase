package edu.ucdavis.genomics.metabolomics.util.collection;

import java.util.List;

import junit.framework.TestCase;

public class HDVectorTest extends TestCase {
	HDVector list;
	
	int size = 100;
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		list = new HDVector();
		
		for(int i = 0; i < size; i++){
			list.add(String.valueOf(i));
		}
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testSize() {
		assertTrue(list.size() == size);
	}

	public void testGetInt() {
		for(int i = 0; i < size; i++){
			assertTrue(list.get(i).equals(String.valueOf(i)));
		}
	}

	public void testSetObjectInt() {
		for(int i = 0; i < size; i++){
			list.set(i, String.valueOf((size-i)));
		}
		for(int i = 0; i < size; i++){
			assertTrue(list.get(i).equals(String.valueOf(size - i)));
		}
	}

	public void testRemoveInt() {
		boolean second = false;
		for(int i = 0; i < list.size(); i++){
			if(second){
				list.remove(i);
				second = false;
			}
			else{
				second = true;
			}
		}

		for(int i = 0; i < list.size() ; i++){
			System.out.println(list.get(i) + " - " + i);
		}
	}
}
