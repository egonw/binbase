package edu.ucdavis.genomics.metabolomics.util.database;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.PropertySetter;

public class SimpleConnectionFactoryTest {

	private Logger logger = Logger.getLogger(getClass());

	@Test
	public void testCreateConnections() throws InstantiationException, IllegalAccessException, ClassNotFoundException, FileNotFoundException, IOException,
			SQLException {
		@SuppressWarnings("unused")
		Properties p = PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");

		ConnectionFactory fact = ConnectionFactory.getFactory(SimpleConnectionFactory.class.getName());
		fact.setProperties(p);

		Connection c = fact.getConnection();

		assertTrue(c.isClosed() == false);

		// should close the connection for us
		fact.close(c);

		// should be a new connection now
		Connection c2 = fact.getConnection();
		assertTrue(c2.isClosed() == false);

		// should not equal the old connection
		assertTrue(c2.equals(c) == false);
		// closing connection
		fact.close(c2);
	}

	@Test
	public void testTransactions() throws Exception {
		Properties p = PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");

		// create 3 factories
		ConnectionFactory a = new SimpleConnectionFactory();
		ConnectionFactory b = new SimpleConnectionFactory();
		ConnectionFactory c = new SimpleConnectionFactory();

		a.setProperties(p);
		b.setProperties(p);
		c.setProperties(p);

		// get 3 connections
		Connection a1 = a.getConnection();
		Connection b1 = b.getConnection();
		Connection c1 = c.getConnection();

		try {
			// disable auto commit
			a1.setAutoCommit(false);
			b1.setAutoCommit(false);
			c1.setAutoCommit(false);

			// create a test table
			createTestTable(c1);

			insertTestData(a1);
			insertTestData(b1);
			insertTestData(c1);

			logger.info("notthing commited -  connection should read now 100 rows");
			assertTrue(getCount(a1) == 100);
			assertTrue(getCount(b1) == 100);
			assertTrue(getCount(c1) == 100);

			a1.commit();
			logger.info("first commit is done");
			getCount(a1);
			getCount(b1);
			getCount(c1);

			b1.commit();
			logger.info("second commit is done");

			getCount(a1);
			getCount(b1);
			getCount(c1);

			c1.commit();
			logger.info("third commit is done");

			// should be 300
			assertTrue(getCount(a1) == 300);
			assertTrue(getCount(b1) == 300);
			assertTrue(getCount(c1) == 300);

			a1.commit();
			b1.commit();
			c1.commit();

			// all connections should be done by now so we can drop the table
			dropTestTable(a1);

			// close all connections
		}
		finally {
			try {
				a.close();
			}
			catch (Exception e) {
				logger.error(e);
			}
			try {
				b.close();
			}
			catch (Exception e) {
				logger.error(e);
			}
			try {
				c.close();
			}
			catch (Exception e) {
				logger.error(e);
			}
		}

	}

	private void insertTestData(Connection c1) throws SQLException {
		logger.info("insert data...");

		// insert some data
		PreparedStatement s = c1.prepareStatement("INSERT INTO TEST VALUES(?,?)");

		for (int x = 0; x < 100; x++) {

			s.setFloat(1, x);
			s.setString(2, "value:" + x);
			s.execute();
		}
		s.close();
	}

	private int getCount(Connection c) throws SQLException {
		Statement s = c.createStatement();
		ResultSet set = s.executeQuery("select count(*) from TEST");
		set.next();
		try {
			int result = set.getInt(1);
			logger.info("count is: " + result);
			return set.getInt(1);
		}
		finally {
			set.close();
			s.close();
		}
	}

	private void dropTestTable(Connection c1) throws Exception {
		logger.info("drop table...");
		DatabaseUtilities.execCommand(c1, "DROP TABLE TEST");
	}

	private void createTestTable(Connection c1) throws Exception {
		logger.info("create a table");

		try {
			DatabaseUtilities.execCommand(c1, "DROP TABLE TEST");
		}
		catch (Exception e) {
			// simple cleanup we dont care about the result
		}
		// create a table
		DatabaseUtilities.execCommand(c1, "CREATE TABLE TEST( id float, name varchar(200))");
	}

	@Test
	public void testLoad() throws Exception {
		Properties p = PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");

		// should attempt to open and close 100 connections
		for (int i = 0; i < 100; i++) {
			logger.info("create factory...");
			ConnectionFactory fact = ConnectionFactory.getFactory(SimpleConnectionFactory.class.getName());
			fact.setProperties(p);

			logger.info("create connection...");

			Connection c = fact.getConnection();

			try {
				assertTrue(c.isClosed() == false);

				logger.info("create a query...");

				// start a transaction
				c.setAutoCommit(false);
				c.commit();
				PreparedStatement s = c.prepareStatement("select * from pg_tables");
				s.execute();
				s.close();

				c.commit();

				createTestTable(c);

				logger.info("insert data...");

				// insert some data
				s = c.prepareStatement("INSERT INTO TEST VALUES(?,?)");

				for (int x = 0; x < 100; x++) {

					s.setFloat(1, i);
					s.setString(2, "value:" + i);
					s.execute();
				}
				s.close();

				s = c.prepareStatement("select count(*) from TEST");

				logger.info("check result...");

				// should be 0 since the transacion is not commited yet
				ResultSet next = s.executeQuery();
				next.next();
				int result = next.getInt(1);
				logger.info("got result: " + result);
				logger.info("exspected result: " + result);

				assertTrue(result == 100);
				next.close();

				s.close();
				// make sure that they are not entered yet

				dropTestTable(c);

			}
			finally {
				logger.info("close factory...");

				// should close the connection for us
				fact.close(c);

			}
		}
	}

	@Test
	public void testHasConnection() throws Exception {
		Properties p = PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");

		ConnectionFactory fact = ConnectionFactory.getFactory(SimpleConnectionFactory.class.getName());
		fact.setProperties(p);

		Connection c = fact.getConnection();

		assertTrue(c.isClosed() == false);

		assertTrue(fact.hasConnection());
		
		// should close the connection for us
		fact.close(c);

		assertFalse(fact.hasConnection());
		
	}

}
