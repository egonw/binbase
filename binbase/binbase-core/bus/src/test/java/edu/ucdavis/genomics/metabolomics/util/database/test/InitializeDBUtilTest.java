package edu.ucdavis.genomics.metabolomics.util.database.test;

import static org.junit.Assert.assertTrue;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.PropertySetter;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

/**
 * tests the initialisation for the database
 * 
 * @author wohlgemuth
 * 
 */
public class InitializeDBUtilTest {

	@Test
	public void testInitialize() throws Exception {
		PropertySetter
				.setPropertiesToSystem("src/test/resources/test.properties");
		String username = System.getProperty("Binbase.user");
		String password = System.getProperty("Binbase.password");
		String host = System.getProperty("Binbase.host");
		String database = System.getProperty("Binbase.database");

		IDataSet set = getDataSet();

		// initialize the database
		InitializeDBUtil.initialize(username, password, host, database,set);

		// validate the database
	}

	protected IDataSet getDataSet() throws Exception {
		try {
			ResourceSource source = new ResourceSource("/minimum-rtx5.xml");
			assertTrue(source.exist());
			return new XmlDataSet(source.getStream());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
