/*
 * Created on Sep 29, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.database;

import java.util.Properties;

import junit.framework.TestCase;

/**
 * DOCUMENT ME!
 * 
 * @author $author$
 * @version $Revision: 1.1 $
 */
public class DriverUtilitiesTest extends TestCase {
	/**
	 * DOCUMENT ME!
	 */
	private Properties p = new Properties();

	/**
	 * DOCUMENT ME!
	 * 
	 * @param args
	 *            DOCUMENT ME!
	 */
	public static void main(String[] args) {
		junit.textui.TestRunner.run(DriverUtilitiesTest.class);
	}

	/*
	 * Test method for
	 * 'edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities.createConnectionProperties(Properties)'
	 */
	public void testCreateOracleConnectionPropertiesProperties() {
		p.setProperty("Binbase.type", "2");
		Properties toTest = DriverUtilities.createConnectionProperties(p);
		assertTrue(toTest.getProperty("Binbase.user").equals("test"));
		assertTrue(toTest.getProperty("Binbase.host").equals("test"));
		assertTrue(toTest.getProperty("Binbase.password").equals("test"));
		assertTrue(toTest.getProperty("Binbase.database").equals("test"));
		assertTrue(toTest.getProperty("Binbase.type").equals("2"));

		assertTrue(toTest.getProperty("hibernate.dialect").equals("org.hibernate.dialect.Oracle9Dialect"));
		assertTrue(toTest.getProperty("hibernate.connection.username").equals("test"));
		assertTrue(toTest.getProperty("hibernate.connection.password").equals("test"));
		assertTrue(toTest.getProperty("hibernate.connection.driver_class").equals(DriverUtilities.ORACLE_DRIVER));
		assertTrue(toTest.getProperty("hibernate.connection.url").equals("jdbc:oracle:thin:@test:1521:test"));
	}

	public void testCreatePostgresConnectionPropertiesProperties() {
		p.setProperty("Binbase.type", "3");
		Properties toTest = DriverUtilities.createConnectionProperties(p);
		assertTrue(toTest.getProperty("Binbase.user").equals("test"));
		assertTrue(toTest.getProperty("Binbase.host").equals("test"));
		assertTrue(toTest.getProperty("Binbase.password").equals("test"));
		assertTrue(toTest.getProperty("Binbase.database").equals("test"));
		assertTrue(toTest.getProperty("Binbase.type").equals("3"));

		assertTrue(toTest.getProperty("hibernate.dialect").equals("org.hibernate.dialect.PostgreSQLDialect"));
		assertTrue(toTest.getProperty("hibernate.connection.username").equals("test"));
		assertTrue(toTest.getProperty("hibernate.connection.password").equals("test"));
		assertTrue(toTest.getProperty("hibernate.connection.driver_class").equals(DriverUtilities.POSTGRES_DRIVER));
		assertTrue(toTest.getProperty("hibernate.connection.url").equals("jdbc:postgresql://test/test"));
	}

	/*
	 * Test method for
	 * 'edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities.generateOracleUrl(String,
	 * String)'
	 */
	public void testGenerateOracleUrl() {
		String url = DriverUtilities.generateOracleUrl("localhost", "test");
		assertTrue(url.trim().equals("jdbc:oracle:thin:@localhost:1521:test"));
	}
	public void testGeneratePostgresUrl() {
		String url = DriverUtilities.generatePostgresUrl("localhost", "test");
		assertTrue(url.trim().equals("jdbc:postgresql://localhost/test"));
	}

	/*
	 * Test method for
	 * 'edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities.generateUrl(String,
	 * String, int)'
	 */
	public void testGenerateUrl() {
		String url = DriverUtilities.generateUrl("localhost", "test", DriverUtilities.ORACLE);
		assertTrue(url.equals("jdbc:oracle:thin:@localhost:1521:test"));

		url = DriverUtilities.generateUrl("localhost", "test", DriverUtilities.UNKNOWN);
		assertNull(url);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @throws Exception
	 *             DOCUMENT ME!
	 */
	protected void setUp() throws Exception {
		super.setUp();
		p.setProperty("Binbase.user", "test");
		p.setProperty("Binbase.host", "test");
		p.setProperty("Binbase.password", "test");
		p.setProperty("Binbase.database", "test");

	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @throws Exception
	 *             DOCUMENT ME!
	 */
	protected void tearDown() throws Exception {
		p = null;
	}
}
