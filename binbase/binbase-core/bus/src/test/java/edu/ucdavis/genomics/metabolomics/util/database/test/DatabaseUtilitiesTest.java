/*
 * Created on Oct 29, 2003
 */
package edu.ucdavis.genomics.metabolomics.util.database.test;

import java.sql.Connection;
import java.sql.ResultSet;

import junit.framework.AssertionFailedError;
import junit.framework.TestCase;
import edu.ucdavis.genomics.metabolomics.util.PropertySetter;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.DatabaseUtilities;

/**
 * @author wohlgemuth
 * @version Sep 18, 2003 <br>
 *          BinBase
 * @description
 */
public class DatabaseUtilitiesTest extends TestCase {
	/**
	 * DOCUMENT ME!
	 */
	private Connection c;

	/**
	 * Constructor for DatabaseUtilitiesTest.
	 * 
	 * @param arg0
	 */
	public DatabaseUtilitiesTest(String arg0) {
		super(arg0);
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @throws Exception
	 *             DOCUMENT ME!
	 */
	public void testExecCommand() throws Exception {
		DatabaseUtilities.execCommand(c, "SELECT * FROM BIN");
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @throws Exception
	 *             DOCUMENT ME!
	 */
	public void testGetQueryResult() throws Exception {
		ResultSet result = DatabaseUtilities.getQueryResult(c,
				"SELECT * FROM BIN");
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @throws Exception
	 *             DOCUMENT ME!
	 */
	protected void setUp() throws Exception {
		try {
			PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");
			ConnectionFactory factory = ConnectionFactory.getFactory();
			factory.setProperties(System.getProperties());
			c = factory.getConnection();
		} catch (Throwable e) {
			throw new AssertionFailedError(
					"sorry problem during initialisation: " + e.getMessage());
		}
		super.setUp();
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @throws Exception
	 *             DOCUMENT ME!
	 */
	protected void tearDown() throws Exception {
		ConnectionFactory.getFactory().close(c);
		super.tearDown();
	}
}
