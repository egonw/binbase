/*
 * Created on Dec 13, 2004
 */
package edu.ucdavis.genomics.metabolomics.util.xml.validate.test;

import edu.ucdavis.genomics.metabolomics.util.xml.validate.XMLValidator;

import junit.framework.TestCase;


/**
 * @author wohlgemuth
 */
public class XMLValidatorTest extends TestCase {
    /*
     * Class under test for boolean isValid(String)
     */
    public static void testIsValidString() {
        assertTrue(XMLValidator.isValid("<xml></xml>"));
        assertFalse(XMLValidator.isValid("<xml>"));
    }

    /*
     * Class under test for boolean isValid(String, boolean)
     */
    public static void testIsValidStringboolean() {

        assertTrue(XMLValidator.isValid("<xml></xml>", false));
        assertFalse(XMLValidator.isValid("<xml>", false));
    }
}
