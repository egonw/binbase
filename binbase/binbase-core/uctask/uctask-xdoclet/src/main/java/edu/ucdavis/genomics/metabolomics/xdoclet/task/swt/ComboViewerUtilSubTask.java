/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.xdoclet.task.swt;

import xdoclet.TemplateSubTask;

/**
 * @author wohlgemuth
 * is used to generate checkbox table viewerutilities
 * @ant.element   display-name="Combo Viewer Util Provider" name="comboviewerutil" parent="edu.ucdavis.genomics.metabolomics.xdoclet.task.swt.SWTTask"
 */
public class ComboViewerUtilSubTask extends TemplateSubTask{
	private static String DEFAULT_TEMPLATE_FILE = "resources/swt-combo-util.xdt";
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * 
	 */
	public ComboViewerUtilSubTask() {
		setDestinationFile("{0}ComboViewerUtil.java");
		setTemplateURL(getClass().getResource(DEFAULT_TEMPLATE_FILE));
		setHavingClassTag("swt");
		setAcceptAbstractClasses(true);
		setAcceptInterfaces(true);
	}

}
