/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.xdoclet.task.swt;

import xdoclet.DocletTask;

/**
 * @author wohlgemuth
 * generates all needed classes for swt mvc patterns like model, modifiers, editors
 */
public class SWTTask extends DocletTask{

	/**
	 * 
	 */
	public SWTTask() {
		super();
		//generate cell modifier
		this.addTemplate(new CellModifierSubTask());
		
		//generate label provider
		this.addTemplate(new LabelProviderSubTask());

		//generate sorter
		this.addTemplate(new ViewerSorterSubTask());
		
		//put all together
		this.addTemplate(new SWTTableUtilSubTask());
		this.addTemplate(new CheckboxTableUtilSubTask());
		this.addTemplate(new ComboViewerUtilSubTask());

	}

}
