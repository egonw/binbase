package edu.ucdavis.genomics.metabolomics.xdoclet.task.jmx;

import xdoclet.TemplateSubTask;

/**
 * @ant.element   display-name="JMX Facade Bean Accessor" name="cellmodifier" parent="edu.ucdavis.genomics.metabolomics.xdoclet.task.jmx.JMXTask"
 * @author wohlgemuth
 *
 */
public class JMXFacadeBeanAccessorTask extends TemplateSubTask{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	/**
	 * the fild for this task
	 */
	private static String DEFAULT_TEMPLATE_FILE = "resources/jmx-facadebean-accessor.xdt";

	public JMXFacadeBeanAccessorTask(){
		setDestinationFile("{0}FacadeBeanAccessor.java");
		setHavingClassTag("jmx.mbean");
		setTemplateURL(getClass().getResource(DEFAULT_TEMPLATE_FILE));
		setAcceptAbstractClasses(true);
		setAcceptInterfaces(true);
	}
}
