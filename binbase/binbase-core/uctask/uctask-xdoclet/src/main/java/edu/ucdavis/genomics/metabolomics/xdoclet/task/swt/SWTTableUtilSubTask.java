/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.xdoclet.task.swt;

import xdoclet.TemplateSubTask;

/**
 * @author wohlgemuth
 * is used to generate table viewerutilities
 * @ant.element   display-name="Table Util Provider" name="tableutil" parent="edu.ucdavis.genomics.metabolomics.xdoclet.task.swt.SWTTask"
 */
public class SWTTableUtilSubTask extends TemplateSubTask{
	private static String DEFAULT_TEMPLATE_FILE = "resources/swt-table-util.xdt";
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * 
	 */
	public SWTTableUtilSubTask() {
		setDestinationFile("{0}TableViewerUtil.java");
		setTemplateURL(getClass().getResource(DEFAULT_TEMPLATE_FILE));
		setHavingClassTag("swt");
		setAcceptAbstractClasses(true);
		setAcceptInterfaces(true);
	}

}
