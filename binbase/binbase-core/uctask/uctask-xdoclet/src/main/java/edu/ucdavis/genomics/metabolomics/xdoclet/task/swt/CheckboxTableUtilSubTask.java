/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.xdoclet.task.swt;

import xdoclet.TemplateSubTask;

/**
 * @author wohlgemuth
 * is used to generate checkbox table viewerutilities
 * @ant.element   display-name="CheckBox Table Util Provider" name="checkboxtableutil" parent="edu.ucdavis.genomics.metabolomics.xdoclet.task.swt.SWTTask"
 */
public class CheckboxTableUtilSubTask extends TemplateSubTask{
	private static String DEFAULT_TEMPLATE_FILE = "resources/swt-checkbox-table-util.xdt";
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * 
	 */
	public CheckboxTableUtilSubTask() {
		setDestinationFile("{0}CheckboxTableViewerUtil.java");
		setTemplateURL(getClass().getResource(DEFAULT_TEMPLATE_FILE));
		setHavingClassTag("swt");
		setAcceptAbstractClasses(true);
		setAcceptInterfaces(true);
	}

}
