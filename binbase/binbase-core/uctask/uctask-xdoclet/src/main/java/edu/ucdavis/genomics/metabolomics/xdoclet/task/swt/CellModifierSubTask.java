/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.xdoclet.task.swt;

import xdoclet.TemplateSubTask;

/**
 * @author wohlgemuth
 * is used for label generation
 * @ant.element   display-name="Cell Modifier" name="cellmodifier" parent="edu.ucdavis.genomics.metabolomics.xdoclet.task.swt.SWTTask"
 */
public class CellModifierSubTask extends TemplateSubTask{	

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	/**
	 * the fild for this task
	 */
	private static String DEFAULT_TEMPLATE_FILE = "resources/swt-cellmodifier.xdt";

	public CellModifierSubTask(){
		setDestinationFile("{0}CellModifier.java");
		setHavingClassTag("swt");
		setTemplateURL(getClass().getResource(DEFAULT_TEMPLATE_FILE));
		setAcceptAbstractClasses(true);
		setAcceptInterfaces(true);
	}
}
