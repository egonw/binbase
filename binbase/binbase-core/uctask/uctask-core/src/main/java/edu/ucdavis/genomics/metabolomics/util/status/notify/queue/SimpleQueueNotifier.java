package edu.ucdavis.genomics.metabolomics.util.status.notify.queue;

import java.util.List;
import java.util.Vector;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.status.notify.Notifier;
import edu.ucdavis.genomics.metabolomics.util.status.notify.PriotizedNotifier;
import edu.ucdavis.genomics.metabolomics.util.status.notify.QueuedNotifier;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * a simple queue notifier which calls the notification method on each
 * registered notifier
 * 
 * @author wohlgemuth
 */
public class SimpleQueueNotifier implements QueuedNotifier {

	private static SimpleQueueNotifier instance = null;

	/**
	 * contains all our notifiers
	 */
	private final List<Notifier> notifier = new Vector<Notifier>();

	public List<Notifier> getNotifiers() {
		return notifier;
	}

	public void removeNotifier(final Notifier notifier) {
		this.notifier.remove(notifier);
	}

	public void notify(final String message) throws NotificationException {
		for (final Notifier notifier : getNotifiers()) {
			if (notifier instanceof PriotizedNotifier) {
				((PriotizedNotifier) notifier).notify(message, ((PriotizedNotifier) notifier).getDefaultPriority());
			}
			else {
				notifier.notify(message);
			}
		}
	}

	public void addNotifier(final Notifier notifier) {
		if (this.notifier.contains(notifier) == false) {
			this.notifier.add(notifier);
		}
	}

	@Override
	public Priority getDefaultPriority() {
		return Priority.INFO;
	}

	@Override
	public void notify(String message, Priority priority) throws NotificationException {

		for (final Notifier notifier : getNotifiers()) {
			if (notifier instanceof PriotizedNotifier) {
				((PriotizedNotifier) notifier).notify(message, priority);
			}
			else {
				notifier.notify(message);
			}
		}
	}

	private SimpleQueueNotifier() {
	}

	public static SimpleQueueNotifier getInstance() {
		if (instance == null) {
			instance = new SimpleQueueNotifier();
		}

		return instance;
	}
}
