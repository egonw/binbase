/*
 * Created on Apr 22, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.status;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * simple reporter based on log4j
 * 
 * @author wohlgemuth
 * @version Apr 22, 2006
 * 
 */
public final class Log4JReport extends Report {
	Logger logger = Logger.getLogger("reporting");

	@Override
	public void report(String owner,Serializable id, ReportEvent event, ReportType type, String ipaddress, Date time, Exception e) {
		if(e == null){
		logger.info(owner + " - " + id + " - " + event + " - " + type + " - " + ipaddress + " - " + time);
		}
		else {
			logger.error(owner + " - " + id + " - " + event + " - " + type + " - " + ipaddress + " - " + time,e);	
		}
	}

	@Override
	public String getOwner() {
		return "log4j";
	}

}
