package edu.ucdavis.genomics.metabolomics.util.status.notify.email;

import edu.ucdavis.genomics.metabolomics.util.status.notify.AbstractBaseNotifier;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * a simple email notifier to send emails to registered email address
 * 
 * @author wohlgemuth
 */
public abstract class EmailNotifier extends AbstractBaseNotifier {

	private EmailConfiguration configuration;
	
	public EmailConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(EmailConfiguration configuration) {
		this.configuration = configuration;
	}

	public EmailNotifier(EmailConfiguration configuration){
		this.setConfiguration(configuration);
	}
	
	public Priority getDefaultPriority() {
		return Priority.ERROR;
	}

	
}
