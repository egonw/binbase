/*
 * Created on Jan 20, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.io.source;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;

/**
 * uses a byte array as source
 * @author wohlgemuth
 * @version Jan 20, 2006
 *
 */
public class ByteArraySource implements Source{
	private byte bytes[] = null;
	
	public ByteArraySource(byte[] bytes) {
		super();
		this.bytes = bytes;
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jan 20, 2006
	 * @see edu.ucdavis.genomics.metabolomics.util.io.source.Source#getStream()
	 */
	public InputStream getStream() throws IOException {
		return new ByteArrayInputStream(this.bytes);
	}

	/**
	 * the name of the source
	 * @author wohlgemuth
	 * @version Jan 20, 2006
	 * @see edu.ucdavis.genomics.metabolomics.util.io.source.Source#getSourceName()
	 */
	public String getSourceName() {
		// TODO Auto-generated method stub
		return String.valueOf(this.bytes.hashCode());
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jan 20, 2006
	 * @see edu.ucdavis.genomics.metabolomics.util.io.source.Source#setIdentifier(java.lang.Object)
	 */
	public void setIdentifier(Object o) throws ConfigurationException {
		throw new ConfigurationException("not supported");
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jan 20, 2006
	 * @see edu.ucdavis.genomics.metabolomics.util.io.source.Source#configure(java.util.Map)
	 */
	public void configure(Map<?, ?> p) throws ConfigurationException {
		throw new ConfigurationException("not supported");
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jan 20, 2006
	 * @see edu.ucdavis.genomics.metabolomics.util.io.source.Source#exist()
	 */
	public boolean exist() {
		return bytes != null;
	}

	public long getVersion() {
		return 0;
	}

}
