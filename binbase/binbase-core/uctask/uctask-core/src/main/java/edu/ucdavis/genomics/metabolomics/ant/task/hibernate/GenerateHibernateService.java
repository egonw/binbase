/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.ant.task.hibernate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * @author wohlgemuth generate the hibernate service file needed for jboss har
 *         files
 */
public class GenerateHibernateService extends Task {

	/**
	 * the needed code
	 */
	private String code = "org.jboss.hibernate.jmx.Hibernate";

	/**
	 * what kind of service we want to use
	 */
	private String serviceName = "jboss.har:service=Hibernate";

	/**
	 * the datasource to use
	 */
	private String datasource = null;

	/**
	 * dialect, the needed dialect for the datasource
	 */
	private String dialect = null;

	/**
	 * the session factory which should be used
	 */
	private String sessionFactory = "java:/hibernate/SessionFactory";

	/**
	 * cache provider to use
	 */
	private String cacheProvider = "org.hibernate.cache.HashtableCacheProvider";

	private String dest = null;

	/**
	 * 
	 */
	public GenerateHibernateService() {
		super();
	}

	public String getCacheProvider() {
		return this.cacheProvider;
	}

	public void setCacheProvider(String cacheProvider) {
		this.cacheProvider = cacheProvider;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDatasource() {
		return this.datasource;
	}

	public void setDatasource(String datasource) {
		this.datasource = datasource;
	}

	public String getDialect() {
		return this.dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getSessionFactory() {
		return this.sessionFactory;
	}

	public void setSessionFactory(String sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void execute() throws BuildException {
		super.execute();

		if (this.getDatasource() == null) {
			throw new BuildException("datasource must be set");
		}

		if (this.getDest() == null) {
			throw new BuildException(
					"dest must be point to the destionation directory and must be set");
		}
		
		if(this.getDatasource() == null){
			throw new BuildException("datasource must be set!");
		}

		File dir = new File(this.getProject().getBaseDir()+"/"+this.getDest());

		if (dir.exists() == false) {
			throw new BuildException(this.getDest() + " doesn't exist");
		}
		if (dir.isDirectory() == false) {
			throw new BuildException(this.getDest() + " is not a directory");
		}
		File file = new File(this.getProject().getBaseDir()+"/"+this.getDest() + "/hibernate-service.xml");

		try {

			BufferedWriter writer = new BufferedWriter(new FileWriter(file));

			writer.write("<server>\n");
			writer.write("\t<mbean code=\"" + this.getCode() + "\" name=\"" + this.getServiceName() + "\">\n");
			writer.write("\t\t<attribute name=\"DatasourceName\">"+this.getDatasource()+"</attribute>\n");
			writer.write("\t\t<attribute name=\"Dialect\">"+this.getDialect()+"</attribute>\n");
			writer.write("\t\t<attribute name=\"SessionFactoryName\">"+this.getSessionFactory()+"</attribute>\n");
			writer.write("\t\t<attribute name=\"CacheProviderClass\">"+this.getCacheProvider()+"</attribute>\n");
			writer.write("\t</mbean>\n");
			writer.write("</server>\n");
			
			writer.flush();
			writer.close();

		} catch (Exception e) {
			throw new BuildException(e);
		}
	}

	public String getDest() {
		return this.dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

}
