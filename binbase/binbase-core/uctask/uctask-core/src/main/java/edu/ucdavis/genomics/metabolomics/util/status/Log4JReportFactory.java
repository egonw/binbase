/*
 * Created on Apr 22, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.status;

import java.util.Properties;

/**
 * creates log4j based reports
 * 
 * @author wohlgemuth
 * @version Apr 22, 2006
 * 
 */
public class Log4JReportFactory extends ReportFactory {
	static Log4JReport report ;

	public Log4JReportFactory() {
		super();
	}

	@Override
	public Report create(Properties p,String owner) {
		if(report == null){
			report = new Log4JReport();
		}
		return report;
	}

}
