/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.ant.task;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * @author wohlgemuth generates a report about used libs
 * generates a file with the project name and all librarys used by this document
 */
public class AntDependencyGenerator extends Task {
	/**
	 * the dir with the librarys
	 */
	private String dir;

	/**
	 * the output file
	 */
	private String outputFile;

	public void execute() throws BuildException {
		super.execute();

		if (dir == null) {
			throw new BuildException("dir cannot be null!");
		}

		File file = new File(dir);

		if (file.exists() == false) {
			throw new BuildException("dir does not exist");
		}

		if (file.isDirectory() == false) {
			throw new BuildException("dir is not a dir!");
		}

		if(this.getOutputFile() == null){
			this.setOutputFile(this.getProject().getName()+".xml");
		}
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(
					this.outputFile));
			String name = this.getProject().getName();

			writer.write("<project name=\"" + name + "\">\n");
			writeContent(writer,file);
			writer.write("</project>\n");

			writer.flush();
			writer.close();
		} catch (Exception e) {
			throw new BuildException(e);
		}
	}

	/**
	 * write the librarys down
	 * @param writer
	 * @param dir
	 * @throws IOException
	 */
	private void writeContent(BufferedWriter writer, File dir) throws IOException {
		if(dir.isDirectory()){
			File files[] = dir.listFiles();
			
			for(int i = 0; i < files.length; i++){
				
				writeContent(writer,files[i]);
			}
		}
		else{
			
			if(dir.getName().endsWith(".jar")){
				writer.write("\t<lib name=\""+dir.getName()+"\" path=\""+dir.toString()+"\"/>\n");
			}
			else if(dir.getName().endsWith(".zip")){
				writer.write("\t<lib name=\""+dir.getName()+"\" path=\""+dir.toString()+"\"/>\n");
			}
		}
	}

	/**
	 * 
	 */
	public AntDependencyGenerator() {
		super();
	}

	public String getDir() {
		return this.dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getOutputFile() {
		return this.outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

}
