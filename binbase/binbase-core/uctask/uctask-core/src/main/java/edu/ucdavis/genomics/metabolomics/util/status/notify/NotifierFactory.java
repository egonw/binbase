package edu.ucdavis.genomics.metabolomics.util.status.notify;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * is used to create a notifier for us
 * 
 * @author wohlgemuth
 */
public abstract class NotifierFactory extends AbstractFactory {

	public static final String DEFAULT_PROPERTY_NAME = NotifierFactory.class.getName();

	/**
	 * configures the factoryy
	 * 
	 * @param p
	 */
	public abstract void configure(Map<?, ?> p);

	/**
	 * creates the notifier
	 * 
	 * @return
	 */
	public abstract Notifier createNotifer();

	/**
	 * creates a new factory
	 * 
	 * @param factoryClass
	 * @return
	 * @throws NotificationException
	 */
	public static NotifierFactory newInstance(final String factoryClass) throws NotificationException {
		return newInstance(factoryClass, System.getProperties());
	}

	/**
	 * trys to find our notifier factory from the system enviorment
	 * 
	 * @return
	 * @throws NotificationException
	 */
	public static NotifierFactory newInstance() throws NotificationException {
		return newInstance(System.getProperty(DEFAULT_PROPERTY_NAME), System.getProperties());
	}

	/**
	 * does the actual initialisation
	 * 
	 * @param factoryClass
	 * @param properties
	 * @return
	 * @throws NotificationException
	 */
	public static NotifierFactory newInstance(final String factoryClass, final Map<?, ?> properties) throws NotificationException {
		if (factoryClass == null) {
			return new NotifierFactory() {

				@Override
				public Notifier createNotifer() {
					return new Log4JNotifier();
				}

				@Override
				public void configure(final Map<?, ?> p) {
				}
			};
		}

		NotifierFactory fact;
		try {
			fact = (NotifierFactory) Class.forName(factoryClass).newInstance();
			fact.configure(properties);

			return fact;

		}
		catch (final InstantiationException e) {
			throw new NotificationException(e.getMessage());
		}
		catch (final IllegalAccessException e) {

			throw new NotificationException(e.getMessage());
		}
		catch (final ClassNotFoundException e) {

			throw new NotificationException(e.getMessage());
		}
	}

}
