/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.ant.task;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author wohlgemuth 
 * this task synchronize the ant classpath with the eclipse classpath. that means the defined ant classpath overrides the eclipse ant path
 */
public class ClassPathSynchronize extends Task {

	/**
	 * the optinal classpath file we wish to update
	 */
	private String classpathFile = ".classpath";

	/**
	 * 
	 */
	public ClassPathSynchronize() {
		super();
	}

	/**
	 * generates the output file itself
	 */
	public void execute() throws BuildException {
		super.execute();

		File classPath = new File(this.getClasspathFile());

		if (classPath.exists() == false) {
			throw new BuildException(classpathFile + " doesn't exist!");
		}

		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document document = builder.parse(classPath);
			
			NodeList librarys = document.getElementsByTagName("classpathentry");
			
			int size = librarys.getLength();
			
			for(int i = 0; i < size; i++){
				Node node = librarys.item(i);				
				NamedNodeMap attributes = node.getAttributes();
				
				Node kind = attributes.getNamedItem("kind");
				String value = kind.getNodeValue().toLowerCase();
				
				if(value.equals("con")){
					//eclipse defined container ignore we don't touch this
				}
				else if(value.equals("src")){
					//src path element we want to update this
				}
				else if(value.equals("lib")){
					//librarys element we want to update this
				}
				else if(value.equals("output")){
					//output folder we want to update this
				}
			}

		} catch (Exception e) {
			throw new BuildException(e);
		}
	}

	/**
	 * return the classpath which should be updated
	 * 
	 * @return
	 */
	public String getClasspathFile() {
		return this.classpathFile;
	}

	/**
	 * sets the classpath which should be updated
	 * 
	 * @param classpathFile
	 */
	public void setClasspathFile(String classpathFile) {
		this.classpathFile = classpathFile;
	}

}
