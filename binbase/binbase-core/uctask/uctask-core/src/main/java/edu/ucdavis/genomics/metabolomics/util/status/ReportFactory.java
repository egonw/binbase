/*
 * Created on Apr 22, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.status;

import java.util.Properties;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * creates reports
 * 
 * @author wohlgemuth
 * @version Apr 22, 2006
 * 
 */
public abstract class ReportFactory extends AbstractFactory {
	public static final String DEFAULT_PROPERTY_NAME = ReportFactory.class
			.getName();

	public ReportFactory() {
		super();
	}

	/**
	 * creates an default instance
	 * 
	 * @author wohlgemuth
	 * @return
	 */
	public static ReportFactory newInstance() {
		return newInstance(findFactory(DEFAULT_PROPERTY_NAME,
				Log4JReportFactory.class.getName()));
	}

	/**
	 * returns an new instance of the factory
	 * 
	 * @author wohlgemuth
	 * @return
	 */
	public static ReportFactory newInstance(String factoryClass) {
		Class<?> classObject;
		ReportFactory factory;

		try {
			Logger.getLogger(ReportFactory.class).info(
					"initializing new report factory of type: " + factoryClass);
			classObject = Class.forName(factoryClass);
			factory = (ReportFactory) classObject.newInstance();
			return factory;

		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @param p
	 * @return
	 */
	public abstract Report create(Properties p, String owner);

	/**
	 * 
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @return
	 */
	public Report create(String owner) {
		return create(System.getProperties(), owner);
	}
}
