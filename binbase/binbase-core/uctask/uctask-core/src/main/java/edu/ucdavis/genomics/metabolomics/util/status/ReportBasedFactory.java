/*
 * Created on Jul 18, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.status;

import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * provides us with the possibilite to assign a report to each fnherit factory
 * @author wohlgemuth
 * @version Jul 18, 2006
 *
 */
public abstract class ReportBasedFactory extends AbstractFactory{
    
	private Report report;
	
	protected Report getReport() {
		return report;
	}

	protected void setReport(Report report) {
		this.report = report;
	}
	
}
