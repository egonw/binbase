package edu.ucdavis.genomics.metabolomics.util.status.notify;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;

/**
 * defines a way to notify
 * 
 * @author wohlgemuth
 */
public interface Notifier {

	/**
	 * notifis with the default priority
	 * 
	 * @param message
	 * @throws NotificationException
	 */
	public void notify(String message) throws NotificationException;

}
