package edu.ucdavis.genomics.metabolomics.util.status.notify.twitter;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.util.status.notify.AbstractBaseNotifier;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * send notification events to twitter
 * 
 * @author wohlgemuth
 */
public abstract class TwitterNotifier extends AbstractBaseNotifier {

	private Logger logger = Logger.getLogger(getClass());
	
	private TwitterConfiguration configuration;

	public TwitterConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(TwitterConfiguration configuration) {
		this.configuration = configuration;
	}

	public TwitterNotifier(TwitterConfiguration configuration){
		this.configuration = configuration;
		
		logger.debug("config: " + this.getConfiguration());
	}
	
	/**
	 * default priority shall be info
	 */
	public Priority getDefaultPriority() {
		return Priority.INFO;
	}
}
