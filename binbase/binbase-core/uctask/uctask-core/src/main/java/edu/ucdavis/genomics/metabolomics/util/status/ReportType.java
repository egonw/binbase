/*
 * Created on Apr 22, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.status;

import java.io.Serializable;

import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * what kind of report is it
 * @author wohlgemuth
 * @version Apr 22, 2006
 *
 */
public class ReportType implements Serializable{

	/**
	 * name of the type
	 */
	private String name;
	
	/**
	 * description of the tye
	 */
	private String description;

	private Serializable  attachement;

	private Priority priority;
	
	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public ReportType(String name, String description,Priority priority) {
		super();
		this.name = name;
		this.description = description;
		this.priority = priority;
	}

	public ReportType(String name, String description) {
		super();
		this.name = name;
		this.description = description;
		this.priority = Priority.TRACE;
	}
	

	public ReportType(String name, String description, Serializable attachement,Priority priority){
		super();
		this.name = name;
		this.description = description;
		this.attachement = attachement;
		this.priority = priority;

	}	

	public ReportType(String name, String description, Serializable attachement){
		super();
		this.name = name;
		this.description = description;
		this.attachement = attachement;
		this.priority = Priority.TRACE;

	}

	/**
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @param description The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name + "(" + description + " (" + attachement + "))";
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ReportType){
			return ((ReportType)obj).getName().equals(this.getName());
		}
		return false;
	}

	public Serializable getAttachement() {
		return attachement;
	}

	public void setAttachement(Serializable attachement) {
		this.attachement = attachement;
	}

}
