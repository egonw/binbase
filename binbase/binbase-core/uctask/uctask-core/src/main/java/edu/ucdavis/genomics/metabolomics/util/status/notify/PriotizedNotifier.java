package edu.ucdavis.genomics.metabolomics.util.status.notify;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * a notifier which has support for priorites
 * 
 * @author wohlgemuth
 */
public interface PriotizedNotifier extends Notifier {

	/**
	 * returns the default priority for this notifier, should be at warning or
	 * error
	 * 
	 * @return
	 */
	public Priority getDefaultPriority();

	/**
	 * executes the notification
	 * 
	 * @param message
	 */
	public void notify(String message, Priority priority) throws NotificationException;
}
