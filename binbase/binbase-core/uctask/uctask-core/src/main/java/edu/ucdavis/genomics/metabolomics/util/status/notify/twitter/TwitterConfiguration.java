package edu.ucdavis.genomics.metabolomics.util.status.notify.twitter;

/**
 * a basic twitter configuration
 * 
 * @author wohlgemuth
 */
public interface TwitterConfiguration {

	public String getUserName();

	public String getPassword();

}
