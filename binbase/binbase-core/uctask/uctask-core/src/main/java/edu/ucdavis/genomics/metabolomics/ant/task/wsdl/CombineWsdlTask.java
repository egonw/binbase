/*
 * Created on Nov 28, 2006
 */
package edu.ucdavis.genomics.metabolomics.ant.task.wsdl;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;

/**
 * combines several wsdl files to one valid wsdl file with several ports
 * 
 * @author wohlgemuth
 * @version Nov 28, 2006
 * 
 */
public class CombineWsdlTask extends Task {

	Collection<String> files = new Vector<String>();

	String output = null;

	@Override
	public void execute() throws BuildException {
		if (files.size() < 2) {
			throw new BuildException("you need to specify at least 2 files");
		}

		if (output == null) {
			throw new BuildException("you need to specify a output directory");
		}

		Iterator<String> it = files.iterator();

		//definition of the xml file
		
		while (it.hasNext()) {
			try {
				
				//elements
				Element current = XmlHandling.readXml(new File(it.next()));
				
				for(int i = 0; i < current.getChildren().size(); i++){
					Element x = (Element)((Element)current.getChildren().get(i)).detach();
					
					if(x.getName().equals("types")){
					}
				}
				
				Element types = null;
				XmlHandling.writeXml(System.out, types);

				//caclulate name of service for new messages
				
				//validate that we have no duplicated types
				
				//validate that we have no duplicated messages
				
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new BuildException(e.getMessage());
			}
		}
	}

	public void addFileName(String file) {
		this.files.add(file);
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}
	
	public static void main(String[] args) {
		CombineWsdlTask wsdl = new CombineWsdlTask();
		wsdl.addFileName("/home/wohlgemuth/auth.wsdl");
		wsdl.addFileName("/home/wohlgemuth/binbase.wsdl");
		wsdl.setOutput("/home/wohlgemuth/services.wsdl");
		wsdl.execute();
	}
}
