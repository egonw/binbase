/*
 * Created on Dec 6, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.thread.locking;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.AlreadyLockedException;
import edu.ucdavis.genomics.metabolomics.exception.LockingException;
import edu.ucdavis.genomics.metabolomics.exception.TimeoutException;
import edu.ucdavis.genomics.metabolomics.util.status.AsyncReport;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

/**
 * provides a standard to aquire objects
 * 
 * @author wohlgemuth
 * @version Dec 6, 2005
 * 
 */
public abstract class AbstractLocking implements Lockable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(getClass());
	/**
	 * defines the time of the attempt to lock the object
	 */
	public static long LOCK_TIME = 300000;

	private Report report = null;

	public AbstractLocking(String owner) {
		super();
		report = new AsyncReport(ReportFactory.newInstance().create(owner));
	}

	/**
	 * takes care of reports
	 * 
	 * @param id
	 * @param event
	 * @param type
	 */
	protected void fireReport(final Serializable id, final ReportEvent event,
			final ReportType type) {
		report.report(id, event, type);
	}

	protected void fireReport(final Serializable id, final ReportEvent event,
			final ReportType type, final Exception e) {
		report.report(id, event, type, e);
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Dec 6, 2005
	 * @throws LockingException
	 * @see edu.ucdavis.genomics.metabolomics.util.thread.locking.Lockable#aquireRessource(java.lang.Object)
	 */
	public synchronized void aquireRessource(Serializable o)
			throws LockingException {
		aquireRessource(o, LOCK_TIME);
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Dec 6, 2005
	 * @throws LockingException
	 * @see edu.ucdavis.genomics.metabolomics.util.thread.locking.Lockable#aquireRessource(java.lang.Object,
	 *      long)
	 */
	public synchronized void aquireRessource(Serializable o, long timeout)
			throws LockingException {
		fireReport(o, Reports.ATTEMPTED, Reports.LOCK);
		Date date = new Date();
		long begin = date.getTime();
		long end = begin + timeout;

		int waits = 1;
		if (lock(o) == false) {
			while (lock(o) == false) {
				try {
					logger.debug("waiting till ressource is released: " + o
							+ " time left: "
							+ (timeout - (getSleepTime() * waits)) + " ("
							+ Thread.currentThread().getId() + "/"
							+ Thread.currentThread().getName() + "/"
							+ Thread.currentThread().getThreadGroup().getName()
							+ ")");
					
					fireReport(o, Reports.WAITING, Reports.LOCK);

					Thread.sleep(getSleepTime());
					waits++;
				} catch (InterruptedException e) {
					fireReport(o, Reports.FAILED, Reports.LOCK, e);

					throw new TimeoutException(e);
				}
				date = new Date();
				if (date.getTime() >= end) {

					TimeoutException e = new TimeoutException(
							"sorry coudln't aquire ressource, operation timed out after: "
									+ timeout);

					logger.debug("timed out: " + e);

					fireReport(o, Reports.FAILED, Reports.LOCK, e);
					throw e;
				}
			}
		}
		try {
			logger.debug("locking ressource: " + o);
			fireReport(o, Reports.AQUIRED, Reports.LOCK);
			this.lock(o);
			fireReport(o, Reports.SUCCESSFUL, Reports.LOCK);

		} catch (AlreadyLockedException e) {
			logger.debug("ressource was already locked!");
			aquireRessource(o, timeout);
		}
	}

	private void fireReport(Serializable o, ReportEvent failed,
			ReportType lock, InterruptedException e) {
		// TODO Auto-generated method stub

	}

	/**
	 * lock this object
	 * 
	 * @author wohlgemuth
	 * @version Dec 6, 2005
	 * @param o
	 * @throws LockingException
	 */
	protected abstract boolean lock(Serializable o) throws LockingException;

	/**
	 * how long are we going to sleep
	 * 
	 * @author wohlgemuth
	 * @version Dec 6, 2005
	 * @see java.lang.Object#wait()
	 */
	protected abstract long getSleepTime();

	public Report getReport() {
		return report;
	}

	public final void releaseRessource(Serializable o) throws LockingException {
		doRelease(o);
		fireReport(o, Reports.RELEASED, Reports.LOCK);
	}

	protected abstract void doRelease(Serializable o) throws LockingException;
}
