/*
 * Created on Dec 6, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.thread.locking;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Vector;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.LockingException;

/**
 * provides a simple locking based on a collection
 * 
 * @author wohlgemuth
 * @version Dec 6, 2005
 */
public class SimpleLocking extends AbstractLocking {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private static SimpleLocking instance;

	private Logger logger = Logger.getLogger(getClass().getName());

	public static SimpleLocking getInstance() {
		if (instance == null) {
			instance = new SimpleLocking();
		}
		return instance;
	}

	/**
	 * contains all locked objects
	 */
	private Collection<Serializable> data = new Vector<Serializable>();

	private long sleepTime = 2500;

	protected SimpleLocking() {
		this(SimpleLocking.class.getSimpleName());
	}

	protected SimpleLocking(String owner) {
		super(owner);
	}

	/**
	 * @author wohlgemuth
	 * @version Dec 6, 2005
	 * @see edu.ucdavis.genomics.metabolomics.util.thread.locking.AbstractLocking#isLocked(java.lang.Object)
	 */
	public boolean isLocked(Serializable o) throws LockingException {
		boolean returnboolean = data.contains(o);
		return returnboolean;
	}

	/**
	 * @author wohlgemuth
	 * @version Dec 6, 2005
	 * @see edu.ucdavis.genomics.metabolomics.util.thread.locking.AbstractLocking#lock(java.lang.Object)
	 */
	protected boolean lock(Serializable o) throws LockingException {
		if (isLocked(o)) {
			return false;
		}
		this.data.add(o);
		return true;
	}

	/**
	 * we wait between the attemps to auquire a ressource
	 * 
	 * @author wohlgemuth
	 * @version Dec 6, 2005
	 * @see edu.ucdavis.genomics.metabolomics.util.thread.locking.AbstractLocking#getSleepTime()
	 */
	protected synchronized long getSleepTime() {
		return (long) sleepTime;
	}

	/**
	 * @author wohlgemuth
	 * @version Dec 6, 2005
	 * @see edu.ucdavis.genomics.metabolomics.util.thread.locking.Lockable#releaseRessource(java.lang.Object)
	 */
	public void doRelease(Serializable o) throws LockingException {
		if (isLocked(o) == false) {
			logger.warn("ressource was not locked: " + o);
		} else {
			this.data.remove(o);
		}
	}

	/**
	 * contains all locked ressources
	 * 
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 */
	public Collection<Serializable> getLockedRessources() {
		return Collections.unmodifiableCollection(data);
	}

	public long getTimeout() throws LockingException {

		return 1000;
	}

	public void setSleepTime(long sleepTime) {
		this.sleepTime = sleepTime;
	}
}
