package edu.ucdavis.genomics.metabolomics.util.status.notify;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.util.status.notify.queue.SimpleQueueNotifier;

public class QueuedNotifierFactory extends NotifierFactory{

	@Override
	public void configure(Map<?, ?> p) {
		
	}

	@Override
	public QueuedNotifier createNotifer() {
		return SimpleQueueNotifier.getInstance();
	}

	public void addNotifier(Notifier notifier){
		createNotifer().addNotifier(notifier);
	}

	
}
