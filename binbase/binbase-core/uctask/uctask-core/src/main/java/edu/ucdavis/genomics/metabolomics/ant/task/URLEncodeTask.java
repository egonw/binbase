/*
 * Created on Oct 7, 2005
 */
package edu.ucdavis.genomics.metabolomics.ant.task;

import java.net.URLEncoder;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
/**
 * takes a givn url and transforms it
 * @author wohlgemuth
 *
 */
public class URLEncodeTask extends Task{
    private String variableToSet;
    
    private String url;
    /**
     * 
     */
    public URLEncodeTask() {
        super();
    }
    

    public String getVariableToSet() {
        return this.variableToSet;
    }
    public void setVariableToSet(String variableToSet) {
        this.variableToSet = variableToSet;
    }
    @SuppressWarnings("deprecation")
	public void execute() throws BuildException {
        super.execute();
        if(url == null){
            throw new BuildException("url must be set to a value");
        }
        
        if(variableToSet == null){
            throw new BuildException("variable must be set to a value");
        }

        this.getProject().setUserProperty(this.getVariableToSet(),URLEncoder.encode(this.url));

    }


    public String getUrl() {
        return url;
    }


    public void setUrl(String url) {
        this.url = url;
    }

}
