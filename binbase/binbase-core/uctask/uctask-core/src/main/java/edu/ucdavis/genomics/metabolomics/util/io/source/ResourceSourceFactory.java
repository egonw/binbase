package edu.ucdavis.genomics.metabolomics.util.io.source;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;

public class ResourceSourceFactory extends SourceFactory{

	@Override
	public Source createSource(Object identifier, Map<?, ?> propertys) throws ConfigurationException {
		return new ResourceSource(identifier.toString());
	}

}
