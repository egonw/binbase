package edu.ucdavis.genomics.metabolomics.binbase.filter;

import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;

import java.util.Properties;
import java.util.Date;
import java.io.Serializable;

import static org.junit.Assert.assertTrue;
import org.apache.log4j.Logger;

/**
 * User: wohlgemuth
 * Date: Jun 28, 2009
 * Time: 5:05:33 PM
 */
public class TestFactory extends ReportFactory{


    private Logger logger = Logger.getLogger(getClass());
    /**
     * checks if all the result's fit the exspectations
     *
     * @param properties
     * @param s
     * @return
     */
    public Report create(Properties properties, String s) {
        return new Report() {
            public void report(String s, Serializable serializable, ReportEvent reportEvent, ReportType reportType, String s1, Date date, Exception e) {
                logger.info("received: " + serializable);
                assertTrue(reportEvent.equals(FilterReports.FITLER_URI_ACCESS));
                assertTrue(reportType.equals(FilterReports.FILTER_REQUEST));

                assertTrue(serializable instanceof IPRequest);
                IPRequest request = (IPRequest) serializable;

                assertTrue(request.getHost().equals("localhost"));
                assertTrue(request.getUri().equals("http://127.0.0.1/test.html"));
                assertTrue(request.getAddress().equals("127.0.0.1"));
                assertTrue(request.getPort() == 80);
            }

            public String getOwner() {

                return IPLogger.class.getSimpleName();
            }
        };
    }
}
