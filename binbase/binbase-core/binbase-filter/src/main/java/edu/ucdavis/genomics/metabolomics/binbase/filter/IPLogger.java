package edu.ucdavis.genomics.metabolomics.binbase.filter;

import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * forwards all requests to the reporter which deals with it
 * <p/>
 * User: wohlgemuth
 * Date: Jun 28, 2009
 * Time: 3:33:00 PM
 */
public class IPLogger implements Filter {

    /**
     * forwards everything to the server
     */
    private Report report;

    /**
     * logging nstances
     */
    private Logger logger = Logger.getLogger(getClass().getName());

    /**
     * initialize the connection to the report method
     *
     * @param filterConfig
     * @throws ServletException
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("initialize filter");

        //access the factory we need for the reporter
        String factory = filterConfig.getInitParameter("factory");

        if (factory == null) {
            logger.info("no factory provided, so we use the default factory");

            report = ReportFactory.newInstance().create(IPLogger.class.getSimpleName());
        } else {
            logger.info("using factory: " + factory);
            report = ReportFactory.newInstance(factory).create(IPLogger.class.getSimpleName());
        }
    }

    /**
     * does the actual filtering
     *
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String host = servletRequest.getRemoteHost();
        String address = servletRequest.getRemoteAddr();
        int port = servletRequest.getServerPort();

        String uri = "unknown";
        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest req = (HttpServletRequest) servletRequest;

            uri = req.getRequestURI();
        }

        //send it out so that we can deal with these event in a different class or so
        report.report(new IPRequest(host, uri, address, port), FilterReports.FITLER_URI_ACCESS, FilterReports.FILTER_REQUEST);
    }

    public void destroy() {
    }
}
