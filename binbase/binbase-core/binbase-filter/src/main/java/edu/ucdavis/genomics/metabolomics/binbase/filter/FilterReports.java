package edu.ucdavis.genomics.metabolomics.binbase.filter;

import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;

/**
 * constants needed for filtering
 *
 * User: wohlgemuth
 * Date: Jun 28, 2009
 * Time: 3:44:53 PM
 */
public interface FilterReports {

    /**
     * a request
     */
    public static ReportType FILTER_REQUEST = new ReportType("request","an request to the server");

    /**
     * an event
     */
    public static ReportEvent FITLER_URI_ACCESS = new ReportEvent("filter uri acces","an uri is accessed");
}
