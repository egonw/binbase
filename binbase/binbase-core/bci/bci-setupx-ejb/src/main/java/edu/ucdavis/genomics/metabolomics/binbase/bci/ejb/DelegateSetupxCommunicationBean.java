package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.Stateless;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;

import org.jboss.logging.Logger;
import org.jboss.mx.util.MBeanServerLocator;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.MetaProviderJMXMBean;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXProvider;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.CommunicationExcpetion;

@Stateless
public class DelegateSetupxCommunicationBean implements SetupXProvider,
		DelegateSeupxProvider {
	/**
	 * Logger for this class
	 */
	private final Logger logger = Logger.getLogger(getClass());

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * contains all need
	 * 
	 * @see SetupXProvider
	 */
	private Collection<SetupXProvider> provider;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateSeupxProvider
	 * #getSetupXId(java.lang.String)
	 */
	public String getSetupXId(String sampleName) throws BinBaseException {
		try {
			sync();
		} catch (RemoteException e) {
			throw new CommunicationExcpetion(e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getSetupXId(String) - start"); //$NON-NLS-1$
		}

		logger.debug("registered providers: " + provider.size());

		Iterator<SetupXProvider> it = provider.iterator();

		while (it.hasNext()) {
			logger.info("call provider: ");
			SetupXProvider pro = (SetupXProvider) it.next();
			logger.info(" -> " + pro);

			try {
				String returnString = pro.getSetupXId(sampleName);
				if (logger.isDebugEnabled()) {
					logger.debug("getSetupXId(String) - end"); //$NON-NLS-1$
				}

				logger.info("setupX id for " + sampleName + " is "
						+ returnString);

				return returnString;
			} catch (Exception e) {
				logger.debug("sorry couldn't perform operation: "
						+ e.getMessage());
			}
		}

		logger.debug("nothing found");
		if (logger.isDebugEnabled()) {
			logger.debug("getSetupXId(String) - end"); //$NON-NLS-1$
		}
		return sampleName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateSeupxProvider
	 * #getMetaInformationByExperiment(java.lang.String)
	 */
	public String getMetaInformationByExperiment(String setupXId)
			throws BinBaseException {

		String key = getClass().getSimpleName()
				+ "getMetaInformationByExperiment" + setupXId;
		try {
			sync();
		} catch (RemoteException e) {
			throw new CommunicationExcpetion(e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getMetaInformation(String) - start"); //$NON-NLS-1$
		}

		Iterator<SetupXProvider> it = provider.iterator();

		while (it.hasNext()) {
			try {
				SetupXProvider pro = (SetupXProvider) it.next();
				String returnString = pro
						.getMetaInformationByExperiment(setupXId);
				if (logger.isDebugEnabled()) {
					logger.debug("getMetaInformation(String) - end"); //$NON-NLS-1$
				}
				if (returnString != null) {
					return returnString;
				}
			} catch (Exception e) {
				logger.debug("sorry couldn't perform operation: "
						+ e.getMessage());
			}
		}

		logger.debug("nothing found");
		if (logger.isDebugEnabled()) {
			logger.debug("getMetaInformation(String) - end"); //$NON-NLS-1$
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateSeupxProvider
	 * #getMetaInformationByClass(java.lang.String)
	 */
	public String getMetaInformationByClass(String setupXId)
			throws BinBaseException {

		String key = getClass().getSimpleName() + "getMetaInformationByClass"
				+ setupXId;

		try {
			sync();
		} catch (RemoteException e) {
			throw new CommunicationExcpetion(e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getMetaInformation(String) - start"); //$NON-NLS-1$
		}

		Iterator<SetupXProvider> it = provider.iterator();

		while (it.hasNext()) {
			try {
				SetupXProvider pro = (SetupXProvider) it.next();
				String returnString = pro.getMetaInformationByClass(setupXId);
				if (logger.isDebugEnabled()) {
					logger.debug("getMetaInformation(String) - end"); //$NON-NLS-1$
				}
				if (returnString != null) {
					return returnString;
				}
			} catch (Exception e) {
				logger.debug("sorry couldn't perform operation: "
						+ e.getMessage());
			}
		}

		logger.warn("nothing found");
		if (logger.isDebugEnabled()) {
			logger.debug("getMetaInformation(String) - end"); //$NON-NLS-1$
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateSeupxProvider
	 * #getMetaInformationBySample(java.lang.String)
	 */
	public String getMetaInformationBySample(String setupXId)
			throws BinBaseException {
		try {
			sync();
		} catch (RemoteException e) {
			throw new CommunicationExcpetion(e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("getMetaInformation(String) - start"); //$NON-NLS-1$
		}

		Iterator<SetupXProvider> it = provider.iterator();

		while (it.hasNext()) {
			try {
				SetupXProvider pro = (SetupXProvider) it.next();
				String returnString = pro.getMetaInformationBySample(setupXId);
				if (logger.isDebugEnabled()) {
					logger.debug("getMetaInformation(String) - end"); //$NON-NLS-1$
				}
				if (returnString != null) {
					return returnString;
				}
			} catch (Exception e) {
				logger.debug("sorry couldn't perform operation: "
						+ e.getMessage());
			}
		}

		logger.debug("nothing found");
		if (logger.isDebugEnabled()) {
			logger.debug("getMetaInformation(String) - end"); //$NON-NLS-1$
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateSeupxProvider
	 * #upload(java.lang.String, java.lang.String)
	 */
	public void upload(String id, String content) throws BinBaseException {
		logger.info("upload: " + id);
		try {
			sync();
		} catch (RemoteException e) {
			throw new CommunicationExcpetion(e);
		}

		logger.info("walking throw the providers");
		Iterator<SetupXProvider> it = provider.iterator();

		while (it.hasNext()) {
			try {
				SetupXProvider pro = (SetupXProvider) it.next();

				logger.info("upload with : " + pro);
				pro.upload(id, content);
			} catch (Exception e) {
				logger.debug("sorry couldn't perform operation: "
						+ e.getMessage());
			}
		}
	}

	/**
	 * syncs the internal provider list with the mbean, maybe somebody added a
	 * new provider
	 * 
	 * @throws RemoteException
	 */
	protected void sync() throws RemoteException {
		try {
			ObjectName name = new ObjectName("binbase:service=MetaProvider");
			MBeanServer server = MBeanServerLocator.locate();

			MetaProviderJMXMBean bean = (MetaProviderJMXMBean) MBeanServerInvocationHandler
					.newProxyInstance(server, name, MetaProviderJMXMBean.class,
							false);

			Collection<?> classes = bean.getProvider();
			this.provider = new ArrayList<SetupXProvider>();

			Iterator<?> it = classes.iterator();

			while (it.hasNext()) {
				String providerName = (String) it.next();
				logger.info("add provider: " + providerName);
				try {
					Class<?> clazz = Class.forName(providerName);
					SetupXProvider pro = (SetupXProvider) clazz.newInstance();
					this.provider.add(pro);
				} catch (Exception e) {
					logger.error("sorry could'nt create provider for class: "
							+ providerName + " it will be skipped", e);
				}
			}
		} catch (Exception e) {
			throw new RemoteException(e.getMessage());
		}
	}

	@Override
	public boolean canCreateBins(String setupxId) throws BinBaseException {
		logger.info("can create bins: " + setupxId);
		try {
			sync();
		} catch (RemoteException e) {
			throw new CommunicationExcpetion(e);
		}

		logger.info("walking throw the providers");
		Iterator<SetupXProvider> it = provider.iterator();

		while (it.hasNext()) {
			try {
				SetupXProvider pro = (SetupXProvider) it.next();

				boolean result = pro.canCreateBins(setupxId);

				if (result == false) {
					return false;
				}
			} catch (Exception e) {
				logger.debug("sorry couldn't perform operation: "
						+ e.getMessage());
			}
		}

		return true;
	}
}
