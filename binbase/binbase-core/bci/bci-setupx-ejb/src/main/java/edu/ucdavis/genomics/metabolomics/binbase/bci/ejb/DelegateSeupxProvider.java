package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import javax.ejb.Remote;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

@Remote
public interface DelegateSeupxProvider {

	public abstract String getSetupXId(String sampleName) throws BinBaseException;

	public abstract String getMetaInformationByExperiment(String setupXId) throws BinBaseException;

	public abstract String getMetaInformationByClass(String setupXId) throws BinBaseException;

	public abstract String getMetaInformationBySample(String setupXId) throws BinBaseException;

	public abstract void upload(String id, String content) throws BinBaseException;
	
	public abstract boolean canCreateBins(String setupxId) throws BinBaseException;

}
