package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class KeyStoreEntityBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	String application;

	@Column(nullable = false)
	String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof KeyStoreEntityBean){
			KeyStoreEntityBean o = (KeyStoreEntityBean) obj;
			
			if(o.getApplication().equals(this.getApplication())){
				return o.getValue().equals(this.getValue());
			}
		}
		
		return false;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public String toString() {
		return getApplication() + " - " + getValue();
	}
	
	
}
