package edu.ucdavis.genomics.metabolomics.binbase.bci;

import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.KeyStoreServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStore;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;

/**
 * provides access to a simple keystore factory
 * @author wohlgemuth
 *
 */
public class SimpleKeyStoreFactory extends KeyStoreFactory{

	@Override
	public KeyStore createKeyStore() throws KeyStoreException {
		try {
			return (KeyStore) EjbClient.getRemoteEjb(Configurator.BCI_GLOBAL_NAME, KeyStoreServiceBean.class);
		}
		catch (NamingException e) {
			throw new KeyStoreException(e);
		}
	}

}
