package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;

import javax.ejb.Remote;

import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStore;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreException;

@Remote
public interface KeyStoreService extends KeyStore,Serializable {

	/**
	 * removes a key
	 */
	public abstract void removeApplication(String applicationName) throws KeyStoreException;

	/**
	 * stores a key
	 */
	public abstract void storeKey(String applicationName, String key) throws KeyStoreException;

	/**
	 * does the application exist
	 * @param applicationName
	 * @return
	 */
	public abstract boolean existApplication(String applicationName);

	/**
	 * deletes all keys
	 * @throws KeyStoreException
	 */
	public void deleteAllKeys() throws KeyStoreException;
	
	/**
	 * does it actually contains the key
	 * @param key
	 * @return
	 * @throws KeyStoreException
	 */
	public boolean containsKey(String key) throws KeyStoreException;
}
