package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;
import java.util.Collection;

import javax.ejb.Remote;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.Authentificator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;

/**
 * a very simple authentification service and user management
 * @author wohlgemuth
 *
 */
@Remote
public interface SimpleAuthentificationService extends Authentificator,Serializable{

	/**
	 * add a new user
	 * 
	 * @param user
	 */
	public abstract void addUser(User user) throws AuthentificationException;

	/**
	 * remove a user
	 * 
	 * @param user
	 * @throws AuthentificationException 
	 */
	public abstract void removeUser(User user) throws AuthentificationException;

	/**
	 * list all users
	 * 
	 * @return
	 */
	public abstract Collection<User> listUser();

	/**
	 * update the user
	 * 
	 * @param user
	 */
	public abstract void updateUser(User user) throws AuthentificationException;

	/**
	 * does the user exist
	 * 
	 * @param user
	 * @return
	 */
	public abstract boolean existUser(User user);

}