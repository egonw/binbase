package edu.ucdavis.genomics.metabolomics.binbase.bci;

import java.util.Map;

import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.Authentificator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificatorFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificatorServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.util.status.Report;

/**
 * profides us with access to the simple authentificator service
 * 
 * @author wohlgemuth
 * 
 */
public class SimpleAuthentificatorFactory extends AuthentificatorFactory {

	@Override
	public Authentificator create(Map<?, ?> properties, Report report) throws AuthentificationException {
		try {
			return (SimpleAuthentificationService) EjbClient.getRemoteEjb(Configurator.BCI_GLOBAL_NAME, SimpleAuthentificatorServiceBean.class);
		} catch (NamingException e) {
			throw new AuthentificationException(e);
		}
	}

}
