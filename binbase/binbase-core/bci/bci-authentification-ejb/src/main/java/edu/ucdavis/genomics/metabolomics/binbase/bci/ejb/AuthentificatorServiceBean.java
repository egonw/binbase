package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.CreateException;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.Authentificator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificatorFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.AuthentificationProviderJMXFacadeLocal;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.AuthentificationProviderJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

/**
 * iterates over all registered authentificators and trys to find any way to
 * authentificate the user
 * 
 * @author wohlgemuth
 */
@Stateless
public class AuthentificatorServiceBean implements AuthentificatorService, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private Report report;

	private Logger logger = Logger.getLogger(getClass());

	@PostConstruct
	public void ejbCreate() throws CreateException {
		try {
			this.report = ReportFactory.newInstance().create("application server");

		}
		catch (Exception e) {
			throw new CreateException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.AuthentificatorService#authentificate(java.lang.String,
	 *      java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public User authentificate(String username, String password, String database) throws AuthentificationException {
		try {
			report.report(username, Reports.STARTED, Authentificator.LOGIN);
			AuthentificationProviderJMXFacadeLocal service = AuthentificationProviderJMXFacadeUtil.getLocalHome().create();

			java.util.Iterator<String> it = service.getProviders().iterator();

			Authentificator auth = null;
			while (it.hasNext()) {
				try {
					auth = ((AuthentificatorFactory) Class.forName(it.next()).newInstance()).create();
					User user = auth.authentificate(username, password, database);
					report.report(username, Reports.DONE, Authentificator.LOGIN);
					return user;
				}
				catch (AuthentificationException e) {
					logger.debug(e.getMessage(),e);
					report.report(username + " - failed to login with the used provider: " + auth, Reports.FAILED, Authentificator.LOGIN);
				}
				catch (Exception e) {
					logger.warn(e.getMessage(),e);

					report.report(username + " - there was an exception and failed to login with the used provider: " + auth, Reports.FAILED,
							Authentificator.LOGIN, e);
				}
			}
		}
		catch (AuthentificationException e1) {
			throw new AuthentificationException(e1);
		}
		catch (Exception e1) {
			throw new AuthentificationException(e1);
		}
		report.report(username, Reports.FAILED, Authentificator.LOGIN);
		throw new AuthentificationException("sorry was not able to authentificate against any of the registered authentificators");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.AuthentificatorService#authentificate(java.lang.String,
	 *      java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public User authentificate(String username, String password) throws AuthentificationException {
		try {
			report.report(username, Reports.STARTED, Authentificator.LOGIN);
			AuthentificationProviderJMXFacadeLocal service = AuthentificationProviderJMXFacadeUtil.getLocalHome().create();

			java.util.Iterator<String> it = service.getProviders().iterator();

			Authentificator auth = null;
			while (it.hasNext()) {
				try {
					auth = ((AuthentificatorFactory) Class.forName(it.next()).newInstance()).create();
					User user = auth.authentificate(username, password);
					report.report(username, Reports.DONE, Authentificator.LOGIN);
					return user;
				}
				catch (AuthentificationException e) {
					logger.debug(e.getMessage(),e);

					report.report(username + " - failed to login with the used provider: " + auth, Reports.FAILED, Authentificator.LOGIN);
				}
				catch (Exception e) {
					logger.warn(e.getMessage(),e);
					
					report.report(username + " - there was an exception and failed to login with the used provider: " + auth, Reports.FAILED,
							Authentificator.LOGIN, e);
				}
			}
		}
		catch (AuthentificationException e1) {
			throw new AuthentificationException(e1);
		}
		catch (Exception e1) {
			throw new AuthentificationException(e1);
		}
		report.report(username, Reports.FAILED, Authentificator.LOGIN);
		throw new AuthentificationException("sorry was not able to authentificate against any of the registered authentificators");
	}

	/**
	 * add a new user
	 * 
	 * @param user
	 * @throws AuthentificationException
	 */
	@SuppressWarnings("unchecked")
	public void addUser(User user) throws AuthentificationException {
		try {
			report.report(user.getUsername(), Reports.STARTED, Authentificator.REGISTER);

			AuthentificationProviderJMXFacadeLocal service = AuthentificationProviderJMXFacadeUtil.getLocalHome().create();

			java.util.Iterator<String> it = service.getProviders().iterator();

			Authentificator auth = null;
			while (it.hasNext()) {
				try {
					auth = ((AuthentificatorFactory) Class.forName(it.next()).newInstance()).create();
					if (auth.canRegisterUser()) {
						auth.addUser(user);
						return;
					}
				}
				catch (AuthentificationException e) {
					logger.debug(e.getMessage(),e);

					report.report(user.getUsername() + " - failed to create user with the used provider: " + auth, Reports.FAILED, Authentificator.REGISTER);
				}
				catch (Exception e) {
					logger.warn(e.getMessage(),e);

					report.report(user.getUsername() + " - there was an exception and failed to create user with the used provider: " + auth, Reports.FAILED,
							Authentificator.REGISTER, e);
				}
			}
		}
		catch (AuthentificationException e1) {
			throw new AuthentificationException(e1);
		}
		catch (Exception e1) {
			throw new AuthentificationException(e1);
		}
		report.report(user.getUsername(), Reports.FAILED, Authentificator.REGISTER);
		throw new AuthentificationException("sorry was not able to create a user against any of the registered authentificators");

	}
}
