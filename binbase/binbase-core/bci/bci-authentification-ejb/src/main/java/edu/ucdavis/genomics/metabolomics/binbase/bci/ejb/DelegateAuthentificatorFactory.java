package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.util.Map;

import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.Authentificator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificatorFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.util.status.Report;

/**
 * provides us with access to the delegate authentification, and forwards all
 * authentifcation request to the application server
 * 
 * @author wohlgemuth
 * 
 */
public class DelegateAuthentificatorFactory extends AuthentificatorFactory {

	@Override
	public Authentificator create(Map<?, ?> properties, Report report) throws AuthentificationException {
		try {
			final AuthentificatorService service = (AuthentificatorService) EjbClient.getRemoteEjb(
					edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator.BCI_GLOBAL_NAME, AuthentificatorServiceBean.class);
			return new Authentificator() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 2L;

				public User authentificate(String username, String password) throws AuthentificationException {
					return service.authentificate(username, password);
				}

				public User authentificate(String username, String password, String database) throws AuthentificationException {
					return service.authentificate(username, password, database);
				}

				public void addUser(User user) throws AuthentificationException {
					service.addUser(user);
				}

				public boolean canRegisterUser() throws AuthentificationException {
					return true;
				}
			};
		} catch (NamingException e) {
			throw new AuthentificationException(e);
		}
	}

}
