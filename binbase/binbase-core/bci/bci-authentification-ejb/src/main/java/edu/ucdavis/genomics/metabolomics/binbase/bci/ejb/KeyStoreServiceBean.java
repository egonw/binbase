package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.jboss.logging.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreException;

/**
 * access to the keystore
 * 
 * @author wohlgemuth
 * 
 */
@Stateless
public class KeyStoreServiceBean implements KeyStoreService, Serializable {

	private Logger logger = Logger.getLogger(getClass());

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	EntityManager manager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.KeyStoreService#listKeys
	 * ()
	 */
	public Map<String, String> listKeys() {
		Map<String, String> map = new HashMap<String, String>();

		for (KeyStoreEntityBean bean : listKeysAsList()) {
			map.put(bean.getApplication(), bean.getValue());
		}
		return map;
	}

	/**
	 * checks if we actually contain the key
	 * 
	 * @param key
	 * @return
	 */
	public boolean containsKey(String key) {
		for (KeyStoreEntityBean bean : listKeysAsList()) {
			logger.info(bean.getApplication() + " - " + bean.getValue());
			if (bean.getValue().equals(key)) {
				logger.info("key found: " + key + " for application " + bean.getApplication());
				return true;
			} else {
				logger.info("given " + key + " was different from " + bean.getValue() + " for application " + bean.getApplication());
			}
		}

		logger.debug("key not found: " + key);
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.KeyStoreService#
	 * listKeysAsList()
	 */
	@SuppressWarnings("unchecked")
	public List<KeyStoreEntityBean> listKeysAsList() {
		return manager
				.createQuery("from " + KeyStoreEntityBean.class.getName())
				.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.KeyStoreService#removeKey
	 * (java.lang.String)
	 */
	public void removeApplication(String applicationName)
			throws KeyStoreException {
		if (existApplication(applicationName)) {
			manager.remove(manager.find(KeyStoreEntityBean.class,
					applicationName));
			manager.flush();
			return;
		}

		throw new KeyStoreException("sorry application NOT registered: "
				+ applicationName);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.KeyStoreService#storeKey
	 * (java.lang.String, java.lang.String)
	 */
	public void storeKey(String applicationName, String key)
			throws KeyStoreException {
		logger.debug("storing key for application: " + applicationName);

		if (existApplication(applicationName) == false) {
			KeyStoreEntityBean bean = new KeyStoreEntityBean();
			bean.setApplication(applicationName);
			bean.setValue(key);
			manager.persist(bean);
			manager.flush();
			logger.debug("key stored");
			return;
		}

		throw new KeyStoreException("sorry applications already registered!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.KeyStoreService#
	 * existApplication(java.lang.String)
	 */
	public boolean existApplication(String applicationName) {
		logger.debug("checking if key for application exists: "
				+ applicationName);
		KeyStoreEntityBean bean = manager.find(KeyStoreEntityBean.class,
				applicationName);

		return bean != null;
	}

	public void deleteAllKeys() throws KeyStoreException {
		for (KeyStoreEntityBean bean : this.listKeysAsList()) {
			this.removeApplication(bean.getApplication());
		}
	}

	@Override
	public boolean containsApplication(String application)
			throws KeyStoreException {
		return existApplication(application);
	}
}
