package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.logging.Logger;


import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;

/**
 * authentificate against an internal database using entity ejbs
 * 
 * @author wohlgemuth
 */
@Stateless
public class SimpleAuthentificatorServiceBean implements SimpleAuthentificationService,Serializable {
	@PersistenceContext
	EntityManager manager;

	private Logger logger = Logger.getLogger(getClass());
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService#addUser(edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User)
	 */
	public void addUser(User user) throws AuthentificationException {
		logger.debug("trying to add a user: " + user);
		if (existUser(user) == false) {
			manager.persist(new UserEntityBean(user));
			manager.flush();
			logger.debug("user is added");
		}
		else {
			throw new AuthentificationException("sorry user already exist in database!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService#removeUser(edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User)
	 */
	public void removeUser(User user) throws AuthentificationException {
		logger.debug("remove user: " + user);
		if (existUser(user) == true) {
			manager.remove(manager.find(UserEntityBean.class, user.getUsername()));
			manager.flush();
			logger.debug("user is removed");

		}
		else {
			throw new AuthentificationException("sorry user does not exist in database!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService#listUser()
	 */
	@SuppressWarnings("unchecked")
	public Collection<User> listUser() {
		logger.debug("retrive all users...");
		Collection<User> result = new Vector<User>();
		List<UserEntityBean> users = manager.createQuery("from " + UserEntityBean.class.getName()).getResultList();

		for (UserEntityBean user : users) {
			result.add(new User(String.valueOf(user.getPassword()), user.getName(), user.hashCode(), user.isAdmin()));
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService#updateUser(edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User)
	 */
	public void updateUser(User user) throws AuthentificationException {
		logger.debug("update user: " + user);
		if (existUser(user)) {
			UserEntityBean bean = manager.find(UserEntityBean.class, user.getUsername());
			bean.setAdmin(user.isMasterUser());
			bean.setPassword(user.getPassword());
			manager.flush();
			logger.debug("user is updated");
		}
		else {
			throw new AuthentificationException("sorry user does not exist in database!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService#existUser(edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User)
	 */
	public boolean existUser(User user) {
		logger.debug("trying to see if the user exists: " + user);
		if (manager.find(UserEntityBean.class, user.getUsername()) != null) {
			logger.debug("user exist");
			return true;
		}
		logger.debug("user does not exist!");
		return false;
	}

	public User authentificate(String username, String password, String database) throws AuthentificationException {
		User user = new User(password, username, username.hashCode());
		logger.debug("authentificate user: " + user);

		if (existUser(user) == true) {
			UserEntityBean bean = manager.find(UserEntityBean.class, user.getUsername());

			if (bean.getPassword().equals(password)) {
				user = new User(password, username, username.hashCode(), bean.isAdmin());
				return user;
			}
			else {
				throw new AuthentificationException("sorry given password doesn't match");
			}
		}
		throw new AuthentificationException("given user not found!");
	}

	public User authentificate(String username, String password) throws AuthentificationException {
		return authentificate(username, password, null);
	}

	public boolean canRegisterUser() throws AuthentificationException {
		return true;
	}
}
