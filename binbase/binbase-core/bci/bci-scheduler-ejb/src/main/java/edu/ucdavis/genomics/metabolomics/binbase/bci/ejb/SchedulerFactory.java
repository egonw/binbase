package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * creates a new scheduler
 * 
 * @author wohlgemuth
 * 
 */
public abstract class SchedulerFactory extends AbstractFactory {

	public final static String DEFAULT_PROPERTIE = SchedulerFactory.class.getName();

	public abstract Scheduler createScheduler();

	public static SchedulerFactory createFactory() {
		return createFactory(System.getProperty(DEFAULT_PROPERTIE));
	}

	public static SchedulerFactory createFactory(String factoryClass) {
		Class<?> classObject;
		SchedulerFactory factory;

		try {

			if (factoryClass == null) {
				/**
				 * we just return access to the ejb scheduler by default
				 */
				return new SchedulerFactory() {

					@Override
					public Scheduler createScheduler() {
						try {
							return (Scheduler) EjbClient.getRemoteEjb(edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator.BCI_GLOBAL_NAME,
									SchedulerBean.class);
						} catch (NamingException e) {
							throw new FactoryException(e);
						}
						
					};

				};
			}
			classObject = Class.forName(factoryClass);

			factory = (SchedulerFactory) classObject.newInstance();
			return factory;

		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}

}
