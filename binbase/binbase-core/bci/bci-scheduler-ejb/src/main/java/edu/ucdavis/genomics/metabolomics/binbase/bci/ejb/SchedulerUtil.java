package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.job.SchedulerJob;
import edu.ucdavis.genomics.metabolomics.binbase.bci.job.SchedulerJobHandler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.SchedulingException;

/**
 * simple util to send scheduler jobs to the queue
 * 
 * @author wohlgemuth
 * 
 */
public class SchedulerUtil {

	private Logger logger = Logger.getLogger(getClass());

	private static SchedulerUtil instance;

	public static SchedulerUtil getInstance() {
		if (instance == null) {
			instance = new SchedulerUtil();
		}

		return instance;
	}

	/**
	 * schedule an import
	 * 
	 * @param clazz
	 * @throws SchedulingException
	 */
	public void scheduleImport(ExperimentClass clazz)
			throws SchedulingException {
		SchedulerJob job = new SchedulerJob(clazz);
		schedule(job);
	}

	/**
	 * schedule an export
	 * 
	 * @param experiment
	 * @throws SchedulingException
	 */
	public void scheduleExport(Experiment experiment)
			throws SchedulingException {
		SchedulerJob job = new SchedulerJob(experiment);
		schedule(job);
	}
	

	/**
	 * schedule an export
	 * 
	 * @param experiment
	 * @throws SchedulingException
	 */
	public void scheduleDSL(DSL dsl)
			throws SchedulingException {
		SchedulerJob job = new SchedulerJob(dsl);
		schedule(job);
	}
	
	

	/**
	 * puts the job into the queue
	 * 
	 * @param job
	 * @throws SchedulingException
	 */
	protected void schedule(SchedulerJob job) throws SchedulingException {
		try {
			logger.info("make sure handler is registered");
			Configurator.getConfigService().addHandler(
					SchedulerJob.class.getName(),
					SchedulerJobHandler.class.getName());
			
			logger.info("scheduling: " + job);
			InitialContext ictx = new InitialContext();

			ConnectionFactory connectionFactory =

			(ConnectionFactory) ictx.lookup

			("java:/XAConnectionFactory");

			Queue destination = (Queue) ictx.lookup("queue/queue");
			logger.debug("create connection");

			Connection connection = connectionFactory.createConnection();

			logger.debug("create session");

			Session session = connection.createSession(false,

			Session.AUTO_ACKNOWLEDGE);

			logger.debug("create producer");

			MessageProducer messageProducer =

			session.createProducer(destination);

			logger.debug("create message");

			ObjectMessage message = session.createObjectMessage();
			message.setObject(job);
			messageProducer.setPriority(Scheduler.PRIORITY_ASAP);
			logger.debug("send message");

			messageProducer.send(message);

			logger.debug("close session and producer");

			messageProducer.close();
			session.close();

			logger.debug("scheduling done");

			
		} catch (Exception e) {
			logger.error(e);
			throw new SchedulingException(e);
		}
	}
}
