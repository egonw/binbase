package edu.ucdavis.genomics.metabolomics.binbase.bci.job;

import java.io.Serializable;

/**
 * a scheduled export job class
 * @author wohlgemuth
 *
 */
public class SchedulerJob implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private Serializable task;
	
	private int id;
	
	public SchedulerJob(Serializable task){
		this.id = task.hashCode();
		this.task = task;
	}
	
	@Override
	public int hashCode(){
		return id;
	}
	
	@Override
	public String toString(){
		return this.getClass().getSimpleName() + " - " + this.task.toString();
	}

	public Serializable getTask() {
		return task;
	}

	public int getId() {
		return id;
	}
	
}


