package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import javax.ejb.Stateless;

import org.jboss.logging.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.exception.SendingException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.ValidatorFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.SchedulingException;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

/**
 * a simple schedule which imports the given data to the binbase queue
 * 
 * @author wohlgemuth
 */
@Stateless
public class SchedulerBean implements Scheduler {

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * @throws SchedulingException
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler#scheduleImport(edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass,
	 *      java.lang.String)
	 */
	public  synchronized void scheduleImport(ExperimentClass clazz, String key) throws AuthentificationException, SendingException, SchedulingException {
		validateKey(key);
		validateClass(clazz);

		Report report = ReportFactory.newInstance().create("application server");
		report.report(clazz, Reports.SCHEDULE, Reports.CLASS);

		// does the actual scheduling
		SchedulerUtil.getInstance().scheduleImport(clazz);
	}

	protected void validateClass(ExperimentClass clazz) throws SchedulingException {
		if (clazz == null) {
			throw new SchedulingException("sorry class can't be null!");
		}
		logger.trace("id: " + clazz.getId());
		if (clazz.getSamples() == null) {
			throw new SchedulingException("sorry no samples defiend!");
		}
		if (clazz.getColumn() == null) {
			throw new SchedulingException("sorry no column defiend!");
		}
		logger.trace("column: " + clazz.getColumn());

		if (clazz.getSamples().length == 0) {
			throw new SchedulingException("sorry no samples defiend!");
		}

		for (ExperimentSample c : clazz.getSamples()) {
			logger.trace("");
			if (c.getId() == null) {
				throw new SchedulingException("sorry no id for this sample defiend!");
			}
			logger.trace("sample id: " + c.getId());

			if (c.getName() == null) {
				throw new SchedulingException("sorry no name for this sample defiend!");
			}

			logger.trace("sample name: " + c.getName());
		}
	}

	protected void validateExperiemnt(Experiment exp) throws SchedulingException {
		if (exp == null) {
			throw new SchedulingException("sorry experiment can't be null!");
		}
		logger.trace("experiment id: " + exp.getId());
		if (exp.getClasses() == null) {
			throw new SchedulingException("sorry no classes defiened defiend!");
		}
		if (exp.getColumn() == null) {
			throw new SchedulingException("sorry no column defiend!");
		}
		logger.trace("experiment column: " + exp.getColumn());

		if (exp.getClasses().length == 0) {
			throw new SchedulingException("sorry no classes defiend!");
		}

		for (ExperimentClass c : exp.getClasses()) {
			logger.trace("");
			validateClass(c);
		}
	}

	/**
	 * @throws SchedulingException
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler#scheduleExport(edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment,
	 *      java.lang.String)
	 */
	public synchronized void scheduleExport(Experiment experiment, String key) throws AuthentificationException, SendingException, SchedulingException {
		validateKey(key);
		validateExperiemnt(experiment);

		// does the actual scheduling
		SchedulerUtil.getInstance().scheduleExport(experiment);

		try {
			Report report = ReportFactory.newInstance().create("application server");
			report.report(experiment, Reports.SCHEDULE, Reports.EXPERIMENT);

		}
		catch (Exception e) {
			logger.warn(e.getMessage(), e);
		}

	}

	/**
	 * validates the the key is identical with the exspected key
	 * 
	 * @param key
	 * @return
	 * @throws AuthentificationException
	 */
	private void validateKey(String key) throws AuthentificationException {
		if (ValidatorFactory.newInstance().createValidator().isKeyValid(key) == false) {
			throw new AuthentificationException("sorry invalid key!");
		}
	}

	/**
	 * schedules both an import and an export with the specific priority
	 */
	public synchronized void schedulePriorityCalculation(Experiment experiment, int priority, String key) throws AuthentificationException, SendingException,
			SchedulingException {

		if (priority >= 1) {
			for (ExperimentClass c : experiment.getClasses()) {
				c.setPriority(priority);
				scheduleImport(c, key);
			}

			experiment.setPriority(priority - 1);

			scheduleExport(experiment, key);
		}
		else {
			throw new SchedulingException("priority needs to be higher than 0!");
		}
	}

	@Override
	public void scheduleDSL(DSL dsl, String key) throws AuthentificationException, SendingException, SchedulingException {
		validateKey(key);

		Report report = ReportFactory.newInstance().create("application server");
		report.report(dsl, Reports.SCHEDULE, Reports.DSL);

		// does the actual scheduling
		SchedulerUtil.getInstance().scheduleDSL(dsl);
	}

}
