/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.ejb.Stateless;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.naming.NamingException;

import org.jboss.logging.Logger;
import org.jboss.mx.util.MBeanServerLocator;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.exception.FileNotFoundException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.exception.SendingException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.handler.GeneralJMSHandler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacadeLocal;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacadeLocal;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXMBean;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Priority;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.action.CalculateSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.action.UpdateExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.ValidatorFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.PostmatchingJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.UpdateBinBaseJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.CommunicationExcpetion;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.WrappedConnection;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

@Stateless
public class BinBaseServiceBean implements BinBaseService, Serializable {

	private static final long serialVersionUID = 2L;

	GeneralJMSHandler handler;

	private Logger logger = Logger.getLogger(BinBaseServiceBean.class);

	private Report report;

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * containsSample(java.lang.String, java.lang.String, java.lang.String)
	 */
	public boolean containsSample(String sampleName, String database, String key)
			throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		boolean result = false;
		BinBaseException ex = null;
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("SELECT * FROM samples WHERE \"visible\" = 'TRUE' and \"sample_name\" = ?");
			statement.setString(1, sampleName);
			ResultSet res = statement.executeQuery();

			result = res.next();
			res.close();
			statement.close();
		} catch (SQLException e) {
			try {
				statement.close();
			} catch (SQLException e1) {
				logger.debug(e.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
			ex = new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

		if (ex != null) {
			throw ex;
		}
		return result;

	}

	private void validateKey(String key) throws BinBaseException {
		logger.debug("validating key");
		ValidatorFactory.newInstance().createValidator().isKeyValid(key);
		this.report = ReportFactory.newInstance().create("application server");
		this.handler = new GeneralJMSHandler();

		try {
			if (edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator
					.getImportService().isDisableServices()) {
				throw new AuthentificationException(
						"sorry all BinBase Service are disabled right now!");
			}
		} catch (BinBaseException e) {
			throw e;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * containsSampleObject
	 * (edu.ucdavis.genomics.metabolomics.binbase.bci.server.
	 * types.ExperimentSample, java.lang.String, java.lang.String)
	 */
	public boolean containsSampleObject(ExperimentSample sample,
			String database, String key) throws BinBaseException {
		return containsSample(sample.getName(), database, key);
	}

	public int getSampleIdForLatestVersion(ExperimentSample sample,
			String database, String key) throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		int result = 0;
		BinBaseException ex = null;
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("SELECT sample_id FROM samples WHERE \"visible\" = 'TRUE' and \"sample_name\" = ?");
			statement.setString(1, sample.getName());
			ResultSet res = statement.executeQuery();

			if (res.next()) {
				logger.debug("got id: " + res.getInt(1));
				result = res.getInt(1);
			} else {
				ex = new BinBaseException(
						"sorry no sample with this name found: "
								+ sample.getName() + "/" + sample.getId());
			}

			res.close();
			statement.close();
		} catch (SQLException e) {
			try {
				statement.close();
			} catch (SQLException e1) {
				logger.debug(e.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
			ex = new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

		if (ex != null) {
			throw ex;
		}
		return result;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * getAllExperimentIds(java.lang.String, java.lang.String)
	 */
	public String[] getAllExperimentIds(String database, String key)
			throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		BinBaseException ex = null;
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("select setupx from result");
			ResultSet res = statement.executeQuery();

			logger.info("querry id's of experiments");
			Vector<String> ids = new Vector<String>();
			while (res.next()) {
				ids.add(res.getString(1));
			}

			res.close();
			statement.close();

			return (String[]) ids.toArray(new String[ids.size()]);

		} catch (SQLException e) {
			try {
				statement.close();
			} catch (SQLException e1) {
				logger.debug(e.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
			ex = new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

		if (ex != null) {
			throw ex;
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * getExperiment(java.lang.String, java.lang.String, java.lang.String)
	 */
	public Experiment getExperiment(String database, String id, String key)
			throws BinBaseException {
		validateKey(key);

		logger.info("load experiment definition for experiment: " + id);

		Connection connection = getConnection(database);
		BinBaseException ex = null;
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("select a.\"class\" from result c,result_link b, samples a where a.\"sample_id\" = b.\"sample_id\" and c.\"result_id\" = b.\"result_id\" and c.setupx = ? group by (a.\"class\")");
			statement.setString(1, id);
			ResultSet res = statement.executeQuery();

			Experiment exp = new Experiment();
			exp.setSopUrl(null);
			exp.setColumn(database);
			exp.setId(id);

			Vector<String> ids = new Vector<String>();
			while (res.next()) {
				ids.add(res.getString(1));
			}

			res.close();
			statement.close();

			ExperimentClass[] clazz = new ExperimentClass[ids.size()];
			for (int i = 0; i < ids.size(); i++) {
				clazz[i] = getExperimentClass(database, ids.get(i).toString(),
						connection);
			}

			exp.setClasses(clazz);

			logger.info("retrived: " + exp);
			return exp;

		} catch (SQLException e) {
			try {
				statement.close();
			} catch (SQLException e1) {
				logger.debug(e.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
			ex = new BinBaseException(e);
			throw ex;
		} finally {
			closeConnection(connection);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * getOutDatedSampleIds(java.lang.String, java.lang.String)
	 */
	public String[] getOutDatedSampleIds(String database, String key)
			throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		BinBaseException ex = null;
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("SELECT \"sample_id\" FROM samples WHERE \"visible\" = 'FALSE' and \"sample_id\" NOT in (select \"sample_id\" from BIN)");

			ResultSet res = statement.executeQuery();

			Collection<String> samples = new Vector<String>();

			while (res.next()) {
				samples.add(res.getString(1));
			}

			res.close();
			statement.close();

			return (String[]) samples.toArray(new String[samples.size()]);
		} catch (SQLException e) {
			try {
				statement.close();
			} catch (SQLException e1) {
				logger.debug(e.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
			ex = new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

		if (ex != null) {
			throw ex;
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * getStoredClass(java.lang.String, java.lang.String, java.lang.String)
	 */
	public ExperimentClass getStoredClass(String database, String id, String key)
			throws BinBaseException {
		validateKey(key);
		Connection connection = getConnection(database);
		try {
			return getExperimentClass(database, id, connection);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * loads a class from the database
	 * 
	 * @param database
	 * @param id
	 * @param connection
	 * @param statement
	 * @return
	 * @throws BinBaseException
	 */
	private ExperimentClass getExperimentClass(String database, String id,
			Connection connection) throws BinBaseException {
		PreparedStatement statement = null;
		try {
			statement = connection
					.prepareStatement("SELECT \"sample_name\",\"setupx_id\" FROM samples WHERE \"visible\" = 'TRUE' and \"class\" = ?");
			statement.setString(1, id);
			ResultSet res = statement.executeQuery();

			Collection<ExperimentSample> samples = new Vector<ExperimentSample>();
			ExperimentClass clazz = new ExperimentClass();

			while (res.next()) {
				ExperimentSample sample = new ExperimentSample();
				sample.setName(res.getString(1));
				sample.setId(res.getString(2));

				samples.add(sample);
			}

			statement.close();

			clazz.setId(id);
			clazz.setColumn(database);
			clazz.setSamples((ExperimentSample[]) samples
					.toArray(new ExperimentSample[samples.size()]));
			res.close();
			statement.close();

			return clazz;
		} catch (SQLException e) {
			try {
				statement.close();
			} catch (SQLException e1) {
				logger.debug(e.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);

			throw new BinBaseException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * getStoredClasses(java.lang.String, java.lang.String)
	 */
	public ExperimentClass[] getStoredClasses(String database, String key)
			throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		PreparedStatement classes = null;
		PreparedStatement statement = null;
		BinBaseException ex = null;

		try {
			classes = connection
					.prepareStatement("select \"class\" from samples group by \"class\"");
			statement = connection
					.prepareStatement("SELECT \"sample_name\",\"setupx_id\" FROM samples WHERE \"visible\" = 'TRUE' and \"class\" = ?");

			Collection<ExperimentClass> foundClasses = new Vector<ExperimentClass>();

			ResultSet classSet = classes.executeQuery();

			while (classSet.next()) {
				String id = classSet.getString(1);

				statement.setString(1, id);
				ResultSet res = statement.executeQuery();

				Collection<ExperimentSample> samples = new Vector<ExperimentSample>();
				ExperimentClass clazz = new ExperimentClass();

				while (res.next()) {
					ExperimentSample sample = new ExperimentSample();
					sample.setName(res.getString(1));
					sample.setId(res.getString(2));

					samples.add(sample);
				}

				clazz.setId(id);
				clazz.setColumn(database);
				clazz.setSamples((ExperimentSample[]) samples
						.toArray(new ExperimentSample[samples.size()]));
				res.close();

				foundClasses.add(clazz);

			}
			classSet.close();
			classes.close();
			statement.close();

			return (ExperimentClass[]) foundClasses
					.toArray(new ExperimentClass[foundClasses.size()]);
		} catch (SQLException e) {
			try {
				statement.close();
				classes.close();
			} catch (SQLException e1) {
				logger.debug(e.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
			ex = new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

		if (ex != null) {
			throw ex;
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * getStoredSample(java.lang.String, java.lang.String, java.lang.String)
	 */
	public ExperimentSample[] getStoredSample(String database, String setupXId,
			String key) throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		BinBaseException ex = null;
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("SELECT \"sample_name\",\"setupx_id\" FROM samples WHERE \"visible\" = 'TRUE' and \"setupx_id\" = ?");
			statement.setString(1, setupXId);
			ResultSet res = statement.executeQuery();

			Collection<ExperimentSample> samples = new Vector<ExperimentSample>();

			while (res.next()) {
				ExperimentSample sample = new ExperimentSample();
				sample.setName(res.getString(1));
				sample.setId(res.getString(2));

				samples.add(sample);
			}

			res.close();
			statement.close();

			return (ExperimentSample[]) samples
					.toArray(new ExperimentSample[samples.size()]);
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			ex = new BinBaseException(e);
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				logger.debug(e.getMessage(), e);
			}
			closeConnection(connection);
		}

		if (ex != null) {
			throw ex;
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerExportById(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void triggerExportById(String id, String database, String key)
			throws BinBaseException {
		validateKey(key);

		Experiment e = getExperiment(database, id, key);
		triggerExportSecure(e, key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerExportSecure
	 * (edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment,
	 * java.lang.String)
	 */
	public void triggerExportSecure(Experiment experiment, String key)
			throws CommunicationExcpetion {
		try {
			validateKey(key);
		} catch (BinBaseException e1) {
			throw new CommunicationExcpetion(e1);
		}

		logger.info("export experiment: " + experiment + " for column: "
				+ experiment.getColumn());

		if (logger.isDebugEnabled()) {
			logger.debug("triggerExport(Experiment) - start"); //$NON-NLS-1$
		}

		if (experiment.getId() == null) {
			throw new CommunicationExcpetion("experiment id can't be null!");
		}
		if (experiment.getClasses() == null) {
			throw new CommunicationExcpetion(
					"experiment classes can't be null!");
		}

		// call handler
		try {

			logger.info("call handler");
			experiment.setIncrease(experiment.getIncrease() + 1);

			if (experiment.getPriority() <= -1) {
				experiment.setPriority(Priority.PRIORITY_LOW);
			}
			if (experiment.getPriority() > Priority.EXPERIMENT_MAX_PRIORITY) {
				logger.info("priority is to high, so we reduce it!");
				experiment.setPriority(Priority.EXPERIMENT_MAX_PRIORITY);
			}

			logger.info("using handler: " + handler);
			if (handler.handle(experiment, experiment.getPriority())) {
				report.report(experiment, Reports.SCHEDULE, Reports.EXPERIMENT);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new CommunicationExcpetion(e.getMessage());
		}

		if (logger.isDebugEnabled()) {
			logger.debug("triggerExport(Experiment) - end"); //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerExportClass
	 * (edu.ucdavis.genomics.metabolomics.binbase.bci.server.types
	 * .ExperimentClass, java.lang.String)
	 */
	public void triggerExportClass(ExperimentClass experiment, String key)
			throws BinBaseException {
		validateKey(key);

		Experiment ex = new Experiment();
		ex.setClasses(new ExperimentClass[] { experiment });
		ex.setColumn(experiment.getColumn());
		ex.setId("BBSX-" + experiment.hashCode());

		triggerExportSecure(ex, key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerExportClassById(java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	public void triggerExportClassById(String database, String id, String key)
			throws BinBaseException {
		validateKey(key);

		ExperimentClass experiment = this.getStoredClass(database, id, key);
		Experiment ex = new Experiment();
		ex.setClasses(new ExperimentClass[] { experiment });
		ex.setColumn(experiment.getColumn());
		ex.setId("BBCE_" + experiment.hashCode());

		triggerExportSecure(ex, key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerExportClasses
	 * (edu.ucdavis.genomics.metabolomics.binbase.bci.server.
	 * types.ExperimentClass[], java.lang.String)
	 */
	public void triggerExportClasses(ExperimentClass[] experiment, String key)
			throws BinBaseException {
		validateKey(key);

		String database = null;
		for (int i = 0; i < experiment.length; i++) {
			if (database == null) {
				database = experiment[i].getColumn();
			}

			if (!database.equals(experiment[i].getColumn())) {
				throw new BinBaseException(
						"all classes must be from the same database!");
			}
		}

		Experiment ex = new Experiment();
		ex.setClasses(experiment);
		ex.setColumn(database);
		ex.setId("class based: " + experiment.hashCode());

		triggerExportSecure(ex, key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerExportClassesById(java.lang.String, java.lang.String[],
	 * java.lang.String)
	 */
	public void triggerExportClassesById(String database, String ids[],
			String key) throws BinBaseException {
		validateKey(key);

		ExperimentClass[] experiment = new ExperimentClass[ids.length];
		for (int i = 0; i < ids.length; i++) {

			experiment[i] = this.getStoredClass(database, ids[i], key);
		}

		Experiment ex = new Experiment();
		ex.setClasses(experiment);
		ex.setColumn(database);
		ex.setId("BBSX" + experiment.hashCode());

		triggerExportSecure(ex, key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerExportDatabase(java.lang.String, java.lang.String)
	 */
	public void triggerExportDatabase(String database, String key)
			throws BinBaseException {
		validateKey(key);

		String[] content = this.getAllExperimentIds(database, key);

		for (int i = 0; i < content.length; i++) {
			this.triggerExportSecure(
					this.getExperiment(database, content[i], key), key);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerImportClassSecure
	 * (edu.ucdavis.genomics.metabolomics.binbase.bci.server
	 * .types.ExperimentClass, java.lang.String)
	 */
	public void triggerImportClassSecure(ExperimentClass experiment, String key)
			throws BinBaseException {
		validateKey(key);

		try {
			logger.info("import class: " + experiment + " for column: "
					+ experiment.getColumn());
			if (experiment == null) {
				throw new BinBaseException("experiment class can't be null!");
			}
			if (experiment.getId() == null) {
				throw new BinBaseException("experiment class id can't be null!");
			}
			if (experiment.getSamples() == null) {
				throw new BinBaseException(
						"experiment class samples can't be null!");
			}
			if (experiment.getColumn() == null) {
				throw new BinBaseException("you need to provide a column!");
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e.getMessage());
		}
		// store class filez to make shure the class is ok and there is not a
		// problem with the machine or missing files.

		ExperimentSample samples[] = experiment.getSamples();

		// contains all file not fond exceptions
		Collection<String> errors = new Vector<String>();

		Connection connection = getConnection(experiment.getColumn());
		logger.debug("aquiere connection to database");

		// store rawdata files

		for (int i = 0; i < samples.length; i++) {
			try {
				ExperimentSample sample = samples[i];
				storeSample(connection, sample);
			} catch (Exception e) {
				logger.info(
						"add new exception to collection of not found files "
								+ e.getMessage(), e);
				errors.add(samples[i].getName() + " - " + e.getMessage());
			}
		}

		try {
			if (Configurator.getImportService().isValidateSources() == true) {
				// check if we didn't find all files
				if (errors.isEmpty() == false) {
					throw new FileNotFoundException(errors);
				}
			} else {
				logger.debug("validation of files is disabled!");
			}
		} catch (Exception e1) {
			throw new BinBaseException(e1);
		}
		try {
			experiment.setIncrease(experiment.getIncrease() + 1);

			if (experiment.getPriority() == Scheduler.NONE) {
				experiment.setPriority(Priority.PRIORITY_HIGH);
			}
			if (experiment.getPriority() > Priority.EXPERIMENT_CLASS_MAX_PRIORITY) {
				logger.info("priority is to high, so we reduce it!");
				experiment.setPriority(Priority.EXPERIMENT_CLASS_MAX_PRIORITY);
			}

			if (handler.handle(experiment, experiment.getPriority())) {

				// report
				report.report(experiment, Reports.SCHEDULE, Reports.CLASS);
			}
		} catch (Exception e) {
			throw new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

	}

	private void storeSample(Connection connection, ExperimentSample sample)
			throws Exception {
		logger.info("store file: " + sample.getName());
		FileStoreUtil util = new FileStoreUtil();
		util.storeFile(report, sample.getName(), connection);
	}

	public void storeSample(ExperimentSample sample, String column, String key)
			throws BinBaseException {
		validateKey(key);
		Connection connection = getConnection(column);

		try {
			storeSample(connection, sample);
		} catch (Exception e) {
			throw new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerReImportClassById(java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	public void triggerReImportClassById(String database, String id, String key)
			throws BinBaseException {
		validateKey(key);
		triggerImportClassSecure(getStoredClass(database, id, key), key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerReImportExperimentById(java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	public void triggerReImportExperimentById(String id, String database,
			String key) throws BinBaseException {
		validateKey(key);

		Experiment exp = this.getExperiment(database, id, key);
		for (int i = 0; i < exp.getClasses().length; i++) {
			triggerImportClassSecure(exp.getClasses()[i], key);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerReImportClassesById(java.lang.String, java.lang.String[],
	 * java.lang.String)
	 */
	public void triggerReImportClassesById(String database, String id[],
			String key) throws BinBaseException {
		validateKey(key);

		for (int i = 0; i < id.length; i++) {
			triggerImportClassSecure(getStoredClass(database, id[i], key), key);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerUpdateDatabase(java.lang.String, java.lang.String)
	 */
	public void triggerUpdateDatabase(String database, String key)
			throws BinBaseException {
		validateKey(key);

		try {
			UpdateBinBaseJob update = new UpdateBinBaseJob();
			update.setColumn(database);

			// it can take a lot of time so should only run if the queue is
			// pretty empty
			if (Configurator.getStatusService().listDatabaseUpdateJobs()
					.contains(update) == false) {
				if (handler.handle(update, Priority.PRIORITY_NORMAL)) {
					// report
					report.report(update, Reports.SCHEDULE,
							Reports.UPDATE_DATABASE);
				}
			} else {
				logger.info("current job was already in the queue: " + update);
			}
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * createCustomizedExperiment(java.lang.String, java.lang.String[],
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	public String createCustomizedExperiment(String database,
			String[] samplesNames, String sendToEmail, String sopToUse,
			String key) throws BinBaseException {
		validateKey(key);

		logger.info("create customized import/export");

		ExperimentClass clazz = new ExperimentClass();
		Experiment experiment = new Experiment();
		ExperimentSample sample[] = new ExperimentSample[samplesNames.length];

		experiment.setEmail(sendToEmail);
		experiment.setSopUrl(sopToUse);

		for (int i = 0; i < sample.length; i++) {
			logger.info("add sample: " + sample[i]);
			sample[i] = new ExperimentSample();
			sample[i].setId(samplesNames[i]);
			sample[i].setName(samplesNames[i]);
		}

		clazz.setSamples(sample);

		String id = "BBSX-" + clazz.hashCode();

		logger.info("created id: " + id);
		clazz.setId(id);
		experiment.setId(id);

		this.triggerImportClassSecure(clazz, key);
		this.triggerExportSecure(experiment, key);

		logger.info("done");
		return id;
	}

	/**
	 * checks if we have the specified netcdf sample
	 * 
	 * @param sampleName
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public boolean hasNetCdfFile(String sampleName, String key)
			throws BinBaseException {
		validateKey(key);

		try {
			logger.debug("create export jmx connection");
			ExportJMXFacadeLocal local = ExportJMXFacadeUtil.getLocalHome()
					.create();
			return local.hasCDF(sampleName);
		} catch (BinBaseException e) {
			throw e;
		} catch (Exception e) {
			throw new BinBaseException(e);
		} finally {
			logger.debug("done with providing netcdf files");
		}
	}

	/**
	 * checks if we have the specified netcdf sample
	 * 
	 * @param sampleName
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public boolean hasTextFile(String sampleName, String key)
			throws BinBaseException {
		validateKey(key);

		try {
			logger.debug("create export jmx connection");
			ServiceJMXFacadeLocal local = ServiceJMXFacadeUtil.getLocalHome()
					.create();

			return local.sampleExist(sampleName);
		} catch (BinBaseException e) {
			throw e;
		} catch (Exception e) {
			throw new BinBaseException(e);
		} finally {
			logger.debug("done with providing netcdf files");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * getNetCdfFile(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public byte[] getNetCdfFile(String sampleName, String key)
			throws BinBaseException {
		try {
			validateKey(key);
			logger.debug("create export jmx connection");
			ExportJMXFacadeLocal local = ExportJMXFacadeUtil.getLocalHome()
					.create();

			byte[] content = local.downloadFile(sampleName);

			if (content == null) {

				throw new BinBaseException(
						"sorry couldn't find the object in all paths: "
								+ sampleName);
			}
			return content;
		} catch (BinBaseException e) {
			throw e;
		} catch (Exception e) {
			throw new BinBaseException(e);
		} finally {
			logger.debug("done with providing netcdf files");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * getAvailableSops(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public String[] getAvailableSops(String key) throws BinBaseException {
		validateKey(key);
		List<String> result;
		try {
			result = Configurator.getExportService().listSops();
			String[] resultS = new String[result.size()];

			for (int i = 0; i < result.size(); i++) {
				resultS[i] = result.get(i);
			}
			return resultS;
		} catch (RemoteException e) {
			throw new BinBaseException(e);
		} catch (CreateException e) {
			throw new BinBaseException(e);
		} catch (NamingException e) {
			throw new BinBaseException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#uploadSop
	 * (java.lang.String, java.lang.String, java.lang.String)
	 */
	public void uploadSop(String key, String name, String xmlContent)
			throws BinBaseException {
		validateKey(key);
		try {
			Configurator.getExportService().uploadSop(name,
					xmlContent.getBytes());
		} catch (Exception e) {
			throw new BinBaseException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * updateInternalMetaDataForSample(java.lang.String, java.lang.String,
	 * edu.ucdavis
	 * .genomics.metabolomics.binbase.bci.server.types.ExperimentSample)
	 */
	public void updateInternalMetaDataForSample(String key, String database,
			ExperimentSample sample) throws BinBaseException {
		validateKey(key);

		try {
			// ok validation is done now try to upload the data

			UpdateExperimentSample up = new UpdateExperimentSample();
			up.setDatabase(database);
			up.setId(sample.getId());
			up.setName(sample.getName());
			up.setXmlContent(null);

			if (handler.handle(up, Priority.PRIORITY_WHENEVER)) {
				report.report(up, Reports.SCHEDULE,
						new ReportType("update sample",
								"updating a sample with existing data"));
			}
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * updateMetaDataForSample(java.lang.String, java.lang.String,
	 * edu.ucdavis.genomics
	 * .metabolomics.binbase.bci.server.types.ExperimentSample,
	 * java.lang.String)
	 */
	public void updateMetaDataForSample(String key, String database,
			ExperimentSample sample, String xmlContent) throws BinBaseException {
		validateKey(key);

		try {

			UpdateExperimentSample up = new UpdateExperimentSample();
			up.setDatabase(database);
			up.setId(sample.getId());
			up.setName(sample.getName());
			up.setXmlContent(xmlContent);

			if (handler.handle(up, Priority.PRIORITY_WHENEVER)) {
				report.report(up, Reports.SCHEDULE,
						new ReportType("update sample",
								"updating a sample with existing data"));
			}
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerCalculateSample(java.lang.String, java.lang.String,
	 * edu.ucdavis.genomics
	 * .metabolomics.binbase.bci.server.types.ExperimentSample)
	 */
	public void triggerCalculateSample(String key, String database,
			ExperimentSample sample) throws BinBaseException {
		validateKey(key);

		try {
			CalculateSample calcSample = new CalculateSample();
			calcSample.setId(sample.getId());
			calcSample.setName(sample.getName());

			if (handler.handle(sample, Priority.PRIORITY_LOW)) {

				// report
				report.report(sample.getId(), Reports.SCHEDULE, Reports.SAMPLE);
			}
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService#
	 * triggerReCalculateSample(java.lang.String, java.lang.String,
	 * edu.ucdavis.genomics
	 * .metabolomics.binbase.bci.server.types.ExperimentSample)
	 */
	public void triggerReCalculateSample(String key, String database,
			ExperimentSample sample) throws BinBaseException {
		validateKey(key);

		try {
			CalculateSample calcSample = new CalculateSample();
			calcSample.setId(sample.getId());
			calcSample.setName(sample.getName());

			if (handler.handle(sample, Priority.PRIORITY_LOW)) {

				// report
				report.report(sample.getId(), Reports.SCHEDULE, Reports.SAMPLE);
			}
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * returns the registered columns
	 */
	public String[] getRegisteredColumns(String key) throws BinBaseException {
		validateKey(key);

		try {
			MBeanServer server = MBeanServerLocator.locate();

			ServiceJMXMBean bean = (ServiceJMXMBean) MBeanServerInvocationHandler
					.newProxyInstance(server, new ObjectName(
							"binbase:service=Import"), ServiceJMXMBean.class,
							false);

			return bean.getDatabases();
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * calculates if the sample is in need for recalculation
	 */
	public boolean isSampleInNeedOfRecalculation(String name, String database,
			String key) throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		boolean result = false;
		BinBaseException ex = null;
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("select a.sample_id from quantification a, samples b where a.sample_id = b.sample_id  and b.visible = 'TRUE' and b.sample_name = ? and a.version != (select count(*) from bin where export = 'TRUE')");
			statement.setString(1, name);
			ResultSet res = statement.executeQuery();

			result = res.next();
			res.close();
			statement.close();
		} catch (SQLException e) {
			try {
				statement.close();
			} catch (SQLException e1) {
				logger.debug(e.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
			ex = new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

		if (ex != null) {
			throw ex;
		}

		return result;

	}

	/**
	 * checks if all the samples of this experiment are actually finished and
	 * visible
	 * 
	 * @param exp
	 * @param database
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public boolean isExperimentAbleToBeRecalculated(Experiment exp,
			String database, String key) throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		boolean result = false;
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("select a.sample_id from Samples a where a.visible = 'TRUE' and a.finished = 'TRUE' and a.sample_name = ?");

			for (ExperimentClass clazz : exp.getClasses()) {
				for (ExperimentSample sample : clazz.getSamples()) {
					statement.setString(1, sample.getName());
					ResultSet res = statement.executeQuery();
					result = res.next();
					res.close();

					if (result == true) {
						return result;
					} else {
						logger.debug(exp.toString() + " - " + sample.getName()
								+ " is missing and can't be recalculated");

					}
				}
			}

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

		return result;

	}

	/**
	 * calculates if the experiment is in need for recalculation
	 */
	public boolean isExperimentInNeedOfRecalculation(Experiment exp,
			String database, String key) throws BinBaseException {
		validateKey(key);

		Connection connection = getConnection(database);
		boolean result = false;
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("select a.sample_id from quantification a, samples b where a.sample_id = b.sample_id  and b.visible = 'TRUE' and b.sample_name = ? and a.version != (select count(*) from bin where export = 'TRUE')");

			for (ExperimentClass clazz : exp.getClasses()) {
				for (ExperimentSample sample : clazz.getSamples()) {
					statement.setString(1, sample.getName());
					ResultSet res = statement.executeQuery();
					result = res.next();
					res.close();

					if (result == true) {
						return result;
					}

				}
			}

		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

		return result;

	}

	private void closeConnection(Connection connection) {
		if (connection instanceof WrappedConnection) {
			WrappedConnection c = (WrappedConnection) connection;
			try {
				c.getConnection().close();
			} catch (SQLException e) {
				logger.warn(e.getMessage(), e);
			}
		}
	}

	/**
	 * gives us a connection for the specified username
	 */
	public Connection getConnection(String userName) throws BinBaseException {
		// helps us that postgres has case sensitive usernames
		userName = userName.toLowerCase();
		// take care of a connection

		try {

			ConnectionFactory factory = ConnectionFactory.createFactory();
			Properties p = Configurator.getDatabaseService().createProperties();

			p.setProperty(SimpleConnectionFactory.KEY_USERNAME_PROPERTIE,
					userName);

			factory.setProperties(p);
			logger.debug("done with factory setup!");
			Connection c = factory.getConnection();

			return c;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e);
		}
	}

	public long getTimeStampForSample(String sample, String key)
			throws BinBaseException {
		validateKey(key);

		try {
			// calculate from file
			long date = new FileStoreUtil().calculateTimeStamp(sample);

			// calculate from the database to look if there is already a
			// timestamp stored, if this is the case we use the existing
			// timestamp
			return date;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e.getMessage());
		}
	}

	@Override
	public void triggerDSLCalculations(DSL dsl, String key)
			throws BinBaseException {
		validateKey(key);


		logger.info("using handler: " + handler);
		try {
			if (handler.handle(dsl, Priority.PRIORITY_NORMAL)) {
				report.report(dsl, Reports.SCHEDULE, Reports.DSL);
			}
		} catch (SendingException e) {
			throw new BinBaseException(e);
		}
	}
	
	/**
	 * triggers an update of the complete database
	 * @param database
	 * @param key
	 * @throws BinBaseException 
	 */
	public void triggerDBUpdate(String database, String key) throws BinBaseException{
		validateKey(key);
		

		Connection connection = getConnection(database);
		PreparedStatement statement = null;

		try {
			statement = connection
					.prepareStatement("select sample_id,sample_name from samples where visible = 'TRUE' and sample_id not in (select sample_id from quantification) union select a.sample_id,sample_name from quantification a, samples b where a.version !=  (select (count(bin_id)) from bin where export = 'TRUE') and a.sample_id = b.sample_id and b.visible = 'TRUE'");

			ResultSet set = statement.executeQuery();
			
			//should be the same as the binbase cpu reqiurement to avoid overloading the cluster
			int batchSize = 10;
			
			while(set.next()){
				PostmatchingJob job = new PostmatchingJob();
				int counter = 1;
				
				job.addSampleID(new ExperimentSample(""+set.getInt(0), set.getString(1)));
				job.setDatabase(database);
			
				//fill the batch set
				while(set.next() && counter < batchSize){
					job.addSampleID(new ExperimentSample(""+set.getInt(0), set.getString(1)));
					job.setDatabase(database);
					counter = counter + 1;
				}
				
				//submit the job for execution
				if(handler.handle(job, Priority.PRIORITY_VERY_LOW)){
					report.report(job, Reports.SCHEDULE, Reports.SAMPLE_POSTMATCH_JOB);
				}
			}
		
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e);
		} finally {
			closeConnection(connection);
		}

	}
	
}
