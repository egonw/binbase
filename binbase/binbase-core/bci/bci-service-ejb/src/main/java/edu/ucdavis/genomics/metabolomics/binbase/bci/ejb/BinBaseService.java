package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;

import javax.ejb.Remote;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.CommunicationExcpetion;

@Remote
public interface BinBaseService extends Serializable {

	static final long serialVersionUID = 2L;

	/**
	 * stores a sample on the server side
	 * 
	 * @param sample
	 * @param column
	 * @param key
	 * @throws BinBaseException
	 */
	public void storeSample(ExperimentSample sample, String column, String key) throws BinBaseException;

	/**
	 * makes shore this experiment is complete and can actually be recalculated
	 * and not that some samples or so are missing or set to invisible for what
	 * ever reason
	 * 
	 * @param exp
	 * @param database
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public boolean isExperimentAbleToBeRecalculated(Experiment exp, String database, String key) throws BinBaseException;

	/**
	 * gets the internal id for the latest version of this sample
	 * 
	 * @param sample
	 * @param database
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public int getSampleIdForLatestVersion(ExperimentSample sample, String database, String key) throws BinBaseException;

	/**
	 * returns true if the experiment is outdated and needs to be recalculated
	 * 
	 * @param exp
	 * @param database
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public boolean isExperimentInNeedOfRecalculation(Experiment exp, String database, String key) throws BinBaseException;

	/**
	 * calculates if the sample is actually in need of recalculation
	 * 
	 * @param name
	 * @param database
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public abstract boolean isSampleInNeedOfRecalculation(String name, String database, String key) throws BinBaseException;

	/**
	 * returns all the registered database
	 */
	public abstract String[] getRegisteredColumns(String key) throws BinBaseException;

	/**
	 * checks if the database contain sthis sample
	 */
	public abstract boolean containsSample(String sampleName, String database, String key) throws BinBaseException;

	/**
	 * checks if this sample is in the database
	 */
	public abstract boolean containsSampleObject(ExperimentSample sample, String database, String key) throws BinBaseException;

	/**
	 * returns all experiment ids
	 */
	public abstract String[] getAllExperimentIds(String database, String key) throws BinBaseException;

	/**
	 * returns the specified experiment
	 */
	public abstract Experiment getExperiment(String database, String id, String key) throws BinBaseException;

	/**
	 * return all sample id which are not really of any use anymore because we
	 * have already newer version in the database
	 */
	public abstract String[] getOutDatedSampleIds(String database, String key) throws BinBaseException;

	/**
	 * returns the class definition for the specified class id
	 */
	public abstract ExperimentClass getStoredClass(String database, String id, String key) throws BinBaseException;

	/**
	 * returns all stored classes in the database
	 */
	public abstract ExperimentClass[] getStoredClasses(String database, String key) throws BinBaseException;

	/**
	 * returns all stored samples in the database
	 */
	public abstract ExperimentSample[] getStoredSample(String database, String setupXId, String key) throws BinBaseException;

	/**
	 * exports and already stored experiment again by the given id
	 */
	public abstract void triggerExportById(String id, String database, String key) throws BinBaseException;

	/**
	 * secure export method which stores and exports the given experiment
	 * definiton
	 */
	public abstract void triggerExportSecure(Experiment experiment, String key) throws CommunicationExcpetion;

	/**
	 * exports the given class
	 */
	public abstract void triggerExportClass(ExperimentClass experiment, String key) throws BinBaseException;

	/**
	 * exports a class by its id instead of its definition
	 */
	public abstract void triggerExportClassById(String database, String id, String key) throws BinBaseException;

	/**
	 * exports the specified classes
	 */
	public abstract void triggerExportClasses(ExperimentClass[] experiment, String key) throws BinBaseException;

	/**
	 * exports the specified classes by there ids
	 */
	public abstract void triggerExportClassesById(String database, String ids[], String key) throws BinBaseException;

	/**
	 * exports the complete database
	 */
	public abstract void triggerExportDatabase(String database, String key) throws BinBaseException;

	/**
	 * stores the class in the database and calculates the matchings
	 */
	public abstract void triggerImportClassSecure(ExperimentClass experiment, String key) throws BinBaseException;

	/**
	 * recalculates the already stored class in the database
	 */
	public abstract void triggerReImportClassById(String database, String id, String key) throws BinBaseException;

	/**
	 * recalculates the already stored experiment
	 */
	public abstract void triggerReImportExperimentById(String id, String database, String key) throws BinBaseException;

	/**
	 * recalculates the already stored classes by there ids
	 */
	public abstract void triggerReImportClassesById(String database, String id[], String key) throws BinBaseException;

	/**
	 * recalculate the complete database
	 */
	public abstract void triggerUpdateDatabase(String database, String key) throws BinBaseException;

	/**
	 * generates a customized experiment based on the sample names
	 */
	public abstract String createCustomizedExperiment(String database, String[] samplesNames, String sendToEmail, String sopToUse, String key)
			throws BinBaseException;

	/**
	 * returns the specified netcdf file from the server
	 */
	public abstract byte[] getNetCdfFile(String sampleName, String key) throws BinBaseException;

	/**
	 * checks if this result file exists
	 * @param sampleName
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public boolean hasNetCdfFile(String sampleName, String key) throws BinBaseException;

	/**
	 * checks if this txt file exist
	 * 
	 * @param sampleName
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public boolean hasTextFile(String sampleName, String key) throws BinBaseException;
	
	/**
	 * returns the ids of all the available sops
	 */
	public abstract String[] getAvailableSops(String key) throws BinBaseException;

	/**
	 * uploads a new sop to the server
	 */
	public abstract void uploadSop(String key, String name, String xmlContent) throws BinBaseException;

	/**
	 * updates the metadata for this sample
	 */
	public abstract void updateInternalMetaDataForSample(String key, String database, ExperimentSample sample) throws BinBaseException;

	/**
	 * uploads predefiend meta data for this sample
	 */
	public abstract void updateMetaDataForSample(String key, String database, ExperimentSample sample, String xmlContent) throws BinBaseException;

	/**
	 * caluclates only this particular sample
	 */
	public abstract void triggerCalculateSample(String key, String database, ExperimentSample sample) throws BinBaseException;

	/**
	 * calculate only this particular sample
	 */
	public abstract void triggerReCalculateSample(String key, String database, ExperimentSample sample) throws BinBaseException;

	/**
	 * returns the timestamp for this sample on the harddrive or complains if it
	 * wasn't found
	 * 
	 * @param sample
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	public abstract long getTimeStampForSample(String sample, String key) throws BinBaseException;

	/**
	 * triggers a dsl calculation
	 * @param dslContent
	 * @param key
	 * @throws BinBaseException
	 */
	public abstract void triggerDSLCalculations(DSL dslContent, String key) throws BinBaseException;
	
	/**
	 * triggers an event in the system to recalculate the complete database sample by sample by sample
	 * @param database
	 * @param key
	 * @throws BinBaseException
	 */
	public void triggerDBUpdate(String database, String key) throws BinBaseException;
}
