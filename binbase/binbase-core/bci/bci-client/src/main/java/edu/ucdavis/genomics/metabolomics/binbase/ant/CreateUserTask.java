package edu.ucdavis.genomics.metabolomics.binbase.ant;

import javax.naming.NamingException;

import org.apache.tools.ant.BuildException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;

/**
 * creates a user
 * 
 * @author wohlgemuth
 */
public class CreateUserTask extends UserTask {

	String userName;

	String password;

	String applicationServer;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getApplicationServer() {
		return applicationServer;
	}

	public void setApplicationServer(String applicationServer) {
		this.applicationServer = applicationServer;
	}

	@Override
	public void execute() throws BuildException {
		if (this.getUserName() == null) {
			throw new BuildException("please provide a username");
		}

		if (this.getPassword() == null) {
			throw new BuildException("please provide a password");
		}

		if (this.getApplicationServer() == null) {
			throw new BuildException("please provide the ip of the application server");
		}

		try {
			this.getService(this.getApplicationServer()).addUser(new User(this.getPassword(), this.getUserName(), this.getUserName().hashCode(), true));
		}
		catch (AuthentificationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (NamingException e) {
			throw new BuildException(e);
		}
	}

}
