package edu.ucdavis.genomics.metabolomics.binbase;

import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.Authentificator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateAuthentificatorFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SchedulerBean;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificatorServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;

/**
 * simple access to the most imporatant binbase services, basically the
 * scheduler
 * 
 * @author wohlgemuth
 */
public class BinBaseServices {

	/**
	 * access to the scheduler
	 * 
	 * @return
	 * @throws NamingException
	 */
	public static Scheduler getScheduler() throws NamingException {
		return (Scheduler) EjbClient.getRemoteEjb(Configurator.BCI_GLOBAL_NAME, SchedulerBean.class);
	}

	/**
	 * access to the complete service
	 * 
	 * @return
	 * @throws NamingException
	 */
	public static BinBaseService getService() throws NamingException {
		return (BinBaseService) EjbClient.getRemoteEjb(Configurator.BCI_GLOBAL_NAME, BinBaseServiceBean.class);
	}

	/**
	 * access to the user management
	 * 
	 * @return
	 * @throws NamingException
	 */
	public static SimpleAuthentificationService getUserManagement() throws NamingException {
		return (SimpleAuthentificationService) EjbClient.getRemoteEjb(Configurator.BCI_GLOBAL_NAME, SimpleAuthentificatorServiceBean.class);
	}

	/**
	 * access to the central authentification
	 * 
	 * @return
	 * @throws AuthentificationException
	 */
	public static Authentificator getCentralAuthentification() throws AuthentificationException {
		return DelegateAuthentificatorFactory.newInstance(DelegateAuthentificatorFactory.class.getName()).create();
	}

}
