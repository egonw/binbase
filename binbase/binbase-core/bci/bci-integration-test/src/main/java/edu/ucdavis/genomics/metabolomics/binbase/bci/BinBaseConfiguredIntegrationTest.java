package edu.ucdavis.genomics.metabolomics.binbase.bci;

import java.io.OutputStream;
import java.sql.Connection;

import org.apache.log4j.Logger;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.junit.After;

import edu.ucdavis.genomics.metabolomics.binbase.bci.io.ConfigSource;
import edu.ucdavis.genomics.metabolomics.binbase.bci.io.ResultDestination;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.ExtractDataFromDatabase;
import edu.ucdavis.genomics.metabolomics.util.database.test.InitializeDBUtil;

/**
 * a binbase test which is configured to run against a database and an
 * application server just needs to be extended for cluster support
 * 
 * @author wohlgemuth
 * 
 */
public abstract class BinBaseConfiguredIntegrationTest extends
		BinBaseConfiguredApplicationServerIntegrationTest {

	private ConnectionFactory factory;
	private DatabaseConnection dbunit;

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public void setUp() throws Exception {
		logger.info("super setup...");
		super.setUp();

		
		logger.info("start with calculation");
		XMLConfigurator.getInstance().addConfiguration(new ConfigSource("binbase.xml"));
		XMLConfigurator.getInstance().addConfiguration(new ConfigSource("sql.xml"));
		
		XMLConfigurator.getInstance().getXMLConfigable("binbase.config").printTree(System.out);

		logger.info("configure database service...");
		// set jmx variable to point to the correct database
		DatabaseJMXFacade database = Configurator.getDatabaseService();
		database.setDatabase(System.getProperty("Binbase.database"));
		database.setDatabaseServer(System.getProperty("Binbase.host"));
		database.setDatabaseServerType(System.getProperty("Binbase.type"));
		database.setDatabaseServerPassword(System
				.getProperty("Binbase.password"));

		logger.info("done...");

		logger.info("bring database in defined state...");
		factory = ConnectionFactory.getFactory();
		factory.setProperties(System.getProperties());

		logger.info("create connection...");
		Connection connection = factory.getConnection();
		try {
			IDataSet set = getDataSet();

			if(set == null){
				throw new BinBaseException("sorry you need to define a dataset first!");
			}
			
			InitializeDBUtil.initialize(connection, set);
		} catch (Exception e) {
			throw e;
		} finally {
			factory.close(connection);
		}
	}

	@After
	@Override
	public void tearDown() throws Exception {
		logger.info("teardown...");

		Connection connection = getConnection();
		logger.info("create xml datafile dump...");

		ResultDestination out = new ResultDestination(this.getClass()
				.getSimpleName()
				+ "-data.xml");
		OutputStream outstream = out.getOutputStream();
		logger.info("writing the content...");
		new ExtractDataFromDatabase().extractData(outstream, getConnection());
		logger.info("closing the streams");
		outstream.flush();
		outstream.close();
		logger.info("closing the connection");
		factory.close(connection);

		super.tearDown();
	}

	/**
	 * here we provide the actual dataset
	 * 
	 * @return
	 * @throws Exception
	 */
	protected abstract IDataSet getDataSet() throws Exception;

	public Connection getConnection() {
		return factory.getConnection();
	}

	public DatabaseConnection getDbunit() {
		return dbunit;
	}

}
