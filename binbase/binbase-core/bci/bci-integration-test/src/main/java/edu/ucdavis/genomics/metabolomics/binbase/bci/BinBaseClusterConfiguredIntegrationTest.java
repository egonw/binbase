package edu.ucdavis.genomics.metabolomics.binbase.bci;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.junit.After;
import org.junit.Before;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractClusterTest;

/**
 * used to test the complete BinBase cluster funtctionality including database
 * and jmx configuration
 * 
 * @author wohlgemuth
 * 
 */
public abstract class BinBaseClusterConfiguredIntegrationTest extends AbstractClusterTest {

	private IDataSet dataset = null;

	protected Logger logger = Logger.getLogger(getClass());
	private BinBaseConfiguredIntegrationTest internal = new BinBaseConfiguredIntegrationTest(){

		@Override
		protected IDataSet getDataSet() {
			return dataset;
		}};

	@Before
	public void setUp() throws Exception {
		logger.info("seeting up cluster service class...");
		super.setUp();
		this.dataset = getDataSet();
		logger.info("seeting up database service class...");
		internal.setUp();
	}

	@After
	public void tearDown() throws Exception {
		internal.tearDown();
		super.tearDown();
	}

	/**
	 * provides our dataset
	 * @return
	 * @throws Exception 
	 */
	protected abstract IDataSet getDataSet() throws Exception;

	public File createTestFile(String name, String content, String extension) throws IOException {
		return internal.createTestFile(name, content, extension);
	}

	public File createTestFile(String name, String extension) throws IOException {
		return internal.createTestFile(name, extension);
	}
}
