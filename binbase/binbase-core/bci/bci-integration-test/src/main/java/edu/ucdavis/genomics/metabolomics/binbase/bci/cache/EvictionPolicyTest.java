package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public abstract class EvictionPolicyTest {

	private EvictableCache cache;
	
	@Before
	public void setUp() throws Exception {
		this.cache = this.getCache();
		this.cache.addEvictionPolicy(this.getPolicy());
	}

	@After
	public void tearDown() throws Exception {
		this.cache.getPolicies().clear();
	}

	@Test
	public void testEvict() {
		for(int i = 0; i < 30; i++){
			this.cache.put("key_"+i, i);
			prepareEviction(cache);
			assertTrue(this.cache.contains("key_"+i));
		}
		System.out.println(this.cache.size());
		assertTrue(checkEvictionSuccessfull(cache));
	}

	/**
	 * does the check of the eviction was successfull
	 * @param cache
	 * @return
	 */
	protected abstract boolean checkEvictionSuccessfull(EvictableCache cache);
	
	/**
	 * prepares stuff for the eviction
	 * @param cache
	 */
	protected abstract void prepareEviction(EvictableCache cache);
	
	/**
	 * returns the policy
	 * @return
	 */
	public abstract EvictionPolicy getPolicy();
	
	/**
	 * returns the cache
	 * @return
	 * @throws Exception 
	 */
	public abstract EvictableCache getCache() throws Exception;
}
