package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public abstract class PersistableCacheTest extends CacheTest{

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPersistant() {
	}

	@Test
	public void testGetPersistantEntries() {
	}

	@Test
	public void testGetNotPersistantEntries() {
	}

	@Test
	public void testMakePersistant() {
	}

	@Override
	protected abstract PersistableCache getCache();

}
