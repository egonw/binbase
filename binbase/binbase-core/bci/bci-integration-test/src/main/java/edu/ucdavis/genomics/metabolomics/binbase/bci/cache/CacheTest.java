package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException;

public abstract class CacheTest {
	
	@Before
	public void setUp() throws Exception {
		this.getCache().clear();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testContains() throws CacheException, Exception {
		for(int i = 0; i < 10; i++){
			getCache().put("key_" + i, new Integer(i));
			assertTrue(getCache().contains("key_" + i));
		}
	}

	@Test
	public void testGet() throws CacheException, Exception {
		for(int i = 0; i < 10; i++){
			getCache().put("key_" + i, i);
			assertTrue(getCache().contains("key_" + i));
			assertTrue(getCache().get("key_" + i).equals(i));
		}
	}

	@Test
	public void testPut() throws CacheException, Exception {
		for(int i = 0; i < 10; i++){
			getCache().put("key_" + i, i);
			assertTrue(getCache().contains("key_" + i));
			assertTrue(getCache().get("key_" + i).equals(i));
		}
		System.out.println(getCache().size());
		assertTrue(getCache().size() == 10);
	}

	@Test
	public void testClear() throws CacheException, Exception {
		for(int i = 0; i < 10; i++){
			getCache().put("key_" + i, i);
		}
		assertTrue(getCache().size() == 10);
		getCache().clear();
		assertTrue(getCache().size() == 0); 
	}

	@Test	
	public void testSize() throws CacheException, Exception {
		for(int i = 0; i < 10; i++){
			getCache().put("key_" + i, i);
		}
		getCache().clear();
		assertTrue(getCache().size() == 0);
	}

	protected abstract Cache getCache() throws Exception;
}
