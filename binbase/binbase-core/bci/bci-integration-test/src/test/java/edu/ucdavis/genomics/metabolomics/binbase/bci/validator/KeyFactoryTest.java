package edu.ucdavis.genomics.metabolomics.binbase.bci.validator;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class KeyFactoryTest extends AbstractApplicationServerTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testGetKey() throws KeyStoreException, RemoteException, BinBaseException, CreateException, NamingException {
		String last = KeyFactory.newInstance().getKey();
		
		for(int i = 0; i < 50; i++){
			assertTrue(KeyFactory.newInstance().getKey().equals(last));
		}
		
		for(int i = 0; i < 50; i++){
			last = KeyFactory.newInstance().getKey();
			Configurator.getKeyManager().generateInternalKey();
			assertTrue(last.equals(KeyFactory.newInstance().getKey()) == false);
		}
	}

}
