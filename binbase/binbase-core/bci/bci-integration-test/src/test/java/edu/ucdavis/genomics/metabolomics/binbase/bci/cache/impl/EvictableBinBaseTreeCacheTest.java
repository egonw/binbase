package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl;


import org.junit.After;
import org.junit.Before;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCacheTest;

public class EvictableBinBaseTreeCacheTest extends EvictableCacheTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected EvictableCache getCache() throws Exception {
		return BinBaseTreeCache.getInstance();
	}

}
