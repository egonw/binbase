package edu.ucdavis.genomics.metabolomics.binbase.bci.validator;


import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.SimpleKeyStoreFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.KeyStoreService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class ValidatorFactoryTest extends AbstractApplicationServerTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
		KeyStoreService store;
		
		store = (KeyStoreService) KeyStoreFactory.newInstance(SimpleKeyStoreFactory.class.getName()).createKeyStore();
		store.deleteAllKeys();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	@Test
	public void createValidator() throws KeyStoreException, RemoteException, BinBaseException, CreateException, NamingException{
		ValidatorFactory factory = ValidatorFactory.newInstance();
		Validator validator = factory.createValidator();

		Configurator.getKeyManager().storeKey("test","sdasdsadasd");
		Assert.assertFalse(validator.isKeyValid("asdasjhdlkasd"));
		Assert.assertTrue(validator.isKeyValid("sdasdsadasd"));
		
		for(int i = 0; i < 10; i++){
			KeyStoreFactory.newInstance(SimpleKeyStoreFactory.class.getName()).createKeyStore().storeKey("app:" + i, "app:"+i);
			Assert.assertTrue(validator.isKeyValid("app:"+i));
			Assert.assertFalse(validator.isKeyValid("aPP:"+i));

		}
	}

}
