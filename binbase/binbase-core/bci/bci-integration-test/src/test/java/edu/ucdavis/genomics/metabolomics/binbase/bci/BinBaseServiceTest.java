package edu.ucdavis.genomics.metabolomics.binbase.bci;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

public class BinBaseServiceTest extends BinBaseConfiguredIntegrationTest {

	BinBaseService service = null;

	Logger logger = Logger.getLogger(getClass());

	@Before
	public void setUp() throws Exception {
		super.setUp();
		service = BinBaseServiceFactory.createFactory().createService();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testContainsSample() throws BinBaseException {
		assertTrue(service.containsSample("GENERATED", "test", KeyFactory.newInstance().getKey()) == true);
		assertTrue(service.containsSample("jdfklasjdklasd", "test", KeyFactory.newInstance().getKey()) == false);
	}

	@Test
	public void testContainsSampleObject() throws BinBaseException {
		assertTrue(service.containsSampleObject(new ExperimentSample("1", "GENERATED"), "test", KeyFactory.newInstance().getKey()) == true);
		assertTrue(service.containsSampleObject(new ExperimentSample("12", "fsdfdsfds"), "test", KeyFactory.newInstance().getKey()) == false);
	}

	@Test
	public void testGetAllExperimentIds() throws BinBaseException {
		assertTrue(service.getAllExperimentIds("test", KeyFactory.newInstance().getKey()).length == 0);
	}

	@Test
	public void testGetExperiment() throws BinBaseException {
		// would make more sense to throw an exception instead of creating an
		// empty result
		Experiment exp = service.getExperiment("test", "NOT_EXIST_YET", KeyFactory.newInstance().getKey());
		assertTrue(exp.getClasses().length == 0);

	}

	@Test
	public void testGetOutDatedSampleIds() throws BinBaseException {
		assertTrue(service.getOutDatedSampleIds("test", KeyFactory.newInstance().getKey()).length == 0);
	}

	@Test
	public void testGetStoredClass() throws BinBaseException {
		ExperimentClass clazz = service.getStoredClass("test", "MSP IMPORT", KeyFactory.newInstance().getKey());
		assertTrue(clazz.getSamples().length == 1);
		assertTrue(clazz.getId().equals("MSP IMPORT"));
		assertTrue(clazz.getSamples()[0].getName().equals("GENERATED"));
		assertTrue(clazz.getSamples()[0].getId().equals("1"));

	}

	@Test
	public void testGetStoredClasses() throws BinBaseException {
		ExperimentClass[] classes = service.getStoredClasses("test", KeyFactory.newInstance().getKey());
		System.out.println(classes.length);
		assertTrue(classes.length == 1);
	}

	@Test
	public void testGetNetCdfFile() throws IOException, BinBaseException {
		for (int i = 0; i < 10; i++) {
			File file = createTestFile("test", "tada" + i, "cdf");

			byte[] content = service.getNetCdfFile("test", KeyFactory.newInstance().getKey());

			String result = new String(content);

			assertTrue(result.equals("tada" + i));
		}
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new XmlDataSet(new ResourceSource("/xml/minimum-rtx5.xml").getStream());
	}

	@Test
	public void testTriggerExportSecure() throws Exception {
		Experiment exp = new Experiment();
		exp.setColumn("test");
		exp.setId("test");

		// create fictional class
		ExperimentClass clazz = new ExperimentClass();
		clazz.setId("test");
		clazz.setColumn("test");

		// create fictional samples
		int count = 5;
		ExperimentSample samples[] = new ExperimentSample[count];
		for (int i = 1; i <= count; i++) {
			samples[i - 1] = new ExperimentSample();
			String name = "";
			if (i < 10) {
				name = "080101abcdsa0" + i;
			} else {
				name = "080101abcdsa" + i;
			}
			name = name + "_1";

			assertTrue(this.createTestFile(name, "txt").exists());
			logger.info("file name is: " + name);
			samples[i - 1].setName(name);
			samples[i - 1].setId("sx_" + i);
		}

		// assign samples to class
		clazz.setSamples(samples);

		// assign class to experiment
		exp.setClasses(new ExperimentClass[] { clazz });

		// make sure queue is empty
		assertTrue(this.getMessageQueue().size() == 0);

		// trigger export
		service.triggerExportSecure(exp, KeyFactory.newInstance().getKey());

		// make sure that the queue contains now 1 exp to schedule
		assertTrue(this.getMessageQueue().size() == 1);

		// make sure message is of right instance
		assertTrue(this.getMessageQueue().get(0) instanceof ObjectMessage);

		// make sure content is of right instance
		ObjectMessage message = (ObjectMessage) this.getMessageQueue().get(0);

		assertTrue(message.getObject() instanceof Experiment);

		// validate the actual content
		Experiment clazz2 = (Experiment) message.getObject();

		assertTrue(clazz2.getColumn().equals(exp.getColumn()));
		assertTrue(clazz2.getId().equals(exp.getId()));
		assertTrue(clazz2.getClasses().length == 1);
	}

	@Test
	public void testTriggerImportClassSecure() throws Exception {

		// create fictional class
		ExperimentClass clazz = new ExperimentClass();
		clazz.setId("test");
		clazz.setColumn("test");

		// create fictional samples
		int count = 5;
		ExperimentSample samples[] = new ExperimentSample[count];
		for (int i = 1; i <= count; i++) {
			samples[i - 1] = new ExperimentSample();
			String name = "";
			if (i < 10) {
				name = "080101abcdsa0" + i;
			} else {
				name = "080101abcdsa" + i;
			}
			name = name + "_1";

			assertTrue(this.createTestFile(name, "txt").exists());
			logger.info("file name is: " + name);
			samples[i - 1].setName(name);
			samples[i - 1].setId("sx_" + i);
		}

		// assign samples to class
		clazz.setSamples(samples);

		// make sure queue is empty
		assertTrue(this.getMessageQueue().size() == 0);

		// trigger import
		service.triggerImportClassSecure(clazz, KeyFactory.newInstance().getKey());

		// make sure that the queue contains now 1 class to schedule
		assertTrue(this.getMessageQueue().size() == 1);

		// make sure message is of right instance
		assertTrue(this.getMessageQueue().get(0) instanceof ObjectMessage);

		// make sure content is of right instance
		ObjectMessage message = (ObjectMessage) this.getMessageQueue().get(0);

		assertTrue(message.getObject() instanceof ExperimentClass);

		// validate the actual content
		ExperimentClass clazz2 = (ExperimentClass) message.getObject();

		assertTrue(clazz2.getColumn().equals(clazz.getColumn()));
		assertTrue(clazz2.getId().equals(clazz.getId()));
		assertTrue(clazz2.getSamples().length == count);

	}
}
