package edu.ucdavis.genomics.metabolomics.binbase.bci;

import org.apache.log4j.Logger;
import org.junit.Test;
import static org.junit.Assert.*;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;

/**
 * tests if we can create/update/delete users and authentificate
 * 
 * @author wohlgemuth
 * 
 */
public class SimpleAuthentificatorTest extends AbstractApplicationServerTest {
	SimpleAuthentificationService service;

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public void setUp() throws Exception {
		super.setUp();
		service = (SimpleAuthentificationService) SimpleAuthentificatorFactory.newInstance(SimpleAuthentificatorFactory.class.getName()).create();

		// if exist remove old users
		logger.info("delete old objects...");
		for (User user : service.listUser()) {
			service.removeUser(user);
		}

		
		
		assertTrue(service.listUser().size() == 0);
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();

		// if exist remove old users
		logger.info("delete old objects...");
		for (User user : service.listUser()) {
			service.removeUser(user);
		}

		assertTrue(service.listUser().size() == 0);
	}

	@Test
	public void testDeleteAdd() throws AuthentificationException{

		int size = 10;

		// create users
		logger.info("add bunch of objects...");
		for (int i = 0; i < size; i++) {
			User user = new User("test_" + i, "test_" + i, 0, false);
			service.addUser(user);
		}

		assertTrue(service.listUser().size() == size);

		// make sure they exist
		for (int i = 0; i < size; i++) {
			User user = new User("test_" + i, "test_" + i, 0, false);
			assertTrue(service.existUser(user));
		}

		// delete users
		for (int i = 0; i < size; i++) {
			User user = new User("test_" + i, "test_" + i, 0, false);
			service.removeUser(user);
		}

		// make sure they dont exist
		for (int i = 0; i < size; i++) {
			User user = new User("test_" + i, "test_" + i, 0, false);
			assertTrue(service.existUser(user) == false);
		}

		// we should have 0 users now
		assertTrue(service.listUser().size() == 0);
		
	}
	@Test
	public void testAuthentificate() throws AuthentificationException {

		// test if we can authentificatate users
		for (int i = 0; i < 5; i++) {
			User user = new User("test_" + i, "test_" + i, 0, false);
			service.addUser(user);
			User user2 = service.authentificate("test_" + i, "test_" + i);

			assertTrue(user2.equals(user));
		}

	}
	
	@Test
	public void testUpdate() throws AuthentificationException{

		// test if we can authentificatate users
		for (int i = 0; i < 5; i++) {
			User user = new User("test_" + i, "test_" + i, 0, false);
			service.addUser(user);
			assertTrue(service.existUser(user));			
			user.setPassword("bla");
			service.updateUser(user);
			assertTrue(service.existUser(user));
			
			service.authentificate("test_" + i, "bla");
		}
		
	}
}
