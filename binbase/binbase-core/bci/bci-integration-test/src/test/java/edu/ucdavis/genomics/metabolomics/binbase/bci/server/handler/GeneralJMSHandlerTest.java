package edu.ucdavis.genomics.metabolomics.binbase.bci.server.handler;

import static org.junit.Assert.assertTrue;

import javax.jms.ObjectMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.BinBaseConfiguredApplicationServerIntegrationTest;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Priority;

/**
 * tests if the handler actually works as intended
 * @author wohlgemuth
 *
 */
public class GeneralJMSHandlerTest extends BinBaseConfiguredApplicationServerIntegrationTest  {

	GeneralJMSHandler handler;
	@Before
	public void setUp() throws Exception {
		super.setUp();
		handler = new GeneralJMSHandler();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
		handler = null;
	}

	@Test
	public void testHandle() throws Exception {
		//check that queue is really empty
		assertTrue(this.getMessageQueue().isEmpty());
		handler.handle("asap", Priority.PRIORITY_ASAP);
		
		//check that we scheduled 1 message
		assertTrue(this.getMessageQueue().size() == 1);
		
		//check if the message is correct
		assertTrue(this.getMessageQueue().get(0) instanceof ObjectMessage);
		
		//check if the content is of correct class
		assertTrue(((ObjectMessage)this.getMessageQueue().get(0)).getObject() instanceof String);
		
		//check if the content is of the correct value
		assertTrue(((ObjectMessage)this.getMessageQueue().get(0)).getObject().equals("asap"));
	}

}
