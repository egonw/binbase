package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;


import org.junit.After;
import org.junit.Before;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCache;

/**
 * very simple put and get test
 * @author Administrator
 *
 */
public class SimpleCacheTest extends CacheTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected Cache getCache() {
		return SimpleCache.getCache();
	}

}
