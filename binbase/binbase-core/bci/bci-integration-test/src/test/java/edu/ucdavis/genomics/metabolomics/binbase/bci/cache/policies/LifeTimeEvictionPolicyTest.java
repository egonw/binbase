package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.policies;


import org.junit.After;
import org.junit.Before;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictionPolicy;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictionPolicyTest;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCache;

public class LifeTimeEvictionPolicyTest extends EvictionPolicyTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected boolean checkEvictionSuccessfull(EvictableCache cache) {
		return cache.size() < 30;
	}

	@Override
	public EvictableCache getCache() throws Exception {
		return SimpleCache.getCache();
	}

	@Override
	public EvictionPolicy getPolicy() {
		return new LifeTimeEvictionPolicy(3);
	}

	@Override
	protected void prepareEviction(EvictableCache cache) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
