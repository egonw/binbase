package edu.ucdavis.genomics.metabolomics.binbase.bci;

import static org.junit.Assert.assertTrue;

import javax.jms.ObjectMessage;

import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SchedulerFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.job.SchedulerJob;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;

/**
 * tests if the scheduler works as he should
 * 
 * @author wohlgemuth
 * 
 */
public class SchedulerTest extends AbstractApplicationServerTest {

	@Test
	public void testImportScheduling() throws Exception {
		Scheduler scheduler = SchedulerFactory.createFactory().createScheduler();

		int complexity = 5;

		// ok queue needs to be emptied
		emptyMessageQueue();

		// test if the import works. Basically we are just filling the queue
		// right now
		for (int i = 0; i < complexity; i++) {
			ExperimentClass clazz = new ExperimentClass();
			clazz.setColumn("dadada");
			clazz.setId("dadasdA");
			clazz.setSamples(new ExperimentSample[]{new ExperimentSample("test","test")});
			scheduler.scheduleImport(clazz, KeyFactory.newInstance().getKey());

		}

		assertTrue(this.getMessageQueue().size() == (complexity));

		for (Object message : getMessageQueue()) {
			ObjectMessage mes = (ObjectMessage) message;
			assertTrue(mes.getObject() instanceof SchedulerJob);
			assertTrue(((SchedulerJob)mes.getObject()).getTask() instanceof ExperimentClass);
		}
	}
	

	@Test
	public void testExportScheduling() throws Exception {
		Scheduler scheduler = SchedulerFactory.createFactory().createScheduler();

		int complexity = 5;

		// ok queue needs to be emptied
		emptyMessageQueue();

		// test if the import works. Basically we are just filling the queue
		// right now
		for (int i = 0; i < complexity; i++) {
			ExperimentClass clazz = new ExperimentClass();
			clazz.setColumn("dadada");
			clazz.setId("dadasdA");
			clazz.setSamples(new ExperimentSample[]{new ExperimentSample("test","test")});

			Experiment exp = new Experiment();
			exp.setClasses(new ExperimentClass[]{clazz});
			exp.setColumn("dadada");
			exp.setId("dasdasda");
			
			scheduler.scheduleExport(exp, KeyFactory.newInstance().getKey());
		}

		assertTrue(this.getMessageQueue().size() == (complexity));

		for (Object message : getMessageQueue()) {
			ObjectMessage mes = (ObjectMessage) message;
			assertTrue(mes.getObject() instanceof SchedulerJob);
			assertTrue(((SchedulerJob)mes.getObject()).getTask() instanceof Experiment);			
		}
	}
}
