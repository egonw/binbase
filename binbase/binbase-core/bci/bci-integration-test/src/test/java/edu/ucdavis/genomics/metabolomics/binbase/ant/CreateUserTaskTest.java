package edu.ucdavis.genomics.metabolomics.binbase.ant;

import static org.junit.Assert.assertTrue;

import javax.naming.Context;

import org.apache.log4j.Logger;
import org.apache.tools.ant.Project;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.SimpleAuthentificatorFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;

/**
 * tests that the user is really created
 * @author wohlgemuth
 *
 */
public class CreateUserTaskTest extends AbstractApplicationServerTest {
	SimpleAuthentificationService service;

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public void setUp() throws Exception {
		super.setUp();
		service = (SimpleAuthentificationService) SimpleAuthentificatorFactory.newInstance(SimpleAuthentificatorFactory.class.getName()).create();

		// if exist remove old users
		logger.info("delete old objects...");
		for (User user : service.listUser()) {
			service.removeUser(user);
		}

		assertTrue(service.listUser().size() == 0);
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();

		// if exist remove old users
		logger.info("delete old objects...");
		for (User user : service.listUser()) {
			service.removeUser(user);
		}

		assertTrue(service.listUser().size() == 0);
	}

	@Test
	public void testExecute() throws AuthentificationException{
		CreateUserTask task = new CreateUserTask();
		task.setProject(new Project());
		
		task.setApplicationServer(System.getProperty(Context.PROVIDER_URL));
		task.setUserName("test");
		task.setPassword("alpha");

		task.execute();
		
		assertTrue(service.listUser().size() == 1);

		service.authentificate("test", "alpha");
	}
}
