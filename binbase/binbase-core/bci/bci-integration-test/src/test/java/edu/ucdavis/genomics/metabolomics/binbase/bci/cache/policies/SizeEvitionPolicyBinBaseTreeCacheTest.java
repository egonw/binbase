package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.policies;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.BinBaseTreeCache;

public class SizeEvitionPolicyBinBaseTreeCacheTest extends SizeEvitionPolicyTest{

	@Override
	public EvictableCache getCache() throws Exception {
		return BinBaseTreeCache.getInstance();
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

}
