package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCacheTest;

public class FileCacheTest extends EvictableCacheTest{

	@Override
	protected EvictableCache getCache() throws Exception {
		return FileCache.getInstance();
	}

}
