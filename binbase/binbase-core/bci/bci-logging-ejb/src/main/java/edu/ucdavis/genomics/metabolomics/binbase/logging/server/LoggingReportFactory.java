package edu.ucdavis.genomics.metabolomics.binbase.logging.server;

import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Report;

import javax.naming.NamingException;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Jun 29, 2009
 * Time: 6:48:06 PM
 */
public class LoggingReportFactory extends ReportFactory{

    /**
     * creates our logging report or throws a runtime exception
     * @param properties
     * @param s
     * @return
     */
    public Report create(Properties properties, String s) {
        try {
            return new LoggingReport(s);
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }
}
