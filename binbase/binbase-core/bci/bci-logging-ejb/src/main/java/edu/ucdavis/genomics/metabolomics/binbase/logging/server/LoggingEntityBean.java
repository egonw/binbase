package edu.ucdavis.genomics.metabolomics.binbase.logging.server;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.ucdavis.genomics.metabolomics.binbase.filter.IPRequest;

/**
 * store logging experiments in a local database
 * 
 * @author wohlgemuth
 */
@Entity
@Table
public class LoggingEntityBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = false)
	private String hostName;

	@Column(nullable = false)
	private String hostAddress;

	@Column(nullable = false)
	private String uri;

	@Column(nullable = false)
	private int port;

	@Column(nullable = false, name = "recieved")
	private Date time;

	public LoggingEntityBean(String hostName, String hostAddress, String uri, int port, Date date) {
		super();
		this.hostName = hostName;
		this.hostAddress = hostAddress;
		this.uri = uri;
		this.port = port;
		this.time = date;
	}

	/**
	 * generate it directly by an iprequests
	 * 
	 * @param request
	 */
	public LoggingEntityBean(IPRequest request) {
		this(request.getHost(), request.getAddress(), request.getUri(), request.getPort(), request.getDate());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		LoggingEntityBean that = (LoggingEntityBean) o;

		return that.getId() == this.getId();
	}

	@Override
	public int hashCode() {
		int result = (int) (id ^ (id >>> 32));
		result = 31 * result + hostName.hashCode();
		result = 31 * result + hostAddress.hashCode();
		result = 31 * result + uri.hashCode();
		result = 31 * result + port;
		result = 31 * result + time.hashCode();
		return result;
	}

	public LoggingEntityBean() {

	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTime() {
       
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * converts the ejb to an ip request
	 * 
	 * @return
	 */
	public IPRequest toRequest() {
		return new IPRequest(this.getHostName(), this.getUri(), this.getHostAddress(), this.getPort(), this.getTime());
	}
}
