package edu.ucdavis.genomics.metabolomics.binbase.logging.server;

import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import edu.ucdavis.genomics.metabolomics.binbase.filter.FilterReports;
import edu.ucdavis.genomics.metabolomics.binbase.filter.IPRequest;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;

import javax.naming.NamingException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Jun 29, 2009
 * Time: 6:13:39 PM

 *
 * simple reporter to store data in database of they are of the right type
 */
public class LoggingReport extends Report{
    private String owner;

    /**
     * internal logging service
     */
    private LoggingService service;

    public LoggingReport(String owner) throws NamingException {

        this.owner = owner;

        service = (LoggingService) EjbClient.getRemoteEjb(Configurator.BCI_GLOBAL_NAME, LoggingServiceBean.class);
        
    }

    /**
     * stores all valid objects in the database
     * @param s
     * @param serializable
     * @param reportEvent
     * @param reportType
     * @param s1
     * @param date
     * @param e
     */
    public void report(String s, Serializable serializable, ReportEvent reportEvent, ReportType reportType, String s1, Date date, Exception e) {
        if(reportEvent.equals(FilterReports.FITLER_URI_ACCESS)){
            if(reportType.equals(FilterReports.FILTER_REQUEST)){
                if(serializable instanceof IPRequest){
                service.registerEvent((IPRequest) serializable);
                }
            }
        }
    }

    public String getOwner() {
        return owner;  
    }
}
