package edu.ucdavis.genomics.metabolomics.binbase.logging.server;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.bm.testsuite.BaseSessionBeanFixture;

import edu.ucdavis.genomics.metabolomics.binbase.filter.IPRequest;

import javax.persistence.EntityTransaction;


/**
 * mock based test for our ejb
 */
public class LoggingServiceTest extends BaseSessionBeanFixture<LoggingServiceBean> {

    public LoggingServiceTest() {
        super(LoggingServiceBean.class, new Class[]{LoggingEntityBean.class});
    }

    List<Date> dates = new Vector<Date>();

    Logger logger = Logger.getLogger(getClass());

    int testSize = 0;

    String[] hostNames = null;

    String[] addres = null;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        testSize = 100;

        hostNames = new String[]{"tada", "alpa"};

        addres = new String[]{"127.0.0.1", "127.0.0.2"};


        LoggingServiceBean bean = this.getBeanToTest();

        assertTrue(bean != null);

        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_YEAR, 1);
        for (int i = 0; i < testSize; i++) {
            c.roll(c.DAY_OF_YEAR, true);
            Date time = c.getTime();

            for (int x = 0; x < hostNames.length; x++) {
                for (int y = 0; y < addres.length; y++) {
                    bean.registerEvent(new IPRequest(hostNames[x], "htp://localhost:8080/test.html", addres[y], 80, time));
                }
            }
            if (dates.contains(time) == false) {
                dates.add(time);
            }

        }

        assertTrue(dates.size() == testSize);

        List<LoggingEntityBean> result = this.getEntityManager().createQuery("from " + LoggingEntityBean.class.getName() + " b").getResultList();
        assertTrue(result.isEmpty() == false);

        int size = result.size();
        int exspected = testSize * addres.length * hostNames.length;

        logger.info(size);
        logger.info(exspected);
        assertTrue(size == exspected);

    }

    @After
    public void tearDown() throws Exception {

        EntityTransaction t = this.getEntityManager().getTransaction();
        t.begin();

        for (Object bean : this.getEntityManager().createQuery("from " + LoggingEntityBean.class.getName() + " b").getResultList()) {
            this.getEntityManager().remove(bean);
        }

        t.commit();
    }

    @Test
    public void testGetRequestString() {
        for (String x : hostNames) {
            List<IPRequest> requests = this.getBeanToTest().getRequest(x);

            assertTrue(requests.size() == (testSize * addres.length));
        }
    }

    @Test
    public void testGetRequestStringDateDate() {

        Date begin = dates.get(0);
        Date end = dates.get(dates.size() - 1);

        for (String s : hostNames) {
            List<IPRequest> result = this.getBeanToTest().getRequest(s, begin, end);

            assertTrue(result != null);
            logger.info("result: " + result);

            assertFalse(result.isEmpty());

            logger.info(result.size());
            assertTrue(result.size() == this.addres.length * testSize);
        }
    }

    @Test
    public void testGetRequestDate() {

        Date begin = dates.get(0);

        List<IPRequest> result = this.getBeanToTest().getRequest(begin);

        assertTrue(result != null);
        logger.info("result: " + result);

        assertFalse(result.isEmpty());

        logger.info(result.size());
        assertTrue(result.size() == this.hostNames.length * this.addres.length);
    }

    @Test
    public void testGetRequestDateDate() {

        Date begin = dates.get(0);
        Date end = dates.get(dates.size() - 1);

        List<IPRequest> result = this.getBeanToTest().getRequest(begin, end);

        assertTrue(result != null);
        logger.info("result: " + result);

        assertFalse(result.isEmpty());

        logger.info(result.size());
        assertTrue(result.size() == this.hostNames.length * this.addres.length * testSize);

    }

    @Test
    public void testGetHostnames() {
        assertTrue(this.getBeanToTest().getHostnames().size() == this.hostNames.length);

        for (String s : this.hostNames) {
            assertTrue(this.getBeanToTest().getHostnames().contains(s));
        }
    }

    @Test
    public void testGetHostnamesDate() {

        for (Date d : dates) {
            assertTrue(this.getBeanToTest().getHostnames(d).size() == this.hostNames.length);

            for (String s : this.hostNames) {
                assertTrue(this.getBeanToTest().getHostnames(d).contains(s));
            }
        }
    }

    @Test
    public void testGetHostnamesDateDate() {

        Date begin = dates.get(0);
        Date end = dates.get(dates.size() - 1);

        List<String> result = this.getBeanToTest().getHostnames(begin, end);

        assertTrue(result != null);
        logger.info("result: " + result);

        assertFalse(result.isEmpty());
        assertTrue(result.size() == this.hostNames.length);

        for (String s : this.hostNames) {
            assertTrue(this.getBeanToTest().getHostnames(begin, end).contains(s));
        }
    }

    @Test
    public void testGetAddresses() {
        assertTrue(this.getBeanToTest().getAddresses() != null);
        logger.info(this.getBeanToTest().getAddresses());
        assertTrue(this.getBeanToTest().getAddresses().size() == this.addres.length);

        for (String s : this.addres) {
            assertTrue(this.getBeanToTest().getAddresses().contains(s));
        }
    }

    @Test
    public void testGetAddressesDate() {

        for (Date d : dates) {
            assertTrue(this.getBeanToTest().getAddresses(d).size() == this.addres.length);

            for (String s : this.addres) {
                assertTrue(this.getBeanToTest().getAddresses(d).contains(s));
            }
        }
    }

    @Test
    public void testGetAddressesDateDate() {

        Date begin = dates.get(0);
        Date end = dates.get(dates.size() - 1);

        List<String> result = this.getBeanToTest().getAddresses(begin, end);

        assertTrue(result != null);
        logger.info("result: " + result);

        assertFalse(result.isEmpty());
        assertTrue(result.size() == this.addres.length);

        for (String s : this.addres) {
            assertTrue(this.getBeanToTest().getAddresses(begin, end).contains(s));
        }
    }

    @Test
    public void testGetRequestStringDate() {
        for (String x : hostNames) {
            for (Date d : dates) {
                logger.info("testing for date: " + d);
                List<IPRequest> requests = this.getBeanToTest().getRequest(x, d);

                assertTrue(requests != null);

                for (IPRequest r : requests) {
                    logger.info(r);
                }
                assertTrue(requests.size() == hostNames.length);
            }
        }
    }
    //
}
