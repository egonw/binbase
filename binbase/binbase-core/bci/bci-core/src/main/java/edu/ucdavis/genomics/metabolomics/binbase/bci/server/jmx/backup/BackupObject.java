package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.backup;

import java.io.Serializable;

/**
 * 
 * @author wohlgemuth
 *
 */
public class BackupObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	int priority;

	Serializable object;

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Serializable getObject() {
		return object;
	}

	public void setObject(Serializable object) {
		this.object = object;
	}
}
