package edu.ucdavis.genomics.metabolomics.binbase.bci.validator;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;

/**
 * creates access to the binbase key
 * 
 * @author wohlgemuth
 */
public abstract class KeyFactory {
	public static final String DEFAULT_PROPERTY_NAME = KeyFactory.class.getName();

	/**
	 * gets the registered key for binbase services
	 * 
	 * @return
	 * @throws BinBaseException 
	 */
	public abstract String getKey();

	public static KeyFactory newInstance() {
		return newInstance(System.getProperty(DEFAULT_PROPERTY_NAME));
	}

	public static KeyFactory newInstance(String factoryClass) {
		Class<?> classObject;
		KeyFactory factory;

		try {
			if (factoryClass == null) {
				return new KeyFactory() {

					Logger logger = Logger.getLogger(getClass());
					
					@Override
					public String getKey() {
						try {
							return Configurator.getKeyManager().getInternalKey();
						}
						catch(Exception e){
							logger.error(e.getMessage(),e);
							throw new RuntimeException(e.getMessage(),e);
						}
					}

				};
			}
			classObject = Class.forName(factoryClass);
			factory = (KeyFactory) classObject.newInstance();
			return factory;

		}
		catch (Exception e) {
			throw new FactoryException(e);
		}
	}
}
