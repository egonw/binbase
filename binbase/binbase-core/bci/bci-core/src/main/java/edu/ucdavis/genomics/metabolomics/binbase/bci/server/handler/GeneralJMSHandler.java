package edu.ucdavis.genomics.metabolomics.binbase.bci.server.handler;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.mail.MailService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.mail.MailServiceFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.exception.SendingException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

/**
 * a general handler to schedule jobs, should be all you ever need!
 * 
 * @author wohlgemuth
 */
public class GeneralJMSHandler {
	private Logger logger = Logger.getLogger(getClass());

	/**
	 * writes this experimentclass into a queue
	 * 
	 * @author wohlgemuth
	 * @version Dec 1, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.server.handler.ExperimentClassHandler#handle(edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass)
	 */
	public boolean handle(Serializable object, int priority) throws SendingException {
		try {

			// we use now a scheduler since this is easier than the old
			// implementation
			Scheduler.schedule(object, priority);

			if (Configurator.getNotifierService().isEnableEmail()) {
				try {
					MailService service = MailServiceFactory.newInstance().create();

					StatusJMXFacade facade = StatusJMXFacadeUtil.getHome(XMLConfigurator.getInstance().getProperties()).create();
					Collection<?> collection = facade.getNotificationsAdresses();
					Iterator<?> it = collection.iterator();

					while (it.hasNext()) {
						String email = it.next().toString();
						service.sendMessage(email, "scheduled action - " + object.toString(), object.toString());
					} 

					ReportFactory.newInstance().create("jms-handler").report(object, Reports.SEND_EMAIL, Reports.EXPERIMENT);

				}
				catch (Exception e) {
					logger.warn("error during sending notification!", e);
				}
			}

		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new SendingException(e);
		}

		return true;
	}

}
