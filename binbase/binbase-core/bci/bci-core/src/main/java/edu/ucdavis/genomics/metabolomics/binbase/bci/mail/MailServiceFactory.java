/*
 * Created on Nov 16, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.mail;

import java.util.Map;
import java.util.Properties;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * used to create authentificators
 * 
 * @author wohlgemuth
 * @version Nov 16, 2006
 * 
 */
public abstract class MailServiceFactory extends AbstractFactory {
	public static final String DEFAULT_PROPERTY_NAME = MailServiceFactory.class.getName();

	/**
	 * creates a new Authentificator
	 * 
	 * @author wohlgemuth
	 * @version Nov 16, 2006
	 * @param properties
	 * @param report
	 * @return
	 * @throws AuthentificationException
	 */
	public MailService create() throws BinBaseException {
		return create(System.getProperties());
	}

	public abstract MailService create(Properties p) throws BinBaseException;

	public static MailServiceFactory newInstance() {
		return newInstance(System.getProperties());
	}

	public static MailServiceFactory newInstance(Map<?, ?> properties) {
		return newInstance((String) properties.get(DEFAULT_PROPERTY_NAME));
	}

	/**
	 * returns an new instance of the specified factory
	 * 
	 * @param factoryClass
	 *            the specified factory to use
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static MailServiceFactory newInstance(String factoryClass) {
		Class<?> classObject;
		MailServiceFactory factory;

		try {
			if (factoryClass == null) {
				return new MailServiceFactory() {

					@Override
					public MailService create(Properties p) throws BinBaseException {
						return new SimpleMailService();
					}

				};
			} else {
				classObject = Class.forName(factoryClass);
			}

			factory = (MailServiceFactory) classObject.newInstance();
			return factory;

		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}
}
