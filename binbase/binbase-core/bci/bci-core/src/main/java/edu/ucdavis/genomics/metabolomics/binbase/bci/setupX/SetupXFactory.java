/*
 * created on $date
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.setupX;

import java.util.Properties;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;

/**
 * used to create setupX factory implementations
 * 
 * @author wohlgemuth
 * @version Nov 5, 2005 @
 */
public abstract class SetupXFactory {

	private static Logger logger = Logger.getLogger(SetupXFactory.class);

	/**
	 * name of the property
	 */
	public static final String DEFAULT_PROPERTY_NAME = SetupXFactory.class.getName();

	/**
	 * creates the provider itself
	 * 
	 * @author wohlgemuth
	 * @version Nov 17, 2005
	 * @return
	 */
	public abstract SetupXProvider createProvider(Properties p);

	/**
	 * creates a provider based on the system properties
	 * 
	 * @author wohlgemuth
	 * @version Nov 17, 2005
	 * @return
	 */
	public SetupXProvider createProvider() {
		return createProvider(System.getProperties());
	}

	/**
	 * returns an new instance of the factory
	 * 
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static SetupXFactory newInstance() {
		return newInstance(System.getProperty(DEFAULT_PROPERTY_NAME));
	}

	/**
	 * returns an new instance of the specified factory
	 * 
	 * @param factoryClass
	 *            the specified factory to use
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static SetupXFactory newInstance(String factoryClass) {
		Class classObject;
		SetupXFactory factory;

		try {
			if (factoryClass == null) {

				logger.info("creating default implementation");
				return new SetupXFactory() {

					@Override
					public SetupXProvider createProvider(Properties p) {
						return new SetupXProvider() {

							public String getMetaInformationByClass(String setupXId) throws BinBaseException {
								throw new BinBaseException("Sorry no class with an implementation was set!");
							}

							public String getMetaInformationByExperiment(String setupXId) throws BinBaseException {
								throw new BinBaseException("Sorry no class with an implementation was set!");
							}

							public String getMetaInformationBySample(String setupXId) throws BinBaseException {
								throw new BinBaseException("Sorry no class with an implementation was set!");
							}

							public String getSetupXId(String sampleName) throws BinBaseException {
								throw new BinBaseException("Sorry no class with an implementation was set!");
							}

							public void upload(String experimentId, String content) throws BinBaseException {
								throw new BinBaseException("Sorry no class with an implementation was set!");
							}

							@Override
							public boolean canCreateBins(String setupxId)
									throws BinBaseException {
								throw new BinBaseException("Sorry no class with an implementation was set!");
							}
						};
					}

				};
			}
			else {
				logger.info("creating factory of class: " + factoryClass);
				classObject = Class.forName(factoryClass);
				factory = (SetupXFactory) classObject.newInstance();
				return factory;
			}
		}
		catch (Exception e) {
			throw new FactoryException(e);
		}
	}

}
