package edu.ucdavis.genomics.metabolomics.binbase.util.netcdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import edu.ucdavis.genomics.metabolomics.util.io.Copy;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.math.Regression;
import edu.ucdavis.genomics.metabolomics.util.math.SpectraArrayKey;

/**
 * defines basic access to netcdf files related to the binbase
 * 
 * @author wohlgemuth
 * 
 */
public abstract class NetCDFData implements SpectraArrayKey {

	private String name;

	/**
	 * method to create a file from a source in case our implementation can only work on files on not streams
	 * 
	 * @param source
	 * @return
	 * @throws IOException
	 */
	protected static File getFile(Source source) throws IOException {
		File file = null;

		if (source instanceof FileSource) {
			return ((FileSource) source).getFile();
		} else {
			try {
				file = File.createTempFile(source.getSourceName(), ".binbase", new File(System.getenv("TMPDIR")));
			} catch (Exception e) {
				file = File.createTempFile(source.getSourceName(), ".binbase");
			}

			file.deleteOnExit();
			Copy.copy(source.getStream(), new FileOutputStream(file));
			return file;
		}
	}

	private double precision = 0.5;

	/**
	 * default to load and initialize a file
	 * 
	 * @param source
	 * @throws IOException
	 */
	public NetCDFData(Source source) throws IOException {
		prepare(source);
		this.setName(source.getSourceName());
		load(source);
	}

	/**
	 * loads a file
	 * 
	 * @param source
	 * @throws IOException
	 */
	protected abstract void load(Source source) throws IOException;

	/**
	 * prepare stuff
	 * 
	 * @author wohlgemuth
	 * @version Nov 6, 2006
	 * @param source
	 * @throws IOException
	 */
	protected void prepare(Source source) throws IOException {

	}

	/**
	 * returns the given spectra at this point or null
	 * 
	 * @param retentionTime
	 * @return
	 */
	public final double[][] getSpectra(double retentionTime) {
		return getSpectra(retentionTime, precision);
	}

	/**
	 * returns the spectra at this time +/- the given precision
	 * 
	 * @param retentionTime
	 * @param precision
	 * @return
	 */
	public final double[][] getSpectra(double retentionTime, double precision) {
		return getSpectra(retentionTime, precision, false);
	}

	/**
	 * returns all spectra at the given time combined to one entry
	 * 
	 * @param retentionTime
	 * @param precision
	 * @param combine
	 * @return
	 */
	public abstract double[][] getSpectra(double retentionTime, double precision, boolean combine);

	/**
	 * close the file and realease ressources
	 * 
	 * @author wohlgemuth
	 * @version Nov 8, 2006
	 */
	public abstract void close();

	/**
	 * sets the curve which should be used for correcting the times in this file so we can access them using retentionIndex instead of retentiontimes
	 * 
	 * we than calculate the right retention time from the given retention index
	 * 
	 * @author wohlgemuth
	 * @version Nov 8, 2006
	 */
	public abstract void setCorrectionCurve(Regression regression);

	/**
	 * contains for each ion the calculated noise
	 * 
	 * @author wohlgemuth
	 * @version Nov 9, 2006
	 * @return
	 */
	public abstract Map<Integer, Double> getNoise();

	/**
	 * returns the noise for a given range
	 * 
	 * @author wohlgemuth
	 * @version Nov 14, 2006
	 * @param time
	 *            time of masspec of interesst
	 * @param range
	 *            range for noise finding
	 * @return
	 */
	public abstract Map<Integer, Double> getNoise(double time, int range);

	public double getPrecision() {
		return precision;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	/**
	 * return all retention times for this massspec
	 * 
	 * @author wohlgemuth
	 * @version Jan 4, 2007
	 * @return
	 */
	public abstract double[] getTimes();

	/**
	 * calculates the tic for this chrommatogram
	 * 
	 * @author wohlgemuth
	 * @version Jan 4, 2007
	 * @return
	 */
	public List<Ion> calculateTic() {
		double[] times = this.getTimes();

		List<Ion> ions = new Vector<Ion>();

		for (int i = 0; i < times.length; i++) {
			double[][] tic = this.getSpectra(times[i]);

			if (tic != null) {
				double result = 0;
				for (int x = 1; x < tic.length; x++) {
					result = result + tic[x - 1][SpectraArrayKey.FRAGMENT_ABS_POSITION];
				}

				Ion ion = new Ion(times[i], result);
				ions.add(ion);
			}

		}
		return ions;
	}

	/**
	 * calculates the ion trace for the given mass
	 * 
	 * @author wohlgemuth
	 * @version Jan 4, 2007
	 * @param mass
	 * @return
	 */
	public List<Ion> calcuateIonTrace(int mass) {
		double[] times = this.getTimes();

		List<Ion> ions = new Vector<Ion>();

		for (int i = 0; i < times.length; i++) {
			double[][] tic = this.getSpectra(times[i]);

			if (tic != null) {

				Ion ion = new Ion(times[i], tic[mass - 1][SpectraArrayKey.FRAGMENT_ABS_POSITION]);
				ions.add(ion);
			}
		}
		return ions;
	}

	/**
	 * internal class to represent an ion
	 * 
	 * @author wohlgemuth
	 * @version Jan 4, 2007
	 * 
	 */
	public final class Ion {
		double retentionTime;

		double intensity;

		public Ion(double retentionTime, double intensity) {
			super();
			this.retentionTime = retentionTime;
			this.intensity = intensity;
		}

		public double getIntensity() {
			return intensity;
		}

		public double getRetentionTime() {
			return retentionTime;
		}

	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jan 4, 2007
	 * @return minimum retention time
	 */
	public double getMinRetentionTime() {
		return this.getTimes()[0];
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jan 4, 2007
	 * @return maximum retention time
	 */
	public double getMaxRetentionTime() {
		double[] times = this.getTimes();
		return times[times.length - 1];
	}

	/**
	 * returns the max intensity for the tic
	 * 
	 * @author wohlgemuth
	 * @version Jan 4, 2007
	 * @return
	 */
	public Ion getMaxIntensityTIC() {
		Collection<Ion> tic = this.calculateTic();
		return Collections.max(tic, new Comparator<Ion>() {

			public int compare(Ion o1, Ion o2) {
				return ((Double) o1.getIntensity()).compareTo(o2.getIntensity());
			}
		});
	}

	/**
	 * returns the max intensity for a given ion
	 * 
	 * @author wohlgemuth
	 * @version Jan 4, 2007
	 * @param ion
	 * @return
	 */
	public Ion getMaxIntensityTrace(int ion) {
		Collection<Ion> trace = this.calcuateIonTrace(ion);
		return Collections.max(trace, new Comparator<Ion>() {

			public int compare(Ion o1, Ion o2) {
				return ((Double) o1.getIntensity()).compareTo(o2.getIntensity());
			}
		});
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
