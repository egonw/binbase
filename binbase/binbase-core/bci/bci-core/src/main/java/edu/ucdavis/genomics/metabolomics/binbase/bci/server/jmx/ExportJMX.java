package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.zip.GZIPInputStream;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.exception.FileNotFoundException;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;

/**
 * used to configure export settings
 * 
 * @author wohlgemuth *
 * @jmx.mbean description = "configuration of the export"
 *            name="binbase:service=Export" extends =
 *            "javax.management.MBeanRegistration"
 */
public class ExportJMX implements ExportJMXMBean {
	Logger logger = Logger.getLogger(getClass());

	boolean enableCache;

	boolean enableClusteredPostmatching;

	boolean enableClusteredStatistics;

	/**
	 * place where we can find netcdf's
	 */
	private Vector netCDFDirs = new Vector();

	/**
	 * place where we can find sops
	 */
	private Vector sopDirs = new Vector();

	/**
	 * place where we can store our results
	 */
	private String resultDirectory = "result";

	private String defaultSop = "";

	/**
	 * @jmx.managed-operation description="cache"
	 */
	public boolean isEnableCache() {
		return enableCache;
	}

	/**
	 * @jmx.managed-operation description="cache"
	 */
	public void setEnableCache(boolean enableCache) {
		this.enableCache = enableCache;
		this.store();
	}

	private void store() {
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			out.writeBoolean(this.isEnableCache());
			out.writeObject(this.getNetCDFDirectories());
			out.writeBoolean(this.enableClusteredPostmatching);
			out.writeBoolean(this.isEnableClusteredStatistics());
			out.writeObject(this.getSopDirs());
			out.writeObject(this.getResultDirectory());
			out.writeObject(this.getDefaultSop());
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}

	/**
	 * @jmx.managed-operation description = "adds a net cdf directory"
	 * @author wohlgemuth
	 * @version Jul 16, 2006
	 * @param string
	 */
	public void addNetCDFDirectory(String string) {
		if (string.endsWith(File.separator) == false) {
			string = string + File.separator;
		}

		if (netCDFDirs.contains(string) == false) {
			netCDFDirs.add(string);
			this.store();
		}
	}

	/**
	 * @jmx.managed-operation description = "removes a net cdf directoy"
	 * @author wohlgemuth
	 * @version Jul 16, 2006
	 * @param string
	 */
	public void removeNetCDFDirectory(String string) {
		netCDFDirs.remove(string);
		this.store();
	}

	/**
	 * @jmx.managed-operation description = "delete all netcdf directories"
	 * @author wohlgemuth
	 * @version Jul 16, 2006
	 */
	public void clearNetCDFDirectorys() {
		netCDFDirs.clear();
		this.store();
	}

	/**
	 * @jmx.managed-operation description = "return all registered net cdf
	 *                        directories where sample files can be"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Collection getNetCDFDirectories() throws BinBaseException {
		return netCDFDirs;
	}

	public void postDeregister() {
		// TODO Auto-generated method stub

	}

	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					file));

			this.enableCache = in.readBoolean();
			this.netCDFDirs = (Vector) in.readObject();

			this.enableClusteredPostmatching = in.readBoolean();
			this.enableClusteredStatistics = in.readBoolean();
			this.sopDirs = (Vector) in.readObject();
			this.resultDirectory = (String) in.readObject();
			this.defaultSop = (String) in.readObject();

			this.setResultDirectory(resultDirectory);
		} catch (Exception e) {
			logger.debug("postRegister(Boolean)", e); //$NON-NLS-1$

		}
	}

	public void preDeregister() throws Exception {
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1)
			throws Exception {
		return null;
	}

	/**
	 * @jmx.managed-operation description="clustered postmatching"
	 */
	public boolean isEnableClusteredPostmatching() {
		return enableClusteredPostmatching;
	}

	/**
	 * @jmx.managed-operation description="clustered postmatching"
	 */
	public void setEnableClusteredPostmatching(
			boolean enableClusteredPostmatching) {
		this.enableClusteredPostmatching = enableClusteredPostmatching;
		store();
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public boolean isEnableClusteredStatistics() {
		return enableClusteredStatistics;
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public void setEnableClusteredStatistics(boolean enableClusteredStatistics) {
		this.enableClusteredStatistics = enableClusteredStatistics;
		store();
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public Collection getSopDirs() {
		return sopDirs;
	}

	/**
	 * @throws FileNotFoundException
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public void addSopDir(String dir) throws FileNotFoundException {
		this.createDir(dir);
		this.sopDirs.add(dir);
		this.store();
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public void clearSopDirs() {
		this.sopDirs.clear();
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public void removeSopDirectory(String dir) {
		this.sopDirs.remove(dir);
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public String getResultDirectory() {
		return resultDirectory;
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public void setResultDirectory(String resultDirectory)
			throws FileNotFoundException {
		this.resultDirectory = resultDirectory;
		createDir(resultDirectory);
		this.store();
	}

	private void createDir(String resultDirectory) throws FileNotFoundException {
		File file = new File(resultDirectory);
		if (file.exists() == false) {
			logger.info("creating new directory");
			file.mkdirs();
			logger.info("created at: " + file.getAbsolutePath());
		}
		if (file.isDirectory() == false) {
			throw new FileNotFoundException("sorry needs to be a directory!");
		}
	}

	/**
	 * @throws IOException
	 * @throws java.io.FileNotFoundException
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public void uploadResult(String sopName, byte[] content)
			throws java.io.FileNotFoundException, IOException {
		Copy.copy(new ByteArrayInputStream(content),
				new FileOutputStream(this.getResultDirectory() + "/" + sopName));
	}

	/**
	 * @throws IOException
	 * @throws java.io.FileNotFoundException
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public void uploadSopToDir(String sopName, String dir, byte[] content)
			throws java.io.FileNotFoundException, IOException {
		Copy.copy(new ByteArrayInputStream(content), new FileOutputStream(dir
				+ "/" + sopName));

	}

	/**
	 * @throws IOException
	 * @throws java.io.FileNotFoundException
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public void uploadNetCdfToDir(String fileName, String dir, byte[] content)
			throws java.io.FileNotFoundException, IOException {
		if (fileName.toLowerCase().endsWith(".cdf.gz")) {

		} else if (fileName.toLowerCase().endsWith(".cdf") == false) {
			fileName = fileName + ".cdf";
		}
		Copy.copy(new ByteArrayInputStream(content), new FileOutputStream(dir
				+ "/" + fileName));
	}

	/**
	 * @throws IOException
	 * @throws java.io.FileNotFoundException
	 * @throws FileNotFoundException
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public byte[] getSop(String sopName) throws java.io.FileNotFoundException,
			IOException, FileNotFoundException {
		Iterator it = this.sopDirs.iterator();
		while (it.hasNext()) {
			String dir = (String) it.next();
			File file = new File(dir + "/" + sopName);
			if (file.exists()) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				Copy.copy(new FileInputStream(file), out);
				return out.toByteArray();
			}
		}
		throw new FileNotFoundException(sopName
				+ " was not found in any specified directory!");
	}

	/**
	 * @throws IOException
	 * @throws java.io.FileNotFoundException
	 * @throws FileNotFoundException
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public String printSoap(String sopName)
			throws java.io.FileNotFoundException, FileNotFoundException,
			IOException {
		return new String(getSop(sopName));
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public List listSops() {
		List result = new Vector();
		Iterator it = this.sopDirs.iterator();
		while (it.hasNext()) {
			String dir = (String) it.next();
			File file = new File(dir);
			File files[] = file.listFiles();

			for (int i = 0; i < files.length; i++) {
				if (files[i].isFile()) {
					result.add(files[i].getName());
				}
			}
		}

		return result;
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public List listResults() {
		List result = new Vector();
		File file = new File(this.getResultDirectory());
		File files[] = file.listFiles();

		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile()) {
				result.add(files[i].getName());
			}
		}

		return result;
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public byte[] getResult(String resultName)
			throws java.io.FileNotFoundException, IOException,
			FileNotFoundException {
		File file = new File(this.getResultDirectory() + "/" + resultName);
		if (file.exists()) {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Copy.copy(new FileInputStream(file), out);
			return out.toByteArray();
		}
		throw new FileNotFoundException("sorry result file was not found! "
				+ resultName);
	}

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public boolean hasResult(String resultName) {
		File file = new File(this.getResultDirectory() + "/" + resultName);
		return file.exists();
	}

	/**
	 * @throws BinBaseException 
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public boolean hasCDF(String sampleName) throws BinBaseException{
		Iterator it = this.getNetCDFDirectories().iterator();

		while (it.hasNext()) {
			String dir = it.next().toString();

			File file = new File(generateFileName(dir, sampleName));

			if (file.exists()) {
				return true;
			}
			
			file = new File(generateFileName(dir, sampleName)+".gz");
			if (file.exists()) {
				return true;
			}
			
		}
		return false;
	}
	

	/**
	 * @jmx.managed-operation description="clustered statistics"
	 * @param sampleName
	 * @return
	 * @throws BinBaseException
	 * @throws IOException
	 */
	public byte[] downloadFile(String sampleName) throws BinBaseException,
			IOException {
		Iterator it = this.getNetCDFDirectories().iterator();

		while (it.hasNext()) {
			String dir = it.next().toString();

			File file = new File(generateFileName(dir, sampleName));

			if (file.exists()) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();

				FileInputStream in = new FileInputStream(file);
				Copy.copy(in, out);

				byte[] result = out.toByteArray();
				return result;
			}
			
			file = new File(generateFileName(dir, sampleName)+".gz");
			if (file.exists()) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();

				FileInputStream in = new FileInputStream(file);
				Copy.copy(new GZIPInputStream(in), out);

				byte[] result = out.toByteArray();
				return result;
			}			
		}
		return null;
	}

	/**
	 * @throws java.io.FileNotFoundException
	 * @throws IOException
	 * @jmx.managed-operation description="clustered statistics"
	 */

	public void uploadSop(String sopName, byte[] content)
			throws FileNotFoundException, java.io.FileNotFoundException,
			IOException {
		if (sopDirs.isEmpty()) {
			logger.info("creating directory for sops...");
			addSopDir("sop");
		}
		this.uploadSopToDir(sopName, (String) sopDirs.get(0), content);
	}

	/**
	 * @throws IOException
	 * @throws BinBaseException
	 * @jmx.managed-operation description="clustered statistics"
	 */
	public void uploadNetCdf(String fileName, byte[] content)
			throws java.io.FileNotFoundException, IOException, BinBaseException {
		if (this.getNetCDFDirectories().isEmpty()) {
			logger.info("creating directory for netcdf...");
			addNetCDFDirectory("netcdf");
		}
		this.uploadNetCdfToDir(fileName, (String) getNetCDFDirectories()
				.iterator().next(), content);
	}

	/**
	 * @jmx.managed-operation description = "return the default sop"
	 * @return
	 */
	public String getDefaultSop() {
		return defaultSop;
	}

	/**
	 * @jmx.managed-operation description = "return the default sop"
	 * @return
	 */
	public void setDefaultSop(String defaultSop) {
		this.defaultSop = defaultSop;
		this.store();
	}
	

	/**
	 * @jmx.managed-operation description = "generates the sample name"
	 * @param validateSources
	 * @throws BinBaseException
	 */
	public String generateFileName(String dir, String sampleName) {
		if (dir.endsWith("/") == false) {
			dir = dir + "/";
		}

		return dir + sampleName.replace(':', '_') + ".cdf";
	}
}
