/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.server.types;

import java.io.Serializable;

/**
 * @swt
 * @author wohlgemuth
 * 
 */
public class ExperimentSample implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * the setupX id
	 */
	private String id;

	/**
	 * the filename
	 */
	private String name;

	public ExperimentSample(){
		
	}
	/**
	 * @param id
	 * @param name
	 */
	public ExperimentSample(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * *
	 * 
	 * @swt.variable visible="true" name="Id" searchable="true"
	 * @swt.modify canModify="true"
	 * @author wohlgemuth
	 * @version Aug 16, 2006
	 * @return
	 */
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * *
	 * 
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="true"
	 * @author wohlgemuth
	 * @version Aug 16, 2006
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return getName() + "/" + getId();
	}

}
