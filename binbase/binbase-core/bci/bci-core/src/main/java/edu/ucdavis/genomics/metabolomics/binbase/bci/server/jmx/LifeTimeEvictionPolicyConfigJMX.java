package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.CacheFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.policies.LifeTimeEvictionPolicy;
import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException;

/**
 * simple wrapper to access the cache
 * 
 * @author Administrator
 * @jmx.mbean description = "simple cache management" extends =
 *            "javax.management.MBeanRegistration"
 *            name="binbase.cache:service=LifeTimeEvictionPolicy"
 */
public class LifeTimeEvictionPolicyConfigJMX implements
		LifeTimeEvictionPolicyConfigJMXMBean {

	private Logger logger = Logger.getLogger(getClass());

	private boolean enable;

	private int lifeInSeconds;

	private LifeTimeEvictionPolicy policy = new LifeTimeEvictionPolicy(100);

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public void setEnable(boolean enable) {
		this.enable = enable;

		try {
			if (enable) {
				if (CacheFactory.getInstance().createCache() instanceof EvictableCache) {
					((EvictableCache) CacheFactory.getInstance().createCache())
							.addEvictionPolicy(policy);
				} else {
					throw new NotSupportedException(
							"sorry evictions are not supported!");
				}
			} else {
				if (CacheFactory.getInstance().createCache() instanceof EvictableCache) {
					((EvictableCache) CacheFactory.getInstance().createCache())
							.removeEvictionPolicy(policy);
				} else {
					throw new NotSupportedException(
							"sorry evictions are not supported!");
				}
			}

			this.store();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void postDeregister() {
	}

	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					file));

			this.setEnable(in.readBoolean());
			this.setLifeInSeconds(in.readInt());

		} catch (Exception e) {
			logger.info(e.getMessage());
		}
	}

	public void preDeregister() throws Exception {
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1)
			throws Exception {
		return null;
	}

	private void store() {
		logger.info("store properties");
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			out.writeBoolean(this.enable);
			out.writeInt(this.lifeInSeconds);
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public int getLifeInSeconds() {
		return lifeInSeconds;
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public void setLifeInSeconds(int lifeInSeconds) {
		this.lifeInSeconds = lifeInSeconds;
		this.store();
		this.policy.setLifeTimeInSeconds(lifeInSeconds);
	}

}
