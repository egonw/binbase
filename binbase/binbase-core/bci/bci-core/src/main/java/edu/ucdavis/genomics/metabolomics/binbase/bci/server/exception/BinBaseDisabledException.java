package edu.ucdavis.genomics.metabolomics.binbase.bci.server.exception;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class BinBaseDisabledException extends BinBaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public BinBaseDisabledException() {
		super();
		
	}

	public BinBaseDisabledException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public BinBaseDisabledException(String arg0) {
		super(arg0);
		
	}

	public BinBaseDisabledException(Throwable arg0) {
		super(arg0);
		
	}

}
