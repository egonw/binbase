/*
 * Created on Apr 21, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;

import org.apache.log4j.Logger;
import org.jboss.mq.SpyObjectMessage;
import org.jboss.mq.server.jmx.QueueMBean;
import org.jboss.mx.util.MBeanServerLocator;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.ejb.BinBaseQueueUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.backup.BackupObject;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * jmx to display the status of the import/export
 * 
 * @author wohlgemuth
 * @version Apr 21, 2006
 * 
 * @jmx.mbean description = "Displays the status of the BinBase and can delete
 *            Queues" extends = "javax.management.MBeanRegistration"
 *            name="binbase:service=Status"
 */
public class StatusJMX implements StatusJMXMBean {

	/**
	 * to notify users over events
	 */
	private Collection notificationsAdresses = new Vector();

	private boolean automaticMetaDataUpdate;
	private boolean automaticBinBaseUpdate;
	private boolean automaticCacheUpdate;

	private Logger logger = Logger.getLogger(getClass());

	public StatusJMX() {
		super();
	}

	/**
	 * @jmx.managed-operation description = "a list of email addresses to notify
	 *                        users"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Collection getNotificationsAdresses() throws BinBaseException {
		return notificationsAdresses;
	}

	/**
	 * @jmx.managed-operation description = "adds an email for notifications"
	 * @author wohlgemuth
	 * @version Jul 16, 2006
	 * @param string
	 */
	public void addNotificationAdress(String emailAdress) {

		if (notificationsAdresses.contains(emailAdress) == false) {
			notificationsAdresses.add(emailAdress);
			this.store();
		}
	}

	/**
	 * @jmx.managed-operation description = "removes an email address for
	 *                        notifications"
	 * @author wohlgemuth
	 * @version Jul 16, 2006
	 * @param string
	 */
	public void removeNotificationAdress(String emailAdresses) {
		notificationsAdresses.remove(emailAdresses);
		this.store();
	}

	/**
	 * @jmx.managed-operation description = "returns the import queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Collection listImportJobs() throws BinBaseException {
		try {
			return Arrays.asList(BinBaseQueueUtil.getLocalHome().create()
					.getImport());
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}
	
	/**
	 * @jmx.managed-operation description = "returns the dsl queue"
	 * @author wohlgemuth
	 * @return
	 * @throws BinBaseException
	 */
	public Collection listDSLJobs() throws BinBaseException{
		try {
			return Arrays.asList(BinBaseQueueUtil.getLocalHome().create()
					.getDSLJobs());
		} catch (Exception e) {
			throw new BinBaseException(e);
		}		
	}

	/**
	 * @jmx.managed-operation description = "returns the import queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Collection listPostmatchingJobs() throws BinBaseException {
		try {
			return Arrays.asList(BinBaseQueueUtil.getLocalHome().create()
					.getPostmatchingJobs());
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the import queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public int getCountOfImportJobs() throws BinBaseException {
		try {
			return BinBaseQueueUtil.getLocalHome().create().getImport().length;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the import queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public int getCountOfPostmatchingJobs() throws BinBaseException {
		try {
			return BinBaseQueueUtil.getLocalHome().create()
					.getPostmatchingJobs().length;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the export queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Collection listExportJobs() throws BinBaseException {
		try {
			return Arrays.asList(BinBaseQueueUtil.getLocalHome().create()
					.getExports());
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the export queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public int getCountOfExportJobs() throws BinBaseException {
		try {
			return BinBaseQueueUtil.getLocalHome().create().getExports().length;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the export queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Collection listAllJobs() throws BinBaseException {
		try {
			return Arrays.asList(BinBaseQueueUtil.getLocalHome().create()
					.getQueueContent());
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * generates a map containing the count of different jobs in the system
	 * 
	 * @jmx.managed-operation description = "returns the scheduling jobs"
	 * @author wohlgemuth
	 * @return
	 * @throws BinBaseException
	 */
	public Map listJobCounts() throws BinBaseException {
		try {
			Collection jobs = listAllJobs();
			Map result = new HashMap();
			Iterator it = jobs.iterator();

			while (it.hasNext()) {
				Object o = it.next();
				if (o != null) {
					String className = o.getClass().getName();

					if (result.containsKey(className) == false) {
						result.put(className, new Integer(0));
					}
					result.put(className, (Integer) result.get(className) + 1);
				}
			}

			return result;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the export queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public int getTotalJobCount() throws BinBaseException {
		try {
			return BinBaseQueueUtil.getLocalHome().create().getQueueContent().length;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the export queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Collection listDatabaseUpdateJobs() throws BinBaseException {
		try {
			return Arrays.asList(BinBaseQueueUtil.getLocalHome().create()
					.getUpdateBinBaseJobs());
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the export queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public int getUpdateDatabaseJobCount() throws BinBaseException {
		try {
			return BinBaseQueueUtil.getLocalHome().create()
					.getUpdateBinBaseJobs().length;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the export queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Collection listCachingMetaDataJobs() throws BinBaseException {
		try {
			return Arrays.asList(BinBaseQueueUtil.getLocalHome().create()
					.getCacheBinMetainformationsJob());
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returns the export queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public int getCachingMetaDataJobCount() throws BinBaseException {
		try {
			return BinBaseQueueUtil.getLocalHome().create()
					.getCacheBinMetainformationsJob().length;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "clears the queue"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public void clearQueue() throws BinBaseException {
		try {
			BinBaseQueueUtil.getLocalHome().create().clearQueue();
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	public ObjectName preRegister(MBeanServer server, ObjectName name)
			throws Exception {
		return null;
	}

	public void postRegister(Boolean registrationDone) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					file));
			this.notificationsAdresses = (Collection) in.readObject();
			this.automaticBinBaseUpdate = in.readBoolean();
			this.automaticMetaDataUpdate = in.readBoolean();
			this.automaticCacheUpdate = in.readBoolean();
		} catch (Exception e) {
			logger.error("postRegister(Boolean)", e); //$NON-NLS-1$

		}
	}

	public void preDeregister() throws Exception {
		store();
	}

	public void postDeregister() {
	}

	private void store() {
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			out.writeObject(this.getNotificationsAdresses());
			out.writeBoolean(isAutomaticBinBaseUpdate());
			out.writeBoolean(isAutomaticMetaDataUpdate());
			out.writeBoolean(isAutomaticCacheUpdate());
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}

	/**
	 * @jmx.managed-operation description = "a list of email addresses to notify
	 *                        users"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public boolean isAutomaticMetaDataUpdate() {
		return automaticMetaDataUpdate;
	}

	/**
	 * @jmx.managed-operation description = "a list of email addresses to notify
	 *                        users"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public void setAutomaticMetaDataUpdate(boolean automaticMetaDataUpdate) {
		this.automaticMetaDataUpdate = automaticMetaDataUpdate;
		store();

	}

	/**
	 * @jmx.managed-operation description = "a list of email addresses to notify
	 *                        users"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public boolean isAutomaticBinBaseUpdate() {
		return automaticBinBaseUpdate;
	}

	/**
	 * @jmx.managed-operation description = "a list of email addresses to notify
	 *                        users"
	 * @author wohlgemuth
	 * @version Apr 21, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public void setAutomaticBinBaseUpdate(boolean automaticBinBaseUpdate) {
		this.automaticBinBaseUpdate = automaticBinBaseUpdate;
		store();

	}

	/**
	 * @jmx.managed-operation description = "a list of email addresses to notify
	 *                        users"
	 * @return
	 */
	public boolean isAutomaticCacheUpdate() {
		return automaticCacheUpdate;
	}

	/**
	 * @jmx.managed-operation description = "a list of email addresses to notify
	 *                        users"
	 * @param automaticCacheUpdate
	 */
	public void setAutomaticCacheUpdate(boolean automaticCacheUpdate) {
		this.automaticCacheUpdate = automaticCacheUpdate;
		store();
	}

	/**
	 * @jmx.managed-operation description = "clears the queue"
	 */
	public File backupQueue() throws Exception {

		ObjectName name = new ObjectName(
				"jboss.mq.destination:service=Queue,name=queue");
		MBeanServer server = MBeanServerLocator.locate();
		QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler
				.newProxyInstance(server, name, QueueMBean.class, false);
		List list = queue.listMessages();

		Iterator it = list.iterator();
		File file = new File("queue");
		file.mkdirs();
		file = new File("queue/backup.queue");

		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(
				file));

		while (it.hasNext()) {
			SpyObjectMessage m = (SpyObjectMessage) it.next();
			Serializable serial = m.getObject();
			logger.info("backing up: " + serial + " with priority "
					+ m.getJMSPriority());
			BackupObject b = new BackupObject();
			b.setObject(serial);
			b.setPriority(m.getJMSPriority());

			out.writeObject(b);

		}

		out.flush();
		out.close();

		return file;
	}

	/**
	 * @jmx.managed-operation description = "clears the queue"
	 */
	public File restoreQueue() throws Exception {
		File file = new File("queue");
		file.mkdirs();
		file = new File("queue/backup.queue");

		if (file.exists() == false) {
			throw new FileNotFoundException("sorry no backup found!");
		}

		ObjectInputStream input = new ObjectInputStream(new FileInputStream(
				file));

		BackupObject serial = null;

		try {
			while ((serial = (BackupObject) input.readObject()) != null) {
				Serializable s = serial.getObject();
				int pri = serial.getPriority();
				logger.info("Reading: " + s + " with priority " + pri);

				Scheduler.schedule(s, pri);

			}

		} catch (EOFException ex) {
			logger.info("restore finished...");
		}

		input.close();

		return file;
	}
}
