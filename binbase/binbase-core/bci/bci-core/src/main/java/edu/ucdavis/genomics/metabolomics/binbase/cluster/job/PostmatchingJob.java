package edu.ucdavis.genomics.metabolomics.binbase.cluster.job;

import java.io.Serializable;
import java.util.Vector;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;

/**
 * simple job to postmatch samples
 * 
 * @author wohlgemuth
 */
public class PostmatchingJob implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private Vector<ExperimentSample> sampleIds = new Vector<ExperimentSample>();
	String database;

	public Vector<ExperimentSample> getSampleIds() {
		return sampleIds;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " - " + sampleIds + " - " + database;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PostmatchingJob) {
			PostmatchingJob job = (PostmatchingJob) obj;
			if (this.getSampleIds().equals(job.getSampleIds())) {
				if (this.getDatabase().equals(job.getDatabase())) {
					return true;
				}
			}
		}
		return false;
	}

	public void addSampleID(ExperimentSample sampleID) {
		this.sampleIds.add(sampleID);
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	private boolean postMatched;

	private boolean xmlGenerated;

	private double current = 0;

	private double max = 2;

	public boolean isPostMatched() {
		return postMatched;
	}

	public void setPostMatched(boolean postMatched) {
		progress(postMatched);
		this.postMatched = postMatched;
	}

	public boolean isXmlGenerated() {
		return xmlGenerated;
	}

	public void setXmlGenerated(boolean xmlGenerated) {
		progress(xmlGenerated);
		this.xmlGenerated = xmlGenerated;
	}

	public double calculateProgress() {
		return (double) current / (double) max * 100;
	}

	private void progress(boolean matched) {
		if (matched) {
			current++;
		}
		else {
			current--;
		}
	}
}
