package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.policies;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictionPolicy;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache.Entry;

/**
 * takes care that the cache gets partly cleared if we are running out of memory
 *
 */
public class MemoryEvictionPolicy implements EvictionPolicy{

	/**
	 * the maximum border of memory usage we accept
	 */
	double startMemoryEviction = 0.8;
	
	/**
	 * the minimum border of memory usage where we stop eviting objects
	 */
	double stopMemoryEviction = 0.5;
	
	/**
	 * evicts objects if we start to run out of memory
	 */
	public boolean evict(Entry entry, EvictableCache cache) {
		
		return false;
	}

}
