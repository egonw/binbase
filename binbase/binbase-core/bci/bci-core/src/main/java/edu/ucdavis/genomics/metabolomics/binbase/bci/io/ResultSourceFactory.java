package edu.ucdavis.genomics.metabolomics.binbase.bci.io;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.io.source.SourceFactory;

public class ResultSourceFactory extends SourceFactory{

	@Override
	public Source createSource(Object identifier, Map<?, ?> propertys) throws ConfigurationException {
		return new ResultSource(identifier.toString());
	}

}
