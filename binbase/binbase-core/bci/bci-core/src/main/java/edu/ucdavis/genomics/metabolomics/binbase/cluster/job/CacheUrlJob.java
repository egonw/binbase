package edu.ucdavis.genomics.metabolomics.binbase.cluster.job;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

/**
 * tells the cluster we want to cache some bin meta informations
 * 
 * @author wohlgemuth
 */
public class CacheUrlJob implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String database;

	Vector<String> urls = new Vector<String>();

	public CacheUrlJob(String database, Vector<String> urls) {
		super();
		this.database = database;
		this.urls = urls;
	}

	public CacheUrlJob(String database, String url) {
		super();
		this.database = database;
		this.urls.add(url);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CacheUrlJob) {
			CacheUrlJob job = (CacheUrlJob) obj;
			if (this.getUrls().equals(job.getUrls())) {
				if (this.getDatabase().equals(job.getDatabase())) {
					return true;
				}
			}
		}
		return false;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public Vector<String> getUrls() {
		return urls;
	}

	public void setUrls(Vector<String> urls) {
		this.urls = urls;
	}
	

	@Override
	public int hashCode() {
		return this.getDatabase().hashCode() + this.getUrls().hashCode();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " - " + this.getDatabase() + " - " + this.getUrls();
	}


	public static String generateBinUrl(int binId, String database) throws MalformedURLException{
		return generateBinUrl(binId, database, new URL(System.getProperty("java.naming.provider.url")).getHost(),8080);
	}

	public static String generateBinUrl(int binId, String database,String server){
		return generateBinUrl(binId, database, server,8080);
	}
	
	public static String generateBinUrl(int binId, String database,String server,int port){
		return "http://"+server+":"+port+"/binbase-compound/bin/show/"+binId+"?db="+database;
	}
}
