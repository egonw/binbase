package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.policies;

import java.util.List;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.Cache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictionPolicy;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.PersistableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache.Entry;

/**
 * evicts objects if the cache hits a specific size
 * 
 */
public class SizeEvitionPolicy implements EvictionPolicy {

	private Logger logger = Logger.getLogger(getClass());
	
	private int sizeLimit = 50;

	public SizeEvitionPolicy() {

	}

	public SizeEvitionPolicy(int max) {
		this.sizeLimit = max;
	}

	/**
	 * makes sure that the entry gets evicted. We are ignoring persistant entries
	 */
	public boolean evict(Entry entry, EvictableCache cache) {
		if(cache instanceof PersistableCache){
			PersistableCache pers = (PersistableCache) cache;
			if(pers.isPersistant(entry) == true){
				List<Entry> notPersistant = pers.getNotPersistantEntries();

				for(Entry e : notPersistant){
					if(evictionNeeded(cache)){
						cache.evict(e);
					}
				}
				
				entry.setNeedsEviction(false);
				return true;
				
			}
		}
		
		if(evictionNeeded(cache)){
			entry.setNeedsEviction(true);
			return true;
		}
		
		return false;
	}

	/**
	 * tells us if we actualy need a conviction
	 * @param cache
	 * @return
	 */
	private boolean evictionNeeded(Cache cache) {
		if (cache.size() >= sizeLimit) {
			logger.debug("cache needs to be evicted, count of objects: " + cache.size());
			return true;
		}
		return false;
	}

	public int getSizeLimit() {
		return sizeLimit;
	}

	public void setSizeLimit(int sizeLimit) {
		this.sizeLimit = sizeLimit;
	}
}
