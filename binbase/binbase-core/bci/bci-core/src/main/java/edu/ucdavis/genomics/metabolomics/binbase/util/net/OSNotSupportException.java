package edu.ucdavis.genomics.metabolomics.binbase.util.net;

import java.io.IOException;

public class OSNotSupportException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OSNotSupportException() {
		super();
		
	}

	public OSNotSupportException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public OSNotSupportException(String message) {
		super(message);
		
	}

	public OSNotSupportException(Throwable cause) {
		super(cause);
		
	}

}
