package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Properties;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.util.net.DetermineIpForInterface;
import edu.ucdavis.genomics.metabolomics.binbase.util.net.InterfaceNotFoundException;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;

/**
 * used to configure export settings
 * 
 * @author wohlgemuth *
 * @jmx.mbean description = "configuration of the export"
 *            name="binbase:service=DatabaseConfiguration" extends =
 *            "javax.management.MBeanRegistration"
 */
public class DatabaseJMX implements DatabaseJMXMBean {
	Logger logger = Logger.getLogger(getClass());

	String databaseServer;

	String databaseServerPassword;

	String databaseServerType;

	String database;

	boolean determineIp;

	/**
	 * @jmx.managed-operation description="server"
	 */
	public boolean isDetermineIp() {
		return determineIp;
	}

	/**
	 * @jmx.managed-operation description="server"
	 */
	public void setDetermineIp(boolean determineIp) {
		this.determineIp = determineIp;

		if (determineIp == false) {
			this.databaseServer = "127.0.0.1";
		} else {
			try {
				determineIp();
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				this.setDatabaseServer("127.0.0.1");

			}
		}
		store();
	}

	/**
	 * determines the ip address, assuming that the application server
	 * runs on the cluster frontend89
	 * 
	 * @throws IOException
	 */
	private void determineIp() throws IOException {
		logger.info("trying to automatically configure the ip for the database server...");

		// check for eth2
		try {
			// public vmware cluster interface
			String ip = DetermineIpForInterface.getIP("eth2");
			this.setDatabaseServer(ip);
			return;
		} catch (InterfaceNotFoundException e) {
			logger.info(e.getMessage());
		}

		// check for eth1
		try {
			// private vmware cluster interface
			String ip = DetermineIpForInterface.getIP("eth1");
			this.setDatabaseServer(ip);
			return;
		} catch (InterfaceNotFoundException e) {
			logger.info(e.getMessage());
		}

		// check for eth0
		try {
			// public cluster interface
			String ip = DetermineIpForInterface.getIP("eth0");
			this.setDatabaseServer(ip);
			return;
		} catch (InterfaceNotFoundException e) {
			logger.info(e.getMessage());
		}

	}

	Properties properties = new Properties();

	private void store() {
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			out.writeObject(this.getDatabase());
			out.writeObject(this.getDatabaseServer());
			out.writeObject(this.getDatabaseServerPassword());
			out.writeObject(this.getDatabaseServerType());
			out.writeObject(this.getProperties());
			out.writeBoolean(this.isDetermineIp());
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}

	public void postDeregister() {
		// TODO Auto-generated method stub

	}

	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					file));
			this.database = (String) in.readObject();
			this.databaseServer = (String) in.readObject();
			this.databaseServerPassword = (String) in.readObject();
			this.databaseServerType = (String) in.readObject();
			this.properties = (Properties) in.readObject();
			this.determineIp = in.readBoolean();
		} catch (Exception e) {
			logger.debug("postRegister(Boolean)", e); //$NON-NLS-1$

		}

		try {
			if (isDetermineIp()) {
				determineIp();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @jmx.managed-operation description="server"
	 */
	public Properties createProperties() {
		Properties p = new Properties();
		p.setProperty(SimpleConnectionFactory.KEY_DATABASE_PROPERTIE,
				getDatabase());
		p.setProperty(SimpleConnectionFactory.KEY_PASSWORD_PROPERTIE,
				getDatabaseServerPassword());
		p.setProperty(SimpleConnectionFactory.KEY_TYPE_PROPERTIE,
				getDatabaseServerType());
		p.setProperty(SimpleConnectionFactory.KEY_HOST_PROPERTIE,
				getDatabaseServer());

		p.putAll(this.getProperties());

		return p;
	}

	public void preDeregister() throws Exception {
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1)
			throws Exception {
		return null;
	}

	/**
	 * @jmx.managed-operation description="database"
	 * @return
	 */
	public String getDatabase() {
		return database;
	}

	/**
	 * @jmx.managed-operation description="database"
	 * @param database
	 */
	public void setDatabase(String database) {
		this.database = database;
		store();
	}

	/**
	 * @jmx.managed-operation description="server"
	 */
	public String getDatabaseServer() {
		return databaseServer;
	}

	/**
	 * @jmx.managed-operation description="server"
	 * @param databaseServer
	 */
	public void setDatabaseServer(String databaseServer) {
		this.databaseServer = databaseServer;
		store();
	}

	/**
	 * @jmx.managed-operation description="password"
	 * @return
	 */
	public String getDatabaseServerPassword() {
		return databaseServerPassword;
	}

	/**
	 * @jmx.managed-operation description="password"
	 * @param databaseServerPassword
	 */
	public void setDatabaseServerPassword(String databaseServerPassword) {
		this.databaseServerPassword = databaseServerPassword;
		store();
	}

	/**
	 * @jmx.managed-operation description="type"
	 * @return
	 */
	public String getDatabaseServerType() {
		return databaseServerType;
	}

	/**
	 * @jmx.managed-operation description="type"
	 * @param databaseServerType
	 */
	public void setDatabaseServerType(String databaseServerType) {
		this.databaseServerType = databaseServerType;
		store();
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	/**
	 * @jmx.managed-operation description="add properties"
	 */
	public void addPropertie(String key, String value) {
		this.getProperties().put(key, value);
		store();
	}

	/**
	 * @jmx.managed-operation description="remove properties"
	 * @param key
	 */
	public void removePropertie(String key) {
		this.getProperties().remove(key);
		store();
	}

}
