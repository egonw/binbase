package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXProvider;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * jmx used for setting the setupX properties for
 * 
 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.server.ejb.SetupXCommunicationBean
 * 
 * @author wohlgemuth
 * @jmx.mbean description = "defiens the class which should used as
 *            implementation for setupX communication, the class must be in the
 *            classpath. so deploy the jar before, the jar also have to register
 *            the service, or you have to do it ny your own" extends =
 *            "javax.management.MBeanRegistration"
 *            name=""binbase:service=MetaProvider""
 * 
 */
public class MetaProviderJMX implements MetaProviderJMXMBean, SetupXProvider {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(MetaProviderJMX.class);

	/**
	 * our collection of providers
	 */
	private Collection provider = new Vector();

	/**
	 * @jmx.managed-operation description = "adds a new setup x provider" adds a
	 *                        new setup x provider
	 * @param providerClass
	 */
	public void addProvider(String providerClass) {
		if (isProviderRegistered(providerClass) == false) {
			this.provider.add(providerClass);
		}
	}

	/**
	 * @jmx.managed-operation description = "removes a setupX provider" removes
	 *                        a setupX provider
	 * @param providerClass
	 */
	public void removeProvider(String providerClass) {
		if (isProviderRegistered(providerClass)) {
			this.provider.remove(providerClass);
		}
	}

	/**
	 * @jmx.managed-attribute description = "returns allProvider which are
	 *                        possible to use" returns allProvider which are
	 *                        possible to use
	 * @return
	 */
	public Collection getProvider() {
		return provider;
	}

	/**
	 * @jmx.managed-operation description = "true if the providerClass already
	 *                        added, false if the provider class is not added"
	 * @param providerClass
	 * @return true if the providerClass already added, false if the provider
	 *         class is not added
	 */
	public boolean isProviderRegistered(String providerClass) {
		boolean returnboolean = this.provider.contains(providerClass);
		return returnboolean;
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1)
			throws Exception {
		return arg1;
	}

	/**
	 * time to load the last configuration
	 */
	public void postRegister(Boolean arg0) {
	}

	/**
	 * time to save the current configuration
	 */
	public void preDeregister() throws Exception {
	}

	public void postDeregister() {
	}

	/**
	 * @jmx.managed-operation description = "setupx"
	 */
	public String getMetaInformationByClass(String setupXId)
			throws BinBaseException {
		try {
			Iterator it = this.getProvider().iterator();

			while (it.hasNext()) {
				SetupXProvider pro = (SetupXProvider) Class.forName(
						it.next().toString()).newInstance();
				String result = pro.getMetaInformationByClass(setupXId);

				if (result != null) {
					return result;
				}
			}
			return null;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "setupx"
	 */
	public String getMetaInformationByExperiment(String setupXId)
			throws BinBaseException {
		try {
			Iterator it = this.getProvider().iterator();

			while (it.hasNext()) {
				SetupXProvider pro = (SetupXProvider) Class.forName(
						it.next().toString()).newInstance();
				String result = pro.getMetaInformationByExperiment(setupXId);

				if (result != null) {
					return result;
				}
			}
			return null;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "setupx"
	 */
	public String getMetaInformationBySample(String setupXId)
			throws BinBaseException {
		Iterator it = this.getProvider().iterator();
		try {
			while (it.hasNext()) {
				SetupXProvider pro = (SetupXProvider) Class.forName(
						it.next().toString()).newInstance();
				String result = pro.getMetaInformationBySample(setupXId);

				if (result != null) {
					return result;
				}
			}

			return null;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "setupx"
	 */
	public String getSetupXId(String sampleName) throws BinBaseException {
		try {
			Iterator it = this.getProvider().iterator();

			while (it.hasNext()) {
				SetupXProvider pro = (SetupXProvider) Class.forName(
						it.next().toString()).newInstance();
				String result = pro.getSetupXId(sampleName);

				if (result != null) {
					return result;
				}
			}

			return null;
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "setupx"
	 */
	public void upload(String experimentId, String content)
			throws BinBaseException {
		try {
			Iterator it = this.getProvider().iterator();

			while (it.hasNext()) {
				SetupXProvider pro = (SetupXProvider) Class.forName(
						it.next().toString()).newInstance();
				pro.upload(experimentId, content);
			}
		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}


	/**
	 * by default it should always return true, only if one implementation says no, we do not generate new bins
	 * @jmx.managed-operation description = "setupx"
	 */
	public boolean canCreateBins(String setupxId) throws BinBaseException {
		try {
			Iterator it = this.getProvider().iterator();

			while (it.hasNext()) {
				SetupXProvider pro = (SetupXProvider) Class.forName(
						it.next().toString()).newInstance();
				boolean result = pro.canCreateBins(setupxId);

				if(!result){
					return false;
				}
			}

			return true;

		} catch (Exception e) {
			throw new BinBaseException(e);
		}
	}
}