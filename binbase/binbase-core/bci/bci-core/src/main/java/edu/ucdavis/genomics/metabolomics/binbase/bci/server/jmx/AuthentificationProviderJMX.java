package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Vector;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

/**
 * jmx used for setting the setupX properties for
 * 
 * @see edu.ucdavis.genomics.metabolomics.binbase.bci.server.ejb.SetupXCommunicationBean
 * @author wohlgemuth
 * @jmx.mbean name="binbase:service=AuthentificationProvider" description =
 *            "defiens the class which should used as implementation for setupX
 *            communication, the class must be in the classpath. so deploy the
 *            jar before, the jar also have to register the service, or you have
 *            to do it ny your own" extends =
 *            "javax.management.MBeanRegistration"
 */
public class AuthentificationProviderJMX implements AuthentificationProviderJMXMBean {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(AuthentificationProviderJMX.class);

	private Collection providers = new Vector();

	/**
	 * @jmx.managed-operation description = "adds a new setup x provider" adds a
	 *                        new setup x provider
	 * @param providerClass
	 */
	public void addProviderFactory(String providerClass) {
		if (this.providers.contains(providerClass) == false) {
			this.providers.add(providerClass);
		}
		this.store();
	}

	/**
	 * @jmx.managed-operation description = "returns allProvider which are
	 *                        possible to use" returns allProvider which are
	 *                        possible to use
	 * @return
	 */
	public Collection getProviders() {
		return providers;
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1) throws Exception {
		return arg1;
	}

	/**
	 * time to load the last configuration
	 */
	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
			this.providers = (Collection) in.readObject();

			this.addProviderFactory("edu.ucdavis.genomics.metabolomics.binbase.bci.SimpleAuthentificatorFactory");

		}
		catch (Exception e) {
			logger.debug("postRegister(Boolean)", e); //$NON-NLS-1$

		}
	}

	/**
	 * time to save the current configuration
	 */
	public void preDeregister() throws Exception {
		store();
	}

	public void postDeregister() {
	}

	protected void store() {
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
			out.writeObject(this.getProviders());

			out.flush();
			out.close();

		}
		catch (Exception e) {
			logger.debug(e.getMessage(), e); //$NON-NLS-1$
		}
	}
}
