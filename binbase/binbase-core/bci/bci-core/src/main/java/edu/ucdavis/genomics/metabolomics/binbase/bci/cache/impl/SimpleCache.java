package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache;

/**
 * really simple cache based on a has map and is a single ton, it provides basic
 * support for evictions
 * 
 * @author wohlgemuth O
 */
public class SimpleCache extends AbstractEvictableCache {

	/**
	 * should take care about issues with several people trying to modify the data at the same time
	 */
	private Map<String, Entry> map = new ConcurrentHashMap<String, Entry>(50);

	private static SimpleCache instance;

	@Override
	protected Entry findEntryForEviction() {
		logger.debug("searching for optimal entry for eviction");
		int min = Integer.MAX_VALUE;
		String minKey = null;

		Iterator<String> k = map.keySet().iterator();

		// searching for a object with the minum access
		while (k.hasNext()) {
			Entry entry = this.map.get(k.next());
			long time = System.currentTimeMillis() - entry.getCreated().getTime();
			logger.debug("age is: " + (time/1000) + " for " + entry + " and number of entries: " + this.size() + " and access count is: " + entry.getAccessCount());
			
			if (entry.getAccessCount() <= min) {
				min = entry.getAccessCount();
				minKey = entry.getKey();

				// its already 0 and we return this value
				if (min == 0) {
					logger.debug("found one with no calls and use it, " + entry);
					return entry;
				}
			}
		}

		if (minKey != null) {
			Entry entry = map.get(minKey);
			logger.debug("found a min key value, " + entry);
			return entry;
		}

		// in the first case we go back to the highest value
		return super.findEntryForEviction();
	}

	private Logger logger = Logger.getLogger(getClass());

	protected SimpleCache() {
		logger
				.warn("creating simple cache which is not transaction save, please considerer providing an implementation which is persistant and save!");
	}

	/**
	 * returns a cache instance
	 * 
	 * @return
	 */
	public static SimpleCache getCache() {
		if (instance == null) {
			instance = new SimpleCache();
		}
		return instance;
	}

	public boolean contains(String key) {
		return map.get(key) != null;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " - (" + map.toString() + ")";
	}

	public void clear() {
		this.map.clear();
	}

	public int size() {
		return this.map.size();
	}

	@Override
	protected void addEntry(Entry entry) {
		map.put(entry.getKey(), entry);
	}

	@Override
	protected Entry getEntry(String key) {
		return map.get(key);
	}

	@Override
	protected Entry getFirstEntry() {
		Object[] o = this.map.keySet().toArray();
		if (o.length > 0) {
			Entry first = map.get(o[0]);
			logger.debug("returning first object: " + first);
			return first;
		} else {
			logger.info("no object found...");
			return null;
		}
	}

	@Override
	protected void removeEntry(Entry entry) {
		logger.debug("removing object: " + entry);
		map.remove(entry.getKey());
	}

	public void setConfiguration(Map<?, ?> map) {
		// no configuraiton needed
	}
}
