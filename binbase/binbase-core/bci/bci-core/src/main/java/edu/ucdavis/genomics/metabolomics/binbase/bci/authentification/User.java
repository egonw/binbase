/*
 * Created on Nov 16, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.authentification;

import java.io.Serializable;

/**
 * simple user
 * 
 * @author wohlgemuth
 * @version Nov 16, 2006
 * 
 */
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private String password;

	private String username;

	private boolean masterUser;

	private long userId;

	public User() {
	}

	public User(String password, String username, long userId) {
		super();
		this.password = password;
		this.username = username;
		this.userId = userId;
	}

	public User(String password, String username, long userId, boolean masterUser) {
		super();
		this.password = password;
		this.username = username;
		this.userId = userId;
		this.masterUser = masterUser;
	}

	public String getPassword() {
		return password;
	}

	public long getUserId() {
		return userId;
	}

	public String getUsername() {
		return username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isMasterUser() {
		return masterUser;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			User user = (User) obj;
			if(user.getUsername().equals(this.getUsername())){
				if(user.isMasterUser() == this.isMasterUser()){
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getUsername().hashCode();
	}

	@Override
	public String toString() {
		return this.getUsername() + " - " + this.isMasterUser();
	}

}