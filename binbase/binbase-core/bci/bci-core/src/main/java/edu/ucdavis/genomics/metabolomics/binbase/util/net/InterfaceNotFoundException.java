package edu.ucdavis.genomics.metabolomics.binbase.util.net;

import java.io.IOException;

public class InterfaceNotFoundException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InterfaceNotFoundException() {
		super();		
	}

	public InterfaceNotFoundException(String arg0, Throwable arg1) {
		super(arg0, arg1);		
	}

	public InterfaceNotFoundException(String arg0) {
		super(arg0);		
	}

	public InterfaceNotFoundException(Throwable arg0) {
		super(arg0);		
	}

}
