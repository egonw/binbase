package edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.action;

import java.io.Serializable;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;

/**
 * job definition to update a sample
 * 
 * @author wohlgemuth
 * 
 */
public class UpdateExperimentSample extends ExperimentSample implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private String database;

	private String xmlContent;

	public UpdateExperimentSample() {

	}

	public UpdateExperimentSample(String name, String setupXId,
			String database, String xmlContent) {
		super(setupXId, name);

		this.database = database;
		this.xmlContent = xmlContent;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getXmlContent() {
		return xmlContent;
	}

	public void setXmlContent(String xmlContent) {
		this.xmlContent = xmlContent;
	}
}
