/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.server.types;

import java.io.Serializable;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;

/**
 * @swt
 * @author wohlgemuth
 * 
 */
public class ExperimentClass implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private ExperimentSample[] samples;

	private String id;

	/**
	 * how often to we tried to import this class
	 */
	private int increase = 0;

	/**
	 * internal database for use
	 */
	private String column;

	
	private int priority = Scheduler.PRIORITY_HIGH;
	/**
	 * *
	 * 
	 * @swt.variable visible="true" name="id" searchable="true"
	 * @swt.modify canModify="true"
	 * @author wohlgemuth
	 * @version Aug 16, 2006
	 * @return
	 */
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * *
	 * 
	 * @author wohlgemuth
	 * @version Aug 16, 2006
	 * @return
	 */
	public ExperimentSample[] getSamples() {
		return this.samples;
	}

	public void setSamples(ExperimentSample[] samples) {
		this.samples = samples;
	}

	@Override
	public String toString() {
		return id + "/" + this.getColumn();
	}

	/**
	 * *
	 * 
	 * @swt.variable visible="true" name="Column" searchable="true"
	 * @swt.modify canModify="true"
	 * @author wohlgemuth
	 * @version Aug 16, 2006
	 * @return
	 */
	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	
	public int getIncrease() {
		return increase;
	}

	public void setIncrease(int increase) {
		this.increase = increase;
	}

	@Override
	public boolean equals(Object arg0) {
		if (arg0 instanceof ExperimentClass) {
			return ((ExperimentClass) arg0).getId().equals(this.getId());
		}
		return false;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
