package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;

import java.io.Serializable;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException;

/**
 * basic
 * 
 * @author wohlgemuth
 * 
 */
public interface Cache {

	/**
	 * does the cache contains this object
	 * 
	 * @param key
	 * @return
	 */
	public boolean contains(String key) throws CacheException;

	/**
	 * returns the object
	 * 
	 * @param key
	 * @return
	 */
	public Serializable get(String key) throws CacheException;

	/**
	 * pus the object
	 * 
	 * @param key
	 * @param value
	 */
	public void put(String key, Serializable value) throws CacheException;

	/**
	 * clears the cache
	 */
	public void clear() throws CacheException;

	/**
	 * returns the size
	 * 
	 * @return
	 */
	public int size() throws CacheException;

	/**
	 * sets the configuration if there is one
	 * 
	 * @param map
	 * @throws Exception 
	 */
	public void setConfiguration(Map<?, ?> map) throws CacheException, Exception;
}
