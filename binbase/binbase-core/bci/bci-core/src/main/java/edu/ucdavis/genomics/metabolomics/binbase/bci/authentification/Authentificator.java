/*
 * Created on Nov 16, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.authentification;

import java.io.Serializable;

import edu.ucdavis.genomics.metabolomics.util.status.ReportType;

/**
 * generic way to authentificate a user
 * 
 * @author wohlgemuth
 */
public interface Authentificator extends Serializable {

	/**
	 * a login
	 */
	ReportType LOGIN = new ReportType("login", "login");

	ReportType REGISTER = new ReportType("register", "register");

	/**
	 * authentificates if this is a valid login configuration
	 * 
	 * @author wohlgemuth
	 * @version Nov 16, 2006
	 * @param username
	 * @param password
	 * @return
	 */
	public User authentificate(String username, String password) throws AuthentificationException;

	/**
	 * authentificate the user for the given database
	 * 
	 * @author wohlgemuth
	 * @version Nov 17, 2006
	 * @param username
	 * @param password
	 * @param database
	 * @return
	 * @throws AuthentificationException
	 */
	public User authentificate(String username, String password, String database) throws AuthentificationException;

	/**
	 * can we register a user
	 * 
	 * @return
	 */
	public boolean canRegisterUser() throws AuthentificationException;

	/**
	 * add a new user
	 * 
	 * @param user
	 * @throws AuthentificationException
	 */
	public void addUser(User user) throws AuthentificationException;
}
