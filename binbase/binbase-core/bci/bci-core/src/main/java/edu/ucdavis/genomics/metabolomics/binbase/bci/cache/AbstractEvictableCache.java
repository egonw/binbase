package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

/**
 * a basis cache with eviction support
 */
public abstract class AbstractEvictableCache implements EvictableCache {

	private Logger logger = Logger.getLogger(getClass());

	private List<EvictionPolicy> policies = new Vector<EvictionPolicy>();

	/**
	 * adds a policy
	 */
	public final void addEvictionPolicy(EvictionPolicy policy) {
		if (policies.contains(policy) == false) {
			policies.add(policy);
		}
	}

	/**
	 * list all policies
	 */
	public final List<EvictionPolicy> getPolicies() {
		return policies;
	}

	/**
	 * remove policies
	 */
	public final void removeEvictionPolicy(EvictionPolicy policy) {
		policies.remove(policy);
	}

	/**
	 * gets an object
	 */
	public final Serializable get(String key) {
		logger.debug("getting object for: " + key);
		Entry entry = this.getEntry(key);

		entry.increaseAccessCount();
		if (this.size() > 1) {
			this.fireEviction(this.findEntryForEviction());
		} else {
			this.fireEviction(entry);
		}
		//updating the entry
		this.addEntry(entry);
		return entry.value;
	}

	/**
	 * adds an object
	 */
	public final void put(String key, Serializable value) {
		logger.debug("adding new object to cache: " + key);
		fireEviction(findEntryForEviction());
		
		Entry entry = null;
		if(value instanceof Entry == false){
			entry = new Entry(key, value);
		}
		else{
			entry = (Entry) value;
		}
		this.addEntry(entry);
	}

	/**
	 * does the actual eviction
	 * 
	 * @param entry
	 */
	protected final void fireEviction(Entry entry) {
		logger.debug("firing request to evict this cache");
		if (entry == null) {
			logger.info("ignoring eviction since entry is null");
		} else {
			for (EvictionPolicy p : this.getPolicies()) {
				if (p.evict(entry, this)) {
					if(entry.needsEviction){
						logger.debug("evicted entry with policy: " + p);					
						this.removeEntry(entry);
						return;
					}
				}
			}
		}
	}

	/**
	 * trys to find the best entry for eviction or returns the first entry
	 * 
	 * @return
	 */
	protected Entry findEntryForEviction(){
		return getFirstEntry();
	}
	
	/**
	 * returns the first entry
	 * @return
	 */
	protected abstract Entry getFirstEntry();

	/**
	 * returns an entry
	 * 
	 * @return
	 */
	protected abstract Entry getEntry(String key);

	/**
	 * adds an entry to the cache
	 * 
	 * @param entry
	 */
	protected abstract void addEntry(Entry entry);

	/**
	 * removes an entry
	 * 
	 * @param entry
	 */
	protected abstract void removeEntry(Entry entry);

	/**
	 * simple entry class for internal caching
	 * 
	 */
	public final class Entry implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Entry) {
				Entry o = (Entry) obj;
				return o.key.equals(this.key);
			}
			return false;
		}

		private Date created = null;
		private String key = null;
		private Serializable value = null;
		private int accessCount = 0;
		
		private boolean needsEviction = false;

		public Entry(String key, Serializable value) {
			this.key = key;
			this.value = value;
			this.created = new Date();
			this.accessCount = 0;
		}

		public Date getCreated() {
			return created;
		}

		public String getKey() {
			return key;
		}

		public Serializable getValue() {
			return value;
		}

		public int getAccessCount() {
			return accessCount;
		}

		public void increaseAccessCount() {
			this.accessCount = accessCount + 1;
		}

		public boolean isNeedsEviction() {
			return needsEviction;
		}

		public void setNeedsEviction(boolean needsEviction) {
			this.needsEviction = needsEviction;
		}

		@Override
		public String toString() {
			return this.key;
		}
	}

	/**
	 * evicts an entry
	 */
	public final void evict(Entry entry) {
		this.fireEviction(entry);
	}
}
