package edu.ucdavis.genomics.metabolomics.binbase.bci.validator;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.WebserviceKeyManagerJMXFacade;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

/**
 * provides us with validators
 * 
 * @author wohlgemuth
 */
public abstract class ValidatorFactory extends AbstractFactory {

	public static final String DEFAULT_PROPERTY_NAME = ValidatorFactory.class
			.getName();

	public abstract Validator createValidator();

	public static ValidatorFactory newInstance() {
		return newInstance(System.getProperty(DEFAULT_PROPERTY_NAME));
	}

	public static ValidatorFactory newInstance(String factoryClass) {
		Class<?> classObject;
		ValidatorFactory factory;

		try {
			if (factoryClass == null) {
				return new ValidatorFactory() {

					@Override
					public Validator createValidator() {
						return new Validator() {

							public boolean isKeyValid(String key)
									throws AuthentificationException {
								try {
									WebserviceKeyManagerJMXFacade facade = Configurator.getKeyManager();
									
									boolean result = facade.isKeyValid(key);

									logger.info("key found: " + result);

									if (result == false) {
										throw new AuthentificationException(
												"key not found: " + key);
									}
									return result;
								}

								catch (Exception e) {
									ReportFactory.newInstance().create(
											getClass().getName()).report(key,
											Reports.FAILED,
											Reports.KEY_VALIDATION, e);

									if (e instanceof AuthentificationException == false) {
										throw new AuthentificationException(e);
									}
									else{
										throw (AuthentificationException)e;
									}
								}
							}

						};
					}

				};
			}
			classObject = Class.forName(factoryClass);
			factory = (ValidatorFactory) classObject.newInstance();
			return factory;

		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}

}
