package edu.ucdavis.genomics.metabolomics.binbase.bci.mail;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.URLDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.CommunicationJMXFacade;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * simple way to send emails to a central server
 * 
 * @author wohlgemuth
 * 
 */
public class SimpleMailService implements MailService {

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * sends a message
	 */
	public void sendMessage(String toEmailAddress, String subject, String content) throws BinBaseException {

		try {
			Message message = createEmail(toEmailAddress, content, subject);

			BodyPart body = new MimeBodyPart();

			body.setText(content);
			Multipart multi = new MimeMultipart();
			multi.addBodyPart(body);
			message.setContent(multi);
			message.setSentDate(new Date());

			Transport.send(message);
		} catch (MessagingException e) {
			throw new BinBaseException(e);
		}

	}

	/**
	 * sends a message
	 */
	public void sendMessage(String toEmailAddress, String subject, String content, String urlOfAttachmenet, String nameOfAttachment) throws BinBaseException {
		try {
			Message message = createEmail(toEmailAddress, content, subject);

			BodyPart body = new MimeBodyPart();

			body.setText(content);

			// create the attachment

			BodyPart attachment = new MimeBodyPart();
			attachment.setDataHandler(new DataHandler(new URLDataSource(new URL(urlOfAttachmenet))));
			attachment.setFileName(nameOfAttachment);

			Multipart multi = new MimeMultipart();
			multi.addBodyPart(body);
			multi.addBodyPart(attachment);

			message.setContent(multi);
			message.setSentDate(new Date());

			Transport.send(message);
		} catch (MessagingException e) {
			throw new BinBaseException(e);
		} catch (MalformedURLException e) {
			throw new BinBaseException(e);
		}

	}

	/**
	 * creates the actual email
	 * @param toEmailAddress
	 * @param content
	 * @param subject
	 * @return
	 * @throws BinBaseException
	 */
	public Message createEmail(String toEmailAddress, String content, String subject) throws BinBaseException {

		try {

			final CommunicationJMXFacade bean = edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator.getCommunicationService();
			//Security.addProvider(new Provider());

			Properties props = new Properties();

			props.put("mail.smtp.host", bean.getSmtpServer());
			props.put("mail.smtp.auth", "true");

			props.put("mail.smtp.port", bean.getSmtpPort());
			props.put("mail.smtp.socketFactory.port", bean.getSmtpPort());
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.socketFactory.fallback", "false");
			
			Authenticator auth = new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					try {
						return new PasswordAuthentication(bean.getUsername(), bean.getPassword());
					} catch (Exception e) {
						throw new RuntimeException("authentifiacation faild: " + e.getMessage(), e);
					}
				}
			};

			Session session = Session.getDefaultInstance(props, auth);

			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(bean.getFromAdress()));
			InternetAddress a[] = new InternetAddress[] { new InternetAddress(toEmailAddress) };

			msg.setRecipients(Message.RecipientType.TO, a);

			msg.setSubject(subject);
			msg.saveChanges();

			return msg;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BinBaseException(e.getMessage(), e);
		}
	}
}
