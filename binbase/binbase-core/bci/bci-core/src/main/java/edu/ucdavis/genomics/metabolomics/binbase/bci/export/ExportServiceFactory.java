/*
 * Created on Nov 18, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.export;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportBasedFactory;

/**
 * factory to create export services
 * 
 * @author wohlgemuth
 * @version Nov 18, 2005
 * 
 */
public abstract class ExportServiceFactory extends ReportBasedFactory {

	/**
	 * name of the property
	 */
	public static final String DEFAULT_PROPERTY_NAME = ExportServiceFactory.class
			.getName();

	public ExportServiceFactory() {
		super();
	}

	/**
	 * returns an new instance of the factory
	 * 
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static ExportServiceFactory newInstance(Report report) {
		return newInstance(System.getProperty(DEFAULT_PROPERTY_NAME), report);
	}

	/**
	 * returns an new instance of the specified factory
	 * 
	 * @param factoryClass
	 *            the specified factory to use
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static ExportServiceFactory newInstance(String factoryClass,
			Report report) {
		Class<?> classObject;
		ExportServiceFactory factory;

		try {
			if (factoryClass == null) {
				return new ExportServiceFactory() {

					@Override
					protected ExportService createService(Map p, Report report)
							throws ConfigurationException {
						return new ExportService() {

							public String export(String name, String setupXID)
									throws BinBaseException {
								throw new BinBaseException(
										"Sorry no class with an implementation was set!");

							}

							public String export(String name, int experimentID)
									throws BinBaseException {
								throw new BinBaseException(
										"Sorry no class with an implementation was set!");

							}

							public String export(String name, String setupXID,
									Source sop) throws BinBaseException {
								throw new BinBaseException(
										"Sorry no class with an implementation was set!");

							}

							public String export(String name, int experimentID,
									Source sop, Destination destination)
									throws BinBaseException {
								throw new BinBaseException(
										"Sorry no class with an implementation was set!");

							}

							public String export(String name, int experimentID,
									Source sop, Destination destination,
									boolean overwrite) throws BinBaseException {
								throw new BinBaseException(
										"Sorry no class with an implementation was set!");

							}
						};
					}

				};
			}
			classObject = Class.forName(factoryClass);
			factory = (ExportServiceFactory) classObject.newInstance();
			factory.setReport(report);
			return factory;

		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}

	/**
	 * creates the classimporter it self
	 * 
	 * @author wohlgemuth
	 * @version Nov 15, 2005
	 * @return
	 * @throws ConfigurationException
	 */
	protected abstract ExportService createService(Map p, Report report)
			throws ConfigurationException;

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jan 20, 2006
	 * @return
	 * @throws ConfigurationException
	 */
	public ExportService createService() throws ConfigurationException {
		return this.createService(System.getProperties(), this.getReport());
	}

	public ExportService createService(Map properties)
			throws ConfigurationException {
		return this.createService(properties, this.getReport());
	}
}
