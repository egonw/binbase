package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.CacheFactory;

public class SimpleCacheFactory extends CacheFactory{

	@Override
	public SimpleCache createCache() {
		return SimpleCache.getCache();
	}

}
