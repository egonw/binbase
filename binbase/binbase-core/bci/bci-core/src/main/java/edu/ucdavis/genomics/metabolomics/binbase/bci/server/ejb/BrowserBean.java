package edu.ucdavis.genomics.metabolomics.binbase.bci.server.ejb;

/* *
 * Created on Aug 7, 2006
 */
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Date;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.CreateException;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Connection;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


/**
 * Helper sessionbean to view the contents of a JMS queue.
 *
 * @ejb.bean name="JmsBrowser"
 *           type="Stateless"
 *           display-name="Jms Browser helpers"
 *           jndi-name="ejb/JmsBrowser"
 *           local-jndi-name="ejb/JmsBrowserLocal"
 *           view-type="both"
 *
 * @ejb.transaction type = "Required"
 *
 * @ejb.util generate = "physical"
 *
 * @ejb.resource-ref res-ref-name = "jms/QueueConnectionFactory"
 *                   res-type = "javax.jms.QueueConnectionFactory"
 *                   res-auth = "Container"
 * @jboss.resource-ref res-ref-name = "jms/QueueConnectionFactory"
 *                     jndi-name = "java:/JmsXA"
 *
 */
public class BrowserBean implements SessionBean {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private static org.apache.log4j.Logger logger =
            org.apache.log4j.Logger.getLogger(BrowserBean.class.getName());

    private transient QueueConnectionFactory queueConnectionFactory;


    /**
     * Creates a new queue browser.
     * @throws CreateException
     *
     * @ejb.create-method
     */
    public void ejbCreate() throws CreateException {
        try {
            init();
        } catch (NamingException e) {
            throw new CreateException("Could not create queue browser: "+e.getMessage());
        }
    }

    /**
     * Returns an <code>Enumeration</code> that is used to scan the queue's messages.
     *
     * @param queue the queue to browse
     * @return Enumeration containing the messages on the queue.
     * @throws JMSException if an error occurs while establishing the connection with the JMS provider.
     *
     * @ejb.interface-method
     */
    public Enumeration browseQueue(Queue queue) throws JMSException {
        return browseQueue(queue, null);
    }

    /**
     * Returns an <code>Enumeration</code> that is used to scan the queue's messages.
     *
     * @param queue the queue to browse
     * @param messageSelector the message selector to use
     * @return Enumeration containing the messages on the queue.
     * @throws JMSException if an error occurs while establishing the connection with the JMS provider.
     *
     * @ejb.interface-method
     */
    public Enumeration browseQueue(Queue queue, String messageSelector) throws JMSException {
        QueueConnection connection = null;
        QueueSession session = null;
        QueueBrowser browser = null;
        Enumeration queueContents = null;

        try {
            connection = getConnection();
            session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
            if (messageSelector != null) {
                browser = session.createBrowser(queue, messageSelector);
            } else {
                browser = session.createBrowser(queue);
            }
            queueContents = browser.getEnumeration();
            
            
        } finally {
            closeQueueBrowser(browser);
            closeSession(session);
            closeConnection(connection);
        }

        return queueContents;
    }

    /**
     * Returns a messages using the specified start date and end date.
     * @param queue the queue to browse
     * @param startDate the beginning date. If null no constraint is set on the start date
     * @param endDate the end date. If null, no constraint is set on the end date
     * @return an <code>Enumaration</code> of messages posted between <tt>startDate</tt> and <tt>endDate</tt>
     * @throws JMSException if an error occurs while establishing the connection with the JMS provider.
     *
     * @ejb.interface-method
     */
    public Enumeration browseQueue(Queue queue, Date startDate, Date endDate) throws JMSException {
        // Let's build the selector
        String selector = null;
        if (startDate != null && endDate != null) {
            selector = "JMSTimestamp >= " + startDate.getTime() + " AND JMSTimestamp <=" + endDate.getTime();
        } else if (startDate != null) {
            selector = "JMSTimestamp >= " + startDate.getTime();
        } else if (endDate != null) {
            selector = "JMSTimestamp <=" + endDate.getTime();
        } else { // no date is set
            selector = null;
        }

        return browseQueue(queue, selector);
    }

    /**
     * Closes the JMS connection.
     */
    private void closeConnection(Connection connection) {
        try {
            if (connection != null)
                connection.close();
        } catch (JMSException e) {
            logger.warn("Could not close JMS connection", e);
        }
    }

    /**
     * Closes the JMS session.
     */
    private void closeSession(Session session) {
        try {
            if (session != null)
                session.close();
        } catch (JMSException e) {
            logger.warn("Could not close JMS session", e);
        }
    }

    /**
     * Closes the JMS session.
     */
    private void closeQueueBrowser(QueueBrowser queueBrowser) {
        try {
            if (queueBrowser!= null)
                queueBrowser.close();
        } catch (JMSException e) {
            logger.warn("Could not close queue browser", e);
        }
    }


    protected QueueConnection getConnection() throws JMSException {
        return queueConnectionFactory.createQueueConnection();
    }

    public void ejbActivate() throws EJBException, RemoteException {
        try {
            init();
        } catch (NamingException e) {
            throw new EJBException("Could not activate queue browser: "+e.getMessage());
        }
    }

    public void ejbPassivate() throws EJBException, RemoteException {
    }

    public void ejbRemove() throws EJBException, RemoteException {
    }

    public void setSessionContext(SessionContext arg0) throws EJBException, RemoteException {
    }

    private void init() throws NamingException {
        Context jndiContext = new InitialContext();
        Context envContext = (Context) jndiContext.lookup("java:/comp/env");
        queueConnectionFactory = (QueueConnectionFactory) envContext.lookup("jms/QueueConnectionFactory");
    }
}
