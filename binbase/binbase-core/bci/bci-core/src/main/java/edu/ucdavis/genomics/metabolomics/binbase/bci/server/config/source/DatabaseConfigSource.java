package edu.ucdavis.genomics.metabolomics.binbase.bci.server.config.source;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;

/**
 * provides an xml stream with the database config properties
 * 
 * @author wohlgemuth
 */
public class DatabaseConfigSource implements Source {

	private DatabaseJMXFacade facade = null;

	/**
	 * configure the source with the given config
	 * @param configuration
	 * @throws ConfigurationException
	 */
	public DatabaseConfigSource(Map configuration) throws ConfigurationException{
		this.configure(configuration);
	}
	
	public DatabaseConfigSource() throws ConfigurationException{
		this.configure(System.getProperties());		
	}
	
	@SuppressWarnings("unchecked")
	public void configure(Map arg0) throws ConfigurationException {
		Hashtable temp = new Hashtable();
		temp.putAll(arg0);

		try {
			facade = DatabaseJMXFacadeUtil.getHome(temp).create();
		} catch (RemoteException e) {
			throw new ConfigurationException(e);
		} catch (CreateException e) {
			throw new ConfigurationException(e);
		} catch (NamingException e) {
			throw new ConfigurationException(e);
		}
	}

	public boolean exist() {
		return facade != null;
	}

	public String getSourceName() {
		return "Database Configuration";
	}

	/**
	 * gets the content of the configuration as xml stream
	 */
	public InputStream getStream() throws IOException {
		try {
			Properties p = facade.createProperties();

			StringWriter writer = protertiesToXML(p);

			return new ByteArrayInputStream(writer.getBuffer().toString()
					.getBytes());
		} catch (BinBaseException e) {
			throw new IOException(e.getMessage());
		}
	}

	/**
	 * converts given properties to xml
	 * @param p
	 * @return
	 */
	public StringWriter protertiesToXML(Properties p) {
		StringWriter writer = new StringWriter();
		writer.write("<config>\n");
		writer.write("\t<parameter>\n");

		Enumeration e = p.keys();

		while (e.hasMoreElements()) {
			String current = e.nextElement().toString();
			writer.write("\t\t<param name=\"" + current + "\" value=\""
					+ p.getProperty(current) + "\"/>\n");
		}

		writer.write("\t</parameter>\n");
		writer.write("</config>\n");
		
		return writer;
	}

	/**
	 * there is no version for this
	 */
	public long getVersion() {
		return 0;
	}

	public void setIdentifier(Object arg0) throws ConfigurationException {

	}

}
