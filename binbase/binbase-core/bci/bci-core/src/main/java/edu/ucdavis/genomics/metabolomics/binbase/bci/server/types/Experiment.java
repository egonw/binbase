/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.server.types;

import java.io.Serializable;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;

/**
 * @swt
 * @author wohlgemuth
 * 
 */
public class Experiment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private ExperimentClass[] classes;

	private String column;

	private String id;
	
	/**
	 * which email to use
	 */
	private String email;
	
	/**
	 * which priority
	 */
	private int priority = Scheduler.NONE;

	/**
	 * url used for processing
	 */
	private String sopUrl = null;

	/**
	 * is this experiment finished
	 */
	boolean finished = false;

	private int increase = 0;

	public Experiment() {
		super();
		this.priority = Scheduler.PRIORITY_LOW;
	}

	/**
	 * 
	 * @return
	 */
	public ExperimentClass[] getClasses() {
		return this.classes;
	}

	public void setClasses(ExperimentClass[] classes) {
		this.classes = classes;
	}

	/**
	 * *
	 * 
	 * @swt.variable visible="true" name="Id" searchable="true"
	 * @swt.modify canModify="true"
	 * @author wohlgemuth
	 * @version Aug 16, 2006
	 * @return
	 */
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * *
	 * 
	 * @author wohlgemuth
	 * @version Aug 16, 2006
	 * @return
	 */
	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public boolean equals(Object arg0) {
		if (arg0 instanceof Experiment) {
			return ((Experiment) arg0).getId().equals(this.getId());
		}
		return false;
	}

	public int hashCode() {
		return this.getId().hashCode();
	}

	/**
	 * *
	 * 
	 * @swt.variable visible="true" name="Sop Url" searchable="true"
	 * @swt.modify canModify="true"
	 * @author wohlgemuth
	 * @version Aug 16, 2006
	 * @return
	 */
	public String getSopUrl() {
		return sopUrl;
	}

	public void setSopUrl(String sopUrl) {
		this.sopUrl = sopUrl;
	}

	@Override
	public String toString() {
		return this.getId() + "/" + this.getColumn() + "/" + this.getSopUrl();
	}

	/**
	 * @swt.variable visible="true" name="Column" searchable="true"
	 * @swt.modify canModify="true"
	 * @author wohlgemuth
	 * @version Aug 16, 2006
	 * @return
	 */
	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public int getIncrease() {
		return increase;
	}

	public void setIncrease(int increase) {
		this.increase = increase;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
