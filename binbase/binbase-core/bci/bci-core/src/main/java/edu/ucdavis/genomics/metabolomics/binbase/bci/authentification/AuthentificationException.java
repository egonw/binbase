/*
 * Created on Nov 16, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.authentification;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * @author wohlgemuth
 * @version Nov 16, 2006
 *
 */
public class AuthentificationException extends BinBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * @author wohlgemuth
	 * @version Nov 16, 2006
	 */
	public AuthentificationException() {
		
	}

	/**
	 * @author wohlgemuth
	 * @version Nov 16, 2006
	 * @param message
	 */
	public AuthentificationException(String message) {
		super(message);
		
	}

	/**
	 * @author wohlgemuth
	 * @version Nov 16, 2006
	 * @param cause
	 */
	public AuthentificationException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @author wohlgemuth
	 * @version Nov 16, 2006
	 * @param message
	 * @param cause
	 */
	public AuthentificationException(String message, Throwable cause) {
		super(message, cause);
		
	}

}
