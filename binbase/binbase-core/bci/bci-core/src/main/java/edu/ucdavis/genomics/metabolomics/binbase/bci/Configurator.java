package edu.ucdavis.genomics.metabolomics.binbase.bci;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.AuthentificationProviderJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.AuthentificationProviderJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.CacheJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.CacheJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.CommunicationJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.CommunicationJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.MetaProviderJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.MetaProviderJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.WebserviceKeyManagerJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.WebserviceKeyManagerJMXFacadeUtil;

/**
 * access to the jmx configurations so that we can configure the server from the
 * client side
 * 
 * @author wohlgemuth
 * 
 */
public class Configurator extends edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator {

	public static final String BCI_GLOBAL_NAME = "bci";
	
	public static CommunicationJMXFacade getCommunicationService() throws RemoteException, CreateException, NamingException {
		return CommunicationJMXFacadeUtil.getHome().create();
	}

	public static CacheJMXFacade getCacheService() throws RemoteException, CreateException, NamingException {
		return CacheJMXFacadeUtil.getHome().create();
	}

	public static AuthentificationProviderJMXFacade getAuthentificationService() throws RemoteException, CreateException, NamingException {
		return AuthentificationProviderJMXFacadeUtil.getHome().create();
	}

	public static DatabaseJMXFacade getDatabaseService() throws RemoteException, CreateException, NamingException {
		return DatabaseJMXFacadeUtil.getHome().create();
	}

	public static ExportJMXFacade getExportService() throws RemoteException, CreateException, NamingException {
		return ExportJMXFacadeUtil.getHome().create();
	}

	public static MetaProviderJMXFacade getMetaService() throws RemoteException, CreateException, NamingException {
		return MetaProviderJMXFacadeUtil.getHome().create();
	}

	public static StatusJMXFacade getStatusService() throws RemoteException, CreateException, NamingException {
		return StatusJMXFacadeUtil.getHome().create();
	}

	public static ServiceJMXFacade getImportService() throws RemoteException, CreateException, NamingException {
		return ServiceJMXFacadeUtil.getHome().create();
	}

	public static WebserviceKeyManagerJMXFacade getKeyManager() throws RemoteException, CreateException, NamingException {
		return WebserviceKeyManagerJMXFacadeUtil.getHome().create();
	}
}
