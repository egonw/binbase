package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;

import java.util.List;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache.Entry;

/**
 * a cache which supports evictions 
 *
 */
public interface EvictableCache extends Cache{
	/**
	 * adds a policy
	 * @param policy
	 */
	public void addEvictionPolicy(EvictionPolicy policy);
	
	/**
	 * removes a policy
	 * @param policy
	 */
	public void removeEvictionPolicy(EvictionPolicy policy);
	
	/**
	 * return all policies
	 * @return
	 */
	public List<EvictionPolicy> getPolicies();

	/**
	 * evicts the given entry
	 * @param entry
	 */
	public void evict(Entry entry);
}
