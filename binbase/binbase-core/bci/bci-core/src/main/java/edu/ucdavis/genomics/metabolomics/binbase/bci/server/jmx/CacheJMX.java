package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.Cache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.CacheFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictionPolicy;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache.Entry;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.FileCacheFactory;

/**
 * simple wrapper to access the cache
 * 
 * @author Administrator
 * @jmx.mbean description = "simple cache management" extends =
 *            "javax.management.MBeanRegistration"
 *            name="binbase.cache:service=QueryCache"
 */
public class CacheJMX implements EvictableCache, CacheJMXMBean {

	private String cacheFactory = null;

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public void clear() {
		getCache().clear();
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public boolean contains(String key) {
		return getCache().contains(key);
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public void print() {
		logger.info(this.getCache());
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public Serializable get(String key) {
		return getCache().get(key);
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public void put(String key, Serializable value) {
		getCache().put(key, value);
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public int size() {
		return getCache().size();
	}

	public void postDeregister() {
	}

	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					file));
			this.cacheFactory = (String) in.readObject();

		} catch (Exception e) {
			logger.info(e.getMessage());
			this.cacheFactory = FileCacheFactory.class.getName();
		}
	}

	public void preDeregister() throws Exception {
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1)
			throws Exception {
		return null;
	}

	private void store() {
		logger.info("store properties");
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			out.writeObject(getCacheFactory());
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public String getCacheFactory() {
		return cacheFactory;
	}

	/**
	 * @throws Exception
	 * @jmx.managed-operation description="cache operation"
	 */
	public void setCacheFactory(String cacheFactory) {
		this.cacheFactory = cacheFactory;
		System.setProperty(CacheFactory.DEFAULT_PROPERTY_NAME, this.cacheFactory);
		store();
	}

	/**
	 * @throws Exception
	 * @jmx.managed-operation description="cache operation"
	 */
	public Cache getCache() {
		try {
			return CacheFactory.getInstance(this.cacheFactory).createCache();
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}

	/**
	 * @throws Exception
	 * @throws CacheException
	 * @jmx.managed-operation description="cache operation"
	 */
	public void setConfiguration(Map<?, ?> map) throws CacheException,
			Exception {
		getCache().setConfiguration(map);
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public void addEvictionPolicy(EvictionPolicy policy) {
		this.getEvictableCache().addEvictionPolicy(policy);
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public void evict(Entry entry) {
		getEvictableCache().evict(entry);
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public List<EvictionPolicy> getPolicies() {
		return getEvictableCache().getPolicies();
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public void removeEvictionPolicy(EvictionPolicy policy) {
		getEvictableCache().removeEvictionPolicy(policy);
	}

	/**
	 * @jmx.managed-operation description="cache operation"
	 */
	public EvictableCache getEvictableCache() {
		if (this.getCache() instanceof EvictableCache) {
			return (EvictableCache) this.getCache();
		} else {
			throw new RuntimeException("sorry not a cache of this kind");
		}
	}
}
