package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

/**
 * jmx needed for communications
 * 
 * @author wohlgemuth *
 * @jmx.mbean description = "needed for the configuration of the email sending
 *            system" extends = "javax.management.MBeanRegistration" name="binbase:service=Communication"
 */
public class CommunicationJMX implements CommunicationJMXMBean {
	private String username;

	private String password;

	private String smtpServer;

	private String fromAdress;

	private String smtpPort;

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * @jmx.managed-operation description="email address of the sender"
	 * @return
	 */
	public String getFromAdress() {
		return fromAdress;
	}

	/**
	 * @jmx.managed-operation description="email address of the sender"
	 * @param fromAdress
	 */
	public void setFromAdress(String fromAdress) {
		this.fromAdress = fromAdress;
		store();
	}

	/**
	 * @jmx.managed-operation description="password of the email account"
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @jmx.managed-operation description="password of the email account"
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
		store();
	}

	/**
	 * @jmx.managed-operation description="smtp port"
	 * @return
	 */
	public String getSmtpPort() {
		return smtpPort;
	}

	/**
	 * @jmx.managed-operation description="smtp port"
	 * @param smtpPort
	 */
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
		store();
	}

	/**
	 * @jmx.managed-operation description="smtp server"
	 * @return
	 */
	public String getSmtpServer() {
		return smtpServer;
	}

	/**
	 * @jmx.managed-operation description="smtp server"
	 * @param smtpServer
	 */
	public void setSmtpServer(String smtpServer) {
		this.smtpServer = smtpServer;
		store();
	}

	/**
	 * @jmx.managed-operation description="username"
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @jmx.managed-operation description="username"
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
		store();
	}

	public void postDeregister() {
		// TODO Auto-generated method stub
		store();
	}

	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					file));
			this.fromAdress = (String) in.readObject();
			this.password = (String) in.readObject();
			this.smtpPort = (String) in.readObject();
			this.smtpServer = (String) in.readObject();
			this.username = (String) in.readObject();

		} catch (Exception e) {
			logger.warn("postRegister(Boolean)", e); //$NON-NLS-1$

		}
	}

	public void preDeregister() throws Exception {
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1)
			throws Exception {
		return null;
	}

	private void store() {
		logger.info("store properties");
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			out.writeObject(getFromAdress());
			out.writeObject(getPassword());
			out.writeObject(getSmtpPort());
			out.writeObject(getSmtpServer());
			out.writeObject(getUsername());

			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}
}
