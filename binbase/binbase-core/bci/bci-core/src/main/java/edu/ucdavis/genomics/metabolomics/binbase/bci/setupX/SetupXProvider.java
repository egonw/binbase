/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.setupX;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * @author wohlgemuth is an interface, used to communicate with a setup
 *         configuration
 */
public interface SetupXProvider{

	/**
	 * @param sampleName
	 * @return the setupX id for this sample or null if nothing found
	 */
	public String getSetupXId(String sampleName) throws BinBaseException;

	/**
	 * return the metadata for the class
	 * @param setupXId
	 * @return the metainformations as xml document. or null if nothing is found
	 */
	public String getMetaInformationByClass(String setupXId) throws BinBaseException;

	/**
	 * returns the meta data for the sample
	 * @param setupXId
	 * @return
	 * @throws BinBaseException
	 */
	public String getMetaInformationBySample(String setupXId) throws BinBaseException;

	/**
	 * returns the meta data for the experiment
	 * @param setupXId
	 * @return
	 * @throws BinBaseException
	 */
	public String getMetaInformationByExperiment(String setupXId) throws BinBaseException;

	/**
	 * send the calculated result to setupX
	 * 
	 * @author wohlgemuth
	 * @version Feb 9, 2006
	 * @param string
	 * @param content
	 * @throws BinBaseException
	 */
	public void upload(String experimentId, String content) throws BinBaseException;

	/**
	 * can this setupxId create new bins
	 * @param setupxId
	 * @return
	 * @throws BinBaseExcpetion
	 */
	public boolean canCreateBins(String setupxId) throws BinBaseException;
}
