package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificatorFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;

/**
 * *
 * 
 * @jmx.mbean name="binbase:service=SimpleAuthentificationProvider" description =
 *            "access to the simple authentification provider" extends =
 *            "javax.management.MBeanRegistration"
 * @author wohlgemuth
 */
public class SimpleAuthentificationProviderJMX implements SimpleAuthentificationProviderJMXMBean{

	/**
	 * @jmx.managed-operation description = "adds a new user" adds a new master
	 *                        user
	 * @param username
	 * @param password
	 * @throws AuthentificationException
	 */
	public void addMasterUser(String username, String password) throws AuthentificationException {
		AuthentificatorFactory factory = AuthentificatorFactory.newInstance("edu.ucdavis.genomics.metabolomics.binbase.bci.SimpleAuthentificatorFactory");
		factory.create().addUser(new User(password, username, username.hashCode(), true));
	}

	/**
	 * @jmx.managed-operation description = "adds a new user" adds a new master
	 *                        user
	 * @param username
	 * @param password
	 * @throws AuthentificationException
	 */
	public void addNormalUser(String username,String password)throws AuthentificationException{
		AuthentificatorFactory factory = AuthentificatorFactory.newInstance("edu.ucdavis.genomics.metabolomics.binbase.bci.SimpleAuthentificatorFactory");
		factory.create().addUser(new User(password, username, username.hashCode(), false));		
	}
	/**
	 * @jmx.managed-operation description = "adds a new user" adds a new master
	 *                        user checks if the user exits
	 * @param username
	 * @param password
	 * @throws AuthentificationException
	 */

	public User queryForUser(String username, String password) throws AuthentificationException {
		AuthentificatorFactory factory = AuthentificatorFactory.newInstance("edu.ucdavis.genomics.metabolomics.binbase.bci.SimpleAuthentificatorFactory");

		return factory.create().authentificate(username, password);

	}

	public void postDeregister() {
		
	}

	public void postRegister(Boolean arg0) {
		
	}

	public void preDeregister() throws Exception {
	}

	public ObjectName preRegister(MBeanServer server, ObjectName name) throws Exception {
		return null;
	}

}
