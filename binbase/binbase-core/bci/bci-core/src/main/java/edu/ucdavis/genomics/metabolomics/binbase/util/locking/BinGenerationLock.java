package edu.ucdavis.genomics.metabolomics.binbase.util.locking;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Date;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.exception.LockingException;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.Lockable;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.LockableFactory;

/**
 * a singleton for internal locking
 * 
 * @author wohlgemuth
 * 
 */
public class BinGenerationLock implements Serializable {

	private Logger logger = Logger.getLogger(getClass());
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static BinGenerationLock instance;
	public BinGenerationLock() {
	}

	/**
	 * generates a new instance of the import lock tool
	 * 
	 * @return
	 */
	public static BinGenerationLock getInstance() {
		if (instance == null) {
			instance = new BinGenerationLock();
		}
		return instance;
	}

	/**
	 * obtains a lock
	 * 
	 * @param identifier
	 * @throws NamingException
	 * @throws ClusterException
	 * @throws RemoteException
	 * @throws LockingException
	 */
	public void obtainLock(String column, String className)
			throws LockingException, RemoteException, ClusterException,
			NamingException {
		// aquire timeout of the given time
		Lockable lock = LockableFactory.newInstance().create("binbase");

		Lock l = new Lock("bin modification lock", column, new Date(), className);
		logger.info("trying to aquire lock: " + l);
		lock.aquireRessource(l, Configurator.getConfigService().getTimeout());
		logger.info("lock is aquired");
	}

	/**
	 * releases a lock
	 * 
	 * @param identifier
	 * @throws LockingException
	 */
	public void releaseLock(String column, String className)
			throws LockingException {

		Lockable lock = LockableFactory.newInstance().create("binbase");

		Lock l = new Lock("bin modification lock", column, new Date(), className);
		logger.info("trying to release lock: " + l);

		lock.releaseRessource(l);
	}
}
