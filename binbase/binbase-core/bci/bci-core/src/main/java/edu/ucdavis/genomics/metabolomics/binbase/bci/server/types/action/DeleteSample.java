package edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.action;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;

/**
 * to delete a sample
 * @author wohlgemuth
 *
 */
public class DeleteSample extends ExperimentSample{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

}
