package edu.ucdavis.genomics.metabolomics.binbase.cluster.job;

import java.io.Serializable;

/**
 * just needs the database as an argument
 * 
 * @author wohlgemuth
 */
public class UpdateBinBaseJob implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String column;

	public String getColumn() {
		return column;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UpdateBinBaseJob) {
			UpdateBinBaseJob job = (UpdateBinBaseJob) obj;
			return job.getColumn().equals(this.getColumn());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getColumn().hashCode();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " - " + this.getColumn();
	}

	public void setColumn(String column) {
		this.column = column;
	}
}
