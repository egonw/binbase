package edu.ucdavis.genomics.metabolomics.binbase.bci.mail;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;


/**
 * defines a simple mail services
 * @author wohlgemuth
 *
 */
public interface MailService {
	
	/**
	 * creates the message
	 * @param toEmailAddress
	 * @param subject
	 * @param content
	 * @return
	 */
	public void sendMessage(String toEmailAddress, String subject,String content)throws BinBaseException;
	
	/**
	 * send a message with a file attachment
	 * @param toEmailAddress
	 * @param subject
	 * @param content
	 * @param urlOfAttachmenet url to the attachment
	 * @param nameOfAttachment name of the attachment
	 */
	public void sendMessage(String toEmailAddress, String subject,String content, String urlOfAttachmenet, String nameOfAttachment)throws BinBaseException;
}
