package edu.ucdavis.genomics.metabolomics.binbase.dsl;

import static org.junit.Assert.*

import java.util.Collection;

import org.jdom.output.Format
import org.jdom.output.XMLOutputter
import org.junit.After
import org.junit.Before

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.dsl.ExporterDSL
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.SopGenerator
import edu.ucdavis.genomics.metabolomics.binbase.dsl.type.CalibrationSample;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling

class ExporterDSLTest extends GroovyTestCase {

	org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass())

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@org.junit.Test
	public void testReadExporterRulesSimple(){
		ExporterDSL dsl = new ExporterDSL()

		def map = dsl.readExporterRules (new File("src/test/resources/dsl/simple.dsl"))

		def soap = new SopGenerator().generateSOP(map)

		assert map.configuration instanceof XMLConfigable
	}
	public void testReadExporterRulesSimpleNoClasses(){
		ExporterDSL dsl = new ExporterDSL()

		def map = dsl.readExporterRules (new File("src/test/resources/dsl/noClasses.dsl"))

		def soap = new SopGenerator().generateSOP(map)

		assert map.configuration instanceof XMLConfigable

		assert map.classes.size == 2

		Collection classes = map.classes

		classes.each { ExperimentClass c ->
			assert c.getSamples().length == 1

			c.samples.each { assert it instanceof ExperimentSample }
		}
	}

	@org.junit.Test
	public void testReadExporterRulesWithConfig(){
		ExporterDSL dsl = new ExporterDSL()

		def map = dsl.readExporterRules (new File("src/test/resources/dsl/simpleConfig.dsl"))

		def soap = new SopGenerator().generateSOP(map)

		assert map.configuration instanceof XMLConfigable
	}
	
	@org.junit.Test
	public void testReadExporterRulesWithCalibration(){
		ExporterDSL dsl = new ExporterDSL()

		def map = dsl.readExporterRules (new File("src/test/resources/dsl/calibration.dsl"))

		def sop = new SopGenerator().generateSOP(map)

		assert map.configuration instanceof XMLConfigable

		assert map.classes.size == 1

		assert map.report[0].calibration
		Collection classes = map.classes

		ExperimentClass c = classes.iterator().next()

		assert c.getSamples().length == 10

		c.samples.each { assert it instanceof CalibrationSample }
		
		assert map.compounds.size == 3
		println new XMLOutputter(Format.getPrettyFormat()).outputString(XmlHandling.readXml(sop.getStream()))
	}
	
	@org.junit.Test
	public void testReadExporterRulesWithLazyCalibration(){
		ExporterDSL dsl = new ExporterDSL()

		def map = dsl.readExporterRules (new File("src/test/resources/dsl/lazycalibration.dsl"))

		def sop = new SopGenerator().generateSOP(map)

		assert map.configuration instanceof XMLConfigable

		assert map.classes.size == 1

		assert map.report[0].calibration
		Collection classes = map.classes

		ExperimentClass c = classes.iterator().next()

		assert c.getSamples().length == 10

		c.samples.each { assert it instanceof CalibrationSample }
		
		assert map.compounds.size == 3
		println new XMLOutputter(Format.getPrettyFormat()).outputString(XmlHandling.readXml(sop.getStream()))
	}
	
	@org.junit.Test
	public void testReadExporterRulesWithRealLazyCalibration(){
		ExporterDSL dsl = new ExporterDSL()

		def map = dsl.readExporterRules (new File("src/test/resources/dsl/reallazycalibration.dsl"))

		def sop = new SopGenerator().generateSOP(map)

		assert map.configuration instanceof XMLConfigable

		assert map.classes.size == 1

		assert map.report[0].calibration
		Collection classes = map.classes

		ExperimentClass c = classes.iterator().next()

		assert c.getSamples().length == 10

		c.samples.each { assert it instanceof CalibrationSample }
		
		assert map.compounds.size == 3
		println new XMLOutputter(Format.getPrettyFormat()).outputString(XmlHandling.readXml(sop.getStream()))
	}
	
	@org.junit.Test
	public void testReadExporterRulesWithRealLazyCalibrationAndCustomSettings(){
		ExporterDSL dsl = new ExporterDSL()

		def map = dsl.readExporterRules (new File("src/test/resources/dsl/reallazycalibrationwithsettings.dsl"))

		def sop = new SopGenerator().generateSOP(map)

		assert map.configuration instanceof XMLConfigable

		assert map.classes.size == 2

		assert map.report[0].calibration 
		Collection classes = map.classes

		Iterator iter = classes.iterator()
		ExperimentClass c = iter.next()

		assert c.getSamples().length == 10

		c.samples.each { assert it instanceof CalibrationSample }
		
		ExperimentClass d = iter.next()
		
		assert d.getSamples().length == 3
		d.samples.each { assert it instanceof CalibrationSample }
				
		assert map.compounds.size == 3
		println new XMLOutputter(Format.getPrettyFormat()).outputString(XmlHandling.readXml(sop.getStream()))
	}
	
	
}
