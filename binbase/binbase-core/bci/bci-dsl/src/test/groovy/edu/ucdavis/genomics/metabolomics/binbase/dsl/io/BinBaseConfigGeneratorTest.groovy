package edu.ucdavis.genomics.metabolomics.binbase.dsl.io;

import static org.junit.Assert.*;

import org.apache.xerces.parsers.XML11Configurable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;

class BinBaseConfigGeneratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenerateBinBaseOptions() {

		//the configuration script
		def toTest = """
		
			config{
				bin{
					allow(minimumClassSize:1)
				}
			}

		
		"""
		
		
		BinBaseConfigGenerator config = new BinBaseConfigGenerator()
		def xml = config.generateBinBaseOptions (toTest)
		
	}
	
	@Test
	public void testGenerateBinBaseOptionsAsConfiguable() {

		//the configuration script
		def toTest = """
		
			config{
				bin{
					allow(minimumClassSize:1)
				}
			}

		
		"""
		
		
		BinBaseConfigGenerator config = new BinBaseConfigGenerator()
		XMLConfigable xml = config.generateBinBaseOptionsAsConfiguable (toTest)
		xml.printTree System.out
		assert(xml.getAttributeValue ("bin.allow","minimumClassSize") == "1")
	}
	
}
