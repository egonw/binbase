export{

	server "test"
	
	name "calibration file examle"
	
	newBins false
	
	column "test"
	
	multithread false
	
	caching false
	
	ignoreMissingSamples false
	

	report{

                //specification how far we want to size down the dataset
                sizedown 10

                //specified output format
                format "xls"
                
                //we want to replace values. Mandetory for calibrations
                replace true
                
                //we want the calibration applied in this report
                calibration true
        }

   //this is a calibration class to be used for calibrations. There should only 1 exist for each dsl. This shows how to lazily intialise this
	CalibrationClass(name:"test"){
	
		samples "a": 0.01,
		 "b": 0.05,
		 "c": 0.1,
		 "d": 0.25,
		 "e": 0.5,
		 "f": 1,
		 "g": 2.5,
		 "h": 5,
		 "i": 10,
		 "j": 20
		
	}
	
	//we need to specify bins, since we have a calibration
	bin "alanine"
	bin "glucose"
	bin "cytosine"
}