export{
	//name of the generated experiment
	name "test"

	//server of the used binbase instance
	server "lila"

	//database of the used binbase instance
	column "test"

	//lets define a single sample for this export
	sample "test"
	
	sample "test-2"

	//defintion for the report
	report{

		//specification how far we want to size down the dataset
		sizedown 80

		//do we want to replace zeros
		replace

		//specified output format
		format "xls"
	}
	//end of report definiton
}
//end of DSL definition
