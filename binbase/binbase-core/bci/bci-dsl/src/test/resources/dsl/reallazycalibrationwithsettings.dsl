export{

	server "test"
	
	name "calibration file examle"
	
	newBins false
	
	column "test"
	
	multithread false
	
	caching false
	
	ignoreMissingSamples false
	

	report{

                //specification how far we want to size down the dataset
                sizedown 10

                //specified output format
                format "xls"
                
                //we want to replace values. Mandatory for calibrations
                replace true
                
                //calibration configuration
                calibration{

					//write out a designated calibrated file
                    postProcess(fileName:"calibrated",method:"edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.WriteProcessedResult", format:"xls")

                }
        }

	//calibration class
	CalibrationClass{
		samples "a": 0.01,"b": 0.05,"c": 0.1, "d": 0.25,"e": 0.5,"f": 1, "g": 2.5,"h": 5,"i": 10,"j": 20		
	}
	//calibration class
	CalibrationClass{
		samples "a1": 0.01,"b1": 0.05,"c2": 0.1
	}
	
	//we need to specify bins, since we have a calibration. We do it the lazy way
	bins "alanine", "glucose", "cytosine"
}