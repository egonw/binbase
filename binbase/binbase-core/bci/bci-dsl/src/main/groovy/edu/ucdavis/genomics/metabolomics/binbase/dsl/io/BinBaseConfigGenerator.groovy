package edu.ucdavis.genomics.metabolomics.binbase.dsl.io

import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfiguration;
import edu.ucdavis.genomics.metabolomics.util.io.source.ByteArraySource;
import groovy.xml.MarkupBuilder;
import groovy.xml.StreamingMarkupBuilder;

import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom.Element;

/**
 * uses to generate the actual binbase configuration script
 * @author wohlgemuth
 *
 */
class BinBaseConfigGenerator{
	
	Logger logger = Logger.getLogger(getClass())
	
	/**
	 * generarte binbase options as an xml file, which can be later merged with the central binbase option file
	 * @param parameter
	 * @return
	 */
	String generateBinBaseOptions(String groovyScriptContent){
		
		def writer = new StringWriter()
		def xml = new MarkupBuilder(writer)
		def binding = new XmlBinding()
		binding.builder = xml
		
		def dslScript = new GroovyShell().parse(groovyScriptContent.trim())
		dslScript.binding =  binding
		dslScript.run()
		
		return writer.toString()
	}
	
	/**
	 * generates a binbase configuration object directly
	 * @param groovyScriptContent
	 * @return
	 */
	XMLConfigable generateBinBaseOptionsAsConfiguable(String groovyScriptContent){
		XMLConfiguration config = new XMLConfiguration()
	
		String content = generateBinBaseOptions (groovyScriptContent)

		config.setSource (new ByteArraySource(content.getBytes()))

		return config
	}
}

class XmlBinding extends Binding{
	def builder
	
	
	Object getVariable(String name) {
		return { Object... args ->  builder.invokeMethod(name,args) }
	}
}