
package edu.ucdavis.genomics.metabolomics.binbase.dsl.io

import org.jdom.output.XMLOutputter;

import org.jdom.output.Format;

import org.jdom.output.XMLOutputter;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;

import groovy.xml.StreamingMarkupBuilder

/**
 * generates an xml config file for the xmlconfigurator
 * User: wohlgemuth
 * Date: Sep 16, 2009
 * Time: 12:20:31 PM
 *
 */

public class GenerateConfigFile {

  /**
   * does the acutal generation and sets the default values
   */
  static def generateFile(String applicationServer,Map binbaseParameter = [:]) {
    String result = new StreamingMarkupBuilder().bind({

      config {
        parameter {
          param(name: "java.naming.provider.url", value: applicationServer, public: true)
          param(name: "java.naming.factory.initial", value: "org.jnp.interfaces.NamingContextFactory", public: true)
          param(name: "java.naming.factory.url.pkgs", value: "org.jboss.naming:org.jnp.interfaces", public: true)
          param(name: "edu.ucdavis.genomics.metabolomics.util.status.ReportFactory", value: "edu.ucdavis.genomics.metabolomics.binbase.cluster.status.EJBReportFactory")
          param(name: "edu.ucdavis.genomics.metabolomics.util.thread.locking.LockableFactory", value: "edu.ucdavis.genomics.metabolomics.binbase.cluster.locking.EJBLockingFactory")
		  
		  /**
		   * adds all the given parameters to the config file
		   */
		  if(binbaseParameter != null && binbaseParameter.isEmpty() == false){
			  binbaseParameter.each{ def currentKeyValuePair ->
				  param(name: currentKeyValuePair.key.toString().trim(), value: currentKeyValuePair.value.toString().trim(),public:true)
			  }
		  }
        }
		target(name:"binbase.sql"){
			"class"(name:"edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfiguration")
			parameter{
				data("sql.xml")
				factory("edu.ucdavis.genomics.metabolomics.binbase.bci.io.ConfigSourceFactory")
			}
		}
		target(name:"binbase.config"){
			"class"(name:"edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfiguration")
			parameter{
				data("binbase.xml")
				factory("edu.ucdavis.genomics.metabolomics.binbase.bci.io.ConfigSourceFactory")
			}			
		}
      }
    }).toString()
	
	result = new XMLOutputter(Format.getPrettyFormat() ).outputString(XmlHandling.readXml (new StringReader(result)))
 
	Logger.getLogger ("GenerateConfigFile").info result
	return result 
  }

}