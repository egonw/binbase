
package edu.ucdavis.genomics.metabolomics.binbase.dsl.generate
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceFactory
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.ConfiguratorSource
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.GenerateConfigFile
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator
import org.apache.log4j.Logger

import java.util.regex.Pattern

class GenerateDSLFromDatabase {

	public static void writeDSL(File file, String dsl){
		file.write dsl
	}

	/**
	 * generates the dsl from the complete database based on a filename pattern
	 * @return
	 */
	public String dslGenerateFromDatabaseForFilePattern(String database,Pattern fileNamePattern, String server = "127.0.0.1", boolean replace = true, int sizeDown = 80, String format = "xls"){

		//does the actual filtering based on the fileName pattern
		def closure = {ExperimentClass clazz, StringBuffer buffer,Experiment experiment ->

			Logger logger = Logger.getLogger("filter sample")

			ExperimentClass clazz2 = new ExperimentClass()

			clazz2.id = clazz.id
			clazz2.column = clazz.column
			clazz2.priority = clazz.priority

			List samples = []

			//actually matches the samples
			clazz.samples.each {ExperimentSample sample ->

				if(sample.name.matches(fileNamePattern)){
					logger.info "sample ${sample.name} matches ${fileNamePattern} -> keep"
					samples.add(sample)
				}
				else{
					logger.info "sample ${sample.name} doesnt match ${fileNamePattern} -> skip"
				}
			}

			clazz2.samples = samples.toArray()

			if(clazz2.samples.length != 0){
				//return the new modified class
				return clazz2
			}
			else{
				return null
			}
		}

		return this.dslGenerateForDatabase (database,server,replace,sizeDown,format,closure)
	}

	/**
	 * generates a dsl for the specific classes and database
	 */
	public String dslGenerateForClasses(Collection<String> classIds, String database, String server = "127.0.0.1", boolean replace = true, int sizeDown = 80, String format = "xls",def closure = null ){

		XMLConfigurator.getInstance(new ConfiguratorSource(GenerateConfigFile.generateFile(server)))

		BinBaseService service = BinBaseServiceFactory.createFactory().createService()

		//fetch the complete database as experiment

		ExperimentClass[] classes = new ExperimentClass[classIds.size()]
		String id = "combined:"

		/**
		 * fetch experiment classes
		 */
		classIds.eachWithIndex { String classId, int i ->

			ExperimentClass clazz =service.getStoredClass (database, classId, Configurator.getKeyManager().getInternalKey())

			classes[i] = clazz
			id = id + " " + classId
		}

		//create a single experiment of all the classes
		Experiment experiment = new Experiment()

		experiment.setClasses classes
		experiment.setColumn database
		experiment.setId id

		//run the generate function
		GenerateDSLFromExperiment generate = new GenerateDSLFromExperiment()

		return generate.dslGenerateForExperiment (experiment, server, replace, sizeDown, format,closure)

	}

	/**
	 * generates the string for a required experiment
	 * @param setupXName
	 * @param database
	 * @return
	 */
	public String generateClassSectionForExperiment(String setupXName, String database, String server = "127.0.0.1"){
		BinBaseService service = BinBaseServiceFactory.createFactory().createService()

		//fetch teh experiment
		Experiment experiment = service.getExperiment(database, setupXName, Configurator.getKeyManager().getInternalKey())

		GenerateDSLFromExperiment generate = new GenerateDSLFromExperiment()

		return generate.generateClasses(experiment, new StringBuffer()).toString()
	}
	/**
	 * generates the dsl representing the given setupx version
	 * @param setupXName
	 * @return
	 */
	public String dslGenerateForExperiment(String setupXName, String database, String server = "127.0.0.1", boolean replace = true, int sizeDown = 80, String format = "xls",def closure = null) {


		XMLConfigurator.getInstance(new ConfiguratorSource(GenerateConfigFile.generateFile(server)))

		BinBaseService service = BinBaseServiceFactory.createFactory().createService()

		//fetch teh experiment
		Experiment experiment = service.getExperiment(database, setupXName, Configurator.getKeyManager().getInternalKey())

		GenerateDSLFromExperiment generate = new GenerateDSLFromExperiment()

		return generate.dslGenerateForExperiment (experiment, server, replace, sizeDown, format,closure)
	}

    /**
     * generates the dsl representing the given setupx version
     * @param setupXName
     * @return
     */
    public String dslGenerateForExperimentWithDefaultSOP(String setupXName, String database, String server = "127.0.0.1") {


        XMLConfigurator.getInstance(new ConfiguratorSource(GenerateConfigFile.generateFile(server)))

        BinBaseService service = BinBaseServiceFactory.createFactory().createService()

        //fetch teh experiment
        Experiment experiment = service.getExperiment(database, setupXName, Configurator.getKeyManager().getInternalKey())

        GenerateDSLFromExperiment generate = new GenerateDSLFromExperiment()

        return generate.dslGenerateForExperimentWithDefaultSOP(experiment, server)
    }
	/**
	 * generates the dsl representing the given setupx version
	 * @param setupXName
	 * @return
	 */
	public String dslGenerateForDatabase(String database, String server = "127.0.0.1", boolean replace = true, int sizeDown = 80, String format = "xls",Closure closure = null) {


		XMLConfigurator.getInstance(new ConfiguratorSource(GenerateConfigFile.generateFile(server)))

		BinBaseService service = BinBaseServiceFactory.createFactory().createService()

		//fetch the complete database as experiment
		ExperimentClass[] classes = service.getStoredClasses(database, Configurator.getKeyManager().getInternalKey())

		//create a single experiment of all the classes
		Experiment experiment = new Experiment()

		experiment.setClasses classes
		experiment.setColumn database
		experiment.setId "complete databases experiment"

		//run the generate function
		GenerateDSLFromExperiment generate = new GenerateDSLFromExperiment()

		return generate.dslGenerateForExperiment (experiment, server, replace, sizeDown, format,closure)
	}

	/**
	 * generates a dsl for this database from the given directory
	 * @param database
	 * @param directory
	 * @return
	 */
	public String dslGenerateFromDirectory(String database, File directory,int samplesForClass=6, String server = "127.0.0.1", boolean replace = true, int sizeDown = 80, String format = "xls",def closure = null){
		Experiment experiment = new Experiment()
		experiment.setColumn(database)
		experiment.setId("dsl_" + System.currentTimeMillis())

		List<ExperimentClass> classesList = new Vector<ExperimentClass>()
		List<ExperimentSample> samplesList = new Vector<ExperimentSample>()

		if(!directory.exists()){
			throw new FileNotFoundException("sorry the provided directory ${directory} does not exist")
		}
		File[] files = directory.listFiles();

		for(int i = 0; i < files.length; i++){
			File f = files[i]

			//we only accept txt files
			if(f.getName().endsWith(".txt")){
				ExperimentSample sample = new ExperimentSample()
				sample.name = f.getName().replaceAll(".txt","")
				sample.id = sample.name

				samplesList.add(sample)

				if(samplesForClass == (samplesList.size() )){
					classesList.add(buildClass(experiment, classesList, samplesList))
					samplesList.clear()
				}
			}
		}

		if(samplesList.size() > 0){
			classesList.add(buildClass(experiment, classesList, samplesList))
		}

		ExperimentClass[] classes = new ExperimentClass[classesList.size()]

		for(int i = 0; i < classes.length; i++){
			classes[i] = classesList.get(i)
		}

		experiment.classes = classes

		//run the generate function
		GenerateDSLFromExperiment generate = new GenerateDSLFromExperiment()

		return generate.dslGenerateForExperiment (experiment, server, replace, sizeDown, format,closure)

	}

	private ExperimentClass buildClass(Experiment experiment, List classesList, List samplesList) {
		ExperimentClass clazz = new ExperimentClass()
		clazz.setId(experiment.getId() + "_" + classesList.size());
		ExperimentSample[] samples = new ExperimentSample[samplesList.size()]

		for(int x = 0; x < samplesList.size(); x++){
			samples[x] = samplesList.get(x)
		}

		clazz.samples = samples
		return clazz
	}
	public static void main(String[] args){
		println new GenerateDSLFromDatabase().dslGenerateFromDirectory(args[0], new File(args[2]),Integer.parseInt(args[3]),);
	}
}