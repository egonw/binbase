package edu.ucdavis.genomics.metabolomics.binbase.dsl.io

import edu.ucdavis.genomics.metabolomics.util.io.source.Source

/**
 *
 * provides us with a configuration based on an cxml document. Really only needed to
 * convert a xml builder to a source
 * User: wohlgemuth
 * Date: Sep 16, 2009
 * Time: 11:30:00 AM
 *
 */

public class ConfiguratorSource implements Source {

  String document

  public ConfiguratorSource(String xmlDocument) {
    this.document = xmlDocument

  }

  public InputStream getStream() {
    return new ByteArrayInputStream(this.document.getBytes());
  }

  public String getSourceName() {
    return "config";
  }

  public void setIdentifier(Object o) {

  }

  public void configure(Map<?, ?> map) {

  }

  public boolean exist() {
    return document != null;
  }

  public long getVersion() {
    return document.size();
  }

}