package edu.ucdavis.genomics.metabolomics.binbase.dsl.type

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;

/**
 * just a marker
 * @author wohlgemuth
 *
 */
class CalibrationClass extends ExperimentClass {

	/**
	 * do we only want to calibrate with the class and do not import stuff
	 */
	boolean calibrationOnly = false
	
	public CalibrationClass() {
	}

}
