package edu.ucdavis.genomics.metabolomics.util.status.notify.email;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.status.notify.Notifier;

public class EmailNotifierImplementationTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNotifyStringPriority() throws NotificationException {

		EmailConfiguration configuration = new EmailConfiguration() {
			
			@Override
			public Boolean isSSLRequired() {
				return true;
			}
			
			@Override
			public String getUsername() {
				return "binbasetest@gmail.com";
			}
			
			@Override
			public String getServer() {
				return "smtp.gmail.com";
			}
			
			@Override
			public String getSenderAddress() {
				// TODO Auto-generated method stub
				return "binbasetest@gmail.com";
			}
			
			@Override
			public Integer getPort() {
				// TODO Auto-generated method stub
				return 465;
			}
			
			@Override
			public String getPassword() {
				return "binbase-test";
			}
			
			@Override
			public List<String> getEmailAddress() {
				List<String> list = new ArrayList<String>();
				list.add("binbasetest@gmail.com");
				return list;
			}
		};
		Notifier notifier = new EmailNotifierImplementation(configuration);
		notifier.notify("this is a test");
	}

}
