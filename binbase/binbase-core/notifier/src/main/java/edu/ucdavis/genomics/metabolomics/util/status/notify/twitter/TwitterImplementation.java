package edu.ucdavis.genomics.metabolomics.util.status.notify.twitter;

import net.unto.twitter.Api;
import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * simple twitter implementation
 * @author wohlgemuth
 *
 */
public class TwitterImplementation extends TwitterNotifier {

	private Api api;

	public TwitterImplementation(TwitterConfiguration configuration) {
		super(configuration);
		this.setAsync(true);

		this.api = Api.builder().username(this.getConfiguration().getUserName()).password(this.getConfiguration().getPassword()).build();

	}

	@Override
	protected void doNotify(String message, Priority priority) throws NotificationException {
		api.updateStatus(message).build().post();

	}

}
