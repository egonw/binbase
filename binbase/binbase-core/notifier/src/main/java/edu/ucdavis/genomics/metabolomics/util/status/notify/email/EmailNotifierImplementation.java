package edu.ucdavis.genomics.metabolomics.util.status.notify.email;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * specific gmail based implementation
 * 
 * @author wohlgemuth
 */
public class EmailNotifierImplementation extends EmailNotifier {

	private Logger logger = Logger.getLogger(getClass().getName());

	public EmailNotifierImplementation(EmailConfiguration configuration) {
		super(configuration);
		this.setAsync(true);
	}

	@Override
	protected void doNotify(String message, Priority priority) throws NotificationException {
		try {
			List<Message> msg = createEmail(this.getConfiguration().getSenderAddress(), message, "binbase - notification");

			for (Message m : msg) {

				logger.info("sending message: " + m.getSubject());
				Transport.send(m);
			}
		}
		catch (MessagingException e) {
			throw new NotificationException(e);
		}
	}

	/**
	 * creates the actual email
	 * 
	 * @param toEmailAddress
	 * @param content
	 * @param subject
	 * @return
	 * @throws NotificationException
	 */
	public List<Message> createEmail(String toEmailAddress, String content, String subject) throws NotificationException {

		try {

			Properties props = new Properties();

			props.put("mail.smtp.host", this.getConfiguration().getServer());
			props.put("mail.smtp.auth", "true");

			props.put("mail.smtp.port", this.getConfiguration().getPort().toString());
			props.put("mail.smtp.socketFactory.port", this.getConfiguration().getPort().toString());

			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

			props.put("mail.smtp.socketFactory.fallback", "false");

			props.list(System.out);
			logger.info("trying to authentificate...");

			Authenticator auth = new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					try {
						return new PasswordAuthentication(getConfiguration().getUsername(), getConfiguration().getPassword());
					}
					catch (Exception e) {
						throw new RuntimeException("authentifiacation faild: " + e.getMessage(), e);
					}
				}
			};

			Session session = Session.getDefaultInstance(props, auth);

			List<Message> result = new ArrayList<Message>(this.getConfiguration().getEmailAddress().size());

			logger.info("generating messages");

			for (String to : this.getConfiguration().getEmailAddress()) {
				Message msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(to));
				InternetAddress a[] = new InternetAddress[] { new InternetAddress(toEmailAddress) };

				msg.setRecipients(Message.RecipientType.TO, a);

				msg.setSubject(subject);

				BodyPart body = new MimeBodyPart();

				body.setText(content);
				Multipart multi = new MimeMultipart();
				multi.addBodyPart(body);
				msg.setContent(multi);
				msg.setSentDate(new Date());
				msg.saveChanges();

				result.add(msg);
			}
			return result;
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new NotificationException(e.getMessage(), e);
		}
	}
}
