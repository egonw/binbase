package edu.ucdavis.genomics.metabolomics.binbase.bdi.util;

import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.persister.entity.EntityPersister;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.test.BinBaseDatabaseTest;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

public class HibernateTest extends BinBaseDatabaseTest {
	private Logger logger = Logger.getLogger(getClass());

	private SessionFactory factory;
	private Properties p;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		logger.info("creating hibernate connection");
		// needed properties
		p = XMLConfigurator.getInstance(new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());

		HibernateFactory fact = HibernateFactory.newInstance(p);
		factory = fact.getInternalFactory();
	}

	@After
	public void tearDown() throws Exception {
		HibernateFactory.newInstance().destroyFactory();
		logger.info("calling super to teardown...");
		super.tearDown();
	}

	@Override
	protected IDataSet getDataSet() {
		try {
			logger.info("looking for datafile...");
			ResourceSource source = new ResourceSource("/minimum-rtx5.xml");
			logger.debug("using source: " + source.getSourceName() + " exist: " + source.exist());
			assertTrue(source.exist());
			return new XmlDataSet(source.getStream());
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testEverything() throws Exception {
		SessionFactory sessionFactory = factory;
		Map metadata = sessionFactory.getAllClassMetadata();
	
		for (Iterator i = metadata.values().iterator(); i.hasNext();) {
			Session session = sessionFactory.openSession();
			EntityPersister persister = (EntityPersister) i.next();
			String className = persister.getClassMetadata().getEntityName();
			try {
				List result = session.createQuery("from " + className + " c").list();
				assertTrue(true);
			}
			catch (Exception e) {
				logger.error(e.getMessage(),e);
				fail("mapping error in: " + className + " - error was: " + e.getMessage());
			}
			finally {
				session.close();
			}
		}
	}
}
