package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Refrence;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.RefrenceClass;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.test.BinBaseDatabaseTest;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

/**
 * tessts the generation of our reference table
 * 
 * @author nase
 * 
 */
public class BinReferenceTableTest extends BinBaseDatabaseTest {

	private Logger logger = Logger.getLogger(getClass());
	private BinReferenceTable table;

	@Before
	public void setUp() throws Exception {
		super.setUp();

		setupReferences();

		 table = new BinReferenceTable();

	}

	protected void setupReferences() {
		// create some references for this test
		Properties p = XMLConfigurator.getInstance(
				new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());

		Session session = HibernateFactory.newInstance(p).getSession();

		// make sure there are no references defined
		assertTrue(session.createCriteria(RefrenceClass.class).list().size() == 0);

		RefrenceClass clazz = new RefrenceClass();
		clazz.setDescription("just a test");
		clazz.setName("A");
		clazz.setUrlPattern("no pattern");

		// needs to be done in a transaction cause we are good database monkeys
		Transaction t = session.beginTransaction();
		session.save(clazz);
		t.commit();

		// make sure we got a refrence class now
		assertTrue(session.createCriteria(RefrenceClass.class).list().size() == 1);

		// create second class

		RefrenceClass clazz2 = new RefrenceClass();
		clazz2.setDescription("just a test");
		clazz2.setName("B");
		clazz2.setUrlPattern("no pattern");

		// needs to be done in a transaction cause we are good database monkeys
		t = session.beginTransaction();
		session.save(clazz2);
		t.commit();

		// make sure we got a refrence class now
		assertTrue(session.createCriteria(RefrenceClass.class).list().size() == 2);

		// assign refrences to bin, has two references in two diffrent classes
		{
			Bin bin = (Bin) session.get(Bin.class, new Integer(14328));
			t.begin();
			Refrence ref = new Refrence();
			ref.setBin(bin);
			ref.setRefrenceClass(clazz);
			ref.setValue("A");
			bin.addRefrence(ref);
			session.save(ref);
			session.saveOrUpdate(bin);
			t.commit();
		}

		{
			Bin bin = (Bin) session.get(Bin.class, new Integer(14328));
			t.begin();
			Refrence ref = new Refrence();
			ref.setBin(bin);
			ref.setRefrenceClass(clazz2);
			ref.setValue("B");
			bin.addRefrence(ref);
			session.save(ref);
			session.saveOrUpdate(bin);
			t.commit();
		}

		// has one refrences in one class
		{
			Bin bin = (Bin) session.get(Bin.class, new Integer(14330));
			t.begin();
			Refrence ref = new Refrence();
			ref.setBin(bin);
			ref.setRefrenceClass(clazz);
			ref.setValue("A");
			bin.addRefrence(ref);
			session.save(ref);
			session.saveOrUpdate(bin);
			t.commit();
		}

		// has one reference in one class
		{
			Bin bin = (Bin) session.get(Bin.class, new Integer(14338));
			t.begin();
			Refrence ref = new Refrence();
			ref.setBin(bin);
			ref.setRefrenceClass(clazz2);
			ref.setValue("B");
			bin.addRefrence(ref);
			session.save(ref);
			session.saveOrUpdate(bin);
			t.commit();
		}

		// has two refrences in one class
		{
			Bin bin = (Bin) session.get(Bin.class, new Integer(14344));
			t.begin();
			Refrence ref = new Refrence();
			ref.setBin(bin);
			ref.setRefrenceClass(clazz);
			ref.setValue("A");
			bin.addRefrence(ref);
			session.save(ref);
			session.saveOrUpdate(bin);
			t.commit();
		}

		// has one reference in one class
		{
			Bin bin = (Bin) session.get(Bin.class, new Integer(14344));
			t.begin();
			Refrence ref = new Refrence();
			ref.setBin(bin);
			ref.setRefrenceClass(clazz);
			ref.setValue("A2");
			bin.addRefrence(ref);
			session.save(ref);
			session.saveOrUpdate(bin);
			t.commit();
		}

		t = null;
		session.close();
	}

	@After
	public void tearDown() throws Exception {
		table = null;
		HibernateFactory.newInstance().destroyFactory();

		super.tearDown();
	}

	@Test
	public void testCreateTable() throws IOException, ConfigurationException {
		DataFile file = table.createTable();

		assertTrue(file.getRowCount() == 14);
		assertTrue(file.getColumnCount() == 3);

		// write output file

		// save the generated file for user validation
		FileDestination dest = new FileDestination("target/content/result");
		dest.setIdentifier((getClass().getSimpleName() + "-result.txt"));

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();

		// analyze our result

		// 1 bin should have two references
		logger.info(file.getCell(1, 1));
		logger.info(file.getCell(1, 2));
		
		assertTrue(file.getCell(1, 1).equals("A"));
		assertTrue(file.getCell(2, 1).equals("B"));

		// 2 bin should have reference A
		assertTrue(file.getCell(1, 2).equals("A"));
		assertTrue(file.getCell(2, 2).equals(""));

		// 3 bin should have reference B
		assertTrue(file.getCell(1, 3).equals(""));
		assertTrue(file.getCell(2, 3).equals("B"));

		// 4 bin should have A,A1
		assertTrue(file.getCell(1, 4).equals("A,A2") || file.getCell(1, 4).equals("A2,A"));
		assertTrue(file.getCell(2, 4).equals(""));

		// 5 bin should have nothing
		assertTrue(file.getCell(1, 5).equals(""));
		assertTrue(file.getCell(2, 5).equals(""));
		
		// rest is uninterresting
	}

	@Override
	protected IDataSet getDataSet() {
		try {
			logger.info("looking for datafile...");
			ResourceSource source = new ResourceSource("/library-rtx5.xml");
			logger.debug("using source: " + source.getSourceName() + " exist: "
					+ source.exist());
			assertTrue(source.exist());
			return new XmlDataSet(source.getStream());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
