package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.msp;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.Library;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.sjp.exception.ParserException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.test.BinBaseDatabaseTest;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

/**
 * tests if the import of the data works as its supposed to be
 * 
 * @author wohlgemuth
 * 
 */
public class ImportMSPFilesTest extends BinBaseDatabaseTest {
	private Logger logger = Logger.getLogger(getClass());
	private Session session;
	private Properties p;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		logger.info("creating hibernate connection");
		// needed properties
		 p = XMLConfigurator.getInstance(new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());

		 session = HibernateFactory.newInstance(p).getSession();

	}

	@After
	public void tearDown() throws Exception {
		HibernateFactory.newInstance().destroyFactory();
		logger.info("calling super to teardown...");
		super.tearDown();
	}

	@Test
	public void testImportMspFileProperties() throws FileNotFoundException, ParserException, InstantiationException, IllegalAccessException, IOException {

		// create session for validation
	
		// make sure we got no libs
		assertTrue(session.createCriteria(Library.class).list().isEmpty());
		ImportMSPFiles msp = new ImportMSPFiles();
		msp.importMsp(new File("src/test/resources/example.msp"), p);

		// make sure there is one lib now
		assertTrue(session.createCriteria(Library.class).list().size() == 1);

		// this lib should have 20 spectra
		Library lib = (Library) session.createCriteria(Library.class).list().get(0);

		assertTrue(lib.getName().equals("example"));
		assertTrue(lib.getSpectra().size() == 11);

	}

	@Override
	protected IDataSet getDataSet() {
		try {
			logger.info("looking for datafile...");
			ResourceSource source = new ResourceSource("/minimum-rtx5.xml");
			logger.debug("using source: " + source.getSourceName() + " exist: " + source.exist());
			assertTrue(source.exist());
			return new XmlDataSet(source.getStream());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
