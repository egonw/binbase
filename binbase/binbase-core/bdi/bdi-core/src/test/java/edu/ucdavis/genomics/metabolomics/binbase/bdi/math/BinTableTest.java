package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.test.BinBaseDatabaseTest;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

/**
 * tests if the generated table looks as exspected
 * 
 * @author nase
 * 
 */
public class BinTableTest extends BinBaseDatabaseTest {

	private Logger logger = Logger.getLogger(getClass());
	private BinTable table;

	@Before
	public void setUp() throws Exception {
		super.setUp();

		 table = new BinTable();
		
	}

	@After
	public void tearDown() throws Exception {
		table = null;
		HibernateFactory.newInstance().destroyFactory();
		super.tearDown();
		
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		try {
			logger.info("looking for datafile...");
			ResourceSource source = new ResourceSource("/library-rtx5.xml");
			logger.debug("using source: " + source.getSourceName() + " exist: "
					+ source.exist());
			assertTrue(source.exist());
			return new XmlDataSet(source.getStream());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testCreateTable() throws ConfigurationException, IOException {
		Properties p = XMLConfigurator.getInstance(new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());

		DataFile file = table.createTable();

		assertTrue(file.getRowCount() == 13 + 1);

		// save the generated file for user validation
		FileDestination dest = new FileDestination("target/content/result");
		dest.setIdentifier((getClass().getSimpleName() + "-result.txt"));

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();

	}
}
