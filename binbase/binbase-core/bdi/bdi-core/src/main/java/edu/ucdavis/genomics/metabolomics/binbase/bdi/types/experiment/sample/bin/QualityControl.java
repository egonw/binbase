/*
 * Created on Nov 14, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Queryable;

/**
 * defines a quality controll
 * 
 * @author wohlgemuth
 * @version Nov 14, 2005
 * @swt
 * @hibernate.class table = "QUALITYCONTROL"
 */
public class QualityControl implements Queryable{

    /**
     * ammount in mg of this qualitcontrol standard
     */
    private Double amount;
    private Integer id;
	private Bin bin;

    public void setId(Integer id) {
        this.id = id;
    }

    public QualityControl() {
        super();
    }

    /**
     * @swt.variable visible="true" name="Amount" searchable="true"
     * @swt.modify canModify="true"
     * 
     * returns the amount in mg of this standard
     * @hibernate.property column = "amount"
     * @author wohlgemuth    
     * @version Nov 14, 2005
     * @return
     */
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @hibernate.id column = "id" generator-class = "assigned"
     * @return
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * @hibernate.one-to-one cascade = "none"
     * @author wohlgemuth
     * @version May 24, 2006
     * @return
     */
    public Bin getBin(){
    	return bin;
    }
    
	public void setBin(Bin bin){
		this.bin = bin;
}


}
