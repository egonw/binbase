/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.exception;

/**
 * @author wohlgemuth
 * is thrown if something goes wrong with the renderes
 */
public class RenderException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * 
	 */
	public RenderException() {
		super();
		
	}

	/**
	 * @param message
	 */
	public RenderException(String message) {
		super(message);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public RenderException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param cause
	 */
	public RenderException(Throwable cause) {
		super(cause);
		
	}

}
