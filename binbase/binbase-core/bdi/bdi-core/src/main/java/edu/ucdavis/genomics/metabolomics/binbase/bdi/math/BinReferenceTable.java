package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import java.util.Collection;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Refrence;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.RefrenceClass;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;

/**
 * creates a datafile with the references to each bin
 * 
 * @author nase
 * 
 */
public class BinReferenceTable extends AbstractBinTable {

	/**
	 * generates a simple datafile containing all the data
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public DataFile createTable() {
		// get objects of interesst
		Collection<Bin> bins = getBins();
		Collection<RefrenceClass> classes = getRefrenceClasses();

		// define the file
		DataFile result = new SimpleDatafile();
		result.setDimension(bins.size() + 1, classes.size() + 1);
		result.setCell(0, 0, "");

		// set all value to ""
		for (int i = 0; i < result.getColumnCount(); i++) {
			for (int x = 0; x < result.getRowCount(); x++) {
				result.setCell(i, x, "");
			}
		}

		// create the header
		int column = 1;
		for (RefrenceClass clazz : classes) {
			result.setCell(column, 0, clazz.getName());
			column++;
		}

		// fill the file
		int row = 1;
		for (Bin bin : bins) {
			column = 0;
			result.setCell(column, row, bin.getId());
			column = 1;

			// assign the refrences
			Collection<Refrence> refrences = bin.getRefrences();
			for (RefrenceClass clazz : classes) {
				for (Refrence ref : refrences) {
					if (ref.getRefrenceClass().getId().equals(clazz.getId())) {
						String value = ref.getValue();

						if (result.getCell(column, row).toString().length() == 0) {
							result.setCell(column, row, value);
						} else {
							result.setCell(column, row, result.getCell(column,
									row)
									+ "," + value);
						}
					}
				}
				column++;
			}
			row++;
		}

		// return the result
		return result;
	}

	/**
	 * gives us all refrence classes
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<RefrenceClass> getRefrenceClasses() {
		return getSession().createCriteria(RefrenceClass.class).list();
	}
}
