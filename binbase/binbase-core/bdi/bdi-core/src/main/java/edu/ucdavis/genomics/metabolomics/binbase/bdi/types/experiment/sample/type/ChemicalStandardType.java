package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.type;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Type;

/**
 * @hibernate.subclass discriminator-value = "Chemical Standard"
 * @author wohlgemuth
 *
 */
public class ChemicalStandardType extends Type{

}
