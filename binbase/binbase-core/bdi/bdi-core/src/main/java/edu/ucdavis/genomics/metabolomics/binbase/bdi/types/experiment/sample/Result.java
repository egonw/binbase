/*
 * Created on Aug 16, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample;

import java.util.Collection;
import java.util.HashSet;


/**
 * @swt
 * @author wohlgemuth
 * @hibernate.class table = "RESULT" dynamic-insert = "true" dynamic-update =
 *                  "true"
 */
public class Result{
    /**
     * DOCUMENT ME!
     */
    private Collection links;

    /**
     * DOCUMENT ME!
     */
    private String description;

    private String setupXId;
    /**
     * DOCUMENT ME!
     */
    private Integer id;

    /**
     *
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Result#setDescription(java.lang.String)
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @swt.variable visible="true" name="Description" searchable="true"
	 * @swt.modify canModify="false"
     * @hibernate.property column = "`description`" insert = true" update =
     *                     "true"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Result#getDescription()
     */	
    public String getDescription() {
        return description;
    }

    /**
     * DOCUMENT ME!
     *
     * @param id DOCUMENT ME!
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @swt.variable visible="false" name="Id" searchable="true"
	 * @swt.modify canModify="false"
     * @hibernate.id column = "`result_id`" generator-class = "native"
     * @hibernate.generator-param name = "sequence" value = "RESULT_ID"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Result#getId()
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Result#setLinks(java.util.Collection)
     */
    public void setLinks(Collection links) {
        this.links = links;
    }

    /**
     *
     * @hibernate.set lazy="true" cascade = "none" inverse = "true"
     * @hibernate.collection-one-to-many class =
     *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.ResultLink"
     * @hibernate.collection-key column = "`result_id`"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Result#getLinks()
     */
    public Collection getLinks() {
        return links;
    }

    public void addLink(ResultLink link){
    	if(links == null){
    		links = new HashSet();
    	}
    	link.setResult(this);
    	getLinks().add(link);
    }

    /**
     * @swt.variable visible="true" name="Setup X Id" searchable="true"
	 * @swt.modify canModify="false"
     * @hibernate.property column = "setupx" insert = true" update =
     *                     "true"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Result#getDescription()
     */
	public String getSetupXId() {
		return setupXId;
	}

	public void setSetupXId(String setUpXID) {
		this.setupXId = setUpXID;
	}
}
