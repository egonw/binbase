/*
 * Created on 01.05.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type;

import java.io.Serializable;


/**
 * @swt.multi = false
 * @author wohlgemuth definiert ein ion
 */
public class Ion implements Serializable, Comparable {
    /**
     * Comment for <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 2L;

    /**
     * die absolute intensit?t
     */
    private double absoluteIntensity;

    /**
     * die relative intensit?t
     */
    private double relativeIntensity;

    /**
     * die masse
     */
    private int mass;

    /**
     * @param absoluteIntensity
     * @param relativeIntensity
     * @param mass
     */
    public Ion(double absoluteIntensity, double relativeIntensity, int mass) {
        super();
        this.absoluteIntensity = absoluteIntensity;
        this.relativeIntensity = relativeIntensity;
        this.mass = mass;
    }

    /**
     *
     */
    public Ion() {
    }

    /**
     * 
     * @swt.variable visible="true" name="Absolute Intensity" searchable="false"
	 * @swt.modify canModify="false"

     * @return Returns the absoluteIntensity.
     *
     * @uml.property name="absoluteIntensity"
     */
    public final double getAbsoluteIntensity() {
        return absoluteIntensity;
    }

    /**
     * @return Returns the mass.
     *
     *     * @swt.variable visible="true" name="Mass" searchable="false"
	 * @swt.modify canModify="false"
     * @uml.property name="mass"
     */
    public final int getMass() {
        return mass;
    }

    /**
     * 
     *      * @swt.variable visible="true" name="Relative Intensity" searchable="false"
	 * @swt.modify canModify="false"
     * @return Returns the relativeIntensity.
     *
     * @uml.property name="relativeIntensity"
     */
    public final double getRelativeIntensity() {
        return relativeIntensity;
    }

    /**
     * @param absoluteIntensity
     *            The absoluteIntensity to set.
     *
     * @uml.property name="absoluteIntensity"
     */
    public void setAbsoluteIntensity(double absoluteIntensity) {
        this.absoluteIntensity = absoluteIntensity;
    }

    /**
     * @param mass
     *            The mass to set.
     *
     * @uml.property name="mass"
     */
    public void setMass(int mass) {
        this.mass = mass;
    }

    /**
     * @param relativeIntensity
     *            The relativeIntensity to set.
     *
     * @uml.property name="relativeIntensity"
     */
    public void setRelativeIntensity(double relativeIntensity) {
        this.relativeIntensity = relativeIntensity;
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Object o) {
        if (o instanceof Ion) {
            Ion i = (Ion) o;

            if (i.getAbsoluteIntensity() > getAbsoluteIntensity()) {
                return 1;
            } else if (i.getAbsoluteIntensity() < getAbsoluteIntensity()) {
                return -1;
            } else {
                return 0;
            }
        } else {
            return -1;
        }
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        if (obj instanceof Ion) {
            Ion b = (Ion) obj;

            if (b.getMass() == this.getMass()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return this.getMass() + ":" + this.getAbsoluteIntensity() + ":" +
        this.getRelativeIntensity();
    }
}
