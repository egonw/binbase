/*
 * Created on 26.06.2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import java.util.Collection;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Queryable;
import edu.ucdavis.genomics.metabolomics.util.type.converter.BooleanConverter;

/**
 * @swt
 * @hibernate.class table = "BIN_RATIO" dynamic-insert = "true" dynamic-update =
 *                  "true"
 * @author wohlgemuth
 * 
 */
public class IonRatio implements Queryable {
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * DOCUMENT ME!
	 */
	private Integer id;

	private Integer firstIon;

	private Integer secondIon;

	private Double minRatio;

	private Double maxRatio;

	private Bin bin;

	/**
	 * 	 * @hibernate.id column = "`id`" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getId()
	 * 
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @swt.variable visible="false" name="Bin" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one column = "`bin_id`" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin"
	 *                        update = "true" insert = "true" 

	 * @version May 24, 2006
	 * @return
	 */
	public Bin getBin() {
		return bin;
	}

	public void setBin(Bin bin) {
		this.bin = bin;
	}

	/**
	 * @swt.variable visible="true" name="Main Ion" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`main_ion`" update = "true"
	 *                     insert = "true" not-null = "false"
	 * @return
	 */
	public Integer getFirstIon() {
		return firstIon;
	}

	public void setFirstIon(Integer firstIon) {
		this.firstIon = firstIon;
	}

	/**
	 * @swt.variable visible="true" name="Secondaery Ion" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`secondaery_ion`" update = "true"
	 *                     insert = "true" not-null = "false"
	 * @return
	 */
	public Integer getSecondIon() {
		return secondIon;
	}

	public void setSecondIon(Integer secondIon) {
		this.secondIon = secondIon;
	}

	/**
	 * @swt.variable visible="true" name="Min Ratio" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`min_ratio`" update = "true"
	 *                     insert = "true" not-null = "false"
	 * @return
	 */
	public Double getMinRatio() {
		return minRatio;
	}

	public void setMinRatio(Double ratio) {
		this.minRatio = ratio;
	}

	/**
	 * @swt.variable visible="true" name="Max Ratio" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`max_ratio`" update = "true"
	 *                     insert = "true" not-null = "false"
	 * @return
	 */
	public Double getMaxRatio() {
		return maxRatio;
	}

	public void setMaxRatio(Double ratio) {
		this.maxRatio = ratio;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
