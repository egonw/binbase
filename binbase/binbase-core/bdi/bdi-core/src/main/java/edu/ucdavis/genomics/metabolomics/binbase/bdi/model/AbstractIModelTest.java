/*
 * Created on Dec 9, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.model;

import junit.framework.TestCase;

/**
 * abstract test to check models
 * @author wohlgemuth
 * @version Dec 9, 2005
 *
 */
public abstract class AbstractIModelTest extends TestCase {
	/**
	 * our model to test
	 */
	private IModel model;

	protected void setUp() throws Exception {
		super.setUp();
		this.model = obtainModel();
		this.addCalled = false;
		this.deleteCalled = false;
		this.updateCalled = false;
		this.queryCalled = false;
		this.model.addChangeListener(new TestChangeListener());
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*
	 * Test method for
	 * 'edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel.executeQuery()'
	 */
	public abstract void testExecuteQuery();

	/*
	 * Test method for
	 * 'edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel.add(Type)'
	 */
	public abstract void testAdd();

	/*
	 * Test method for
	 * 'edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel.delete(Type)'
	 */
	public abstract void testDelete();

	/*
	 * Test method for
	 * 'edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel.update(Type)'
	 */
	public abstract void testUpdate();

	/**
	 * creates the model
	 * 
	 * @author wohlgemuth
	 * @version Dec 9, 2005
	 * @return
	 */
	protected abstract IModel obtainModel();

	/**
	 * used to test if the listener is implementad as we exspected
	 * @author wohlgemuth
	 * @version Dec 9, 2005
	 *
	 */
	private class TestChangeListener implements ChangeListener {
		public <Type> void eventAdd(Type value) {
			addCalled = true;
		}

		public <Type> void eventDelete(Type value) {
			deleteCalled = true;
		}

		public <Type> void eventUpdate(Type value) {
			updateCalled = true;
		}

		public <Type> void eventQuery(IModel model) {
			queryCalled = true;
		}
	}

	private boolean addCalled;

	private boolean queryCalled;

	private boolean deleteCalled;

	private boolean updateCalled;

	public boolean isAddCalled() {
		return addCalled;
	}

	public boolean isDeleteCalled() {
		return deleteCalled;
	}

	public boolean isQueryCalled() {
		return queryCalled;
	}

	public boolean isUpdateCalled() {
		return updateCalled;
	}

}
