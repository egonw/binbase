/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types;

/**
 * @swt
 * @author wohlgemuth defines a basic comment
 * @hibernate.class table = "COMMENTS" dynamic-insert = "true" dynamic-update =
 *                  "true" discriminator-value = "0"
 * @hibernate.discriminator column = "DISCRIMINATOR"
 */
public class Comment implements Comparable{

	private String text;

	private Integer id;

	/**
	 * refrence to the belonging type
	 */
	private Commentable type;
	
	/**
	 * @hibernate.id column = "id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "TYPE_ID"
	 * 
	 * the internal id
	 * @return
	 */
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @swt.variable visible="true" name="Text" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "text" update = "true" insert = "true" not-null = "true"
	 * @return
	 */
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @swt.variable visible="false" name="Text" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public Commentable getType() {
		return this.type;
	}

	public void setType(Commentable type) {
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object arg0) {
		if(arg0 instanceof Comment){
			Comment a = (Comment) arg0;
			return a.getId().compareTo(this.getId());
		}
		return this.toString().compareTo(arg0.toString());
	}

}
