/*
 * Created on 09.06.2004
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter;

import java.io.InputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.exception.SpectraConversionException;
import edu.ucdavis.genomics.metabolomics.util.serialize.ClassConverter;


/**
 * @author wohlgemuth
 */
public class SpectraConverter {
    /**
     * DOCUMENT ME!
     *
     * @param c DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static String collectionToApexString(List<Ion> c) {
        Iterator<Ion> it = c.iterator();
        StringBuffer buffer = new StringBuffer();

        while (it.hasNext()) {
            Ion i = (Ion) it.next();
            buffer.append(i.getMass());

            if (it.hasNext()) {
                buffer.append("+");
            }
        }

        return buffer.toString();
    }

    /**
     * DOCUMENT ME!
     *
     * @param c DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static String collectionToString(Collection<Ion> c) {
        Iterator<Ion> it = c.iterator();
        StringBuffer buffer = new StringBuffer();

        while (it.hasNext()) {
            Ion i = (Ion) it.next();
            buffer.append(i.getMass() + ":" + i.getAbsoluteIntensity());

            if (it.hasNext()) {
                buffer.append(" ");
            }
        }

        return buffer.toString();
    }

    /**
     * DOCUMENT ME!
     *
     * @param stream DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws SpectraConversionException DOCUMENT ME!
     */
    public static List<Ion> streamToCollection(InputStream stream)
        throws SpectraConversionException {
        try {
            return stringToCollection(ClassConverter.convertStreamToString(
                    stream));
        } catch (Exception e) {
            throw new SpectraConversionException(e);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param s DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static java.util.List<Ion> stringToCollection(String s) {
        StringTokenizer tokenizer = new StringTokenizer(s, " ");
        double maxAbundance = 0;
        Vector <Ion>c = new Vector<Ion>();

        //f???llt die hashmap mit den gew???nschten daten
        try {
            while (tokenizer.hasMoreTokens()) {
                StringTokenizer token = new StringTokenizer(tokenizer.nextToken(),
                        ":");
                Ion ion = new Ion();
                ion.setMass(Integer.parseInt(token.nextToken()));
                ion.setAbsoluteIntensity(Double.parseDouble(token.nextToken()));

                double abs = ion.getAbsoluteIntensity();

                if (abs >= maxAbundance) {
                    maxAbundance = abs;
                }

                c.add(ion);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new SpectraConversionException(e);
        }

        for (int i = 0; i < c.size(); i++) {
            Ion x = c.get(i);
            x.setRelativeIntensity(x.getAbsoluteIntensity() / maxAbundance * 100);
        }

        return c;
    }
}
