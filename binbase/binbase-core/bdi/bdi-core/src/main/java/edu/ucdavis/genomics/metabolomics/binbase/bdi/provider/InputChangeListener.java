/*
 * Created on Jan 12, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.provider;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;

/**
 * is fired when the input is changed
 * @author wohlgemuth
 * @version Jan 12, 2006
 *
 */
public interface InputChangeListener {
	
	/**
	 * 
	 * @author wohlgemuth
	 * @version Jan 12, 2006
	 * @param newInput the new input which is displayed in the associated viewer
	 * @param model the used model
	 */
	public void inputChanged(Object[] newInput,IModel model);
}
