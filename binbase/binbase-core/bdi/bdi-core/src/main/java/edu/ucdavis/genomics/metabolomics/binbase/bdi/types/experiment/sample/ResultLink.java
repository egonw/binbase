/*
 * Created on Aug 16, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Result;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample;

/**
 * 
 * @author wohlgemuth
 * 
 * @hibernate.class table = "RESULT_LINK" dynamic-insert = "true"
 *                  dynamic-update = "true"
 */
public class ResultLink {
	/**
	 * DOCUMENT ME!
	 */
	private Result result;

	/**
	 * DOCUMENT ME!
	 */
	private Sample sampleResult;

	/**
	 * DOCUMENT ME!
	 */
	private int id;

	/**
	 * DOCUMENT ME!
	 * 
	 * @param id
	 *            DOCUMENT ME!
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @hibernate.id column = "`id`" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "LINK_ID"
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @param result
	 *            The result to set.
	 */
	public void setResult(Result result) {
		this.result = result;
	}

	/**
	 * @hibernate.many-to-one column = "`result_id`" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Result"
	 * 
	 * @return Returns the result.
	 */
	public Result getResult() {
		return result;
	}

	/**
	 * @param sampleResult
	 *            The sampleResult to set.
	 */
	public void setSample(Sample sampleResult) {
		this.sampleResult = sampleResult;
	}

	/**
	 * @hibernate.many-to-one column = "`sample_id`" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample"
	 * 
	 * @return Returns the sampleResult.
	 */
	public Sample getSample() {
		return sampleResult;
	}
}
