package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.type;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Type;

/**
 * @hibernate.subclass discriminator-value = "Sample"
 * @author wohlgemuth
 *
 */
public class SampleType extends Type{

}
