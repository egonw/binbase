/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Commentable;

/**
 * @author wohlgemuth
 * @hibernate.subclass discriminator-value = "4"
 */
public class LibraryMassSpecComment extends Comment {

	/**
	 * 
	 */
	public LibraryMassSpecComment() {
		super();
	}


	/**
	 * @hibernate.many-to-one  column = "type" class = "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.LibraryMassSpec" update = "true" insert = "true" not-null = "true"
	 */
	public Commentable getType() {
		return super.getType();
	}
}
