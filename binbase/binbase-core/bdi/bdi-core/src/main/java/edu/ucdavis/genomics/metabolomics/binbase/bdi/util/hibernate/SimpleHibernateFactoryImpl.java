/*
 * Created on Dec 14, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate;

import java.io.File;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.w3c.dom.Document;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;

/**
 * provides as with a really simple factory implementation
 * 
 * @author wohlgemuth
 * @version Dec 14, 2005
 */
public class SimpleHibernateFactoryImpl extends HibernateFactory {

	private Session session;

	private SessionFactory sessionFactory;

	private ConnectionFactory connectionFactory = null;

	private Logger logger = Logger.getLogger(getClass());
	
	public SimpleHibernateFactoryImpl() {
		super();
	}

	@Override
	public synchronized Session getSession() {
		if (session == null) {
			HibernateFactory.logger.info("create new session");
			session = sessionFactory.openSession(connectionFactory
					.getConnection());
		}
		if (session.isOpen() == false) {
			destroySession();
			session = sessionFactory.openSession(connectionFactory
					.getConnection());
		}
		return session;
	}

	@Override
	protected void initialize(Properties p) {
		Configuration c = new Configuration();

		Document d = null;
		try {
			Source source = getSource();

			d = DocumentBuilderFactory.newInstance().newDocumentBuilder()
					.parse(source.getStream());

		} catch (Exception e) {
			throw new HibernateException(e);
		}
		c.setProperties(p);
		c.configure(d);

		connectionFactory = new SimpleConnectionFactory();
		connectionFactory.setProperties(p);

		sessionFactory = c.buildSessionFactory();
	}

	private Source getSource() throws ConfigurationException {
		if (new File("config/hibernate.cfg.xml").exists()) {
			return new FileSource(new File("config/hibernate.cfg.xml"));
		}
		if (new File("/config/hibernate.cfg.xml").exists()) {
			return new FileSource(new File("/config/hibernate.cfg.xml"));
		}
		if (new File(".config/hibernate.cfg.xml").exists()) {
			return new FileSource(new File(".config/hibernate.cfg.xml"));
		}
		if (new File("/.config/hibernate.cfg.xml").exists()) {
			return new FileSource(new File("/.config/hibernate.cfg.xml"));
		}

		ResourceSource source = new ResourceSource("/config/hibernate.cfg.xml");
		return source;
	}

	@Override
	public void destroySession() {
		logger.info("destroying session...");
		if (session != null) {
			if (session.isOpen()) {
				session.close();
			}
		}
		session = null;
	}

	@Override
	public void destroyFactory() {
		destroySession();
		super.destroyFactory();
	}

	@Override
	public SessionFactory getInternalFactory() {
		getSession();
		return sessionFactory;
	}

}
