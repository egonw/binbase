/*
 * Created on 05.08.2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample;

import java.io.InputStream;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;


/**
 *
 * @hibernate.class table = "QUANTIFICATION" dynamic-insert = "false"
 *                  dynamic-update = "true"
 * @author wohlgemuth
 *
 */
public class SampleResult{
    /**
     * DOCUMENT ME!
     */
    private Blob data;

    /**
     * DOCUMENT ME!
     */
    private Sample sample;

    /**
     * DOCUMENT ME!
     */
    private int id;

    /**
     * DOCUMENT ME!
     */
    private int version;

    /**
     * Creates a new SampleResult object.
     */
    public SampleResult() {
        super();
    }

    /**
     * @param data
     *            The data to set.
     */
    public void setData(Blob data) {
        this.data = data;
    }

    /**
     * @hibernate.property column = "`result`" update = "false" insert = "false"
     *                     type = "java.sql.Blob"
     * @return Returns the data.
     */
    public Blob getData() {
        return data;
    }

    /**
     * DOCUMENT ME!
     *
     * @param id DOCUMENT ME!
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @hibernate.id column = "`sample_id`" type = "int" generator-class =
     *               "assigned"
     */
    public int getId() {
        return id;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public InputStream getResult() {
        try {
            return getData().getBinaryStream();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param sample DOCUMENT ME!
     */
    public void setSample(Sample sample) {
        this.sample = sample;
    }

    /**
     * @hibernate.one-to-one class =
     *                       "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample"
     *                       constrained = "true"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getResult()
     */
    public Sample getSample() {
        return sample;
    }

    /**
     * DOCUMENT ME!
     *
     * @param version DOCUMENT ME!
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * @hibernate.property column = "`version`" insert = true" update = "true"
     *                     not-null = "false"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.SampleResult#getVersion()
     */
    public int getVersion() {
        return version;
    }
}
