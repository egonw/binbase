/*
 * Created on 01.05.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import java.io.Serializable;



/**
 * @swt
 * @hibernate.class table  = "VIRTUAL_BIN" dynamic-insert = "true" dynamic-update = "true"
 * @author wohlgemuth
 * defines a virutal bin, containing all the propteries like a real bin, except it cannot create virtual bins
 */
public class VirtualBin implements Serializable{
    /**
     * Comment for <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 2L;

    /**
     * DOCUMENT ME!
     *
     * @uml.property name="bin"
     * @uml.associationEnd multiplicity="(0 1)"
     */
    private Bin bin;

    /**
     * DOCUMENT ME!
     *
     * @uml.property name="id" multiplicity="(0 1)"
     */
    private Integer id;

    /**
     * DOCUMENT ME!
     *
     * @uml.property name="ionA" multiplicity="(0 1)"
     */
    private Integer ionA;

    /**
     * DOCUMENT ME!
     *
     * @uml.property name="ionB" multiplicity="(0 1)"
     */
    private Integer ionB;

    /**
     * DOCUMENT ME!
     *
     * @uml.property name="name" multiplicity="(0 1)"
     */
    private String name;

    /**
     * DOCUMENT ME!
     *
     * @uml.property name="ratio" multiplicity="(0 1)"
     */
    private Double ratio;


    /**
     * @hibernate.many-to-one  column = "`parent_id`" class = "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin" update = "true" insert = "true" not-null = "true"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#getBin()
     *
     * @uml.property name="bin"
     */
    public Bin getBin() {
        return bin;
    }

    /**
     * @hibernate.id column = "`bin_id`" generator-class = "native"
     * @hibernate.generator-param name = "sequence" value = "BIN_ID"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#getId()
     *
     * @uml.property name="id"
     */
    public Integer getId() {
        return id;
    }

    /**
     * @hibernate.property column = "`ion_a`" update = "true" insert = "true" not-null = "true"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#getIonA()
     *@swt.variable visible="true" name="First Ion" searchable="true"
	 * @swt.modify canModify="true"
     * @uml.property name="ionA"
     */
    public Integer getIonA() {
        return ionA;
    }

    /**
     * @hibernate.property column = "`ion_b`" update = "true" insert = "true" not-null = "true"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#getIonB()
     *@swt.variable visible="true" name="Second Ion" searchable="true"
	 * @swt.modify canModify="true"
     * @uml.property name="ionB"
     */
    public Integer getIonB() {
        return ionB;
    }


	/**
     * @hibernate.property column = "`name`" update = "true" insert = "true" not-null = "true"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#getName()
     *@swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="true"
     * @uml.property name="name"
     */
    public String getName() {
        return name;
    }

	/**
     * @hibernate.property column = "`ratio`" update = "true" insert = "true" not-null = "true"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#getRatio()
     *@swt.variable visible="true" name="Ratio" searchable="true"
	 * @swt.modify canModify="true"
     * @uml.property name="ratio"
     */
    public Double getRatio() {
        return ratio;
    }

	/**
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#setBin(edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.LibraryMassSpec)
     *
     * @uml.property name="bin"
     */
    public void setBin(
        Bin bin) {
        this.bin = bin;
    }

	/**
     * @param id
     *            The id to set.
     *
     * @uml.property name="id"
     */
    public void setId(Integer id) {
        this.id = id;
    }

	/**
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#setIonA(Integer)
     *
     * @uml.property name="ionA"
     */
    public void setIonA(Integer ionA) {
        this.ionA = ionA;
    }

	/**
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#setIonB(Integer)
     *
     * @uml.property name="ionB"
     */
    public void setIonB(Integer ionB) {
        this.ionB = ionB;
    }

	/**
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#setName(java.lang.String)
     *
     * @uml.property name="name"
     */
    public void setName(String name) {
        this.name = name;
    }

	/**
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.meta.virtual.VirtualBin#setRatio(double)
     *
     * @uml.property name="ratio"
     */
    public void setRatio(Double ratio) {
        this.ratio = ratio;
    }
}
