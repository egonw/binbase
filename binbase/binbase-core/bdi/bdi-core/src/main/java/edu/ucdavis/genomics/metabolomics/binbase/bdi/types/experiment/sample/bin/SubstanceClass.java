/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import java.util.Collection;
import java.util.HashSet;

/**
 * @swt
 * @author wohlgemuth
 * @hibernate.class table = "SUBSTANCE_CLASSES" dynamic-insert = "true"
 *                  dynamic-update = "true"
 */
public class SubstanceClass implements Comparable{
	/**
	 * classifications of this substance class
	 * contains all objects of this class
	 */
	private Collection classifications;
	
	/**
	 * description
	 */
	private String description;

	/**
	 * internal id
	 */
	private Integer id;

	/**
	 * internal name
	 */
	private String name;

	/**
	 * smile code
	 */
	private String smile;

	/**
	 * 
	 */
	public SubstanceClass() {
		super();
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
     * @hibernate.collection-one-to-many class = "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Classification"
     * @hibernate.collection-key column = "class"
	 * @return
	 */
	public Collection getClassifications() {
		return this.classifications;
	}

	/**
	 * @swt.variable visible="true" name="Description" searchable="true"
	 * @swt.modify canModify="true"

	 * the description of this class
	 * 
	 * @hibernate.property column = "describtion" not-null = "false"
	 * @return
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @hibernate.id column = "id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 *                            the internal id
	 * @return
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * the name of this class
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "name" not-null = "true"
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * a smile code for this, usefull so we can generate pictures
	 * 
	 * @swt.variable visible="true" name="Smile" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "smile" not-null = "false"
	 * @return
	 */
	public String getSmile() {
		return this.smile;
	}

	public void setClassifications(Collection classifications) {
		this.classifications = classifications;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSmile(String smile) {
		this.smile = smile;
	}

	public int compareTo(Object arg0) {
		if(arg0 instanceof SubstanceClass){
			this.getId().compareTo(((SubstanceClass)arg0).getId());
		}
		return -1;
	}

	public void addClassifications(Classification ref) {
		if (this.getClassifications() == null) {
			this.setClassifications(new HashSet());
		}
		ref.setSubstanceClass(this);
		this.getClassifications().add(ref);
	}

	@Override
	public String toString() {
		return this.getName();
	}

}
