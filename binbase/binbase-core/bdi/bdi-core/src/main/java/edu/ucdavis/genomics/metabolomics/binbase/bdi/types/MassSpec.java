/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.batik.svggen.SVGGeneratorContext;
import org.apache.batik.svggen.SVGGraphics2D;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.w3c.dom.Document;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.math.CalculateSpectraProperties;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter;
import edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.plot.SpectraChart;
import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet;

/**
 * @author wohlgemuth defines a massspec used in the binbase and for external
 *         representations
 */
public abstract class MassSpec implements Commentable, Comparable {

	public static final int BAD_MATCH = 0;

	public static final int MEDICORE_MATCH = 500;

	public static final int GOOD_MATCH = 700;

	public int compareTo(final Object arg0) {
		if (arg0 instanceof MassSpec) {
			final MassSpec a = (MassSpec) arg0;
			return a.getRetentionIndex().compareTo(getRetentionIndex());
		}
		return toString().compareTo(arg0.toString());
	}

	public abstract String getApexSpec();

	public abstract void setApexSpec(String apexSpec);

	/**
	 * 
	 */
	public MassSpec() {
		super();
	}

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * DOCUMENT ME!
	 */
	private CalculateSpectraProperties prop;

	/**
	 * DOCUMENT ME!
	 */
	private List<Ion> apex;

	/**
	 * DOCUMENT ME!
	 */
	private List<Ion> mostabundant;

	/**
	 * DOCUMENT ME!
	 */
	private List<Ion> spectra;

	/**
	 * DOCUMENT ME!
	 */
	private Integer basepeak;

	/**
	 * @param apex
	 *            The apex to set.
	 * @uml.property name="apex"
	 */
	public final void setApex(final List<Ion> apex) {
		this.apex = apex;
		setApexSpec((SpectraConverter.collectionToApexString(apex)));
	}

	/**
	 * die apexmassen!
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getApex()
	 * @uml.property name="apex"
	 */
	public final List<Ion> getApex() {
		getSpectra();

		return apex;
	}

	/**
	 * @param id
	 *            The id to set.
	 * @uml.property name="id"
	 */
	public abstract void setId(Integer id);

	/**
	 * 
	 */
	public abstract Integer getId();

	/**
	 * @param massSpec
	 *            The massSpec to set.
	 * @uml.property name="massSpec"
	 */
	public abstract void setMassSpec(String massSpec);

	public abstract String getMassSpec();

	/**
	 * @param retentionIndex
	 *            The retentionIndex to set.
	 * @uml.property name="retentionIndex"
	 */
	public abstract void setRetentionIndex(Integer retentionIndex);

	/**
	 * @return
	 */
	public abstract Integer getRetentionIndex();

	/**
	 * @param similarity
	 *            The similarity to set.
	 * @uml.property name="similarity"
	 */
	public abstract void setSimilarity(Double similarity);

	/**
	 * @uml.property name="similarity"
	 */
	public abstract Double getSimilarity();

	/**
	 * @param spectra
	 *            The spectra to set.
	 * @uml.property name="spectra"
	 */
	public final void setSpectra(final List<Ion> spectra) {
		this.spectra = spectra;
		setMassSpec((SpectraConverter.collectionToString(spectra)));
	}

	/**
	 * das massenspektrum!
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getSpectra()
	 * @uml.property name="spectra"
	 */
	public final List<Ion> getSpectra() {
		if (getApexSpec() == null) {
			prop = new CalculateSpectraProperties((getMassSpec()));
		} else {
			prop = new CalculateSpectraProperties((getMassSpec()),
					(getApexSpec()));
		}

		basepeak = new Integer(prop.calculateBasePeak());
		spectra = prop.calculateSpectra();
		apex = prop.calculateApex();
		mostabundant = prop.calculateMostAbundant();

		return spectra;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.MassSpec#calculateBasePeak()
	 */
	public Integer getBasePeak() {
		return basepeak;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.MassSpec#calculateMostAbundant()
	 */
	public final List<Ion> getMostAbundant() {
		return mostabundant;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		try {
			if (obj instanceof MassSpec) {
				if (obj != null) {
					if (((MassSpec) obj).getId().equals(getId())) {
						return true;
					}
				}
				return false;
			}

		} catch (final Exception e) {
			return false;

		}
		return false;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	@Override
	public String toString() {
		return "" + getId();
	}

	public final double[][] transform() {
		return getProperties().transform();
	}

	protected CalculateSpectraProperties getProperties() {
		getSpectra();
		return prop;
	}

	public void addComment(final Comment comment) {
		if (getComments() == null) {
			setComments(new HashSet());
		}
		comment.setType(this);
		getComments().add(comment);
	}

	public boolean isGoodMatch() {
		return getSimilarity() > GOOD_MATCH;
	}

	public boolean isMedicoreMatch() {
		return getSimilarity() > MEDICORE_MATCH;
	}

	public boolean isBadMatch() {
		return getSimilarity() > BAD_MATCH;
	}

	public Integer binKey() {
		return getMassSpec().hashCode();
	}

	/**
	 * generate a chart from the mass spec
	 * 
	 * @param includeName
	 * @param includeRi
	 * @param includeId
	 * @return
	 */
	public JFreeChart generateMassSpecChart() {
		// load the data into a dataset
		BinBaseXYDataSet dataset = new BinBaseXYDataSet();

		// get the spectra from the parameters
		Collection<Ion> c = SpectraConverter.stringToCollection(this
				.getMassSpec());

		// parse the string
		double[] masses = new double[c.size()];
		double[] intensities = new double[c.size()];

		int maxIon = 0;
		int counter = 0;
		for (Ion o : c) {
			masses[counter] = o.getMass();
			intensities[counter] = o.getRelativeIntensity();
			counter++;

			// just to set the scale to a smart value
			if (o.getMass() > maxIon) {
				if (o.getRelativeIntensity() > 0.5) {
					maxIon = o.getMass();
				}
			}
		}

		// add our created dataset
		dataset.addDataSet(masses, intensities, "spec");

		// the chart it self
		JFreeChart chart = SpectraChart.createChart(dataset);
		chart.removeLegend();

		String title = generateChartTitle();

		if (title.isEmpty() == false) {
			chart.setTitle(title);
		}

		// set the axis to 110%
		ValueAxis axis = chart.getXYPlot().getRangeAxis();
		axis.setRange(0, 110);

		// set the x axis to 35 - x
		axis = chart.getXYPlot().getDomainAxis();
		axis.setRange(30, maxIon + 5);

		// sets the background color
		chart.setBackgroundPaint(Color.white);
		return chart;
	}

	/**
	 * generates the title for the chart
	 * 
	 * @return
	 */
	protected String generateChartTitle() {
		return "ri: " + this.getRetentionIndex() + " - " + " id: "
				+ this.getId();
	}

	/**
	 * generates a new buffered image of the given massspec
	 */
	public BufferedImage generateImageOfSpectra(int width, int height) {
		JFreeChart chart = generateMassSpecChart();

		BufferedImage image = chart.createBufferedImage(width, height);

		chart = null;

		return image;
	}

	/**
	 * generates a SVG chart of a massspec
	 * 
	 * @param width
	 * @param height
	 * @return
	 * @throws ParserConfigurationException 
	 */
	public SVGGraphics2D generateSVGOfSpectra(int width, int height) throws ParserConfigurationException {

		JFreeChart chart = generateMassSpecChart();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();

		SVGGeneratorContext ctx = SVGGeneratorContext.createDefault(doc);
		/**/ctx.setEmbeddedFontsOn(true);
		/**/ctx.setPrecision(12); // Best (default=4)
		
		SVGGraphics2D g2d = new SVGGraphics2D(doc);
		/**/g2d = new SVGGraphics2D(ctx, false);
		/**/g2d.setSVGCanvasSize(new Dimension(width, height));

		chart.draw(g2d, new Rectangle(width, height));
		
		return g2d;
	}

}
