/*
 * Created on Jun 6, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import java.util.Collection;
import java.util.HashSet;

/**
 * a simple refrence class for defining meta informations *
 * 
 * @swt
 * @hibernate.class table = "REFERENCE_CLASS" dynamic-insert = "true" dynamic-update =
 *                  "true"
 * @author wohlgemuth
 * @version Jun 6, 2006
 * 
 */
public class RefrenceClass implements Comparable{
	Integer id;

	String name;

	String description;

	Collection refrences;

	String urlPattern;
	/**
	 * @swt.variable visible="true" name="Description" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "description" update = "true" insert =
	 *                     "true" not-null = "true" returns the refrence value
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @hibernate.id column = "id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 *                            returns the id belonging to this refrence
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "name" update = "true" insert = "true"
	 *                     not-null = "true" returns the refrence value
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "all" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Refrence"
	 * @hibernate.collection-key column = "id"
	 */
	public Collection getRefrences() {
		return refrences;
	}

	public void setRefrences(Collection refrences) {
		this.refrences = refrences;
	}

	public void addRefrence(Refrence ref) {
		if (this.getRefrences() == null) {
			this.setRefrences(new HashSet());
		}
		ref.setRefrenceClass(this);
	}

	public int compareTo(Object o) {
		if(o instanceof RefrenceClass){
			return ((RefrenceClass)o).getId().compareTo(getId());
		}
		return 0;
	}
	
	public String toString(){
		return this.getName();
	}

	/**
	 * @swt.variable visible="true" name="Pattern" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`pattern`" update = "true" insert =
	 *                     "true" not-null = "true" returns the refrence value
	 * @return
	 */
	public String getUrlPattern() {
		return urlPattern;
	}

	public void setUrlPattern(String urlPattern) {
		this.urlPattern = urlPattern;
	}
}
