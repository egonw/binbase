/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.AbstractHibernateTest;


/**
 * @author wohlgemuth
 *
 */
public abstract class AbstractCommentableTest extends AbstractHibernateTest {

	/*
	 * Test method for 'edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Commentable.addComment(Comment)'
	 */
	public final void testAddComment() {
		Session session = getSession();
		Transaction t = session.beginTransaction();

		//fill with data
		Commentable toTest = (Commentable) session.load(this.getCommentableClass(),this.getCommentableID());
		int size = toTest.getComments().size();
		for (int i = 0; i < 20; i++) {
			Comment test = this.getCommentableComment();
			test.setText(String.valueOf(test));
			toTest.addComment(test);
		}
		session.save(toTest);
		//check that it is full
		assertTrue(session.createCriteria(Comment.class).list().size() == size+20);
		
		//we want a rollback so that our data are not saved!
		t.rollback();
	}

	public final void testStore() {
		Session session = getSession();
		Transaction t = session.beginTransaction();
		//get the size of the data already in
		Commentable toTest = (Commentable) session.load(this.getCommentableClass(),this.getCommentableID());
		int size = session.createCriteria(this.getCommentableComment().getClass()).list().size();
		
		for (int i = 0; i < 20; i++) {
			Comment test = this.getCommentableComment();
			test.setText(String.valueOf(test));
			test.setType(toTest);
			session.save(test);
		}
		//check that it is full
		assertTrue(session.createCriteria(this.getCommentableComment().getClass()).list().size() == size+20);

		for (int i = 0; i < 10; i++) {
			Comment test = this.getCommentableComment();
			test.setText(String.valueOf(test));
			test.setType(toTest);
			session.save(test);
		}
		//check that more objects ared added
		assertTrue(session.createCriteria(this.getCommentableComment().getClass()).list().size() == size+30);
		
		//we want a rollback so that our data are not saved!
		t.rollback();
	}

	public final void testDelete(){
		Session session = getSession();
		Transaction t = session.beginTransaction();
		//get the size of the data already in
		Commentable toTest = (Commentable) session.load(this.getCommentableClass(),this.getCommentableID());
		int size = session.createCriteria(this.getCommentableComment().getClass()).list().size();
		
		for (int i = 0; i < 20; i++) {
			Comment test = this.getCommentableComment();
			test.setText(String.valueOf(test));
			test.setType(toTest);
			session.save(test);
		}
		//check that it is full
		assertTrue(session.createCriteria(this.getCommentableComment().getClass()).list().size() == size+20);		
		
		//delete all data
		Criteria crit = session.createCriteria(this.getCommentableComment().getClass());
		Iterator it = crit.list().iterator();
		
		while(it.hasNext()){
			session.delete(it.next());
		}
		
		//check that it is null
		assertTrue(session.createCriteria(this.getCommentableComment().getClass()).list().size() == size);
		//we want a rollback so that our data are not saved!
		t.rollback();		
	}
	
	/**
	 * checks if the returned typs are from the right type
	 *
	 */
	public final void testQuery(){

		Session session = getSession();
		Transaction t = session.beginTransaction();
		//get the size of the data already in
		Commentable toTest = (Commentable) session.load(this.getCommentableClass(),this.getCommentableID());
		int size = session.createCriteria(this.getCommentableComment().getClass()).list().size();
		
		for (int i = 0; i < 20; i++) {
			Comment test = this.getCommentableComment();
			test.setText(String.valueOf(test));
			test.setType(toTest);
			session.save(test);
		}
		//check that it is full
		List tests = session.createCriteria(this.getCommentableComment().getClass()).list();
		assertTrue(tests.size() == size+20);
		
		
		for(int i = 0; i < tests.size(); i++){
			assertTrue(((Comment)tests.get(i)).getType().equals(toTest));
		}
		
		t.rollback();
	}
	
	/**
	 * returns the needed commentable for testing
	 * @return
	 */
	public abstract Class getCommentableClass();
	
	/**
	 * returns the needed commentable for testing
	 * @return
	 */
	public abstract Serializable getCommentableID();
	
	/**
	 * returns a comment specified for testing
	 * @return
	 */
	public abstract Comment getCommentableComment();
	
}
