/*
 * Created on Jan 19, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.executor;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;

public class SimpleExecutorImpl extends Execute {
	private ExecutionHandler handler;

	private Logger logger = Logger.getLogger(getClass());

	public SimpleExecutorImpl(ExecutionHandler handler) {
		this.handler = handler;
	}

	@Override
	public synchronized void executeQuery(IModel type) {
		handler.done(type.executeQuery());
	}

}
