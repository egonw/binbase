/*
 * Created on 09.06.2004
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec;
import edu.ucdavis.genomics.metabolomics.exception.DataStoreException;
import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException;

/**
 * @swt
 * @hibernate.class table = "LIBRARY" dynamic-insert = "true" dynamic-update =
 *                  "true" select-before-update = "true"
 */
public class Library {
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * 
	 * @uml.property name="sim"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	edu.ucdavis.genomics.metabolomics.util.math.Similarity sim = new edu.ucdavis.genomics.metabolomics.util.math.Similarity();

	/**
	 * DOCUMENT ME!
	 */
	private Collection spectra;

	/**
	 * DOCUMENT ME!
	 */
	private Logger logger = Logger.getLogger(getClass());

	/**
	 * DOCUMENT ME!
	 */
	private String description;

	/**
	 * DOCUMENT ME!
	 */
	private String name;

	/**
	 * DOCUMENT ME!
	 */
	private Integer id;

	/**
	 * DOCUMENT ME!
	 * 
	 * @param id
	 *            DOCUMENT ME!
	 * 
	 * @uml.property name="id"
	 */
	public final void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @hibernate.id column = "`id`" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 * @uml.property name="id"
	 */
	public final Integer getId() {
		return id;
	}

	/**
	 * @param description
	 *            The description to set.
	 * 
	 * @uml.property name="description"
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`name`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.Library#getName()
	 * 
	 * @uml.property name="name"
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @hibernate.property column = "`description`" update = "true" insert =
	 *                     "true" not-null = "true"
	 * 
	 * @uml.property name="description"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * berechnet die passenden spectren und ist damit eine buisness methode
	 * 
	 * @throws DataStoreException
	 * @throws NotSupportedException
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.Library#getMatchingContent(edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra,
	 *      double)
	 */
	public Collection getMatchingContent(MassSpec massspec, double similarity) throws NotSupportedException, DataStoreException {
		return getMatchingContent(massspec, similarity, Integer.MAX_VALUE);
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.Library#getMatchingContent(edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.MassSpec,
	 *      double, Integer)
	 */
	public Collection getMatchingContent(MassSpec massspec, double similarity, int window) throws NotSupportedException, DataStoreException {
		Iterator it = this.getSpectra().iterator();
		List result = new Vector();
		sim.setUnknownSpectra(massspec.transform());
		
		while (it.hasNext()) {
			try {
				LibraryMassSpec spec = (LibraryMassSpec) it.next();

				if (spec.getSpectra() != null) {
					if ((spec.getRetentionIndex().intValue() == -1) | (window == Integer.MAX_VALUE)) {
						sim.setLibrarySpectra(spec.transform());

						double simca = sim.calculateSimimlarity();
						if (simca > similarity) {
							spec.setSimilarity(new Double(simca));
							result.add(spec);
						}
					} else {
						if (spec.getRetentionIndex().intValue() > (massspec.getRetentionIndex().intValue() - window)) {
							if (spec.getRetentionIndex().intValue() < (massspec.getRetentionIndex().intValue() + window)) {
								logger.debug("is in range...");
								sim.setLibrarySpectra(spec.transform());

								double simca = sim.calculateSimimlarity();

								if (simca > similarity) {
									spec.setSimilarity(new Double(simca));
									result.add(spec);
								}
							}
						}
					}
				}
			} catch (NullPointerException e) {
				logger.warn(e);
			} catch (Exception e) {
				logger.error(e);
			}
		}

		//sort the result by the highest similarity
		Collections.sort(result, new Comparator() {

			public int compare(Object o1, Object o2) {
				LibraryMassSpec a = (LibraryMassSpec) o1;
				LibraryMassSpec b = (LibraryMassSpec) o2;
				
				return a.getSimilarity().compareTo(b.getSimilarity());
			}

		});
		return result;
	}

	/**
	 * @param name
	 *            The name to set.
	 * 
	 * @uml.property name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @param spectra
	 *            The spectra to set.
	 * 
	 * @uml.property name="spectra"
	 */
	public void setSpectra(Collection spectra) {
		this.spectra = spectra;
	}

	/**
	 * @hibernate.set inverse = "true" lazy = "true" inverse="true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.LibraryMassSpec"
	 * @hibernate.collection-key column = "`id`"
	 * @uml.property name="spectra"
	 */
	public Collection getSpectra() {
		return spectra;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	public String toString() {
		return this.getName();
	}

	/**
	 * adds just one spectra
	 * 
	 * @param spectra
	 */
	public void addSpectra(LibraryMassSpec spectra) {
		if (this.getSpectra() == null) {
			this.setSpectra(new HashSet());
		}
		spectra.setLibrary(this);
		this.getSpectra().add(spectra);
	}
}
