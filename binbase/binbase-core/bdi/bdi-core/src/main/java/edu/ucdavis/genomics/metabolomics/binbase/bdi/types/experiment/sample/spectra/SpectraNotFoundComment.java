/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Commentable;

/**
 * @author wohlgemuth
 * @hibernate.subclass discriminator-value = "3"
 */
public class SpectraNotFoundComment extends Comment{

	/**
	 * 
	 */
	public SpectraNotFoundComment() {
		super();
	}

	/**
	 * @hibernate.many-to-one  column = "type" class = "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.NotFoundSpectra" update = "true" insert = "true" not-null = "true"
	 */
	public Commentable getType() {
		return super.getType();
	}

}
