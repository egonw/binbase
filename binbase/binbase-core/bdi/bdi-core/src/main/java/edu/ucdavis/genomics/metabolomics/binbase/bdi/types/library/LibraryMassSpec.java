/*
 * Created on 09.06.2004
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library;

import java.util.Collection;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec;

/**
 * @swt
 * @hibernate.class table = "LIBRARY_SPEC" dynamic-insert = "true"
 *                  dynamic-update = "true"
 * @author wohlgemuth
 */

public class LibraryMassSpec extends MassSpec {

	private Collection comments;

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * DOCUMENT ME!
	 */
	private String massSpec;

	/**
	 * DOCUMENT ME!
	 * 
	 * @uml.property name="library"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private Library library;

	/**
	 * DOCUMENT ME!
	 */
	private String name;

	/**
	 * DOCUMENT ME!
	 */
	private Double similarity;

	/**
	 * DOCUMENT ME!
	 */
	private Integer id;

	private Integer retentionIndex;
	
	/**
	 * @param id
	 *            The id to set.
	 * 
	 * @uml.property name="id"
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @hibernate.id column = "`specId`" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 * 
	 * @uml.property name="id"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param library
	 *            The library to set.
	 * 
	 * @uml.property name="library"
	 */
	public void setLibrary(Library library) {
		this.library = library;
	}

	/**
	 * @hibernate.many-to-one cascade = "none" column = "`id`"
	 * @uml.property name="library"
	 */
	public Library getLibrary() {
		return library;
	}

	/**
	 * @param massSpec
	 *            The massSpec to set.
	 * 
	 * @uml.property name="massSpec"
	 */
	public void setMassSpec(String massSpec) {
		this.massSpec = massSpec;
	}

	/**
	 * @hibernate.property column = "`spectra`" update = "true" insert = "true"
	 *                      not-null = "true"
	 * @uml.property name="massSpec"
	 */
	public String getMassSpec() {
		return massSpec;
	}

	/**
	 * @param name
	 *            The name to set.
	 * 
	 * @uml.property name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @hibernate.property column = "`name`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="true"
	 * @uml.property name="name"
	 */
	public String getName() {
		return name;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param similarity
	 *            DOCUMENT ME!
	 * 
	 * @uml.property name="similarity"
	 */
	public void setSimilarity(Double similarity) {
		this.similarity = similarity;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @swt.variable visible="true" name="Similarity" searchable="true"
	 * @swt.modify canModify="false"
	 * @return DOCUMENT ME!
	 * 
	 * @uml.property name="similarity"
	 */
	public Double getSimilarity() {
		return similarity;
	}


	/**
	 * @swt.variable visible="true" name="Retention Index" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`retention_index`" update = "true" insert = "true"
	 *                     not-null = "false"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.LibraryMassSpec#getRetentionIndex()
	 */
	public Integer getRetentionIndex() {
		return this.retentionIndex;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec#setRetentionIndex(java.lang.Integer)
	 */
	public void setRetentionIndex(Integer retentionIndex) {
		this.retentionIndex = retentionIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec#getApexSpec()
	 */
	public String getApexSpec() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec#setApexSpec(java.sql.Clob)
	 */
	public void setApexSpec(String apexSpec) {
		throw new RuntimeException("not availbale");
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.LibraryMassSpecComment"
	 * @hibernate.collection-key column = "type"
	 */
	public Collection getComments() {
		return this.comments;
	}

	public void setComments(Collection comments) {
		this.comments = comments;
	}

	public Comment createComment() {
		return new LibraryMassSpecComment();
	}
}
