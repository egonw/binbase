/*
 * Created on Dec 8, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.model;

import java.util.HashMap;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * provides us with models and takes care that frome very modelfactory only one
 * factory exist
 * 
 * @author wohlgemuth
 * @version Dec 8, 2005
 * 
 */
public abstract class ModelFactory extends AbstractFactory {
	public static final String DEFAULT_PROPERTY_NAME = ModelFactory.class.getName();

	/**
	 * contains refrences to all initialized factorys
	 */
	private static Map <String,ModelFactory>factorys = new HashMap<String,ModelFactory>();

	public ModelFactory() {
		super();
	}

	/**
	 * creates the model for the given clazz, the clazz can be used by the
	 * implementation of the factory as indendifer for an internal caching
	 * system
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @return
	 */
	public final IModel createModel(Class clazz){
		return this.createModel(clazz, false);
	}

	/**
	 * creates the model for the given clazz, the clazz can be used by the
	 * implementation of the factory as indendifer for an internal caching
	 * system
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @return
	 */
	public abstract IModel createModel(Class clazz, boolean forceNew);

	
	/**
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @return
	 */
	public static ModelFactory newInstance() {
		return newInstance(findFactory(DEFAULT_PROPERTY_NAME, HibernateModelFactoryImpl.class.getName()));
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @return
	 */
	public static ModelFactory newInstance(String factoryClass) {
		//use an already provided factory
		if(factorys.get(factoryClass) != null){
			logger.debug("using cached implementation");
			return factorys.get(factoryClass);
		}
		
		logger.debug("create new implementation");
		Class classObject;
		ModelFactory factory;

		try {
			classObject = Class.forName(factoryClass);
			factory = (ModelFactory) classObject.newInstance();
			factorys.put(factoryClass,factory);
			return factory;
		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}

}
