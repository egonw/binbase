package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.type;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Type;

/**
 * @hibernate.subclass discriminator-value = "Method Blank"
 * @author wohlgemuth
 *
 */
public class MethodBlankType extends Type{

}
