/*
 * Created on Jan 19, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.executor;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * creates executors
 * 
 * @author wohlgemuth
 * @version Jan 19, 2007
 * 
 */
public abstract class ExecutorFactory extends AbstractFactory {
	public abstract Execute create(ExecutionHandler handler);

	public static final String DEFAULT_PROPERTY_NAME = ExecutorFactory.class.getName();

	/**
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @return
	 */
	public static ExecutorFactory newInstance() {
		if (System.getProperty(DEFAULT_PROPERTY_NAME) == null) {
			return newInstance(findFactory(DEFAULT_PROPERTY_NAME, SimpleExecutorFactoryImpl.class.getName()));
		} else {
			return newInstance(findFactory(DEFAULT_PROPERTY_NAME, System.getProperty(DEFAULT_PROPERTY_NAME)));
		}
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @return
	 */
	public static ExecutorFactory newInstance(String factoryClass) {
		Class classObject;
		ExecutorFactory factory;

		try {
			classObject = Class.forName(factoryClass);
			factory = (ExecutorFactory) classObject.newInstance();
			return factory;
		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}
}
