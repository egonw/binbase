/*
 * Created on Jan 19, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.executor;

public interface ExecutionHandler {
	/**
	 * our result
	 * @author wohlgemuth
	 * @version Jan 19, 2007
	 * @param object
	 */
	public void done(Object[] object);
}
