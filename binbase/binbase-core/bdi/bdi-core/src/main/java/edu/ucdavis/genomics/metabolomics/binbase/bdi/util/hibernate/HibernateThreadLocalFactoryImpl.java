/*
 * Created on Dec 9, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate;

import java.util.Properties;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.w3c.dom.Document;

import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

/**
 * provides us with sessions over the hibernate thread local pattern
 * 
 * @author wohlgemuth
 * @version Dec 9, 2005
 * 
 */
public class HibernateThreadLocalFactoryImpl extends HibernateFactory {
	private SessionFactory sessionFactory;

	private ConnectionFactory fact = null;
	/**
	 * internal session
	 */
	private final ThreadLocal<Session> session = new ThreadLocal<Session>();

	public HibernateThreadLocalFactoryImpl() {
		super();
	}

	@Override
	public Session getSession() {
		Session s = session.get();
		// Open a new Session, if this thread has none yet
		if (s == null) {
			logger.info("open session...");
			s = sessionFactory.openSession(fact.getConnection());
			// Store it in the ThreadLocal variable
			session.set(s);
		}
		if (s.isOpen() == false) {
			if (s.isConnected() == false) {
				HibernateFactory.logger.info("reopen session");
				s = null;
				s = sessionFactory.openSession(fact.getConnection());
				// Store it in the ThreadLocal variable
				session.set(s);
			}
		}

		return s;
	}

	@Override
	public void initialize(Properties p) {

		Configuration c = new Configuration();


		ResourceSource source = new ResourceSource("/config/hibernate.cfg.xml");
		Document d = null;
		try {
			d = DocumentBuilderFactory.newInstance().newDocumentBuilder()
					.parse(source.getStream());

		} catch (Exception e) {
			throw new HibernateException(e);
		}

		c.setProperties(p);
		c.configure(d);

		fact = ConnectionFactory.getFactory();
		fact.setProperties(p);
		sessionFactory = c.buildSessionFactory();
	}

	@Override
	public void destroySession() {
		logger.info("destroying session...");
		getSession().close();
	}

	@Override
	public void destroyFactory() {
		destroySession();
		sessionFactory.close();
		super.destroyFactory();
	}


	@Override
	public SessionFactory getInternalFactory() {
		getSession();
		return sessionFactory;
	}

}
