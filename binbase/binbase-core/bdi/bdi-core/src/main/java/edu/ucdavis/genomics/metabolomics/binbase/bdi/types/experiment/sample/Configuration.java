/*
 * Created on 30.06.2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample;

import java.sql.Clob;
import java.util.Collection;

import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;


/**
 * @swt
 * @author wohlgemuth
 * @hibernate.class table = "CONFIGURATION" dynamic-insert = "false" dynamic-update = "false" 
 *
 */
public class Configuration
    {
    /**
     * Comment for <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 2L;

    /**
     * DOCUMENT ME!
     */
    private Clob dataClob;

    /**
     * DOCUMENT ME!
     */
    private Integer id;

    /**
     * DOCUMENT ME!
     */
    private Collection samples;

    /**
     * DOCUMENT ME!
     */
    private Integer version;

    /**
     * @hibernate.property column = "data" update = "false" insert = "false"
     * @return
     */
    public Clob getDataClob() {
        return dataClob;
    }

    /**
     * @hibernate.id column = "`configuration_id`" generator-class = "assigned"
     */
    public Integer getId() {
        return id;
    }

    /**
     * @hibernate.set lazy="true" cascade = "none" inverse = "true"
     * @hibernate.collection-one-to-many class =
     *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample"
     * @hibernate.collection-key column = "`configuration_id`"
     */
    public Collection getSamples() {
        return samples;
    }

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Configuration#getTree()
     */
    public Element getTree() {
        try {
            return XmlHandling.readXml(this.getDataClob().getAsciiStream());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @swt.variable visible="true" name="Version" searchable="true"
	 * @swt.modify canModify="false"
     * @hibernate.property column = "`version`" insert = "false" update = "false" unique = "true"
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * DOCUMENT ME!
     *
     * @param dataClob DOCUMENT ME!
     */
    public void setDataClob(Clob dataClob) {
        this.dataClob = dataClob;
    }

    /**
     * DOCUMENT ME!
     *
     * @param id DOCUMENT ME!
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * DOCUMENT ME!
     *
     * @param samples DOCUMENT ME!
     */
    public void setSamples(Collection samples) {
        this.samples = samples;
    }

    /**
     * DOCUMENT ME!
     *
     * @param version DOCUMENT ME!
     */
    public void setVersion(Integer version) {
        this.version = version;
    }
}
