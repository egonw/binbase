/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

/**
 * @swt
 * @hibernate.class table = "CLASSIFICATION" dynamic-insert = "true"
 *                  dynamic-update = "true"
 * @author wohlgemuth is used to link substanceclasses to structures
 */
public class Classification {
	/**
	 * the internal id
	 */
	private Integer id;

	/**
	 * the assigned structure
	 */
	private Structure structure;

	/**
	 * the assigned substance class
	 */
	private SubstanceClass substanceClass;

	/**
	 * 
	 * 
	 */
	public Classification() {
		super();
	}

	/**
	 * @hibernate.id column = "id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 * @return
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @hibernate.many-to-one column = "STRUCTURE" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Structure"
	 *                        update = "true" insert = "true"
	 * @return
	 */
	public Structure getStructure() {
		return this.structure;
	}

	/**
	 * @swt.variable visible="true" name="Class" searchable="false"
	 * @hibernate.many-to-one column = "class" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.SubstanceClass"
	 *                        update = "true" insert = "true"
	 * @return
	 */
	public SubstanceClass getSubstanceClass() {
		return this.substanceClass;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setStructure(Structure structure) {
		this.structure = structure;
	}

	public void setSubstanceClass(SubstanceClass substanceClass) {
		this.substanceClass = substanceClass;
	}

}
