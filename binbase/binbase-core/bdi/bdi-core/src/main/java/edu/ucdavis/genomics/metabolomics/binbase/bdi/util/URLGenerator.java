package edu.ucdavis.genomics.metabolomics.binbase.bdi.util;

public class URLGenerator {
	public static final String ID_TAG = "<ID>";
	
	/**
	 * generates the url by replacing all id tags with a corresponding url
	 * @param pattern
	 * @param id
	 * @return
	 */
	public static String generateURL(String pattern, String id){
		return pattern.replaceAll(ID_TAG, id);
	}
	
	/**
	 * makes sure the url patter contains 1 id tag
	 * @param pattern
	 * @return
	 */
	public static boolean validateURLPattern(String pattern){
		if(pattern == null){
			return false;
		}
		return pattern.contains(ID_TAG);
	}
}
