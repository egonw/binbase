/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;

/**
 * @swt
 * @author wohlgemuth defines a synonym
 * @hibernate.class table = "BIN_COMPARE"
 */
public class BinCompare {
	private Integer id;

	/**
	 * 
	 */
	private Bin bin;

	private Bin compareTo;

	
	/**
	 * 
	 */
	public BinCompare() {
		super();
	}

	/**
	 * @hibernate.id column = "id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 * @return
	 */
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * @swt.variable visible="true" name="Bin" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one cascade = "none" column = "bin_id" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin"
	 */
	public Bin getBin() {
		return this.bin;
	}

	public void setBin(Bin bin) {
		this.bin = bin;
	}

	/**
	 * @swt.variable visible="true" name="Compare To" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one cascade = "none" column = "compare" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin"
	 */
	public Bin getCompareTo() {
		return compareTo;
	}

	public void setCompareTo(Bin compareTo) {
		this.compareTo = compareTo;
	}
}
