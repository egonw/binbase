package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample;


/**
 * a most basic sample
 * 
 * @author wohlgemuth
 * 
 */
public interface GenericSample{

	public abstract Integer getId();

	public abstract String getName();

	public abstract String getSetupxId();

	public abstract Integer getVersion();

	public abstract Boolean isCorrectionFailed();

	public abstract Boolean isSaturated();

	public abstract Boolean isVisible();

	public abstract Boolean isFinished();

}