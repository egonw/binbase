/*
 * Created on 24.06.2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.math;

import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter;
import edu.ucdavis.genomics.metabolomics.util.math.SpectraArrayKey;
import edu.ucdavis.genomics.metabolomics.util.serialize.ClassConverter;

/**
 * @author wohlgemuth
 */
public class CalculateSpectraProperties implements SpectraArrayKey {
	/**
	 * DOCUMENT ME!
	 */
	private InputStream apexStream = null;

	/**
	 * DOCUMENT ME!
	 */
	private InputStream spectraStream = null;

	/**
	 * DOCUMENT ME!
	 */
	private List<Ion> apex;

	/**
	 * DOCUMENT ME!
	 */
	private List<Ion> mostAbundant;

	/**
	 * DOCUMENT ME!
	 */
	private List<Ion> spectra = null;

	/**
	 * DOCUMENT ME!
	 */
	private boolean generate = false;

	/**
	 * DOCUMENT ME!
	 */
	private int basePeak;

	/**
	 * DOCUMENT ME!
	 */
	private int unique;

	/**
	 * sets the properties
	 * 
	 * @param spectra
	 */
	public CalculateSpectraProperties(InputStream spectra, InputStream apex) {
		this.spectraStream = spectra;
		this.apexStream = apex;
		this.calculate();
	}

	/**
	 * Creates a new CalculateSpectraProperties object.
	 * 
	 * @param spectra
	 *            DOCUMENT ME!
	 */
	public CalculateSpectraProperties(InputStream spectra) {
		this.spectraStream = spectra;
		this.apexStream = null;
		this.calculate();
	}

	/**
	 * Creates a new CalculateSpectraProperties object.
	 * 
	 * @param spectra
	 *            DOCUMENT ME!
	 */
	public CalculateSpectraProperties(String spectra) {
		try {
			this.spectraStream = ClassConverter.stringToInputStream(spectra);
			this.apexStream = null;
			this.calculate();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public CalculateSpectraProperties(String spectra, String apex) {
		try {
			this.spectraStream = ClassConverter.stringToInputStream(spectra);
			this.apexStream = ClassConverter.stringToInputStream(apex);
			this.calculate();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Creates a new CalculateSpectraProperties object.
	 * 
	 * @param spectra
	 *            DOCUMENT ME!
	 */
	public CalculateSpectraProperties(Collection<Ion> spectra) {
		this(SpectraConverter.collectionToString(spectra));
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	public List<Ion> calculateApex() {
		return apex;
	}

	/**
	 * calculate the apexsn in a stupid way just the average sn over all apex
	 * masses
	 * 
	 * @param the
	 *            estimated noise
	 * @return
	 */
	public double calculateApexSn(double noise) {
		double value = 0;

		for (int i = 0; i < apex.size(); i++) {
			value = value + calculateSn((apex.get(i)).getMass(), noise);
		}

		return value / apex.size();
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	public int calculateBasePeak() {
		return basePeak;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param quantmass
	 *            DOCUMENT ME!
	 * @return DOCUMENT ME!
	 */
	public double calculateHeight(int quantmass) {
		try {
			Ion ion = new Ion();
			ion.setMass(quantmass);
			ion = this.spectra.get(this.spectra.indexOf(ion));

			return ion.getAbsoluteIntensity();
		}
		catch (ArrayIndexOutOfBoundsException e) {
			return -1;
		}
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	public List<Ion> calculateMostAbundant() {
		return mostAbundant;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param mass
	 *            DOCUMENT ME!
	 * @return DOCUMENT ME!
	 */
	public double calculateRatio(int mass) {
		try {
			if (mass > 0) {
				Ion quali = new Ion();
				Ion base = new Ion();

				base.setMass(this.calculateBasePeak());
				base = spectra.get(spectra.indexOf(base));

				quali.setMass(mass);
				quali = spectra.get(spectra.indexOf(quali));

				return (quali.getAbsoluteIntensity() / base.getAbsoluteIntensity());
			}
		}
		catch (Exception e) {
			return -1;
		}

		return -1;
	}

	public double calculateUniqueRatio() {
		return calculateRatio(calculateUniqueMass());
	}

	/**
	 * trys to calculate the signal noise in a stupid way
	 * 
	 * @param noise
	 *            the estimated noise
	 * @param mass
	 *            the mass to use
	 * @return
	 */
	public double calculateSn(int mass, double noise) {
		Ion a = new Ion();
		a.setMass(mass);
		a = this.spectra.get(this.spectra.indexOf(a));

		return a.getAbsoluteIntensity() / noise;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	public List<Ion> calculateSpectra() {
		return spectra;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	public int calculateUniqueMass() {
		return unique;
	}

	/**
	 * DOCUMENT ME!
	 */
	private void calculate() {
		spectra = new Vector<Ion>();
		apex = new Vector<Ion>();
		mostAbundant = new Vector<Ion>();

		String massspec = null;
		String masses = null;

		try {
			if (apexStream != null) {
				this.generate = false;
				masses = ClassConverter.convertStreamToString(this.apexStream);

				if (masses.indexOf("+") > -1) {
					StringTokenizer tokenizer = new StringTokenizer(masses, "+");

					while (tokenizer.hasMoreTokens()) {
						apex.add(new Ion(0, 0, Integer.parseInt(tokenizer.nextToken())));
					}
				}
				else {
					apex.add(new Ion(0, 0, Integer.parseInt(masses)));
				}
			}
			else {
				generate = true;
			}

			massspec = ClassConverter.convertStreamToString(this.spectraStream);
		}
		catch (Exception e1) {
			e1.printStackTrace();
		}

		StringTokenizer tokenizer = new StringTokenizer(massspec, " ");
		double maxAbundance = 0;
		double uniqueAbundance = 0;

		basePeak = 0;
		unique = 0;

		while (tokenizer.hasMoreTokens()) {
			StringTokenizer token = new StringTokenizer(tokenizer.nextToken(), ":");
			Integer ionA = new Integer(token.nextToken());
			Double abundanceA = new Double(token.nextToken());

			if (abundanceA.doubleValue() > 0) {
				Ion ion = new Ion(abundanceA.doubleValue(), 0, ionA.intValue());
				spectra.add(ion);

				double abs = abundanceA.doubleValue();

				if (abs >= maxAbundance) {
					maxAbundance = abs;
					basePeak = ion.getMass();
				}
			}
		}

		List<Ion> temp = spectra;
		Collections.sort(temp);

		for (int i = 0; i < spectra.size(); i++) {
			Ion ion = temp.get(i);
			ion.setRelativeIntensity(ion.getAbsoluteIntensity() / maxAbundance);

			if (i < 20) {
				mostAbundant.add(ion);
			}

			int index = apex.indexOf(ion);

			if (index != -1) {
				Ion a = apex.get(index);
				a.setRelativeIntensity(ion.getRelativeIntensity());
				a.setAbsoluteIntensity(ion.getAbsoluteIntensity());
			}

			if (ion.getMass() > 160) {
				if (ion.getAbsoluteIntensity() > uniqueAbundance) {
					if (ion.getAbsoluteIntensity() > ((maxAbundance * 5) / 100)) {
						uniqueAbundance = ion.getAbsoluteIntensity();
						unique = ion.getMass();
					}
				}
			}

			// if its true we must generate the apexmasses by our self
			if (generate) {
				double value = 0;

				if (ion.getMass() <= 200) {
					value = (maxAbundance * 10) / 100;
				}
				else {
					value = (maxAbundance * 5) / 100;
				}

				if (ion.getAbsoluteIntensity() > value) {
					apex.add(ion);
				}
			}
		}

		if (unique == 0) {
			unique = basePeak;
		}
	}

	public double[][] transform() {
		Collection spectra = this.calculateSpectra();
		double[][] spec = new double[MAX_ION][ARRAY_WIDTH];
		Iterator it = spectra.iterator();

		while (it.hasNext()) {
			Ion ion = (Ion) it.next();

			if (ion.getMass() > (MAX_ION - 1)) {
				// logger.warn("cut masspspec to max " + MAX_ION +
				// " masses , current ion is greater than the defined max
				// massspec size");
			}
			else {
				spec[ion.getMass()][FRAGMENT_ION_POSITION] = ion.getMass();
				spec[ion.getMass()][FRAGMENT_ABS_POSITION] = ion.getAbsoluteIntensity();
				spec[ion.getMass()][FRAGMENT_REL_POSITION] = ion.getRelativeIntensity();
			}
		}

		return spec;
	}
}
