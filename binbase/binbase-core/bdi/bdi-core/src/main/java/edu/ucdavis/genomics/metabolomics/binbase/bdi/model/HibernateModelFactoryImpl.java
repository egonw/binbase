/*
 * Created on Dec 8, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.model;

import java.util.HashMap;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;

/**
 * provides us with the implementation of the hibernate persistence layer, every model is also only creted ones for a given type to make sure that we dont have
 * models doubled
 * 
 * @author wohlgemuth
 * @version Dec 9, 2005
 * 
 */
public class HibernateModelFactoryImpl extends ModelFactory {
	private Map<Class, IModel> map = new HashMap<Class, IModel>();

	public HibernateModelFactoryImpl() {
		super();
	}

	@Override
	public final synchronized IModel createModel(Class clazz, boolean forceNew) {
		IModel model;

		if (forceNew == false) {
			if (map.containsKey(clazz)) {
				logger.debug("used cached model for class: " + clazz);
				model = map.get(clazz);
			} else {
				logger.debug("create new model for class: " + clazz);
				model = new HibernateModel(HibernateFactory.newInstance());
				logger.debug("model successfully created");
				map.put(clazz, model);
			}

		} else {
			logger.debug("create EXPLICITE new model for class: " + clazz);
			model = new HibernateModel(HibernateFactory.newInstance());
		}

		logger.debug("returned model");
		return model;
	}
}
