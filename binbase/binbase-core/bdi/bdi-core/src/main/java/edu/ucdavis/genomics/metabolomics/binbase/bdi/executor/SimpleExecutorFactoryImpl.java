/*
 * Created on Jan 19, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.executor;

public class SimpleExecutorFactoryImpl extends ExecutorFactory{

	@Override
	public Execute create(ExecutionHandler handler) {
		return new SimpleExecutorImpl(handler);
	}

}
