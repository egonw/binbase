/*
 * Created on Jun 27, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import java.util.Collection;
import java.util.HashSet;

/**
 * @swt
 * @hibernate.class table = "BIN_GROUP" dynamic-insert = "true" dynamic-update =
 *                  "true"
 *                  
 * defines a group of identical bins. so that we can
 * merge them at the export to one bin
 * 
 * @author wohlgemuth
 * @version Jun 27, 2006
 * 
 */
public class BinGroup implements Comparable{

	
	public String toString() {
		return name;
	}

	private Collection bins;

	private Integer id;

	private String name;

	private String description;

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin"
	 * @hibernate.collection-key column = "group_id"
	 */
	public Collection getBins() {
		return bins;
	}

	public void setBins(Collection bins) {
		this.bins = bins;
	}

	/**
	 * @hibernate.id column = "group_id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 * 
	 * the internal id
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public void addBin(Bin bin){
		if(this.getBins() == null){
			this.setBins(new HashSet());
		}
		bin.setGroup(this);
		this.getBins().add(bin);
	}

	/**
	 * @swt.variable visible="true" name="Description" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "description" update = "true" insert = "true" not-null = "true"
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "name" update = "true" insert = "true" not-null = "true"
	 * @author wohlgemuth
	 * @version Jun 27, 2006
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int compareTo(Object arg0) {
		if (arg0 instanceof BinGroup) {
			BinGroup a = (BinGroup) arg0;
			return a.getName().compareTo(this.getName());
		}
		return this.toString().compareTo(arg0.toString());
	}
}
