package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.msp;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.ModelFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter;

/**
 * converts binbase massspec files into msp compatible files
 * 
 * @author wohlgemuth
 * 
 */
public class BinToMspConverter {

	/**
	 * converts to given spectra to a msp file
	 * 
	 * @param name
	 * @param spectra
	 * @return
	 */
	public static String convert(String name, String spectra, int retentionIndex) {
		return convert(null, name,
				SpectraConverter.stringToCollection(spectra), retentionIndex);
	}

	/**
	 * converts a bin to a spectra
	 * 
	 * @param bin
	 * @return
	 */
	public static String convertBinToMsp(Bin bin) {

		StringBuffer result = new StringBuffer();
		writeBinProperties(bin.getId(), bin.getName(), bin.getRetentionIndex(),
				result);

		result.append(convertSpectraToMspSpectra(bin.getSpectra()));
		return result.toString();
	}

	/**
	 * converts a bin to a spectra
	 * 
	 * @param bin
	 * @return
	 */
	public static String convertBinToMsp(int binId) {

		IModel model = ModelFactory.newInstance().createModel(Bin.class);
		model.setQuery("SELECT a FROM Bin a where a.id = " + binId);
		Object[] result = model.executeQuery();

		if (result.length > 0) {
			return convertBinToMsp((Bin) result[0]);
		} else {
			return "";
		}
	}

	/**
	 * converts a bin to a spectra
	 * 
	 * @param bin
	 * @return
	 * @throws IOException
	 */
	public static void convertBinsToMsp(int[] binId, File outputFile)
			throws IOException {

		FileWriter out = new FileWriter(outputFile);

		for (int i = 0; i < binId.length; i++) {
			String convert = convertBinToMsp(binId[i]);

			out.write(convert);
			out.write("\n\n");
			out.flush();
		}
		out.close();

	}

	/**
	 * converts the string to an msp file
	 * 
	 * @param binIdsSeperatedByComa
	 * @param outputFile
	 * @throws IOException
	 */
	public static void convertBinsToMsp(String binIdsSeperatedByComa,
			File outputFile, String seperator) throws IOException {

		String[] content = binIdsSeperatedByComa.split(seperator);
		int[] ids = new int[content.length];

		for (int i = 0; i < ids.length; i++) {
			ids[i] = Integer.parseInt(content[i]);
		}

		convertBinsToMsp(ids, outputFile);

	}

	public static String convertSpectraToMspSpectra(String toConvert) {
		return convertSpectraToMspSpectra(SpectraConverter
				.stringToCollection(toConvert));
	}

	public static String convertSpectraToMspSpectra(Collection<Ion> spectra) {

		StringBuffer result = new StringBuffer();

		result.append("Num Peaks: ");
		result.append(spectra.size());
		result.append("\n");

		Iterator<Ion> it = spectra.iterator();

		int count = 0;
		while (it.hasNext()) {
			Ion ion = it.next();
			count++;

			if (count == 6) {
				count = 0;
				result.append("\n");
			}

			result.append(ion.getMass());
			result.append("    ");
			result.append(ion.getAbsoluteIntensity());
			result.append(";");
		}

		return result.toString();
	}

	/**
	 * converts the given msp collection to a msp file
	 * 
	 * @param name
	 * @param spectra
	 * @return
	 */
	public static String convert(Integer id, String name, Collection spectra,
			int retentionIndex) {
		StringBuffer result = new StringBuffer();
		writeBinProperties(id, name, retentionIndex, result);

		result.append(convertSpectraToMspSpectra(spectra));
		return result.toString();
	}

	private static void writeBinProperties(Integer id, String name,
			int retentionIndex, StringBuffer result) {
		result.append("Name: ");
		result.append(name);
		result.append("\n");
		result.append("RI: ");
		result.append(retentionIndex);
		result.append("\n");

		if (id != null) {
			result.append("BinId: ");
			result.append(id);
			result.append("\n");
		}
	}

	/**
	 * converts all unknown bins to msp files and stores them into the specified
	 * output directory
	 * 
	 * @param outputDiretory
	 * @throws IOException
	 */
	public static void convertAllUnknownBinsToMsp(String outputDiretory)
			throws IOException {
		IModel model = ModelFactory.newInstance().createModel(Bin.class);
		model.setQuery("SELECT a FROM Bin a ORDER by a.id");
		Object[] result = model.executeQuery();

		for (int i = 0; i < result.length; i++) {
			Bin bin = (Bin) result[i];

			if (bin.getName().equals(String.valueOf(bin.getId())) == true) {
				String convert = convert(bin.getId(), bin.getName(),
						bin.getSpectra(), bin.getRetentionIndex());

				FileWriter out = new FileWriter(new File(outputDiretory
						+ File.separator + bin.getName() + ".msp"));

				out.write(convert);
				out.flush();
				out.close();

			}
		}
	}

	/**
	 * converts all knowns bin and stores the result in the given output
	 * directory
	 * 
	 * @param outputDiretory
	 * @throws IOException
	 */
	public static void convertAllKnownBinsToMsp(String outputDiretory)
			throws IOException {
		IModel model = ModelFactory.newInstance().createModel(Bin.class);
		model.setQuery("SELECT a FROM Bin a ORDER by a.id");
		Object[] result = model.executeQuery();

		for (int i = 0; i < result.length; i++) {
			Bin bin = (Bin) result[i];

			if (bin.getName().equals(String.valueOf(bin.getId())) == false) {
				String convert = convert(bin.getId(), bin.getName(),
						bin.getSpectra(), bin.getRetentionIndex());

				FileWriter out = new FileWriter(new File(outputDiretory
						+ File.separator + bin.getName() + ".msp"));

				out.write(convert);
				out.flush();
				out.close();

			}
		}
	}

	/**
	 * converts all knowns bin and stores the result in the given output
	 * directory
	 * 
	 * @param outputDiretory
	 * @throws IOException
	 */
	public static void convertAllKnownBinsToMsp(File outputFile)
			throws IOException {

		IModel model = ModelFactory.newInstance().createModel(Bin.class);
		model.setQuery("SELECT a FROM Bin a ORDER by a.id");
		Object[] result = model.executeQuery();

		FileWriter out = new FileWriter(outputFile);

		for (int i = 0; i < result.length; i++) {
			Bin bin = (Bin) result[i];

			if (bin.getName().equals(String.valueOf(bin.getId())) == false) {
				String convert = convert(bin.getId(), bin.getName(),
						bin.getSpectra(), bin.getRetentionIndex());

				out.write(convert);
				out.write("\n\n");
				out.flush();

			}
		}
		out.close();

	}

	public static void convertAllUnKnownBinsToMsp(File outputFile)
			throws IOException {

		IModel model = ModelFactory.newInstance().createModel(Bin.class);
		model.setQuery("SELECT a FROM Bin a ORDER by a.id");
		Object[] result = model.executeQuery();

		FileWriter out = new FileWriter(outputFile);

		for (int i = 0; i < result.length; i++) {
			Bin bin = (Bin) result[i];

			if (bin.getName().equals(String.valueOf(bin.getId())) == true) {
				String convert = convert(bin.getId(), bin.getName(),
						bin.getSpectra(), bin.getRetentionIndex());

				out.write(convert);
				out.write("\n\n");
				out.flush();

			}
		}
		out.close();

	}

	/**
	 * converts all knowns bin and stores the result in the given output
	 * directory
	 * 
	 * @param outputDiretory
	 * @throws IOException
	 */
	public static void convertAllBinsToMsp(File outputFile) throws IOException {

		IModel model = ModelFactory.newInstance().createModel(Bin.class);
		model.setQuery("SELECT a FROM Bin a ORDER by a.id");
		Object[] result = model.executeQuery();

		FileWriter out = new FileWriter(outputFile);

		for (int i = 0; i < result.length; i++) {
			Bin bin = (Bin) result[i];

			String convert = convert(bin.getId(), bin.getName(),
					bin.getSpectra(), bin.getRetentionIndex());

			out.write(convert);
			out.write("\n\n");
			out.flush();

		}
		out.close();

	}

	/**
	 * converts all knowns bin and stores the result in the given output
	 * directory
	 * 
	 * @param outputDiretory
	 * @throws IOException
	 */
	public static void convertAllBinsToMsp(String outputDiretory)
			throws IOException {
		IModel model = ModelFactory.newInstance().createModel(Bin.class);
		model.setQuery("SELECT a FROM Bin a ORDER by a.id");
		Object[] result = model.executeQuery();

		for (int i = 0; i < result.length; i++) {
			Bin bin = (Bin) result[i];

			String convert = convert(bin.getId(), bin.getName(),
					bin.getSpectra(), bin.getRetentionIndex());

			FileWriter out = new FileWriter(new File(outputDiretory
					+ File.separator + bin.getName() + ".msp"));

			out.write(convert);
			out.flush();
			out.close();

		}
	}

	public static void main(String[] args) throws IOException {
		convertAllUnknownBinsToMsp("/home/wohlgemuth/msp/unknown");
		convertAllKnownBinsToMsp("/home/wohlgemuth/msp/known");
	}
}
