package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import java.io.OutputStream;
import java.sql.Connection;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.ModelFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.math.Similarity;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.BinObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;

public class BinComparisonMatrix extends AbstractBinTable {

	Logger logger = Logger.getLogger(getClass());

	/**
	 * creates a matrix for the given bins
	 * 
	 * @param retentionIndexWindow
	 * @param minimumSimilarity
	 * @param bins
	 * @return
	 */
	public DataFile createMatrix(int retentionIndexWindow,
			double minimumSimilarity, Set<Integer> bins, Connection connection) {

		Collection<Bin> newBins = new Vector<Bin>();
		Collection<Bin> compareBins = new Vector<Bin>();

		for (Integer in : bins) {

			IModel model = ModelFactory.newInstance().createModel(Bin.class);
			model.setQuery("SELECT a FROM Bin a where a.id = " + in.intValue());
			Object[] result = model.executeQuery();

			if (result.length > 0) {
				Bin bin = (Bin) result[0];
				newBins.add(bin);
				compareBins.add(bin);
			}
		}

		return createMatrix(retentionIndexWindow, minimumSimilarity, newBins,
				compareBins);

	}

	/**
	 * creates a matrix for the given bins
	 * 
	 * @param retentionIndexWindow
	 * @param minimumSimilarity
	 * @param bins
	 * @return
	 */
	public DataFile createMatrix(int retentionIndexWindow,
			double minimumSimilarity, Set<Integer> bins) {

		Collection<Bin> newBins = new Vector<Bin>();
		Collection<Bin> compareBins = new Vector<Bin>();

		for (Integer in : bins) {

			IModel model = ModelFactory.newInstance().createModel(Bin.class);
			model.setQuery("SELECT a FROM Bin a where a.id = " + in.intValue());
			Object[] result = model.executeQuery();

			if (result.length > 0) {
				Bin bin = (Bin) result[0];
				newBins.add(bin);
				compareBins.add(bin);
			}
		}

		return createMatrix(retentionIndexWindow, minimumSimilarity, newBins,
				compareBins);

	}

	/**
	 * creates a matrix defined by the given windows and bins
	 * 
	 * @param retentionIndexWindow
	 * @param minimumSimilarity
	 * @return
	 */
	public DataFile createMatrix(int retentionIndexWindow,
			double minimumSimilarity, Collection<Bin> bins,
			Collection<Bin> compare) {

		DataFile result = new SimpleDatafile();

		Similarity similarity = new Similarity();

		Set<String> hitCache = new HashSet<String>();

		generateHeader(result);

		for (Bin bin : bins) {

			logger.info("find similar bins for bin: " + bin.getName());
			similarity.setLibrarySpectra(bin.getMassSpec());

			int begin = bin.getRetentionIndex() - retentionIndexWindow;
			int end = bin.getRetentionIndex() + retentionIndexWindow;

			logger.info("looking in range (" + begin + " - " + end + ")");

			for (Bin hit : compare) {

				if (hit.getId().equals(bin.getId()) == false) {

					if (hit.getRetentionIndex() > begin
							&& hit.getRetentionIndex() < end) {

						if (hitCache.contains(hit.getId() + "_" + bin.getId()) == false) {
							hitCache.add(hit.getId() + "_" + bin.getId());

							similarity.setUnknownSpectra(hit.getMassSpec());

							double value = similarity.calculateSimimlarity();

							logger.info("similarity is (" + value + ")");
							if (value >= minimumSimilarity) {
								logger.info("accepted and add to result: "
										+ hit.getName());
								generateEntry(bin, hit, result, value);
							}
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * creates a matrix defined by the given windows.
	 * 
	 * @param retentionIndexWindow
	 * @param minimumSimilarity
	 * @return
	 */
	public DataFile createMatrix(int retentionIndexWindow,
			double minimumSimilarity) {

		logger.info("loading bins...");
		Collection<Bin> bins = getBins();
		logger.info("loaded: " + bins.size());

		logger.info("loading bins to compare...");
		Collection<Bin> compare = getBins();
		logger.info("loaded: " + compare.size());

		return createMatrix(retentionIndexWindow, minimumSimilarity, bins,
				compare);
	}

	/**
	 * add the generated entry
	 * 
	 * @param bin
	 * @param hit
	 */
	private void generateEntry(Bin bin, Bin hit, DataFile result,
			double similarity) {
		if (bin.getId() != hit.getId()) {

			List<FormatObject<?>> row = new Vector<FormatObject<?>>();

			Sample binSample = null;
			Sample hitSample = null;
			{
				IModel model = ModelFactory.newInstance().createModel(
						Sample.class);
				model.setQuery("SELECT a FROM Sample a where a.name = '"
						+ bin.getSample().getName() + "'");
				binSample = (Sample) model.executeQuery()[0];
			}
			{

				IModel model = ModelFactory.newInstance().createModel(
						Sample.class);
				model.setQuery("SELECT a FROM Sample a where a.name = '"
						+ hit.getSample().getName() + "'");
				hitSample = (Sample) model.executeQuery()[0];
			}

			row.add(new BinObject<String>(bin.getName()));
			row.add(new ContentObject<String>(hit.getName()));
			row.add(new ContentObject<Integer>(bin.getRetentionIndex()
					- hit.getRetentionIndex()));
			row.add(new ContentObject<Integer>(Math.abs(bin.getRetentionIndex()
					- hit.getRetentionIndex())));

			row.add(new BinObject<Integer>(bin.getRetentionIndex()));
			row.add(new ContentObject<Integer>(hit.getRetentionIndex()));
			row.add(new ContentObject<Double>(similarity));
			row.add(new BinObject<Integer>(bin.getQuantMass()));
			row.add(new BinObject<Integer>(bin.getUniqueMass()));
			row.add(new ContentObject<Integer>(hit.getUniqueMass()));

			row.add(new ContentObject<Boolean>(hit.getUniqueMass().equals(
					bin.getUniqueMass())));

			List<Ion> apexingMasses = hit.getApex();

			boolean contains = false;
			for (int i = 0; i < apexingMasses.size(); i++) {
				Ion ion = apexingMasses.get(i);

				if (ion.getMass() == bin.getUniqueMass()) {
					contains = true;
					i = apexingMasses.size();
				}
			}
			row.add(new ContentObject<Boolean>(contains));

			apexingMasses = bin.getApex();

			boolean contains2 = false;
			for (int i = 0; i < apexingMasses.size(); i++) {
				Ion ion = apexingMasses.get(i);

				if (ion.getMass() == hit.getUniqueMass()) {
					contains2 = true;
					i = apexingMasses.size();
				}
			}
			row.add(new ContentObject<Boolean>(contains2));

			if (hit.getGroup() == null) {
				row.add(new ContentObject<String>("no group defined"));
			} else {
				row.add(new ContentObject<String>(hit.getGroup().getName()));
			}

			if (bin.getGroup() == null) {
				row.add(new BinObject<String>("no group defined"));
			} else {
				row.add(new BinObject<String>(bin.getGroup().getName()));
			}

			row.add(new ContentObject<Integer>(hit.getId()));

			row.add(new BinObject<Integer>(bin.getId()));
			row.add(new BinObject<String>(bin.getSample().getName()));
			row.add(new HeaderFormat<String>(hit.getSample().getName()));

			row.add(new BinObject<Integer>(bin.getSpectraId()));
			row.add(new BinObject<Integer>(hit.getSpectraId()));

			row.add(new BinObject<String>(binSample.getExperiment().getName()));
			row.add(new BinObject<String>(hitSample.getExperiment().getName()));

			result.addRow(row);
			logger.info("row is added");

		}

	}

	/**
	 * defines the header
	 * 
	 * @param result
	 */
	private void generateHeader(DataFile result) {
		List<HeaderFormat<String>> headers = new Vector<HeaderFormat<String>>();

		headers.add(new HeaderFormat<String>("bin"));
		headers.add(new HeaderFormat<String>("compareTo"));
		headers.add(new HeaderFormat<String>("retention index difference"));
		headers.add(new HeaderFormat<String>("retention index difference (abs)"));

		headers.add(new HeaderFormat<String>("retention index bin"));
		headers.add(new HeaderFormat<String>("retention index compareTo"));
		headers.add(new HeaderFormat<String>("similarity score"));
		headers.add(new HeaderFormat<String>("quant ion"));
		headers.add(new HeaderFormat<String>("unique ion"));
		headers.add(new HeaderFormat<String>("unique ion compareTo"));
		headers.add(new HeaderFormat<String>("unique ion identical"));
		headers.add(new HeaderFormat<String>(
				"unique ion in compareTo bins apex masses"));
		headers.add(new HeaderFormat<String>(
				"compareTo unique ion in bins apex masses"));

		headers.add(new HeaderFormat<String>("group"));
		headers.add(new HeaderFormat<String>("group compareTo"));

		headers.add(new HeaderFormat<String>("id compareTo"));
		headers.add(new HeaderFormat<String>("id"));
		headers.add(new HeaderFormat<String>("sample bin"));
		headers.add(new HeaderFormat<String>("sample compareTo"));

		headers.add(new HeaderFormat<String>("spectra id bin"));

		headers.add(new HeaderFormat<String>("spectra id compareTo"));

		headers.add(new HeaderFormat<String>("class bin"));

		headers.add(new HeaderFormat<String>("class compareTo"));

		logger.info("adding defiend header!");
		result.addRow(headers);
	}

	public static void main(String args[]) throws Exception {
		if (args.length != 5) {
			System.out.println("USAGE: ");
			System.out.println("args[0]: database server");
			System.out.println("args[1]: username");
			System.out.println("args[2]: password");
			System.out.println("args[3]: database name");
			System.out.println("args[4]: directory");

			System.out.println("found amount of attributes: " + args.length);
			return;
		}

		System.setProperty(ConnectionFactory.KEY_USERNAME_PROPERTIE, args[1]);
		System.setProperty(ConnectionFactory.KEY_HOST_PROPERTIE, args[0]);
		System.setProperty(ConnectionFactory.KEY_PASSWORD_PROPERTIE, args[2]);
		System.setProperty(ConnectionFactory.KEY_DATABASE_PROPERTIE, args[3]);

		if (System.getProperty(ConnectionFactory.KEY_TYPE_PROPERTIE) == null) {
			System.setProperty(ConnectionFactory.KEY_TYPE_PROPERTIE,
					String.valueOf(DriverUtilities.POSTGRES));
		}
		XMLConfigurator.getInstance().addConfiguration(
				new ResourceSource("/config/hibernate.xml"));

		BinComparisonMatrix matrix = new BinComparisonMatrix();
		DataFile data = matrix.createMatrix(5000, 800);

		FileDestination dest = new FileDestination(args[4]);
		dest.setIdentifier("comparisonMatrix.txt");
		OutputStream out = dest.getOutputStream();
		data.write(out);
		out.flush();
		out.close();
	}
}
