package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import java.util.Collection;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;

/**
 * simple class for table generation
 * 
 * @author nase
 * 
 */
public class AbstractBinTable {

	private Logger logger = Logger.getLogger(getClass());
	/**
	 * provides us with a session
	 * 
	 * @return
	 */
	public Session getSession() {
		return HibernateFactory.newInstance().getSession();
	}

	@SuppressWarnings("unchecked")
	public Collection<Bin> getBins() {
		return getSession().createCriteria(Bin.class).addOrder(Order.asc("id"))
				.list();
	}

	/**
	 * gives us the list of known bins
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<Bin> getKnownBins() {
		Session session = getSession();

		Collection<Bin> bins = session.createCriteria(Bin.class).addOrder(
				Order.asc("id")).list();

		Collection<Bin> ofInterrested = new Vector<Bin>();

		for (Bin bin : bins) {
			// check if its an unknown
			if (bin.getName().equals(bin.getId().toString()) == false) {
				ofInterrested.add(bin);
			}
		}
		return ofInterrested;
	}

	public Bin getBin(int id){
		logger.info("loading bin: " + id);
		Session session = getSession();
		
		return (Bin) session.load(Bin.class, id);
	}
	/**
	 * gives us the list of unknown bins
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<Bin> getUnKnownBins() {
		Session session = getSession();

		Collection<Bin> bins = session.createCriteria(Bin.class).addOrder(
				Order.asc("id")).list();

		Collection<Bin> ofInterrested = new Vector<Bin>();

		for (Bin bin : bins) {
			// check if its an unknown
			if (bin.getName().equals(bin.getId().toString()) == true) {
				ofInterrested.add(bin);
			}
		}
		return ofInterrested;
	}

}
