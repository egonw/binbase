package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample;


/**
 * @swt
 * @author wohlgemuth defines a type
 * @hibernate.class table = "TYPE" dynamic-insert = "true" dynamic-update =
 *                  "true" discriminator-value = "0"
 * @hibernate.discriminator column = "name"
 */
public class Type {
	private Integer id;
	
	private String description;

	private String name;

	private String pattern;

	/**
	 * @swt.variable visible="true" name="Description" searchable="true"
	 * @swt.modify canModify="true"
	 * 
	 * @hibernate.property column = "description" insert = "true" update =
	 *                     "true"
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="false"
	 * 
	 * @hibernate.property column = "name" insert = "false" update =
	 *                     "false"
	 */
	public String getName() {
		return name;
	}


	/**
	 * @swt.variable visible="true" name="Pattern" searchable="true"
	 * @swt.modify canModify="true"
	 * 
	 * @hibernate.property column = "pattern" insert = "true" update =
	 *                     "true" not-null = "true"
	 */
	public String getPattern() {
		return pattern;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * @hibernate.id column = "id" generator-class = "native"
     * @hibernate.generator-param name = "sequence" value = "TYPE_ID"
	 * the internal id
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
