/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.exception;

/**
 * @author wohlgemuth
 *
 */
public class FormatException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * 
	 */
	public FormatException() {
		super();
		
	}

	/**
	 * @param message
	 */
	public FormatException(String message) {
		super(message);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public FormatException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param cause
	 */
	public FormatException(Throwable cause) {
		super(cause);
		
	}

}
