/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types;

import java.util.Collection;

/**
 * @author wohlgemuth
 *
 */
public interface Commentable {
	/**
	 * returns all comments to this massspec
	 * @return
	 */
	public Collection getComments();

	/**
	 * sets comments
	 * @param comments
	 */
	public void setComments(Collection comments);
	
	/**
	 * adds a new comment
	 * @param comment
	 */
	public void addComment(Comment comment);
	
	/**
	 * returns the id of this commentable
	 * @return
	 */
	public Integer getId();
	
	/**
	 * creates the correct implementation for a comment
	 * @return
	 */
	public Comment createComment();
}
