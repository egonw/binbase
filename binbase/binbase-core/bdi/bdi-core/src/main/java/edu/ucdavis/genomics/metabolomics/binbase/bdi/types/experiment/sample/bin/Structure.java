/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import java.util.Collection;
import java.util.HashSet;

/**
 * @swt
 * @hibernate.class table = "STRUCTURE"
 * @author wohlgemuth defines structural properties of a bin
 */
public class Structure {

	private Collection classification;

	/**
	 * structure formula
	 */
	private String formula;

	/**
	 * refrence id
	 */
	private Integer id;

	/**
	 * molare mass
	 */
	private Double molareMass;

	/**
	 * smiles code
	 */
	private String smile;

	/**
	 * 
	 */
	public Structure() {
		super();
	}

	/**
	 * @swt.variable visible="true" name="Formula" searchable="true"
	 * @swt.modify canModify="true" 
	 * @hibernate.property column = "formula" update = "true" insert = "true"
	 *                     not-null = "false"
	 * @return
	 */
	public String getFormula() {
		return this.formula;
	}

	/**
	 * @hibernate.id column = "id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 * @return
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @swt.variable visible="true" name="Molare Mass" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "molare_mass" update = "true" insert = "true"
	 *                     not-null = "false"
	 * @return
	 */
	public Double getMolareMass() {
		return this.molareMass;
	}

	/**
	 * @swt.variable visible="true" name="Smile" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "smiles" update = "true" insert = "true"
	 *                     not-null = "false"
	 * @return
	 */
	public String getSmile() {
		return this.smile;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setMolareMass(Double molareMass) {
		this.molareMass = molareMass;
	}

	public void setSmile(String smile) {
		this.smile = smile;
	}

	/**
	 * adds a new substanceclass and should be the only way to add substance classes
	 * @param clazz
	 */
	public void addSubstanceClass(SubstanceClass clazz){
		Classification classification = new Classification();
		classification.setStructure(this);
		classification.setSubstanceClass(clazz);
		this.addClassification(classification);
	}

	/**
	 * for internal use only stores a new classification
	 * @param classification
	 */
	protected void addClassification(Classification classification){
		if(this.getClassification() == null){
			this.setClassification(new HashSet());
		}
		this.getClassification().add(classification);
	}
	
	/**
	 * gets all classifications
	 * @hibernate.set lazy="true" cascade = "none" 
     * @hibernate.collection-one-to-many class =
     *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Classification" 
     * @hibernate.collection-key column = "id"
	 * @return
	 */
	public Collection getClassification() {
		return this.classification;
	}

	/**
	 * sets a new classification
	 * @param classification
	 */
	public void setClassification(Collection classification) {
		this.classification = classification;
	}
}
