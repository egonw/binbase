/*
 * Created on 20.07.2004
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.msp;

import java.util.Properties;

import javax.naming.Context;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.ModelFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.Library;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.LibraryMassSpec;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter;
import edu.ucdavis.genomics.metabolomics.sjp.ParserHandler;
import edu.ucdavis.genomics.metabolomics.sjp.exception.ParserException;

/**
 * @author ibm user
 */
public class HibernateDabaseResultHandler extends
		edu.ucdavis.genomics.metabolomics.util.BasicObject implements
		ParserHandler {
	/**
	 * DOCUMENT ME!
	 */
	public static final String FACTORY = Context.INITIAL_CONTEXT_FACTORY;

	/**
	 * DOCUMENT ME!
	 */
	public static final String PREFIXES = Context.URL_PKG_PREFIXES;

	/**
	 * DOCUMENT ME!
	 */
	public static final String PROVIDER = Context.PROVIDER_URL;

	/**
	 * DOCUMENT ME!
	 */
	public static final String LIBRARY = "LIBRARY";

	/**
	 * DOCUMENT ME!
	 * 
	 * @uml.property name="library"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private Library library;

	/**
	 * DOCUMENT ME!
	 * 
	 * @uml.property name="spectra"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private LibraryMassSpec spectra;

	/**
	 * DOCUMENT ME!
	 */
	private Logger logger = Logger
			.getLogger(HibernateDabaseResultHandler.class);

	/**
	 * DOCUMENT ME!
	 */
	private Properties p;

	public HibernateDabaseResultHandler() throws ParserException {
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.nistms.MSPParserHandler#setProperties(java.util.Properties)
	 */
	public void setProperties(Properties p) throws ParserException {
		try {
			this.p = p;
		} catch (Exception e) {
			throw new ParserException(e);
		}
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.sjp.ParserHandler#endAttribute(java.lang.String,
	 *      java.lang.String)
	 */
	public void endAttribute(String element, String name)
			throws ParserException {
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.sjp.ParserHandler#endDataSet()
	 */
	public void endDataSet() throws ParserException {
		try {
			if (spectra.getName() != null && spectra.getName().length() > 0) {
				spectra.setLibrary(library);
				ModelFactory.newInstance().createModel(LibraryMassSpec.class)
						.add(spectra);
				library.addSpectra(spectra);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.sjp.ParserHandler#endDocument()
	 */
	public void endDocument() throws ParserException {
		ModelFactory.newInstance().createModel(Library.class)
				.update(this.library);
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.sjp.ParserHandler#endElement(java.lang.String)
	 */
	public void endElement(String name) throws ParserException {
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.sjp.ParserHandler#startAttribute(java.lang.String,
	 *      java.lang.String, java.lang.String)
	 */
	public void startAttribute(String element, String name, String value)
			throws ParserException {
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.sjp.ParserHandler#startDataSet()
	 */
	public void startDataSet() throws ParserException {
		spectra = new LibraryMassSpec();
		logger.debug("add spectra " + spectra.getName());
		spectra.setLibrary(this.library);
		spectra.setRetentionIndex(0);

		ModelFactory.newInstance().createModel(Library.class).add(this.library);
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.sjp.ParserHandler#startDocument()
	 */
	public void startDocument() throws ParserException {
		try {
			library = new Library();
			this.library.setName(p.getProperty(LIBRARY));
			this.library.setDescription("importet with " + this.getClass());
		} catch (Exception e) {
			throw new ParserException(e);
		}
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.nistms.MSPParserHaOsndler#attributeFound(java.lang.String,
	 *      java.lang.String)
	 */
	public void startElement(String name, String value) throws ParserException {
		logger.debug(name + " - " + value);
		if (name.equals("Name".toLowerCase())) {

			if (value.endsWith("_")) {
				value = value.substring(0, value.indexOf("_")).trim();
			}

			logger.info("set as name: " + value);

			spectra.setName(value);

			if (value.contains("_RI ")) {
				try {
					spectra.setRetentionIndex(Integer.parseInt(value
							.split("_RI")[1].trim()));
				} catch (Exception e) {
					logger.warn(e.getMessage(), e);
					spectra.setRetentionIndex(0);
				}
			}
		} else if (name.toLowerCase().equals("ri")) {

			try {
				int retentionIndex = Integer.parseInt(value);
				spectra.setRetentionIndex(retentionIndex);
			} catch (Exception e) {
				logger.warn(e.getMessage(), e);
				logger.info("set as ri");
				spectra.setRetentionIndex(0);
				spectra.setName(spectra.getName() + "RI " + value);

			}
		} else if (name.equals("spectra".toLowerCase())) {
			logger.info("set as spectra");
			spectra.setSpectra(SpectraConverter.stringToCollection(value));
		} else {
			logger.info("set as comment: " + name + " - " + value);
			Comment comment = spectra.createComment();
			comment.setText(name + " - " + value);
			comment.setType(spectra);
			spectra.addComment(comment);
		}
	}
}
