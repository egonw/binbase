package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import java.util.Collection;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;

/**
 * exports the bin table into a datafile with all it columns
 * 
 * @author wohlgemuth
 * 
 */
public class BinTable extends AbstractBinTable{

	@SuppressWarnings("unchecked")
	public DataFile createTable() {
		DataFile result = new SimpleDatafile();

		Collection<Bin> bins = getBins();

		result.setDimension(bins.size() + 1, 8);

		// set the heade row
		result.setCell(0, 0, "name");
		result.setCell(1, 0, "retention index");
		result.setCell(2, 0, "id");
		result.setCell(3, 0, "quant mass");
		result.setCell(4, 0, "unique mass");
		result.setCell(5, 0, "export");
		result.setCell(6, 0, "created by sample");
		result.setCell(7, 0, "massspec");

		//write the data out
		int row = 1;
		for (Bin bin : bins) {

			result.setCell(0, row, bin.getName());
			result.setCell(1, row, bin.getRetentionIndex());
			result.setCell(2, row, bin.getId());
			result.setCell(3, row, bin.getQuantMass());
			result.setCell(4, row, bin.getUniqueMass());
			result.setCell(5, row, bin.getExportString());
			result.setCell(6, row, bin.getSample().getName());
			result.setCell(7, row, SpectraConverter.collectionToString(bin
					.getSpectra()));

			row++;
		}
		return result;
	}
}
