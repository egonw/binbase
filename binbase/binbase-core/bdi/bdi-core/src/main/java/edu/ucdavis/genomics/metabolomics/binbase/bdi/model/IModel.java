package edu.ucdavis.genomics.metabolomics.binbase.bdi.model;

import java.io.Serializable;
import java.util.Collection;


/**
 * basic model needed for all query operations and uses somehow hibernate as implementatios
 * 
 * @author wohlgemuth
 * 
 */
public interface IModel<Type> {
	/**
	 * execute the given query
	 */
	public Type[] executeQuery();

	/**
	 * executes an internal query without calling the listeners for a query change
	 * @author wohlgemuth
	 * @version Nov 13, 2006
	 * @param o
	 * @return
	 */
	public Type[] executeInternalQuery(Object o);

	/**
	 * sets a criteria based query
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @param criteria
	 */
	public void setQuery(Object querry);

	/**
	 * adds a object of type
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @param object
	 */
	public void add(Type object);

	/**
	 * deltes an object of type
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @param object
	 */
	public void delete(Type object);

	/**
	 * updates an object of type
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @param object
	 */
	public void update(Type object);

	/**
	 * adds a changelistener to this model
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @param listener
	 */
	public void addChangeListener(ChangeListener listener);

	/**
	 * removes the change listener
	 * 
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @param listener
	 */
	public void removeChangeListener(ChangeListener listener);
	
	/**
	 * returns the change listener
	 * @author wohlgemuth
	 * @version Dec 16, 2005
	 * @return
	 */
	@SuppressWarnings("hiding")
	public <ChangeListener>Collection getChangeListener();

	/**
	 * adds the progress listener
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 * @param listener
	 */
	public void addProgressListener(ProgressListener listener);
	
	/**
	 * remove a progress listener
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 * @param listener
	 */
	public void removeProgressListener(ProgressListener listener);
	
	/**
	 * get all progress listener
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 * @param <ProgressListener>
	 * @return
	 */
	@SuppressWarnings("hiding")
	public <ProgressListener>Collection getProgressListener();
	
	/**
	 * reloads the given object
	 * @author wohlgemuth
	 * @version Nov 13, 2006
	 */
	public void reload(Type type);
	
	/**
	 * runs the same query again
	 */
	public void refreshQuery();
	
	/**
	 * possible owner of this model
	 * @param o
	 */
	public void setOwner(Object o);
	
	public Type load(Class<Type> clazz,Serializable key);
}
