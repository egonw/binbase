/*
 * Created on Jan 11, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.GenericSample;

public abstract class SimpleSpectra extends MassSpec{

	public abstract Integer getRetentionTime() ;
	
	public abstract Integer getShift();
	
	public abstract Double getPurity();
	
	public abstract GenericSample getSample();
}
