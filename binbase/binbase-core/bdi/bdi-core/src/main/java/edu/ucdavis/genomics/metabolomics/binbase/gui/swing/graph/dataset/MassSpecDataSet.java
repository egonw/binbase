/*
 * Created on 06.04.2004
 */
package edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.dataset;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.util.graph.dataset.AbstractDataset;
import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet;

/**
 * @author wohlgemuth
 */
public class MassSpecDataSet extends BinBaseXYDataSet implements AbstractDataset{

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 1L;

	boolean absolute = false;

	boolean apexMassesOnly = false;

	boolean highest20MassesOnly = false;

	/**
	 * DOCUMENT ME!
	 */
	private HashMap map = new HashMap();

	/**
	 * @param absolute
	 *            The absolute to set.
	 * 
	 * @uml.property name="absolute"
	 */
	public void setAbsolute(boolean absolute) {
		this.absolute = absolute;
	}

	/**
	 * @return Returns the absolute.
	 * 
	 * @uml.property name="absolute"
	 */
	public boolean isAbsolute() {
		return absolute;
	}

	/**
	 * @param apexMassesOnly
	 *            The apexMassesOnly to set.
	 * 
	 * @uml.property name="apexMassesOnly"
	 */
	public void setApexMassesOnly(boolean value) {
		this.apexMassesOnly = value;
	}

	/**
	 * @return Returns the apexMassesOnly.
	 * 
	 * @uml.property name="apexMassesOnly"
	 */
	public boolean isApexMassesOnly() {
		return apexMassesOnly;
	}

	/**
	 * @param highest20MassesOnly
	 *            The highest20MassesOnly to set.
	 * 
	 * @uml.property name="highest20MassesOnly"
	 */
	public void setHighest20MassesOnly(boolean value) {
		this.highest20MassesOnly = value;
	}

	/**
	 * @return Returns the highest20MassesOnly.
	 * 
	 * @uml.property name="highest20MassesOnly"
	 */
	public boolean isHighest20MassesOnly() {
		return highest20MassesOnly;
	}

	/**
	 * @param spectra
	 */
	public void addMassSpec(MassSpec spectra) {
		Collection massSpec;

		if (this.isApexMassesOnly()) {
			massSpec = spectra.getApex();
		} else if (this.isHighest20MassesOnly()) {
			massSpec = spectra.getMostAbundant();
		} else {
			massSpec = spectra.getSpectra();
		}

		double[] x = new double[massSpec.size()];
		double[] y = new double[massSpec.size()];

		Iterator it = massSpec.iterator();
		int i = 0;

		while (it.hasNext()) {
			Ion ion = (Ion) it.next();
			x[i] = ion.getMass();

			if (isAbsolute() == false) {
				y[i] = ion.getRelativeIntensity();
			} else {
				y[i] = ion.getAbsoluteIntensity();
			}

			i++;
		}

		this.addDataSet(x, y, String.valueOf(spectra.getId()));
		map.put(new Integer(this.getIndex(String.valueOf(spectra.getId()))),
				spectra);
	}

	/*
	 * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.graph.dataset.BinBaseXYDataSet#clear()
	 */
	public void clear() {
		super.clear();
		map.clear();
	}

	/*
	 * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.graph.dataset.BinBaseXYDataSet#clear(int)
	 */
	public void clear(int index) {
		super.clear(index);
		map.remove(new Integer(index));
	}

	/*
	 * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.graph.dataset.BinBaseXYDataSet#refresh()
	 */
	public void refresh() {
		Collection set = map.values();
		List temp = new Vector();
		Iterator it = set.iterator();

		while (it.hasNext()) {
			temp.add(it.next());
		}

		this.clear();

		for (int i = 0; i < temp.size(); i++) {
			System.err.println(temp.get(i).getClass());
			this.addMassSpec((MassSpec) temp.get(i));
		}
	}

	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Comparable getSeriesKey(int arg0) {
		System.err.println("get series TODO!!!!");
		return arg0;
	}
}
