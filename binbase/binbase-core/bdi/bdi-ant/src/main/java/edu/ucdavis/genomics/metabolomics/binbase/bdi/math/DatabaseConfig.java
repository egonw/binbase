package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

/**
 * simple configuration file
 * 
 * @author nase
 * 
 */
public class DatabaseConfig {

	private String database;

	private String user;

	private String password;

	private String type;

	private String host;

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
}
