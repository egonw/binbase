package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.tools.ant.Project;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.test.BinBaseDatabaseTest;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;

/**
 * tests our similarity task
 * 
 * @author nase
 * 
 */
public class CreateBinSimilarityMatrixTaskTest extends BinBaseDatabaseTest {

	CreateBinSimilarityMatrixTask task;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		task = new CreateBinSimilarityMatrixTask();
		task.setProject(new Project());

		File f = new File("target/content/result/");
		f.mkdirs();
		// simple cleanup
		for (File file : f.listFiles()) {
			file.delete();
		}

	}

	@After
	public void tearDown() throws Exception {
		task = null;
		HibernateFactory.newInstance().destroySession();

		super.tearDown();
	}

	/**
	 * simulates the configuration
	 * 
	 * @return
	 */
	protected DatabaseConfig createConfig() {
		DatabaseConfig config = new DatabaseConfig();

		config.setDatabase(System
				.getProperty(ConnectionFactory.KEY_DATABASE_PROPERTIE));
		config
				.setHost(System
						.getProperty(ConnectionFactory.KEY_HOST_PROPERTIE));
		config.setPassword(System
				.getProperty(ConnectionFactory.KEY_PASSWORD_PROPERTIE));
		config.setUser(System
				.getProperty(ConnectionFactory.KEY_USERNAME_PROPERTIE));
		config
				.setType(System
						.getProperty(ConnectionFactory.KEY_TYPE_PROPERTIE));

		return config;
	}

	@Test(timeout=60000)
	public void testFormating() throws FileNotFoundException, IOException {
		DatabaseConfig config = createConfig();

		task.addConfig(config);
		task.setOutputDirectory("target/content/result/");
		task.setLibraryToCompare("example");

		task.setIncludeBinProperties(false);
		task.setIncludeBinRefrences(false);
		task.setCreateKnownVsKnownMatrix(true);
		task.setCreateUnKnownVsUnKnownMatrix(false);
		task.setCreateKnownVsUnKnownMatrix(false);

		// since a similarity is always between 0 and 1000 the result has to be
		// a length of 4 (0.00) and 6 (999.99) or 6 (1000.0)
		task.setSimilarityFormat("#0.00");
		task.execute();

		SimpleDatafile file = new SimpleDatafile();
		file.read(new FileInputStream(
				"target/content/result/matrix-knownVSknown.txt"));

		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < file.getColumnCount(); i++) {
			if (i > 0) {
				for (int x = 0; x < file.getRowCount(); x++) {
					if (x > 0) {
						String value = file.getCell(i, x).toString();
						assertTrue(value.length() >= 4);
						assertTrue(value.length() <= 7);
						if(value.length() < min){
							min = value.length();
						}
						if(value.length() > max){
							max = value.length();
						}
						
					}
				}
			}
		}
		
		//the text needs to be in this range
		assertTrue(min >= 4);
		assertTrue(max == 6);
		
		

		// since a similarity is always between 0 and 1000 the result has to be
		// a length of 6 (0.0000) and 8 (999.9999) or 6 (1000.0)
		task.setSimilarityFormat("#0.0000");
		task.execute();

		file = new SimpleDatafile();
		file.read(new FileInputStream(
				"target/content/result/matrix-knownVSknown.txt"));

		min = Integer.MAX_VALUE;
		max = Integer.MIN_VALUE;
		for (int i = 0; i < file.getColumnCount(); i++) {
			if (i > 0) {
				for (int x = 0; x < file.getRowCount(); x++) {
					if (x > 0) {
						String value = file.getCell(i, x).toString();
						assertTrue(value.length() >= 6);
						assertTrue(value.length() <= 8);
						if(value.length() < min){
							min = value.length();
						}
						if(value.length() > max){
							max = value.length();
						}
						
					}
				}
			}
		}
		
		//the text needs to be in this range
		assertTrue(min >= 6);
		assertTrue(max == 8);
	}

	@Test(timeout=60000)
	public void testCompleteExecute() {
		DatabaseConfig config = createConfig();

		task.addConfig(config);
		task.setOutputDirectory("target/content/result/");
		task.setLibraryToCompare("example");

		task.setIncludeBinProperties(true);
		task.setIncludeBinRefrences(true);
		task.setCreateKnownVsKnownMatrix(true);
		task.setCreateUnKnownVsUnKnownMatrix(true);
		task.setCreateKnownVsUnKnownMatrix(true);
		task.setCreateMspVsMsp(true);

		task.execute();

		// we should have now 6 files in the output directory
		assertTrue(new File("target/content/result/matrix.txt").exists());
		assertTrue(new File("target/content/result/properties.txt").exists());
		assertTrue(new File("target/content/result/references.txt").exists());
		assertTrue(new File("target/content/result/matrix-knownVSknown.txt")
				.exists());
		assertTrue(new File("target/content/result/matrix-unknownVSunknown.txt")
				.exists());
		assertTrue(new File("target/content/result/matrix-knownVSunknown.txt")
				.exists());
		assertTrue(new File("target/content/result/matrix-mspVSmsp.txt")
		.exists());

		// validation of files is not neccessaery, since know that this works
		// based on the other tests
	}

	@Test(timeout=60000)
	public void testPropertyExecute() {
		DatabaseConfig config = createConfig();

		task.addConfig(config);
		task.setOutputDirectory("target/content/result/");
		task.setLibraryToCompare("example");

		task.setIncludeBinProperties(true);
		task.setIncludeBinRefrences(false);
		task.execute();

		// only matrix + property file
		assertTrue(new File("target/content/result/matrix.txt").exists());
		assertTrue(new File("target/content/result/properties.txt").exists());
		assertFalse(new File("target/content/result/references.txt").exists());

		// validation of files is not neccessaery, since know that this works
		// based on the other tests
	}

	@Test(timeout=60000)
	public void testRefrenceExecute() {
		DatabaseConfig config = createConfig();

		task.addConfig(config);
		task.setOutputDirectory("target/content/result/");
		task.setLibraryToCompare("example");

		task.setIncludeBinProperties(false);
		task.setIncludeBinRefrences(true);
		task.execute();

		// only matrix + reference file
		assertTrue(new File("target/content/result/matrix.txt").exists());
		assertFalse(new File("target/content/result/properties.txt").exists());
		assertTrue(new File("target/content/result/references.txt").exists());

		// validation of files is not neccessaery, since know that this works
		// based on the other tests
	}

	@Test(timeout=60000)
	public void testMatrixExecute() {
		DatabaseConfig config = createConfig();

		task.addConfig(config);
		task.setOutputDirectory("target/content/result/");
		task.setLibraryToCompare("example");

		task.setIncludeBinProperties(false);
		task.setIncludeBinRefrences(false);
		task.setCreateKnownVsKnownMatrix(false);
		task.setCreateUnKnownVsUnKnownMatrix(false);
		task.execute();

		// only matrix file
		assertTrue(new File("target/content/result/matrix.txt").exists());
		assertFalse(new File("target/content/result/properties.txt").exists());
		assertFalse(new File("target/content/result/references.txt").exists());
		assertFalse(new File("target/content/result/matrix-knownVSknown.txt")
				.exists());
		assertFalse(new File(
				"target/content/result/matrix-unknownVSunknown.txt").exists());

		// validation of files is not neccessaery, since know that this works
		// based on the other tests
	}

	@Test(timeout=60000)
	public void testAllMatrixExecute() {
		DatabaseConfig config = createConfig();

		task.addConfig(config);
		task.setOutputDirectory("target/content/result/");
		task.setLibraryToCompare("example");

		task.setIncludeBinProperties(false);
		task.setIncludeBinRefrences(false);
		task.setCreateKnownVsKnownMatrix(true);
		task.setCreateUnKnownVsUnKnownMatrix(true);
		task.setCreateKnownVsUnKnownMatrix(true);
		task.execute();

		// only matrixes files
		assertTrue(new File("target/content/result/matrix.txt").exists());
		assertFalse(new File("target/content/result/properties.txt").exists());
		assertFalse(new File("target/content/result/references.txt").exists());
		assertTrue(new File("target/content/result/matrix-knownVSknown.txt")
				.exists());
		assertTrue(new File("target/content/result/matrix-unknownVSunknown.txt")
				.exists());
		assertTrue(new File("target/content/result/matrix-knownVSunknown.txt")
				.exists());

		// validation of files is not neccessaery, since know that this works
		// based on the other tests
	}

	@Test(timeout=60000)
	public void testCreateNothingExecute() {
		DatabaseConfig config = createConfig();

		task.addConfig(config);
		task.setOutputDirectory("target/content/result/");

		task.setIncludeBinProperties(false);
		task.setIncludeBinRefrences(false);
		task.setCreateKnownVsKnownMatrix(false);
		task.setCreateUnKnownVsUnKnownMatrix(false);
		task.setCreateKnownVsUnKnownMatrix(false);
		task.setCreateMspVsMsp(false);
		task.execute();

		// only matrixes files
		assertFalse(new File("target/content/result/matrix.txt").exists());
		assertFalse(new File("target/content/result/properties.txt").exists());
		assertFalse(new File("target/content/result/references.txt").exists());
		assertFalse(new File("target/content/result/matrix-knownVSknown.txt")
				.exists());
		assertFalse(new File(
				"target/content/result/matrix-unknownVSunknown.txt").exists());
		assertFalse(new File("target/content/result/matrix-knownVSunknown.txt")
				.exists());
		assertFalse(new File("target/content/result/matrix-mspVSmsp.txt")
		.exists());
		// validation of files is not neccessaery, since know that this works
		// based on the other tests
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		try {
			ResourceSource source = new ResourceSource("/library-rtx5.xml");
			assertTrue(source.exist());
			return new XmlDataSet(source.getStream());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
