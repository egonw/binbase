import java.util.HashMap;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtilFactory;

/*
 * Created on Jul 28, 2006
 */

/**
 * an example which shows us how we can access the cluster
 */
public class AccessToTheCluster {

	public static void main(String[] args) throws Exception {
		String username = args[0];
		String password = args[1];
		String server = args[2];
		String factory = args[3];
		
		//create propeties
		Map<String, String> properties = new HashMap<String, String>();
		
		properties.put("username", username);
		properties.put("password", password);
		properties.put("server", server);
		
		//create the util
		ClusterUtil util = ClusterUtilFactory.newInstance(factory).createUtil(properties);
		
		//print the information about the cluster
		System.err.println(util.getClusterInformation());
	}
}
