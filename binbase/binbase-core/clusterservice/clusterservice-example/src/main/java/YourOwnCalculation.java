import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.AbstractClusterHandler;

/*
 * Created on Jul 28, 2006
 */

/**
 *  a simple cluster handler which does nothing
 */
public class YourOwnCalculation extends AbstractClusterHandler{

	/**
	 * the start method of a cluster handler
	 * @author wohlgemuth
	 * @version Jul 28, 2006
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterHandler#start()
	 */
	public boolean startProcessing() throws Exception {

		System.out.println("here I am and thats my receveid object for configuration: " + this.getObject());
		
		//successfull
		return true;
	}
	
}
