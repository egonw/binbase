#simple script to start a node
source $HOME/.bash_profile
PATH=$PATH:$HOME/cluster/ant/bin
BINBASE_MEMORY=`free -m | awk '/Mem:/ {print int($2 * 0.75)}'`


ANT_OPTS="-Xmx128m" ant -DMYJOB_ID=$JOB_ID -DMYTEMP=$TMPDIR -DBINBASE_NODE_MEMORY=$BINBASE_MEMORY -f ../run.xml Node
