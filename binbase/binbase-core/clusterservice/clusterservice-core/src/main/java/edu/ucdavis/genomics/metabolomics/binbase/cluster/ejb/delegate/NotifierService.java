package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate;

import javax.ejb.Remote;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;

@Remote
public interface NotifierService {

	public abstract boolean isEnableEmail() throws NotificationException;

	public abstract boolean isEnableReportForwarding() throws NotificationException;

	public abstract boolean isEnableTwitter() throws NotificationException;

	public abstract void setEnableEmail(boolean enableEmail) throws NotificationException;

	public abstract void setEnableReportForwarding(boolean enableReportForwarding) throws NotificationException;

	public abstract void setEnableTwitter(boolean enableTwitter) throws NotificationException;

}
