package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtilFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.node.LocalCalculationNode;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.status.EJBReportFactory;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;

/**
 * provides access to a local cluster installation
 * 
 * @author wohlgemuth
 * 
 */
public class LocalCalculationFactory extends ClusterUtilFactory {

	@Override
	public ClusterUtil createUtil(Map<?,?> p) {

		if (p.get("java.naming.provider.url") != null) {
			System.setProperty("java.naming.factory.initial",
					"org.jnp.interfaces.NamingContextFactory");
			System.setProperty("java.naming.factory.url.pkgs",
					"org.jboss.naming:org.jnp.interfaces");
			System.setProperty("java.naming.provider.url", p.get(
					"java.naming.provider.url").toString());
		} else {
			XMLConfigurator.getInstance();
		}

		System.setProperty(ReportFactory.DEFAULT_PROPERTY_NAME,
				EJBReportFactory.class.getName());

		try {
			LocalCalculationUtil util = new LocalCalculationUtil();
			util.prepare();
			
			return util;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public String[] getNeededProperties() {
		return new String[] { "java.naming.provider.url" };
	}
}
