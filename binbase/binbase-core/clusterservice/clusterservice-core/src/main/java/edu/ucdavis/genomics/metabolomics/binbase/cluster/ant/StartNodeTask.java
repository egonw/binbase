/*
 * Created on Aug 2, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;

/**
 * starts nodes on the cluster
 * 
 * @author wohlgemuth
 * @version Aug 2, 2006
 */
public class StartNodeTask extends ClusterInitializeTask {
	int nodes = -1;

	public void doExecution(ClusterUtil util) throws Exception {
		if (nodes <= 0) {
			util.startNode();
		}
		else {
			util.startNodes(nodes);
		}
	}

	public int getNodes() {
		return nodes;
	}

	public void setNodes(int nodes) {
		this.nodes = nodes;
	}
}
