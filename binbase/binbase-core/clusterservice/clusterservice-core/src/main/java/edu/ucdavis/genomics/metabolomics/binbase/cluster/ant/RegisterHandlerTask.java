package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import java.util.Properties;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.filters.StringInputStream;
import org.apache.tools.ant.taskdefs.Java;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;

public class RegisterHandlerTask  extends JavaDelegateTask {

	private String classOfInterrest;
	
	private String handler;
	
	@Override
	protected void generateArguments(Java java) {
		java.createArg().setValue(getFactory());
		java.createArg().setValue(getClassOfInterrest());
		java.createArg().setValue(getHandler());
		
	}

	@Override
	protected void validate() throws BuildException {
		if (this.getClassOfInterrest() == null) {
			throw new BuildException("you need to set a class of interrest");
		}	
		if (this.getHandler().length() == 0) {
			throw new BuildException("you need to set a handler");
		}
		if(this.getFactory() == null){
			throw new BuildException("you need to set a factory");
		}
		if(this.getFactory().length() == 0){
			throw new BuildException("you need to set a factory");
		}
	}

	@Override
	protected void run(Properties p) throws Exception {
		ClusterUtil util = createFactory(p).createUtil(p);

		try {
			util.registerHandler(this.getClassOfInterrest(), this.getHandler());
			log("done", Project.MSG_INFO);
		} finally {
			util.destroy();
		}
	}
	public static void main(String[] args) throws Exception {
		Project pro = new Project();

		Properties p = new Properties();
		RegisterHandlerTask report = new RegisterHandlerTask();
		report.setProject(pro);
		report.setFactory(args[1]);
		report.setHandler(args[2]);
		report.setClassOfInterrest(args[3]);

		p.load(new StringInputStream(args[0]));

		report.run(p);
	}

	protected String getClassOfInterrest() {
		return classOfInterrest;
	}

	protected void setClassOfInterrest(String classOfInterrest) {
		this.classOfInterrest = classOfInterrest;
	}

	protected String getHandler() {
		return handler;
	}

	protected void setHandler(String handler) {
		this.handler = handler;
	}

}
