/*
 * Created on Jul 19, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtilFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.status.EJBReportFactory;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;

/**
 * factory for creating the implementation
 * @author wohlgemuth
 * @version Jul 31, 2006
 *
 */
public class RocksClusterFactoryImpl extends ClusterUtilFactory {
	private Logger logger = Logger.getLogger(getClass());

	private static final HashMap<Long, RocksClusterImplementation> cache = new HashMap<Long, RocksClusterImplementation>();

	@Override
	public ClusterUtil createUtil(Map p) {
		String username = (String) p.get("username");
		String password = (String) p.get("password");
		String server = (String) p.get("server");

		if (p.get("java.naming.provider.url") != null) {
			System.setProperty("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			System.setProperty("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
			System.setProperty("java.naming.provider.url", p.get("java.naming.provider.url").toString());
		}
		else{
			XMLConfigurator.getInstance();
		}

		System.setProperty(ReportFactory.DEFAULT_PROPERTY_NAME, EJBReportFactory.class.getName());

		long hashcode = server.hashCode() + username.hashCode() + password.hashCode();

		RocksClusterImplementation util = cache.get(hashcode);

		if (util == null) {
			logger.info("create new implementation of the cluster util");
			util = new RocksClusterImplementation(server, username, password);
		} else {
			logger.info("already established connection, use cached version");
		}

		return util;
	}

	@Override
	public String[] getNeededProperties() {
		return new String[] { "username", "server", "password", "java.naming.provider.url" };
	}

}
