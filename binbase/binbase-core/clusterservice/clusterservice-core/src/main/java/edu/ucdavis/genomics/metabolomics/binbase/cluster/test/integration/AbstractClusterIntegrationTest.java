/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.complex.ComplexClusterHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.complex.ComplexClusterJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.simple.SimpleClusterHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.simple.SimpleClusterJob;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * basic test to compare diffrent cluster implementations to each other
 * 
 * @author wohlgemuth
 * 
 */
public abstract class AbstractClusterIntegrationTest extends AbstractClusterTest {

	/**
	 * needs to be implemented so that we can provide some load for the cluster
	 * 
	 * @return
	 */
	protected abstract int countOfJobs();

	/**
	 * tests if the scheduling and calculation of the cluster works for a simple
	 * job without any kind of a job hierachy
	 * 
	 * @throws Exception
	 * @throws ConfigurationException
	 */
	@org.junit.Test(timeout = 3000000)
	public void testSimpleJob() throws ConfigurationException, Exception {
		// how many jobs we want to calculate
		int count = countOfJobs();

		logger.info("create result map");
		// our map for collecting the results
		final Map<Integer, Integer> results = new HashMap<Integer, Integer>();


		logger.info("schedule some calculations");
		// register handler and define some datasets to calculate
		SimpleClusterJob[] jobs = new SimpleClusterJob[count];
		for (int i = 0; i < jobs.length; i++) {

			Integer[] data = new Integer[jobs.length];
			for (int x = 0; x < data.length; x++) {
				data[x] = x + i;
			}

			jobs[i] = new SimpleClusterJob(i, data);

			// register our handler and schedule a job
			UTIL.scheduleJob(jobs[i], SimpleClusterHandler.class.getName());
		}

		logger.info("start nodes");

		UTIL.startNode();

		if(UTIL.getNodeCount() > 1){
			UTIL.startNode();			
		}
		else{
			logger.info("only one node supported");
		}

		logger.info("waiting till all nodes are scheduled to run");
		// wait till the nodes are running
		UTIL.waitTillPendingQueueCurrentUserIsEmpty();

		logger.info("waiting till all nodes are finished");
		// wait till they are done
		UTIL.waitTillQueueCurrentUserIsEmpty();

		logger.info("analyze result of this calculation");
		// analyze the result

		logger.info("sleep for 10 seconds...");
		Thread.sleep(getSleepTime());
		logger.info("results: " +results);
		
		assertTrue(results.size() == count);

		// doing the same calculation here which we did in the cluster job
		// to make sure the remote side did the same
		for (SimpleClusterJob job : jobs) {
			Integer[] values = job.getValues();

			Integer result = new Integer(0);

			for (Integer value : values) {
				result = result + value;
			}

			logger.info("checking calcualtion for job: " + job.getId() + " - expsected result: " + result + " - returned result: " + results.get(job.getId()));
			assertTrue(result.equals(results.get(job.getId())));
		}

		// test is done
	}

	protected int getSleepTime() {
		return 10000;
	}

	/**
	 * test if the cluster works for jobs with several sub jobs and reports the
	 * results of the sub jobs to the server, so that we can fetch them here
	 * 
	 * it is also kind of a smoke test since it generates the number of jobs
	 * defiened in
	 * 
	 * @see {@link AbstractClusterIntegrationTest} countOfJobs() and generates
	 *      for each of these jobs the same amount of sub jobs. So if you define
	 *      5 jobs, you actually run 25
	 * 
	 * This also means that this test can run extremly long, so be carefull with
	 * the amount of jobs you want to test
	 * 
	 * We defined an anotation with a timeout of: 300000 ms which should be more
	 * or enough for a test run.
	 * 
	 * @throws Exception
	 * @throws InterruptedException
	 */
	@org.junit.Test(timeout = 3000000)
	public void testComplexJob() throws InterruptedException, Exception {
		// how many jobs we want to calculate
		int count = countOfJobs();

		logger.info("create result map");
		// our map for collecting the results
		final Map<Integer, Integer> results = new HashMap<Integer, Integer>();


		logger.info("schedule some calculations");
		// register handler and define some datasets to calculate
		int jobCounter = 0;
		ComplexClusterJob[] myJobs = new ComplexClusterJob[count];
		for (int i = 0; i < myJobs.length; i++) {
			SimpleClusterJob[] jobs = new SimpleClusterJob[count];
			for (int x = 0; x < jobs.length; x++) {

				Integer[] data = new Integer[jobs.length];
				for (int y = 0; y < data.length; y++) {
					data[y] = y + x;
				}

				jobs[x] = new SimpleClusterJob(jobCounter, data);
				jobCounter = jobCounter + 1;
			}

			myJobs[i] = new ComplexClusterJob(i, jobs);
			// register our handler and schedule a job
			UTIL.scheduleJob(myJobs[i], ComplexClusterHandler.class.getName());

		}

		assertTrue(jobCounter == count * count);

		logger.info("start nodes");

		UTIL.startNode();

		if(UTIL.getNodeCount() > 1){
			UTIL.startNode();			
		}
		else{
			logger.info("only one node supported");
		}

		logger.info("waiting till all nodes are scheduled to run");
		// wait till the nodes are running
		UTIL.waitTillPendingQueueCurrentUserIsEmpty();

		logger.info("waiting till all nodes are finished");
		// wait till they are done
		UTIL.waitTillQueueCurrentUserIsEmpty();

		logger.info("analyze result of this calculation");
		// analyze the result

		logger.info("sleep for 10 seconds...");
		Thread.sleep(getSleepTime());

		assertTrue(results.size() == jobCounter);

	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();

		logger.info("removing eventually registered handler");
		Configurator.getConfigService().clearHandler();
		
		UTIL.killJobOfCurrentUser();
		UTIL.waitTillQueueCurrentUserIsEmpty();
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();

		logger.info("removing eventually registered handler");
		Configurator.getConfigService().clearHandler();
		
		UTIL.waitTillQueueCurrentUserIsEmpty();
	}

}