/*
 * Created on Apr 22, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.status;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;

/**
 * a report which connects to the server whcih than handles all the reports
 * 
 * @author wohlgemuth
 * @version Apr 22, 2006
 */
public class EJBReport extends Report {

	private Logger logger = Logger.getLogger(EJBReport.class);

	private String owner;

	private JMSProvider provider;

	public EJBReport(String owner, JMSProvider provider) {
		super();
		try {

			this.setOwner(owner);
			this.provider = provider;

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	private void setOwner(String owner) {
		this.owner = owner;
	}

	@Override
	public synchronized void report(String owner, Serializable id,
			ReportEvent event, ReportType type, String ipaddress, Date time,
			Exception ex) {
		logger.debug(owner + " - " + id + " - " + event + " - " + type + " - "
				+ ipaddress);
		try {

				HashMap<String, Object> map = new HashMap<String, Object>();

				map.put("owner", owner);
				map.put("id", id);
				map.put("event", event);
				map.put("type", type);
				map.put("ip", ipaddress);
				map.put("time", time);
				map.put("exception", ex);

				provider.sendMessage(map);

		} catch (Exception e) {
			logger.warn("reporting failed, " + e.getMessage());
			logger.debug("reporting failed, " + e.getMessage(), e);
		}
	}

	@Override
	public String getOwner() {
		return owner;
	}

}
