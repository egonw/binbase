package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

import edu.ucdavis.genomics.metabolomics.util.status.notify.twitter.TwitterConfiguration;

public interface TwitterNotifierJMXMBean extends TwitterConfiguration, javax.management.MBeanRegistration {

	public String getUserName();

	public String getPassword();
	
	public void setUserName(String username);
	
	public void setPassword(String password);
}
