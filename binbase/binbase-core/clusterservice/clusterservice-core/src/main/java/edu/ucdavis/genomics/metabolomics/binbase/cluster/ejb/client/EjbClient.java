package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client;

import java.io.Serializable;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

/**
 * provides access to the ejb's
 * 
 * @author wohlgemuth
 */
public class EjbClient implements Serializable {

	private static final long serialVersionUID = 2L;

	private static Logger logger = Logger.getLogger(EjbClient.class);

	/**
	 * @param clazz
	 * @return
	 * @throws NamingException
	 */
	public static Object getRemoteEjb(final String archive, final Class<?> clazz) throws NamingException {
		final String name = archive + "/" + clazz.getSimpleName() + "/remote";
		logger.trace("looking for remote bean: " + name);
		final InitialContext context = new InitialContext();
		return context.lookup(name);
	}

	public static Object getLocalEjb(final String archive, final Class<?> clazz) throws NamingException {
		final InitialContext context = new InitialContext();
		final String name = archive + "/" + clazz.getSimpleName() + "/local";
		logger.trace("looking for local bean: " + name);
		return context.lookup(name);
	}

	/**
	 * @param clazz
	 * @return
	 * @throws NamingException
	 */
	public static Object getRemoteEjb(final String archive, final Class<?> clazz, final Properties p) throws NamingException {
		final String name = archive + "/" + clazz.getSimpleName() + "/remote";
		logger.trace("looking for remote bean: " + name);
		final InitialContext context = new InitialContext(p);
		return context.lookup(name);
	}

	public static Object getLocalEjb(final String archive, final Class<?> clazz, final Properties p) throws NamingException {
		final InitialContext context = new InitialContext(p);
		final String name = archive + "/" + clazz.getSimpleName() + "/local";
		logger.trace("looking for local bean: " + name);
		return context.lookup(name);
	}

	/**
	 * @param clazz
	 * @return
	 * @throws NamingException
	 */
	public static Object getRemoteEjb(final String archive, final Class<?> clazz, final String server) throws NamingException {
		final Properties p = System.getProperties();
		p.setProperty("java.naming.provider.url", "jnp://" + server + ":1099");
		final String name = archive + "/" + clazz.getSimpleName() + "/remote";
		logger.trace("looking for remote bean: " + name);
		final InitialContext context = new InitialContext(p);
		return context.lookup(name);
	}

	public static Object getLocalEjb(final String archive, final Class<?> clazz, final String server) throws NamingException {
		final Properties p = System.getProperties();
		p.setProperty("java.naming.provider.url", "jnp://" + server + ":1099");
		final InitialContext context = new InitialContext(p);
		final String name = archive + "/" + clazz.getSimpleName() + "/local";
		logger.trace("looking for local bean: " + name);
		return context.lookup(name);
	}

}
