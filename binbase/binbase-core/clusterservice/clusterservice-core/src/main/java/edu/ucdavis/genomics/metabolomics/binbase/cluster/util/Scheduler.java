package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import java.io.Serializable;

/**
 * simple scheduler which is basically a wrapper around the JMSScheduler
 * 
 * @author wohlgemuth
 */
public class Scheduler {

	
	public final static int NONE = -1;

	/**
	 * the lowest possible priority should be used for tasks where it is not
	 * important when they are finished
	 */
	public final static int PRIORITY_WHENEVER = 0;

	public final static int PRIORITY_LOWEST = 1;

	public final static int PRIORITY_VERY_LOW = 2;

	/**
	 * low priority
	 */
	public final static int PRIORITY_LOW = 3;

	/**
	 * the normal priority for calculations
	 */
	public final static int PRIORITY_NORMAL = 5;

	/**
	 * if you are in a hurry
	 */
	public final static int PRIORITY_HIGH = 6;

	public final static int PRIORITY_VERY_HIGH = 7;

	public final static int PRIORITY_HIGHEST = 8;

	/**
	 * should be calculate as soon as ressources are available
	 */
	public final static int PRIORITY_ASAP = 9;

	/**
	 * schedules a calculation with the given priority
	 * 
	 * @param object
	 * @param priority
	 * @throws SchedulingException
	 */
	public static void schedule(Serializable object, int priority) throws SchedulingException {
		schedule(object, priority, null);
	}

	/** 
	 * schedules a simple task
	 * 
	 * @param s
	 * @throws SchedulingException
	 */
	public static void schedule(Serializable s) throws SchedulingException {
		schedule(s, Scheduler.PRIORITY_NORMAL);
	}

	/**
	 * schedules a calculation with the given priority
	 * 
	 * @param object
	 * @param priority
	 * @throws SchedulingException
	 */
	public static void schedule(Serializable object, int priority, String handler) throws SchedulingException {
		JMSScheduler.getInstance().schedule(object, priority, handler);
	}
}
