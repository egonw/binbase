package edu.ucdavis.genomics.metabolomics.binbase.cluster.logger.log4j;

import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

/**
 * simple pattern layout to attach the job id to log file to simplify analysis
 * and possible link data somehow in the future. If we store the logfile in a
 * database
 * 
 * @author wohlgemuth
 * 
 */
public class JobIdPatternLayout extends PatternLayout {

	private String id = getId();

	public JobIdPatternLayout() {
		super();
	}

	private String getId() {
		if (System.getenv().get("JOB_ID") != null) {
			return (System.getenv("JOB_ID"));
		} else {
			return ("local");
		}
	}

	public JobIdPatternLayout(String pattern) {
		super(pattern);
	}

	@Override
	public String format(LoggingEvent event) {
		String result = super.format(event);

		result = "[" + id + "] " + result;

		return result;
	}

}
