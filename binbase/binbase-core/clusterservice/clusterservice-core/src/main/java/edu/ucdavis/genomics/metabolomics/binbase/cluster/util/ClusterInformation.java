/*
 * Created on Jul 21, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import java.io.Serializable;

/**
 * informations about the cluster
 * 
 * @author wohlgemuth
 * @version Jul 21, 2006
 * 
 */
public class ClusterInformation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private String load;

	private String numberOfCpu;

	private String arch;

	private String memTotal;

	private String memUse;

	private String swapTotal;

	private String swapUse;

	private String name;

	/**
	 * used arch of the node
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getArch() {
		return arch;
	}

	public void setArch(String arch) {
		this.arch = arch;
	}

	/**
	 * current load
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getLoad() {
		return load;
	}

	public void setLoad(String load) {
		this.load = load;
	}

	/**
	 * current memory used
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getMemUse() {
		return memUse;
	}

	public void setMemUse(String memUse) {
		this.memUse= memUse;
	}

	/**
	 * maximum available memory
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getMemTotal() {
		return memTotal;
	}

	public void setMemTotal(String memTotal) {
		this.memTotal = memTotal;
	}

	/**
	 * name of the node
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * count of cpus on this node
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getNumberOfCpu() {
		return numberOfCpu;
	}

	public void setNumberOfCpu(String numberOfCpu) {
		this.numberOfCpu = numberOfCpu;
	}

	/**
	 * currently used swap
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getSwapUse() {
		return swapUse;
	}

	public void setSwapUse(String swapFree) {
		this.swapUse = swapFree;
	}

	/**
	 * total available swap space
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getSwapTotal() {
		return swapTotal;
	}

	public void setSwapTotal(String swapTotal) {
		this.swapTotal = swapTotal;
	}
	
	/**
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.name;
	}
}
