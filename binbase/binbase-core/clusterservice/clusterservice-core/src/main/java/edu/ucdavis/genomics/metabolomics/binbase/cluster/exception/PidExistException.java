package edu.ucdavis.genomics.metabolomics.binbase.cluster.exception;

public class PidExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PidExistException() {
		super();
	}

	public PidExistException(String message, Throwable cause) {
		super(message, cause);
	}

	public PidExistException(String message) {
		super(message);
	}

	public PidExistException(Throwable cause) {
		super(cause);
	}

}
