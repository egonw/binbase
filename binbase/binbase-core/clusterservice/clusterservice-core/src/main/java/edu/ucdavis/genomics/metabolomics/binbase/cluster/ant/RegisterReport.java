/*
 * Created on Jul 29, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import java.util.Properties;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.filters.StringInputStream;
import org.apache.tools.ant.taskdefs.Java;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;

/**
 * register a report at the cluster
 * 
 * @author wohlgemuth
 * @version Jul 29, 2006
 * 
 */
public class RegisterReport extends JavaDelegateTask {
	private String reportClass;

	public RegisterReport() {
	}

	/**
	 * registers the given report
	 * 
	 * @author wohlgemuth
	 * @version Aug 9, 2006
	 * @param args
	 * @throws Exception
	 */
	protected void run(Properties p) throws Exception {
		ClusterUtil util = createFactory(p).createUtil(p);

		try {
			util.registerReport(getReportClass());
			log("done", Project.MSG_INFO);
		} finally {
			util.destroy();
		}
	}

	public String getReportClass() {
		return reportClass;
	}

	public void setReportClass(String reportClass) {
		this.reportClass = reportClass;
	}

	/**
	 * main methode to run the actual import and set all the parameter, do not
	 * call if from the commandline!
	 * 
	 * @author wohlgemuth
	 * @version Aug 9, 2006
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		Project pro = new Project();

		Properties p = new Properties();
		RegisterReport report = new RegisterReport();
		report.setProject(pro);
		report.setFactory(args[1]);
		report.setReportClass(args[2]);

		p.load(new StringInputStream(args[0]));

		report.run(p);
	}

	@Override
	protected void generateArguments(Java java) {
		java.createArg().setValue(getFactory());
		java.createArg().setValue(getReportClass());
	}

	@Override
	protected void validate() throws BuildException {
		if (this.getReportClass() == null) {
			throw new BuildException("you need to set a reportclass");
		}	
		if (this.getReportClass().length() == 0) {
			throw new BuildException("you need to set a reportclass");
		}
		if(this.getFactory() == null){
			throw new BuildException("you need to set a factory");
		}
		if(this.getFactory().length() == 0){
			throw new BuildException("you need to set a factory");
		}
	}
}
