package edu.ucdavis.genomics.metabolomics.binbase.cluster.node;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.logger.log4j.JobIdPatternLayout;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.LocalCalculationUtil;

/**
 * a calculation node which can only run once on a box and is used for local
 * calculations only
 * 
 * @author wohlgemuth
 */
public final class LocalCalculationNode extends CalculationNode {

	private static LocalCalculationNode instance;

	private static Appender localAppender = null;

	/**
	 * internal locking file
	 */
	private static File LOCK = new File(LocalCalculationUtil.LOCK_FILE);

	// static constructor to clean internal things and initialize the appenders
	static {
		try {

			// needed to cleanup some erros from the previous runs
			if (instance == null) {
				if (LOCK.exists()) {
					LOCK.delete();
				}
			}

			localAppender = new FileAppender(new JobIdPatternLayout(
					"[%d{ABSOLUTE}] [%-5p] [%t] [%-5c] [%m]%n"),
					LocalCalculationUtil.LOG_FILE, false);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		localAppender.setName("local-appender");
	}

	public static LocalCalculationNode getInstance() throws Exception {
		if (instance == null) {
			instance = new LocalCalculationNode();
			instance.logger.addAppender(localAppender);
			instance.logger.info("initialized a new node...");
		}
		return instance;
	}

	public static void main(final String[] args) throws Exception {
		try {
			final Logger logger = Logger.getLogger("main class...");

			boolean autoRestart = false;

			if (args.length == 1) {
				autoRestart = new Boolean(args[0]);
			} else if (args.length > 1) {
				System.out.println("USAGE: ");
				System.out.println("auto restart: true|false ");

			}

			do {
				logger.info(new LocalCalculationUtil().startNode());
				while (LocalCalculationNode.getInstance().isRunning()) {
					logger.debug("waiting till calculation is done!");
					Thread.sleep(10000);
				}
			} while (autoRestart);

		} catch (final Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private final Logger logger = Logger.getLogger(LocalCalculationNode.class);

	private LocalCalculationNode() throws Exception {
		super();

		if (logger.getAppender(localAppender.getName()) == null) {
			logger.addAppender(localAppender);
		}

		setSingleRun(false);

	}

	@Override
	protected synchronized boolean canStartCheck() {
		logger.info("lock file: " + LOCK.getAbsolutePath());

		if (LOCK.exists()) {
			logger.debug("lock file already exists and so we can't start another node");
			return false;
		} else {
			try {
				logger.debug("creating a new lock file...");
				LOCK.createNewFile();
				LOCK.deleteOnExit();
			} catch (final IOException e) {
				logger.error(e.getMessage());
				throw new RuntimeException("something went wrong! "
						+ e.getMessage(), e);
			}
			logger.debug("lock file is created and we can start a node now");
			return true;
		}

	}

	@Override
	protected void finalize() throws Throwable {
		if (LOCK != null) {
			if (LOCK.exists()) {
				logger.info("delete locking file: " + LOCK);
				LOCK.delete();
			}
		}
		logger.removeAppender(localAppender.getName());
		super.finalize();
	}

	@Override
	public boolean isSingleRun() {
		return false;
	}

	public boolean lockFileExist() {
		final boolean lock = LOCK.exists();
		logger.debug("locking file found: " + lock);
		return lock;
	}

	@Override
	protected void shutdownCompleteAction() {
		if (LOCK.exists()) {
			logger.info("delete locking file: " + LOCK);
			logger.info("deleted: " + LOCK.delete());
		}

		super.shutdownCompleteAction();
	}
}
