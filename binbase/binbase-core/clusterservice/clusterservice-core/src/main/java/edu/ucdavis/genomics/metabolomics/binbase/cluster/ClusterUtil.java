/*
 * Created on Jul 19, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster;

import static edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.ClusterInformation;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.JobInformation;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;

/**
 * abstract class for providing us with cluster utils needs to be implemented if
 * you want to use your own cluster, we only provide a factory for rocks linux
 * cluster
 * 
 * @author wohlgemuth
 * @version Jul 19, 2006
 */
public abstract class ClusterUtil {

	protected ClusterUtil() {

	}

	public void prepare()  throws Exception{
		
	}
	
	/**
	 * return the queue of the cluster
	 * 
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @return
	 * @throws Exception
	 */
	public abstract List<JobInformation> getQueue() throws Exception;

	/**
	 * returns the queue of the connected user
	 * 
	 * @author wohlgemuth
	 * @version Jul 21, 2006
	 * @return
	 * @throws Exception
	 */
	public abstract List<JobInformation> getQueueCurrentUser() throws Exception;

	/**
	 * returns the queue for the given user
	 * 
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @param user
	 * @return
	 */
	public abstract List<JobInformation> getQueueByUser(String user) throws Exception;

	/**
	 * return the queue of the cluster
	 * 
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @return
	 * @throws Exception
	 */
	public abstract List<JobInformation> getPendingQueue() throws Exception;

	/**
	 * returns the queue for the given user
	 * 
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @param user
	 * @return
	 */
	public abstract List<JobInformation> getPendingQueueByUser(String user) throws Exception;

	/**
	 * returns the queue for the given user
	 * 
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @param user
	 * @return
	 */
	public abstract List<JobInformation> getPendingQueueByCurrentUser() throws Exception;

	/**
	 * executes a command on the cluster headnode
	 * 
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @return
	 */
	public abstract String executeCommand(String command) throws Exception;

	/**
	 * kills the given job
	 * 
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @param id
	 */
	public abstract String killJob(int id) throws Exception;

	/**
	 * kills all jobs of this user
	 * 
	 * @author wohlgemuth
	 * @version Jul 21, 2006
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public abstract String killJobs(String user) throws Exception;

	/**
	 * kill all jobs of the currently connected user
	 * 
	 * @author wohlgemuth
	 * @version Jul 21, 2006
	 * @return
	 * @throws Exception
	 */
	public abstract String killJobOfCurrentUser() throws Exception;

	/**
	 * provides us with information about the cluster
	 * 
	 * @author wohlgemuth
	 * @version Jul 21, 2006
	 * @return
	 * @throws Exception
	 */
	public abstract Collection<ClusterInformation> getClusterInformation() throws Exception;

	/**
	 * gives us informations about all nodes taken by this user
	 * 
	 * @author wohlgemuth
	 * @version Jul 21, 2006
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public abstract List<ClusterInformation> getClusterInformationByUser(String username) throws Exception;

	/**
	 * gives us all nodes taken by the currently connected user
	 * 
	 * @author wohlgemuth
	 * @version Jul 21, 2006
	 * @return
	 * @throws Exception
	 */
	public abstract Collection<ClusterInformation> getClusterInformationCurrentUser() throws Exception;

	/**
	 * generates the java enviorement to use the cluster like downloading
	 * libraries, creating scripts and so one
	 * 
	 * @author wohlgemuth
	 * @version Jul 27, 2006
	 * @throws Exception
	 */
	public final String initializeCluster() throws Exception {
		return this.initializeCluster("127.0.0.1");
	}

	/**
	 * initialize the cluster and including the application server
	 * 
	 * @param applicationServer
	 * @return
	 * @throws Exception
	 */
	public abstract String initializeCluster(String applicationServer) throws Exception;

	/**
	 * uploads a new library in form of a jar file to the cluster so that we can
	 * use this classes with other jobs
	 * 
	 * @param source
	 *            the source where we find the library. For example on an ftp
	 *            server or so
	 * @author wohlgemuth
	 * @version Jul 27, 2006
	 */

	public void uploadLibrary(String name, Source source) throws Exception {
		name = name.replaceAll("\\\\", "/");
		String temp[] = name.split("/");
		name = temp[temp.length - 1];
		name = getClusterLibDir() + name;

		storeDataOnCluster(name, source);
	}

	/**
	 * the directory where all the libs are stored
	 * @return
	 */
	protected abstract String getClusterLibDir();

	/**
	 * install the given properties on the cluster, for example if we wanna
	 * change the application server or so
	 * 
	 * @author wohlgemuth
	 * @version Jul 27, 2006
	 * @param p
	 * @throws Exception
	 */
	public abstract void setClusterProperties(Properties p) throws Exception;

	/**
	 * stores some data on the cluster
	 * 
	 * @param filePathOnCluster
	 *            where the data should be stored <code>
	 * 	filePathOnCluster = /tmp/data.txt
	 * </code>
	 *            stores the source in the data directory
	 * @author wohlgemuth
	 * @version Jul 27, 2006
	 * @param source
	 * @throws Exception
	 */
	public abstract void storeDataOnCluster(String filePathOnCluster, Source source) throws Exception;

	/**
	 * downloads data stored on the cluster
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @param fileOnCluster
	 * @return
	 * @throws Exception
	 */
	public abstract Source downloadDataFromCluster(String fileOnCluster) throws Exception;

	/**
	 * schedules a job on the cluster with the given parameters and handler the
	 * handler will be registerd with the class of the parameter. If the handler
	 * is already registered we only schedule the job
	 * 
	 * @author wohlgemuth
	 * @version Jul 27, 2006
	 * @param parameter
	 * @param handler
	 */
	public final void scheduleJob(Serializable parameter, String clusterHandler) throws Exception {
		this.scheduleJob(parameter, clusterHandler, PRIORITY_NORMAL);
	}

	/**
	 * register a new cluster handler
	 * 
	 * @param objectOfInterrestClass
	 * @param clusterHandler
	 * @throws Exception
	 */
	public void registerHandler(String objectOfInterrestClass, String clusterHandler) throws Exception {
		Configurator.getConfigService().addHandler(objectOfInterrestClass, clusterHandler);

	}

	/**
	 * schedules a job on the cluster with the given parameters and handler the
	 * handler will be registerd with the class of the parameter. If the handler
	 * is already registered we only schedule the job
	 * 
	 * @author wohlgemuth
	 * @version Oct 16, 2006
	 * @param parameter
	 * @param handler
	 */
	public void scheduleJob(Serializable object, int priority) throws Exception {
		Scheduler.schedule(object, priority);
	}

	/**
	 * schedules a job
	 * 
	 * @param parameter
	 * @throws Exception
	 */
	public void scheduleJob(Serializable parameter) throws Exception {
		this.scheduleJob(parameter, PRIORITY_NORMAL);
	}

	/**
	 * schedules a job
	 * 
	 * @param parameter
	 * @param priority
	 * @throws Exception
	 */
	public void scheduleJob(Serializable object, String handler, int priority) throws Exception {
		Scheduler.schedule(object, priority, handler);
	}

	/**
	 * starts a node on the cluster
	 * 
	 * @author wohlgemuth
	 * @version Jul 28, 2006
	 * @throws Exception
	 */
	public abstract String startNode() throws Exception;

	/**
	 * starts several nodes
	 * 
	 * @author wohlgemuth
	 * @version Jul 29, 2006
	 * @param count
	 * @throws Exception
	 */
	public abstract String startNodes(int count) throws Exception;

	/**
	 * returns all registered libries at the cluster to avoid double deployment
	 * 
	 * @author wohlgemuth
	 * @version Jul 27, 2006
	 * @return
	 * @throws Exeption
	 */
	public abstract String[] getRegisteredLibraries() throws Exception;

	/**
	 * register a report at the cluster, can used for events and to transport
	 * results to the application server
	 * 
	 * @author wohlgemuth
	 * @version Jul 29, 2006
	 * @param report
	 * @throws Exception
	 */

	public void registerReport(String report) throws Exception {
		Configurator.getReportService().addReport(report);
	}

	/**
	 * closes all connection or so, just a clean up method
	 * 
	 * @author wohlgemuth
	 * @version Jul 28, 2006
	 * @throws Exception
	 */
	public abstract void destroy() throws Exception;

	/**
	 * uploads a configuration to the cluster
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @param targetName
	 * @param className
	 * @param targetParameter
	 * @throws Exception
	 */
	public void registerConfiguration(String name, Source source) throws Exception {
		// name = name.replaceAll("\\\\", "/");
		String temp[] = name.split("/");
		name = temp[temp.length - 1];
		name = getClusterConfigDir() + name;

		storeDataOnCluster(name, source);
	}
	protected abstract String getClusterConfigDir();

	
	/**
	 * deletes a configuration from the cluster
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @param targetName
	 */
	public abstract void unregisterConfiguration(String name) throws Exception;

	/**
	 * retuns all jobs for the current user
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getJobCountCurrentUser() throws Exception {
		return getQueueCurrentUser().size();
	}

	/**
	 * returns the number of nodes the cluster has
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract int getNodeCount() throws Exception;

	/**
	 * returns the number of free nodes
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract int getFreeNodeCount() throws Exception;

	/**
	 * returns the number of used nodes
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract int getUsedNodeCount() throws Exception;

	/**
	 * returns the ammount of dead nodes
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract int getDeadNodeCount() throws Exception;

	/**
	 * returns the number of nodes the cluster has
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract int getSlotCount() throws Exception;

	/**
	 * returns the number of free slots
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract int getFreeSlotCount() throws Exception;

	/**
	 * returns the number of used slotes
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract int getUsedSlotCount() throws Exception;

	/**
	 * return the info log for the given id
	 * 
	 * @param jobID
	 * @return
	 * @throws Exception
	 */
	public abstract String getInfoLogForJob(String jobID) throws Exception;

	/**
	 * return the error log for the given id
	 * 
	 * @param jobID
	 * @return
	 * @throws Exception
	 */
	public abstract String getErrorLogForJob(String jobID) throws Exception;

	/**
	 * return the info log for the given id limited by the last N lines
	 * 
	 * @param jobID
	 * @param lastLines
	 * @return
	 * @throws Exception
	 */
	public abstract String getInfoLogForJob(String jobID, int lastLines) throws Exception;

	/**
	 * return the error log for the job limited by the last N lines
	 * 
	 * @param jobID
	 * @param lastLines
	 * @return
	 * @throws Exception
	 */
	public abstract String getErrorLogForJob(String jobID, int lastLines) throws Exception;

	/**
	 * gives as all ids for available log files
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract String[] getAvailableLogIds() throws Exception;

	/**
	 * waits till the queue with the pending jobs is empty
	 * 
	 * @param user
	 * @throws InterruptedException
	 * @throws Exception
	 */
	public void waitTillPendingQueueIsEmpty(String user) throws InterruptedException, Exception {

		while (getPendingQueueByUser(user).isEmpty() != true) {
			Thread.sleep(500);
		}

	}

	/**
	 * waits till the queue is empty
	 * 
	 * @param user
	 * @throws Exception
	 * @throws InterruptedException
	 */
	public void waitTillQueueIsEmpty(String user) throws InterruptedException, Exception {
		while (getQueueByUser(user).isEmpty() != true) {
			Thread.sleep(500);
		}
	}

	/**
	 * waits till the queue with the pending jobs is empty
	 * 
	 * @throws InterruptedException
	 * @throws Exception
	 */
	public void waitTillPendingQueueCurrentUserIsEmpty() throws InterruptedException, Exception {

		while (getPendingQueueByCurrentUser().isEmpty() != true) {
			Thread.sleep(500);
		}

	}

	/**
	 * waits till the queue is empty
	 * 
	 * @throws Exception
	 * @throws InterruptedException
	 */
	public void waitTillQueueCurrentUserIsEmpty() throws InterruptedException, Exception {
		while (getQueueCurrentUser().isEmpty() != true) {
			Thread.sleep(500);
		}
	}

}
