package edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.simple;

import java.io.Serializable;

/**
 * just a simple object which we want to use as our cluster job interface
 * 
 * @author wohlgemuth
 * 
 */
public class SimpleClusterJob implements Serializable {

	private Integer id;
	
	/**
	 * important you need to implement this method
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SimpleClusterJob){
			return ((SimpleClusterJob)obj).getId().equals(this.getId());
		}
		return false;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	public Integer[] values = null;

	public SimpleClusterJob(Integer id,Integer[] values) {
		super();
		this.values = values;
		this.id = id;
	}

	public SimpleClusterJob() {

	}

	public Integer[] getValues() {
		return values;
	}

	public void setValues(Integer[] values) {
		this.values = values;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " - " + this.getId();
	}
}
