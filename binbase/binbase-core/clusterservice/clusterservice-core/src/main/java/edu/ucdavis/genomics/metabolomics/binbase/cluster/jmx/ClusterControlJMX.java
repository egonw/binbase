/*
 * Created on Apr 18, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.exception.LockingException;

public class ClusterControlJMX implements ClusterControlJMXMBean, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public ClusterControlJMX() {
		super();
	}

	/**
	 * @jmx.managed-operation description = "release all locked ressources"
	 * @return
	 * @throws LockingException
	 */
	public void releaseLocks() throws LockingException {
		try {
			Iterator<Serializable> it = Configurator.getLockingService().getLockedRessources().iterator();
			Collection<Serializable> content = new Vector<Serializable>();
			while (it.hasNext()) {
				content.add(it.next());
			}
			it = content.iterator();

			while (it.hasNext()) {
				Configurator.getLockingService().releaseRessource(it.next());
			}
		} catch (LockingException e) {
			throw e;
		} catch (Exception e) {
			throw new LockingException(e);
		}

	}

	public ObjectName preRegister(MBeanServer server, ObjectName name) throws Exception {
		return null;
	}

	public void postRegister(Boolean registrationDone) {
	}

	public void preDeregister() throws Exception {
	}

	public void postDeregister() {
	}
}
