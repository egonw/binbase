/*
 * Created on Jul 14, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.node;

import java.util.Map;

import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.AbstracNode;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterHandler;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

/**
 * node for generic calculation which loads the implementation for the received
 * objects from the configuration jmx
 * 
 * @author wohlgemuth
 * @version Jul 14, 2006
 */
public class CalculationNode extends AbstracNode {

	public CalculationNode() throws Exception {
		super();
	}

	private Logger logger = Logger.getLogger(getClass());

	private String limitToHandler = null;

	private boolean wrongHandlerShutdown = false;

	@Override
	/**
	 * connects to the defined queue
	 */
	protected Queue initializeQueue(InitialContext ctx) throws Exception {
		String queue = getConfig().getQueue();

		logger.info("connection to queue: " + queue);
		return (Queue) ctx.lookup(queue);
	}

	/**
	 * basicly checks if the message if of the right type and once this is
	 * confirmed its generates an instance of the defined handlers and starts
	 * the calculation, In case of error it sends the message back and hopes
	 * that somebody will fix it
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.AbstracNode#onMessage(javax.jms.Message)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void onMessage(Message message) throws Exception {
		logger.debug("received: " + message);

		if (message instanceof ObjectMessage) {

			Map<String, String> handler = getConfig().getHandler();

			ObjectMessage m = (ObjectMessage) message;

			try {

				if (m.getJMSExpiration() == -1) {
					logger.info("message is marked as not longer required and will be dropped from the queue");
				}
				else {
					if (m.getObject() != null) {

						Class clazz = m.getObject().getClass();

						getReport().report(m.getObject(), Reports.AQUIRED, Reports.MESSAGE);

						if (handler.get(clazz.getName()) != null) {

							if (this.getLimitToHandler() != null) {
								if (this.getLimitToHandler().equals(handler.get(clazz.getName())) == false) {
									this.setRunning(!isWrongHandlerShutdown());
									throw new RuntimeException("sorry this node is limited to only handlers of type: " + this.getLimitToHandler()
											+ " but the current handler is: " + handler.get(clazz.getName()));
								}
							}
							ClusterHandler contentHandler = (ClusterHandler) Class.forName(handler.get(clazz.getName().toString())).newInstance();

							contentHandler.setNode(this);
							contentHandler.setObject(m.getObject());
							contentHandler.setProperties(this.getProperties());
							contentHandler.setReport(this.getReport());

							if (contentHandler.start() != true) {
								throw new RuntimeException("the content handler didnt return true, so there is a problem with the calculation!");
							}
							return;
						}
						else {
							logger.info("content of message is from wrong kind, should be: " + clazz.getName() + " , but is of type: "
									+ m.getObject().getClass().getName());
						}
					}
					else {
						throw new NullPointerException("content of message can't be null");
					}
				}
			}
			catch (InstantiationException e1) {
				logger.error(e1.getMessage(), e1);
			}
			catch (IllegalAccessException e1) {
				logger.error(e1.getMessage(), e1);
			}
			catch (ClassNotFoundException e1) {
				logger.error(e1.getMessage(), e1);
			}

			throw new RuntimeException("no implementation for the content of this message found in the configuration");
		}
		else {
			logger.warn("this kind of message is not supported drop it from queue");
		}

	}

	/**
	 * starts the node
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		try {
			new CalculationNode().start();
		}
		catch (Error e) {
			e.printStackTrace();
		}
	}

	public String getLimitToHandler() {
		return limitToHandler;
	}

	public void setLimitToHandler(String limitToHandler) {
		logger.info("node is limited to handler of kind: " + limitToHandler);
		this.limitToHandler = limitToHandler;
	}

	/**
	 * is the node going down in case of a wrong handler
	 * 
	 * @return
	 */
	public boolean isWrongHandlerShutdown() {
		return wrongHandlerShutdown;
	}

	/**
	 * if the handler is the wrong kind do we want to shut the node down?
	 * 
	 * @param wrongHandlerShutdown
	 */
	public void setWrongHandlerShutdown(boolean wrongHandlerShutdown) {
		this.wrongHandlerShutdown = wrongHandlerShutdown;
	}
}
