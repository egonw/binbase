package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.report.NodeStarterReport;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.report.NotifierReport;
import org.apache.log4j.Logger;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.*;
import java.util.Collection;
import java.util.Vector;

/**
 * jmx used for setting the report properties
 * 
 * @author wohlgemuth
 * @jmx.mbean description = "defines all runtime reports for the binbase"
 *            extends = "javax.management.MBeanRegistration"
 * 
 */
public class ReportJMX implements ReportJMXMBean,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	/**
	 * our collection of providers
	 */
	private Collection<String> provider = new Vector<String>();


	private boolean enableReporting = true;
	
	public boolean isEnableReporting() {
		return enableReporting;
	}



	public void setEnableReporting(boolean enableReporting) {
		this.enableReporting = enableReporting;
		store();

	}


	
	private Logger logger = Logger.getLogger(getClass());
	/**
	 * @jmx.managed-operation description = "adds a new report" adds a new setup
	 *                        x provider
	 * @param providerClass
	 */
	public void addReport(String providerClass) {
		if (!isReportRegistered(providerClass)) {
			this.provider.add(providerClass);
		}

	}



	/**
	 * @jmx.managed-operation description = "removes a report" removes a setupX
	 *                        provider
	 * @param providerClass
	 */
	public void removeReport(String providerClass) {
		if (isReportRegistered(providerClass)) {
			this.provider.remove(providerClass);
		}
	}

	/**
	 * @jmx.managed-attribute description = "returns all reports" returns
	 *                        allProvider which are possible to use
	 * @return
	 */
	public Collection<String> getReports() {
		return provider;
	}

	/**
	 * @jmx.managed-operation description = "true if the providerClass already
	 *                        added, false if the provider class is not added"
	 * @param providerClass
	 * @return true if the providerClass already added, false if the provider
	 *         class is not added
	 */
	public boolean isReportRegistered(String providerClass) {
		return this.provider.contains(providerClass);
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1) throws Exception {
		return arg1;
	}

	/**
	 * time to load the last configuration
	 */
	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					file));
			this.enableReporting = in.readBoolean();

			in.close();

		} catch (Exception e) {
			logger.error("postRegister(Boolean)", e); //$NON-NLS-1$
		}

		//this.addReport(NodeReport.class.getName());
		this.setEnableReporting(isEnableReporting());
		
		//adds this report so that we can start cluster nodes automatically
		this.addReport(NodeStarterReport.class.getName());
		
		//add the notifier support
		this.addReport(NotifierReport.class.getName());
		
		
	}

	/**
	 * time to save the current configuration
	 */
	public void preDeregister() throws Exception {
		this.store();
	}

	public void postDeregister() {
	}


	private void store() {
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			out.writeBoolean(this.enableReporting);
			
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}

}