package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageObject;

@Entity
@Table(name="storage")
public class StorageEntityBean implements Serializable {

	private static final long serialVersionUID = 2L;

	private String key;
	
	private StorageObject object;

	public StorageEntityBean(){
		
	}
	
	@Id
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public StorageObject getObject() {
		return object;
	}

	public void setObject(StorageObject object) {
		this.object = object;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof StorageEntityBean){
			if(((StorageEntityBean)obj).getKey().equals(this.getKey())){
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return this.getKey() + " - " + this.getObject();
	}

	@Override
	public int hashCode() {
		return this.getKey().hashCode();
	}
	
	
}
