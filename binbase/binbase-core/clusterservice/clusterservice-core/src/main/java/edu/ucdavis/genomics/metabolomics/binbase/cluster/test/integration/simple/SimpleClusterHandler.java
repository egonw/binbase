package edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.simple;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.AbstractClusterHandler;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;

/**
 * does nothing than to add values to each other
 * 
 * @author wohlgemuth
 * 
 */
public class SimpleClusterHandler extends AbstractClusterHandler {

	private Logger logger = Logger.getLogger(getClass());
	public static final ReportEvent FINISHED_EVENT = new ReportEvent(SimpleClusterHandler.class.getName(), SimpleClusterJob.class.getName());
	public static final ReportType FINISHED_TYPE = new ReportType(SimpleClusterHandler.class.getName(), SimpleClusterJob.class.getName());

	@Override
	protected boolean startProcessing() throws Exception {
		logger.info("received object of type: " + this.getObject().getClass().getName());
		logger.info("is it of expspected class:" + (this.getObject() instanceof SimpleClusterJob));
		
		SimpleClusterJob object = (SimpleClusterJob) this.getObject();

		if (object.getValues() == null) {
			throw new Exception("you need to provide some values");
		}
		if (object.getValues().length == 0) {
			throw new Exception("you need to provide some values");
		}

		// just a simple stupid calculation, nothing fancy
		Integer sum = new Integer(0);

		for (Integer value : object.getValues()) {
			sum = sum + value;
		}

		logger.info("finished and result is: " + sum + " for job: " + object.getId());
		// add our attachment
		FINISHED_TYPE.setAttachement(new Integer[]{object.getId(),sum});

		// send the result to the main node so that some event listen to it - we want to avoid to use the object itself since we dont know if the classes are deploid on the cluster 
		getReport().report(getObject().getClass().getName()+ " - " + object.getId(), FINISHED_EVENT, FINISHED_TYPE);

		return true;

	}

}
