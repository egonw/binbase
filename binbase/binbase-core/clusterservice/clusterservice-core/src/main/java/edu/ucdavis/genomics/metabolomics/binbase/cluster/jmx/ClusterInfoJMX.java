/*
 * Created on Jul 21, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

import java.io.Serializable;
import java.util.Collection;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.exception.LockingException;

/**
 * displays informations about the cluster
 * 
 * @jmx.mbean description = "shows infos about the cluster" extends =
 *            "javax.management.MBeanRegistration"
 * @author wohlgemuth
 * @version Jul 21, 2006
 * 
 */
public class ClusterInfoJMX implements ClusterInfoJMXMBean, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * @jmx.managed-operation description = "returned all locked ressources"
	 * @return
	 */
	public Collection<Serializable> getLockedRessources() throws LockingException {
		try {
			return Configurator.getLockingService()
					.getLockedRessources();
		} catch (Exception e) {
			throw new LockingException(e);
		}
	}

	/**
	 * @jmx.managed-operation description = "returned all locked ressources
	 *                        informations"
	 * @return
	 */
	public Collection<Serializable> getLockedRessourcesInformation() throws LockingException {
		try {
			return Configurator.getLockingService()
					.getLockedRessourcesInformation();
		} catch (Exception e) {
			throw new LockingException(e);
		}
	}

	public void postDeregister() {
	}

	public void postRegister(Boolean arg0) {
	}

	public void preDeregister() throws Exception {
	}

	public ObjectName preRegister(MBeanServer server, ObjectName name)
			throws Exception {
		return null;
	}

}
