/*
 * Generated file - Do not edit!
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

/**
 * MBean interface.
 * @author wohlgemuth
 * @version Apr 18, 2006
 */
public interface ClusterControlJMXMBean extends javax.management.MBeanRegistration {

  void releaseLocks() throws edu.ucdavis.genomics.metabolomics.exception.LockingException;

}
