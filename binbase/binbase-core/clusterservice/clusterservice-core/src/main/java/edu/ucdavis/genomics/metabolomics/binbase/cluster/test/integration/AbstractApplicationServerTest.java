package edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.jms.Message;
import javax.naming.Context;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.util.PropertySetter;

/**
 * simple test class for an application server which provides us with some
 * helpers methods
 * 
 * @author wohlgemuth
 * 
 */
public class AbstractApplicationServerTest {

	protected Logger logger = Logger.getLogger(AbstractApplicationServerTest.class);

	protected static Properties UNMODIFIED_SYSTEM_PROPERTIES = System.getProperties();

	@BeforeClass
	public static void initializeParameters() throws FileNotFoundException, IOException {
		System.out.println("initialize parmeters");
		@SuppressWarnings("unused")
		Properties p = PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");
	}

	@Before
	public void setUp() throws Exception {
		logger.info("setting test up...");
		// define application server settings
		System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		System.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

		// development machine
		if (System.getProperty("test.binbase.cluster.application-server") != null) {
			System.out.println("using defined application server: " + System.getProperty("test.binbase.cluster.application-server"));
			System.setProperty(Context.PROVIDER_URL, System.getProperty("test.binbase.cluster.application-server"));
		}
		// test machine modus
		else {
			System.out.println("using cluster frontend as application server: " + System.getProperty("test.binbase.cluster.server"));
			System.setProperty(Context.PROVIDER_URL, System.getProperty("test.binbase.cluster.server"));
		}

		Configurator.getConfigService().setKeepRunning(true);
		
		//empty the queue
		emptyMessageQueue();
		assertTrue(getMessageQueue().isEmpty());
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * waits till the message queue is empty
	 */
	protected void waitForMessageQueue() throws Exception {
		while (		Configurator.getConfigService().getMessages().isEmpty() == false) {
			logger.info("waiting that the message queue is empty!");
			Thread.sleep(1000);
		}
	}

	/**
	 * empty the queue
	 */
	protected void emptyMessageQueue() throws Exception {
		Configurator.getConfigService().clearQueue();
		assertTrue(getMessageQueue().isEmpty());
	}

	/**
	 * get the count of messages in the queue
	 */
	protected List<Message> getMessageQueue() throws Exception {
		return Configurator.getConfigService().getMessages();
	}

}
