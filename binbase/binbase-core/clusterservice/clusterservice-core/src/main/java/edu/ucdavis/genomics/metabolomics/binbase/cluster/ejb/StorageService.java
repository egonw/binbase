package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb;

import javax.ejb.Remote;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.StoringException;

@Remote
/**
 * simple solution to store objects and retrieve objects
 */
public interface StorageService {

	public abstract void store(String key, StorageObject object);

	public abstract boolean hasObject(String key);

	/**
	 * attemps to remove an object and if the object does not existis it is ignore
	 * @param key
	 * @throws StoringException
	 */
	public abstract void removeObject(String key) throws StoringException;

	/**
	 * remove an object and if its not exist we get an error message
	 * @param key
	 * @throws StoringException
	 */
	public abstract void removeExistingObject(String key) throws StoringException;

	
	public abstract StorageObject getObject(String key) throws StoringException;

	public abstract StorageObject[] getObjects() throws StoringException;

	public abstract String[] getKeys() throws StoringException;

	public abstract void clear() throws StoringException;

	public abstract int getSize() throws StoringException;

}