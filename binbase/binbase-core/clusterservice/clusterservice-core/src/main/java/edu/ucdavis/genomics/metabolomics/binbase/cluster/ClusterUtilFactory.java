/*
 * Created on Jul 19, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster;

import java.util.HashMap;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.RocksClusterFactoryImpl;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * creates utils for use to access clusters
 * 
 * @author wohlgemuth
 * @version Jul 19, 2006
 * 
 */
public abstract class ClusterUtilFactory extends AbstractFactory {
	/**
	 * the name of the porpertie for the which can be set in the system
	 * configuration
	 */
	public static final String DEFAULT_PROPERTY_NAME = ClusterUtilFactory.class.getName();

	private static String foundFactory = null;

	/**
	 * internal cache to keep the ammount of factories low
	 */
	private static Map<String, ClusterUtilFactory> cache = new HashMap<String, ClusterUtilFactory>();

	/**
	 * creates a new instance of the factory
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public static ClusterUtilFactory newInstance() {
		// Locate Factory
		foundFactory = findFactory(DEFAULT_PROPERTY_NAME, RocksClusterFactoryImpl.class.getName());

		return newInstance(foundFactory);
	}

	/**
	 * returns an new instance of the specified factory
	 * 
	 * @param factoryClass
	 *            the specified factory to use
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static ClusterUtilFactory newInstance(String factoryClass) {
		if (cache.get(factoryClass) == null) {
			Class classObject;
			ClusterUtilFactory factory;

			try {
				classObject = Class.forName(factoryClass);
				factory = (ClusterUtilFactory) classObject.newInstance();
				cache.put(factoryClass, factory);
				return factory;

			} catch (Exception e) {
				throw new FactoryException(e);
			}
		} else {
			return cache.get(factoryClass);
		}
	}

	/**
	 * create a util with the given properties
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @param p
	 * @return
	 */
	public abstract ClusterUtil createUtil(Map<?,?> p);

	/**
	 * creates a util using the system properties
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public ClusterUtil createUtil() {
		return createUtil(System.getProperties());
	}

	/**
	 * returns an array with the needed properties
	 * 
	 * @author wohlgemuth
	 * @version Jul 28, 2006
	 * @return
	 */
	public abstract String[] getNeededProperties();
}
