package edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.complex;

import java.io.Serializable;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.simple.SimpleClusterJob;

/**
 * a more complex jobs which has an array of simple jobs to execute
 * 
 * @author wohlgemuth
 * 
 */
public class ComplexClusterJob implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private Integer id;

	private SimpleClusterJob jobs[];

	public ComplexClusterJob(Integer id, SimpleClusterJob[] jobs) {
		super();
		this.id = id;
		this.jobs = jobs;
	}

	public boolean equals(Object obj) {
		if (obj instanceof ComplexClusterJob) {
			return ((ComplexClusterJob) obj).getId().equals(this.getId());
		}
		return false;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SimpleClusterJob[] getJobs() {
		return jobs;
	}

	public void setJobs(SimpleClusterJob[] jobs) {
		this.jobs = jobs;
	}

	/**
	 * very important if this isnt overwritten sub jobs wont work since the same
	 * object has different to string representations in diffrent vm's
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " - " + this.getId();
	}
}
