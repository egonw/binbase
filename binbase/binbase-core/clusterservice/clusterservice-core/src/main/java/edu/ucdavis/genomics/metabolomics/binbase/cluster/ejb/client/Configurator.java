package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.LockingService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.LockingServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.*;

import javax.naming.NamingException;

/**
 * access to the configurations
 * 
 * @author wohlgemuth
 */
public class Configurator {

	public static final String CLUSTER_SERVICE_GLOBAL_NAME = "clusterservice";

	/**
	 * access to the config service
	 * 
	 * @return
	 * @throws NamingException
	 */
	public static ClusterConfigService getConfigService() throws NamingException {
		return ((ClusterConfigService) EjbClient.getRemoteEjb(CLUSTER_SERVICE_GLOBAL_NAME, ClusterConfigServiceBean.class));
	}

	/**
	 * access to the config service
	 * 
	 * @return
	 * @throws NamingException
	 */
	public static ClusterReportConfigService getReportService() throws NamingException {
		return (ClusterReportConfigService) EjbClient.getRemoteEjb(CLUSTER_SERVICE_GLOBAL_NAME, ClusterReportConfigServiceBean.class);
	}

	/**
	 * access to the locking service
	 * 
	 * @return
	 * @throws NamingException
	 */
	public static LockingService getLockingService() throws NamingException {
		return (LockingService) EjbClient.getRemoteEjb(CLUSTER_SERVICE_GLOBAL_NAME, LockingServiceBean.class);
	}

	/**
	 * access to the storage service
	 * 
	 * @return
	 * @throws NamingException
	 */
	public static StorageService getStorageService() throws NamingException {
		return (StorageService) EjbClient.getRemoteEjb(CLUSTER_SERVICE_GLOBAL_NAME, StorageServiceBean.class);
	}

	/**
	 * access to the notification service
	 * 
	 * @return
	 * @throws NamingException
	 */
	public static NotifierService getNotifierService() throws NamingException {
		return (NotifierService) EjbClient.getRemoteEjb(CLUSTER_SERVICE_GLOBAL_NAME, NotifierServiceBean.class);
	}

}
