package edu.ucdavis.genomics.metabolomics.binbase.cluster.exception;

public class StoringException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * 
	 */
	public StoringException() {
		super();
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public StoringException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 */
	public StoringException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public StoringException(Throwable arg0) {
		super(arg0);
	}

}
