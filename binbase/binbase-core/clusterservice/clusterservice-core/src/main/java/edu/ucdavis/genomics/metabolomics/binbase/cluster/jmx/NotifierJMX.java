package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.report.NotifierReport;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * general notification configuration
 * 
 * @author wohlgemuth
 */
public class NotifierJMX implements NotifierJMXMBean {

	private Logger logger = Logger.getLogger(getClass());
	
	public boolean isEnableTwitter() {
		return enableTwitter;
	}

	public void setEnableTwitter(boolean enableTwitter) {
		this.enableTwitter = enableTwitter;
		this.store();
	}

	public boolean isEnableEmail() {
		return enableEmail;
	}

	public void setEnableEmail(boolean enableEmail) {
		this.enableEmail = enableEmail;
		this.store();

	}

	public boolean isEnableReportForwarding() {
		return enableReportForwarding;
	}

	public void setEnableReportForwarding(boolean enableReportForwarding) {
		this.enableReportForwarding = enableReportForwarding;
		this.store();

	}

	boolean enableTwitter;

	boolean enableEmail;

	boolean enableReportForwarding;

	@Override
	public void postDeregister() {
	}

	@Override
	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
			this.enableTwitter = in.readBoolean();
			this.enableReportForwarding = in.readBoolean();
			this.enableEmail = in.readBoolean();
			
			in.close();

		} catch (Exception e) {
			logger.debug("postRegister(Boolean)", e); //$NON-NLS-1$
		}
		
		
	}

	@Override
	public void preDeregister() throws Exception {
		this.store();
	}

	@Override
	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1) throws Exception {
		return null;
	}

	@Override
	public void fireTestMessage() throws Exception {
		if (this.isEnableReportForwarding()) {
			new NotifierReport().report("a simpe test", new ReportEvent("test message", "a test message", Priority.FATAL), new ReportType("a test",
					"a simple test", Priority.FATAL));
		}
		else {
			throw new RuntimeException("you need to enable report forwarding before you can fire a test message.");
		}
	}
	

	private void store() {
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));

			out.writeBoolean(this.isEnableTwitter());
			out.writeBoolean(this.isEnableReportForwarding());
			out.writeBoolean(this.isEnableEmail());
			
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}

}
