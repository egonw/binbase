package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.entity.StorageEntityBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.StoringException;
import org.apache.log4j.Logger;

import javax.ejb.ApplicationException;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@ApplicationException(rollback = false)
public class StorageServiceBean implements StorageService {

	private Logger logger = Logger.getLogger(StorageEntityBean.class);

	private static final long serialVersionUID = 2L;

	@PersistenceContext
	private EntityManager manager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService#store(java.lang.String,
	 *      edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageObject)
	 */
	public void store(String key, StorageObject object) {
		StorageEntityBean bean = new StorageEntityBean();
		bean.setKey(key);
		bean.setObject(object);
		manager.persist(bean);
	}

	protected StorageEntityBean load(String key) {
		StorageEntityBean bean = manager.find(StorageEntityBean.class, key);
		return bean;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService#hasObject(java.lang.String)
	 */
	public boolean hasObject(String key) {
		try {
			@SuppressWarnings("unused")
			StorageEntityBean bean = manager.find(StorageEntityBean.class, key);
			if (bean == null) {
				return false;
			}
		} catch (Exception e) {
			logger.info(e);
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService#removeObject(java.lang.String)
	 */
	public void removeObject(String key) throws StoringException {
		try {
			StorageEntityBean bean = load(key);

			if (bean != null) {
				manager.remove(bean);
			}
		} catch (EJBException e) {
			// ignored
			logger.info("exspected error: " + e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService#getObject(java.lang.String)
	 */
	public StorageObject getObject(String key) throws StoringException {
		return load(key).getObject();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService#getObjects()
	 */
	public StorageObject[] getObjects() throws StoringException {
		try {
			logger.debug("get all objects...");
			Object o[] = getObjectdsAsList().toArray();
			StorageObject b[] = new StorageObject[o.length];

			for (int i = 0; i < o.length; i++) {
				b[i] = ((StorageEntityBean) o[i]).getObject();
			}

			return b;
		} catch (EJBException e) {
			throw new StoringException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<StorageEntityBean> getObjectdsAsList() throws StoringException {
		return manager.createQuery("from " + StorageEntityBean.class.getName())
				.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService#getKeys()
	 */
	public String[] getKeys() throws StoringException {
		try {
			logger.debug("get all keys...");
			Object o[] = getObjectdsAsList().toArray();
			String b[] = new String[o.length];

			for (int i = 0; i < o.length; i++) {
				b[i] = ((StorageEntityBean) o[i]).getKey();
			}

			return b;
		} catch (EJBException e) {
			throw new StoringException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService#clear()
	 */
	public void clear() throws StoringException {
		try {
			java.util.Iterator<StorageEntityBean> a = getObjectdsAsList()
					.iterator();

			while (a.hasNext()) {
				manager.remove(a.next());
			}
		} catch (EJBException e) {
			throw new StoringException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService#getSize()
	 */
	public int getSize() throws StoringException {
		try {
			return getObjectdsAsList().size();
		} catch (EJBException e) {
			throw new StoringException(e);
		}
	}

	public void removeExistingObject(String key) throws StoringException {
		try {
			manager.remove(load(key));
		} catch (EJBException e) {
			// ignored
			throw new StoringException(e);
		}
	}

}
