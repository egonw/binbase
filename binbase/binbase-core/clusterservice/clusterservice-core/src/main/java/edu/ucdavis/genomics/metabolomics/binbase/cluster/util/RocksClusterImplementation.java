/*
 * Created on Jul 19, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import org.apache.log4j.Logger;
import org.jdom.Element;

import java.io.*;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

/**
 * specific implementation for rocks linux cluster only edit if you know what
 * you are doing!
 *
 * @author wohlgemuth
 * @version Jul 19, 2006
 */
public class RocksClusterImplementation extends AbstractUtil {
    private final Logger logger = Logger.getLogger(getClass());

    /**
     * connects to cluster
     *
     * @param servername
     * @param username
     * @param password
     * @author wohlgemuth
     * @version Jul 19, 2006
     */
    public RocksClusterImplementation(final String servername, final String username, final String password) {
        super(servername, username, password);
    }

    @Override
    public List<JobInformation> getQueue() throws Exception {

        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qstat -u \\* -xml", out);
        return generateQueue(XmlHandling.readXml(new ByteArrayInputStream(out.toByteArray())));
    }

    @Override
    public List<JobInformation> getQueueByUser(final String user) throws Exception {

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qstat -u " + user + " -xml", out);

        return generateQueue(XmlHandling.readXml(new ByteArrayInputStream(out.toByteArray())));
    }

    @Override
    public List<JobInformation> getPendingQueue() throws Exception {

        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qstat -u \\* -xml", out);

        return generatePendingQueue(XmlHandling.readXml(new ByteArrayInputStream(out.toByteArray())));
    }

    @Override
    public List<JobInformation> getPendingQueueByUser(final String user) throws Exception {

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qstat -u " + user + " -xml", out);

        return generatePendingQueue(XmlHandling.readXml(new ByteArrayInputStream(out.toByteArray())));
    }

    @Override
    public String killJob(final int id) throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qdel " + id, out);

        out.flush();
        final String ret = out.toString();
        out.close();
        return ret;
    }

    /**
     * generate the job queue
     *
     * @param root
     * @return
     * @author wohlgemuth
     * @version Jul 19, 2006
     */
    private List<JobInformation> generateQueue(final Element root) {
        final List result = root.getChild("queue_info").getChildren("job_list");
        final List<JobInformation> res = new Vector<JobInformation>();

        for (int i = 0; i < result.size(); i++) {
            final Element e = (Element) result.get(i);

            final JobInformation inf = new JobInformation();

            inf.setId(Integer.parseInt(e.getChild("JB_job_number").getText()));
            inf.setPriority(Double.parseDouble(e.getChild("JAT_prio").getText()));
            inf.setName(e.getChild("JB_name").getText());
            inf.setState(e.getChild("state").getText());
            inf.setOwner(e.getChild("JB_owner").getText());
            inf.setSubmission(e.getChild("JAT_start_time").getText());
            inf.setSlots(Integer.parseInt(e.getChild("slots").getText()));
            inf.setQueue(e.getChild("queue_name").getText());

            inf.setHost(inf.getQueue().substring(6).replaceAll(".local", ""));

            res.add(inf);
        }
        return res;
    }

    private List<JobInformation> generatePendingQueue(final Element root) {
        final List result = root.getChild("job_info").getChildren("job_list");
        final List<JobInformation> res = new Vector<JobInformation>();

        for (int i = 0; i < result.size(); i++) {
            final Element e = (Element) result.get(i);

            final JobInformation inf = new JobInformation();

            inf.setId(Integer.parseInt(e.getChild("JB_job_number").getText()));
            inf.setPriority(Double.parseDouble(e.getChild("JAT_prio").getText()));
            inf.setName(e.getChild("JB_name").getText());
            inf.setState(e.getChild("state").getText());
            inf.setOwner(e.getChild("JB_owner").getText());
            inf.setSubmission(e.getChild("JB_submission_time").getText());
            inf.setSlots(Integer.parseInt(e.getChild("slots").getText()));

            res.add(inf);
        }
        return res;
    }

    @Override
    public String killJobs(final String user) throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qdel -u " + getUsername(), out);

        out.flush();
        final String ret = out.toString();
        out.close();
        return ret;

    }

    @Override
    public List<ClusterInformation> getClusterInformation() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qhost", out);

        out.flush();
        final BufferedReader reader = new BufferedReader(new StringReader(out.toString()));
        String line = null;
        final List<ClusterInformation> result = new Vector<ClusterInformation>();

        int x = 0;
        while ((line = reader.readLine()) != null) {
            x++;
            if (x > 3) {
                // remove whitespaces
                String[] array = line.split(" ");
                final StringBuffer buffer = new StringBuffer();
                for (final String element : array) {
                    if (element.length() > 0) {
                        buffer.append(element);
                        buffer.append("\t");
                    }
                }

                // split by tab to get fields
                array = buffer.toString().split("\t");

                if (array.length == 8) {
                    final ClusterInformation info = new ClusterInformation();
                    info.setName(array[0]);
                    info.setArch(array[1]);
                    info.setNumberOfCpu(array[2]);
                    info.setLoad(array[3]);
                    info.setMemTotal(array[4]);
                    info.setMemUse(array[5]);
                    info.setSwapTotal(array[6]);
                    info.setSwapUse(array[7]);

                    result.add(info);
                }
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ClusterInformation> getClusterInformationByUser(final String username) throws Exception {
        final List<JobInformation> user = getQueueByUser(username);
        if (user.isEmpty()) {
            return new Vector<ClusterInformation>();
        }
        final List<ClusterInformation> cluster = getClusterInformation();
        final List<ClusterInformation> result = new Vector<ClusterInformation>();

        for (int i = 0; i < user.size(); i++) {

            for (int x = 0; x < cluster.size(); x++) {
                if (cluster.get(x).getName().equals(user.get(i).getHost())) {
                    result.add(cluster.get(x));
                    x = cluster.size();
                }
            }
        }

        return result;
    }

    @Override
    public String startNode() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "sh binbase/start.sh", out);
        out.flush();
        out.close();
        logger.info(out.toString());

        return out.toString();
    }

    @Override
    public String startNodes(final int count) throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "sh binbase/start.sh " + count, out);
        out.flush();
        out.close();
        logger.info(out.toString());

        return out.toString();
    }

    @Override
    public int getNodeCount() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qhost | awk '{ print $3}'", out);
        out.flush();
        out.close();

        logger.info(out.toString());
        final Scanner scanner = new Scanner(new ByteArrayInputStream(out.toByteArray()));
        scanner.next();
        scanner.next();

        int nodes = 0;
        while (scanner.hasNext()) {
            nodes = nodes + 1;
            scanner.next();
        }
        return nodes;
    }

    @Override
    public int getDeadNodeCount() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qhost | awk '{ if($4 == \"-\") {print $3}}'", out);
        out.flush();
        out.close();

        final Scanner scanner = new Scanner(new ByteArrayInputStream(out.toByteArray()));
        scanner.next();

        int nodes = 0;
        while (scanner.hasNext()) {
            nodes = nodes + 1;
            scanner.nextInt();
        }
        return nodes;
    }

    @Override
    public int getFreeNodeCount() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qhost -j | grep -v global | grep -v \".---\"", out);
        out.flush();
        out.close();

        final BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(out.toByteArray())));

        int freeNodes = 0;
        String line = null;
        boolean foundJob = false;

        while ((line = reader.readLine()) != null) {

            if (line.matches("compute.*")) {
                freeNodes++;
                foundJob = false;
            } else if (line.indexOf("all.q@comp") > -1) {
                if (foundJob == false) {
                    if (line.indexOf("dr") == -1) {
                        freeNodes--;
                        foundJob = true;
                    }
                }
            }
        }

        logger.info("found free nodes: " + freeNodes);

        return freeNodes;
    }

    @Override
    public int getFreeSlotCount() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qhost -j | grep -v global | grep -v \".---\"", out);
        out.flush();
        out.close();

        final BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(out.toByteArray())));

        String line = null;

        int maxSlots = getSlotCount();

        while ((line = reader.readLine()) != null) {
            if (line.indexOf("all.q@comp") > -1) {
                if (line.indexOf("dr") == -1) {
                    maxSlots--;
                }
            }
        }

        return maxSlots;
    }

    @Override
    public int getSlotCount() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qhost | awk '{ print $3}'", out);
        out.flush();
        out.close();

        final Scanner scanner = new Scanner(new ByteArrayInputStream(out.toByteArray()));
        scanner.next();
        scanner.next();

        int nodes = 0;
        while (scanner.hasNext()) {
            nodes = nodes + scanner.nextInt();
        }
        return nodes;
    }

    @Override
    public int getUsedNodeCount() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qhost -j | grep -v global | grep -v \".---\"", out);
        out.flush();
        out.close();

        final BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(out.toByteArray())));

        int freeNodes = 0;
        int nodesTotal = 0;
        String line = null;
        boolean foundJob = false;

        while ((line = reader.readLine()) != null) {

            if (line.matches("compute.*")) {
                freeNodes++;
                nodesTotal++;
                foundJob = false;
            } else if (line.indexOf("all.q@comp") > -1) {
                if (foundJob == false) {
                    freeNodes--;
                    foundJob = true;
                }
            }
        }

        return nodesTotal - freeNodes;
    }

    @Override
    public int getUsedSlotCount() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "qhost -j | grep -v global | grep -v \".---\"", out);
        out.flush();
        out.close();

        final BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(out.toByteArray())));

        String line = null;

        int slots = 0;
        while ((line = reader.readLine()) != null) {
            if (line.indexOf("all.q@comp") > -1) {
                slots++;
            }
        }

        return slots;
    }

    @Override
    public String getErrorLogForJob(final String jobID) throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cat binbase/ClusterService/log/BinBaseNode.e" + jobID, out);
        out.flush();
        out.close();

        return new String(out.toByteArray());
    }

    @Override
    public String getErrorLogForJob(final String jobID, final int lastLines) throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "tail -n " + lastLines + " binbase/ClusterService/log/BinBaseNode.e" + jobID, out);
        out.flush();
        out.close();

        return new String(out.toByteArray());
    }

    @Override
    public String getInfoLogForJob(final String jobID) throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cat binbase/ClusterService/log/BinBaseNode.o" + jobID, out);
        out.flush();
        out.close();

        return new String(out.toByteArray());
    }

    @Override
    public String getInfoLogForJob(final String jobID, final int lastLines) throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "tail -n " + lastLines + " binbase/ClusterService/log/BinBaseNode.o" + jobID, out);
        out.flush();
        out.close();

        return new String(out.toByteArray());
    }

    @Override
    public String[] getAvailableLogIds() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "ls binbase/ClusterService/log/BinBaseNode.o*", out);
        out.flush();
        out.close();

        final String result[] = new String(out.toByteArray()).split("\n");

        for (int i = 0; i < result.length; i++) {
            result[i] = result[i].replaceAll("binbase/ClusterService/log/BinBaseNode.o", "");
        }
        return result;
    }

    @Override
    public String initializeCluster(String applicationServer, final File file) throws Exception {
        applicationServer = applicationServer.trim();
        logger.info("starting to intialize the cluster for application server: " + applicationServer);
        final StringBuffer content = new StringBuffer();
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final OutputStream out2 = new ByteArrayOutputStream();

        int result = 0;

        logger.info("checking if jboss variable exists");
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(),
                "if [ \"test\" = \"test$JBOSS_HOME\" ]; then echo please provide the variable JBOSS_HOME ; else echo found JBOSS_HOME at $JBOSS_HOME; fi;",
                out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }


        logger.info("remove old installation");
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "rm -rdf binbase", out);

        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "mkdir binbase", out);

        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "mkdir binbase/tmp", out);

        logger.info("download cluster librarys");
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "mkdir $HOME/binbase/tmp/ClusterService", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        logger.info("copy archive: " + file.getAbsolutePath());
        SSHUtil.scpToFile(new FileSource(file), getServername(), getUsername(), getPassword(), "$HOME/binbase/tmp/ClusterService/clusterservice.zip");

        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/tmp/ClusterService && unzip clusterservice.zip", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/tmp/ClusterService && rm clusterservice.zip", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cp -rdf $HOME/binbase/tmp/ClusterService /$HOME/binbase/", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        logger.info("checking if ant file exist in the temp directory");

        // copies the file from the local temp directory instead of
        // downloading it
        if (checkExistLocal("apache-ant-1.6.5-bin.zip")) {
            logger.info("found locally and copy to location");
            SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cp /tmp/apache-ant-1.6.5-bin.zip $HOME/binbase/tmp", out);
        } else {
            logger.info("not found locally, downloading it");
            result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(),
                    "cd $HOME/binbase/tmp && wget http://archive.apache.org/dist/ant/binaries/apache-ant-1.6.5-bin.zip", out);
            if (result != 0) {
                throw new ClusterException("error at last line: " + out.toString());
            }

        }

        // copies the file from the local temp directory instead of
        // downloading it
        if (checkExistLocal("commons-net-1.4.1.tar.gz")) {
            logger.info("found locally and copy to location");
            SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cp /tmp/commons-net-1.4.1.tar.gz $HOME/binbase/tmp", out);
        } else {
            logger.info("not found locally, downloading it");
            result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(),
                    "cd $HOME/binbase/tmp && wget http://archive.apache.org/dist/jakarta/commons/net/binaries/commons-net-1.4.1.tar.gz", out);
            if (result != 0) {
                throw new ClusterException("error at last line: " + out.toString());
            }

        }

        // copies the file from the local temp directory instead of
        // downloading it
        if (checkExistLocal("jakarta-oro-2.0.8.tar.gz")) {
            logger.info("found locally and copy to location");
            SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cp /tmp/jakarta-oro-2.0.8.tar.gz $HOME/binbase/tmp", out);
        } else {
            logger.info("not found locally, downloading it");

            result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(),
                    "cd $HOME/binbase/tmp && wget http://archive.apache.org/dist/jakarta/oro/source/jakarta-oro-2.0.8.tar.gz", out);
            if (result != 0) {
                throw new ClusterException("error at last line: " + out.toString());
            }
        }

        logger.info("create ant installation for the runtime");
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/tmp && unzip $HOME/binbase/tmp/apache-ant-1.6.5-bin.zip", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/tmp && cp -rdf apache-ant-1.6.5 $HOME/binbase/ant", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/tmp && gunzip $HOME/binbase/tmp/commons-net-1.4.1.tar.gz", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/tmp && tar -xf $HOME/binbase/tmp/commons-net-1.4.1.tar", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(),
                "cd $HOME/binbase/tmp && mv -f $HOME/binbase/tmp/commons-net-1.4.1/commons-net-1.4.1.jar $HOME/binbase/ant/lib/", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/tmp && gunzip $HOME/binbase/tmp/jakarta-oro-2.0.8.tar.gz", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/tmp && tar -xf $HOME/binbase/tmp/jakarta-oro-2.0.8.tar", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(),
                "cd $HOME/binbase/tmp && mv -f $HOME/binbase/tmp/jakarta-oro-2.0.8/jakarta-oro-2.0.8.jar $HOME/binbase/ant/lib/", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        logger.info("creating start script");

        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo ' #!/bin/sh\n' > binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo ' PATH=$HOME/binbase/ant/bin:$PATH\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'JAVA_HOME=`which java`\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo ' PATH=$JAVA_HOME:$PATH\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'JAVA_HOME=`dirname $JAVA_HOME`\n' >> binbase/start.sh", out);
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'ANT_HOME=$HOME/binbase/ant\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'export ANT_HOME PATH JAVA_HOME\n' >> binbase/start.sh", out);
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'cd $HOME/binbase/ClusterService/bin\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'if [ ! -n \"$1\" ]\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'then\n' >> binbase/start.sh", out);
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'echo no arguments just start one node\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(),
                "echo 'qsub -S /bin/bash -N BinBaseNode -V -o ../log -e ../log -cwd node.sh\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'exit 0\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'else\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'echo got arguments start $1 nodes\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(),
                "echo 'qsub -S /bin/bash -N BinBaseNode -V -o ../log -e ../log -cwd -t 1:$1 node.sh\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "echo 'fi\n' >> binbase/start.sh", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "mkdir $HOME/binbase/ClusterService/config", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "mkdir $HOME/binbase/ClusterService/log", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }

        logger.info("create configuration file for the application server");
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(),
                "cd $HOME/binbase/ClusterService && echo '<?xml version=\"1.0\" encoding=\"UTF-8\"?>' > config/applicationServer.xml", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/ClusterService && echo '<config>' >> config/applicationServer.xml", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/ClusterService && echo '<parameter>' >> config/applicationServer.xml", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        addParameterToConfig("java.naming.provider.url", applicationServer, out);
        addParameterToConfig("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory", out);
        addParameterToConfig("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces", out);
        addParameterToConfig("edu.ucdavis.genomics.metabolomics.util.status.ReportFactory",
                "edu.ucdavis.genomics.metabolomics.binbase.cluster.status.EJBReportFactory", out);
        addParameterToConfig("edu.ucdavis.genomics.metabolomics.util.thread.locking.LockableFactory",
                "edu.ucdavis.genomics.metabolomics.binbase.cluster.locking.EJBLockingFactory", out);

        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/ClusterService && echo '</parameter>' >> config/applicationServer.xml", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/ClusterService && echo '</config>' >> config/applicationServer.xml", out);

        logger.info("cleaning up");
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "rm -rdf $HOME/binbase/tmp", out);

        logger.info("done with installation");

        content.append(new String(out.toByteArray()));

        if (content.toString().indexOf("Build failed") > -1) {
            throw new ClusterException("was not able to initialize the cluster: " + content.toString());
        }

        return content.toString();

    }

    /**
     * checks that a file exist in the defined temp directory
     *
     * @param query
     * @return
     * @throws Exception
     */
    protected boolean checkExistLocal(final String query) throws Exception {
        logger.info(query);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "if [ -e /tmp/" + query + " ] ; then echo \"1\"; fi", out);
        if (new String(out.toByteArray()).trim().equals("1")) {
            return true;
        }
        return false;
    }

    private void addParameterToConfig(final String name, final String value, final ByteArrayOutputStream out) throws Exception, ClusterException {
        int result;
        result = SSHUtil.executeCommand(getServername(), getUsername(), getPassword(), "cd $HOME/binbase/ClusterService && echo '<param name=\"" + name + "\" value=\"" + value
                + "\"  public=\"true\"/>' >> config/applicationServer.xml", out);
        if (result != 0) {
            throw new ClusterException("error at last line: " + out.toString());
        }
    }

}
