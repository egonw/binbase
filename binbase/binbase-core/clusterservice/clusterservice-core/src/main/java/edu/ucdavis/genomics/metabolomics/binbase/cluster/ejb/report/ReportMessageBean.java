/*
 * Created on Aug 3, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.report;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx.ReportJMXMBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.status.EJBReport;
import edu.ucdavis.genomics.metabolomics.exception.CommunicationExcpetion;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import org.apache.log4j.Logger;
import org.jboss.mx.util.MBeanServerLocator;

import javax.ejb.*;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import java.io.Serializable;
import java.util.*;

/**
 * used to forward incomming message to all registered reports on the server
 * 
 * @author wohlgemuth
 * 
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/Reports") })
@ApplicationException(rollback = false)
public class ReportMessageBean implements MessageDrivenBean, MessageListener {

	private Logger logger = Logger.getLogger(ReportMessageBean.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private Set<Report> reports;

	private ReportJMXMBean bean;

	public void ejbRemove() throws EJBException {

	}

	public void setMessageDrivenContext(MessageDrivenContext arg0)
			throws EJBException {

	}

	/**
	 * iterator over reports and do actions
	 * 
	 * @author wohlgemuth
	 * @version Aug 3, 2006
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	@SuppressWarnings("unchecked")
	public void onMessage(Message arg0) {
		if (arg0 instanceof ObjectMessage) {
			try {
				Object o = ((ObjectMessage) arg0).getObject();

				if (o instanceof Map) {
					sync();

					if (bean.isEnableReporting()) {
						Iterator<Report> it = reports.iterator();
						Map<?, ?> map = (Map) o;

						// forward to all registered reports
						while (it.hasNext()) {
							try {

								String owner = (String) map.get("owner");
								Serializable id = (Serializable) map.get("id");
								ReportEvent event = (ReportEvent) map
										.get("event");
								ReportType type = (ReportType) map.get("type");
								String ipaddress = (String) map.get("ip");
								Date date = (Date) map.get("time");
								Exception ex = (Exception) map.get("ex");

								Report pro = it.next();

								if (pro instanceof EJBReport) {
									logger.warn("this report is a report which directly sends messages to this queue, it's ignored. Please check your configuration!");
								} else {
									pro.report(owner, id, event, type,
											ipaddress, date, ex);
								}
							} catch (Exception e) {
								logger.debug(e.getMessage(), e);
							}
						}
					}
					else{
						logger.debug("reporting is disabled in the configuration");
					}
				}
			}

			catch (Exception e) {
				logger.warn(e.getMessage(), e);
			}
		}
	}

	/**
	 * @ejb.create-method
	 * @throws CreateException
	 */
	public void ejbCreate() {

	}

	public void sync() throws CommunicationExcpetion {
		try {
			if (bean == null) {
				ObjectName name = new ObjectName(
						"binbase:service=RuntimeReport");
				MBeanServer server = MBeanServerLocator.locate();

				bean = (ReportJMXMBean) MBeanServerInvocationHandler
						.newProxyInstance(server, name, ReportJMXMBean.class,
								false);
			}

			Collection<String> classes = bean.getReports();
			this.reports = new HashSet<Report>(classes.size());

			Iterator<String> it = classes.iterator();

			while (it.hasNext()) {
				String providerName = (String) it.next();
				try {
					Class<?> clazz = Class.forName(providerName);
					Report pro = (Report) clazz.newInstance();
					this.reports.add(pro);
				} catch (Exception e) {
					logger.error("sorry could'nt create report for class: "
							+ providerName + " it will be skipped", e);
				}
			}
		} catch (Exception e) {
			throw new CommunicationExcpetion(e);
		}
	}
}
