package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.exception;

import javax.ejb.ApplicationException;

import edu.ucdavis.genomics.metabolomics.exception.LockingException;

@ApplicationException(rollback=true)
public class LockingFailedException extends LockingException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public LockingFailedException() {
		super();
		
	}

	public LockingFailedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public LockingFailedException(String arg0) {
		super(arg0);
		
	}

	public LockingFailedException(Throwable arg0) {
		super(arg0);
		
	}

}
