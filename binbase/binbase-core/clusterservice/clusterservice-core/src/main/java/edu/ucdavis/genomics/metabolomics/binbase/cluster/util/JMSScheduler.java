package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;
import org.apache.log4j.Logger;

import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.Serializable;
import java.rmi.RemoteException;

/**
 * jms based scheduler
 *
 * @author nase
 */
public class JMSScheduler {

    private static JMSScheduler instance = null;

    Logger logger = Logger.getLogger(getClass());

    private Queue queue;

    private QueueSession session;

    private QueueConnection queueConnection;

    public static JMSScheduler getInstance() {
        if (instance == null) {
            instance = new JMSScheduler();
        }
        return instance;
    }

    private void openSession() throws NamingException, JMSException,
            RemoteException, ClusterException {

        InitialContext ic = new InitialContext();

        // send to queue the parameter
        logger.debug("create queue factory");
        QueueConnectionFactory qcf = (QueueConnectionFactory) ic
                .lookup("QueueConnectionFactory");
        queueConnection = qcf.createQueueConnection();

        queue = (Queue) ic.lookup(Configurator.getConfigService().getQueue());
        logger.debug("connected to queue: " + queue);

        session = queueConnection.createQueueSession(false,
                javax.jms.Session.AUTO_ACKNOWLEDGE);

    }

    @Override
    protected void finalize() throws Throwable {
        closeSession();
        super.finalize();
    }

    private void closeSession() throws JMSException {

        if (session != null)
            session.close();

        if (queueConnection != null)
            queueConnection.close();

        queue = null;
        session = null;
        queueConnection = null;

    }

    /**
     * schedules something
     *
     * @param object
     * @param priority
     * @param handler
     * @throws SchedulingException
     */
    public final synchronized void schedule(Serializable object, int priority,
                                      String handler) throws SchedulingException {

        try {
            try {
                openSession();

                if (handler != null) {
                    Configurator.getConfigService().addHandler(
                            object.getClass().getName(), handler);
                }

                MessageProducer producer = session.createProducer(queue);
                producer.setPriority(priority);

                logger.debug("created producer");

                ObjectMessage message = session.createObjectMessage();
                message.setObject(object);

                logger.debug("send message: " + message + "with priority " + priority);
                producer.send(message);

                producer.close();

                logger.debug("create report");

                Report report = ReportFactory.newInstance().create("scheduler");
                report.report(object, Reports.SCHEDULE, Reports.JOB);

                logger.debug("scheduling done");

            } finally {
                closeSession();
            }
        } catch (Exception e) {
            throw new SchedulingException(e);
        }

    }
}
