package edu.ucdavis.genomics.metabolomics.binbase.cluster.report;

import java.io.Serializable;
import java.util.Date;

import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.apache.log4j.Logger;
import org.jboss.mx.util.MBeanServerLocator;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx.EmailNotifierJMXMBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx.NotifierJMXMBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx.TwitterNotifierJMXMBean;
import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import edu.ucdavis.genomics.metabolomics.util.status.notify.Log4JNotifier;
import edu.ucdavis.genomics.metabolomics.util.status.notify.Notifier;
import edu.ucdavis.genomics.metabolomics.util.status.notify.email.EmailNotifierImplementation;
import edu.ucdavis.genomics.metabolomics.util.status.notify.twitter.TwitterImplementation;

/**
 * needs to run on the server side!
 * @author wohlgemuth
 *
 */
public class NotifierReport extends Report {

	private NotifierJMXMBean bean;

	private Logger logger = Logger.getLogger(getClass());

	private Notifier notifier = new Log4JNotifier();

	public NotifierReport() throws MalformedObjectNameException, NullPointerException {
		MBeanServer server = MBeanServerLocator.locate();

		bean = (NotifierJMXMBean) MBeanServerInvocationHandler.newProxyInstance(server, new ObjectName("binbase.notify:service=Notifier"), NotifierJMXMBean.class,
				false);
	}

	@Override
	public String getOwner() {
		return "application-server";
	}

	@Override
	public void report(String owner, Serializable id, ReportEvent event, ReportType type, String ipaddress, Date time, Exception e) {

		if (bean.isEnableReportForwarding()) {

			String message = id + " - " + event.getName() + " " + type.getName();

			try {
				notifier.notify(message);
			}
			catch (NotificationException e1) {
				logger.warn(e1.getMessage(), e);
			}
			if (bean.isEnableEmail()) {

				try {
					MBeanServer server = MBeanServerLocator.locate();

					EmailNotifierJMXMBean email = (EmailNotifierJMXMBean) MBeanServerInvocationHandler.newProxyInstance(server, new ObjectName(
							"binbase.notify:service=EmailConfiguration"), EmailNotifierJMXMBean.class, false);

					new EmailNotifierImplementation(email).notify(message, event.getPriority());
				}
				catch (NotificationException e1) {
					logger.warn(e1.getMessage(), e1);

				}
				catch (MalformedObjectNameException e1) {
					logger.warn(e1.getMessage(), e1);

				}
				catch (NullPointerException e1) {
					logger.warn(e1.getMessage(), e1);

				}
			}
			else{

				logger.debug("email forwarding is disabled...");
			}
			if (bean.isEnableTwitter()) {

				try {
					MBeanServer server = MBeanServerLocator.locate();

					TwitterNotifierJMXMBean twitter = (TwitterNotifierJMXMBean) MBeanServerInvocationHandler.newProxyInstance(server, new ObjectName(
							"binbase.notify:service=TwitterConfiguration"), TwitterNotifierJMXMBean.class, false);

					
					new TwitterImplementation(twitter).notify(message, event.getPriority());
				}
				catch (NotificationException e1) {
					logger.warn(e1.getMessage(), e1);

				}
				catch (MalformedObjectNameException e1) {
					logger.warn(e1.getMessage(), e1);

				}
				catch (NullPointerException e1) {
					logger.warn(e1.getMessage(), e1);

				}
			}
			else{

				logger.debug("twitter forwarding is disabled...");
			}
		}
		else{
			logger.debug("forwarding is disabled...");
		}
	}

}
