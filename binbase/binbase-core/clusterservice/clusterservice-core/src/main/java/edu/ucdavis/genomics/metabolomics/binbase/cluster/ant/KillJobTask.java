/*
 * Created on Aug 2, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;

/**
 * kills a job or all the jobs of a user
 * 
 * @author wohlgemuth
 * @version Aug 2, 2006
 */
public class KillJobTask extends ClusterInitializeTask {
	int jobId = -1;

	public void doExecution(ClusterUtil util) throws Exception {
		if (jobId <= 0) {
			util.killJobOfCurrentUser();
		}
		else {
			util.killJob(jobId);
		}
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
}
