/*
 * Created on Apr 21, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.locking;

import java.io.Serializable;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.LockingService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.exception.LockingException;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.AbstractLocking;

/**
 * provides an ejb based locking and is basicly a wrapper to the ejb
 * 
 * @author wohlgemuth
 * @version Apr 21, 2006
 * 
 */
public class EJBLocking extends AbstractLocking {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private long id;

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @param owner
	 * @param p
	 * @param id
	 */
	public EJBLocking(String owner, long id) {
		super(owner);
		this.id = id;
	}

	/**
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @see edu.ucdavis.genomics.metabolomics.util.thread.locking.AbstractLocking#lock(java.io.Serializable)
	 */
	@Override
	protected boolean lock(Serializable o) throws LockingException {
		try {
			logger.debug("trying to lock: " + o);
			LockingService service = getService();
			return service.lock(id, o, getReport().getOwner());
		} catch (Exception e) {
			logger.warn("locking error for: " + o, e);
			return false;
		}
	}

	/**
	 * accessing the service
	 * 
	 * @return
	 * @throws NamingException
	 */
	protected LockingService getService() throws NamingException {
		return Configurator.getLockingService();
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @see edu.ucdavis.genomics.metabolomics.util.thread.locking.AbstractLocking#getSleepTime()
	 */
	@Override
	protected long getSleepTime() {
		try {
			return Configurator.getConfigService().getLockingInterval();
		} catch (Exception e) {
			logger.error(
					"there was an error, we are using the default sleep time: "
							+ e.getMessage(), e);
		}
		return (long) (10000);
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @see edu.ucdavis.genomics.metabolomics.util.thread.locking.Lockable#releaseRessource(java.io.Serializable)
	 */
	public void doRelease(Serializable o) throws LockingException {
		try {
			logger.debug("releasing resource: " + o);
			getService().releaseRessource(o);
		} catch (Exception e) {
			throw new LockingException(e);
		}
	}

	public long getTimeout() throws LockingException {
		try {
			return Configurator.getConfigService().getTimeout();
		} catch (Exception e) {
			throw new LockingException(e);
		}
	}

}
