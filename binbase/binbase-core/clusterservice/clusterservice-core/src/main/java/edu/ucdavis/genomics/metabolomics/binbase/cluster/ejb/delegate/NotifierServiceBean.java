package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx.NotifierJMXMBean;
import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import org.jboss.mx.util.MBeanServerLocator;

import javax.ejb.ApplicationException;
import javax.ejb.Stateless;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * access to the notification service
 * 
 * @author wohlgemuth
 */
@Stateless
@ApplicationException(rollback = false)
public class NotifierServiceBean implements NotifierService {

	NotifierJMXMBean bean;

	/* (non-Javadoc)
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.NotifierService#isEnableEmail()
	 */
	public boolean isEnableEmail() throws NotificationException {
		sync();
		return bean.isEnableEmail();
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.NotifierService#isEnableReportForwarding()
	 */
	public boolean isEnableReportForwarding() throws NotificationException {
		sync();
		return bean.isEnableReportForwarding();
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.NotifierService#isEnableTwitter()
	 */
	public boolean isEnableTwitter() throws NotificationException {
		sync();
		return bean.isEnableTwitter();
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.NotifierService#setEnableEmail(boolean)
	 */
	public void setEnableEmail(boolean enableEmail) throws NotificationException {
		sync();
		bean.setEnableEmail(enableEmail);
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.NotifierService#setEnableReportForwarding(boolean)
	 */
	public void setEnableReportForwarding(boolean enableReportForwarding) throws NotificationException {
		sync();
		bean.setEnableReportForwarding(enableReportForwarding);
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.NotifierService#setEnableTwitter(boolean)
	 */
	public void setEnableTwitter(boolean enableTwitter) throws NotificationException {
		sync();
		bean.setEnableTwitter(enableTwitter);
	}

	protected void sync() throws NotificationException {
		MBeanServer server = MBeanServerLocator.locate();

		try {

			bean = (NotifierJMXMBean) MBeanServerInvocationHandler.newProxyInstance(server, new ObjectName("binbase.notify:service=Notifier"),
					NotifierJMXMBean.class, false);

		}
		catch (MalformedObjectNameException e) {
			e.printStackTrace();
			throw new NotificationException(e);
		}
		catch (NullPointerException e) {
			e.printStackTrace();
			throw new NotificationException(e);
		}
	}

}
