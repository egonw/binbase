package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.SSHUtil;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import java.io.File;

public class SCPTask extends Task {

	@Override
	public void execute() throws BuildException {

		if (this.server == null) {
			throw new BuildException("please set the server");
		}
		if (this.username == null) {
			throw new BuildException("please set the username");
		}
		if (this.password == null) {
			throw new BuildException("please set the password");
		}
		if (this.file == null) {
			throw new BuildException("please set the file");
		}
		if (this.destinationDir == null) {
			throw new BuildException("please set the destination dir");
		}

		try {

				SSHUtil.scpToFile(new FileSource(new File(this.file)), getServer(),username,password, destinationDir + "/" + new File(this.file).getName());

		} catch (Exception e) {
			throw new BuildException(e);
		}

	}

	private String server;

	private String username;

	private String password;

	private String file;

	private String destinationDir;

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getDestinationDir() {
		return destinationDir;
	}

	public void setDestinationDir(String destinationDir) {
		this.destinationDir = destinationDir;
	}
}
