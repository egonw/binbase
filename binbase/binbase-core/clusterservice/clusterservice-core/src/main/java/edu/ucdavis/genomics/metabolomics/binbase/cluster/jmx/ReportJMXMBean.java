/*
 * Generated file - Do not edit!
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

/**
 * MBean interface.
 * 
 * @author wohlgemuth
 */
public interface ReportJMXMBean extends javax.management.MBeanRegistration {

	void addReport(java.lang.String providerClass);

	void removeReport(java.lang.String providerClass);

	java.util.Collection<String> getReports();

	boolean isReportRegistered(java.lang.String providerClass);

	boolean isEnableReporting();

	void setEnableReporting(boolean enableMonitoring);

}
