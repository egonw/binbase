package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb;

import java.io.Serializable;
import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.Remote;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.exception.LockingException;

@Remote
public interface LockingService {

	public abstract void releaseRessource(Serializable o) throws LockingException, EJBException, RemoveException, FinderException, NamingException;

	public abstract boolean lock(long id, Serializable o, String ipAddress) throws CreateException, NamingException, EJBException, RemoveException, FinderException,
			LockingException;

	public abstract Collection<Serializable> getLockedRessourcesInformation() throws FinderException, NamingException;

	public abstract Collection<Serializable> getLockedRessources() throws FinderException, NamingException;

	public abstract void releaseAllRessources() throws LockingException, EJBException, RemoveException, FinderException, NamingException;

}