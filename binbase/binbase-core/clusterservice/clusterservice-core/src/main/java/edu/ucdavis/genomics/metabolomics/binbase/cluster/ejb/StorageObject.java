package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb;

import java.io.Serializable;

public class StorageObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public StorageObject(){
		
	}

	public StorageObject(Serializable object){
		this.setObject(object);
	}

	private Serializable object;

	public Serializable getObject() {
		return object;
	}

	public void setObject(Serializable object) {
		this.object = object;
	}
}
