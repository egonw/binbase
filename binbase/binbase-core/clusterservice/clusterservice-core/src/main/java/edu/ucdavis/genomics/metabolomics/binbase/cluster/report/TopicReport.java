package edu.ucdavis.genomics.metabolomics.binbase.cluster.report;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.status.JMSProvider;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.status.JMSTopicProvider;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;

/**
 * sends events to a topic so that other people can monitor them
 * 
 * @author wohlgemuth
 * 
 */
public class TopicReport extends Report {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public String getOwner() {
		return "application server";
	}

	@Override
	public void report(String owner, Serializable id, ReportEvent event,
			ReportType type, String ipaddress, Date time, Exception e) {

		try {

			JMSProvider provider = JMSTopicProvider.getInstance();

			HashMap<String, Serializable> map = new HashMap<String, Serializable>();

			map.put("owner", owner);
			map.put("id", id);
			map.put("event", event);
			map.put("type", type);
			map.put("ip", ipaddress);
			map.put("time", time);
			map.put("exception", e);

			provider.sendMessage(map);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
	}
}
