/*
 * Created on Jul 13, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster;

import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * some predefined reports for the visualisation of the node
 * @author wohlgemuth
 * @version Jul 31, 2006
 *
 */
public interface NodeReports extends Reports {
	ReportType NODE = new ReportType("node", "a node of a cluster",Priority.DEBUG);

	ReportEvent STARTUP = new ReportEvent("startup", "something starts up",Priority.DEBUG);

	ReportEvent SHUTDOWN = new ReportEvent("shutdown", "a shutdown is requested",Priority.DEBUG);

	ReportEvent RECEIVE_MESSAGE = new ReportEvent("start calculation", "node received a message to process",Priority.INFO);
	
	ReportEvent FINISHED_MESSAGE = new ReportEvent("finished calculation", "node finished the message",Priority.INFO);
	
}
