package edu.ucdavis.genomics.metabolomics.binbase;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import org.junit.Test;

import javax.naming.NamingException;

import static org.junit.Assert.assertTrue;

/**
 * just a test to validate that objects are not nullt and can be looked up
 * @author wohlgemuth
 *
 */
public class EjbIntegrationTest extends AbstractApplicationServerTest {

	@Test
	public void testGetConfigService() throws NamingException {
		assertTrue(Configurator.getConfigService() != null);
	}

	@Test
	public void testGetReportService() throws NamingException {
		assertTrue(Configurator.getReportService() != null);
	}

	@Test
	public void testGetLockingService() throws NamingException {
		assertTrue(Configurator.getLockingService() != null);
	}

	@Test
	public void testGetStorageService() throws NamingException {
		assertTrue(Configurator.getStorageService() != null);
	}

}
