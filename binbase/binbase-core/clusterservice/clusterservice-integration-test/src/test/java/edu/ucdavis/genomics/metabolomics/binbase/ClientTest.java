package edu.ucdavis.genomics.metabolomics.binbase;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Assert;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;

/**
 * just tests if the server is correctly bound
 * 
 * @author wohlgemuth
 * 
 */
public class ClientTest extends AbstractApplicationServerTest {
	@Test
	public void testRemote() throws NamingException {
		@SuppressWarnings("unused")
		StorageService service = Configurator.getStorageService();
	}

	@Test
	public void testBasicRemote() throws NamingException {
		@SuppressWarnings("unused")
		InitialContext context = new InitialContext();

		// this should never work...
		boolean worked = false;
		try {
			context.lookup("clusterservice-ear-1.0-SNAPSHOT/StorageServiceBean/remote");
			worked = true;
			System.out.println("is not supposed to work! since the call has to be independent of the used archive");
		} catch (Exception e) {
			worked = false;
		}

		Assert.assertTrue(worked == false);
	}

}
