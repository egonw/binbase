package edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.local;

import org.junit.BeforeClass;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractClusterIntegrationTest;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.LocalCalculationFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.LocalCalculationUtil;
import edu.ucdavis.genomics.metabolomics.util.PropertySetter;

public class LocalClusterIntegrationTest  extends AbstractClusterIntegrationTest {
	
	/**
	 * time to setup the utility
	 * @throws Exception
	 */
	@BeforeClass
	public static void initializeCluster() throws Exception {
		PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");
		UTIL = new LocalCalculationUtil();
		
		AbstractClusterIntegrationTest.initializeUtil();
	}

	@Override
	protected int countOfJobs() {
		return 5;
	}

	@Override
	protected String getClusterFactoryClass() {
		return LocalCalculationFactory.class.getName();
	}

	@Override
	protected int getSleepTime() {
		return 100;
	}
}