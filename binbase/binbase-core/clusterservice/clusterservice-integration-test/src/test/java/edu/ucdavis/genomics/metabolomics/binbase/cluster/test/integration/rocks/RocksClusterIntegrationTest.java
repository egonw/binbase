package edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.rocks;

import org.junit.BeforeClass;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractClusterIntegrationTest;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.RocksClusterFactoryImpl;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.RocksClusterImplementation;
import edu.ucdavis.genomics.metabolomics.util.PropertySetter;

/**
 * runs an integration test with the rocks cluster
 * 
 * @author wohlgemuth
 */
public class RocksClusterIntegrationTest extends AbstractClusterIntegrationTest {

	/**
	 * time to setup the utility
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void initializeCluster() throws Exception {
		PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");
		UTIL = new RocksClusterImplementation(System.getProperty("test.binbase.cluster.server"), System.getProperty("test.binbase.cluster.username"), System
				.getProperty("test.binbase.cluster.password"));

		AbstractClusterIntegrationTest.initializeUtil();
	}

	@Override
	protected int countOfJobs() {
		return 5;
	}

	@Override
	protected String getClusterFactoryClass() {
		return RocksClusterFactoryImpl.class.getName();
	}
}
