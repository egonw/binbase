/*
 * Created on Apr 21, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.locking;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.AbstractLockableTest;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.Lockable;

public class EJBLockingTest extends AbstractLockableTest {

	public EJBLockingTest() {
		super();
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();

		AbstractApplicationServerTest delegate = new AbstractApplicationServerTest();
		AbstractApplicationServerTest.initializeParameters();
		delegate.setUp();
		
		Configurator.getLockingService().releaseAllRessources();
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
		Configurator.getLockingService().releaseAllRessources();
	}

	@Override
	protected Lockable getLockable() {
		return EJBLockingFactory.newInstance(EJBLockingFactory.class.getName()).create(Thread.currentThread().getName(), System.getProperties());
	}

	@Override
	protected int getLoad() {
		return 30;
	}

}
