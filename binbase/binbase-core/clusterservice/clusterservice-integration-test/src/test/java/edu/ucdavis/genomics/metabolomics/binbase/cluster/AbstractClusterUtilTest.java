package edu.ucdavis.genomics.metabolomics.binbase.cluster;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.JobInformation;

/**
 * base testing class
 * 
 * @author wohlgemuth
 * 
 */
public abstract class AbstractClusterUtilTest {

	private Logger logger = Logger.getLogger(getClass());
	protected static ClusterUtil UTIL = null;

	@Before
	public void setUp() throws Exception {
		// kill all jobs just in case
		UTIL.prepare();

		UTIL.killJobOfCurrentUser();
		waitForActionDelete();
	}

	@After
	public void tearDown() {
		try {
			new File(System.getProperty("user.home") + File.separatorChar
					+ ".config/applicationServer.xml").delete();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Test(timeout = 100000)
	public void testGetQueueCurrentUser() throws Exception {
		getLogger().info("killing jobs");
		UTIL.killJobOfCurrentUser();

		waitForActionDelete();

		List<JobInformation> jobs = UTIL.getQueueCurrentUser();
		assertTrue(jobs.isEmpty());

		getLogger().info("starting node");
		UTIL.startNode();

		getLogger().info("waiting till started");

		waitForActionPending();

		getLogger().info("get current queue");

		jobs = UTIL.getQueueCurrentUser();

		assertFalse(jobs.isEmpty());
		assertTrue(jobs.size() == 1);

		getLogger().info("killing jobs");

		UTIL.killJobOfCurrentUser();
	}

	@Test
	public void testExecuteCommand() throws Exception {
		String result = UTIL.executeCommand("ls -l $HOME/cluster");
		getLogger().info("result was: " + result);
		String[] res = result.split("\n");

		assertTrue(res.length == 4);
		assertTrue(res[1].indexOf("ant") > -1);
		assertTrue(res[2].indexOf("ClusterService") > -1);
		assertTrue(res[3].indexOf("start.sh") > -1);

	}

	@Test(timeout = 40000)
	public void testKillJobOfCurrentUser() throws Exception {
		UTIL.killJobs(System.getProperty("test.binbase.cluster.username"));
		waitForActionDelete();
		assertTrue(UTIL.getQueueByUser(
				System.getProperty("test.binbase.cluster.username")).isEmpty());
	}

	@Test(timeout = 400000)
	public void testGetClusterInformation() throws Exception {
		UTIL.startNode();
		waitForActionPending();
		assertTrue(UTIL.getClusterInformation().isEmpty() == false);
	}

	@Test(timeout = 100000)
	public void testGetClusterInformationByUser() throws Exception {
		UTIL.startNode();
		waitForActionPending();
		assertTrue(UTIL.getClusterInformationByUser(
				System.getProperty("test.binbase.cluster.username")).isEmpty() == false);
	}

	@Test(timeout = 40000)
	public void testGetClusterInformationCurrentUser() throws Exception {
		UTIL.startNode();
		waitForActionPending();
		assertTrue(UTIL.getClusterInformationCurrentUser().isEmpty() == false);
	}

	@Test(timeout = 100000)
	public void testStartNode() throws Exception {
		logger.info("killing jobs...");

		UTIL.killJobOfCurrentUser();
		logger.info("wait for killing...");

		waitForActionDelete();
		assertTrue(UTIL.getQueueCurrentUser().size() == 0);
		logger.info(UTIL.startNode());
		waitForActionPending();
		assertTrue(UTIL.getQueueCurrentUser().size() == 1);
	}

	private void waitForActionDelete() throws Exception, InterruptedException {
		getLogger().info("waiting till queue is deleted");
		while (UTIL.getQueueCurrentUser().isEmpty() != true) {
			Thread.sleep(getPendingTime());
			getLogger().info(
					"waiting, remaining: " + UTIL.getQueueCurrentUser());
		}
		getLogger().info("waiting a bit just to make sure...");
		Thread.sleep(getWaitTime());
		getLogger().info("queue is empty");

	}

	protected int getPendingTime() {
		return 1000;
	}

	private void waitForActionPending() throws Exception, InterruptedException {
		getLogger().info("waiting till all jobs are started");

		while (UTIL.getPendingQueueByCurrentUser().isEmpty() != true) {
			Thread.sleep(getPendingTime());
			getLogger().info("waiting...");
		}
		getLogger().info(UTIL.getQueueCurrentUser());
		getLogger().info("waiting a bit just to make sure...");
		Thread.sleep(getWaitTime());
		getLogger().info("jobs are started");

		getLogger().info(UTIL.getQueueCurrentUser());
	}

	@Test
	public void testGetNodeCount() throws Exception {
		getLogger().info(UTIL.getNodeCount());
		assertTrue(UTIL.getNodeCount() == UTIL.getFreeNodeCount()
				+ UTIL.getUsedNodeCount());
	}

	@Test(timeout = 100000)
	public void testGetFreeNodeCount() throws Exception {
		UTIL.killJobOfCurrentUser();
		waitForActionDelete();

		assertTrue(UTIL.getFreeNodeCount() == (UTIL.getNodeCount() - UTIL
				.getUsedNodeCount()));
		UTIL.startNode();

		waitForActionPending();
		assertTrue(UTIL.getFreeNodeCount() == (UTIL.getNodeCount() - UTIL
				.getUsedNodeCount()));

		UTIL.killJobOfCurrentUser();
		waitForActionDelete();
		UTIL.startNode();

		waitForActionPending();
		assertTrue(UTIL.getFreeNodeCount() == (UTIL.getNodeCount() - UTIL
				.getUsedNodeCount()));

	}

	@Test(timeout = 100000)
	public void testGetUsedNodeCount() throws Exception {
		UTIL.killJobOfCurrentUser();
		waitForActionDelete();
		assertTrue(UTIL.getUsedNodeCount() == 0);

		UTIL.startNode();

		waitForActionPending();

		assertTrue(UTIL.getUsedNodeCount() == UTIL.getNodeCount()
				- UTIL.getFreeNodeCount());
	}

	@Test
	public void testGetSlotCount() throws Exception {
		assertTrue(UTIL.getSlotCount() == UTIL.getFreeSlotCount()
				+ UTIL.getUsedSlotCount());
	}

	@Test(timeout = 100000)
	public void testGetFreeSlotCount() throws Exception {
		UTIL.killJobOfCurrentUser();
		waitForActionDelete();

		assertTrue(UTIL.getFreeSlotCount() == UTIL.getSlotCount()
				- UTIL.getUsedNodeCount());
		UTIL.startNode();
		waitForActionPending();
		assertTrue(UTIL.getFreeSlotCount() == UTIL.getSlotCount()
				- UTIL.getUsedNodeCount());
	}

	@Test(timeout = 100000)
	public void testGetUsedSlotCount() throws Exception {
		UTIL.killJobOfCurrentUser();

		waitForActionDelete();
		assertTrue(UTIL.getQueueCurrentUser().size() == 0);
		UTIL.startNode();

		waitForActionPending();

		assertTrue(UTIL.getUsedSlotCount() == 1);
	}

	@Test(timeout = 100000)
	public void testGetInfoLogForJobString() throws Exception {
		UTIL.killJobOfCurrentUser();
		waitForActionDelete();
		String id = UTIL.startNode();

		waitForActionPending();
		String jobId = "";

		for (String v : id.split("\\s")) {
			try {
				jobId = "" + Integer.parseInt(v);
				break;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		logger.info("id is: " + jobId);

		// lets wait a little so the node can do something
		Thread.sleep(getWaitTime());

		String content = UTIL.getInfoLogForJob(jobId);

		getLogger().info(content);
		assertTrue(content.length() > 0);
	}

	protected int getWaitTime() {
		return 5000;
	}

	@Test
	public void testGetErrorLogForJobString() throws Exception {
		UTIL.killJobOfCurrentUser();
		waitForActionDelete();
		String id = UTIL.startNode();

		waitForActionPending();
		String jobId = "";

		for (String v : id.split("\\s")) {
			try {
				jobId = "" + Integer.parseInt(v);
				break;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		logger.info("id is: " + jobId);

		// lets wait a little so the node can do something
		Thread.sleep(getWaitTime());

		UTIL.getErrorLogForJob(jobId);
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Logger getLogger() {
		return logger;
	}

}
