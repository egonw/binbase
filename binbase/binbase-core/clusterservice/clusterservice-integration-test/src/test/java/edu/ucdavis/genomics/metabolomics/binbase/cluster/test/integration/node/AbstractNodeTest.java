package edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.node;

import java.util.List;
import java.util.Vector;

import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.naming.InitialContext;

import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.AbstracNode;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;

import static org.junit.Assert.*;

/**
 * test to see if a node can start
 * 
 * @author wohlgemuth
 * 
 */
public class AbstractNodeTest extends AbstractApplicationServerTest {

	/**
	 * actually runs the node in its own thread
	 * @throws Exception
	 */
	@Test(timeout=100000)
	public void testThreadedNode() throws Exception {
		this.emptyMessageQueue();
		logger.info("schedule message");
		Scheduler.schedule("A", Scheduler.PRIORITY_LOW);
		Thread.sleep(2000);

		logger.info("schedule message");
		Scheduler.schedule("B", Scheduler.PRIORITY_LOWEST);
		Thread.sleep(2000);

		logger.info("schedule message");
		Scheduler.schedule("C", Scheduler.PRIORITY_HIGH);
		Thread.sleep(2000);

		logger.info("schedule message");
		Scheduler.schedule("D", Scheduler.PRIORITY_ASAP);

		logger.info("sleep for 20 seconds...");
		Thread.sleep(20000);

		logger.info("sleep for 20 seconds...");
		Thread.sleep(20000);
		assertTrue(this.getMessageQueue().size() == 4);

		final List<String> result = new Vector<String>();

		AbstracNode node = new AbstracNode() {

			@Override
			protected Queue initializeQueue(InitialContext context) throws Exception {
				String queue = getConfig().getQueue();

				logger.info("connection to queue: " + queue);
				return (Queue) context.lookup(queue);
			}

			@Override
			protected void onMessage(Message message) throws Exception {
				logger.info("received object: " + ((ObjectMessage) message).getObject());
				result.add((String) ((ObjectMessage) message).getObject());
			}

		};

		node.setName("test - " + System.currentTimeMillis());
		node.setSingleRun(false);
		node.start();

		waitForMessageQueue();
		
		//wait for the shutdown of the node
		node.shutdown();
		

		logger.info("sleep for 20 seconds...");
		Thread.sleep(20000);
		
		//check results
		logger.info("result content: " +  result);
		logger.info("result size: " +  result.size());
		assertTrue(result.size() == 4);
		assertTrue(result.contains("A"));
		assertTrue(result.contains("B"));
		assertTrue(result.contains("C"));
		assertTrue(result.contains("D"));

		assertTrue(result.indexOf("A") == 2);
		assertTrue(result.indexOf("B") == 3);
		assertTrue(result.indexOf("C") == 1);
		assertTrue(result.indexOf("D") == 0);
		
	}

	/**
	 * just runs the node and does not start it
	 * @throws Exception
	 */
	@Test(timeout=100000)
	public void testNonThreadedNode() throws Exception {
		this.emptyMessageQueue();
		logger.info("schedule message");
		Scheduler.schedule("A", Scheduler.PRIORITY_LOW);
		Thread.sleep(2000);

		logger.info("schedule message");
		Scheduler.schedule("B", Scheduler.PRIORITY_LOWEST);
		Thread.sleep(2000);

		logger.info("schedule message");
		Scheduler.schedule("C", Scheduler.PRIORITY_HIGH);
		Thread.sleep(2000);

		logger.info("schedule message");
		Scheduler.schedule("D", Scheduler.PRIORITY_ASAP);

		logger.info("sleep for 20 seconds...");
		Thread.sleep(20000);
		//check results
		logger.info("queue content: " +  this.getMessageQueue());
		logger.info("queue size: " +  this.getMessageQueue().size());
		assertTrue(this.getMessageQueue().size() == 4);

		final List<String> result = new Vector<String>();

		AbstracNode node = new AbstracNode() {

			@Override
			protected Queue initializeQueue(InitialContext context) throws Exception {
				String queue = getConfig().getQueue();

				logger.info("connection to queue: " + queue);
				return (Queue) context.lookup(queue);
			}

			@Override
			protected void onMessage(Message message) throws Exception {
				logger.info("received object: " + ((ObjectMessage) message).getObject());
				result.add((String) ((ObjectMessage) message).getObject());
			}

		};

		node.setName("test - " + System.currentTimeMillis());
		node.setSingleRun(true);

		//since the nod wll always shutdown after one run we need to specify 4 nodes
		logger.info("starting first node");
		node.run();
		logger.info("starting second node");
		node.run();
		logger.info("starting third node");
		node.run();
		logger.info("starting fourth node");
		node.run();

		assertTrue(result.size() == 4);
		assertTrue(result.contains("A"));
		assertTrue(result.contains("B"));
		assertTrue(result.contains("C"));
		assertTrue(result.contains("D"));

		assertTrue(result.indexOf("A") == 2);
		assertTrue(result.indexOf("B") == 3);
		assertTrue(result.indexOf("C") == 1);
		assertTrue(result.indexOf("D") == 0);
	}

}
