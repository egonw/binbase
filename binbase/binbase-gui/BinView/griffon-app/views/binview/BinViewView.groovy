package binview

import edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.plot.SpectraChart
import net.miginfocom.swing.MigLayout
import org.jfree.chart.ChartPanel
import org.jfree.chart.JFreeChart
import org.jfree.chart.plot.XYPlot
import org.jfree.chart.renderer.xy.XYBarRenderer

import javax.swing.*
import javax.swing.event.ListSelectionListener
import javax.swing.table.TableColumnModel
import javax.swing.table.TableModel
import java.awt.*
import java.awt.event.*

import static java.awt.BorderLayout.*

//calculate the size of the window for login
def tk = Toolkit.getDefaultToolkit()
def width = 320
int height = 120
def x = (int) ((tk.getScreenSize().width - width) / 2)
def y = (int) ((tk.getScreenSize().height - height) / 2)

//create a dialog for the selection of the database
dialog(
        id: "selectDatabase",
        title: 'Database Selection Configuration',
        size: [width, height],
        location: [x, y],
        modal: true,
        layout: new MigLayout('insets 10')
) {
    label("database", constraints: 'split, span')
    separator(constraints: 'growx, wrap')

    comboBox(id: "selectDatabaseValue", items: model.databases, constraints: 'growx')

    button(id: "selectDatabaseButton", constraints: 'wrap', action: action(name: 'select', closure: {

        //tell the controller to define the application server
        controller.selectDB(selectDatabaseValue.selectedItem)

        //disable the dialog
        selectDatabase.visible = false

    }))

}

//builds the actual view
application(id: 'application', title: 'BinView',

        location: [50, 50],
        //pack: true,
        extendedState: Frame.MAXIMIZED_BOTH,
        locationByPlatform: true,
        iconImage: imageIcon('/griffon-icon-48x48.png').image,
        iconImages: [imageIcon('/griffon-icon-48x48.png').image,
                imageIcon('/griffon-icon-32x32.png').image,
                imageIcon('/griffon-icon-16x16.png').image]
) {

//our actions
    actions {
        /**
         * ends the application
         */
        action(id: 'exitAction',
                name: 'Quit',
                closure: controller.quit)

        /**
         * selects a new database
         */
        action(id: 'selectDatabaseAction',
                name: 'Select Database',
                closure: controller.changeDB)

    }

    /**
     * the menu bar to use
     */
    menuBar(id: 'menuBar') {

        //file menu
        menu(text: 'File', mnemonic: 'F') {
            menuItem(selectDatabaseAction)

            menuItem(exitAction)
        }

        /**
         * all accessible queries
         */
        menu(text: 'Database Queries', mnemonic: 'Q') {
            menuItem(
                    action(
                            name: 'Query All Bins',
                            closure: {
                                controller.displayCustomBinQuery("select name, bin_id,spectra,apex, retention_index,uniquemass,group_id from bin a")
                            }
                    )

            )
            separator()

            menuItem(
                    action(
                            name: 'Query All Bins with a group',
                            closure: {
                                controller.displayCustomBinQuery("select name, bin_id,spectra,apex, retention_index,uniquemass,group_id from bin a where group_id is null")
                            }
                    )
            )

            menuItem(
                    action(
                            name: 'Query All Bins without a group',
                            closure: {
                                controller.displayCustomBinQuery("select name, bin_id,spectra,apex, retention_index,uniquemass,group_id from bin a where group_id is not null")
                            }
                    )
            )

            separator()

            menuItem(
                    action(
                            name: 'Query all Bins without InChI Key',
                            closure: {
                                controller.displayCustomBinQuery("select name, bin_id,spectra,apex, retention_index,uniquemass,group_id from bin a where inchi_key is null")
                            }
                    )
            )

            menuItem(
                    action(
                            name: 'Query all Bins with InChI Key',
                            closure: {
                                controller.displayCustomBinQuery("select name, bin_id,spectra,apex, retention_index,uniquemass,group_id from bin a where inchi_key is not null")
                            }
                    )
            )

            separator()

            menuItem(
                    action(
                            name: 'Query all Bins with Name',
                            closure: {
                                controller.displayCustomBinQuery("select a.name, a.bin_id,spectra,apex, retention_index,uniquemass, a.group_id from bin a where a.name != cast(a.bin_id as varchar)")
                            }
                    )
            )


            menuItem(
                    action(
                            name: 'Query all Bins without Name',
                            closure: {
                                controller.displayCustomBinQuery("select a.name, a.bin_id,spectra,apex, retention_index,uniquemass, a.group_id from bin a where a.name = cast(a.bin_id as varchar)")
                            }
                    )
            )

            menuItem(
                    action(
                            name: 'Query All Bins with Library Match',
                            closure: {
                                controller.displayCustomBinQuery("select e.name, e.bin_id,spectra,apex, retention_index,uniquemass, e.group_id from bin e where e.bin_id in ( select distinct bin_id from library_match a, library_spec b, library c where b.id = c.id and a.spectra_id = b.\"specId\" and similarity between ? and ? and retention_index_diff < ? and c.name = ?)", [model.minSimilarity, model.maxSimilarity, model.riWindow, model.selectedLibrary])
                            }
                    )
            )
            menuItem(
                    action(
                            name: 'Query All Bins with Library Match and no name',
                            closure: {
                                controller.displayCustomBinQuery("select e.name, e.bin_id,spectra,apex, retention_index,uniquemass, e.group_id from bin e where  e.name != cast(e.bin_id as varchar) and e.bin_id in ( select distinct bin_id from library_match a, library_spec b, library c where b.id = c.id and a.spectra_id = b.\"specId\" and similarity between ? and ? and retention_index_diff < ? and c.name = ?)", [model.minSimilarity, model.maxSimilarity, model.riWindow, model.selectedLibrary])
                            }
                    )
            )

            separator()

            menuItem(
                    action(
                            name: 'Query All Bins by Id',
                            closure: {

                                dialog(
                                        id: "queryBinByIdDialog",
                                        title: 'Query Bins by id',
                                        size: [width, height],
                                        location: [x, y],
                                        modal: true,
                                        layout: new MigLayout('insets 10')
                                ) {
                                    label("Id", constraints: 'split, span')
                                    separator(constraints: 'growx, wrap')

                                    textField(id: "value", constraints: 'growx', columns: 100)

                                    button(constraints: 'wrap', action: action(name: 'execute query', closure: {
                                        controller.displayAllBinsById(value.text)
                                        queryBinByIdDialog.visible = false

                                    }))

                                }.show()
                            })
            )
            menuItem(
                    action(
                            name: 'Query All Bins by Name',
                            closure: {

                                dialog(
                                        id: "queryBinByNameDialog",
                                        title: 'Query Bins by name',
                                        size: [width, height],
                                        location: [x, y],
                                        modal: true,
                                        layout: new MigLayout('insets 10')
                                ) {
                                    label("Name", constraints: 'split, span')
                                    separator(constraints: 'growx, wrap')

                                    textField(id: "value", constraints: 'growx', columns: 100)

                                    button(constraints: 'wrap', action: action(name: 'execute query', closure: {
                                        controller.displayAllBinsByName("%" + value.text + "%")

                                        queryBinByNameDialog.visible = false

                                    }))

                                }.show()
                            })
            )

            menuItem(
                    action(
                            name: 'Query All Bins by Retention Index Range',
                            closure: {

                                def dialog = dialog(
                                        id: "queryBinByRiRangeDialog",
                                        title: 'Query Bins by retention index range',
                                        size: [width, height],
                                        modal: true,
                                        layout: new MigLayout('insets 10'), pack: true
                                ) {
                                    label("From", constraints: 'split, span')
                                    separator(constraints: 'growx, wrap')

                                    textField(id: "from", constraints: 'growx,wrap', columns: 100)

                                    label("To", constraints: 'split, span')
                                    separator(constraints: 'growx, wrap')

                                    textField(id: "to", constraints: 'growx', columns: 100)

                                    separator(constraints: 'growx, wrap')


                                    button(constraints: 'wrap', action: action(name: 'execute query', closure: {
                                        controller.displayCustomBinQuery("select name, bin_id,spectra,apex, retention_index,uniquemass,group_id from bin a where retention_index between ? and ?", [Integer.parseInt(from.text), Integer.parseInt(to.text)])

                                        queryBinByRiRangeDialog.visible = false

                                    }))

                                }

                                dialog.pack()
                                dialog.setLocation((int) ((tk.getScreenSize().width - dialog.getWidth()) / 2), (int) (tk.getScreenSize().height - dialog.getHeight()) / 2)
                                dialog.show()
                            })
            )
        }


    }

    /**
     * application
     */
    panel() {

        borderLayout()

        /**
         * general i
         */
        panel(constraints: NORTH) {
            borderLayout()

            panel(constraints: EAST, layout: new MigLayout('insets 10')) {

                label(text: "Count of Bin's in database:", constraints: 'growx')
                label(id: "binCount", text: "no database selected", constraints: 'growx')

                label(text: "Current database:", constraints: 'gap 20px, growx')
                label(id: "currentDatabase", text: "no database selected", constraints: 'wrap')


            }
        }

        //main view
        splitPane(dividerLocation: 280) {

            panel() {

                borderLayout()

                /**
                 * scroll panel for the table containing all bins
                 */
                scrollPane(constraints: CENTER, preferredSize: [160, -1]) {

                    table(id: "bintable") {
                        createTableModel(model)

                        bintable.selectionModel.addListSelectionListener({ evt ->

                            //tell the controller to execute the disply action
                            controller.displayBin(bintable.selectedElement, evt)
                        } as ListSelectionListener)
                    }

                    header = bintable.getTableHeader()
                    header.setUpdateTableInRealTime(false)

                    /**
                     * mouse listener to take care of the sorting of the table
                     */
                    header.addMouseListener({
                        mouseClicked: {

                            if (it.clickCount == 1 && it.ID == MouseEvent.MOUSE_RELEASED) {

                                try {

                                    TableColumnModel colModel = bintable.getColumnModel()
                                    int columnModelIndex = colModel.getColumnIndexAtX(it.getX())


                                    field = model.headerRowMap.get(bintable.getColumnName(columnModelIndex))

                                    if (field) {
                                        log.info("sorting by: ${field}")
                                        model.sortingColumn = field

                                        if (model.sortingDirection.equals("ASC")) {
                                            model.sortingDirection = "DESC"
                                        } else {
                                            model.sortingDirection = "ASC"
                                        }

                                        log.info("sorting by direction: ${model.sortingDirection}")

                                        controller.resetPagination()
                                        controller.executeLastQuery(false)

                                        //WHY is this here? bintable.get
                                    } else {
                                        log.warn("sorry not a sortable field: ${bintable.getColumnName(columnModelIndex)}")
                                    }
                                }
                                catch (Exception e) {
                                    log.debug("ignore: ${e.getMessage()}", e)
                                }
                            }
                        }

                    } as MouseListener)

                    header.setReorderingAllowed(false);

                    /**
                     * mouse listener which takes car of the popup menu
                     */
                    bintable.addMouseListener({

                        mousePressed: {
                            if (it.isPopupTrigger()) {

                                popupMenu {
                                    menuItem {
                                        action(id: "createGroup", name: "group selected bin's ", closure: {


                                            String name = "please provide a name"

                                            Collection<Bin> bins = bintable.selectedElements

                                            bins.each { Bin bin ->

                                                if (bin.group != "none") {
                                                    name = bin.group
                                                }
                                            }




                                            dialog(
                                                    id: "binGroupDialog",
                                                    title: 'Define Bin Group Name',
                                                    size: [width, height],
                                                    location: [x, y],
                                                    modal: true,
                                                    layout: new MigLayout('insets 10')
                                            ) {
                                                label("Group", constraints: 'split, span')
                                                separator(constraints: 'growx, wrap')

                                                textField(id: "value", constraints: 'growx', columns: 100, text: name)

                                                button(constraints: 'wrap', action: action(name: 'define group', closure: {

                                                    controller.executeBinGrouping(bins, value.text)


                                                    binGroupDialog.visible = false

                                                }))

                                            }.show()

                                        })
                                        createGroup.setEnabled(bintable.selectedElements.size() >= 2)

                                    }
                                    menuItem {

                                        action(id: "deleteGroup", name: "delete selected group", closure: {
                                            controller.deleteBinGroup(bintable.selectedElements.get(0).group)
                                        })
                                        deleteGroup.setEnabled(bintable.selectedElements.size() == 1 && bintable.selectedElements.get(0).group != "none")
                                    }
                                    menuItem {

                                        action(id: "removeFromGroup", name: "remove from group", closure: {
                                            controller.removeFromBinGroup(bintable.selectedElements.get(0))
                                        })
                                        removeFromGroup.setEnabled(bintable.selectedElements.size() == 1 && bintable.selectedElements.get(0).group != "none")
                                    }


                                }.show(it.getComponent(), it.getX(), it.getY())
                            }
                        }
                    } as MouseListener)
                }

                /**
                 * panel to select next and previous bins
                 */
                panel(constraints: SOUTH) {
                    button(id: "previous", action: action(name: '<<', closure: {

                        //tell the controller to define the application server
                        controller.paginate(true)

                    }))
                    button(id: "next", action: action(name: '>>', closure: {

                        //tell the controller to define the application server
                        controller.paginate(false)

                    }))
                }

            }

            /**
             * main split panel for massspectra
             */
            splitPane(orientation: JSplitPane.VERTICAL_SPLIT, dividerLocation: tk.getScreenSize().height / 2) {

                /**
                 * panel which displays the bin massspec
                 * and the difference spectra
                 */
                splitPane(orientation: JSplitPane.HORIZONTAL_SPLIT, dividerLocation: tk.getScreenSize().width / 3) {

                    panel(id: "displaybin") {
                        gridLayout(cols: 1, rows: 1)
                        widget(createChart(model))
                    }

                    /**
                     * display the actual library massspec
                     */
                    panel(id: "displaylib") {
                        gridLayout(cols: 1, rows: 1)
                        widget(createLibChart(model))
                    }
                }



                displaybin.addHierarchyListener({ evt ->
                    if (displaybin.isShowing()) {
                        controller.displayCTSBinProperties(inchikey_value.text)
                    }
                } as HierarchyListener)

                displaybin.addComponentListener({ evt ->
                    controller.displayCTSBinProperties(inchikey_value.text)
                } as ComponentListener)

                application.addWindowFocusListener({ evt ->
                    controller.displayCTSBinProperties(inchikey_value.text)
                } as WindowFocusListener)

                /**
                 * panel which display the library massspec and table
                 */
                splitPane(orientation: JSplitPane.HORIZONTAL_SPLIT, dividerLocation: tk.getScreenSize().width / 3) {

                    tabbedPane() {

                        /**
                         * all our bin properties
                         */
                        panel(title: "Bin Properties", id: "displayBinProperties") {
                            gridLayout(cols: 1, rows: 1)
                            widget(createBinProperties(model))
                        }

                        /**
                         * all registered external id's for this bin
                         */
                        panel(title: "External Id's", id: "externalIds") {
                            gridLayout(cols: 1, rows: 1)
                            widget(createExternalIdsProperties(model))
                        }


                    }
                    tabbedPane() {

                        /**
                         * displays our similarity search windows to be used for identification of unknowns
                         */
                        panel(title: "Similarity Search", id: "displayLibraryTable") {

                            borderLayout(new BorderLayout(5, 5))

                            /**
                             * create a library table, which shows the actual spectra hits
                             */
                            scrollPane(constraints: CENTER, preferredSize: [160, -1]) {

                                table(id: "librarytable") {
                                    createLibraryTableModel(model)

                                    librarytable.selectionModel.addListSelectionListener({ evt ->

                                        if (evt.getValueIsAdjusting() == false) {
                                            //tell the controller to execute the disply action
                                            controller.displayLibrarySpectra(librarytable.selectedElement)
                                        }
                                    } as ListSelectionListener)

                                    librarytable.addMouseListener({
                                        mouseClicked: {
                                            /**
                                             * updates the name of the hit
                                             */
                                            if (it.clickCount == 2) {
                                                LibraryHit hit = librarytable.selectedElement
                                                Bin bin = hit.bin
                                                bin.name = hit.name

                                                controller.updateBinsName(bin)
                                            }
                                        }
                                    } as MouseListener)
                                }
                            }

                            createLibrarySearchPanel(model)

                            panel(constraints: SOUTH, layout: new MigLayout('fillx,insets 10')) {
                                label(id: "displayWarningMessage")
                            }
                        }

                        /**
                         * displays a table, which contains all the apex ions of this bin and so the list of allowed unique ions for bins to be assigned to it
                         */
                        panel(title: "Apex Masses/List of allowed unique Ions") {
                            gridLayout(cols: 1, rows: 1)
                            widget createApexMassesList(model)

                        }

                        panel(title: "Chemify Identification") {
                            gridLayout(cols: 1, rows: 1)
                            widget createChemifyIdentification(model)

                        }

                        panel(title: "MiniX Information") {
                            gridLayout(cols: 1, rows: 1)
                            widget createMiniXInformationPanel(model)

                        }


                    }
                }
            }
        }
    }
}

def createMiniXInformationPanel(model) {
    borderLayout(new BorderLayout(5, 5))

    label(text: "Associated MiniX data for the sample, which created this bin", constraints: NORTH)

    scrollPane(constraints: CENTER, preferredSize: [160, -1]) {

        table(id: "minixTable") {
            tableModel(list: model.minixHits) {
                propertyColumn(
                        header: "Key Name",
                        propertyName: "key",
                        editable: false
                )

                propertyColumn(
                        header: "Key Value",
                        propertyName: "value",
                        editable: false
                )

            }
        }

    }
}
/**
 * generates chemify based meta informations for us
 * @return
 */
def createChemifyIdentification(model) {

    borderLayout(new BorderLayout(5, 5))

    //to enable and disable the chemify functionality
    checkBox(constraints: NORTH, text: "enable chemify search, be aware that this can take some time", id: "enableChemify", actionPerformed: {
        if (enableChemify.selected) {
            Bin bin = bintable.selectedElement
            controller.displayBinProperties(bin, null)
        } else {
            model.chemifyHits.clear()
            chemifyTable.model.fireTableDataChanged()
        }
    })

    /**
     * basic informations
     */
    panel(constraints: SOUTH, layout: new MigLayout('fillx,insets 10')) {

        label(text: "Molecular Formula: ")
        label(id: "chemifyMolecularFormula", constraints: 'growx', text: "")

        panel(id: "chemifyDrawingArea", constraints: 'span 2 5, width 160, height 160px,al right, wrap')

        label(text: "Molare Weight: ")
        label(id: "chemifyMolareWeight", constraints: 'growx, wrap', text: "")

        label(text: "Exact Mass: ")
        label(id: "chemifyExactMass", constraints: 'growx, wrap', text: "")

        chemifyDrawingArea.addHierarchyListener({ evt ->
            if (chemifyDrawingArea.isShowing()) {
                controller.displayCTSProperties(chemifyTable.selectedElement)
            }
        } as HierarchyListener)

    }

    /**
     * our result table
     */
    scrollPane(constraints: CENTER, preferredSize: [160, -1]) {

        table(id: "chemifyTable") {
            tableModel(list: model.chemifyHits) {
                propertyColumn(
                        header: "InChI Key",
                        propertyName: "key",
                        editable: false
                )

                propertyColumn(
                        header: "Search term",
                        propertyName: "query",
                        editable: false
                )

                propertyColumn(
                        header: "Score",
                        propertyName: "score",
                        editable: false
                )
                propertyColumn(
                        header: "Algorithm",
                        propertyName: "algorithm",
                        editable: false
                )
            }
        }

        //run against cts to show metadata
        chemifyTable.selectionModel.addListSelectionListener({ evt ->
            if (evt.getValueIsAdjusting() == false) {

                controller.displayCTSProperties(chemifyTable.selectedElement)
            }
        } as ListSelectionListener)

        /**
         * events to update our bin's information
         */
        chemifyTable.addMouseListener({
            mouseClicked: {
                /**
                 * updates the name of the hit
                 */
                if (it.clickCount == 2) {
                    Bin bin = bintable.selectedElement
                    log.info("table selection: ${chemifyTable.selectedElement}")
                    bin.inchi = chemifyTable.selectedElement.key

                    if (bin.inchi.size() == 27) {
                        controller.updateBin(bin.inchi, "inchi_key", bin.id)
                    } else {
                        throw new RuntimeException("an InChI key needs to be exactly 27 characters long!")
                    }
                }
            }
        } as MouseListener)
    }
}

/**
 * generates all our apexing masses and displays them for us
 * @return
 */
def createApexMassesList(model) {

    borderLayout(new BorderLayout(5, 5))

    /**
     * create a library table, which shows the actual spectra hits
     */
    scrollPane(constraints: CENTER, preferredSize: [160, -1]) {

        table(id: "apexTable") {
            tableModel(list: model.apexMasses) {
                propertyColumn(
                        header: "Ion",
                        propertyName: "mass",
                        editable: false
                )

                propertyColumn(
                        header: "Absolute Intensity",
                        propertyName: "absoluteIntensity",
                        editable: false
                )
                propertyColumn(
                        header: "Relative Intensity",
                        propertyName: "relativeIntensity",
                        editable: false
                )
            }
        }
    }
}

/**
 * creates our library search panel for the system to use
 * @param model
 * @return
 */
JPanel createLibrarySearchPanel(model) {
    panel(constraints: NORTH, id: "displayLibrarySearchParameters", layout: new MigLayout('insets 10')) {

        label(text: "Retention Index Window:", constraints: 'growx, gapright 10')
        textField(id: "riWIndow", text: "${model.riWindow}", constraints: 'growx, gapright 20')

        label(text: "Similarity Range:", constraints: 'growx, gapright 10')

        textField(id: "similarityWindowBegin", text: "${model.minSimilarity}", constraints: 'wmin 80, wmax 80')
        textField(id: "similarityWindowEnd", text: "${model.maxSimilarity}", constraints: 'wmin 80, wmax 80, gapright 20')

        label(text: "Selected Library:", constraints: 'growx, gapright 10')
        comboBox(id: "similaritySelectedLibrary", model: bind {
            model.availableLibraries
        }, constraints: 'growx, al right, wrap')

        /**
         * fetch the current library and rerun the query
         */
        similaritySelectedLibrary.addActionListener({ evt ->
            model.selectedLibrary = similaritySelectedLibrary.selectedItem;
            controller.displayBin(bintable.selectedElement)

        } as ActionListener)

        riWIndow.addActionListener({ evt ->

            try {
                def value = riWIndow.text
                def number = Integer.parseInt(value)
                log.info("updating the ri window")
                model.riWindow = number
                displayWarningMessage.text = "updated search to use ri window of: ${model.riWindow}"

                bintable.model.fireTableDataChanged()
                log.info("updated retention index window: ${model.riWindow}")
            }
            catch (Exception e) {
                log.error(e.getMessage(), e)
                displayWarningMessage.text = "it has to be an integer value between 0 and n"
            }

        } as ActionListener)

        similarityWindowBegin.addActionListener({ evt ->
            try {
                def value = similarityWindowBegin.text.trim()
                def number = Integer.parseInt(value)
                if (number > 0 && number <= 1000) {
                    log.info("updating the similarity search window")

                    model.minSimilarity = number
                    displayWarningMessage.text = "updated  search to use minimum similarity: ${model.minSimilarity}"

                    bintable.model.fireTableDataChanged()
                    log.info("updated similarity search window: ${model.minSimilarity}")

                } else {
                    throw new RuntimeException("sorry value is out of range")
                }

            }
            catch (Exception e) {
                log.error(e.getMessage(), e)
                displayWarningMessage.text = "it has to be an integer value between 0 and 1000"
            }
        } as ActionListener)


        similarityWindowEnd.addActionListener({ evt ->
            try {
                def value = similarityWindowEnd.text.trim()
                def number = Integer.parseInt(value)
                if (number > 0 && number <= 1000) {
                    log.info("updating the similarity search window")

                    model.maxSimilarity = number
                    displayWarningMessage.text = "updated  search to use maximum similarity: ${model.maxSimilarity}"

                    bintable.model.fireTableDataChanged()
                    log.info("updated similarity search window: ${model.maxSimilarity}")

                } else {
                    throw new RuntimeException("sorry value is out of range")
                }

            }
            catch (Exception e) {
                log.error(e.getMessage(), e)
                displayWarningMessage.text = "it has to be an integer value between 0 and 1000"
            }
        } as ActionListener)


    }
}
/**
 * our bin properties view
 * @param model
 * @return
 */
JPanel createBinProperties(def model) {
    panel(
            id: "binproperties",
            layout: new MigLayout('fillx,insets 10')
    ) {


        label(text: "name", constraints: '')
        textField(id: "name_value", constraints: 'growx, wrap', columns: 100).addActionListener({ evt ->
            controller.updateBin(name_value.text, "name", bin_id_value.text as Integer)
        } as ActionListener)

        label(text: "export", constraints: '')
        checkBox(id: "export_value", constraints: 'growx, wrap').addActionListener({ evt ->
            controller.updateBin(export_value.selected, "export", bin_id_value.text as Integer)
        } as ActionListener)

        label(text: "quantmass", constraints: '')
        textField(id: "quant_value", constraints: 'growx, wrap', columns: 100).addActionListener({ evt ->
            Integer mass = Integer.parseInt(quant_value.text)
            controller.updateBin(mass, "quantmass", bin_id_value.text as Integer)
        } as ActionListener)

        label(text: "InChI Key", constraints: '')
        textField(id: "inchikey_value", constraints: 'growx, wrap', columns: 100).addActionListener({ evt ->
            String value = inchikey_value.text

            if (value.size() == 27) {
                controller.updateBin(value, "inchi_key", bin_id_value.text as Integer)
            } else {
                throw new RuntimeException("an InChI key needs to be exactly 27 characters long!")
            }
        } as ActionListener)

        //read only fields
        separator(constraints: 'growx, wrap,span 4')

        label(text: "Sample Name", constraints: '')
        label(id: "sample_name_value", constraints: 'growx, wrap')



        label(text: "Retention Index", constraints: '')
        label(id: "retention_index_value", constraints: ' wrap')
        label(text: "Unique Ion", constraints: '')
        label(id: "unique_ion_value", constraints: ' wrap')

        label(text: "Bin Id", constraints: '')
        label(id: "bin_id_value", constraints: ' wrap')

        //cts metadata

        label(text: "Molecular Formula: ")
        label(id: "propMolecularFormula", constraints: ' wrap', text: "")

        label(text: "Molare Weight: ")
        label(id: "propMolareWeight", constraints: ' wrap', text: "")

        label(text: "Exact Mass: ")
        label(id: "propExactMass", constraints: ' wrap', text: "")

    }
}

/**
 * a simple panel containing all our external ids in the system
 * @param model
 * @return
 */
JPanel createExternalIdsProperties(def model) {
    panel(
            id: "idproperties",
            layout: new MigLayout('insets 10')
    ) {


        label(text: "Pubchem Compound Id", constraints: '')
        textField(id: "cid_value", constraints: 'growx, wrap', columns: 100).addActionListener({ evt ->
            String value = cid_value.text

            if (value.isEmpty()) {
                controller.updateBinExternalId("", Bin.PUBCHEM, bin_id_value.text as Integer)
            } else if (value.size() > 0 && value.matches("[0-9]+")) {
                controller.updateBinExternalId(value, Bin.PUBCHEM, bin_id_value.text as Integer)
            } else {
                throw new RuntimeException("a PubChem compound id must be defined as positive number between 1 and n!")
            }
        } as ActionListener)


        label(text: "Kegg Id", constraints: '')
        textField(id: "kegg_value", constraints: 'growx, wrap', columns: 100).addActionListener({ evt ->
            String value = kegg_value.text

            if (value.size() > 0 && value.matches("C[0-9]+")) {
                controller.updateBinExternalId(value, Bin.KEGG, bin_id_value.text as Integer)
            } else {
                throw new RuntimeException("a kegg id needs to start with C and followed by the characters 0-9!")
            }
        } as ActionListener)

    }
}

/**
 * creates the binbase mass spec
 * @param model
 * @return
 */
ChartPanel createChart(def model) {
    ChartPanel panel = new ChartPanel(SpectraChart.createChart(model.graph))

    JFreeChart chart = panel.getChart()

    ((XYBarRenderer) ((XYPlot) chart.getPlot()).getRenderer()).setShadowVisible(false)

    chart.removeLegend()
    chart.setTitle("bin spectra")


    return panel
}

/**
 * creates the library mass spec
 * @param model
 * @return
 */
ChartPanel createLibChart(def model) {
    ChartPanel panel = new ChartPanel(SpectraChart.createChart(model.spectraGraph))

    JFreeChart chart = panel.getChart()

    ((XYBarRenderer) ((XYPlot) chart.getPlot()).getRenderer()).setShadowVisible(false)

    chart.removeLegend()
    chart.setTitle("library spectra")


    return panel
}

//table model
TableModel createTableModel(model) {
    tableModel(list: model.bins) {
        propertyColumn(
                header: "Id",
                propertyName: "id",
                editable: false
        )
        propertyColumn(
                header: "Name",
                propertyName: "name",
                editable: false
        )
        propertyColumn(
                header: "Retention Index",
                propertyName: "retentionIndex",
                editable: false
        )
        propertyColumn(
                header: "Unique Mass",
                propertyName: "uniqueMass",
                editable: false
        )
        propertyColumn(
                header: "Bin Group",
                propertyName: "group",
                editable: false
        )

    }
}

//table model
TableModel createLibraryTableModel(model) {
    tableModel(list: model.binHits) {

        propertyColumn(
                header: "Name",
                propertyName: "name",
                editable: false
        )
        propertyColumn(
                header: "Retention Index Diff",
                propertyName: "retentionIndexDifference",
                editable: false
        )

        propertyColumn(
                header: "Similiarity",
                propertyName: "similarity",
                editable: false
        )
        propertyColumn(
                header: "Library",
                propertyName: "libraryName",
                editable: false
        )

    }
}
