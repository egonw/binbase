package binview

optionPane(
        id: 'pane',
        messageType: JOptionPane.INFORMATION_MESSAGE,
        optionType: JOptionPane.DEFAULT_OPTION,
        message: bind {model.message})