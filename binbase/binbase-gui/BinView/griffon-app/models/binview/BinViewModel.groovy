package binview
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion
import edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.dataset.MassSpecDataSet
import groovy.sql.Sql
import minix.MiniXResult
import parser.ChemifyResult

import javax.swing.*

/**
 * contains the model for the view to display all the bins
 */
class BinViewModel {

    /**
     * all our bins, should be populated from the database
     */
    @Bindable List<Bin> bins = new Vector<Bin>()



    @Bindable List<Ion> apexMasses = new Vector<Ion>()

    @Bindable List<ChemifyResult> chemifyHits = new Vector<ChemifyResult>()

    @Bindable List<MiniXResult> minixHits = new Vector<MiniXResult>()

    /**
     * hits for a given bin
     */
    @Bindable List<LibraryHit> binHits = new Vector<LibraryHit>()

    /**
     * the acutal graph of the bin
     */
    @Bindable MassSpecDataSet graph = new MassSpecDataSet()

    /**
     * the difference spectra
     */
    @Bindable MassSpecDataSet differenceGraph = new MassSpecDataSet()

    /**
     * the acutal graph of the selected library spectra
     */
    @Bindable MassSpecDataSet spectraGraph = new MassSpecDataSet()

    //the avaialbeable databases
    @Bindable def databases = []

    //the used database
    @Bindable def database = ""

    /**
     * where is our current pagination
     */
    @Bindable int currentPagination = -100

    /**
     * what is the stepsize
     */
    @Bindable int stepPagination = 100

    /**
     * sql connection
     */
    @Bindable Sql sqlConnection

    /**
     * last executed query
     */
    @Bindable String lastBinQuery

    /**
     * parameters for the last query
     */
    @Bindable List lastBinQueryParameters = []

    /**
     * list of libraries for similarity searches
     */
    @Bindable ComboBoxModel availableLibraries = new DefaultComboBoxModel()

    @Bindable String selectedLibrary = ""
    /**
     * min similarity for a library search
     */
    @Bindable int minSimilarity = 500

    /**
     * max similarity for a library search
     */
    @Bindable int maxSimilarity = 1000

    /**
     * retention index window for a library search
     */
    @Bindable int riWindow = 2000

    /**
     * sorting direction
     */

    @Bindable String sortingDirection = "ASC"

    @Bindable String sortingColumn = "retention_index"

    /**
     * maps column ids to table fields
     */
    def headerRowMap = ["Id":"bin_id","Name":"name","Retention Index":"retention_index","Unique Mass":"uniquemass"]
 }