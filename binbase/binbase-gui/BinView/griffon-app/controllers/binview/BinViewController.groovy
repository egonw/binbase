package binview

import edu.ucdavis.genomics.metabolomics.binbase.server.ejb.compounds.Configurator
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory
import groovy.sql.Sql
import parser.CTSParser
import parser.CTSResult
import parser.ChemifyParser
import parser.ChemifyResult
import parser.MiniXParser
import render.StructureRenderer

import javax.swing.*
import java.sql.Connection

/**
 * controller does all the work
 */
class BinViewController {
    // injected model
    BinViewModel model

    //injected view
    BinViewView view

    /**
     *
     * initializes a default database connection
     * @param args
     */
    void mvcGroupInit(Map args) {
        if (model.sqlConnection == null) {
            //automatically select our database if we have only 1 registered
            def databases = Configurator.importService.getDatabases()

            if (databases.size() == 1) {
                log.info("size of databases is just 1, so preselect")
                model.database = databases[0]
                initializeDatabaseConnection();
            }
        }
    }

    /**
     * displays a bin in the massspec viewer
     */
    def displayBin(Bin bin, def event = null) {

        model.graph.clear()

        //fire the selection
        try {
            model.graph.addMassSpec bin
        }
        catch (NullPointerException e) {
        }

        model.binHits.clear()

        //to avoid event's from firing twice
        displayBinMatches(bin, event)
        displayBinProperties(bin, event)
        displayBinMetaData(bin, event)
    }

    def displayBinMetaData(Bin bin, def event) {
        if (bin) {
            MiniXParser p = new MiniXParser()

            model.minixHits.clear()
            p.queryMiniX(bin.sampleName).each {
                model.minixHits.add(it)
            }
            view.minixTable.model.fireTableDataChanged()

        }
    }

    /**
     * displays cts properties
     */
    def displayCTSProperties(ChemifyResult result) {


        view.chemifyMolecularFormula.text = ""
        view.chemifyMolareWeight.text = ""
        view.chemifyExactMass.text = ""

        if (result != null) {

            String key = result.key

            CTSParser parser = new CTSParser()
            CTSResult res = parser.getProperties(key)

            if (res != null) {
                view.chemifyMolecularFormula.text = res.formula
                view.chemifyMolareWeight.text = res.molWeight
                view.chemifyExactMass.text = res.exactMass

                new StructureRenderer().renderInChICode(res.code, view.chemifyDrawingArea, 160, 160)
            }
        }
    }

    /**
     * displays cts properties
     */
    def displayCTSBinProperties(String key) {
        execInsideUIAsync {


            view.propMolecularFormula.text = ""
            view.propMolareWeight.text = ""
            view.propExactMass.text = ""

            if (key != null && key.length() == 27) {

                CTSParser parser = new CTSParser()
                CTSResult res = parser.getProperties(key)

                if (res != null) {
                    view.propMolecularFormula.text = res.formula
                    view.propMolareWeight.text = res.molWeight
                    view.propExactMass.text = res.exactMass

                    int width = 160
                    int height = 160
                    int panelWidth = view.displaybin.getWidth().intValue()
                    int panelHeight = view.displaybin.getHeight().intValue()


                    int offsetX = panelWidth - (panelWidth / 4).intValue() - width
                    int offsetY = 50


                    new StructureRenderer().renderInChICode(res.code, view.displaybin, width, height, offsetX, offsetY, false)
                }
            }
        }
    }

    /**
     * a simple query to generate all the bin properties
     * @param bin
     */
    private void displayBinProperties(Bin bin, def event = null) {

        if (bin != null) {
            model.sqlConnection.eachRow("select a.*, b.sample_name from bin a, samples b where bin_id = ? and a.sample_id = b.sample_id", [bin.id]) { row ->

                try {
                    bin.name = row.name
                    bin.quantMass = row.quantmass
                    bin.uniqueMass = row.uniquemass
                    bin.export = Boolean.parseBoolean(row.export)
                    bin.sampleName = row.sample_name
                    bin.retentionIndex = row.retention_index
                    bin.inchi = row.inchi_key


                    model.sqlConnection.eachRow("select name,value from reference a, reference_class b where a.class_id = b.id and bin_id = ?", [bin.id]) { ref ->
                        println("${ref.name} - ${ref.value}")
                        if (ref.name.equals(Bin.PUBCHEM)) {
                            bin.cid = ref.value
                            println("assigning as cid....")
                        } else if (ref.name.equals(Bin.KEGG)) {
                            bin.kegg = ref.value
                            println("assigning as kegg....")
                        }
                    }
                }
                catch (Exception e) {
                    log.error(e.getMessage(), e)
                }

                //setting the actual values
                view.name_value.text = bin.name
                view.quant_value.text = bin.quantMass
                view.export_value.selected = bin.export
                view.inchikey_value.text = bin.inchi
                view.sample_name_value.text = bin.sampleName
                view.retention_index_value.text = bin.retentionIndex
                view.unique_ion_value.text = bin.uniqueMass
                view.bin_id_value.text = bin.id
                view.cid_value.text = bin.cid
                view.kegg_value.text = bin.kegg


                displayCTSBinProperties(bin.inchi)
            }

            //render apex masses
            model.apexMasses.clear()
            bin.getApex().each { ion ->
                model.apexMasses.add(ion)
            }
            view.apexTable.model.fireTableDataChanged()

            //chemify hits - only be used in case it's requested

            if (view.enableChemify.selected) {
                execInsideUIAsync {
                    def result = new ChemifyParser().getIdentifications(bin.name)

                    model.chemifyHits.clear()
                    result.each {
                        model.chemifyHits.add(it)
                    }
                    view.chemifyTable.model.fireTableDataChanged()

                    if (model.chemifyHits.size() != 0) {
                        //make first element active
                        JTable table = view.chemifyTable
                        table.getSelectionModel().setSelectionInterval(0, 0)
                    }
                }
            } else {
                displayCTSProperties(null)
            }
        }
    }

    /**
     * calculates the matches based on the settings of the model
     * @param bin
     */
    private void displayBinMatches(Bin bin, def event) {
        view.librarytable.model.fireTableDataChanged()

        if (bin != null) {
            model.sqlConnection.eachRow("select a.spectra_id, b.name, a.similarity, a.retention_index_diff, b.spectra, c.name as lib_name from library_match a, library_spec b, library c where b.id = c.id and a.spectra_id = b.\"specId\" and bin_id = ? and similarity between ? and ? and retention_index_diff < ? and c.name = ? order by similarity DESC LIMIT 10", [bin.id, model.minSimilarity, model.maxSimilarity, model.riWindow, model.selectedLibrary]) { row ->

                LibraryHit hit = new LibraryHit(bin)

                hit.name = row.name
                hit.similarity = row.similarity
                hit.retentionIndexDifference = row.retention_index_diff
                hit.massSpec = row.spectra
                hit.id = row.spectra_id
                hit.libraryName = row.lib_name

                model.binHits.add(hit)
            }

            view.librarytable.model.fireTableDataChanged()

            if (model.binHits.size() != 0) {
                //make first element active
                JTable table = view.librarytable
                table.getSelectionModel().setSelectionInterval(0, 0)
            }

        }
    }

    /**
     * displays a selected spectra
     */
    def displayLibrarySpectra = { LibraryHit hit = null ->

        model.spectraGraph.clear()
        model.differenceGraph.clear()

        try {
            model.spectraGraph.addMassSpec hit
        }
        catch (NullPointerException e) {
            //ignored
        }

        try {
            model.differenceGraph.setAbsolute(true)
            model.differenceGraph.addMassSpec new DifferenceSpectra(hit.getBin(), hit)
        }
        catch (NullPointerException e) {
            //ignored
        }


    }

    /**
     * quits the application
     */
    def quit = {
        System.exit(0)
    }

    /**
     * load the dbs from the configurator
     * @return
     */
    def loadDB() {

        log.info("loading databases...")
        Configurator.getImportService().getDatabases().each { String s ->
            if (model.databases.contains(s) == false) {
                model.databases.add(s)
            }
        }

        log.info("loading loaded: ${model.databases.size()}")

    }

    /**
     * actually assign which db we use
     */
    def selectDB = { String db ->
        model.database = db
    }

    /**
     * here we choose our db and log into the applicaiton server
     */
    def changeDB = { evt = null ->

        if (model.databases.isEmpty()) {
            loadDB()
        }

        view.selectDatabaseValue.removeAllItems()
        //populate the database entries
        model.databases.each { String s ->
            view.selectDatabaseValue.addItem(s)
        }

        view.selectDatabase.show()

        resetPagination()
        initializeDatabaseConnection()

        displayBinCount()
        displayCustomBinQuery("select name, bin_id,spectra,apex, retention_index,uniquemass,group_id from bin a")
    }

    /**
     * tells the view the count of all bins in the selected database
     * @return
     */
    def displayBinCount() {
        view.binCount.setText(model.sqlConnection.firstRow("select count(*) as c from bin").c.toString())
    }

    /**
     * executes a custom binbase query
     * @param query
     * @return
     */
    def displayCustomBinQuery(String query) {
        resetPagination()
        executeBinQuery(query)

    }

    /**
     * custom query with provided params
     * @param query
     * @param params
     * @return
     */
    def displayCustomBinQuery(String query, List params) {
        resetPagination()
        executeBinQuery(query, params)
    }

    def displayAllBinsById(String id) {
        resetPagination()
        executeBinQuery("select name, bin_id,spectra,apex, retention_index,uniquemass,group_id from bin a  where bin_id = ?", [id.toInteger()])
    }

    def displayAllBinsByName(String name) {
        resetPagination()
        executeBinQuery("select name, bin_id,spectra,apex, retention_index,uniquemass,group_id from bin where name LIKE ?", [name])
    }

    /**
     * updates the bins name
     */
    def updateBinsName = { Bin bin ->

        updateBin(bin.name, "name", bin.id)

    }

    /**
     * updates external id's for bins
     * @param value
     * @param name
     * @param id
     * @return
     */
    def updateBinExternalId(String value, String name, int binId) {
        log.debug("updating external refrence with name ${name} to value ${value} for binId ${binId}")
        Sql sql = model.sqlConnection

        if (sql.firstRow("select count(*) as c from reference_class where name = ?", [name]).c == 0) {
            sql.executeInsert("insert into reference_class values(nextval('hibernate_sequence'),?,?)", [name, name]);
        }

        int classId = sql.firstRow("select id from reference_class where name = ?", [name]).id
        sql.execute("delete from reference a where bin_id = ? and class_id = ?", [binId, classId])
        sql.execute("insert into reference(id,bin_id,class_id,value) values(nextval('hibernate_sequence'),?,?,?)", binId, classId, value)


    }

    /**
     * updates teh bins name
     */
    def updateBin = { def value, String fieldName, int binId ->

        log.debug("updating bin: ${value} - ${fieldName} - ${binId}")
        //execute the actual update
        Sql sql = model.sqlConnection
        sql.execute("update bin set ${fieldName} = ? where bin_id = ?", [value, binId])

        //make sure the current bin stays selected
        JTable table = view.bintable
        int row = table.getSelectionModel().getAnchorSelectionIndex()

        view.bintable.model.fireTableDataChanged()

        table.getSelectionModel().setSelectionInterval(row, row)

    }

    /**
     * intializes the connection to the database and populates the library field
     */
    private void initializeDatabaseConnection() {
        log.info("configure database connection...")
        Properties p = new Properties()
        p.setProperty(SimpleConnectionFactory.KEY_USERNAME_PROPERTIE, model.database)
        p.setProperty(SimpleConnectionFactory.KEY_DATABASE_PROPERTIE, Configurator.getDatabaseService().getDatabase())
        p.setProperty(SimpleConnectionFactory.KEY_HOST_PROPERTIE, Configurator.getDatabaseService().getDatabaseServer())
        p.setProperty(SimpleConnectionFactory.KEY_PASSWORD_PROPERTIE, Configurator.getDatabaseService().getDatabaseServerPassword())
        p.setProperty(SimpleConnectionFactory.KEY_TYPE_PROPERTIE, Configurator.getDatabaseService().getDatabaseServerType())

        log.debug("using properties: ${p}")

        ConnectionFactory factory = ConnectionFactory.createFactory();
        factory.setProperties(p)

        view.currentDatabase.setText(model.database.toString())

        Connection connection = factory.getConnection()
        //assign sql conenction
        model.sqlConnection = new Sql(connection)
        log.info("initialized database connection")

        //populate our libaries

        model.availableLibraries.removeAllElements()
        model.sqlConnection.eachRow("select name from library") {

            model.availableLibraries.addElement(it.name)
        }

    }

    def resetPagination() {
        model.currentPagination = model.stepPagination * -1
    }
    /**
     * executes the acutal query on the model and is needed also for pagination
     */
    def executeBinQuery(String query, List parameters = [], boolean reverse = false) {
        log.info("current: ${model.currentPagination} with step ${model.stepPagination}")

        //in case we want to go to the previous page
        if (reverse) {
            model.currentPagination = model.currentPagination - model.stepPagination
        } else {
            model.currentPagination = model.currentPagination + model.stepPagination
        }


        if (model.currentPagination <= 0) {
            model.currentPagination = 0
            view.previous.enabled = false
        } else {
            view.previous.enabled = true
        }

        log.info("=> after current: ${model.currentPagination} with step ${model.stepPagination}")

        //remove the old data
        model.bins.clear()
        model.binHits.clear()

        view.bintable.model.fireTableDataChanged()

        List tempParameters = []

        //add our parameters to the list of used parameters
        parameters.each {
            tempParameters.add(it)
        }

        //add the pagination parameters
        tempParameters.add(model.stepPagination)

        if (model.currentPagination < 0) {
            log.warn("the currentPagination should never be < 0! This seems to be a bug and we set it to 0 for this reason")
            model.currentPagination = 0;
        }
        tempParameters.add(model.currentPagination)

        log.info("execute query: ${query} with parameters: ${tempParameters}")

        //fetch the result and build the bin list
        model.sqlConnection.eachRow(query + " order by ${model.sortingColumn} ${model.sortingDirection} limit ? offset ?", tempParameters) { row ->

            Bin bin = new Bin()
            bin.id = row.bin_id
            bin.massSpec = row.spectra
            bin.apexSpec = row.apex
            bin.retentionIndex = row.retention_index
            bin.uniqueMass = row.uniquemass
            bin.name = row.name

            if (row.group_id != null) {
                log.info "looking for related group with id ${row.group_id}"

                def res = model.sqlConnection.firstRow("select name from bin_group where group_id = ${row.group_id}")

                if (res) {
                    bin.group = res.name
                } else {
                    log.error("trying to fetch a group which no longer exists! remove association from bin to avoid this in the future")
                    model.sqlConnection.executeUpdate("update bin set group_id = null where group_id = ${row.group_id}")
                }
            } else {
                bin.group = "none"
            }

            model.bins.add bin

        }

        //update the bin table
        view.bintable.model.fireTableDataChanged()

        //assign the parameters needed for the track of the query
        model.lastBinQuery = query
        model.lastBinQueryParameters = parameters

        //disable the next button if we have less than step size compounds
        view.next.enabled = !(model.bins.size() < model.stepPagination)
    }

    /**
     * reruns the last query
     */
    def executeLastQuery(boolean reverse = false) {
        //reset the pagination
        model.currentPagination = model.currentPagination - model.stepPagination
        this.executeBinQuery(model.lastBinQuery, model.lastBinQueryParameters, reverse)
    }

    /**
     * paginates to the next or previous page
     * @param reverse
     * @return
     */
    def paginate(boolean reverse = false) {
        this.executeBinQuery(model.lastBinQuery, model.lastBinQueryParameters, reverse)
    }

    /**
     * groups all the bins in the given model
     * @param model
     */
    def executeBinGrouping(Collection<Bin> bins, String groupName) {

        //select existing group
        //select * from bin_group where name = "?"

        def rows = model.sqlConnection.rows("select * from bin_group where name = ?", groupName)

        if (rows.size > 0) {
            int groupId = rows.get(0).group_id

            bins.each { Bin bin ->
                model.sqlConnection.executeUpdate("update bin set group_id = ${groupId} where bin_id = ${bin.id}")
            }
        } else {

            model.sqlConnection.execute("insert into bin_group values ( nextval('hibernate_sequence'),'${groupName}','created by binview')")

            rows = model.sqlConnection.rows("select * from bin_group where name = ?", groupName)

            if (rows.size > 0) {
                int groupId = rows.get(0).group_id

                bins.each { Bin bin ->
                    model.sqlConnection.executeUpdate("update bin set group_id = ${groupId} where bin_id = ${bin.id}")
                }
            }
        }
        executeLastQuery(false)
    }

    /**
     * deletes a given group
     * @param groupName
     */
    def deleteBinGroup(String groupName) {
        def rows = model.sqlConnection.rows("select * from bin_group where name = ?", groupName)

        if (rows.size > 0) {
            rows.each {
                int groupId = it.group_id
                model.sqlConnection.executeUpdate("update bin set group_id = null where group_id = ${groupId}")
                model.sqlConnection.execute("delete from bin_group where name= ?", groupName)
            }
            executeLastQuery(false)
        }
    }

    /**
     * removes the bin from the grouo
     * @param bin
     */
    def removeFromBinGroup(Bin bin) {

        def rows = model.sqlConnection.rows("select * from bin_group where name = ?", bin.group)

        if (rows.size == 1) {
            model.sqlConnection.executeUpdate("update bin set group_id = null where bin_id = ${bin.id}")
            executeLastQuery(false)
        } else if (rows.size > 0) {
            deleteBinGroup(bin.group)
        }
    }
}
