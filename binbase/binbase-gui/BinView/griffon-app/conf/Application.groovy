application {
    title='BinView'
    startupGroups = ['BinView']

        // Should Griffon exit when no Griffon created frames are showing?
    autoShutdown = true

    // If you want some non-standard application class, apply it here
    //frameClass = 'javax.swing.JFrame'
}
mvcGroups {
    // MVC Group for "dialog"
    'dialog' {
        model      = 'binview.DialogModel'
        view       = 'binview.DialogView'
        controller = 'binview.DialogController'
    }

    // MVC Group for "BinView"
    'BinView' {
        model = 'binview.BinViewModel'
        controller = 'binview.BinViewController'
        view = 'binview.BinViewView'
    }

}
