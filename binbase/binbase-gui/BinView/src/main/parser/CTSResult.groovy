package parser

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/13/14
 * Time: 1:50 PM
 *
 * simple cts result
 */
class CTSResult {

    String key

    String code

    Double molWeight

    Double exactMass

    String formula
}
