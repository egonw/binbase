package parser
import groovy.json.JsonSlurper
import groovyx.net.http.URIBuilder
import minix.MiniXResult
/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 2/28/14
 * Time: 11:58 AM
 */
class MiniXParser {

    String url = ""

    public MiniXParser(){

        url = "http://minix.fiehnlab.ucdavis.edu/services/communications?wsdl" //MiniXConfigurationJMXFacadeUtil.getHome().create().getUrl()
    }

    List<MiniXResult> queryMiniX(String sample){

        URIBuilder builder = new URIBuilder(url)
        builder.path = "/rest/getMetadataForSample/${sample}"

        def data = builder.toURL()
        def result = []
        try{
        new JsonSlurper().parse(new InputStreamReader(data.openStream())).each {
            def c = new MiniXResult()
            c.key = it.key
            c.value = it.value

            result.add(c)
        }
        }
        catch (FileNotFoundException e){

            def c = new MiniXResult()
            c.key = "error"
            c.value = "sample not found!"
            result.add(c)
        }

        return result
    }
}
