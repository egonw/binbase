package parser
import groovy.json.JsonSlurper
import groovyx.net.http.URIBuilder

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/10/14
 * Time: 2:20 PM
 */
class ChemifyParser {

    private def url = "http://cts.fiehnlab.ucdavis.edu"

    /**
     * returns a list of maps with all the identifications for the given name
     * @param name
     * @return
     */
    Collection getIdentifications(String name) {

        URIBuilder builder = new URIBuilder(url)
        builder.path = "/chemify/rest/identify/query"
        builder.addQueryParam("value",name)

        def data = builder.toURL()
        def result = []
        new JsonSlurper().parse(new InputStreamReader(data.openStream())).each {
            def c = new ChemifyResult()
            c.key = it.result
            c.algorithm = it.algorithm
            c.score = it.score
            c.query = it.query

            result.add(c)
        }

        return result
    }
}
