package parser

import groovy.json.JsonSlurper
import groovyx.net.http.URIBuilder

/**
 *
 * very simple cts connectivity to get more information about a compound
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/13/14
 * Time: 1:44 PM
 */
class CTSParser {

    private def url = "http://cts.fiehnlab.ucdavis.edu"

    /**
     * returns a list of maps with all the identifications for the given name
     * @param name
     * @return
     */
    CTSResult getProperties(String inchiKey) {

        URIBuilder builder = new URIBuilder(url)
        builder.path = "/service/compound/${inchiKey}"

        def data = builder.toURL()
        def result = []
        def json = new JsonSlurper().parse(new InputStreamReader(data.openStream()))
        def c = new CTSResult()

        //if we get a list, nothing was found...
        if (json instanceof ArrayList == false) {
            c.key = json.inchikey
            c.code = json.inchicode
            c.molWeight = json.molweight
            c.exactMass = json.exactmass
            c.formula = json.formula

            return c
        } else {
            return null
        }
    }
}