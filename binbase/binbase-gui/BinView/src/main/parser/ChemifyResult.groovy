package parser

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/10/14
 * Time: 3:28 PM
 */
class ChemifyResult {

    String key

    double score

    String algorithm

    String query
}
