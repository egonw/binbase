package binview

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment
import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException

/**
 * a basic bin massspec
 */
class Bin extends MassSpec {

    static final String PUBCHEM = "PubChem"

    static final String KEGG = "KEGG"

    String apexSpec

    String massSpec

    Integer id

    Integer retentionIndex

    Integer uniqueMass

    Integer quantMass

    String name

    String group

    Boolean export

    String inchi

    String cid

    String kegg

    String sampleName

    Collection getComments() {
        throw new NotSupportedException("sorry not supported")
    }

    void setComments(Collection collection) {
        throw new NotSupportedException("sorry not supported")

    }

    Comment createComment() {
        throw new NotSupportedException("sorry not supported")
    }

    @Override
    void setSimilarity(Double aDouble) {
        throw new NotSupportedException("sorry not supported")
    }

    @Override
    Double getSimilarity() {
        throw new NotSupportedException("sorry not supported")
    }
}
