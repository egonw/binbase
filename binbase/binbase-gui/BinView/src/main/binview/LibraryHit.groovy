package binview

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment
import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException

/**
 *
 */
class LibraryHit extends MassSpec {

    Integer id

    String name

    String retentionIndexDifference

    String massSpec

    String libraryName

    Double similarity

    Bin bin

    public LibraryHit(Bin bin){
        this.bin = bin
    }
    Collection getComments() {
        throw new NotSupportedException("sorry not supported")
    }

    void setComments(Collection collection) {
        throw new NotSupportedException("sorry not supported")
    }

    Comment createComment() {
        throw new NotSupportedException("sorry not supported")
    }


    @Override
    String getApexSpec() {
        return "0"
    }

    @Override
    void setApexSpec(String s) {
        throw new NotSupportedException("sorry not supported")
    }

    @Override
    void setRetentionIndex(Integer integer) {
        throw new NotSupportedException("sorry not supported")
    }

    @Override
    Integer getRetentionIndex() {
        throw new NotSupportedException("sorry not supported")
    }
}
