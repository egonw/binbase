update bin set sample_id = 1

GO

delete from spectra where sample_id not in (select sample_id from bin)

GO

delete from samples where sample_id not in (select sample_id from bin)

GO

delete from result_link

GO

delete from result

GO

delete from rawdata

GO

delete from correction_data

GO

delete from comments

GO

delete from configuration where configuration_id not in (select configuration_id from samples)

GO

delete from locking

GO

delete from meta_key

GO

delete from metainformation

GO

delete from qualitycontrol

GO

delete from quantification

GO

delete from runtime

GO

delete from sample_info

GO

delete from standard_hist

GO

delete from synonyme

GO 

vacuum