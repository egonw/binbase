package edu.ucdavis.genomics.metabolomics.binbase.binbase.dsl;

import java.io.File;

/**
 * this generates a DSL from all files in a directory
 * 
 * @author wohlgemuth
 * 
 */
public class GenerateDSLFromDirectory {

	public static void main(String args[]) {

		if (args.length == 5) {
			edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromDatabase generate = new edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromDatabase();
			String dsl = generate.dslGenerateFromDirectory(args[1], new File(
					args[2]), Integer.parseInt(args[3]), args[0], true, 80,
					"xls");
			generate.writeDSL(new File(args[4]), dsl);
		} else {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out.println("arg[2] : directory:");
			System.out.println("arg[3] : class size:");
			System.out.println("arg[4] : outPutFile:");
			System.out.println("");

			System.exit(-1);
		}
	}
}
