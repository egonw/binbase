package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.msp;

import java.io.IOException;

import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.batik.svggen.SVGGraphics2D;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.ModelFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.util.HibernatePreparation;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.binbase.util.locking.BinGenerationLock;
import edu.ucdavis.genomics.metabolomics.exception.LockingException;

/**
 * exports a bin as png
 * @author wohlgemuth
 *
 */
public class ExportBinAsSVG {

	/**
	 * @param args
	 * @throws NamingException 
	 * @throws ClusterException 
	 * @throws LockingException 
	 * @throws IOException 
	 * @throws ParserConfigurationException 
	 * @throws NumberFormatException 
	 */
	public static void main(String[] args) throws LockingException, ClusterException, NamingException, IOException, NumberFormatException, ParserConfigurationException {

		try {

			if (args.length >= 4) {
				HibernatePreparation.prepare(args[0], args[1]);
				BinGenerationLock.getInstance().obtainLock(args[1], "");
				
				
				for (int i = 4; i < args.length; i++) {

					int bin = Integer.parseInt(args[i]);

					IModel model = ModelFactory.newInstance().createModel(Bin.class);
					model.setQuery("SELECT a FROM Bin a where a.id = " + bin);
					Object[] result = model.executeQuery();

					if (result.length > 0) {
						Bin currentBin = (Bin)result[0];
						
						SVGGraphics2D image = currentBin.generateSVGOfSpectra(Integer.parseInt(args[2]), Integer.parseInt(args[3]));
						image.stream(bin + ".svg");
					} else {
						System.out.println("sorry id not found - " + bin);
					}
				}


			} else {
				System.out.println("About:");
				System.out.println("");
				System.out
						.println("This tool exports the specific bin id's as SVG file and stores them in the current directory");
				System.out.println("");
				System.out.println("Usage:");
				System.out.println("");
				System.out.println("arg[0] : application server");
				System.out.println("arg[1] : column");
				System.out.println("arg[2] : width of the picture");
				System.out.println("arg[3] : height of the picture");
				
				System.out
						.println("arg[4...] : the id of the bin's you like to convert, seperated by space");

				System.exit(-1);
			}
		} finally {
			BinGenerationLock.getInstance().releaseLock(args[1], "");
		}
	}

}
