package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.export;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.TXT;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.math.BinCalculateSpectraSimilarity;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.util.HibernatePreparation;
import edu.ucdavis.genomics.metabolomics.binbase.util.locking.BinGenerationLock;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

/**
 * calculates similarities to the given spectra. Format has to be the standard
 * pegasus string
 * 
 * @author wohlgemuth
 * 
 */
public class CalculateSimilaritiesToSpectra {

	public static void main(String[] args) throws FileNotFoundException,
			IOException, Exception {
		if (args.length < 5) {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out.println("arg[2] : min simiarity");

			System.out.println("arg[3] : inPutFile:");
			System.out.println("arg[4] : outPutFile:");

			System.out.println("");

			System.exit(-1);
		}

		try {

			Logger logger = Logger
					.getLogger(CalculateSimilaritiesToSpectra.class);
			HibernatePreparation.prepare(args[0], args[1]);
			BinGenerationLock.getInstance().obtainLock(args[1], "");

			Scanner scanner = new Scanner(new File(args[3]));

			StringBuffer spectra = new StringBuffer();

			// name valu pattern
			Pattern nameValue = Pattern
					.compile("([0-9]+(\\.[0-9]+)*)+.([0-9]+(\\.[0-9]+)*)+");
			int counter = 0;

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().trim();

				if (line.length() > 0) {
					// pegasus string
					if (line.matches("([0-9]:[0-9]\\s)+")) {

						doWork(new File(new File(args[4]), "result_" + counter
								+ new File(args[3]).getName()), spectra);
						counter = counter + 1;
					}
					// ion value pairs
					else if (line.matches(nameValue.pattern())) {
						Matcher matcher = nameValue.matcher(line);

						matcher.find();

						String ion = matcher.group(1);
						String intensity = matcher.group(3);

						spectra.append(Math.round(Double.parseDouble(ion)));
						spectra.append(":");
						spectra.append(intensity);

						if (scanner.hasNextLine()) {
							spectra.append(" ");
						}
					}
					// invalid
					else {
						throw new RuntimeException("invalid file format: "
								+ line);
					}
				}
			}

			// if anything else but null, we already generated files
			if (counter == 0) {
				doWork(new File(new File(args[4]), "result_"
						+ new File(args[3]).getName()), spectra);
			}
		} finally {

			BinGenerationLock.getInstance().releaseLock(args[1], "");
		}
	}

	private static void doWork(File out, StringBuffer spectra)
			throws IOException, FileNotFoundException {
		BinCalculateSpectraSimilarity matrix = new BinCalculateSpectraSimilarity();
		DataFile file = matrix.calculate(spectra.toString().trim());

		TXT txt = new TXT();
		txt.write(new FileOutputStream(out), file);
	}

}
