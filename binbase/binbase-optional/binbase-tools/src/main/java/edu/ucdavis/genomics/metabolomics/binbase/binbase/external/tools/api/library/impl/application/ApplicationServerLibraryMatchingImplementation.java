package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.application;

import java.io.File;
import java.io.FileOutputStream;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.XLS;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.ApplicationServerLibraryMatchable;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfiguration;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfigurationFactory;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.database.DatabaseLibraryMatchableImplementation;

/**
 * fetches the configuration from the application server and needs less
 * configuration for the this reason
 * 
 * @author wohlgemuth
 */
public class ApplicationServerLibraryMatchingImplementation extends
		DatabaseLibraryMatchableImplementation implements
		ApplicationServerLibraryMatchable {

	private String applicationServer;

	public String getApplicationServer() {
		return applicationServer;
	}

	public void setApplicationServer(final String value) {
		applicationServer = value;
	}

	@Override
	public void prepare() throws Exception {

		// here we set the system properties needed for connection

		final DatabaseConfigurationFactory factory = DatabaseConfigurationFactory
				.getFactory(ApplicationServerDatabaseConfigurationFactory.class
						.getName(),
						ApplicationServerDatabaseConfigurationFactory
								.buildConfiguration(getApplicationServer(),
										getColumnName()));

		final DatabaseConfiguration configuration = factory.getConfiguration();

		setDatabaseName(configuration.getDatabaseName());
		setDatabaseServer(configuration.getHostName());
		setDatabasePassword(configuration.getPassword());

		super.prepare();
	}

	public static void main(final String[] args) throws Exception {
		if (args.length < 3) {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out.println("arg[2] : outPutFile:");
			System.out.println("");

			System.exit(-1);
		}

		try {
			final ApplicationServerLibraryMatchingImplementation m = new ApplicationServerLibraryMatchingImplementation();
			m.setApplicationServer(args[0]);
			m.setColumnName(args[1]);
			m.setMinimalSimilarity(700);
			m.setRiRange(2000);
			m.calculateAndWrite(new XLS(), new FileOutputStream(new File(
					args[2])));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

}
