package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library;

import java.util.Properties;

/**
 * used to configure the database
 * 
 * @author wohlgemuth
 */
public interface DatabaseConfiguration {

	/**
	 * returns the name of database host
	 * 
	 * @return
	 */
	public String getHostName();

	/**
	 * returns the database name
	 * 
	 * @return
	 */
	public String getDatabaseName();

	/**
	 * returns the username
	 * 
	 * @return
	 */
	public String getUserName();

	/**
	 * returns the password
	 * 
	 * @return
	 */
	public String getPassword();

	/**
	 * returns the type of the database
	 * 
	 * @return
	 */
	public String getType();

	/**
	 * generates the standard connection properties which are used by the old
	 * connection factories
	 * 
	 * @return
	 */
	public Properties generateConnectionProperties();
}
