package edu.ucdavis.genomics.metabolomics.binbase.binbase.calculator;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SchedulerUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.dsl.ExporterDSL;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.GenerateConfigFile;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.io.source.ByteArraySource;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.StringWriter;
import java.util.Map;
import java.util.Scanner;

/**
 * sends a DSL to the cluster for calculations and the result will be attached to the responding experiment in MiniX
 * @author wohlgemuth
 *
 */
public class GroovyDSLScheduler {

	public GroovyDSLScheduler() {
	}
	
	@SuppressWarnings("rawtypes")
	public static void main(final String args[]) throws Exception{
		if (args.length < 1) {
			System.out.println("Usage:");
			System.out.println("");
			System.out
					.println("arg[0] : dsl file which defines the calculation you would like to run. Please make sure that the experiment names corresponds to a minix id! Or your results will be not attached");

			System.out.println("");

			System.exit(-1);
		}

		
		Logger logger = Logger.getLogger(GroovyDSLScheduler.class.toString());

		//lets test if we can read the dsl
        ExporterDSL dsl = new ExporterDSL();
        Map readDSL = dsl.readExporterRules(new File(args[0]));
        
        //seems all good, now lets start it for calculations

        String server = readDSL.get("server").toString();
        DSL dslObject = new DSL();

        //set id
        dslObject.setId(new File(args[0]).getName());


        StringWriter writer = new StringWriter();

        Scanner scanner = new Scanner(new File(args[0]));
        
        while(scanner.hasNextLine()){
            writer.write(scanner.nextLine());
            writer.write("\n");
        }

        writer.flush();
        writer.close();

        dslObject.setContent(writer.getBuffer().toString());

        logger.info("generating config file to connect to cluster: " + server);

        String config = GenerateConfigFile.generateFile(server).toString();

        XMLConfigurator.getInstance().addConfiguration(new ByteArraySource(config.getBytes()));

        logger.info("dsl to be scheduled is: \n\n" + dslObject.getContent() + "\n\n");

        logger.info("getting ready to schedule the calculation...");

        SchedulerUtil.getInstance().scheduleDSL(dslObject);
        
        logger.info("calculation should be running on the cluster now!");
	}

}
