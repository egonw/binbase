package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.export;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseServices;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.exception.SendingException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;

/**
 * used to recalculate an existing binbase experiment
 * @author wohlgemuth
 *
 */
public class ReExportBinBaseExperiment {


	public static void main(String args[]) throws BinBaseException, NamingException, RemoteException, CreateException, SendingException {

		if (args.length != 3) {
			System.out.println("Readme:");
			System.out.println("");
			System.out
					.println("this tool is used to  reexport a given study, which has to exist in BinBase.");
		
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : database");
			System.out.println("arg[2] : binbase experiment id");

			System.out.println("");

			System.exit(-1);
		}

		XMLConfigurator.getInstance().reset();
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.factory.initial",
				"org.jnp.interfaces.NamingContextFactory", true);
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.factory.url.pkgs",
				"org.jboss.naming:org.jnp.interfaces", true);
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.provider.url", args[0] + ":1099", true);

		if (args.length == 3) {
			String key = Configurator.getKeyManager().getInternalKey();
			
			Scheduler scheduler = BinBaseServices.getScheduler();
			Experiment experiment = BinBaseServices.getService().getExperiment(args[1],args[2], key);
			scheduler.scheduleExport(experiment, key);
		} 

	}
}
