package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.msp;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.ModelFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.util.HibernatePreparation;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.binbase.util.locking.BinGenerationLock;
import edu.ucdavis.genomics.metabolomics.exception.LockingException;

public class ExportAsJPEG  {

	/**
	 * @param args
	 * @throws NamingException 
	 * @throws ClusterException 
	 * @throws LockingException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws LockingException, ClusterException, NamingException, IOException {

		try {

			if (args.length >= 4) {
				HibernatePreparation.prepare(args[0], args[1]);
				BinGenerationLock.getInstance().obtainLock(args[1], "");
				
				
				for (int i = 4; i < args.length; i++) {

					int bin = Integer.parseInt(args[i]);

					IModel model = ModelFactory.newInstance().createModel(Bin.class);
					model.setQuery("SELECT a FROM Bin a where a.id = " + bin);
					Object[] result = model.executeQuery();

					if (result.length > 0) {
						Bin currentBin = (Bin)result[0];
						
						BufferedImage image = currentBin.generateImageOfSpectra(Integer.parseInt(args[2]), Integer.parseInt(args[3]));
						
						 ImageIO.write((RenderedImage)image, "JPEG", new File(bin + ".jpeg"));
					} else {
						System.out.println("sorry id not found - " + bin);
					}
				}


			} else {
				System.out.println("About:");
				System.out.println("");
				System.out
						.println("This tool exports the specific bin id's as JPEG file and stores them in the current directory");
				System.out.println("");
				System.out.println("Usage:");
				System.out.println("");
				System.out.println("arg[0] : application server");
				System.out.println("arg[1] : column");
				System.out.println("arg[2] : width of the picture");
				System.out.println("arg[3] : height of the picture");
				
				System.out
						.println("arg[4...] : the id of the bin's you like to convert, seperated by space");

				System.exit(-1);
			}
		} finally {
			BinGenerationLock.getInstance().releaseLock(args[1], "");
		}
	}

}