package edu.ucdavis.genomics.metabolomics.binbase.binbase.calculator;

import edu.ucdavis.genomics.metabolomics.binbase.dsl.calc.LocalExporter;

import java.io.File;

public class GroovyDSLCalculator {

	public static void main(final String args[]) {
		if (args.length < 1) {
			System.out.println("Usage:");
			System.out.println("");
			System.out
					.println("arg[0] : dsl file which defines the calculation");

			System.out.println("");

			System.exit(-1);
		}

		new LocalExporter(new File(args[0])).run();

	}
}
