package edu.ucdavis.genomics.metabolomics.binbase.binbase.dsl;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;

import edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromDatabase;

/**
 * generates a dsl for the given class id 
 * @author wohlgemuth
 *
 */
public class GenerateDSLFromClassId {

	/**
	 * main method
	 * @param args
	 */
	public static void main(String args[]){
		
		if (args.length != 4) {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out
					.println("arg[2] : class id's seperated by coma like '1,2,3,4,5' ");

			System.out.println("arg[3] : outPutFile:");
			System.out.println("");

			System.exit(-1);
		}
		
		Collection<String> classes = new HashSet<String>();
		
		for(String s : args[2].split(",")){
			classes.add(s);
		}
		
		GenerateDSLFromDatabase generate = new GenerateDSLFromDatabase();
		String dsl = generate.dslGenerateForClasses(classes, args[1], args[0],true,80,"xls",true);
		generate.writeDSL(new File(args[3]), dsl);
	}
}
