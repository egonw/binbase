package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library;

/**
 * access directly the database
 * 
 * @author wohlgemuth
 * 
 */
public interface DatabaseLibraryMatchable extends LibraryMatchable {

	/**
	 * returns the database server
	 * 
	 * @return
	 */
	public String getDatabaseServer();

	/**
	 * returns the database password
	 * 
	 * @return
	 */
	public String getDatabasePassword();

	/**
	 * returns the database name
	 * 
	 * @return
	 */
	public String getDatabaseName();

	/**
	 * returns the username
	 * 
	 * @return
	 */
	public String getUserName();

	public void setUserName(String value);

	public void setDatabaseName(String value);

	public void setDatabasePassword(String value);

	public void setDatabaseServer(String value);
}
