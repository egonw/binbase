package edu.ucdavis.genomics.metabolomics.binbase.binbase.dsl;

import edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromDatabase;

import java.io.File;

/**
 * generats a DSL for the given arguments
 * @author wohlgemuth
 *
 */
public class GenerateDSLFromSetupxID {

	/**
	 * main method
	 * @param args
	 */
	public static void main(String args[]){
		
		if (args.length < 4) {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out
					.println("arg[2] : experiment name or setupx id");

			System.out.println("arg[3] : outPutFile:");
			System.out.println("");

			System.exit(-1);
		}
		
		GenerateDSLFromDatabase generate = new GenerateDSLFromDatabase();
		String dsl = generate.dslGenerateForExperiment(args[2], args[1], args[0],true,80,"xls");
		generate.writeDSL(new File(args[3]), dsl);
	}
}
