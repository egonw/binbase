
package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.export;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.TXT;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.math.BinSimilarityMatrix;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.util.HibernatePreparation;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.binbase.util.locking.BinGenerationLock;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

/**
 * calculates a similarity matrix for bins
 * 
 * @author wohlgemuth
 * 
 */
public class CalculateSimilarityMatrix {

	public static void main(String[] args) throws FileNotFoundException,
			IOException, Exception, ClusterException, NamingException {
		if (args.length < 4) {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out
					.println("arg[2] : mode (known-known unknown-unknown known-unknown)");

			System.out.println("arg[3] : outPutFile:");
			System.out.println("");

			System.exit(-1);
		}


		try {
			Logger logger = Logger.getLogger("similarity");
			logger.info("locking bin table...");
			BinGenerationLock.getInstance().obtainLock(args[1],"");
			logger.info("locked...");
			
			BinSimilarityMatrix matrix = new BinSimilarityMatrix();

			HibernatePreparation.prepare(args[0], args[1]);

			DataFile file = null;

			if (args[2].equals("known-known")) {
				file = matrix.createKnownVsKnownMatrix();
			} else if (args[2].equals("known-unknown")) {
				file = matrix.createKnownVsUnknownMatrix();
			} else if (args[2].equals("unknown-unknown")) {
				file = matrix.createUnknownVsUnknownMatrix();
			} else {
				System.out.println("");
				System.out
						.println("sorry unknown mode, please provide a valid mode");
				System.out.println("");
				System.out.println("known-known");
				System.out.println("known-unknown");
				System.out.println("unknown-unknown");
				System.out.println("");
				System.exit(-1);
			}

			TXT txt = new TXT();
			txt.write(new FileOutputStream(new File(args[3])), file);
		} finally {

			BinGenerationLock.getInstance().releaseLock(args[1],"");
		}
	}
}
