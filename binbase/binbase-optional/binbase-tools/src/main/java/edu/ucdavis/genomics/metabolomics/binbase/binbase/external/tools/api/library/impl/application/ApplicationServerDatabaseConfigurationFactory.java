package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.application;

import java.util.HashMap;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfiguration;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfigurationFactory;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities;

/**
 * uses the application server to configure the source
 * 
 * @author wohlgemuth
 */
public class ApplicationServerDatabaseConfigurationFactory extends DatabaseConfigurationFactory {

	public static final String APPLICATION_SERVER = "application.server";
	public static final String DATABASE_COLUMN = "database.column";
	ApplicationServerDatabaseConfiguration configuration = new ApplicationServerDatabaseConfiguration();

	@Override
	public DatabaseConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * prepares and configures the configuration
	 */
	@Override
	public void prepare(final Map<?, ?> map) {

		assert (map.get(DATABASE_COLUMN) != null);
		assert (map.get(APPLICATION_SERVER) != null);

		// here we set the system properties needed for connection
		XMLConfigurator.getInstance().reset();
		XMLConfigurator.getInstance().addConfigurationPropertie("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory", true);
		XMLConfigurator.getInstance().addConfigurationPropertie("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces", true);
		XMLConfigurator.getInstance().addConfigurationPropertie("java.naming.provider.url", map.get(APPLICATION_SERVER) + ":1099", true);

		try {
			final DatabaseJMXFacade facade = Configurator.getDatabaseService();

			configuration.setDatabaseName(facade.getDatabase());
			configuration.setHostName(facade.getDatabaseServer());
			configuration.setPassword(facade.getDatabaseServerPassword());
			configuration.setUserName(map.get(DATABASE_COLUMN).toString());
			configuration.setType(String.valueOf(DriverUtilities.POSTGRES));
		}
		catch (final Exception e) {
			throw new FactoryException(e.getMessage(), e);
		}
	}

	public static Map<String, String> buildConfiguration(final String applicationServer, final String column) {
		final Map<String, String> map = new HashMap<String, String>();
		map.put(APPLICATION_SERVER, applicationServer);
		map.put(DATABASE_COLUMN, column);

		return map;
	}
}
