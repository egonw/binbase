package edu.ucdavis.genomics.metabolomics.binbase.binbase.dsl;

import java.io.File;

import edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromDatabase;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromSXDump;

public class GenerateDSLFromSXDumpFile {
	/**
	 * main method
	 * @param args
	 */
	public static void main(String args[]){
		
		if (args.length < 4) {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out
					.println("arg[2] : inputFile");

			System.out.println("arg[3] : outPutFile:");
			System.out.println("");

			System.exit(-1);
		}
		
		GenerateDSLFromSXDump generate = new GenerateDSLFromSXDump();
		String dsl = generate.dslGenerateFromXMLDump(new File(args[2]), args[1], args[0],true,80,"xls",null,25,true);
		GenerateDSLFromDatabase.writeDSL(new File(args[3]), dsl);
	}
}
