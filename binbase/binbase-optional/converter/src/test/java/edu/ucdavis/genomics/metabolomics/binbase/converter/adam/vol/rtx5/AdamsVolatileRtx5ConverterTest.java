package edu.ucdavis.genomics.metabolomics.binbase.converter.adam.vol.rtx5;

import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.converter.AbstractConverterTest;
import edu.ucdavis.genomics.metabolomics.binbase.converter.Converter;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.sjp.parser.msp.MSPParser;
import edu.ucdavis.genomics.metabolomics.sjp.handler.ConsoleHandler;

import java.io.File;
import java.util.Scanner;

public class AdamsVolatileRtx5ConverterTest extends AbstractConverterTest {
    private static final String SRC_TEST_RESOURCES_ADAMS_VOLATILE_MSP = "src/test/resources/adams_volatile.msp";
    private static final String TARGET_RESULT_RESULT3_MSP = "target/result.msp";

    private Logger logger = Logger.getLogger(getClass());

    @Override
    protected Converter createConverter() {
        return new AdamsVolatileRtx5Converter();
    }

    @After
    @Override
    public void tearDown() throws Exception {

        File file = new File(TARGET_RESULT_RESULT3_MSP);

        if (file.exists()) {
            file.delete();
        }
        super.tearDown();
    }

    @Test
    @Override
    public void testConvertFileStringString() throws Exception {
        String from = SRC_TEST_RESOURCES_ADAMS_VOLATILE_MSP;
        String to = TARGET_RESULT_RESULT3_MSP;
        converter.convertFile(from, to);

        File file = new File(TARGET_RESULT_RESULT3_MSP);
        assertTrue(file.exists());

        new MSPParser().parse(file, new ConsoleHandler());

        Scanner scanner = new Scanner(file);

        int counter = 0;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            logger.info(line);
            if (line.startsWith("RI:")) {
                String content[] = line.split(":");

                assertTrue(content[0].equals("RI"));
                double value = Double.parseDouble(content[1]);

                if (counter == 0) {
                    assertTrue(Math.abs(value - 343632) < 0.01);
                } else if (counter == 1) {
                    assertTrue(Math.abs(value - 499881) < 0.01);
                }

                counter++;
            }
        }

        logger.info(counter);
        assertTrue(counter == 2);
    }

    @Test
    @Override
    public void testConvertFileFileFile() throws Exception {
        File from = new File(SRC_TEST_RESOURCES_ADAMS_VOLATILE_MSP);
        File to = new File(TARGET_RESULT_RESULT3_MSP);
        converter.convertFile(from, to);

        File file = new File(TARGET_RESULT_RESULT3_MSP);
        assertTrue(file.exists());

        new MSPParser().parse(file, new ConsoleHandler());

        Scanner scanner = new Scanner(file);

        int counter = 0;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            logger.info(line);
            if (line.startsWith("RI:")) {
                String content[] = line.split(":");

                assertTrue(content[0].equals("RI"));
                double value = Double.parseDouble(content[1]);

                if (counter == 0) {
                    assertTrue(Math.abs(value - 343632) < 0.01);
                } else if (counter == 1) {
                    assertTrue(Math.abs(value - 499881) < 0.01);
                }

                counter++;
            }
        }

        logger.info(counter);
        assertTrue(counter == 2);
    }

    @Test
    @Override
    public void testConvertFileSourceSource() throws Exception {
        Source from = new FileSource(new File(SRC_TEST_RESOURCES_ADAMS_VOLATILE_MSP));
        FileDestination to = new FileDestination();
        to.setIdentifier(TARGET_RESULT_RESULT3_MSP);
        converter.convertFile(from, to);

        File file = new File(TARGET_RESULT_RESULT3_MSP);
        assertTrue(file.exists());

        new MSPParser().parse(file, new ConsoleHandler());

        Scanner scanner = new Scanner(file);

        int counter = 0;

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            logger.info(line);
            if (line.startsWith("RI:")) {
                String content[] = line.split(":");

                assertTrue(content[0].equals("RI"));
                double value = Double.parseDouble(content[1]);

                if (counter == 0) {
                    assertTrue(Math.abs(value - 343632) < 0.01);
                } else if (counter == 1) {
                    assertTrue(Math.abs(value - 499881) < 0.01);
                }

                counter++;
            }
        }

        logger.info(counter);
        assertTrue(counter == 2);
    }


    @Test
    @Override
    public void testConvertRetentionIndex() throws Exception {
        double value = converter.convert(529);
        assertTrue(Math.abs(value - 154625.3911) < 0.5);
    }

    @Override
    public void testConvertRetentionIndexArray() throws Exception {
        double value[] = converter.convert(new double[]{529, 606, 623});

        assertTrue(Math.abs(value[0] - 154625.3911) < 0.01);
        assertTrue(Math.abs(value[1] - 197628.4982) < 0.01);
        assertTrue(Math.abs(value[2] - 206972.8536) < 0.01);

        assertFalse(Math.abs(value[0] - 1546118) < 0.01);
        assertFalse(Math.abs(value[1] - 1976119) < 0.01);
        assertFalse(Math.abs(value[2] - 2069622) < 0.01);


    }

}
