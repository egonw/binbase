package edu.ucdavis.genomics.metabolomics.binbase.converter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * tests our converter class
 * @author wohlgemuth
 *
 */
public abstract class AbstractConverterTest {

	protected Converter converter;
	@Before
	public void setUp() throws Exception {
		converter = createConverter();
		
	}

	/**
	 * creates a converter
	 * @return
	 */
	protected abstract Converter createConverter();
	
	@After
	public void tearDown() throws Exception {
		converter = null;
	}

@Test
	public abstract void testConvertFileStringString() throws Exception;

@Test
	public abstract void testConvertFileSourceSource() throws Exception;

@Test
	public abstract void testConvertFileFileFile() throws Exception;


	@Test
	public abstract void testConvertRetentionIndex() throws Exception;
	@Test
	public abstract void testConvertRetentionIndexArray() throws Exception;
}
