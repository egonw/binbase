package edu.ucdavis.genomics.metabolomics.binbase.converter;

import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination;
import edu.ucdavis.genomics.metabolomics.binbase.converter.exception.ConverterException;

import java.io.File;

/**
 * a basic converter to convert units
 * @author wohlgemuth
 *
 */
public interface Converter {

    /**
     * converts two MSP files
     * @param from
     * @param to
     */
    void convertFile(File from, File to) throws ConverterException;

    /**
     * converts two msp files
     * @param from
     * @param to
     */
    void convertFile(String from, String to) throws ConverterException;


    /**
     * converts a simple retention index
     * @param retentionIndex
     * @return
     */
    double convert(double retentionIndex) throws ConverterException;

    /**
     * converts an array of retention indexes
     * @param retentionIndex
     * @return
     */
    double[] convert(double[] retentionIndex) throws ConverterException;

    /**
     * converts from one MSP source to another MSP source
     * @param from
     * @param to
     */
    void convertFile(Source from, Destination to) throws ConverterException;
}
