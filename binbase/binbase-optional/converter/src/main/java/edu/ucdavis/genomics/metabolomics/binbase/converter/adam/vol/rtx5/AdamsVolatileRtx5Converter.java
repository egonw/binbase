package edu.ucdavis.genomics.metabolomics.binbase.converter.adam.vol.rtx5;

import edu.ucdavis.genomics.metabolomics.binbase.converter.Converter;
import edu.ucdavis.genomics.metabolomics.binbase.converter.exception.ConverterException;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.sjp.parser.msp.MSPParser;
import edu.ucdavis.genomics.metabolomics.sjp.exception.ParserException;
import edu.ucdavis.genomics.metabolomics.sjp.ParserHandler;
import edu.ucdavis.genomics.metabolomics.sjp.handler.ConsoleHandler;

import java.io.File;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import org.apache.log4j.Logger;

/**
 * User: wohlgemuth
 * Date: Sep 3, 2009
 * Time: 3:27:18 PM
 */
public class AdamsVolatileRtx5Converter implements Converter {

    private Logger logger = Logger.getLogger(getClass());

    public void convertFile(File from, File to) throws ConverterException {
        logger.info("converting: " + from.getAbsolutePath() + " to " + to.getAbsolutePath());
        FileSource fromSource = null;
        FileDestination toSource = null;

        try {
            fromSource = new FileSource(from);
        } catch (ConfigurationException e) {
            logger.error(e.getMessage(), e);
            throw new ConverterException(e);
        }

        toSource = new FileDestination();
        try {
            toSource.setIdentifier(to);
        } catch (ConfigurationException e) {
            logger.error(e.getMessage(), e);
            throw new ConverterException(e);
        }

        convertFile(fromSource, toSource);
    }

    public void convertFile(String from, String to) throws ConverterException {
        convertFile(new File(from), new File(to));
    }


    /**
     * converts all RI's in the msp file from to the library using the internal formula
     *
     * @param from
     * @param to
     * @throws ConverterException
     */
    public void convertFile(Source from, Destination to) throws ConverterException {
        try {
            Scanner fromScanner = new Scanner(from.getStream());

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(to.getOutputStream()));

            while (fromScanner.hasNextLine()) {
                String line = fromScanner.nextLine();

                if (line.startsWith("RI:")) {
                    String value = line.split(":")[1];

                    double result = convert(Double.parseDouble(value));
                    writer.write("AdamsRI:" + value + "\n");
                    writer.write("RI:" + (int) result + "\n");


                } else {
                    writer.write(line);
                    writer.write("\n");
                }
            }

            writer.flush();
            writer.close();

        } catch (IOException e) {
            throw new ConverterException(e);
        }


    }

    /**
     * converts the value based on the formule described here
     *
     * @param retentionIndex
     * @return
     */
    public double convert(double retentionIndex) {
        return -0.093765414581 * retentionIndex * retentionIndex + 664.905656251699 * retentionIndex - 170870.293682091000;
    }

    public double[] convert(double[] retentionIndex) {
        double[] result = new double[retentionIndex.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = convert(retentionIndex[i]);
        }

        return result;
    }

    /**
     * simple main method
     *
     * @param args
     */
    public static void main(String[] args) throws ConverterException {
        if (args.length == 1) {
            System.out.println(
                    new AdamsVolatileRtx5Converter().convert(Double.parseDouble(args[0]))
            );
        } else if (args.length == 2) {
            new AdamsVolatileRtx5Converter().convertFile(args[0], args[1]);
        } else {                                       
            System.out.println("Usage:");
            System.out.println("convert simple ri, please provide one number on the adams scale");
            System.out.println("batch convert ris, please provide an input MSP and an output MSP files");
        }

    }

}
