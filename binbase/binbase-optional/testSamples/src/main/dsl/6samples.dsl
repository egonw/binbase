/**
 * simple examples how to calculate samples with a dsl
 * and binbase
 * just execute
 * java -jar DSLCalculator.jar 6samples.dsl
 *
 * but please make sure that the data files are in a
 * binbase accessable directory
 *
 */
export{
	//name of the generated experiment
	name "6samplesExample"

	//server of the used binbase instance
	server "127.0.0.1"

	//database of the used binbase instance
	column "binbase"

	//are new bins permitted
	newBins true

	//a simple class with 6 samples
	Class(name: "6SamplesExample") {
		sample "4125ea02_2"
		sample "4125ea03_2"
		sample "4125ea04_2"
		sample "4125ea02_2"
		sample "4125ea03_2"
		sample "4125ea04_2"
	}

	//defintion for the report
	report{

		//specification how far we want to size down the dataset
		sizedown 80

		//do we want to replace zeros
		replace

		//specified output format
		format "xls"
	}
	//end of report definiton
}
//end of DSL definition
