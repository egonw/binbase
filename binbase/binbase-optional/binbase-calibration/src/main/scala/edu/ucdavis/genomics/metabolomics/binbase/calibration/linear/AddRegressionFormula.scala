package edu.ucdavis.genomics.metabolomics.binbase.calibration.linear

import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.DontWriteProcessableResult
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.FormatObject
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.ObjectDetector
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.CalibrationFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.BinNotFoundException
import edu.ucdavis.genomics.metabolomics.util.math.LinearRegression


/**
 * adds a report with the regression formula to the report
 */
class AddRegressionFormula  extends DontWriteProcessableResult with ObjectDetector {

  def getDescription: String = "add's rows with the regression formula to the datafile, if the calibrated datafile used this model"

  /**
   * does the actual processing
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    datafile match {
      case file: CalibrationFile =>
        //find out if it used a linear model
        val linear = true

        //add a row with the R2 score for each bin to this datafile

        if (linear) {
          val position: Int = insertProtectedRow(datafile)

          file.getCell(0, position).asInstanceOf[FormatObject[Any]].setValue("formula")

          file.getCalibrationStandards().toSet.foreach { standard: String =>
            try {
              val bin = file.getBinPosition(standard)
              val formula = file.getCurve(standard).asInstanceOf[LinearRegression].getFormula()
              file.getCell(bin, position).asInstanceOf[FormatObject[Any]].setValue(formula)
            } catch {
              case e: BinNotFoundException => //nothing
            }
          }
        }

        return file
      case _ =>
        //wrong kind of file just return it
        return datafile
    }
  }
}