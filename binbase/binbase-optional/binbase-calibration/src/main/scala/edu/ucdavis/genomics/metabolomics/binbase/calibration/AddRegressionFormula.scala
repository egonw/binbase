package edu.ucdavis.genomics.metabolomics.binbase.calibration

import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.DontWriteProcessableResult
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.FormatObject
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.ObjectDetector
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.CalibrationFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.BinNotFoundException
import edu.ucdavis.genomics.metabolomics.util.math.LinearRegression

class AddRegressionFormula extends DontWriteProcessableResult with ObjectDetector {

  def getDescription: String = "add's rows with the regression formula to the datafile"

  /**
   * does the actual processing
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    datafile match {
      case file: CalibrationFile =>

        var positions: List[Int] = List.empty

        file.getCalibrationStandards().toSet.foreach { standard: String =>
          try {
            val bin = file.getBinPosition(standard)

            val formulas = file.getCurve(standard).getFormulas()

            while (formulas.size > positions.size) {
              val position = insertProtectedRow(datafile)
              positions = positions :+ position
              file.getCell(0, position).asInstanceOf[FormatObject[Any]].setValue("formula")

            }

            val it = positions.iterator
            formulas.foreach { formula: String =>

              file.getCell(bin, it.next).asInstanceOf[FormatObject[Any]].setValue(formula)

            }

          } catch {
            case e: BinNotFoundException => //nothing
          }
        }

        return file
      case _ =>
        //wrong kind of file just return it
        return datafile
    }
  }
}