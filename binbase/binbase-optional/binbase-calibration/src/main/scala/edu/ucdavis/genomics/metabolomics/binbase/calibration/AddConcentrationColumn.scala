package edu.ucdavis.genomics.metabolomics.binbase.calibration
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.DontWriteProcessableResult
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.ObjectDetector
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.CalibrationFile
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.FormatObject
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.math.LinearRegression
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.BinNotFoundException
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.HeaderFormat
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.SampleObject
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.NotCalibratedException
import org.apache.log4j.Logger
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.ContentObject

class AddConcentrationColumn extends DontWriteProcessableResult with ObjectDetector {

  val logger = Logger.getLogger(getClass());

  def getDescription: String = "add's a column with all the concentrations for each sample"

  /**
   * does the actual processing
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    datafile match {
      case file: CalibrationFile =>
        //find out if it used a linear model

        val column = insertProtectedColumn(file)

        val conc = new HeaderFormat[String]("concentration");

        file.setCell(column, 0, conc)

        file.getSamples().foreach { sample: SampleObject[String] =>
          val row = file.getSamplePosition(sample.getValue())

          try {

            if (sample.getAttributes().get("concentration") != null) {
              val value = sample.getAttributes().get("concentration");
              file.setCell(column, row, new ContentObject[String](value))
            }
          } catch {
            case t: NotCalibratedException =>
              logger.info("it looks like we tried to access a none calibrated sample", t)
          }
        }
        return file
      case _ =>
        //wrong kind of file just return it
        return datafile
    }
  }
}