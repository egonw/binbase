export{

	server "eros"
	
	name "calibration file examle"
	
	newBins false
	
	multithread false
	
	caching false
	
	ignoreMissingSamples false
	
report{

                //specification how far we want to size down the dataset
                sizedown 10

                //do we want to replace zeros
                replace{

                         //replacement method
                        method "edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.ReplaceWithQuantIntesnityBasedOnAverageRTwithRiCurveFallback"

						//does the actual calibration
                        method "edu.ucdavis.genomics.metabolomics.binbase.calibration.CalculateCalibrationCurves"

                        //transpose datafile
                        postProcess(method:"edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.TransposeDataFileProcess", format:"xls")

                        //write result
                        postProcess(fileName:"transposed",method:"edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.WriteProcessedResult", format:"xls")
                }

                //specified output format
                format "xls"
        }

   //this is a calibration class to be used for calibrations. There should only 1 exist for each dsl
	CalibrationClass{
	
		sample "130329cmssa10", 0.01
		sample "130329cmssa11", 0.05
		sample "130329cmssa12", 0.1
		sample "130329cmssa14", 0.25
		sample "130329cmssa20", 0.5
		sample "130329cmssa22", 1
		sample "130329cmssa23", 2.5
		sample "130329cmssa24", 5
		sample "130329cmssa25", 10
		sample "130329cmssa26", 20
		
	}
}