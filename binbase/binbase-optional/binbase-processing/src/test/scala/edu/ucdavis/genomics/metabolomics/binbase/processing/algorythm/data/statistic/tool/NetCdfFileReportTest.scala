package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool

import junit.framework._
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFileFactory
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import org.scalatest.Spec
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource
import org.jdom._
import org.jdom.input.SAXBuilder
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.StaticStatisticActions
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.SampleTimeResolver
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.NetCDFResolver

/**
 * testing the reporting of this datafile
 */
@RunWith(classOf[JUnitRunner])
class NetCdfFileReportTest extends FunSuite with BeforeAndAfter {

  var file: ResultDataFile = null

  /**
   * setup of this test
   */
  before {
    System.setProperty(DataFileFactory.DEFAULT_PROPERTY_NAME,
      classOf[ResultDataFileFactory].getName())

    assert(new ResourceSource("/datafile/testFileSop.xml").exist())
    assert(new ResourceSource("/datafile/testFile.xml").exist())

    val sopDefinition: Document = new SAXBuilder().build(new ResourceSource(
      "/datafile/testFileSop.xml").getStream())
    val root: Element = sopDefinition.getRootElement()

    file = StaticStatisticActions.parseXMLContent(new ResourceSource(
      "/datafile/testFile.xml"), root.getChildren().get(0).asInstanceOf[Element])

    file.setIndexed(true)

    // we just mock the time and calculate the average over all data
    file.setResolver(new SampleTimeResolver() {
      def resolveTime(sample: String) = 5000;
    })

  }

  /**
   * cleanup
   */
  after {
    file = null
  }

  test("testing the processing of a report") {

    val report: NetCdfFileReport = new NetCdfFileReport() with TestResolver

    val result:DataFile = report.process(file, null)

    result.print(System.out)
    
    assert(result.getRowCount() == 7)
    assert(result.getColumnCount() == 2)
    
    result.getColumn(1).toList.foreach(arg =>
    	assert(arg == "exist" || arg == "true")
    )
    
  }

  /**
   * simple resolver to mock our object
   */
  trait TestResolver extends NetCDFResolver {
    override def hasNetcdf(name: String): Boolean = {

      return new ResourceSource("/datafile/" + name + ".cdf").exist()
    }
  }
} 