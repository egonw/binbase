package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool

import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFileFactory
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource
import org.jdom.Document
import org.jdom.input.SAXBuilder
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.StaticStatisticActions
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.SampleTimeResolver
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.combiner.CombineByMax
import edu.ucdavis.genomics.metabolomics.util.statistics.replacement.ReplaceWithMean
import edu.ucdavis.genomics.metabolomics.util.statistics.replacement.ReplaceWithMin
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.ReplaceWithQuantIntensityBasedOnAverageBinRtOverADay
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits._
import javax.naming.Context

@RunWith(classOf[JUnitRunner])
class MergeWithMiniXDataTest extends FunSuite with BeforeAndAfter with DataFilePrint {

  var file: ResultDataFile = null

  /**
   * setup of this test
   */
  before {

    System.setProperty(DataFileFactory.DEFAULT_PROPERTY_NAME,
      classOf[ResultDataFileFactory].getName())
    System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
    System.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
    System.setProperty(Context.PROVIDER_URL, "eros")

    assert(new ResourceSource("/datafile/testFileSop.xml").exist())
    assert(new ResourceSource("/datafile/testFile.xml").exist())

    val sopDefinition: Document = new SAXBuilder().build(new ResourceSource(
      "/datafile/testFileSop.xml").getStream())
    val root: Element = sopDefinition.getRootElement()

    file = StaticStatisticActions.parseXMLContent(new ResourceSource(
      "/datafile/testFile.xml"), root.getChildren().get(0).asInstanceOf[Element])

    // we just mock the time and calculate the average over all data
    file.setResolver(new SampleTimeResolver() {
      def resolveTime(sample: String) = 5000;
    })

    file.combineColumns(new CombineByMax())

    file.sizeDown(true, 1, 20)

    val replace: ReplaceWithQuantIntensityBasedOnAverageBinRtOverADay = new ReplaceWithQuantIntensityBasedOnAverageBinRtOverADay()
    replace.setFile(file)
    file.replaceZeros(replace, 1)
  }

  /**
   * cleanup
   */
  after {
    file = null
  }

  test("testing the merging of the minix data") {

    val report: MergeWithMiniXData = new MergeWithMiniXData()
    report.process(file, null)

  }

  test("testing the merging of the minix data while merging with another report") {

    val miniX: MergeWithMiniXData = new MergeWithMiniXData()
    val detection: DetectionReportByBin = new DetectionReportByBin()

    file = miniX.process(file, null).asInstanceOf[ResultDataFile]
    file = detection.process(file, null).asInstanceOf[ResultDataFile]

  }

  test("testing the merging of the minix data while merging with another report and transposing the result file") {

    val miniX: MergeWithMiniXData = new MergeWithMiniXData()
    val detection: DetectionReportByBin = new DetectionReportByBin()
    val transpose: TransposeDataFileProcess = new TransposeDataFileProcess()

    file = miniX.process(file, null).asInstanceOf[ResultDataFile]
    file = detection.process(file, null).asInstanceOf[ResultDataFile]
    printlnDatafile(transpose.process(file, null))

  }

}