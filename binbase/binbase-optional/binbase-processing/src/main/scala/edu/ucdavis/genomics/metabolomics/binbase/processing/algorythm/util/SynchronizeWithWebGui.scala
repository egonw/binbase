package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.util

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.action.{ ResultFileAction, BasicAction }
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.io.source.Source
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import java.net.URL
import scala.collection.JavaConversions._

/**
 * pushes notifications to the BinBaseWebGui to fetch metdata and update statistics in the system
 */
class SynchronizeWithWebGui extends ResultFileAction {

  override def getDescription: String = "initializes the synchronisation with the BinBase WebGui so that all the metadata are updated"

  override def getFolder: String = "none"

  /**
   * notifies the server that we want to upate the given experiment
   */
  override def runDatafile(result: ResultDataFile, configuration: Element, rawdata: Source, sop: Source) = {

    getLogger().info("updating web gui's with content...");

    if (configuration.getChildren("destination").isEmpty()) {
      throw new NullPointerException("please provide an element with the name 'destination' to the web gui! The content should be the url or a collection of attributes containing mappings to the database to be used");
    }

    val element: List[Element] = configuration.getChildren("destination").toList.asInstanceOf[List[Element]]

    element.foreach { e =>
      if (e.getAttributeValue("database") != null && e.getAttributeValue("url") != null) {
        if (e.getAttributeValue("database").trim().equals(result.getDatabase().trim())) {

          invokeUrl(result, e.getAttributeValue("url"))

        }
      } else if (e.getText().isEmpty() == false) {
        invokeUrl(result, e.getText())
      } else {
        throw new NullPointerException("please provide an element with the name 'destination' to the web gui! The content should be the url or a the attributes 'database' and 'url', incase you want to push results to different databases");

      }
    }

    //done

  }

  private def invokeUrl(result: edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile, urlLocation: String): Unit = {
    var url: String = urlLocation

    if (url.endsWith("/BBSynchronize/synchronizeExperiment") == false) {
      url = url + "/BBSynchronize/synchronizeExperiment/"
    }

    if (url.endsWith("/") == false) {
      url = url + "/"
    }

    url = url + result.getResultId().toString

    getLogger().info("invoking url: " + url);

    val invokeURL = new URL(url)

    val ssource = scala.io.Source.fromInputStream(invokeURL.openStream())

    getLogger().info("response: " + ssource.mkString);

  }
}