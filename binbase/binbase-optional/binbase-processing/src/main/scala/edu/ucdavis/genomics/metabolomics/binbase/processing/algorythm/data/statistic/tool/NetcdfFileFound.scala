package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.NullObject
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.ContentObject
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`._
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.RemoteNetCDFResolver

/**
 * adds a column to show if the netcdf file was found or not
 */
class NetcdfFileFound extends DontWriteProcessableResult with ObjectDetector with RemoteNetCDFResolver {

  def getDescription: String = "adds a column to your result file, which contains the information if the netcdf file was available"

  /**
   * adds the actual column to the datafile
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    //insert a new column
    val column = insertProtectedColumn(datafile)
    datafile.getCell(column, 0).asInstanceOf[FormatObject[Any]].setValue("netcdf file found")

    //go over all rows and check if the file exist
    datafile.getSamples().toList.foreach { sample =>

      //position of this sample
      val samplePosition: Int = datafile.getSamplePosition(sample.getValue())

      //query service is this sample exist
      val found: Boolean = hasNetcdf(sample.getValue())

      //update the result table
      datafile.getCell(column, samplePosition).asInstanceOf[CalculationObject[Any]].setValue(found.toString())
    }

    //return datafile
    return datafile;
  }
}