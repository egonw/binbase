package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.NullObject
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.ContentObject
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`._
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.RemoteNetCDFResolver
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory
import java.sql.PreparedStatement
import java.sql.ResultSet

/**
 * adds a row to the bins showing if this is a newly generated bin or not in this dataset
 * for the given sample id's
 */
class NewBin extends DontWriteProcessableResult with ObjectDetector with RemoteNetCDFResolver {

  def getDescription: String = "adds a row to your result file, which contains the information if the given bin was generated in this dataset"

  /**
   * adds the actual column to the datafile
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    //create sql connection
    val connectionFactory: ConnectionFactory = ConnectionFactory.createFactory()
    val connection = createDatabaseConnection(datafile.getDatabase(), connectionFactory)

    val statement: PreparedStatement = connection.prepareStatement("select * from bin where sample_id = ? and bin_id = ? ")

    //insert a new column
    val row = insertProtectedRow(datafile)
    datafile.getCell(0, row).asInstanceOf[FormatObject[Any]].setValue("newly generated bin in dataset")

    //go over all bins 
    datafile.getBins(true).toList.foreach { bin: HeaderFormat[String] =>

      val binId: Int = Integer.parseInt(bin.getAttributes().get("id"))

      val binPosition: Int = datafile.getBinPosition(binId)

      //retrieve the sample id's for the given bin
      datafile.getSamples().toList.foreach { sample: SampleObject[String] =>

        //position of this sample
        val samplePosition: Int = datafile.getSamplePosition(sample.getValue())

        val sampleId: Int = Integer.parseInt(sample.getAttributes().get("id"))

        val newBin: Boolean = wasGenerated(binId, sampleId, statement)

        //we only update datasets in case a bin was generated, no need to confuse people
        if (newBin) {
          datafile.getCell(binPosition, row).asInstanceOf[FormatObject[Any]].setValue(true)
        }

      }
    }

    connectionFactory.close(connection)

    //return datafile
    return datafile;
  }

  /**
   * queries the database to determine if this bin was newly generated
   */
  def wasGenerated(binId: Integer, sampleId: Integer, statement: PreparedStatement): Boolean = {

    statement.setInt(1, sampleId)
    statement.setInt(2, binId)

    val set: ResultSet = statement.executeQuery()

    var result = false

    if (set.next()) {
      result = true
    }

    set.close()
    return result
  }
}