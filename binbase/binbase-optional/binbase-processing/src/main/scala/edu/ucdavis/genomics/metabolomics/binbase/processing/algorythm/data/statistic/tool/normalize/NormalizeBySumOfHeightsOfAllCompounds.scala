package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.normalize
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.DontWriteProcessableResult
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.NullObject
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.ContentObject
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`._
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.RemoteNetCDFResolver

/**
 * normalizes the data based on the sum of the height of all compounds
 */
class NormalizeBySumOfHeightsOfAllCompounds extends DontWriteProcessableResult {

  def getDescription: String = "normalizes the result set based on the sum of all heights of all compounds."

  /**
   * does the actual processing and works on the original datafile and returns the modified datafile
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    datafile.getSamples().toList.foreach { sample: SampleObject[String] =>

      val sum: Double = calculateSum(datafile, sample)

      datafile.getBins().toList.foreach { bin: HeaderFormat[String] =>
        val spectra: FormatObject[Double] = datafile.getSpectra(sample.getValue(), Integer.parseInt(bin.getAttributes().get("id"))).asInstanceOf[FormatObject[Double]]
        spectra.value = spectra.getValue().asInstanceOf[Double] / sum

      }

    }
    //return datafile
    return datafile;
  }

  /**
   * calculates the sum over this datafile
   */
  def calculateSum(datafile: ResultDataFile, sample: SampleObject[String]): Double = {
    var sum: Double = 0

    datafile.getBins().toList.foreach { bin: HeaderFormat[String] =>
      val spectra: FormatObject[_] = datafile.getSpectra(sample.getValue(), Integer.parseInt(bin.getAttributes().get("id")))
      sum = sum + spectra.getValue().asInstanceOf[Double]
    }
    sum
  }
}