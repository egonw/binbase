package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import java.util.List
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.FormatObject

/**
 * simple trait to help with datafile printing
 */
trait DataFilePrint {

  def printlnDatafile(file: DataFile, classes: Boolean = false) = {
    printDatafile(file, classes)
  }

  /**
   * prints the data file and
   */
  def printDatafile(file: DataFile, classes: Boolean = false, width: Int = 40) = {

    println("")
    var rows: Int = 0

    val rowsIgnore: Array[Int] = file.getIgnoreRows()
    val columnsIgnore: Array[Int] = file.getIgnoreColumns()

    printLine(file.getTotalColumnCount(), width)

    //go over the internal matrix
    file.getData().toList.foreach { row =>

      var rowIgnore: Boolean = false

      //do we ignore this row
      rowsIgnore.foreach(x =>
        if (x == rows) {
          rowIgnore = true
        })

      var columns: Int = 0

      print("|")
      //go over the columns
      row.asInstanceOf[List[Object]].toList.foreach { column =>
        var columnIgnore: Boolean = false

        //do we ignore this row
        columnsIgnore.foreach(x =>
          if (x == columns) {
            columnIgnore = true
          })

        if (columnIgnore || rowIgnore) {
          if (classes) {
            printf( "[%" + width + "s ] |", clean(column.getClass().getSimpleName(), width))
          } else {
            column match {
              case o: FormatObject[Any] => {
                printf(" [%" + width + "s ] |", clean(o.getValue().toString(), width))
              }
              case _ => {
                printf(" [%" + width + "s ] |", clean(column.toString(), width))
              }
            }
          }
        } else {
          if (classes) {
            printf(" %" + width + "s    |", clean(column.getClass().getSimpleName(), width))
          } else {
            column match {
              case o: FormatObject[Any] => {
                printf(" %" + width + "s    |", clean(o.getValue().toString(), width))
              }
              case _ => {
                printf(" %" + width + "s    |", clean(column.toString(), width))
              }
            }
          }
        }

        columns = columns + 1
      }

      rows = rows + 1
      System.out.println("")

      printLine(columns, width)

    }

  }

  private def clean(value: String, width: Int) = {
    var result = value

    if (result.length() > width) {
      result = result.substring(0, width - 5) + "..."
    }
    result
  }

  private def printLine(columns: Int, width: Int) = {

    print("+")
    for (i <- 0 until columns) {
      for (x <- 0 until (width + 5)) {
        print("-")
      }
      print("+")
    }

    println()
  }
}