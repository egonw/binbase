package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator._
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceFactory

/**
 * resolves netcdf files using the jboss
 */
trait RemoteNetCDFResolver extends NetCDFResolver {
  /**
   * do we have a netcdf file
   */
  override def hasNetcdf(name: String): Boolean = {

    val service: BinBaseService = BinBaseServiceFactory.createFactory().createService()

    return service.hasNetCdfFile(name, getKeyManager().getInternalKey())

  }
}