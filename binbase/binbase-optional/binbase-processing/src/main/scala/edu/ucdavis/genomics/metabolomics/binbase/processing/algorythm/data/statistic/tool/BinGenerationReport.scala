package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import java.sql.Connection
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.Vector
import java.util.Collection
import edu.ucdavis.genomics.metabolomics.binbase.bdi.math.BinComparisonMatrix
import java.util.HashSet
import java.util.Set

/**
 * a report to generate the list of duplicated bins, needed for error detection
 */
class BinGenerationReport extends DontWriteProcessableResult {

  /**
   * the folder whe this report is located
   */
  override def getFolder(): String = {
    "report"
  }

  /**
   * name of this report
   */
  override def getFileIdentifier(): String = {
    "bin generation report"
  }

  /**
   * what does this report do
   */
  override def getDescription(): String = {
    "this reports all bins generated in this experiment"
  }

  /**
   * does the actual processing
   */
  override def process(file: ResultDataFile, config: Element): DataFile = {

    //query pareameters
    val riWindow:Int = 2000
    val similairty:Double = 800
    
    //query all bins generated in this experiment
    val resultId: Int = file.getResultId()

    val connectionFactory: ConnectionFactory = ConnectionFactory.createFactory()
    val connection = createDatabaseConnection(file.getDatabase(), connectionFactory)

    val statement: PreparedStatement = connection.prepareStatement("select bin_id from result_link a, bin b where result_id = ? and a.sample_id = b.sample_id")

    statement.setInt(1, resultId)

    val result: ResultSet = statement.executeQuery()

    val bins: Set[Integer] = new HashSet()

    while (result.next()) {
      val binId: Int = result.getInt(1)
      bins.add(binId)
    }

    //generate comparisson matrix
    if (bins.isEmpty() == false) {

      //prepare hibernate settings
      val matrix:BinComparisonMatrix = new BinComparisonMatrix()

      val resultFile:DataFile = matrix.createMatrix(riWindow,similairty,bins)
      //return matrix
      return resultFile
    } else {
      
      //return nothing since no bins were generated
      return null
    }

  }

}