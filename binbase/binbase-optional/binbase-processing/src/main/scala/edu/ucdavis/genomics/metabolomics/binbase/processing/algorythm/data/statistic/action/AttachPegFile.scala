package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.action

/**
 * attaches an peg file
 */
class AttachPegFile extends AttachSMPFile {

  override protected def getFileExtension(): String = "peg"

  override def getDescription: String = "this action attaches all peg files to this result. Please make sure that the attribute with the name 'path' contains the paths, where the file are to be found is specified!"

  override def getFolder: String = "rawdata/peg"

}