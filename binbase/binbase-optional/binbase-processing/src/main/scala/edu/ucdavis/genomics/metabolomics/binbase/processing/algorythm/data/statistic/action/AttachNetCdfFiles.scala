package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.action
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.action.BasicAction
import edu.ucdavis.genomics.metabolomics.util.io.source.Source
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.action.ResultFileAction
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.SampleObject
import java.io.InputStream
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver.ResolverBuilder
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver.SimpleResolverBuilder
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.RawdataResolver
import java.io.File
import scala.util.control.Breaks._
import java.io.FileInputStream

/**
 * attaches all netcdf files to this experiment
 */
class AttachNetCdfFiles extends ResultFileAction {

  /**
   * in which folder do we want to store this result
   */
  def getFolder: String = "rawdata"

  /**
   * description of the action
   */
  def getDescription: String = "this action attaches all netcdf files to this result"

  /**
   * writes the actual sop file out
   */
  def runDatafile(datafile: ResultDataFile, configuration: Element, rawdata: Source, sop: Source) = {

    //build our resolver
    val builder: ResolverBuilder = new SimpleResolverBuilder()
    val resolver: List[RawdataResolver] = builder.build().toList

    //go over all samples
    datafile.getSamples().toList.foreach { sample: SampleObject[String] =>

      val sampleName: String = sample.getValue()

      //create a breakable loop
      breakable {
        resolver.foreach { current: RawdataResolver =>

          //retrieve the actual file
          val file: File = current.resolveNetcdfFile(sampleName)

          //check if the file is null
          if (file != null) {

            //check if the file actuall exist
            if (file.exists()) {
              
              //provide a stream
              val sampleStream: InputStream = new FileInputStream(file)

              //write it out
              writeCDFFile(sampleStream, sampleName, "cdf")
              
              sampleStream.close();
              
              //leave the sub loop
              break
            }
          }
        }
      }
    }

    //call it a day
  }
}
