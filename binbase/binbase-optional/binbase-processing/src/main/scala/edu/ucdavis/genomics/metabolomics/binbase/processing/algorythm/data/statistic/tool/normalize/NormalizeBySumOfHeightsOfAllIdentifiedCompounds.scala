package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.normalize
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.SampleObject
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.HeaderFormat
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.FormatObject

class NormalizeBySumOfHeightsOfAllIdentifiedCompounds extends NormalizeBySumOfHeightsOfAllCompounds {

  override def getDescription: String = "normalizes the result set based on the sum of all heights of all identified compounds."

  /**
   * calculates the sum based on that the bin id and name have to be identical to be not identified
   */
  override def calculateSum(datafile: ResultDataFile, sample: SampleObject[String]): Double = {
    var sum: Double = 0

    datafile.getBins().toList.foreach { bin: HeaderFormat[String] =>
      if (bin.getAttributes().get("id").trim().equals(bin.getAttributes().get("name").trim()) == false) {
        val spectra: FormatObject[_] = datafile.getSpectra(sample.getValue(), Integer.parseInt(bin.getAttributes().get("id")))
        sum = sum + spectra.getValue().asInstanceOf[Double]
      }
    }
    sum
  }
}