package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits
import edu.ucdavis.genomics.metabolomics.binbase.minix.wsdl.MiniXProvider

/**
 * a simple trait to hook into minix meta data directly
 */
trait MiniXSupport {

  /**
   * returns the url of the minix webservice
   */
  def getMiniXUrl():String
  
  /**
   * returns the species information for this minix sample
   */
  def getSpecies(sample: String): String = {
    new MiniXProvider(getMiniXUrl()).getSpecies(sample)
  }
  
  /**
   * returns the organ information for this sample
   */
  def getOrgan(sample: String): String = {
    new MiniXProvider(getMiniXUrl()).getOrgan(sample)
  }
  
  /**
   * returns treatment information for this sample
   */
  def getTreatment(sample: String): String = {
    new MiniXProvider(getMiniXUrl()).getTreatment(sample)
  }
  
  /**
   * returns comment information for this sample
   */
  def getComment(sample: String): String = {
    new MiniXProvider(getMiniXUrl()).getComment(sample)
  }
  
  /**
   * returns label information for this sample
   */
  def getLabel(sample: String): String = {
    new MiniXProvider(getMiniXUrl()).getLabel(sample)
  }
  
  
}