package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits

import java.awt.image.BufferedImage
import org.jfree.data.xy.AbstractXYDataset
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.chart.ChartFactory
import org.jfree.chart.plot.PlotOrientation
import org.jfree.data.xy.XYSeriesCollection
import java.awt.Color

trait GraphRenderer {

  /**
   * generates a XY line graph
   */
  def  createXYGraph(x:Array[Double], y:Array[Double],title:String = "" ,legend:String = "data",  xLabel:String = "x", yLabel:String = "y",width:Integer=320, height:Integer=240) : BufferedImage = {
    

    val series = new XYSeries(legend)

    val xIterator = x.iterator
    val yIterator = y.iterator
    
    while(xIterator.hasNext && yIterator.hasNext){
      series.add(xIterator.next, yIterator.next)
      
    }
    
    val dataset = new XYSeriesCollection()
    dataset.addSeries(series)
    
    val chart = ChartFactory.createScatterPlot(title,xLabel,yLabel,dataset,PlotOrientation.VERTICAL,false,false,false)
    chart.setBackgroundPaint(Color.white)
    
    chart.createBufferedImage(width, height)
    
  }
}