package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import org.jdom.Element
import scala.collection.JavaConversions._

/**
 * writes the processed result file
 */
final class WriteProcessedResult extends BasicProccessable {

  var postfix: String = null
  /**
   * the file will be attached to the result file
   */
  override def writeResultToFile(): Boolean = true

  /**
   * returns the folder where the netcdf report is stored
   */
  def getFolder(): String = {
    "result"
  }

  /**
   * returns our file identifier
   */
  override def getFileIdentifier(): String = {
    if (postfix == null) {
      return InternalCounter.increaseCounter() + "_result"
    } else {
      return InternalCounter.increaseCounter() + "_" + postfix
    }
  }

  def getDescription(): String = "writes a result file to the specified folder"

  /**
   * just returns the result so it can be written to a file
   */
  final def process(file: ResultDataFile, config: Element): DataFile = {
    simpleProcess(file, config)
  }

  /**
   * just return the file, the statistic processor is going todo all the work for us
   */
  final override def simpleProcess(file: DataFile, config: Element): DataFile = {
    config.getChildren("argument").toList.foreach { arg =>

      val element: Element = arg.asInstanceOf[Element]

      if (element.getAttributeValue("name").toLowerCase() == "filename") {
        this.postfix = element.getAttributeValue("value").trim()
      }
    }
    return file
  }

}

/**
 * simple counter
 */
object InternalCounter {
  var value: Int = 0

  /**
   * increase the counter by one
   */
  def increaseCounter(): Int = {
    value = value + 1
    return value
  }
}