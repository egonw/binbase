package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.action
import java.io.InputStream
import java.io.File
import java.io.FileInputStream
import scala.collection.JavaConversions._
import java.util.zip.GZIPInputStream
import org.apache.log4j.Logger
import org.jdom.Element

/**
 * attaches an smp file
 */
class AttachSMPFile extends AttachFile {

  val logger: Logger = Logger.getLogger(getClass())

  /**
   * retrieve the file from the specified path in the configuration element
   */
  protected def retrieveFile(name: String): InputStream = {

    getConfiguration().getChildren("path").toList.foreach { element =>

      val current: Element = element.asInstanceOf[Element]
      val path: String = current.getText().trim()

      val parent: File = new File(path.toString())

      if (parent.exists()) {
        var file: File = new File(parent, name + "." + getFileExtension())

        logger.info("checking for file: " + file.getAbsolutePath())

        if (file.exists()) {
          return new FileInputStream(file)
        }

        //incase the file is compressed
        file = new File(parent, name + "." + getFileExtension() + ".gz")

        logger.info("checking for compressed file: " + file.getAbsolutePath())

        if (file.exists()) {
          return new GZIPInputStream(new FileInputStream(file))
        }
      } else {
        logger.info("specified path was not found: " + path)
      }
    }

    logger.info("file with identifier: " + name + " was not found")

    return null
  }

  override def getFolder: String = "rawdata/smp"

  protected def getFileExtension(): String = "smp"

  def getDescription: String = "this action attaches all smp files to this result. Please make sure that the attribute with the name 'path' contains the paths, where the file are to be found is specified!"

}