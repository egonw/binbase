package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.NullObject
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.ContentObject
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits._

import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`._

/**
 * this enhances the default report with several detection statistics
 */
class DetectionReportByBin extends DontWriteProcessableResult with ObjectDetector {

  /**
   *
   */
  def getDescription: String = "adds several calculated statistics of the bin finding rates to the result file"

  /**
   * adds several statistics like
   *
   * count detected compounds
   * count replaced values
   * avg(intensity) detected compounds
   * avg(intensity) replaced values
   * avg replaced intensity divided by avg detected intensity
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    //insert rows for the results
    val foundBin: Int = insertProtectedRow(datafile)
    val replacedBin: Int = insertProtectedRow(datafile)
    val foundBinAverage: Int = insertProtectedRow(datafile)
    val replacedBinAverage: Int = insertProtectedRow(datafile)
    val averageFactor: Int = insertProtectedRow(datafile)

    //insert columns for the result file
    val foundBinInSample: Int = insertProtectedColumn(datafile)
    val notFoundBinInSample: Int = insertProtectedColumn(datafile)
    val notFoundFoundRatio: Int = insertProtectedColumn(datafile)

    //where do the spectra begin, just calculates the minimum of columns with spectra
    val spectraBegin = detectColumnsWithSpectra(datafile) reduceRight (_ min _)

    //set header information for rows
    datafile.getCell(0, foundBin).asInstanceOf[FormatObject[Any]].setValue("detected count of spectra")
    datafile.getCell(0, replacedBin).asInstanceOf[FormatObject[Any]].setValue("replaced count of spectra")
    datafile.getCell(0, foundBinAverage).asInstanceOf[FormatObject[Any]].setValue("detected count of spectra average")
    datafile.getCell(0, replacedBinAverage).asInstanceOf[FormatObject[Any]].setValue("replaced count of spectra average")
    datafile.getCell(0, averageFactor).asInstanceOf[FormatObject[Any]].setValue("avg replaced/avg detected")

    //set header information for columns
    datafile.getCell(foundBinInSample, 0).asInstanceOf[FormatObject[Any]].setValue("count of bins in sample")
    datafile.getCell(notFoundBinInSample, 0).asInstanceOf[FormatObject[Any]].setValue("count of missing bins in sample")
    datafile.getCell(notFoundFoundRatio, 0).asInstanceOf[FormatObject[Any]].setValue("missing/found ratio of bins in sample")

    //go over all samples and calculated the statistics for each sample
    datafile.getSamples().toList.foreach { sample =>
      //get the annotation count
      val annotations: Int = datafile.getMassspecsForSample(sample.getValue(), true).size()

      //calculate the missing count
      val missing: Int = datafile.getBins(true).size() - annotations

      //ratio between found and missing
      val ratio: Double = missing.asInstanceOf[Double] / annotations.asInstanceOf[Double]

      //position of this sample
      val samplePosition: Int = datafile.getSamplePosition(sample.getValue())

      //update the result table
      datafile.getCell(foundBinInSample, samplePosition).asInstanceOf[CalculationObject[Any]].setValue(annotations)
      datafile.getCell(notFoundBinInSample, samplePosition).asInstanceOf[CalculationObject[Any]].setValue(missing)
      datafile.getCell(notFoundFoundRatio, samplePosition).asInstanceOf[CalculationObject[Any]].setValue(ratio)

    }

    //go over all bins and calculate the statistics for each bin
    datafile.getBins(true).toList.foreach { bin =>

      //required variables
      var found: Int = 0
      var replaced: Int = 0
      var foundAverage: Double = 0
      var replacedAverage: Double = 0
      val binColumn: Int = datafile.getBinPosition(bin.getAttributes().get("id").toInt)

      try{
      //go over all samples
      datafile.getSamples().toList.foreach { sample =>

        //match the type of spectra to get out values
        datafile.getSpectra(sample.getValue(), bin.getAttributes().get("id").toInt) match {
          case n: NullObject[Double] => {
            replaced = replaced + 1
            replacedAverage = replacedAverage + n.getValue()
          }
          case v: ContentObject[Double] => {
            found = found + 1
            foundAverage = foundAverage + v.getValue()
          }
        }
      }

      //make sure its not NaN
      replacedAverage = (replacedAverage / replaced) match {
        case x if x.isNaN => 0
        case y: Double => y
      }

      //make sure its not NaN
      foundAverage = (foundAverage / found) match {
        case x if x.isNaN => 0
        case y: Double => y
      }

      //make sure its not NaN
      val averageReplacedFoundFactor = (replacedAverage / foundAverage) match {
        case x if x.isNaN => 0
        case y: Double => y
      }

      //set the calculated values for this bin
      datafile.getCell(binColumn, foundBin).asInstanceOf[CalculationObject[Any]].setValue(found)
      datafile.getCell(binColumn, replacedBin).asInstanceOf[CalculationObject[Any]].setValue(replaced)
      datafile.getCell(binColumn, foundBinAverage).asInstanceOf[CalculationObject[Any]].setValue(foundAverage)
      datafile.getCell(binColumn, replacedBinAverage).asInstanceOf[CalculationObject[Any]].setValue(replacedAverage)
      datafile.getCell(binColumn, averageFactor).asInstanceOf[CalculationObject[Any]].setValue(averageReplacedFoundFactor)

      }
      catch {
      	case t:MatchError => t.printStackTrace()
      }
    }

    //returns the generated result
    datafile
  }

}