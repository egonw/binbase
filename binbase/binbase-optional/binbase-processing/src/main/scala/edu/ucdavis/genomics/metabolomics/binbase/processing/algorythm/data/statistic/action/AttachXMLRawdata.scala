package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.action
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.io.source.Source
import org.jdom.output.XMLOutputter
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling
import org.jdom.output.Format
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.action.BasicAction
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.Writer
import java.io.OutputStream
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import java.io.IOException
import edu.ucdavis.genomics.metabolomics.util.io.Copy
import java.io.ByteArrayInputStream
import java.io.InputStream

/**
 * attaches the xml rawdata file to the result file
 */
class AttachXMLRawdata extends AttachXMLFile {

  /**
   * in which folder do we want to store this result
   */
  def getFolder: String = "rawdata/xml"

  /**
   * description of the action
   */
  def getDescription: String = "this action attaches the generated xml file to the result file"

  /**
   * writes the actual sop file out
   */
  def run(configuration: Element, rawdata: Source, sop: Source) = {

    val xml: Element = XmlHandling.readXml(rawdata)

    val outputter: XMLOutputter = new XMLOutputter(Format.getPrettyFormat())

    val result: String = outputter.outputString(xml)

    val in: InputStream = new ByteArrayInputStream(result.getBytes())

    writeXML(in, "rawdata")
  }
}