package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.TransposeDataFile

/**
 * transposes a binbase result datafile, please be aware that the resulting file does not conform to your standard datafile and can create problems
 * if you try to work on it. You should really only use this as last modification
 */
class TransposeDataFileProcess extends DontWriteProcessableResult with TransposeDataFile {

  def getDescription: String = "transposes the datafile and returns an unmodifieable datafile"

  /**
   * transposes the datafile
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    //does the actual transposing
    val result = transpose(datafile)

    //nothing to return since we don't allow further operations with a transposed file
    result
  }
}