package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`._

/**
 * usefull to detect rows with objects
 */
trait ObjectDetector extends DataFilePrint {

  /**
   * returns a tuple containing all rows wiht bin objects
   */
  def detectRowsWithBinObject(dataFile: ResultDataFile): Set[Int] = {
    detectRow(dataFile, { obj =>
      obj match {
        case o: BinObject[Any] => true
        case _ => false
      }
    })
  }

  /**
   * returns a tuple containing all rows wiht bin objects
   */
  def detectRowsWithRefrenceObject(dataFile: ResultDataFile): Set[Int] = {
    detectRow(dataFile, { obj =>
      obj match {
        case o: RefrenceObject[Any] => true
        case _ => false
      }
    })
  }

  /**
   * returns a tuple containing all rows wiht sample objects
   */
  def detectRowsWithSampleObject(dataFile: ResultDataFile): Set[Int] = {
    detectRow(dataFile, { obj =>
      obj match {
        case o: SampleObject[Any] => true
        case _ => false
      }
    })
  }

  /**
   * returns a tuple containing all rows wiht sample objects
   */
  def detectRowsWithMetaObject(dataFile: ResultDataFile): Set[Int] = {
    detectRow(dataFile, { obj =>
      obj match {
        case o: MetaObject[Any] => true
        case _ => false
      }
    })
  }

  /**
   * returns a tuple containing all rows wiht sample objects
   */
  def detectRowsWithCalculationObject(dataFile: ResultDataFile): Set[Int] = {
    detectRow(dataFile, { obj =>
      obj match {
        case o: CalculationObject[Any] => true
        case _ => false
      }
    })
  }

  /**
   * detect all columns which contain spectra data
   */
  def detectColumnsWithSpectra(dataFile: ResultDataFile): Set[Int] = {
    detectColumn(dataFile, { obj =>
      obj match {
        case o: ContentObject[Any] => true
        case _ => false
      }
    })
  }

  /**
   * detect all columns which contain spectra data
   */
  def detectColumnsWithBin(dataFile: ResultDataFile): Set[Int] = {
    detectColumn(dataFile, { obj =>
      obj match {
        case o: BinFormat[Any] => true
        case _ => false
      }
    })
  }

  /**
   * detect all columns which contain spectra data
   */
  def detectColumnsWithSample(dataFile: ResultDataFile): Set[Int] = {
    detectColumn(dataFile, { obj =>
      obj match {
        case o: SampleObject[Any] => true
        case _ => false
      }
    })
  }

  /**
   * detect all columns which contain spectra data
   */
  def detectColumnsWithSetupXData(dataFile: ResultDataFile): Set[Int] = {
    detectColumn(dataFile, { obj =>
      obj match {
        case o: SetupXFormat[Any] => true
        case _ => false
      }
    })
  }
  /**
   * detect all columns which contain calculation data
   */
  def detectColumnsWithCalculationData(dataFile: ResultDataFile): Set[Int] = {
    detectColumn(dataFile, { obj =>
      obj match {
        case o: CalculationObject[Any] => true
        case _ => false
      }
    })
  }

  /**
   * detects all columns for which the match function returns true
   */
  def detectColumn(dataFile: ResultDataFile, matchFormatObject: (Any) => Boolean) = {
    var result: Set[Int] = Set()

    //go over all rows
    dataFile.getData().toList.foreach { rows =>
      var counter: Int = 0

      //go over all columns
      rows.toList.foreach { obj =>
        //match for our required objects
        if (matchFormatObject(obj)) result = result + counter

        //increase the counter with one
        counter = counter + 1
      }

    }

    //return result
    result
  }

  /**
   * detects all rows for which the match function returns true
   */
  def detectRow(dataFile: ResultDataFile, matchFormatObject: (Any) => Boolean) = {

    var result: Set[Int] = Set()

    var counter: Int = 0

    //go over all rows
    dataFile.getData().toList.foreach { rows =>
      rows.toList.foreach { obj =>
        //match for our required objects
        if (matchFormatObject(obj)) result = result + counter
      }
      //increase the counter with one
      counter = counter + 1
    }

    //return result
    result
  }

  /**
   * inserts a row and sets this as not modfiable. It returns the position of this row
   */
  def insertProtectedRow(dataFile: ResultDataFile): Int = {
    val index: Int = detectRowsWithSampleObject(dataFile) reduceRight (_ min _)

    dataFile.addEmptyRowAtPosition(index)

    var list: java.util.List[Any] = dataFile.getRow(index).asInstanceOf[java.util.List[Any]]

    //assign new objects to the list
    for (i <- 0 until list.size()) {
      list.set(i, new CalculationObject[Any](""))
    }
    dataFile.setIgnoreRow(index, true)

    //return the new position
    index
  }

  /**
   * inserts a column into the datafile and returns it its postion. It will be insert between the setupx and bins columns
   */
  def insertProtectedColumn(dataFile: ResultDataFile): Int = {

    //find the last column of type setupx format
    var index: Int = detectColumnsWithSetupXData(dataFile) reduceRight (_ min _)

    /* detectColumnsWithCalculationData(dataFile).foreach { value =>
      if (value > max) max = value
    }
    */

    dataFile.addEmptyColumn("", index)

    var list: java.util.List[Any] = dataFile.getColumn(index).asInstanceOf[java.util.List[Any]]

    //assign new objects to the list
    for (i <- 0 until list.size()) {
      if (i == 0) assert(dataFile.setCell(index, i, new HeaderFormat[Any]("")))
      else assert(dataFile.setCell(index, i, new CalculationObject[Any]("")))
    }
    dataFile.setIgnoreColumn(index, true)

    //return the new position
    index
  }
}