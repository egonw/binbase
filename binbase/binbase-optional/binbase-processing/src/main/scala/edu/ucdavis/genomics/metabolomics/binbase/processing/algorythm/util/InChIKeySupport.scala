package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.util

import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.DontWriteProcessableResult
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.{ObjectDetector, MiniXSupport}
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.{SampleObject, HeaderFormat, CalculationObject, FormatObject}
import edu.ucdavis.genomics.metabolomics.binbase.minix.jmx.MiniXConfigurationJMXFacadeUtil._
import scala.collection.JavaConversions._

/**
 *
 * Adds an additional row containing the InChI Key for this given compound
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 2/11/14
 * Time: 11:41 AM
 */
class InChIKeySupport extends DontWriteProcessableResult with ObjectDetector {

	def getDescription(): String = "integrates InChI Keys into the result set as additional row"

	/**
	 * adds the minix metadata to the result file and returns it for further processing
	 */
	def process(file: ResultDataFile, config: Element): DataFile = {

		//add columns
		val inchiKey: Integer = insertProtectedRow(file)

		file.getCell(0, inchiKey).asInstanceOf[FormatObject[Any]].setValue("InChI Key")

		//go over all bins
		file.getBins(true).toList.foreach {
			bin: HeaderFormat[String] =>

				val binId: Int = Integer.parseInt(bin.getAttributes().get("id"))

				val binPosition: Int = file.getBinPosition(binId)

				if(bin.getAttributes.get("inchi_key") != null && bin.getAttributes.get("inchi_key") != "null"){
					file.getCell(binPosition, inchiKey).asInstanceOf[FormatObject[Any]].setValue(bin.getAttributes.get("inchi_key"))				  
				}
				else{
					file.getCell(binPosition, inchiKey).asInstanceOf[FormatObject[Any]].setValue("")				  
				}
		}

		file
	}

}