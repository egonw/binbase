package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool;
import scala.collection.JavaConversions._
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable
import edu.ucdavis.genomics.metabolomics.util.statistics.data.{ SimpleDatafile, DataFile }
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.SampleObject
import java.util.Vector
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.NetCDFResolver

/**
 * used to generate a report containing all the netcdf files
 */
class NetCdfFileReport extends DontWriteProcessableResult with NetCDFResolver {

  /**
   * returns the folder where the netcdf report is stored
   */
  override def getFolder(): String = {
    "report"
  }

  def getDescription() : String = "generates a report if the netcdf files exist"
    
  /**
   * generates a report if each netdcf files exists
   */
  def process(file: ResultDataFile, config: Element): DataFile = {
    val samples: List[SampleObject[String]] = file.getSamples().toList

    val result: DataFile = new SimpleDatafile()
    result.addEmptyColumn("sample")
    result.addEmptyColumn("exist")

    samples.foreach { arg =>

      val list: Vector[String] = new Vector();
      list.add(arg.getValue())
      list.add(hasNetcdf(arg.getValue()).toString())
      
      result.addRow(list)
    }

    
    null
  }

}