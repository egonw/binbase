package edu.ucdavis.genomics.metabolomics.binbase.minix.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;

import org.apache.log4j.Logger;
import org.jboss.mx.util.MBeanServerLocator;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.MetaProviderJMXMBean;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXProvider;
import edu.ucdavis.genomics.metabolomics.binbase.minix.wsdl.MiniXProvider;
import edu.ucdavis.genomics.metabolomics.binbase.util.net.DetermineIpForInterface;
import edu.ucdavis.genomics.metabolomics.binbase.util.net.InterfaceNotFoundException;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * * @jmx.mbean name="binbase.miniX:service=MiniXConfigurationJMX" description =
 * "configure the minix system" extends = "javax.management.MBeanRegistration"
 * 
 * 
 * @author wohlgemuth
 * 
 */
public class MiniXConfigurationJMX implements MiniXConfigurationJMXMBean,
		SetupXProvider {

	private Logger logger = Logger.getLogger(getClass());

	private String url;

	private boolean automaticURLDetection;

	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * @return
	 */
	public boolean isAutomaticURLDetection() {
		return automaticURLDetection;
	}

	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * @return
	 */
	public void setAutomaticURLDetection(boolean automaticURLDetection) {
		this.automaticURLDetection = automaticURLDetection;

		if (this.automaticURLDetection) {
			try {
				this.determineIp();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}

	}

	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * 
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;

		this.unregister();
		this.register();

		this.save();
	}

	/**
	 * @author wohlgemuth
	 * @version Mar 30, 2006
	 */
	private void register() {
		try {
			// register service
			ObjectName name = new ObjectName("binbase:service=MetaProvider");
			MBeanServer server = MBeanServerLocator.locate();

			MetaProviderJMXMBean bean = (MetaProviderJMXMBean) MBeanServerInvocationHandler
					.newProxyInstance(server, name, MetaProviderJMXMBean.class,
							false);

			if (bean.getProvider().contains(MiniXProvider.class.getName()) == false) {
				bean.addProvider(MiniXProvider.class.getName());
			} else {
				logger.warn("service already registered!");
			}
		} catch (Exception e) {
			logger.error("could'nt register the service", e);
		}

	}

	/**
	 * determines the ip address, assuming that the application server runs on
	 * the cluster frontend89
	 * 
	 * @throws IOException
	 */
	private void determineIp() throws IOException {
		logger.info("trying to automatically configure the ip for the database server...");

		// check for eth2
		try {
			// public vmware cluster interface
			String ip = DetermineIpForInterface.getIP("eth2");
			this.setUrl("http://" + ip
					+ ":8080/minix/services/communications?wsdl");
			return;
		} catch (InterfaceNotFoundException e) {
			logger.info(e.getMessage());
		}

		// check for eth1
		try {
			// private vmware cluster interface
			String ip = DetermineIpForInterface.getIP("eth1");
			this.setUrl("http://" + ip
					+ ":8080/minix/services/communications?wsdl");
			return;
		} catch (InterfaceNotFoundException e) {
			logger.info(e.getMessage());
		}

		// check for eth0
		try {
			// public cluster interface
			String ip = DetermineIpForInterface.getIP("eth0");
			this.setUrl("http://" + ip
					+ ":8080/minix/services/communications?wsdl");
			return;
		} catch (InterfaceNotFoundException e) {
			logger.info(e.getMessage());
		}

	}

	public void postDeregister() {
		unregister();
	}

	public void preDeregister() throws Exception {
	}

	/**
	 * @author wohlgemuth
	 * @version Mar 30, 2006
	 */
	private void unregister() {
		try {
			// register service
			ObjectName name = new ObjectName("binbase:service=MetaProvider");
			MBeanServer server = MBeanServerLocator.locate();

			MetaProviderJMXMBean bean = (MetaProviderJMXMBean) MBeanServerInvocationHandler
					.newProxyInstance(server, name, MetaProviderJMXMBean.class,
							false);

			if (bean.getProvider().contains(MiniXProvider.class.getName()) == true) {
				bean.removeProvider(MiniXProvider.class.getName());
			} else {
				logger.warn("service not registered!");
			}
		} catch (Exception e) {
			logger.error("could'nt unregister the service", e);
		}
	}

	private void load() {

		Properties p = new Properties();

		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			FileInputStream in = new FileInputStream(file);
			p.load(in);

			if (p.getProperty("url") != null) {
				this.setUrl(p.getProperty("url"));
			}
			if (p.getProperty("automatic") != null) {
				this.setAutomaticURLDetection(Boolean.parseBoolean(p
						.getProperty("automatic")));
			}

		} catch (InvalidPropertiesFormatException e) {
			logger.error("postRegister(Boolean)", e); //$NON-NLS-1$
		} catch (FileNotFoundException e) {
			logger.error("postRegister(Boolean)", e); //$NON-NLS-1$
		} catch (IOException e) {
			logger.error("postRegister(Boolean)", e); //$NON-NLS-1$

		}

	}

	private void save() {

		try {
			File file = new File(getClass().getName() + ".properties");
			Properties p = new Properties();

			p.setProperty("url", this.url);
			p.setProperty("automatic",
					new Boolean(this.isAutomaticURLDetection()).toString());

			FileOutputStream out = new FileOutputStream(file);
			p.store(out, "-- no comment --");
			out.close();

		} catch (InvalidPropertiesFormatException e) {
			logger.error("postRegister(Boolean)", e); //$NON-NLS-1$
		} catch (FileNotFoundException e) {
			logger.error("postRegister(Boolean)", e); //$NON-NLS-1$
		} catch (IOException e) {
			logger.error("postRegister(Boolean)", e); //$NON-NLS-1$
		}
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1)
			throws Exception {
		load();
		return arg1;
	}

	/**
	 * add our service to the mainservice as implmentation
	 * 
	 * @author wohlgemuth
	 * @version Feb 9, 2006
	 * @see javax.management.MBeanRegistration#postRegister(java.lang.Boolean)
	 */
	public void postRegister(Boolean arg0) {
	}

	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * @return
	 */
	public String getMetaInformationByClass(String setupXId)
			throws BinBaseException {
		return new MiniXProvider().getMetaInformationByClass(setupXId);
	}

	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * @return
	 */
	public String getMetaInformationByExperiment(String setupXId)
			throws BinBaseException {
		return new MiniXProvider().getMetaInformationByExperiment(setupXId);
	}

	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * @return
	 */
	public String getMetaInformationBySample(String setupXId)
			throws BinBaseException {
		return new MiniXProvider().getMetaInformationBySample(setupXId);
	}

	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * @return
	 */
	public String getSetupXId(String sampleName) throws BinBaseException {
		return new MiniXProvider().getSetupXId(sampleName);
	}

	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * @return
	 */
	public void upload(String experimentId, String content)
			throws BinBaseException {
		logger.info("using minix provider to upload: " + experimentId);
		new MiniXProvider().upload(experimentId, content);
	}
	/**
	 * @jmx.managed-operation description = "url of the webservice"
	 * @return
	 */
	public boolean canCreateBins(String setupxId) throws BinBaseException {
		return new MiniXProvider().canCreateBins(setupxId);
	}
}
