package edu.ucdavis.genomics.metabolomics.binbase.collections;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.collection.factory.MapFactory;

public class JDBM3MapFactoryImplTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * ensures that the creates maps do not point to the same and the factories
	 * are different
	 */
	@Test
	public void testCreateMap() {
		MapFactory<String, String> factoryA = MapFactory
				.newInstance(JDBM3MapFactoryImpl.class.getName());
		MapFactory<String, String> factoryB = MapFactory
				.newInstance(JDBM3MapFactoryImpl.class.getName());

		assertTrue(factoryA.equals(factoryB) == false);

		Map<String, String> a = factoryA.createMap();
		Map<String, String> b = factoryB.createMap();

		a.put("a", "test-1");
		b.put("b", "test-1");


		assertTrue(a.containsKey("a"));
		assertTrue(a.containsKey("b") == false);

		assertTrue(b.containsKey("b"));
		assertTrue(b.containsKey("a") == false);

	}

	/**
	 * test the mutlithreaded generation and access to the map
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testTestMultiThreadedCreate() throws InterruptedException {

		Runnable run = new Runnable() {

			@Override
			public void run() {
				MapFactory<Integer, String> a = MapFactory
						.newInstance(JDBM3MapFactoryImpl.class
								.getName());
				Map<Integer, String> b = a.createMap();

				for (int i = 0; i < 1000; i++) {
					b.put(i, "a_" + i);
				}

			}
		};

		ExecutorService exec = Executors.newCachedThreadPool();

		for (int i = 0; i < 100; i++) {
			exec.submit(run);
		}

		exec.shutdown();

		exec.awaitTermination(50000, TimeUnit.DAYS);
	}

	/**
	 * we are generating roughly 100GB worth of data and store them in the map
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testSingleThreadedOutOfMemoryExcpetionsAvoidance()
			throws InterruptedException {
		MapFactory<Integer, String> a = MapFactory
				.newInstance(JDBM3MapFactoryImpl.class.getName());

		/**
		 * generates 1GB of data in a single threaded mode
		 */
		for (int i = 0; i < 10; i++) {

			Map<Integer, String> b = a.createMap();

			/**
			 * creates 100MB worth of data and stores it in the map
			 */
			for (int x = 0; x < 10; x++) {
				StringBuffer buffer = new StringBuffer();

				/**
				 * generates a 10mb big object
				 */
				for (int z = 0; z < 1024 * 1024 * 10 / 2; z++) {
					buffer.append('a');
				}

				b.put(i, buffer.toString());
			}

		}

	}

	/**
	 * we are generating roughly 100GB worth of data and store them in the map
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testMultiThreadedOutOfMemoryExcpetionsAvoidance()
			throws InterruptedException {

		Runnable run = new Runnable() {
			/**
			 * generates 10 X 10MB objects, so takes up 100MB in memory
			 */
			@Override
			public void run() {
				try {
					MapFactory<Integer, String> a = MapFactory
							.newInstance(JDBM3MapFactoryImpl.class
									.getName());
					Map<Integer, String> b = a.createMap();

					/**
					 * 100MB each
					 */
					for (int i = 0; i < 10; i++) {

						StringBuffer buffer = new StringBuffer();

						/**
						 * generates a 10mb big object
						 */
						for (int x = 0; x < 1024 * 1024 * 10 / 2; x++) {
							buffer.append('a');
						}
						b.put(i, buffer.toString());
					}
				} catch (OutOfMemoryError e) {
					fail(e.getMessage());
				}

			}
		};

		ExecutorService exec = Executors.newCachedThreadPool();

		/**
		 * generates 10 threads which each generate 100MB of data
		 */
		for (int i = 0; i < 10; i++) {
			exec.submit(run);
		}

		exec.shutdown();

		exec.awaitTermination(50000, TimeUnit.DAYS);
	}

	@Test
	public void testMillionsOfEntries() throws InterruptedException {

		/**
		 * generates one million objects in the hashmap
		 */
		Runnable run = new Runnable() {
			/**
			 * generates 100000 X 1MB
			 */
			@Override
			public void run() {
				MapFactory<Integer, String> a = MapFactory
						.newInstance(JDBM3MapFactoryImpl.class
								.getName());
				Map<Integer, String> b = a.createMap();

				for (int i = 0; i < 100000; i++) {

					StringBuffer buffer = new StringBuffer();

					/**
					 * generates a 1mb big object
					 */
					for (int x = 0; x < 1024 * 1024 / 2; x++) {
						buffer.append('a');
					}
					b.put(i, buffer.toString());
				}

			}
		};

		ExecutorService exec = Executors.newCachedThreadPool();

		/**
		 * generates 1 million objects
		 */
		for (int i = 0; i < 10; i++) {
			exec.submit(run);
		}

		exec.shutdown();

		exec.awaitTermination(50000, TimeUnit.DAYS);
	}


}
