package edu.ucdavis.genomics.metabolomics.binbase.collections;

import java.io.File;
import java.io.IOException;

import junit.framework.TestCase;

import com.sleepycat.je.DatabaseException;

public class BerkleyDBIndependentMapTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * is a test to ensure that the different maps, which should be all stored at one location do not have 
	 * the same values or share references
	 * @throws IllegalArgumentException
	 * @throws DatabaseException
	 * @throws IOException
	 */
	public void testMultiplePutDatabase() throws IllegalArgumentException, DatabaseException, IOException{
		

        File data = File.createTempFile("independent_map", "test");
        File dir = new File(data.getAbsolutePath() + "1/");
        dir.mkdirs();
        data.deleteOnExit();
        dir.deleteOnExit();
		
        BerkleyDBIndependentMap<String, Integer> a = new BerkleyDBIndependentMap<String, Integer>(dir);
        BerkleyDBIndependentMap<String, Integer> b = new BerkleyDBIndependentMap<String, Integer>(dir);
        BerkleyDBIndependentMap<String, Integer> c = new BerkleyDBIndependentMap<String, Integer>(dir);
        BerkleyDBIndependentMap<String, Integer> d = new BerkleyDBIndependentMap<String, Integer>(dir);
		
        
        String test = "a simple test";
        
        assertTrue(a.containsKey(test) == false);
        assertTrue(b.containsKey(test) == false);
        assertTrue(c.containsKey(test) == false);
        assertTrue(d.containsKey(test) == false);
        
        a.put(test, 1);
		
        assertTrue(a.containsKey(test) == true);
        assertTrue(b.containsKey(test) == false);
        assertTrue(c.containsKey(test) == false);
        assertTrue(d.containsKey(test) == false);
        
        b.put(test, 2);
        
        assertTrue(a.containsKey(test) == true);
        assertTrue(b.containsKey(test) == true);
        assertTrue(c.containsKey(test) == false);
        assertTrue(d.containsKey(test) == false);
        
        assertTrue(a.get(test) == 1);
        assertTrue(b.get(test) == 2);
 
        a.put(test,5);
        assertTrue(a.get(test) == 5);
        assertTrue(b.get(test) == 2);
        
        System.out.println(a.getInternalKey());
        System.out.println(b.getInternalKey());
        
        assertTrue(a.getInternalKey() != b.getInternalKey());
        
	}
}
