package edu.ucdavis.genomics.metabolomics.binbase.collections;

import java.io.File;

import junit.framework.TestCase;

public class BerkleyDBMapTest extends TestCase {

	BerkleyDBMap<String, Object> map;
	BerkleyDBMap<Long, Long> memoryTestMap;
	
	
	protected void setUp() throws Exception {
		super.setUp();


        File data = File.createTempFile("map", "test");
        File dir = new File(data.getAbsolutePath() + "1/");
        dir.mkdirs();
        data.deleteOnExit();
        dir.deleteOnExit();
        
        this.map = new BerkleyDBMap<String, Object>(dir);
        this.memoryTestMap = new BerkleyDBMap<Long, Long>(dir);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testBasicPutAndGet() {

		String a = "a test";
		String b = "anoter test";

		assertFalse(map.containsKey(a));
		assertFalse(map.containsKey(b));

		map.put(a, "a test object");
		map.put(b, "the second object");
		
		assertTrue(map.containsKey(a));
		assertTrue(map.containsKey(b));
		
		System.out.println(map.get(a));
		System.out.println(map.get(b));
		
	}
	
	/**
	 * jvm should not run out of memory
	 */
	public void testMemoryConsumption(){
		for(long i = 0; i < 100000L; i++){
			this.memoryTestMap.put(i, i);
		}	
	}

}
