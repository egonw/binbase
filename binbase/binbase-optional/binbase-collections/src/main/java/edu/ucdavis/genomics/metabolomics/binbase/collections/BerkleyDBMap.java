package edu.ucdavis.genomics.metabolomics.binbase.collections;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredIterator;
import com.sleepycat.collections.StoredMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

/**
 * map which utilizes the berkleyDB database system in this implementation all
 * maps share one hashmap at the given database directory
 * 
 * @author wohlgemuth
 * 
 */
public class BerkleyDBMap<K, V> implements Map<K, V> {

	StoredMap<BerkleyKey<K>, BerkleyValue<V>> internalMap = null;

	private Logger logger = Logger.getLogger(getClass());

	public void clear() {
		internalMap.clear();
	}

	@SuppressWarnings("unchecked")
	public boolean containsKey(Object key) {
		BerkleyKey<K> internalKey = new BerkleyKey<K>((K) key);
		return internalMap.containsKey(internalKey);
	}

	@SuppressWarnings("unchecked")
	public boolean containsValue(Object value) {
		BerkleyValue<V> internalValue = new BerkleyValue<V>((V) value);
		return internalMap.containsValue(internalValue);
	}

	public Set<java.util.Map.Entry<K, V>> entrySet() {
		Set<Map.Entry<BerkleyKey<K>, BerkleyValue<V>>> set = internalMap
				.entrySet();

		Set<java.util.Map.Entry<K, V>> result = new HashSet<Entry<K, V>>();

		Iterator<Entry<BerkleyKey<K>, BerkleyValue<V>>> it = set.iterator();

		while (it.hasNext()) {
			final Entry<BerkleyKey<K>, BerkleyValue<V>> current = it.next();

			Map.Entry<K, V> entry = new Map.Entry<K, V>() {

				@Override
				public K getKey() {
					return current.getKey().getKey();
				}

				@Override
				public V getValue() {
					return current.getValue().getValue();
				}

				@Override
				public V setValue(V value) {
					current.getValue().setValue(value);
					return current.getValue().getValue();
				}
			};
			result.add(entry);
		}
		StoredIterator.close(it);

		return result;
	}

	public boolean equals(Object other) {
		return internalMap.equals(other);
	}

	@SuppressWarnings("unchecked")
	public V get(Object key) {
		BerkleyKey<K> internalKey = new BerkleyKey<K>((K) key);
		return internalMap.get(internalKey).getValue();
	}

	public boolean isEmpty() {
		return internalMap.isEmpty();
	}

	public Set<K> keySet() {
		Set<K> keySet = new HashSet<K>();

		Iterator<BerkleyKey<K>> it = internalMap.keySet().iterator();

		while (it.hasNext()) {
			keySet.add(it.next().getKey());
		}

		StoredIterator.close(it);

		return keySet;
	}

	public V put(K key, V value) {

		BerkleyKey<K> keyBinding = new BerkleyKey<K>(key);
		BerkleyValue<V> valueBinding = new BerkleyValue<V>(value);

		// remove value
		internalMap.remove(keyBinding);
		// add value
		internalMap.put(keyBinding, valueBinding);

		return null;
	}

	public void putAll(Map<? extends K, ? extends V> arg0) {
		// TODO internalMap.putAll(arg0);
	}

	public boolean remove(Object arg0, Object arg1) {
		return internalMap.remove(arg0, arg1);
	}

	@SuppressWarnings("unchecked")
	public V remove(Object key) {
		BerkleyKey<K> toRemove = new BerkleyKey<K>((K) key);
		return internalMap.remove(toRemove).getValue();
	}

	public int size() {
		return internalMap.size();
	}

	public String toString() {
		return internalMap.entrySet().toString();
	}

	public Collection<V> values() {
		return null;// internalMap.values();

	}

	public BerkleyDBMap(final File dataStorage)
			throws IllegalArgumentException, DatabaseException {
		this(dataStorage, false);
	}

	/**
	 * creates a map for the given database
	 * 
	 * @param database
	 * @throws DatabaseException
	 */
	public BerkleyDBMap(Database database) throws DatabaseException {
		createCatalog(database);
	}

	/**
	 * creates a map database at the given directory in the file system
	 * 
	 * @param deleteOnExit
	 *            if true, it will try to delete the datadir on exist, but there
	 *            are no promise for this
	 * @param dataStorage
	 * @throws IllegalArgumentException
	 * @throws DatabaseException
	 */
	public BerkleyDBMap(final File dataStorage, final boolean deleteOnExit)
			throws IllegalArgumentException, DatabaseException {

		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setTransactional(false);
		envConfig.setAllowCreate(true);
		envConfig.setCachePercent(10);

		Environment env = new Environment(dataStorage, envConfig);

		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setTransactional(false);
		dbConfig.setAllowCreate(true);

		final Database database = env.openDatabase(null, "binbase_map_catalog",
				dbConfig);

		final StoredClassCatalog catalog = createCatalog(database);

		Runtime.getRuntime().addShutdownHook(new Thread() {

			@Override
			public void run() {
				try {
					catalog.close();
					if (deleteOnExit) {
						BerkleyDBIndependentMapFactoryImpl.deleteFile(dataStorage);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});

	}

	/**
	 * initialized the actual map and builds the catalog
	 * 
	 * @param database
	 * @return
	 * @throws DatabaseException
	 */

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private StoredClassCatalog createCatalog(final Database database)
			throws DatabaseException {
		final StoredClassCatalog catalog = new StoredClassCatalog(database);

		EntryBinding<K> keyBinding = new SerialBinding<K>(catalog,
				(Class<K>) BerkleyKey.class);
		EntryBinding<V> valueEntityBinding = new SerialBinding<V>(catalog,
				(Class<V>) BerkleyValue.class);

		this.internalMap = new StoredMap(database, keyBinding,
				valueEntityBinding, true);

		return catalog;
	}

}
