package edu.ucdavis.genomics.metabolomics.binbase.collections;

import java.io.File;
import java.util.Map;

import net.kotek.jdbm.DB;
import net.kotek.jdbm.DBMaker;
import edu.ucdavis.genomics.metabolomics.util.collection.factory.MapFactory;

/**
 * factory to create JDBM3 map. Which is very good at moving stuff to hd and in smaller size very fast
 * but seriously slow with very large maps containing several dozen gigabytes of data 
 * @author wohlgemuth
 * 
 * @param <K>
 * @param <V>
 */
public class JDBM3MapFactoryImpl<K, V> extends MapFactory<K, V> {

	/**
	 * Initialized one temperaery directory at the first access to the factory
	 */
	private static File temporaryDirectory = BerkleyDBIndependentMapFactoryImpl
			.generateDirectory();

	private static DB db = createDatabase();

	private static Long INSTANCE_ID = new Long(0);

	protected static DB createDatabase() {
		DBMaker maker = DBMaker.openFile(temporaryDirectory.getAbsolutePath());
		maker.disableCache();
		maker.useRandomAccessFile();
		maker.disableTransactions();

		final DB db = maker.make();
		Runtime.getRuntime().addShutdownHook(new Thread() {

			@Override
			public void run() {
				try {
					//db.close();
					File parentDir = temporaryDirectory.getParentFile();
					
					for(File f :parentDir.listFiles()){
						
						//this map creates directories with a .[a-z].[0-9] added
						if(f.getName().contains(temporaryDirectory.getName()+".")){
							BerkleyDBIndependentMapFactoryImpl.deleteFile(f);
							
						}
					}
					
					BerkleyDBIndependentMapFactoryImpl
							.deleteFile(temporaryDirectory);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
		return db;
	}

	@Override
	public Map<K, V> createMap() {
		Long instanceId = new Long(0);
		synchronized (INSTANCE_ID) {
			INSTANCE_ID = INSTANCE_ID + 1;
			instanceId = INSTANCE_ID;
		}
		return new AutoDeleteMap<K, V>((Map<K, V>) db.createHashMap("id_" + instanceId));
	}

}
