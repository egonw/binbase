package edu.ucdavis.genomics.metabolomics.binbase.collections;

import java.io.Serializable;

public class BerkleyValue<V> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private V value;
	
	protected void setValue(V value) {
		this.value = value;
	}

	protected V getValue() {
		return value;
	}

	@Override
	public boolean equals(Object obj) {
		return value.equals(obj);
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public String toString() {
		return value.toString();
	}

	public BerkleyValue(V value){
		this.value = value;
	}
}
