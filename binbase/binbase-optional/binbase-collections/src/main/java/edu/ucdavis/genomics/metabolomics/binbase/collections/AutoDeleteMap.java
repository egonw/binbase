package edu.ucdavis.genomics.metabolomics.binbase.collections;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * explicitely clears the map at finalize
 * @author wohlgemuth
 *
 * @param <K>
 * @param <V>
 */
public class AutoDeleteMap<K,V> implements Map<K,V> {

	private Map<K,V> delegate;

	public AutoDeleteMap(Map<K,V> map){
		this.delegate = map;
	}
	
	@Override
	protected void finalize() throws Throwable {
		this.clear();
		super.finalize();
	}

	public void clear() {
		delegate.clear();
	}

	public boolean containsKey(Object arg0) {
		return delegate.containsKey(arg0);
	}

	public boolean containsValue(Object arg0) {
		return delegate.containsValue(arg0);
	}

	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return delegate.entrySet();
	}

	public boolean equals(Object arg0) {
		return delegate.equals(arg0);
	}

	public V get(Object arg0) {
		return delegate.get(arg0);
	}

	public int hashCode() {
		return delegate.hashCode();
	}

	public boolean isEmpty() {
		return delegate.isEmpty();
	}

	public Set<K> keySet() {
		return delegate.keySet();
	}

	public V put(K arg0, V arg1) {
		return delegate.put(arg0, arg1);
	}

	public void putAll(Map<? extends K, ? extends V> arg0) {
		delegate.putAll(arg0);
	}

	public V remove(Object arg0) {
		return delegate.remove(arg0);
	}

	public int size() {
		return delegate.size();
	}

	public Collection<V> values() {
		return delegate.values();
	}
}
