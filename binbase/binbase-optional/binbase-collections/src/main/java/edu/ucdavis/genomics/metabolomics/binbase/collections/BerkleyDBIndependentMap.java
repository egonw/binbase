package edu.ucdavis.genomics.metabolomics.binbase.collections;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseException;

import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException;

/**
 * uses one filebacked hashmap, but generates internal maps stored in the actual
 * map for datastorage so we should have no collissions
 * 
 * @author wohlgemuth
 * 
 */
public class BerkleyDBIndependentMap<K, V> implements Map<K, V> {

	Logger logger = Logger.getLogger(getClass());

	Map<Long, Map<K, V>> internalInstance;

	private Long internalKey;

	public void clear() {
		this.internalInstance.clear();
	}

	public boolean containsKey(Object key) {
		return getActualMap().containsKey(key);
	}

	public boolean containsValue(Object value) {
		return getActualMap().containsValue(value);
	}

	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return getActualMap().entrySet();
	}

	public boolean equals(Object o) {
		return getActualMap().equals(o);
	}

	public V get(Object key) {
		return getActualMap().get(key);
	}

	public int hashCode() {
		return getActualMap().hashCode();
	}

	public boolean isEmpty() {
		return getActualMap().isEmpty();
	}

	public Set<K> keySet() {
		return getActualMap().keySet();
	}

	public V put(K key, V value) {

		Map<K, V> actualMap = getActualMap();
		V v = actualMap.put(key, value);
		updateInternalMap(actualMap);

		return v;
	}

	public void putAll(Map<? extends K, ? extends V> m) {
		throw new NotSupportedException();
	}

	public V remove(Object key) {
		Map<K, V> actualMap = getActualMap();
		V value = actualMap.remove(key);
		updateInternalMap(actualMap);
		return value;
	}

	public int size() {
		return getActualMap().size();
	}

	public Collection<V> values() {
		return getActualMap().values();
	}

	protected void updateInternalMap(Map<K, V> map) {
		internalInstance.put(getInternalKey(), map);
	}

	static Long internalInstanceKey = new Long(0);

	public Long getInternalKey() {
		if (internalKey == null) {
			throw new RuntimeException(
					"for some reason the internal key was null, this should never be, check your default constructors please");
		}
		return internalKey;
	}

	protected Map<Long, Map<K, V>> getInternalInstance() {
		return this.internalInstance;
	}

	private BerkleyDBIndependentMap() {
		synchronized (internalInstanceKey) {
			internalInstanceKey = internalInstanceKey + 1;
			internalKey = internalInstanceKey;
		}
	}

	/**
	 * generates our shared map instance based on the following directory.
	 * 
	 * @param file
	 * @throws IllegalArgumentException
	 * @throws DatabaseException
	 */
	public BerkleyDBIndependentMap(File file, boolean deleteOnExit)
			throws IllegalArgumentException, DatabaseException {
		this();
		internalInstance = new BerkleyDBMap<Long, Map<K, V>>(file, deleteOnExit);
		internalInstance.put(getInternalKey(), new HashMap<K, V>());
	}

	public Map<K, V> getActualMap() {
		return internalInstance.get(getInternalKey());
	}

	@Override
	protected void finalize() throws Throwable {
		logger.debug("deleting database content during cleanup for instance: "
				+ getInternalKey());
		this.internalInstance.remove(getInternalKey());
	}

	public BerkleyDBIndependentMap(File file) throws IllegalArgumentException,
			DatabaseException {
		this(file, false);
	}

	/**
	 * uses an existing database for the operation
	 * 
	 * @param database
	 * @throws DatabaseException
	 */
	public BerkleyDBIndependentMap(Database database) throws DatabaseException {
		this();
		internalInstance = new BerkleyDBMap<Long, Map<K, V>>(database);
		internalInstance.put(getInternalKey(), new HashMap<K, V>());
	}

}
