DROP VIEW bin_samples;
DROP VIEW experiment_class;
DROP VIEW spectra_found;
DROP VIEW spectra_not_found;

ALTER TABLE "spectra" ALTER COLUMN "sample_id" TYPE int8;
ALTER TABLE "spectra" ALTER COLUMN "spectra_id" TYPE int8;
ALTER TABLE "spectra" ALTER COLUMN "bin_id" TYPE int8;
ALTER TABLE "samples" ALTER COLUMN "sample_id" TYPE int8;
ALTER TABLE "result_link" ALTER COLUMN "result_id" TYPE int8;
ALTER TABLE "result_link" ALTER COLUMN "sample_id" TYPE int8;
ALTER TABLE "result_link" ALTER COLUMN "id" TYPE int8;
ALTER TABLE "result" ALTER COLUMN "result_id" TYPE int8;
CREATE VIEW bin_samples
AS
SELECT samples.sample_id, samples.configuration_id, samples.correction_failed, samples.created_bin, samples.class, samples.sample_name, samples."allowNewBin" AS allownewbin, samples.priority, samples.saturated, samples.setupx_id, samples.status, samples.version, samples.finished, samples.visible, samples.date, samples.sod, samples.type FROM samples WHERE (samples.sample_id IN (SELECT bin.sample_id FROM bin));
;
CREATE VIEW experiment_class
AS
SELECT samples.class FROM samples GROUP BY samples.class;
;
CREATE VIEW spectra_found
AS
SELECT spectra.uniquemass, spectra.signal_noise, spectra.purity, spectra.retention_index, spectra.retention_time, spectra.sample_id, spectra.spectra_id, spectra.apex, spectra.spectra, spectra.apex_sn, spectra.bin_id, spectra.match, spectra.new_bin, spectra.found_at_correction, spectra.leco FROM spectra WHERE (spectra.bin_id IS NOT NULL);
;
CREATE VIEW spectra_not_found
AS
SELECT spectra.uniquemass, spectra.signal_noise, spectra.purity, spectra.retention_index, spectra.retention_time, spectra.sample_id, spectra.spectra_id, spectra.apex, spectra.spectra, spectra.apex_sn FROM spectra WHERE (spectra.bin_id IS NULL);
;
CREATE INDEX "spectra_bin_id_not_null"
	ON "spectra"("bin_id") where bin_id is not null;

ALTER TABLE "result_link" ALTER COLUMN "sample_id" TYPE int8;

