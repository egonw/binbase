	--
-- PostgreSQL binbase schema version 5.0.1.sql
--

SET statement_timeout = 0;
SET client_encoding = 'UNICODE';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: binbase; Type: SCHEMA; Schema: -; Owner: binbase
--

drop schema binbase cascade;

drop extension if exists "plpgsql" cascade;

drop language if exists "plpgsql" cascade;

create language "plpgsql";

CREATE SCHEMA binbase;


ALTER SCHEMA binbase OWNER TO binbase;

SET search_path = binbase, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bin; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE bin (
    bin_id int8 NOT NULL,
    apex_sn double precision,
    apex text,
    export character varying(255) DEFAULT 'FALSE'::character varying,
    generatequantmass character varying(255) DEFAULT 'TRUE'::character varying,
    spectra text,
    minus int8 DEFAULT 2000,
    name character varying(255),
    "new" character varying(255) DEFAULT 'TRUE'::character varying,
    plus int8 DEFAULT 2000,
    purity double precision,
    quantmass int8,
    retention_index int8,
    sample_id int8,
    spectra_id int8,
    uniquemass int8,
    signal_noise double precision,
    molare_mass double precision,
    group_id double precision,
    quality character varying(6) DEFAULT 'FALSE'::character varying
);


ALTER TABLE binbase.bin OWNER TO binbase;

--
-- Name: bin_compare; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE bin_compare (
    id double precision NOT NULL,
    bin_id double precision NOT NULL,
    compare double precision
);


ALTER TABLE binbase.bin_compare OWNER TO binbase;

--
-- Name: bin_group; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE bin_group (
    group_id double precision,
    name character varying(255),
    description character varying(1000)
);


ALTER TABLE binbase.bin_group OWNER TO binbase;

--
-- Name: bin_id; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE bin_id
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    NO MINVALUE
    CACHE 20;


ALTER TABLE binbase.bin_id OWNER TO binbase;

--
-- Name: bin_id; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('bin_id', 1, false);


--
-- Name: bin_ratio; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE bin_ratio (
    id double precision,
    bin_id double precision,
    main_ion double precision,
    secondaery_ion double precision,
    ratio double precision,
    min_ratio double precision,
    max_ratio double precision
);


ALTER TABLE binbase.bin_ratio OWNER TO binbase;

--
-- Name: bin_references; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE bin_references (
    id int8 NOT NULL,
    bin_id int8,
    refrence_class int8,
    refrence_id character varying(255)
);


ALTER TABLE binbase.bin_references OWNER TO binbase;

--
-- Name: samples; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE samples (
    sample_id int8 NOT NULL,
    configuration_id int8,
    correction_failed character varying(255) DEFAULT 'FALSE'::character varying,
    created_bin character varying(255) DEFAULT 'FALSE'::character varying,
    class character varying(255),
    sample_name character varying(255),
    "allowNewBin" character varying(255) DEFAULT 'FALSE'::character varying,
    priority int8 DEFAULT 3,
    saturated character varying(255) DEFAULT 'FALSE'::character varying,
    setupx_id character varying(255) DEFAULT 'not assigned'::character varying,
    status int8 DEFAULT 0,
    version int8 NOT NULL,
    finished character varying(5) DEFAULT 'FALSE'::character varying,
    visible character varying(5) DEFAULT 'TRUE'::character varying,
    date timestamp without time zone,
    sod double precision DEFAULT 1,
    type double precision,
    operator character varying(25),
    machine character varying(25),
    run_id double precision,
    date_of_import timestamp without time zone,
    msmethod character varying(255),
    dpmethod character varying(255),
    tray double precision,
    qcmethod character varying(255),
    asmethod character varying(255),
    gcmethod character varying(255),
    "correctedWith" int8
);


ALTER TABLE binbase.samples OWNER TO binbase;

--
-- Name: bin_samples; Type: VIEW; Schema: binbase; Owner: binbase
--

CREATE VIEW bin_samples AS
    SELECT samples.sample_id, samples.configuration_id, samples.correction_failed, samples.created_bin, samples.class, samples.sample_name, samples."allowNewBin" AS allownewbin, samples.priority, samples.saturated, samples.setupx_id, samples.status, samples.version, samples.finished, samples.visible, samples.date, samples.sod, samples.type FROM samples WHERE (samples.sample_id IN (SELECT bin.sample_id FROM bin));


ALTER TABLE binbase.bin_samples OWNER TO binbase;

--
-- Name: classification; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE classification (
    id int8 NOT NULL,
    structure int8,
    class int8
);


ALTER TABLE binbase.classification OWNER TO binbase;

--
-- Name: comment_id; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE comment_id
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    NO MINVALUE
    CACHE 20;


ALTER TABLE binbase.comment_id OWNER TO binbase;

--
-- Name: comment_id; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('comment_id', 1, false);


--
-- Name: comments; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE comments (
    id bigint NOT NULL,
    text text,
    discriminator bigint,
    type double precision
);


ALTER TABLE binbase.comments OWNER TO binbase;

--
-- Name: configuration; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE configuration (
    configuration_id int8 NOT NULL,
    data text,
    version int8
);


ALTER TABLE binbase.configuration OWNER TO binbase;

--
-- Name: correction_data; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE correction_data (
    sample_id int8,
    bin_id int8,
    x double precision,
    y double precision,
    "position" int8
);


ALTER TABLE binbase.correction_data OWNER TO binbase;

--
-- Name: experiment_class; Type: VIEW; Schema: binbase; Owner: binbase
--

CREATE VIEW experiment_class AS
    SELECT samples.class FROM samples GROUP BY samples.class;


ALTER TABLE binbase.experiment_class OWNER TO binbase;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    NO MINVALUE
    CACHE 20;


ALTER TABLE binbase.hibernate_sequence OWNER TO binbase;

--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('hibernate_sequence', 1, false);


--
-- Name: job_id; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE job_id
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    MINVALUE 0
    CACHE 20;


ALTER TABLE binbase.job_id OWNER TO binbase;

--
-- Name: job_id; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('job_id', 1, false);


--
-- Name: library; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE library (
    id int8 NOT NULL,
    description character varying(255),
    name character varying(255)
);


ALTER TABLE binbase.library OWNER TO binbase;

--
-- Name: library_spec; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE library_spec (
    "specId" int8 NOT NULL,
    id int8,
    spectra text,
    name character varying(255)
);


ALTER TABLE binbase.library_spec OWNER TO binbase;

--
-- Name: link_id; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE link_id
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    NO MINVALUE
    CACHE 20;


ALTER TABLE binbase.link_id OWNER TO binbase;

--
-- Name: link_id; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('link_id', 1, false);


--
-- Name: locking; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE locking (
    id double precision,
    description character varying(255),
    locked_by character varying(255)
);


ALTER TABLE binbase.locking OWNER TO binbase;

--
-- Name: meta_key; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE meta_key (
    key_id double precision,
    key character varying(255)
);


ALTER TABLE binbase.meta_key OWNER TO binbase;

--
-- Name: metainformation; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE metainformation (
    setupxid character varying(25) NOT NULL,
    data text
);


ALTER TABLE binbase.metainformation OWNER TO binbase;

--
-- Name: qualitycontrol; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE qualitycontrol (
    id double precision NOT NULL,
    amount double precision NOT NULL
);


ALTER TABLE binbase.qualitycontrol OWNER TO binbase;

--
-- Name: quantification; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE quantification (
    sample_id int8 NOT NULL,
    version int8,
    result bytea
);


ALTER TABLE binbase.quantification OWNER TO binbase;

--
-- Name: quantification_id; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE quantification_id
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    MINVALUE 0
    CACHE 100;


ALTER TABLE binbase.quantification_id OWNER TO binbase;

--
-- Name: quantification_id; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('quantification_id', 1, false);


--
-- Name: rawdata; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE rawdata (
    name character varying(255) NOT NULL,
    data bytea,
    version timestamp without time zone,
    visible character varying(5) DEFAULT 'TRUE'::character varying,
    hash double precision NOT NULL
);


ALTER TABLE binbase.rawdata OWNER TO binbase;

--
-- Name: reference; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE reference (
    id int8 NOT NULL,
    bin_id int8,
    value character varying(3000),
    class_id double precision
);


ALTER TABLE binbase.reference OWNER TO binbase;

--
-- Name: reference_class; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE reference_class (
    id int8 NOT NULL,
    name character varying(255),
    description character varying(3000),
    pattern character varying(2000)
);


ALTER TABLE binbase.reference_class OWNER TO binbase;

--
-- Name: result; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE result (
    result_id double precision NOT NULL,
    name character varying(255),
    setupx character varying(255),
    description character varying(2000),
    pattern character varying(2000)
);


ALTER TABLE binbase.result OWNER TO binbase;

--
-- Name: result_id; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE result_id
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    NO MINVALUE
    CACHE 20;


ALTER TABLE binbase.result_id OWNER TO binbase;

--
-- Name: result_id; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('result_id', 1, false);


--
-- Name: result_link; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE result_link (
    result_id double precision,
    sample_id double precision,
    id double precision NOT NULL,
    class character varying(256)
);


ALTER TABLE binbase.result_link OWNER TO binbase;

--
-- Name: runtime; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE runtime (
    sample_id double precision NOT NULL,
    operation character varying(255) NOT NULL,
    start double precision NOT NULL,
    "end" double precision NOT NULL,
    "group" character varying(255),
    type double precision
);


ALTER TABLE binbase.runtime OWNER TO binbase;

--
-- Name: sample_id; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE sample_id
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    MINVALUE 0
    CACHE 100;


ALTER TABLE binbase.sample_id OWNER TO binbase;

--
-- Name: sample_id; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('sample_id', 1, false);


--
-- Name: sample_info; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE sample_info (
    sample_id double precision,
    key_id double precision,
    value character varying(2048),
    id double precision
);


ALTER TABLE binbase.sample_info OWNER TO binbase;

--
-- Name: spectra; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE spectra (
    uniquemass double precision,
    signal_noise double precision,
    purity double precision,
    retention_index double precision,
    retention_time double precision,
    sample_id double precision,
    spectra_id double precision NOT NULL,
    apex text,
    spectra text,
    apex_sn double precision,
    bin_id double precision,
    match double precision DEFAULT (-1),
    new_bin character varying(6) DEFAULT 'FALSE'::character varying,
    found_at_correction character varying(6) DEFAULT 'FALSE'::character varying,
    problematic character varying(6) DEFAULT 'FALSE'::character varying,
    leco character varying(25)
);


ALTER TABLE binbase.spectra OWNER TO binbase;

--
-- Name: spectra_found; Type: VIEW; Schema: binbase; Owner: binbase
--

CREATE VIEW spectra_found AS
    SELECT spectra.uniquemass, spectra.signal_noise, spectra.purity, spectra.retention_index, spectra.retention_time, spectra.sample_id, spectra.spectra_id, spectra.apex, spectra.spectra, spectra.apex_sn, spectra.bin_id, spectra.match, spectra.new_bin, spectra.found_at_correction, spectra.leco FROM spectra WHERE (spectra.bin_id IS NOT NULL);


ALTER TABLE binbase.spectra_found OWNER TO binbase;

--
-- Name: spectra_id; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE spectra_id
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    NO MINVALUE
    CACHE 20;


ALTER TABLE binbase.spectra_id OWNER TO binbase;

--
-- Name: spectra_id; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('spectra_id', 1, false);


--
-- Name: spectra_not_found; Type: VIEW; Schema: binbase; Owner: binbase
--

CREATE VIEW spectra_not_found AS
    SELECT spectra.uniquemass, spectra.signal_noise, spectra.purity, spectra.retention_index, spectra.retention_time, spectra.sample_id, spectra.spectra_id, spectra.apex, spectra.spectra, spectra.apex_sn FROM spectra WHERE (spectra.bin_id IS NULL);


ALTER TABLE binbase.spectra_not_found OWNER TO binbase;

--
-- Name: standard; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE standard (
    bin_id int8 NOT NULL,
    qualifier double precision NOT NULL,
    min_ratio double precision NOT NULL,
    min_apex_sn double precision NOT NULL,
    max_ratio double precision NOT NULL,
    min_distance_ratio double precision DEFAULT 0.9,
    max_distance_ratio double precision DEFAULT 1.1,
    min_similarity double precision DEFAULT 800,
    required character varying(5) DEFAULT 'TRUE'::character varying
);


ALTER TABLE binbase.standard OWNER TO binbase;

--
-- Name: standard_hist; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE standard_hist (
    bin_id double precision,
    qualifier double precision,
    min_ratio double precision,
    min_apex_sn double precision,
    max_ratio double precision,
    min_distance_ratio double precision,
    max_distance_ratio double precision,
    min_similarity double precision,
    required character varying(5),
    changed_at timestamp without time zone,
    changed_by character varying(255),
    id double precision
);


ALTER TABLE binbase.standard_hist OWNER TO binbase;

--
-- Name: structure; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE structure (
    id double precision NOT NULL,
    molare_mass double precision,
    smiles character varying(2000),
    formula character varying(2000),
    bin_id double precision
);


ALTER TABLE binbase.structure OWNER TO binbase;

--
-- Name: substance_classes; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE substance_classes (
    id int8 NOT NULL,
    name character varying(255),
    smile character varying(1000),
    describtion character varying(3000)
);


ALTER TABLE binbase.substance_classes OWNER TO binbase;

--
-- Name: synonyme; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE synonyme (
    id double precision NOT NULL,
    name character varying(255),
    bin_id double precision
);


ALTER TABLE binbase.synonyme OWNER TO binbase;

--
-- Name: type; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE type (
    id double precision NOT NULL,
    name character varying(255),
    pattern character varying(255) NOT NULL,
    create_bin character varying(5) DEFAULT 'FALSE'::character varying,
    description text
);


ALTER TABLE binbase.type OWNER TO binbase;

--
-- Name: type_id; Type: SEQUENCE; Schema: binbase; Owner: binbase
--

CREATE SEQUENCE type_id
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483646
    NO MINVALUE
    CACHE 20;


ALTER TABLE binbase.type_id OWNER TO binbase;

--
-- Name: type_id; Type: SEQUENCE SET; Schema: binbase; Owner: binbase
--

SELECT pg_catalog.setval('type_id', 1, false);


--
-- Name: user_validation; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE user_validation (
    user_id double precision,
    sample_id double precision
);


ALTER TABLE binbase.user_validation OWNER TO binbase;

--
-- Name: virtual_bin; Type: TABLE; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE TABLE virtual_bin (
    bin_id int8 NOT NULL,
    parent_id int8,
    ion_a int8,
    ion_b int8,
    name character varying(255),
    ratio double precision,
    export character varying(5) DEFAULT 'TRUE'::character varying
);


ALTER TABLE binbase.virtual_bin OWNER TO binbase;

--
-- Name: view_virtualbin; Type: VIEW; Schema: binbase; Owner: binbase
--

CREATE VIEW view_virtualbin AS
    SELECT virtual_bin.ratio, virtual_bin.ion_b, virtual_bin.ion_a, virtual_bin.bin_id, bin.signal_noise, bin.apex_sn, bin.apex, bin.spectra, bin.retention_index, bin.purity, bin.spectra_id, bin.quantmass, virtual_bin.export, virtual_bin.name FROM bin, virtual_bin WHERE (bin.bin_id = virtual_bin.parent_id);


ALTER TABLE binbase.view_virtualbin OWNER TO binbase;


create unique index bin_spectra_unique on bin(spectra_id);


ALTER TABLE "library_spec"
        ADD COLUMN "retention_index" int8 NULL;

CREATE INDEX "library_spec_retention_index_key"
        ON "library_spec"("retention_index");

CREATE TABLE "library_match"  ( 
	bin_id         	bigint NOT NULL,
	spectra_id        	bigint NOT NULL,
	similarity     	double precision NOT NULL,
	retention_index_diff	bigint NOT NULL ,
	name	 varchar(2000) NOT NULL

	)
;

ALTER TABLE binbase.library_match OWNER TO binbase;

create unique index library_match_index on library_match(bin_id,spectra_id);

--
-- Name: bin_compare_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY bin_compare
    ADD CONSTRAINT bin_compare_pkey PRIMARY KEY (id);


--
-- Name: bin_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY bin
    ADD CONSTRAINT bin_pkey PRIMARY KEY (bin_id);


--
-- Name: bin_references_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY bin_references
    ADD CONSTRAINT bin_references_pkey PRIMARY KEY (id);


--
-- Name: classification_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY classification
    ADD CONSTRAINT classification_pkey PRIMARY KEY (id);


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: configuration_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY configuration
    ADD CONSTRAINT configuration_pkey PRIMARY KEY (configuration_id);


--
-- Name: library_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY library
    ADD CONSTRAINT library_pkey PRIMARY KEY (id);


--
-- Name: library_spec_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY library_spec
    ADD CONSTRAINT library_spec_pkey PRIMARY KEY ("specId");


--
-- Name: quantification_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY quantification
    ADD CONSTRAINT quantification_pkey PRIMARY KEY (sample_id);


--
-- Name: rawdata_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY rawdata
    ADD CONSTRAINT rawdata_pkey PRIMARY KEY (name, hash);


--
-- Name: reference_class_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY reference_class
    ADD CONSTRAINT reference_class_pkey PRIMARY KEY (id);


--
-- Name: reference_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY reference
    ADD CONSTRAINT reference_pkey PRIMARY KEY (id);


--
-- Name: result_link_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY result_link
    ADD CONSTRAINT result_link_pkey PRIMARY KEY (id);


--
-- Name: result_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY result
    ADD CONSTRAINT result_pkey PRIMARY KEY (result_id);


--
-- Name: samples_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY samples
    ADD CONSTRAINT samples_pkey PRIMARY KEY (sample_id);


--
-- Name: spectra_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY spectra
    ADD CONSTRAINT spectra_pkey PRIMARY KEY (spectra_id);


--
-- Name: standard_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY standard
    ADD CONSTRAINT standard_pkey PRIMARY KEY (bin_id);


--
-- Name: structure_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY structure
    ADD CONSTRAINT structure_pkey PRIMARY KEY (id);


--
-- Name: substance_classes_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY substance_classes
    ADD CONSTRAINT substance_classes_pkey PRIMARY KEY (id);


--
-- Name: synonyme_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY synonyme
    ADD CONSTRAINT synonyme_pkey PRIMARY KEY (id);


--
-- Name: type_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY type
    ADD CONSTRAINT type_pkey PRIMARY KEY (id);


--
-- Name: virtual_bin_pkey; Type: CONSTRAINT; Schema: binbase; Owner: binbase; Tablespace: 
--

ALTER TABLE ONLY virtual_bin
    ADD CONSTRAINT virtual_bin_pkey PRIMARY KEY (bin_id);


--
-- Name: bin_export_index; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX bin_export_index ON bin USING btree (export);


--
-- Name: comments_type_index; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX comments_type_index ON comments USING btree (type);


--
-- Name: meta_key_index; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX meta_key_index ON meta_key USING btree (key);


--
-- Name: qualification_index_sample_id; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE UNIQUE INDEX qualification_index_sample_id ON quantification USING btree (sample_id);


--
-- Name: refrence_binid_index; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX refrence_binid_index ON reference USING btree (bin_id);


--
-- Name: result_link_index; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX result_link_index ON result_link USING btree (result_id);


--
-- Name: sample_info_id; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX sample_info_id ON sample_info USING btree (key_id);


--
-- Name: sample_info_id_sample; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX sample_info_id_sample ON sample_info USING btree (sample_id);


--
-- Name: sample_info_value; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX sample_info_value ON sample_info USING btree (value);


--
-- Name: spectra_binid; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX spectra_binid ON spectra USING btree (bin_id);


--
-- Name: spectra_index_sample_bin; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX spectra_index_sample_bin ON spectra USING btree (bin_id, sample_id);


--
-- Name: spectra_primaery; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE UNIQUE INDEX spectra_primaery ON spectra USING btree (spectra_id);


--
-- Name: spectra_sample_id; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE INDEX spectra_sample_id ON spectra USING btree (sample_id);


--
-- Name: standard_index_binid; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE UNIQUE INDEX standard_index_binid ON standard USING btree (bin_id);


--
-- Name: user01; Type: INDEX; Schema: binbase; Owner: binbase; Tablespace: 
--

CREATE UNIQUE INDEX user01 ON user_validation USING btree (user_id, sample_id);


--
-- Name: FK454B6D93BDFDA0EB; Type: FK CONSTRAINT; Schema: binbase; Owner: binbase
--

ALTER TABLE ONLY virtual_bin
    ADD CONSTRAINT "FK454B6D93BDFDA0EB" FOREIGN KEY (parent_id) REFERENCES bin(bin_id);

    
--
-- We need a column for the bin table to hold the date of generation
-- which should be by default NOW
--

alter table bin add column date_of_generation timestamp; 

alter table bin alter column date_of_generation SET DEFAULT CURRENT_TIMESTAMP;

--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;

--
-- stored procedures
--

/**
 * converts and absolute spectra to a relative spectra
 */
create or replace function createRelavieSpectra(spectra float8[]) returns float8[] AS $$

DECLARE
    result float8[1000];
    array_len int;
    maxValue float8 := 0;

BEGIN

    array_len = array_upper(spectra,1);
    
    for i in 1 .. array_len
    LOOP
            IF spectra[i] is not null and spectra[i] > maxValue
            THEN
               maxValue := spectra[i];
            END IF;
    END LOOP;
    
    for i in 1 .. array_len
    LOOP
        if spectra[i] is not null 
        then
            result[i] := spectra[i]/maxValue * 100;
        end if;
    END LOOP;

    RETURN result;

END;
$$ LANGUAGE plpgsql;

-- converts a spectra string to a float array for further use --
create or replace function convertSpectra(spectra text) returns float8[] AS $$

DECLARE
    result float8[1000];
    spectraSplit text[];
    ionSplit text[];
    array_len int;

    ion int;
    value float8;

BEGIN

    spectraSplit = regexp_split_to_array(spectra,' ');
    array_len = array_upper(spectraSplit,1);
    
    for i in 1 .. array_len
    LOOP
            ionSplit = regexp_split_to_array(spectraSplit[i],':');

           ion := ionSplit[1];
           value := ionSplit[2];


            result[ion] = value;

    END LOOP;

    RETURN result;

END;
$$ LANGUAGE plpgsql;

/**
 * calculates the similarity between two massspecs
 */
CREATE OR REPLACE FUNCTION binbase.calculatesimilarity (in unknown text, in library text) RETURNS float8 AS
$BODY$

DECLARE
    result float8;
    sameIons int[];
    sameSpectraRelativeValuesunknown float8[];
    sameSpectraAbsoluteValuesunknown float8[];

    sameSpectraRelativeValueslibrary float8[];
    sameSpectraAbsoluteValueslibrary float8[];

    unknownSpectra float8[];
    unknownSpectraRel float8[];

    librarySpectra float8[];
    librarySpectraRel float8[];

    unknownSpectraLength int :=0;

    f1 float8 := 0;
    f2 float8 := 0;

    lib float8 := 0;
    unk float8 := 0;

    sqrt1 float8 := 0;
    summ float8 := 0;
    summ4 float8 := 0;
    summ2 float8 := 0;
    summ3 float8 := 0;

    array_len int;
    sameIon int;

BEGIN

    unknownSpectra = convertSpectra(unknown);
    unknownSpectraRel = createRelavieSpectra(unknownSpectra);

    librarySpectra = convertSpectra(library);
    librarySpectraRel = createRelavieSpectra(librarySpectra);

    array_len = 1000;

    sameIon = 0;

    for i in 1 .. array_len
    LOOP
        -- this will contain all the identical ions --
        IF unknownSpectra[i] is not null and librarySpectra[i] is not null
        then
            sameIons[sameIon] = i;
            sameSpectraRelativeValuesunknown[sameIon] = unknownSpectraRel[i];
            sameSpectraAbsoluteValuesunknown[sameIon] = unknownSpectra[i];
            sameSpectraRelativeValueslibrary[sameIon] = librarySpectraRel[i];
            sameSpectraAbsoluteValueslibrary[sameIon] = librarySpectra[i];
            sameIon = sameIon + 1;
        END IF;
       
    END LOOP;


    -- calculate f1 --
    for i in 1 .. sameIon
    LOOP
        -- this will contain all the identical ions --
        IF sameIons[i] is not null 
        then
            sqrt1 = sqrt(sameSpectraRelativeValueslibrary[i] * sameSpectraRelativeValuesunknown[i]);
            summ4 = summ4 + (sqrt1 * sameIons[i]);

            IF i > 0 
            THEN
                unk = sameSpectraRelativeValuesunknown[i]/sameSpectraRelativeValuesunknown[i-1];
                lib = sameSpectraRelativeValueslibrary[i]/sameSpectraRelativeValueslibrary[i-1];

                if unk <= lib
                then
                    summ = summ + (unk/lib);
                else
                    summ = summ + (lib/unk);
                end if;
            END IF;
        END IF;
    END LOOP;

    unknownSpectraLength = 0;

    for i in 1 .. array_len
    LOOP
        IF librarySpectra[i] is not null and librarySpectra[i] > 0
        then
            summ2 = summ2 + (librarySpectraRel[i] * i);
        END IF;
       
        IF unknownSpectra[i] is not null and unknownSpectra[i] > 0
        then
            unknownSpectraLength = unknownSpectraLength + 1;
            summ3 = summ3 + (unknownSpectraRel[i] * i);
        END IF;
    END LOOP;

    f1 = summ4 / sqrt(summ2 * summ3);
    f2 = 1.0/sameIon * summ;

    result = (1000.0/(unknownSpectraLength + sameIon))*((unknownSpectraLength * f1) + (sameIon * f2));

    RETURN result;

EXCEPTION
    WHEN division_by_zero THEN
        RAISE NOTICE 'caught division_by_zero';
        RETURN 0;
END;
$BODY$
LANGUAGE 'plpgsql'

;

/**
 * deletes all the matches whenever a library is deleted
 */
CREATE or replace FUNCTION deletelibmatch (in spec_id_to_match int8) RETURNS void AS
$BODY$

begin

        delete from library_match where spectra_id = spec_id_to_match;

        return;
end;
$BODY$
LANGUAGE 'plpgsql'

;



/**
 * calculates all the matches whenever a library spectra is insert
 */
CREATE or replace FUNCTION insertlibmatch (in spec_id_to_match int8) RETURNS void AS
$BODY$

declare

    matchRecord record;

begin

        delete from library_match where spectra_id = spec_id_to_match;

    for matchRecord in
        select a.bin_id, b."specId", calculateSimilarity(b.spectra,a.spectra) as sim,abs(a.retention_index - b.retention_index) as diff ,b.name from bin a, library_spec b where  b."specId" = spec_id_to_match and abs(a.retention_index - b.retention_index) < 5000
    loop
                insert into library_match values (matchRecord.bin_id,matchRecord."specId",matchRecord.sim,matchRecord.diff,matchRecord.name);
    END LOOP;
    return;
end;
$BODY$
LANGUAGE 'plpgsql'

;


/**
 * when ever a new bin is created this function should be called
 */
create or replace function insertSpecMatches() returns trigger as
$BODY$

begin
    perform * from insertlibMatch(new."specId");

    return null;

end;
$BODY$
LANGUAGE 'plpgsql'

;

/**
 * whenever a bin is delete this function should be called
 */
create or replace function removeSpecMatches() returns trigger as
$BODY$

begin
    perform * from deletelibMatch(old."specId");

    return null;

end;
$BODY$
LANGUAGE 'plpgsql'


;



/**
 * calculates all the matches whenever a bin is insert
 */
CREATE or replace FUNCTION insertmatch (in bin_id_to_match int8) RETURNS void AS
$BODY$

declare

    matchRecord record;

begin

        delete from library_match where bin_id = bin_id_to_match;

    for matchRecord in
        select a.bin_id, b."specId", calculateSimilarity(b.spectra,a.spectra) as sim,abs(a.retention_index - b.retention_index) as diff ,b.name from bin a, library_spec b where a.bin_id = bin_id_to_match and abs(a.retention_index - b.retention_index) < 5000
    loop
                insert into library_match values (matchRecord.bin_id,matchRecord."specId",matchRecord.sim,matchRecord.diff,matchRecord.name);
    END LOOP;
    return;
end;
$BODY$
LANGUAGE 'plpgsql'

;


/**
 * deletes all the matches whenever a bin is deleted
 */
CREATE or replace FUNCTION deletematch (in bin_id_to_match int8) RETURNS void AS
$BODY$

begin

        delete from library_match where bin_id = bin_id_to_match;

    return;
end;
$BODY$
LANGUAGE 'plpgsql'

;



/**
 * when ever a new bin is created this function should be called
 */
create or replace function insertBinMatches() returns trigger as
$BODY$

begin
    perform * from  insertMatch(new.bin_id);

    return null;

end;
$BODY$
LANGUAGE 'plpgsql'

;

/**
 * whenever a bin is delete this function should be called
 */
create or replace function removeBinMatches() returns trigger as
$BODY$

begin
    perform * from  deleteMatch(old.bin_id);

    return null;

end;
$BODY$
LANGUAGE 'plpgsql'

;

/**
 * called when a bin is insert and should update all the matches
 */
create trigger insert_bin_matches after insert  on bin for each row execute procedure insertBinMatches() ;

/**
 * called when a bin is removed and should delete all the matches
 */
create trigger remove_bin_matches after delete  on bin for each row execute procedure removeBinMatches() ;

/**
 * when ever a spectra is insert this should be called
 */
create trigger insert_spec_matches after insert  on library_spec for each row execute procedure insertSpecMatches() ;

/**
 * when ever a spectra is removed, this should be called
 */
create trigger remove_spec_matches after delete  on library_spec for each row execute procedure removeSpecMatches() ;


--
-- InchiKey to link bins to each other and different compounds
--
alter table bin add column inchi_key char(27);

/**
 * 4.2.0 changes
 */
 
DROP VIEW bin_samples;
DROP VIEW experiment_class;
DROP VIEW spectra_found;
DROP VIEW spectra_not_found;

ALTER TABLE "spectra" ALTER COLUMN "sample_id" TYPE int8;
ALTER TABLE "spectra" ALTER COLUMN "spectra_id" TYPE int8;
ALTER TABLE "spectra" ALTER COLUMN "bin_id" TYPE int8;
ALTER TABLE "samples" ALTER COLUMN "sample_id" TYPE int8;
ALTER TABLE "result_link" ALTER COLUMN "result_id" TYPE int8;
ALTER TABLE "result_link" ALTER COLUMN "sample_id" TYPE int8;
ALTER TABLE "result_link" ALTER COLUMN "id" TYPE int8;
ALTER TABLE "result" ALTER COLUMN "result_id" TYPE int8;
CREATE VIEW bin_samples
AS
SELECT samples.sample_id, samples.configuration_id, samples.correction_failed, samples.created_bin, samples.class, samples.sample_name, samples."allowNewBin" AS allownewbin, samples.priority, samples.saturated, samples.setupx_id, samples.status, samples.version, samples.finished, samples.visible, samples.date, samples.sod, samples.type FROM samples WHERE (samples.sample_id IN (SELECT bin.sample_id FROM bin));
;
CREATE VIEW experiment_class
AS
SELECT samples.class FROM samples GROUP BY samples.class;
;
CREATE VIEW spectra_found
AS
SELECT spectra.uniquemass, spectra.signal_noise, spectra.purity, spectra.retention_index, spectra.retention_time, spectra.sample_id, spectra.spectra_id, spectra.apex, spectra.spectra, spectra.apex_sn, spectra.bin_id, spectra.match, spectra.new_bin, spectra.found_at_correction, spectra.leco FROM spectra WHERE (spectra.bin_id IS NOT NULL);
;
CREATE VIEW spectra_not_found
AS
SELECT spectra.uniquemass, spectra.signal_noise, spectra.purity, spectra.retention_index, spectra.retention_time, spectra.sample_id, spectra.spectra_id, spectra.apex, spectra.spectra, spectra.apex_sn FROM spectra WHERE (spectra.bin_id IS NULL);
;
CREATE INDEX "spectra_bin_id_not_null"
	ON "spectra"("bin_id") where bin_id is not null;

ALTER TABLE "result_link" ALTER COLUMN "sample_id" TYPE int8;
