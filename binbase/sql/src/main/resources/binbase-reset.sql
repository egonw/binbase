delete from bin where bin_id not in (select bin_id from standard)
GO
delete from samples where sample_id not in (select sample_id from bin)
GO
delete from spectra where sample_id not in (select sample_id from bin)
GO
delete from result_link
GO
delete from result
GO
delete from rawdata
go
delete from bin_group where group_id not in (select group_id from bin)
go
delete from reference where bin_id not in (select bin_id from bin)
go
delete from library
go
delete from library_match
go
delete from library_spec
go
delete from correction_data;
go
delete from runtime;
go
delete from configuration;
go
delete from quantification
